package de._125m125.ktv2.mail;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;

public class MailManagerTest extends DatabaseTester {

    MailManager uut;

    @Before
    public void beforeMailManagerTest() {
        this.uut = new MailManager();
        Whitebox.setInternalState(this.uut, Clock.fixed(Instant.ofEpochMilli(10000), ZoneId.of("UTC")));
    }

    @Test
    public void testCheckAndUpdateEmailDelayStatus_nonExistant() throws Exception {
        final long checkAndUpdateEmailDelayStatus = this.uut.checkAndUpdateEmailDelayStatus("example@example.com");
        assertEquals(0L, checkAndUpdateEmailDelayStatus);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(true, rs.getBoolean("active"));
                assertEquals(1, rs.getInt("count"));
                assertTrue(rs.getTimestamp("lastSent").getTime() > System.currentTimeMillis() - 1000);
            }
        }
    }

    @Test
    public void testCheckAndUpdateEmailDelayStatus_existingAllowed() throws Exception {
        final Operation op = insertInto("mailcounter").columns("mail", "count", "lastSent")
                .values("example@example.com", 1, new Timestamp(4000)).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final long checkAndUpdateEmailDelayStatus = this.uut.checkAndUpdateEmailDelayStatus("example@example.com");
        assertEquals(0L, checkAndUpdateEmailDelayStatus);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(true, rs.getBoolean("active"));
                assertEquals(2, rs.getInt("count"));
                assertTrue(rs.getTimestamp("lastSent").getTime() > System.currentTimeMillis() - 1000);
            }
        }
    }

    @Test
    public void testCheckAndUpdateEmailDelayStatus_locked() throws Exception {
        final Operation op = insertInto("mailcounter").columns("mail", "active", "count", "lastSent")
                .values("example@example.com", false, 1, new Timestamp(4000)).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final long checkAndUpdateEmailDelayStatus = this.uut.checkAndUpdateEmailDelayStatus("example@example.com");
        assertEquals(-1L, checkAndUpdateEmailDelayStatus);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(false, rs.getBoolean("active"));
                assertEquals(1, rs.getInt("count"));
                assertEquals(new Timestamp(4000), rs.getTimestamp("lastSent"));
            }
        }
    }

    @Test
    public void testCheckAndUpdateEmailDelayStatus_existingTimeout() throws Exception {
        final Operation op = insertInto("mailcounter").columns("mail", "count", "lastSent")
                .values("example@example.com", 1, new Timestamp(6000)).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final long checkAndUpdateEmailDelayStatus = this.uut.checkAndUpdateEmailDelayStatus("example@example.com");
        assertEquals(1000L, checkAndUpdateEmailDelayStatus);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(true, rs.getBoolean("active"));
                assertEquals(1, rs.getInt("count"));
                assertEquals(new Timestamp(6000), rs.getTimestamp("lastSent"));
            }
        }
    }

    @Test
    public void testResetCount_empty() throws Exception {
        final boolean resetCount = this.uut.resetCount("example@example.com");
        assertTrue(resetCount);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertFalse(rs.next());
            }
        }
    }

    @Test
    public void testResetCount_existing() throws Exception {
        final Operation op = insertInto("mailcounter").columns("mail", "count", "lastSent")
                .values("example@example.com", 1, new Timestamp(6000)).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final boolean resetCount = this.uut.resetCount("example@example.com");
        assertTrue(resetCount);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(true, rs.getBoolean("active"));
                assertEquals(0, rs.getInt("count"));
            }
        }
    }

    @Test
    public void testDeactivate_new() throws Exception {
        final boolean deactivate = this.uut.deactivate("example@example.com");
        assertTrue(deactivate);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(false, rs.getBoolean("active"));
                assertEquals(0, rs.getInt("count"));
            }
        }
    }

    @Test
    public void testDeactivate_existing() throws Exception {
        final Operation op = insertInto("mailcounter").columns("mail", "count", "lastSent")
                .values("example@example.com", 1, new Timestamp(6000)).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final boolean deactivate = this.uut.deactivate("example@example.com");
        assertTrue(deactivate);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(false, rs.getBoolean("active"));
                assertEquals(1, rs.getInt("count"));
            }
        }
    }

    @Test
    public void testActivate_new() throws Exception {
        final boolean activate = this.uut.activate("example@example.com");
        assertTrue(activate);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertFalse(rs.next());
            }
        }
    }

    @Test
    public void testActivate_existing_notLocked() throws Exception {
        final Operation op = insertInto("mailcounter").columns("mail", "count", "lastSent")
                .values("example@example.com", 1, new Timestamp(6000)).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final boolean activate = this.uut.activate("example@example.com");
        assertTrue(activate);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(true, rs.getBoolean("active"));
                assertEquals(0, rs.getInt("count"));
            }
        }
    }

    @Test
    public void testActivate_existing_locked() throws Exception {
        final Operation op = insertInto("mailcounter").columns("mail", "active", "count", "lastSent")
                .values("example@example.com", false, 1, new Timestamp(6000)).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final boolean activate = this.uut.activate("example@example.com");
        assertTrue(activate);

        try (Connection con = DatabaseTester.c.getConnection(); Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM mailcounter")) {
                assertTrue(rs.next());
                assertEquals("example@example.com", rs.getString("mail"));
                assertEquals(true, rs.getBoolean("active"));
                assertEquals(0, rs.getInt("count"));
            }
        }
    }
}
