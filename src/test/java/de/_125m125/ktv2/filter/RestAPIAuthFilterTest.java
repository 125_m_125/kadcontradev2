package de._125m125.ktv2.filter;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class RestAPIAuthFilterTest extends DatabaseTester {
    private RestAPIAuthFilter              uut;
    private HttpServletRequest             request;
    private ContainerRequestContext        requestContext;
    private UriInfo                        uriInfo;
    private HttpSession                    session;
    private MultivaluedMap<String, String> multivaluedMap;
    private MultivaluedMap<String, String> multivaluedMap2;

    @Before
    public void beforeRestAPIAuthFilterTest() {
        this.request = mock(HttpServletRequest.class);
        this.requestContext = mock(ContainerRequestContext.class);
        this.uriInfo = mock(UriInfo.class);
        this.multivaluedMap = new MultivaluedHashMap<>();
        this.multivaluedMap2 = new MultivaluedHashMap<>();
        this.uut = new RestAPIAuthFilter();
        this.session = mock(HttpSession.class);

        when(this.requestContext.getUriInfo()).thenReturn(this.uriInfo);
        when(this.uriInfo.getPathParameters()).thenReturn(this.multivaluedMap);
        when(this.uriInfo.getQueryParameters()).thenReturn(this.multivaluedMap2);
        when(this.requestContext.getMethod()).thenReturn("GET");
        Whitebox.setInternalState(this.uut, this.request);
    }

    @Test(expected = BadRequestException.class)
    public void testDoFilter_twoUsers() throws Exception {
        this.multivaluedMap.add("user", "1");
        this.multivaluedMap.add("user", "2");
        when(this.request.getHeader("Authorization")).thenReturn("Basic Mjo0");
        try {
            this.uut.filter(this.requestContext);
        } catch (final BadRequestException e) {
            assertEquals("UnexpectedSecondUser", e.getMessage());
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void testDoFilter_nanUser() throws Exception {
        this.multivaluedMap.add("user", "Da");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_authFailure() throws Exception {
        this.multivaluedMap.add("user", "1");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_authFailure_detectsQueryParameterUser() throws Exception {
        this.multivaluedMap2.add("user", "1");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_authFailure_detectsQueryParameterUid() throws Exception {
        this.multivaluedMap2.add("uid", "1");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_basicAuth_authNotBasic() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");
        when(this.request.getHeader("Authorization")).thenReturn("Complex Mjo0");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_basicAuth_emptyAuth() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");
        when(this.request.getHeader("Authorization")).thenReturn("");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_basicAuth_noColon() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");
        when(this.request.getHeader("Authorization")).thenReturn("Basic Mi00");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_basicAuth_setsBasicAuthRequiredHeader() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");
        try {
            this.uut.filter(this.requestContext);
        } catch (final NotAuthorizedException e) {
            assertEquals("Basic realm=\"API\"", e.getResponse().getHeaderString("WWW-Authenticate"));
            throw e;
        }
    }

    @Test
    public void testDoFilter_basicAuth_success() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");
        when(this.request.getHeader("Authorization")).thenReturn("Basic Mjo0");

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request).setAttribute("authType", "btkn-2");
        verify(this.request).setAttribute("permissions", 3);
        verify(this.request).setAttribute("id", 1L);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_basicAuth_wrongAuth() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");
        when(this.request.getHeader("Authorization")).thenReturn("Basic bm90OmNvcnJlY3Q=");

        this.uut.filter(this.requestContext);
    }

    @Test
    public void testDoFilter_noAuth() throws Exception {
        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request, times(0)).setAttribute(eq("authType"), any());
        verify(this.request, times(0)).setAttribute(eq("permissions"), any());
        verify(this.request, times(0)).setAttribute(eq("id"), any());
    }

    @Test
    public void testDoFilter_requestOther_admin() throws Exception {
        when(this.session.getAttribute("id")).thenReturn(2L);
        when(this.session.getAttribute("admin")).thenReturn(true);

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request, times(1)).setAttribute("authType", "pwd");
        verify(this.request, times(1)).setAttribute("permissions", Permission.ALL_PERMISSIONS.getInteger());
        verify(this.request, times(1)).setAttribute("id", 1L);
    }

    @Test(expected = ForbiddenException.class)
    public void testDoFilter_requestOther_admin_post() throws Exception {
        when(this.session.getAttribute("id")).thenReturn(2L);
        when(this.session.getAttribute("admin")).thenReturn(true);
        when(this.requestContext.getHeaderString("content-type")).thenReturn("abc");

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        when(this.requestContext.getMethod()).thenReturn("POST");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = ForbiddenException.class)
    public void testDoFilter_requestOther_noAdmin() throws Exception {
        when(this.session.getAttribute("id")).thenReturn(2L);
        when(this.session.getAttribute("admin")).thenReturn(false);

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = ForbiddenException.class)
    public void testDoFilter_requestOther_noAdmin_post() throws Exception {
        when(this.session.getAttribute("id")).thenReturn(2L);
        when(this.session.getAttribute("admin")).thenReturn(false);

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        when(this.requestContext.getMethod()).thenReturn("POST");
        when(this.requestContext.getHeaderString("content-type")).thenReturn("abc");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = ForbiddenException.class)
    public void testDoFilter_sessionIdAuth_wrongUser() throws Exception {
        when(this.session.getAttribute("id")).thenReturn(2L);

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        this.uut.filter(this.requestContext);
    }

    @Test
    public void testDoFilter_sessionIdAuth_success() throws Exception {
        when(this.session.getAttribute("id")).thenReturn(1L);

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request).setAttribute("authType", "pwd");
        verify(this.request).setAttribute("permissions", Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        verify(this.request).setAttribute("id", 1L);
    }

    @Test(expected = ForbiddenException.class)
    public void testDoFilter_sessionTidAuth_wrongUser() throws Exception {
        when(this.session.getAttribute("tbid")).thenReturn(2L);
        when(this.session.getAttribute("tknid")).thenReturn(3L);
        when(this.session.getAttribute("permissions")).thenReturn(3);

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        this.uut.filter(this.requestContext);
    }

    @Test
    public void testDoFilter_sessionTokenAuth_success() throws Exception {
        when(this.session.getAttribute("tbid")).thenReturn(1L);
        when(this.session.getAttribute("tknid")).thenReturn(2L);
        when(this.session.getAttribute("permissions")).thenReturn(3);

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request).setAttribute("authType", "stkn-2");
        verify(this.request).setAttribute("permissions", 3);
        verify(this.request).setAttribute("id", 1L);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_tokenAuth_failure_tidToken() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(anyBoolean())).thenReturn(this.session);
        this.multivaluedMap.add("user", "1");
        when(this.request.getParameter("tid")).thenReturn("2");
        when(this.request.getParameter("tkn")).thenReturn("3");

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_tokenAuth_failure_tidWrong() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);

        this.multivaluedMap.add("user", "1");
        when(this.request.getParameter("tid")).thenReturn("1");
        when(this.request.getParameter("tkn")).thenReturn("4");

        this.uut.filter(this.requestContext);

        final ArgumentCaptor<Response> abortedWith = ArgumentCaptor.forClass(Response.class);
        verify(this.requestContext).abortWith(abortedWith.capture());
        assertEquals(401, abortedWith.getValue().getStatus());
        assertEquals("Basic realm=\"API\"", abortedWith.getValue().getHeaderString("WWW-Authenticate"));
        verify(this.request).setAttribute("authFailure", true);
    }

    @Test
    public void testDoFilter_tokenAuth_success() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);

        this.multivaluedMap.add("user", "1");
        this.multivaluedMap2.add("tid", "2");
        this.multivaluedMap2.add("tkn", "4");

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request).setAttribute("authType", "tkn-2");
        verify(this.request).setAttribute("permissions", 3);
        verify(this.request).setAttribute("id", 1L);
    }

    @Test
    public void testDoFilter_tokenAuth_success_base10Auth() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1693563801, 1693563800, 3, 4226879282195208740L).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        this.multivaluedMap.add("user", "D1693563801");
        this.multivaluedMap2.add("tid", "D1693563800");
        this.multivaluedMap2.add("tkn", "D4226879282195208740");

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request).setAttribute("authType", "tkn-D1693563800");
        verify(this.request).setAttribute("permissions", 3);
        verify(this.request).setAttribute("id", 1693563801L);
    }

    @Test
    public void testDoFilter_tokenAuth_success_longToken() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1693563801, 1693563800, 3, 4226879282195208740L).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        this.multivaluedMap.add("user", "1if3esp");
        this.multivaluedMap2.add("tid", "1if3eso");
        this.multivaluedMap2.add("tkn", "3la749vlr86h4");

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request).setAttribute("authType", "tkn-1if3eso");
        verify(this.request).setAttribute("permissions", 3);
        verify(this.request).setAttribute("id", 1693563801L);
    }

    @Test
    public void testDoFilter_signatureAuth_success() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        this.multivaluedMap.add("user", "1");

        this.multivaluedMap2.add("tid", "2");
        this.multivaluedMap2.add("timestamp", "1000");
        this.multivaluedMap2.add("signature", "c7bba0c0266423e0dc148e2f09a0a9076527a8c1a7e2f703444d210a7fe40186");

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);

        Whitebox.setInternalState(this.uut, Clock.fixed(Instant.ofEpochMilli(1000), ZoneId.of("NZ")));

        this.uut.filter(this.requestContext);

        verify(this.requestContext, times(0)).abortWith(any());
        verify(this.request).setAttribute("authType", "sig-2");
        verify(this.request).setAttribute("permissions", 3);
        verify(this.request).setAttribute("id", 1L);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_signatureAuth_signatureWrong() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        this.multivaluedMap.add("user", "1");

        this.multivaluedMap2.add("tid", "2");
        this.multivaluedMap2.add("timestamp", "1000");
        this.multivaluedMap2.add("signature", "c7bba0c0266423e0dc148e2f09a0a9076527a8c1a7e2f703444d210a7fe40187");

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);

        Whitebox.setInternalState(this.uut, Clock.fixed(Instant.ofEpochMilli(1000), ZoneId.of("NZ")));

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_signatureAuth_timestampWrong() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        this.multivaluedMap.add("user", "1");

        final Map<String, String[]> params = new HashMap<>();
        params.put("tid", new String[] { "2" });
        params.put("timestamp", new String[] { "1000" });
        params.put("signature", new String[] { "c7bba0c0266423e0dc148e2f09a0a9076527a8c1a7e2f703444d210a7fe40186" });

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        when(this.request.getParameter(any()))
                .thenAnswer(invocation -> params.get(invocation.getArgumentAt(0, String.class)) != null
                        ? params.get(invocation.getArgumentAt(0, String.class))[0] : null);
        when(this.request.getParameterMap()).thenReturn(params);

        Whitebox.setInternalState(this.uut, Clock.fixed(Instant.ofEpochMilli(1000000), ZoneId.of("NZ")));

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_signatureAuth_signatureMissing() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        this.multivaluedMap.add("user", "1");

        final Map<String, String[]> params = new HashMap<>();
        params.put("tid", new String[] { "2" });
        params.put("timestamp", new String[] { "1000" });

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        when(this.request.getParameter(any()))
                .thenAnswer(invocation -> params.get(invocation.getArgumentAt(0, String.class)) != null
                        ? params.get(invocation.getArgumentAt(0, String.class))[0] : null);
        when(this.request.getParameterMap()).thenReturn(params);

        Whitebox.setInternalState(this.uut, Clock.fixed(Instant.ofEpochMilli(1000), ZoneId.of("NZ")));

        this.uut.filter(this.requestContext);
    }

    @Test(expected = NotAuthorizedException.class)
    public void testDoFilter_signatureAuth_timestampMissing() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        this.multivaluedMap.add("user", "1");

        final Map<String, String[]> params = new HashMap<>();
        params.put("tid", new String[] { "2" });
        params.put("signature", new String[] { "a8df4c33fb090bce6bd4c2b10a05a390adefde0196c170307d0e1831279fa9c8" });

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(true)).thenReturn(this.session);
        when(this.request.getParameter(any()))
                .thenAnswer(invocation -> params.get(invocation.getArgumentAt(0, String.class)) != null
                        ? params.get(invocation.getArgumentAt(0, String.class))[0] : null);
        when(this.request.getParameterMap()).thenReturn(params);

        Whitebox.setInternalState(this.uut, Clock.fixed(Instant.ofEpochMilli(1000), ZoneId.of("NZ")));

        this.uut.filter(this.requestContext);
    }
}
