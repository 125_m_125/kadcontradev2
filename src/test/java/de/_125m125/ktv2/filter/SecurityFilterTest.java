package de._125m125.ktv2.filter;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import com.google.common.collect.ImmutableSet;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.logging.Log;

@RunWith(MockitoJUnitRunner.class)
public class SecurityFilterTest {
    @Mock
    RequestDispatcher   rd;
    @Mock
    FilterChain         chain;
    @Mock
    HttpServletRequest  request;
    @Mock
    HttpServletResponse response;
    @Mock
    HttpSession         session;

    SecurityFilter      uut;

    @BeforeClass
    public static void beforeClass() {
        Log.disable();
        Whitebox.setInternalState(Variables.class, "ALLOWED_HOSTS", new String[] { "125m125.de" });
        Whitebox.setInternalState(Variables.class, "allowedServers",
                ImmutableSet.copyOf(new String[] { "125m125.de" }));
    }

    @AfterClass
    public static void afterClass() {
        Log.enable();
    }

    @Before
    public void before() {
        this.uut = new SecurityFilter();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(false)).thenReturn(this.session);
        when(this.request.getRequestDispatcher("/forbiddenHost.jsp")).thenReturn(this.rd);
        when(this.request.getRequestURI()).thenReturn("/index");
    }

    @Test
    public void testDoFilter_invalidServerName() throws Exception {
        when(this.request.getServerName()).thenReturn("badServerName");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(this.chain, times(0)).doFilter(any(), any());
    }

    @Test
    public void testDoFilter_get() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.chain, times(1)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_notLoggedIn() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.chain, times(1)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_loggedIn_id() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.session.getAttribute("id")).thenReturn(1L);

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(this.chain, times(0)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_loggedIn_tknid() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.session.getAttribute("tknid")).thenReturn(1L);

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(this.chain, times(0)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_wrongOrigin() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getHeader("origin")).thenReturn("badServerName");
        when(this.request.getParameter("csrfPreventionSalt")).thenReturn("validSalt");
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.session.getAttribute("csrfPreventionSalt")).thenReturn("validSalt");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(this.chain, times(0)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_wrongReferer() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getHeader("referer")).thenReturn("badServerName");
        when(this.request.getParameter("csrfPreventionSalt")).thenReturn("validSalt");
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.session.getAttribute("csrfPreventionSalt")).thenReturn("validSalt");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(this.chain, times(0)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_noCSRFPrevSalt() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.session.getAttribute("id")).thenReturn(1L);

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(this.chain, times(0)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_wrongCSRFPrevSalt() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getParameter("csrfPreventionSalt")).thenReturn("invalidSalt");
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.session.getAttribute("csrfPreventionSalt")).thenReturn("validSalt");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_FORBIDDEN);
        verify(this.chain, times(0)).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_validPost() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getParameter("csrfPreventionSalt")).thenReturn("validSalt");
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.session.getAttribute("csrfPreventionSalt")).thenReturn("validSalt");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.chain, times(1)).doFilter(this.request, this.response);
        verify(this.session, times(1)).removeAttribute("csrfPreventionSalt");
    }

    @Test
    public void testDoFilter_validPost_oriAndRef() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getHeader("origin")).thenReturn("http://localhost");
        when(this.request.getHeader("referer")).thenReturn("http://localhost");
        when(this.request.getParameter("csrfPreventionSalt")).thenReturn("validSalt");
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.session.getAttribute("csrfPreventionSalt")).thenReturn("validSalt");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.chain, times(1)).doFilter(this.request, this.response);
        verify(this.session, times(1)).removeAttribute("csrfPreventionSalt");
    }

    @Test
    public void testDoFiler_keepsCsrfPreventionSaltWhenExisting() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("GET");
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.session.getAttribute("csrfPreventionSalt")).thenReturn("validSalt");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.chain, times(1)).doFilter(this.request, this.response);
        verify(this.session, times(0)).removeAttribute("csrfPreventionSalt");
        verify(this.session, times(0)).setAttribute(matches("csrfPreventionSalt"), any());
    }

    @Test
    public void testDoFiler_setsCsrfPreventionSalt() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("GET");
        when(this.session.getAttribute("id")).thenReturn(1L);

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.chain, times(1)).doFilter(this.request, this.response);
        verify(this.session, times(0)).removeAttribute("csrfPreventionSalt");
        verify(this.session, times(2)).setAttribute(matches("csrfPreventionSalt"), any());
    }

    @Test
    public void testDoFiler_leavesCsrfPreventionSaltForErrorreport() throws Exception {
        when(this.request.getServerName()).thenReturn("localhost");
        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getHeader("origin")).thenReturn("http://localhost");
        when(this.request.getHeader("referer")).thenReturn("http://localhost");
        when(this.request.getParameter("csrfPreventionSalt")).thenReturn("validSalt");
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.session.getAttribute("csrfPreventionSalt")).thenReturn("validSalt");
        when(this.request.getRequestURI()).thenReturn("/api/v2.0/users/1/errors");

        this.uut.doFilter(this.request, this.response, this.chain);

        verify(this.chain, times(1)).doFilter(this.request, this.response);
        verify(this.session, times(0)).removeAttribute("csrfPreventionSalt");
        verify(this.session, times(0)).setAttribute(matches("csrfPreventionSalt"), any());
    }

}
