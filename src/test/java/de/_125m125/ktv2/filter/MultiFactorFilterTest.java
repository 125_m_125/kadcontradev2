package de._125m125.ktv2.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.google.common.cache.Cache;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.twoFactorAuth.AuthState;

public class MultiFactorFilterTest extends DatabaseTester {
    @SuppressWarnings("unchecked")
    private static final Map<Long, List<WeakReference<AuthState>>> AUTH_STATES_PER_USERID = Whitebox
            .getInternalState(MultiFactorFilter.class, Map.class);
    @SuppressWarnings("unchecked")
    private static final Cache<String, AuthState>                  AUTH_STATES            = Whitebox
            .getInternalState(MultiFactorFilter.class, Cache.class);

    private HttpServletRequest                                     request;
    private HttpServletResponse                                    response;
    private FilterChain                                            chain;
    private HttpSession                                            s;
    private RequestDispatcher                                      rd;
    private AuthState                                              as;
    private final Map<String, String[]>                            parameterMap           = new HashMap<>();

    @Before
    public void beforeMultiFactorFilterTest() {
        MultiFactorFilterTest.AUTH_STATES.invalidateAll();
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.clear();
        this.parameterMap.clear();

        this.request = mock(HttpServletRequest.class);
        this.response = mock(HttpServletResponse.class);
        this.chain = mock(FilterChain.class);
        this.s = mock(HttpSession.class);
        this.rd = mock(RequestDispatcher.class);
        this.as = mock(AuthState.class);

        when(this.request.getRequestDispatcher(any())).thenReturn(this.rd);
        when(this.request.getParameter(any()))
                .thenAnswer(invocation -> this.parameterMap.get(invocation.getArgumentAt(0, String.class)) != null
                        ? this.parameterMap.get(invocation.getArgumentAt(0, String.class))[0] : null);
        when(this.request.getParameterMap()).thenReturn(this.parameterMap);

        // defaults
        when(this.s.getId()).thenReturn("session1");
        when(this.request.getMethod()).thenReturn("GET");
        when(this.request.getSession(false)).thenReturn(this.s);
        when(this.request.getRequestURI()).thenReturn("/");
        when(this.s.getAttribute("id")).thenReturn(1L);
    }

    @Test
    public void testDoFilter_alreadyAuthorized() throws Exception {
        when(this.s.getAttribute("multiAuth")).thenReturn(true);
        when(this.s.getAttribute("id")).thenReturn(1L);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.chain).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_noSession() throws Exception {
        when(this.request.getSession(false)).thenReturn(null);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.chain).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_notLoggedIn() throws Exception {
        when(this.s.getAttribute("id")).thenReturn(null);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.chain).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_noAuthStatus_no2FA() throws Exception {
        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.chain).doFilter(this.request, this.response);
        assertEquals(1, MultiFactorFilterTest.AUTH_STATES.size());
        assertEquals(1, MultiFactorFilterTest.AUTH_STATES_PER_USERID.size());
        final AuthState authState = MultiFactorFilterTest.AUTH_STATES.getIfPresent("session1");
        assertNotNull(authState);
        assertSame(authState, MultiFactorFilterTest.AUTH_STATES_PER_USERID.get(1L).get(0).get());
        assertTrue(authState.isAuthenticated());
    }

    @Test
    public void testDoFilter_authenticatedAuthStatus() throws Exception {
        MultiFactorFilterTest.AUTH_STATES.put("session1", this.as);
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.put(1L, Arrays.asList(new WeakReference<>(this.as)));

        when(this.as.isAuthenticated()).thenReturn(true);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.chain).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_unauthenticatedAuthStatus() throws Exception {
        MultiFactorFilterTest.AUTH_STATES.put("session1", this.as);
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.put(1L, Arrays.asList(new WeakReference<>(this.as)));

        when(this.as.isAuthenticated()).thenReturn(false);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.request).getRequestDispatcher("/multiFactor.jsp");
        verify(this.rd).forward(this.request, this.response);
    }

    @Test
    public void testDoFilter_unauthenticatedAuthStatus_static() throws Exception {
        MultiFactorFilterTest.AUTH_STATES.put("session1", this.as);
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.put(1L, Arrays.asList(new WeakReference<>(this.as)));

        when(this.as.isAuthenticated()).thenReturn(false);
        when(this.request.getRequestURI()).thenReturn("/static/js/test.js");

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.chain).doFilter(this.request, this.response);
    }

    @Test
    public void testDoFilter_unauthenticatedAuthStatus_triesLimitReached() throws Exception {
        MultiFactorFilterTest.AUTH_STATES.put("session1", this.as);
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.put(1L, Arrays.asList(new WeakReference<>(this.as)));

        when(this.as.isAuthenticated()).thenReturn(false);
        when(this.as.triesLimitReached()).thenReturn(true);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.request).getRequestDispatcher("/empty.jsp");
        verify(this.rd).forward(this.request, this.response);
        verify(this.request).setAttribute("error", Variables.translate("triesLimitReached"));
    }

    @Test
    public void testDoFilter_unauthenticatedAuthStatus_non2FaAuthPost() throws Exception {
        MultiFactorFilterTest.AUTH_STATES.put("session1", this.as);
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.put(1L, Arrays.asList(new WeakReference<>(this.as)));

        when(this.as.isAuthenticated()).thenReturn(false);
        when(this.request.getMethod()).thenReturn("POST");

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.request).getRequestDispatcher("/multiFactor.jsp");
        verify(this.rd).forward(this.request, this.response);
    }

    @Test
    public void testDoFilter_unauthenticatedAuthStatus_2FaAuthPost_success() throws Exception {
        MultiFactorFilterTest.AUTH_STATES.put("session1", this.as);
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.put(1L, Arrays.asList(new WeakReference<>(this.as)));

        when(this.as.isAuthenticated()).thenReturn(false);
        when(this.request.getMethod()).thenReturn("POST");
        this.parameterMap.put("2FaAuth", new String[] { "2FaAuth" });
        this.parameterMap.put("scAuth", new String[] { "dataData" });
        when(this.as.verify("SC", "dataData", true)).thenReturn(true);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.response).setStatus(HttpServletResponse.SC_SEE_OTHER);
        verify(this.response).setHeader("Location", "/?");
    }

    @Test
    public void testDoFilter_unauthenticatedAuthStatus_2FaAuthPost_failure() throws Exception {
        MultiFactorFilterTest.AUTH_STATES.put("session1", this.as);
        MultiFactorFilterTest.AUTH_STATES_PER_USERID.put(1L, Arrays.asList(new WeakReference<>(this.as)));

        when(this.as.isAuthenticated()).thenReturn(false);
        when(this.request.getMethod()).thenReturn("POST");
        this.parameterMap.put("2FaAuth", new String[] { "2FaAuth" });
        this.parameterMap.put("scData", new String[] { "dataData" });
        when(this.as.verify("SC", "dataData", true)).thenReturn(false);

        new MultiFactorFilter().doFilter(this.request, this.response, this.chain);

        verify(this.request).getRequestDispatcher("/multiFactor.jsp");
        verify(this.rd).forward(this.request, this.response);
        verify(this.request).setAttribute("error", Variables.translate("2FaFailed"));
    }

}
