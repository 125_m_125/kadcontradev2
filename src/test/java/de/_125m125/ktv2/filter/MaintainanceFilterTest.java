package de._125m125.ktv2.filter;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.logging.Log;

public class MaintainanceFilterTest {

    @BeforeClass
    public static void beforeClass() {
        Log.disable();
    }

    @AfterClass
    public static void afterClass() {
        Log.enable();
    }

    @Test
    public void testDoFilter_disabled() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("admin")).thenReturn(false);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/a/c");
        when(request.getContextPath()).thenReturn("/a");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        final FilterChain chain = mock(FilterChain.class);

        MaintainanceFilter.configure(false, null);
        final MaintainanceFilter uut = new MaintainanceFilter();
        uut.doFilter(request, null, chain);

        verify(request, times(0)).setAttribute(any(), any());
        verify(chain).doFilter(request, null);
    }

    @Test
    public void testDoFilter_allowUser() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("admin")).thenReturn(false);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/a/c");
        when(request.getContextPath()).thenReturn("/a");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        final FilterChain chain = mock(FilterChain.class);

        MaintainanceFilter.configure(true, new MaintainanceFilter.Settings(false, "/b"));
        final MaintainanceFilter uut = new MaintainanceFilter();
        uut.doFilter(request, null, chain);

        verify(request).setAttribute("info", Variables.translate("maintainance"));
        verify(chain).doFilter(request, null);
    }

    @Test
    public void testDoFilter_allowUser_adminIsNull() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("admin")).thenReturn(null);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/a/c");
        when(request.getContextPath()).thenReturn("/a");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        final FilterChain chain = mock(FilterChain.class);

        MaintainanceFilter.configure(true, new MaintainanceFilter.Settings(false, "/b"));
        final MaintainanceFilter uut = new MaintainanceFilter();
        uut.doFilter(request, null, chain);

        verify(request).setAttribute("info", Variables.translate("maintainance"));
        verify(chain).doFilter(request, null);
    }

    @Test
    public void testDoFilter_blockUser() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("admin")).thenReturn(false);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/a/b");
        when(request.getContextPath()).thenReturn("/a");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        final RequestDispatcher rd = mock(RequestDispatcher.class);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getRequestDispatcher("/empty.jsp")).thenReturn(rd);

        final FilterChain chain = mock(FilterChain.class);

        MaintainanceFilter.configure(true, new MaintainanceFilter.Settings(false, "/b"));
        final MaintainanceFilter uut = new MaintainanceFilter();
        uut.doFilter(request, response, chain);

        verify(request).setAttribute("error", Variables.translate("maintainanceError"));
        verify(response).setStatus(503);
        verify(rd).forward(request, response);
        verify(chain, times(0)).doFilter(any(), any());
    }

    @Test
    public void testDoFilter_blockUser_adminIsNull() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("admin")).thenReturn(null);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/a/b");
        when(request.getContextPath()).thenReturn("/a");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        final RequestDispatcher rd = mock(RequestDispatcher.class);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getRequestDispatcher("/empty.jsp")).thenReturn(rd);

        final FilterChain chain = mock(FilterChain.class);

        MaintainanceFilter.configure(true, new MaintainanceFilter.Settings(false, "/b"));
        final MaintainanceFilter uut = new MaintainanceFilter();
        uut.doFilter(request, response, chain);

        verify(request).setAttribute("error", Variables.translate("maintainanceError"));
        verify(response).setStatus(503);
        verify(rd).forward(request, response);
        verify(chain, times(0)).doFilter(any(), any());
    }

    @Test
    public void testDoFilter_allowAdmin() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("admin")).thenReturn(true);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/a/b");
        when(request.getContextPath()).thenReturn("/a");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        final FilterChain chain = mock(FilterChain.class);

        MaintainanceFilter.configure(true, new MaintainanceFilter.Settings(false, "/b"));
        final MaintainanceFilter uut = new MaintainanceFilter();
        uut.doFilter(request, null, chain);

        verify(request).setAttribute("info", Variables.translate("maintainanceAdmin"));
        verify(chain).doFilter(request, null);
    }

    @Test
    public void testDoFilter_blockAdmin() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("admin")).thenReturn(true);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/a/b");
        when(request.getContextPath()).thenReturn("/a");
        when(request.getSession()).thenReturn(session);

        final RequestDispatcher rd = mock(RequestDispatcher.class);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getRequestDispatcher("/empty.jsp")).thenReturn(rd);

        final FilterChain chain = mock(FilterChain.class);

        MaintainanceFilter.configure(true, new MaintainanceFilter.Settings(true, "/b"));
        final MaintainanceFilter uut = new MaintainanceFilter();
        uut.doFilter(request, response, chain);

        verify(request).setAttribute("error", Variables.translate("maintainanceError"));
        verify(response).setStatus(503);
        verify(rd).forward(request, response);
        verify(chain, times(0)).doFilter(any(), any());
    }

}
