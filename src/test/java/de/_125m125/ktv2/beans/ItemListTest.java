package de._125m125.ktv2.beans;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;

public class ItemListTest extends DatabaseTester {
    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.put("-1", "Kadis(-1)");
        Variables.ID_TO_DN.put("1", "Stone(1)");
    }

    @Test
    public void testGetCsvString_empty() {
        final ItemList uut = new ItemList();

        final String csvString = uut.getTsvString(true);

        assertEquals("id\tname\tamount\r\n", csvString);
    }

    @Test
    public void testGetCsvString() {
        final ItemList uut = new ItemList();
        uut.addEntry(new Item("-1", 1000000));
        uut.addEntry(new Item("1", 100));

        final String csvString = uut.getTsvString(true);

        assertEquals("id\tname\tamount\r\n-1\tKadis(-1)\t1.000000\r\n1\tStone(1)\t100\r\n", csvString);
    }
}
