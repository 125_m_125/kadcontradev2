package de._125m125.ktv2.beans;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;

public class ItemTest extends DatabaseTester {
    private Item item;

    @Before
    public void before() {
        this.item = new Item();
    }

    @Test
    public void testAmount_item() {
        this.item.setId("5");
        this.item.setAmount(500);

        Assert.assertEquals("500", this.item.getAmount());
    }

    @Test
    public void testAmount_money_1() {
        this.item.setId("-1");
        this.item.setAmount(1);

        Assert.assertEquals("0.000001", this.item.getAmount());
    }

    @Test
    public void testAmount_money_10() {
        this.item.setId("-1");
        this.item.setAmount(10);

        Assert.assertEquals("0.000010", this.item.getAmount());
    }

    @Test
    public void testAmount_money_100() {
        this.item.setId("-1");
        this.item.setAmount(100);

        Assert.assertEquals("0.000100", this.item.getAmount());
    }

    @Test
    public void testAmount_money_1000() {
        this.item.setId("-1");
        this.item.setAmount(1000);

        Assert.assertEquals("0.001000", this.item.getAmount());
    }

    @Test
    public void testAmount_money_highDouble() {
        this.item.setId("-1");
        this.item.setAmount(1234567890);

        Assert.assertEquals("1234.567890", this.item.getAmount());
    }

    @Test
    public void testAmount_money_highInt() {
        this.item.setId("-1");
        this.item.setAmount(2000000);

        Assert.assertEquals("2.000000", this.item.getAmount());
    }

    @Test
    public void testAmount_money_nearHighDouble() {
        this.item.setId("-1");
        this.item.setAmount(123456);

        Assert.assertEquals("0.123456", this.item.getAmount());
    }

    @Test
    public void testEquals() {
        final Item i1 = new Item(), i2 = new Item(), i3 = new Item(), i4 = new Item();
        final Integer i = new Integer(20);

        i1.setId("1");
        i2.setId("1");
        i3.setId("1");
        i4.setId("2");
        i1.setAmount(10);
        i2.setAmount(10);
        i3.setAmount(20);
        i4.setAmount(10);

        Assert.assertTrue(i1.equals(i1));
        Assert.assertTrue(i1.equals(i2));
        Assert.assertFalse(i1.equals(i3));
        Assert.assertFalse(i1.equals(i4));
        Assert.assertFalse(i1.equals(null));
        Assert.assertFalse(i1.equals(i));
    }

    @Test
    public void testHashEquality() {
        final Item i1 = new Item(), i2 = new Item();
        for (int i = 1; i < 1024; i *= 2) {
            i1.setId(String.valueOf(i));
            i2.setId(String.valueOf(i));
            i1.setAmount(i);
            i2.setAmount(i);
            Assert.assertTrue(i1.hashCode() == i2.hashCode());
        }
    }

    @Test
    public void testName() {
        Variables.ID_TO_DN.put("-1", "Kadis(-1)");
        this.item.setId("-1");
        Assert.assertEquals("Kadis(-1)", this.item.getName());
    }

    @Test
    public void testGetItemsFirstMissing() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount")
                .values("1", true, "4", 100).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Item[] items = Item.getItems(1, "-1", "4");

        assertEquals(new Item("-1", 0), items[0]);
        assertEquals(new Item("4", 100), items[1]);
    }

    @Test
    public void testGetItemsLastMissing() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount")
                .values("1", true, "4", 100).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Item[] items = Item.getItems(1, "4", "5");

        assertEquals(new Item("4", 100), items[0]);
        assertEquals(new Item("5", 0), items[1]);
    }

    @Test
    public void testGetItemsMiddleMissing() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount")
                .values("1", true, "4", 100).values("1", true, "-1", 20).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Item[] items = Item.getItems(1, "-1", "1", "4");

        assertEquals(new Item("-1", 20), items[0]);
        assertEquals(new Item("1", 0), items[1]);
        assertEquals(new Item("4", 100), items[2]);
    }

    @Test
    public void testGetItemsBothExist() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount")
                .values("1", true, "4", 100).values("1", true, "-1", 20).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Item[] items = Item.getItems(1, "-1", "4");

        assertEquals(new Item("-1", 20), items[0]);
        assertEquals(new Item("4", 100), items[1]);
    }

    @Test
    public void testGetItemsWrongOrder() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount")
                .values("1", true, "4", 100).values("1", true, "-1", 20).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Item[] items = Item.getItems(1, "4", "-1");

        assertEquals(new Item("-1", 20), items[0]);
        assertEquals(new Item("4", 100), items[1]);
    }

    @Test
    public void testAmountWholeNumberWithDecimal() {
        final Item uut = new Item("4", "14.0");

        assertEquals(14l, uut.getLongAmount());
    }
}
