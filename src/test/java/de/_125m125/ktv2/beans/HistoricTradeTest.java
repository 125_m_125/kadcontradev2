package de._125m125.ktv2.beans;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.Timestamp;

import org.junit.Test;

import de._125m125.ktv2.DatabaseTester;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;

import de._125m125.ktv2.beans.HistoricTrade;
import de._125m125.ktv2.market.Trade.buySell;

public class HistoricTradeTest extends DatabaseTester {

    @Test
    public void testGetLong() throws Exception {
        final Insert insert = insertInto("tradeHistory")
                .columns("id", "traderid", "type", "buysell", "material", "amount", "price", "time")
                .values(1, 2, true, true, 3, 4, 5, "2016-01-01 00:00:00")
                .values(2, 3, true, false, 4, 5, 6, "2016-01-01 01:01:01").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final HistoricTrade historicTrade = HistoricTrade.get(2);

        assertNotNull(historicTrade);
        assertEquals(2L, historicTrade.getId());
        assertEquals(3L, historicTrade.getOwner());
        assertEquals(buySell.SELL, historicTrade.getBuyOrSell());
        assertEquals("4", historicTrade.getMaterial());
        assertEquals(5L, historicTrade.getAmount());
        assertEquals(6L, historicTrade.getPrice());
        assertEquals(Timestamp.valueOf("2016-01-01 01:01:01"), historicTrade.getCreationTime());

    }

    @Test
    public void testGetLong_empty() throws Exception {
        final HistoricTrade historicTrade = HistoricTrade.get(0xDEAD);
        assertNull(historicTrade);
    }

    @Test
    public void testGetLongLongString_Item() throws Exception {
        final Insert insert = insertInto("tradeHistory")
                .columns("id", "traderid", "type", "buysell", "material", "amount", "price", "time")
                .values(1, 2, true, false, "4", 4, 5, "2016-01-01 00:00:00")
                .values(2, 2, true, false, "4", 5, 6, "2016-01-01 00:00:01")
                .values(3, 3, true, false, "4", 6, 7, "2016-01-01 01:01:01")
                .values(4, 2, true, true, "4", 7, 8, "2016-01-01 00:00:02")
                .values(5, 2, true, false, "5", 8, 9, "2016-01-01 00:00:03").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final HistoricTrade[] historicTrade = HistoricTrade.get(2, Timestamp.valueOf("2016-01-01 00:00:00"), "4");

        assertEquals(1, historicTrade.length);
        assertNotNull(historicTrade);
        assertEquals(2L, historicTrade[0].getId());
        assertEquals(2L, historicTrade[0].getOwner());
        assertEquals(buySell.SELL, historicTrade[0].getBuyOrSell());
        assertEquals("4", historicTrade[0].getMaterial());
        assertEquals(5L, historicTrade[0].getAmount());
        assertEquals(6L, historicTrade[0].getPrice());
        assertEquals(Timestamp.valueOf("2016-01-01 00:00:01"), historicTrade[0].getCreationTime());
    }

    @Test
    public void testGetLongLongString_Kadis() throws Exception {
        final Insert insert = insertInto("tradeHistory")
                .columns("id", "traderid", "type", "buysell", "material", "amount", "price", "time")
                .values(1, 2, true, true, "4", 4, 5, "2016-01-01 00:00:00")
                .values(2, 2, true, false, "4", 5, 6, "2016-01-01 00:00:01")
                .values(3, 3, true, true, "4", 6, 7, "2016-01-01 01:01:01")
                .values(4, 2, true, true, "4", 7, 8, "2016-01-01 00:00:02").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final HistoricTrade[] historicTrade = HistoricTrade.get(2, Timestamp.valueOf("2016-01-01 00:00:00"), "-1");

        assertEquals(1, historicTrade.length);
        assertNotNull(historicTrade);
        assertEquals(4L, historicTrade[0].getId());
        assertEquals(2L, historicTrade[0].getOwner());
        assertEquals(buySell.BUY, historicTrade[0].getBuyOrSell());
        assertEquals("4", historicTrade[0].getMaterial());
        assertEquals(7L, historicTrade[0].getAmount());
        assertEquals(8L, historicTrade[0].getPrice());
        assertEquals(Timestamp.valueOf("2016-01-01 00:00:02"), historicTrade[0].getCreationTime());
    }

    @Test
    public void testGetLongLongString_empty() throws Exception {
        final HistoricTrade[] historicTrade = HistoricTrade.get(0xDEAD, Timestamp.valueOf("2016-01-01 00:00:00"), "0");
        assertEquals(0, historicTrade.length);
    }

}
