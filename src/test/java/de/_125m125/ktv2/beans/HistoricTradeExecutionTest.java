package de._125m125.ktv2.beans;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;

import de._125m125.ktv2.DatabaseTester;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({ HistoricTrade.class })
public class HistoricTradeExecutionTest extends DatabaseTester {
    @Mock
    HistoricTrade ht;

    @Test
    public void testGetFor() throws Exception {
        mockStatic(HistoricTrade.class);
        final HistoricTrade ht2 = mock(HistoricTrade.class);
        when(HistoricTrade.get(2)).thenReturn(ht2);

        final Insert insert = insertInto("tradesDoneHistory").columns("time", "id1", "id2", "amount")
                .values("2016-01-01 00:00:00", 1, 2, 10).values("2016-01-01 00:00:00", 2, 1, 10)
                .values("2016-01-01 00:00:01", 2, 3, 10).values("2016-01-01 00:00:01", 3, 2, 10).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        when(this.ht.getId()).thenReturn(1L);

        final List<HistoricTradeExecution> result = HistoricTradeExecution.getFor(this.ht);

        assertEquals(1, result.size());
        assertEquals(Timestamp.valueOf("2016-01-01 00:00:00"), result.get(0).getExecutionTime());
        assertEquals(1L, result.get(0).getId1());
        assertEquals(2L, result.get(0).getId2());
        assertEquals(10L, result.get(0).getAmount());
        assertEquals(this.ht, result.get(0).getTrade1());
        assertEquals(ht2, result.get(0).getTrade2());
    }

    @Test
    public void testGetFor_empty() throws Exception {
        when(this.ht.getId()).thenReturn(0L);
        final List<HistoricTradeExecution> result = HistoricTradeExecution.getFor(this.ht);

        assertEquals(0, result.size());
    }

}
