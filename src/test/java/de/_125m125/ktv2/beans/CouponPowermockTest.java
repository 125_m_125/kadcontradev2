package de._125m125.ktv2.beans;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class CouponPowermockTest extends DatabaseTester {
    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    @AfterClass
    public static void afterClass() {
        Variables.ID_TO_DN.clear();
    }

    @Test
    @PrepareForTest(Coupon.class)
    public void testCreate_duplicate() throws Exception {
        spy(Coupon.class);
        when(Coupon.class, "nextCode").thenReturn(BigInteger.valueOf(1L), BigInteger.valueOf(5L));

        final Operation op = insertInto("giftcodes").columns("precode", "code", "material", "amount")
                .values("t1", 1L, "1", 4L).values("t2", 5L, "2", 5L).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final List<BigInteger> create = Coupon.create("t1", "4", 10, 1);

        assertEquals(1, create.size());
        assertEquals(BigInteger.valueOf(5L), create.get(0));

        final ArrayList<HashMap<String, Object>> codes = Main.instance().getConnector()
                .executeQuery("SELECT * FROM giftcodes ORDER BY precode, code");
        assertEquals(3, codes.size());

        assertEquals("t1", codes.get(0).get("preCode"));
        assertEquals(BigInteger.valueOf(1L), codes.get(0).get("code"));
        assertEquals("1", codes.get(0).get("material"));
        assertEquals(BigInteger.valueOf(4L), codes.get(0).get("amount"));

        assertEquals("t1", codes.get(1).get("preCode"));
        assertEquals(BigInteger.valueOf(5L), codes.get(1).get("code"));
        assertEquals("4", codes.get(1).get("material"));
        assertEquals(BigInteger.valueOf(10L), codes.get(1).get("amount"));

        assertEquals("t2", codes.get(2).get("preCode"));
        assertEquals(BigInteger.valueOf(5L), codes.get(2).get("code"));
        assertEquals("2", codes.get(2).get("material"));
        assertEquals(BigInteger.valueOf(5L), codes.get(2).get("amount"));
    }
}
