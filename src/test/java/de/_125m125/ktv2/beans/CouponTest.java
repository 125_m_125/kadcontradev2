package de._125m125.ktv2.beans;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

@RunWith(JUnitParamsRunner.class)
public class CouponTest extends DatabaseTester {

    @Test
    @Parameters(method = "parametersForTestCodeToString")
    @TestCaseName("testCodeToString_{2}")
    public void testCodeToString(final String preString, final BigInteger code, final String expected)
            throws Exception {
        assertEquals(expected, Coupon.codeToString(preString, code));
    }

    @SuppressWarnings("unused")
    private Object[] parametersForTestCodeToString() {
        // @formatter:off
        return new Object[] {
                new Object[] { "t1", new BigInteger("00000000000000000001"), "ktt1-0000-0000-0000-1" },
                new Object[] { "t2", new BigInteger("00000000000000000011"), "ktt2-0000-0000-0000-b" },
                new Object[] { "t3", new BigInteger("18446744073709551615"), "ktt3-fvvv-vvvv-vvvv-v" },
                new Object[] { "t4", new BigInteger("01234567890123456789"), "ktt4-128g-guhu-uj08-l" },
        };
        // @formatter:on
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodeToString_preStringTooLong() throws Exception {
        Coupon.codeToString("lon", new BigInteger("00000000000000000001"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodeToString_preStringTooShort() throws Exception {
        Coupon.codeToString("s", new BigInteger("00000000000000000001"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodeToString_preStringIsNull() throws Exception {
        Coupon.codeToString(null, new BigInteger("00000000000000000001"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodeToString_codeTooSmall() throws Exception {
        Coupon.codeToString("te", new BigInteger("-00000000000000000001"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodeToString_codeTooBig() throws Exception {
        Coupon.codeToString("te", new BigInteger("18446744073709551616"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCodeToString_codeIsNull() throws Exception {
        Coupon.codeToString("te", null);
    }

    @Test
    @Parameters(method = "parametersForTestStringToCode")
    @TestCaseName("testCodeToString_{2}")
    public void testStringToCode(final String input, final BigInteger expected) throws Exception {
        assertEquals(expected, Coupon.stringToCode(input));
    }

    @SuppressWarnings("unused")
    private Object[] parametersForTestStringToCode() {
        // @formatter:off
        return new Object[] {
                new Object[] { "ktt1-0000-0000-0000-1", new BigInteger("00000000000000000001") },
                new Object[] { "ktt2000000000000b", new BigInteger("00000000000000000011") },
                new Object[] { "ktt3-fv-vv-vv-vv-vv-vv-v", new BigInteger("18446744073709551615") },
                new Object[] { "ktw4-128g-guhu-uj08-l", new BigInteger("01234567890123456789") },
        };
        // @formatter:on
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStringToCode_inputIsNull() throws Exception {
        Coupon.stringToCode(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStringToCode_inputIsTooBig() throws Exception {
        Coupon.stringToCode("ktt4-g000-0000-0000-0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStringToCode_inputHasWrongCharcount() throws Exception {
        Coupon.stringToCode("ktt4-0000-0000-0000-00");
    }

    @Test(expected = NumberFormatException.class)
    public void testStringToCode_inputIsInvalid() throws Exception {
        Coupon.stringToCode("ktt4-128g-guhu-uj08-w");
    }

    @Test
    public void testGetIfExistsString_exists() throws Exception {
        final Operation op = insertInto("giftcodes").columns("preCode", "code", "amount", "material")
                .values("ab", 1, 10, "4").values("ab", 2, 11, "5").values("cd", 2, 12, "6").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Coupon result = Coupon.getIfExists("ktab-0000-0000-0000-2");

        assertEquals("ab", result.getPreCode());
        assertEquals(new BigInteger("2"), result.getCode());
        assertEquals(11, result.getAmount());
        assertEquals("5", result.getMaterial());
    }

    @Test
    public void testGetIfExistsString_nonExistant() throws Exception {
        final Operation op = insertInto("giftcodes").columns("preCode", "code", "amount", "material")
                .values("ab", 1, 10, "4").values("ab", 2, 11, "5").values("cd", 2, 12, "6").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Coupon result = Coupon.getIfExists("ktab-0000-0000-0000-3");

        assertNull(result);
    }

    @Test
    public void testRedeemIfPossible() throws Exception {
        final Operation op = insertInto("giftcodes").columns("preCode", "code", "amount", "material")
                .values("ab", 1, 10, "4").values("ab", 2, 11, "5").values("cd", 2, 12, "6").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Coupon result = Coupon.redeemIfPossible(7L, "ktab-0000-0000-0000-2");

        assertEquals(7L, (long) result.getRedeemer());

        try (Connection c = DatabaseTester.ds.getConnection(); Statement s = c.createStatement()) {
            try (ResultSet rs = s.executeQuery("SELECT * FROM giftcodes ORDER BY preCode, code")) {
                assertTrue(rs.next());
                assertEquals("ab", rs.getString("preCode"));
                assertEquals(1, rs.getInt("code"));
                assertEquals(10, rs.getInt("amount"));
                assertEquals("4", rs.getString("material"));
                rs.getLong("redeemer");
                assertTrue(rs.wasNull());

                assertTrue(rs.next());
                assertEquals("ab", rs.getString("preCode"));
                assertEquals(2, rs.getInt("code"));
                assertEquals(11, rs.getInt("amount"));
                assertEquals("5", rs.getString("material"));
                assertEquals(7L, rs.getLong("redeemer"));

                assertTrue(rs.next());
                assertEquals("cd", rs.getString("preCode"));
                assertEquals(2, rs.getInt("code"));
                assertEquals(12, rs.getInt("amount"));
                assertEquals("6", rs.getString("material"));
                rs.getLong("redeemer");
                assertTrue(rs.wasNull());

                assertFalse(rs.next());
            }

            try (ResultSet rs = s.executeQuery("SELECT * FROM materials")) {
                assertTrue(rs.next());
                assertEquals(7, rs.getInt("id"));
                assertTrue(rs.getBoolean("type"));
                assertEquals("5", rs.getString("matid"));
                assertEquals(11, rs.getInt("amount"));

                assertFalse(rs.next());
            }
        }
    }

    @Test
    public void testRedeemIfPossible_alreadyRedeemed() throws Exception {
        final Operation op = insertInto("giftcodes").columns("preCode", "code", "amount", "material", "redeemer")
                .values("ab", 1, 10, "4", null).values("ab", 2, 11, "5", 1).values("cd", 2, 12, "6", null).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Coupon result = Coupon.redeemIfPossible(7L, "ktab-0000-0000-0000-2");

        assertNull(result);

        try (Connection c = DatabaseTester.ds.getConnection(); Statement s = c.createStatement()) {
            try (ResultSet rs = s.executeQuery("SELECT * FROM giftcodes ORDER BY preCode, code")) {
                assertTrue(rs.next());
                assertEquals("ab", rs.getString("preCode"));
                assertEquals(1, rs.getInt("code"));
                assertEquals(10, rs.getInt("amount"));
                assertEquals("4", rs.getString("material"));
                rs.getLong("redeemer");
                assertTrue(rs.wasNull());

                assertTrue(rs.next());
                assertEquals("ab", rs.getString("preCode"));
                assertEquals(2, rs.getInt("code"));
                assertEquals(11, rs.getInt("amount"));
                assertEquals("5", rs.getString("material"));
                assertEquals(1L, rs.getLong("redeemer"));

                assertTrue(rs.next());
                assertEquals("cd", rs.getString("preCode"));
                assertEquals(2, rs.getInt("code"));
                assertEquals(12, rs.getInt("amount"));
                assertEquals("6", rs.getString("material"));
                rs.getLong("redeemer");
                assertTrue(rs.wasNull());

                assertFalse(rs.next());
            }

            try (ResultSet rs = s.executeQuery("SELECT * FROM materials")) {
                assertFalse(rs.next());
            }
        }
    }

    @Test
    public void testRedeemIfPossible_unknownCoupon() throws Exception {
        final Operation op = insertInto("giftcodes").columns("preCode", "code", "amount", "material", "redeemer")
                .values("ab", 1, 10, "4", null).values("ab", 2, 11, "5", null).values("cd", 2, 12, "6", null).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final Coupon result = Coupon.redeemIfPossible(7L, "ktab-0000-0000-0000-3");

        assertNull(result);

        try (Connection c = DatabaseTester.ds.getConnection(); Statement s = c.createStatement()) {
            try (ResultSet rs = s.executeQuery("SELECT * FROM giftcodes ORDER BY preCode, code")) {
                assertTrue(rs.next());
                assertEquals("ab", rs.getString("preCode"));
                assertEquals(1, rs.getInt("code"));
                assertEquals(10, rs.getInt("amount"));
                assertEquals("4", rs.getString("material"));
                rs.getLong("redeemer");
                assertTrue(rs.wasNull());

                assertTrue(rs.next());
                assertEquals("ab", rs.getString("preCode"));
                assertEquals(2, rs.getInt("code"));
                assertEquals(11, rs.getInt("amount"));
                assertEquals("5", rs.getString("material"));
                rs.getLong("redeemer");
                assertTrue(rs.wasNull());

                assertTrue(rs.next());
                assertEquals("cd", rs.getString("preCode"));
                assertEquals(2, rs.getInt("code"));
                assertEquals(12, rs.getInt("amount"));
                assertEquals("6", rs.getString("material"));
                rs.getLong("redeemer");
                assertTrue(rs.wasNull());

                assertFalse(rs.next());
            }

            try (ResultSet rs = s.executeQuery("SELECT * FROM materials")) {
                assertFalse(rs.next());
            }
        }
    }
}
