package de._125m125.ktv2.beans;

import java.sql.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

@RunWith(JUnitParamsRunner.class)
public class HistoryEntryTest {

    @Test
    public void testHistoryEntry() throws Exception {
        final HistoryEntry uut = new HistoryEntry(1L, 2L, 3L, 4L, 5L, 6L, new Date(0));

        Assert.assertEquals("0.000001", uut.getOpen());
        Assert.assertEquals("0.000002", uut.getClose());
        Assert.assertEquals("0.000003", uut.getLow());
        Assert.assertEquals("0.000004", uut.getHigh());
        Assert.assertEquals(5L, uut.getAmount());
        Assert.assertEquals(new Date(0), uut.getDate());
        Assert.assertEquals("1970-01-01\t0.000001\t0.000004\t0.000003\t0.000002\t5\t0.000006\r\n",
                uut.getTsvString(false));
    }

    @Test
    @Parameters
    @TestCaseName("testHistoryEntry_incomplete_{0}")
    public void testHistoryEntry_incomplete(final String name, final Long open, final Long close, final Long low,
            final Long high, final long amount, final long total, final Date date, final String csvString)
            throws Exception {
        final HistoryEntry uut = new HistoryEntry(open, close, low, high, amount, total, date);

        Assert.assertEquals(csvString, uut.getTsvString(false));
    }

    public Object[] parametersForTestHistoryEntry_incomplete() {
        return new Object[][] { { "missingAll", null, 2L, null, null, 5, 6, new Date(0),
                "1970-01-01\t\t\t\t0.000002\t5\t0.000006\r\n" }, };
    }
}
