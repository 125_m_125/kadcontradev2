package de._125m125.ktv2.updates;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import de._125m125.ktv2.DatabaseTester;

public class MysqlCleanerTest extends DatabaseTester {

    @Test
    public void testExecute_oldLog() throws Exception {
        DatabaseTester.c.executeUpdate(
                "INSERT INTO logging VALUES(1,\"2001-01-01 00:00:00\",\"aa\",800,0,\"Test\"),(2,CURRENT_TIMESTAMP,\"aa\",800,0,\"Test2\")");

        new MysqlCleaner().execute(LocalDate.now(), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT id FROM logging");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(2L, result.get(0).get("id"));
    }

    @Test
    public void testExecute_oldLogSR() throws Exception {
        DatabaseTester.c.executeUpdate(
                "INSERT INTO logging VALUES(1,\"2001-01-01 00:00:00\",\"sr\",800,0,\"Test\"),(2,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -15 DAY),\"sr\",800,0,\"Test2\")");

        new MysqlCleaner().execute(LocalDate.now(), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT id FROM logging");
        assertNull(result);
    }

    @Test
    public void testExecute_oldMessage() throws Exception {
        DatabaseTester.c
                .executeUpdate("INSERT INTO messages VALUES(1,1,\"Test\",\"2001-01-01 00:00:00\")");
        new MysqlCleaner().execute(LocalDate.now(), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT id FROM messages");
        assertNull(result);
    }

    @Test
    public void testExecute_newMessage() throws Exception {
        try (Connection con = DatabaseTester.c.getConnection()) {
            try (PreparedStatement st = con.prepareStatement(
                    "INSERT INTO messages VALUES(?,1,\"test\",CURRENT_TIMESTAMP)")) {
                for (int i = 0; i < 60;) {
                    st.setInt(1, ++i);
                    st.executeUpdate();
                }
            }
        }
        new MysqlCleaner().execute(LocalDate.now(), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT id FROM messages ORDER BY id");

        assertNotNull(result);
        assertEquals(60, result.size());
        for (long i = 1; i < 61; i++) {
            assertEquals(i, result.get((int) (i - 1)).get("id"));
        }
    }

}
