package de._125m125.ktv2.updates;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;

import de._125m125.ktv2.DatabaseTester;

public class InProgressPayoutCleanupTest extends DatabaseTester {

    @Test
    public void testExecute() throws Exception {
        final Date d = Date.valueOf("2016-11-11");
        final Insert insert = Insert.into("payout")
                .columns("uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown",
                        "payoutType", "date", "message", "agent")
                .values(1, true, "4", 10, true, false, false, true, false, "lieferung", d, "", null)
                .values(1, true, "4", 10, false, false, false, false, false, "bs2", d, "", 2)
                .values(1, true, "4", 10, false, false, false, true, true, "bs2", d, "", null)
                .values(1, true, "4", 10, false, true, true, false, false, "bs2", d, "test", 2)
                .values(1, true, "-1", 10000000L, false, false, false, false, false, "kadcon", d,
                        "blabla", 3)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final InProgressPayoutCleanup uut = new InProgressPayoutCleanup();
        Whitebox.setInternalState(uut,
                Clock.fixed(Instant.parse("2017-01-01T00:00:00.00Z"), ZoneId.of("Z")));
        uut.execute(LocalDate.parse("2017-01-01"), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");

        final String[] cNames = { "uid", "type", "matid", "amount", "userInteraction", "success",
                "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "payoutType",
                "date", "message", "agent" };
        //@formatter:off
        final Object[][] expected = new Object[][]{
            {1L, true, "4", 10L, true, false, false, true, false, "lieferung", d, "", null},
            {1L, true, "4", 10L, true, false, false, true, false, "bs2", d, "", 2L},
            {1L, true, "4", 10L, false, false, false, true, true, "bs2", d, "", null},
            {1L, true, "4", 10L, false, true, true, false, false, "bs2", d, "test", 2L},
            {1L, true, "-1", 10000000L, true, false, false, true, false, "kadcon", d, "blabla", 3L},
        };
        //@formatter:on
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < cNames.length; j++) {
                assertEquals("row " + i + " column " + j, expected[i][j],
                        result.get(i).get(cNames[j]));
            }
        }
        assertEquals(expected.length, result.size());
    }

    @Test
    public void testExecute_outOfTimeRange() throws Exception {
        final Date d = Date.valueOf("2016-11-11");
        final Insert insert = Insert.into("payout")
                .columns("uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown",
                        "payoutType", "date", "message", "agent")
                .values(1, true, "4", 10, false, false, false, false, false, "lieferung", d, "",
                        null)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final InProgressPayoutCleanup uut = new InProgressPayoutCleanup();
        Whitebox.setInternalState(uut,
                Clock.fixed(Instant.parse("2017-01-01T06:00:00.00Z"), ZoneId.of("Z")));
        uut.execute(LocalDate.parse("2017-01-01"), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertEquals(false, result.get(0).get("userInteraction"));
    }

}
