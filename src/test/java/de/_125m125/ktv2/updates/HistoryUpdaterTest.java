package de._125m125.ktv2.updates;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.UnavailableException;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.logging.Log;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

@RunWith(JUnitParamsRunner.class)
public class HistoryUpdaterTest {
    private static Connector c;

    public static final Operation DELETE_ALL = Operations.deleteAllFrom(Connector.TABLES);
    private static DataSource     ds;

    private final HttpSession s = mock(HttpSession.class);

    private final HistoryUpdater vc = mock(HistoryUpdater.class, Mockito.CALLS_REAL_METHODS);

    @AfterClass
    public static void afterClass() {
        Log.enable();
        HistoryUpdaterTest.c.close();
    }

    @BeforeClass
    public static void beforeClass() throws UnavailableException {
        Variables.dbname = "kttest";
        Variables.dbpass = "kttupass";
        Variables.dbuser = "kttu";
        Variables.setPoolSize(1);

        new Main(false);
        Log.disable();
        HistoryUpdaterTest.c = new Connector();
        Main.instance().setConnector(HistoryUpdaterTest.c);

        HistoryUpdaterTest.ds = HistoryUpdaterTest.c.getDataSource();

        HistoryUpdaterTest.c.executeUpdate("DROP DATABASE kttest");
        HistoryUpdaterTest.c.executeUpdate("CREATE DATABASE kttest");
        HistoryUpdaterTest.c.execute("use kttest");
        HistoryUpdaterTest.c.checktables();
    }

    @Before
    public void before() {
        Mockito.reset(this.s);
    }

    @Before
    public void prepareDB() {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds),
                HistoryUpdaterTest.DELETE_ALL);
        dbSetup.launch();
    }

    @Test
    public void testExecute_tradesdoneIsCleared() {
        final Date old = Date.valueOf(getOffsetDate(-1));
        final Date current = Date.valueOf(getCurrentDate());
        final Date future = Date.valueOf(getOffsetDate(5));

        final Insert i = Operations.insertInto("tradesdone")
                .columns("time", "type", "material", "amount", "price").values(old, true, 1, 1, 1)
                .values(current, true, 1, 1, 1).values(future, true, 1, 1, 1).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), i);
        dbSetup.launch();

        this.vc.execute(getCurrentDate(), HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                HistoryUpdaterTest.c.executeQuery("SELECT * FROM tradesdone");
        Assert.assertTrue("query did not return two entries",
                executeQuery != null && executeQuery.size() == 2);
        Assert.assertEquals(current, executeQuery.get(0).get("time"));
        Assert.assertEquals(future, executeQuery.get(1).get("time"));

    }

    @Test
    public void testExecute_Total() {
        final Operation op =
                insertInto("tradesdone").columns("time", "type", "material", "price", "amount")
                        .values("2015-01-01", true, 1, 50, 10).values("2015-01-01", true, 1, 10, 10)
                        .values("2015-01-01", true, 2, 30, 1).build();
        if (op != null) {
            final DbSetup dbSetup =
                    new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), op);
            dbSetup.launch();
        }
        final Date t = Date.valueOf(getCurrentDate());
        this.vc.execute(getCurrentDate(), HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery = HistoryUpdaterTest.c
                .executeQuery("SELECT * FROM history ORDER BY time,type,material");

        assertEquals(3, executeQuery.size());
        final Object[][] check = { { true, "-1", 1000000000L, 21L, 630L },
                { true, "1", 30L, 20L, 600L }, { true, "2", 30L, 1L, 30L } };
        for (int i = 0; i < 3; i++) {
            assertEquals(t, executeQuery.get(i).get("time"));
            assertEquals(check[i][0], executeQuery.get(i).get("type"));
            assertEquals(check[i][1], executeQuery.get(i).get("material"));
            assertEquals(check[i][2], executeQuery.get(i).get("value"));
            assertEquals(check[i][3], executeQuery.get(i).get("amount"));
            assertEquals(check[i][4], executeQuery.get(i).get("total"));
        }
    }

    @Test
    public void testExecute_index_single() {
        final LocalDate ld = getCurrentDate();
        final Date t = Date.valueOf(getCurrentDate());
        final Date tOld = Date.valueOf(getOffsetDate(-1));

        final Operation op = sequenceOf(
                insertInto("tradesdone").columns("time", "type", "material", "price", "amount")
                        .values("2015-01-01", true, 1, 0, 1).build(),
                insertInto("history")
                        .columns("time", "type", "material", "value", "amount", "total")
                        .values(tOld, true, -1, 1000000000, 0, 0)
                        .values(tOld, true, 1, 1000, 10000, 0).build());
        if (op != null) {
            final DbSetup dbSetup =
                    new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), op);
            dbSetup.launch();
        }
        this.vc.execute(ld, HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery = HistoryUpdaterTest.c
                .executeQuery("SELECT * FROM history ORDER BY time,type,material");

        final Object[][] check =
                { { tOld, true, "-1", 1000000000L, 0L, 0L }, { tOld, true, "1", 1000L, 10000L, 0L },
                        { t, true, "-1", 500000000L, 1L, 0L }, { t, true, "1", 500L, 1L, 0L } };
        assertEquals(check.length, executeQuery.size());
        for (int i = 0; i < check.length; i++) {
            assertEquals(i + "-ti: ", check[i][0], executeQuery.get(i).get("time"));
            assertEquals(i + "-ty: ", check[i][1], executeQuery.get(i).get("type"));
            assertEquals(i + "-ma: ", check[i][2], executeQuery.get(i).get("material"));
            assertEquals(i + "-va: ", check[i][3], executeQuery.get(i).get("value"));
            assertEquals(i + "-am: ", check[i][4], executeQuery.get(i).get("amount"));
            assertEquals(i + "-to: ", check[i][5], executeQuery.get(i).get("total"));
        }
    }

    @Test
    public void testExecute_index_multi() {
        final LocalDate t = getCurrentDate();
        final LocalDate tOld = getOffsetDate(-1);

        final Operation op = sequenceOf(
                insertInto("tradesdone").columns("time", "type", "material", "price", "amount")
                        .values("2015-01-01", true, 1, 0, 1).values("2015-01-01", true, 2, 1500, 10)
                        .build(),
                insertInto("history")
                        .columns("time", "type", "material", "value", "amount", "total")
                        .values(Date.valueOf(tOld), true, -1, 1000000000, 0, 0)
                        .values(Date.valueOf(tOld), true, 1, 1000, 10000, 0)
                        .values(Date.valueOf(tOld), true, 2, 1000, 100000, 0)
                        .values(Date.valueOf(tOld), true, 3, 1000, 100, 0).build());
        if (op != null) {
            final DbSetup dbSetup =
                    new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), op);
            dbSetup.launch();
        }
        this.vc.execute(t, HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery = HistoryUpdaterTest.c
                .executeQuery("SELECT * FROM history ORDER BY time,type,material");

        final Object[][] check = { { Date.valueOf(tOld), true, "-1", 1000000000L, 0L, 0L },
                { Date.valueOf(tOld), true, "1", 1000L, 10000L, 0L },
                { Date.valueOf(tOld), true, "2", 1000L, 100000L, 0L },
                { Date.valueOf(tOld), true, "3", 1000L, 100L, 0L },
                { Date.valueOf(t), true, "-1", 1181818181L, 11L, 15000L },
                { Date.valueOf(t), true, "1", 500L, 1L, 0L },
                { Date.valueOf(t), true, "2", 1250L, 10L, 15000L },
                { Date.valueOf(t), true, "3", 1000L, 0L, 0L } };
        assertEquals(check.length, executeQuery.size());
        for (int i = 0; i < check.length; i++) {
            assertEquals(i + "-ti: ", check[i][0], executeQuery.get(i).get("time"));
            assertEquals(i + "-ty: ", check[i][1], executeQuery.get(i).get("type"));
            assertEquals(i + "-ma: ", check[i][2], executeQuery.get(i).get("material"));
            assertEquals(i + "-va: ", check[i][3], executeQuery.get(i).get("value"));
            assertEquals(i + "-am: ", check[i][4], executeQuery.get(i).get("amount"));
            assertEquals(i + "-to: ", check[i][5], executeQuery.get(i).get("total"));
        }
    }

    @Test
    public void testExecute_dateGetsInsertedIfMissing() {
        final LocalDate t = getCurrentDate();
        this.vc.execute(t, HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                HistoryUpdaterTest.c.executeQuery("SELECT * FROM system");
        Assert.assertTrue(executeQuery != null && executeQuery.size() == 1);
        Assert.assertEquals("lastHistoryUpdate", executeQuery.get(0).get("k"));
        Assert.assertEquals(t.toString(), executeQuery.get(0).get("v"));
    }

    @Test
    public void testExecute_dateGetsUpdated() {
        LocalDate t = getOffsetDate(-1);
        final Insert i = Operations.insertInto("system").columns("k", "v")
                .values("lastHistoryUpdate", t.toString()).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), i);
        dbSetup.launch();

        t = getCurrentDate();
        this.vc.execute(t, HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                HistoryUpdaterTest.c.executeQuery("SELECT * FROM system");
        Assert.assertTrue(executeQuery != null && executeQuery.size() == 1);
        Assert.assertEquals("lastHistoryUpdate", executeQuery.get(0).get("k"));
        Assert.assertEquals(t.toString(), executeQuery.get(0).get("v"));
    }

    @Test
    @Ignore("catching up is done by UpdateManager")
    public void testExecute_missingUpdatesAreCatchedUp() {
        final Date t = getOffsetDate2(-2);
        final Operation op = Operations.sequenceOf(
                Operations.insertInto("system").columns("k", "v")
                        .values("lastHistoryUpdate", t.toString()).build(),
                Operations.insertInto("history")
                        .columns("time", "type", "material", "value", "amount")
                        .values(t, true, 1, 50, 10).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), op);
        dbSetup.launch();
        final LocalDate t3 = getOffsetDate(-1);
        final LocalDate t2 = getCurrentDate();
        this.vc.execute(t2, HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                HistoryUpdaterTest.c.executeQuery("SELECT * FROM history ORDER BY time");
        Assert.assertTrue(executeQuery != null);
        assertEquals(5, executeQuery.size());

        Assert.assertEquals(t, executeQuery.get(0).get("time"));
        Assert.assertEquals(t3, executeQuery.get(1).get("time"));
        Assert.assertEquals(t3, executeQuery.get(2).get("time"));
        Assert.assertEquals(t2, executeQuery.get(3).get("time"));
        Assert.assertEquals(t2, executeQuery.get(4).get("time"));
    }

    @Test
    public void testExecute_currentUpdateIsExecuted() {
        final Date t = getOffsetDate2(-1);
        final Operation op = Operations.sequenceOf(
                Operations.insertInto("system").columns("k", "v")
                        .values("lastHistoryUpdate", t.toString()).build(),
                Operations.insertInto("history")
                        .columns("time", "type", "material", "value", "amount")
                        .values(t, true, 1, 50, 10).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), op);
        dbSetup.launch();

        final Date t2 = getCurrentDate2();
        this.vc.execute(getCurrentDate(), HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                HistoryUpdaterTest.c.executeQuery("SELECT * FROM history ORDER BY time");
        Assert.assertTrue(executeQuery != null && executeQuery.size() == 3);

        Assert.assertEquals(t, executeQuery.get(0).get("time"));
        Assert.assertEquals(t2, executeQuery.get(1).get("time"));
        Assert.assertEquals(t2, executeQuery.get(2).get("time"));
    }

    @Test
    @Ignore("ignoring is done by updateManager")
    public void testExecute_noDuplicates() {
        final LocalDate t2 = getOffsetDate(-1);
        final Date datet2 = Date.valueOf(t2);
        final LocalDate t = getCurrentDate();
        final Date datet = Date.valueOf(t);
        final Operation op = Operations.sequenceOf(
                Operations.insertInto("system").columns("k", "v").values("lastHistoryUpdate", datet)
                        .build(),
                Operations.insertInto("history")
                        .columns("time", "type", "material", "value", "amount")
                        .values(datet2, true, 1, 50, 10).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), op);
        dbSetup.launch();

        this.vc.execute(t, HistoryUpdaterTest.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                HistoryUpdaterTest.c.executeQuery("SELECT * FROM history ORDER BY time");
        Assert.assertTrue(executeQuery != null);
        assertEquals(1, executeQuery.size());

        Assert.assertEquals(datet2, executeQuery.get(0).get("time"));
    }

    private static final String[][] querys = {
            { "SELECT * FROM history ORDER BY time,type,material", "time", "type", "material",
                    "value", "amount" },
            { "SELECT * FROM minmax ORDER BY time,type,material", "time", "type", "material", "min",
                    "max" }, };

    @Test
    @Parameters
    @TestCaseName("testExecute_{0}")
    public void testExecute(final String name, final Operation op, final Object[][][] check)
            throws Exception {
        if (op != null) {
            final DbSetup dbSetup =
                    new DbSetup(new DataSourceDestination(HistoryUpdaterTest.ds), op);
            dbSetup.launch();
        }
        final LocalDate t = getCurrentDate();
        final Date date = Date.valueOf(t);
        this.vc.execute(t, HistoryUpdaterTest.c);

        if (check.length != HistoryUpdaterTest.querys.length) {
            Assert.fail("check-array for test has wrong length");
        }
        for (int i = 0; i < HistoryUpdaterTest.querys.length; i++) {
            final ArrayList<HashMap<String, Object>> al =
                    HistoryUpdaterTest.c.executeQuery(HistoryUpdaterTest.querys[i][0]);
            if (check[i].length == 0) {
                Assert.assertTrue(
                        HistoryUpdaterTest.querys[i][0]
                                + " returned something, but expected empty result.",
                        al == null || al.size() == 0);
            } else {
                Assert.assertNotNull(HistoryUpdaterTest.querys[i][0]
                        + " returned nothing when it should have returned something", al);
                Assert.assertEquals(
                        "result of " + HistoryUpdaterTest.querys[i][0] + "has the wrong length.",
                        check[i].length, al.size());
                for (int j = 0; j < check[i].length; j++) {
                    final HashMap<String, Object> hashMap = al.get(j);
                    for (int k = 0; k < HistoryUpdaterTest.querys[i].length - 1; k++) {
                        if ("time".equals(check[i][j][k])) {
                            Assert.assertEquals(date,
                                    hashMap.get(HistoryUpdaterTest.querys[i][k + 1]));
                        } else {
                            if ((check[i][j][k] instanceof Double) && (hashMap
                                    .get(HistoryUpdaterTest.querys[i][k + 1]) instanceof Double)) {
                                Assert.assertEquals(
                                        HistoryUpdaterTest.querys[i][0]
                                                + " returned wrong value for column <"
                                                + HistoryUpdaterTest.querys[i][k + 1]
                                                + "> in Line <" + j + ">.",
                                        (double) check[i][j][k],
                                        (double) hashMap.get(HistoryUpdaterTest.querys[i][k + 1]),
                                        0.000001);
                            } else {
                                Assert.assertEquals(
                                        HistoryUpdaterTest.querys[i][0]
                                                + " returned wrong value for column <"
                                                + HistoryUpdaterTest.querys[i][k + 1]
                                                + "> in Line <" + j + ">.",
                                        check[i][j][k],
                                        hashMap.get(HistoryUpdaterTest.querys[i][k + 1]));
                            }
                        }
                    }
                }
            }
        }
    }

    private LocalDate getCurrentDate() {
        return getOffsetDate(0);
    }

    private Date getCurrentDate2() {
        return getOffsetDate2(0);
    }

    private LocalDate getOffsetDate(final int i) {
        return LocalDate.now().plusDays(i);
    }

    private Date getOffsetDate2(final int i) {
        return Date.valueOf(getOffsetDate(i));
    }

    public Object[] parametersForTestExecute() {
        return new Object[][] { { "emptyMakesNothing", null,
                new Object[][][] { { { "time", true, "-1", 1000000000L, 0L } }, // history
                        {},// minmax
                } }, // emptyMakesNothing
                { "oldValueIsKeptIfRestIsEmpty",
                        Operations.insertInto("history")
                                .columns("time", "type", "material", "value", "amount")
                                .values("2015-01-01", false, 1, 50, 10)
                                .values("2015-01-01", true, 1, 10, 10)
                                .values("2015-01-01", true, 2, 30, 1).build(),
                        new Object[][][] { { { Date.valueOf("2015-01-01"), false, "1", 50L, 10L },
                                { Date.valueOf("2015-01-01"), true, "1", 10L, 10L },
                                { Date.valueOf("2015-01-01"), true, "2", 30L, 1L },
                                { "time", false, "1", 50L, 0L },
                                { "time", true, "-1", 1000000000L, 0L },
                                { "time", true, "1", 10L, 0L }, { "time", true, "2", 30L, 0L } }, // history
                                {},// minmax
                        } }, // oldValueIsKeptIfRestIsEmpty
                { "tradesdoneGetsCopiedIfRestIsEmpty",
                        Operations.insertInto("tradesdone")
                                .columns("time", "type", "material", "price", "amount")
                                .values("2015-01-01", false, 1, 50, 10)
                                .values("2015-01-01", true, 1, 10, 10)
                                .values("2015-01-01", true, 2, 30, 1).build(),
                        new Object[][][] { { { "time", false, "1", 50L, 10L },
                                { "time", true, "-1", 1000000000L, 11L },
                                { "time", true, "1", 10L, 10L }, { "time", true, "2", 30L, 1L } }, // history
                                { { "time", false, "1", 50L, 50L }, { "time", true, "1", 10L, 10L },
                                        { "time", true, "2", 30L, 30L } },// minmax
                        } }, // tradesdoneGetsCopiedIfRestIsEmpty
                { "valuesFromOpenTradesGetCopiedIfRestIsEmpty",
                        Operations.insertInto("trade")
                                .columns("traderid", "buysell", "type", "material", "price",
                                        "amount", "sold")
                                .values(1, true, false, "1", 50, 10, 0)
                                .values(1, true, true, "1", 10, 10, 0)
                                .values(1, true, true, "2", 30, 10, 0).build(),
                        new Object[][][] { { { "time", false, "1", 50L, 0L },
                                { "time", true, "-1", 1000000000L, 0L },
                                { "time", true, "1", 10L, 0L }, { "time", true, "2", 30L, 0L } }, // history
                                {},// minmax
                        } }, // valuesFromOpenTradesGetCopiedIfRestIsEmpty

                { "error1",
                        Operations
                                .sequenceOf(
                                        Operations.insertInto("history")
                                                .columns("time", "type", "material", "value",
                                                        "amount")
                                                .values(Date.valueOf("2016-01-01"), 1, 4, 1019223,
                                                        5)
                                                .values(Date.valueOf("2016-01-02"), 1, 4, 1657871,
                                                        0)
                                                .build(),
                                        Operations.insertInto("trade")
                                                .columns("traderid", "buysell", "type", "material",
                                                        "amount", "price", "sold")
                                                .values(1, 1, 1, 4, 10, 1000000, 5)
                                                .values(1, 1, 1, 4, 10, 1990000, 0).build()),
                        new Object[][][] {
                                { { Date.valueOf("2016-01-01"), true, "4", 1019223L, 5L },
                                        { Date.valueOf("2016-01-02"), true, "4", 1657871L, 0L },
                                        { "time", true, "-1", 1000000000L, 0L },
                                        { "time", true, "4", 1659999L, 0L } }, // history
                                {},// minmax
                        } }, // error1
        };

    }

    private Object[][] calculateExpectedHistory() {
        final Object[][] ret = new Object[32][];
        for (int i = 0; i < 30; i++) {
            ret[29 - i] = new Object[] { getOffsetDate(-(i + 1)), true, "1", i * 100L, 1L };
        }
        ret[30] = new Object[] { "time", true, "-1", 1000000000L, 0L };
        ret[31] = new Object[] { getOffsetDate(0), true, "1", 0L, 0L };
        return ret;
    }
}
