package de._125m125.ktv2.updates;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;

import de._125m125.ktv2.DatabaseTester;

public class UserUpdaterTest extends DatabaseTester {

    @Test
    public void testExecute() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(
                        Operations.insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("2", true, "-1", "1001").values("1", true, "2", "1000").build(),
                        Operations.insertInto("reftaxes").columns("id", "amount").values("2", 20).values("1", 90)
                                .values("3", 10000).build()));

        dbSetup.launch();

        final UserUpdater updater = new UserUpdater();
        updater.execute(null, DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM materials ORDER BY id, matid");
        Assert.assertEquals(1L, result.get(0).get("id"));
        Assert.assertEquals("-1", result.get(0).get("matid"));
        Assert.assertEquals(90L, result.get(0).get("amount"));

        Assert.assertEquals(1L, result.get(1).get("id"));
        Assert.assertEquals("2", result.get(1).get("matid"));
        Assert.assertEquals(1000L, result.get(1).get("amount"));

        Assert.assertEquals(2L, result.get(2).get("id"));
        Assert.assertEquals("-1", result.get(2).get("matid"));
        Assert.assertEquals(1021L, result.get(2).get("amount"));

        Assert.assertEquals(3L, result.get(3).get("id"));
        Assert.assertEquals("-1", result.get(3).get("matid"));
        Assert.assertEquals(10000L, result.get(3).get("amount"));

        Assert.assertEquals(4, result.size());

        final ArrayList<HashMap<String, Object>> reftaxes = DatabaseTester.c.executeQuery("SELECT * FROM reftaxes");
        Assert.assertTrue("reftaxes was not cleared", reftaxes == null || reftaxes.size() == 0);
    }

    @Test
    public void testExecute_empty() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.insertInto("materials").columns("id", "type", "matid", "amount")
                        .values("2", true, "-1", "1001").values("1", true, "2", "1000").build());

        dbSetup.launch();

        final UserUpdater updater = new UserUpdater();
        updater.execute(null, DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM materials ORDER BY id, matid");

        Assert.assertEquals(1L, result.get(0).get("id"));
        Assert.assertEquals("2", result.get(0).get("matid"));
        Assert.assertEquals(1000L, result.get(0).get("amount"));

        Assert.assertEquals(2L, result.get(1).get("id"));
        Assert.assertEquals("-1", result.get(1).get("matid"));
        Assert.assertEquals(1001L, result.get(1).get("amount"));

        Assert.assertEquals(2, result.size());
    }

}
