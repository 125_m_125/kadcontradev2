package de._125m125.ktv2.updates;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.UnavailableException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.LotteryItemConfig;
import de._125m125.ktv2.beans.TradeSlot;
import de._125m125.ktv2.cache.TradeSlotBuilder;
import de._125m125.ktv2.market.Trade;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.market.TradeManager;
import de._125m125.ktv2.servlets.apiv2.HistoryRestAPI;

public class LotteryTraderTest extends DatabaseTester {
    private LotteryTrader uut;

    @AfterClass
    public static void afterClassLotteryTraderTest() {
        Whitebox.setInternalState(Variables.class, "lotteryItemConfig",
                (Map<String, LotteryItemConfig>) null);
    }

    @BeforeClass
    public static void beforeClassLotteryTraderTest() throws UnavailableException {
        final LotteryItemConfig lotteryItemConfig = new LotteryItemConfig(2, "-3",
                LocalDate.now().minusDays(5), LocalDate.now().plusDays(5), 10,
                LocalDate.now().minusDays(10), LocalDate.now().plusDays(10), 5);
        final HashMap<String, LotteryItemConfig> lic = new HashMap<>();
        lic.put("-2", lotteryItemConfig);
        Whitebox.setInternalState(Variables.class, "lotteryItemConfig", lic);
        Whitebox.setInternalState(Main.instance(), new TradeManager());
    }

    @Before
    public void beforeLotteryTraderTest() {
        this.uut = new LotteryTrader();
        HistoryRestAPI.invalidateAll();
    }

    @Test
    public void testCreatesTradeWithoutUserOrHistory() {
        this.uut.execute(LocalDate.now(), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM trade");
        assertEquals(1, executeQuery.size());

        final TradeSlot slot = new TradeSlotBuilder().build(executeQuery.get(0));
        assertEquals("-3", slot.getMaterialId());
        assertEquals(10, slot.getMax());
    }

    @Test
    public void testCreatesTradeWithHistory() {
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount")
                .values("2015-01-01", true, -3, 50, 10).values("2015-01-02", true, -3, 30, 20)
                .build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        this.uut.execute(LocalDate.now(), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT id,traderid FROM trade");
        assertEquals(1, executeQuery.size());

        final Trade trade = Trade.fullyLoad((long) executeQuery.get(0).get("id"),
                (long) executeQuery.get(0).get("traderid"), ITEM_OR_STOCK.ITEM);
        assertEquals(2, trade.getOwner());
        assertEquals("-3", trade.getMaterial());
        assertEquals(10, trade.getAmount());
        assertEquals(30, trade.getPrice());
    }

    @Test
    public void testDoesNotCreateTradeWhenTooLate() {
        this.uut.execute(LocalDate.now().plusDays(7), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT id,traderid FROM trade");
        assertNull(executeQuery);
    }

    @Test
    public void testDoesNotCreateTradeWhenTooEarly() {
        this.uut.execute(LocalDate.now().minusDays(7), DatabaseTester.c);

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT id,traderid FROM trade");
        assertNull(executeQuery);
    }
}
