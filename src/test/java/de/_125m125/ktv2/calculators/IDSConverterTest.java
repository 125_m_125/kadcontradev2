package de._125m125.ktv2.calculators;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class IDSConverterTest {
    @Test
    public void testDoubleToInt() {
        Assert.assertEquals(1, IDSConverter.doubleToLong(0.000001));
        Assert.assertEquals(13, IDSConverter.doubleToLong(0.000013));
        Assert.assertEquals(297, IDSConverter.doubleToLong(0.000297));
        Assert.assertEquals(1234, IDSConverter.doubleToLong(0.001234));
        Assert.assertEquals(56789, IDSConverter.doubleToLong(0.056789));
        Assert.assertEquals(876543, IDSConverter.doubleToLong(0.876543));
        Assert.assertEquals(1800999, IDSConverter.doubleToLong(1.800999));
    }

    @Test
    public void testIntToDouble() {
        Assert.assertEquals(0.000001, IDSConverter.longToDouble(1), 0);
        Assert.assertEquals(0.000013, IDSConverter.longToDouble(13), 0);
        Assert.assertEquals(0.000297, IDSConverter.longToDouble(297), 0);
        Assert.assertEquals(0.001234, IDSConverter.longToDouble(1234), 0);
        Assert.assertEquals(0.056789, IDSConverter.longToDouble(56789), 0);
        Assert.assertEquals(0.876543, IDSConverter.longToDouble(876543), 0);
        Assert.assertEquals(1.800999, IDSConverter.longToDouble(1800999), 0);
    }

    @Test
    public void testStringToInt() {
        Assert.assertEquals(12000000, IDSConverter.stringToLong("12", 2));
        Assert.assertEquals(171, IDSConverter.stringToLong("0,000171"));
        Assert.assertEquals(1200, IDSConverter.stringToLong("0,0012", 4));
        Assert.assertEquals(12777777, IDSConverter.stringToLong("12.777777"));
        Assert.assertEquals(12770000, IDSConverter.stringToLong("12.77000000000"));
        assertEquals(2010000, IDSConverter.stringToLong("2.01"));
    }

    @Test
    public void testLongToDoubleString() {
        Assert.assertEquals("12.000000", IDSConverter.longToDoubleString(12000000));
        Assert.assertEquals("0.000171", IDSConverter.longToDoubleString(171));
        Assert.assertEquals("0.001200", IDSConverter.longToDoubleString(1200));
        Assert.assertEquals("12.777777", IDSConverter.longToDoubleString(12777777));
        Assert.assertEquals("-20.123456", IDSConverter.longToDoubleString(-20123456));
        Assert.assertEquals("-0.000001", IDSConverter.longToDoubleString(-1));
        Assert.assertEquals("1.000000", IDSConverter.longToDoubleString(1000000));
    }

    @Test
    public void testLongToMachineDoubleString() throws Exception {
        Assert.assertEquals("12.0000", IDSConverter.longToMachineDoubleString(12000000));
        Assert.assertEquals("0.0171", IDSConverter.longToMachineDoubleString(17100));
        Assert.assertEquals("0.1200", IDSConverter.longToMachineDoubleString(120000));
        Assert.assertEquals("12.7777", IDSConverter.longToMachineDoubleString(12777777));
        Assert.assertEquals("-20.1234", IDSConverter.longToMachineDoubleString(-20123456));
        Assert.assertEquals("0", IDSConverter.longToMachineDoubleString(-1));
        Assert.assertEquals("1.0000", IDSConverter.longToMachineDoubleString(1000000));
    }

    @Test(expected = NumberFormatException.class)
    public void testStringToInt_numberFormat() {
        IDSConverter.stringToLong("hello");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStringToInt_illegalArgument() {
        IDSConverter.stringToLong("0.0001", 3);
    }

}
