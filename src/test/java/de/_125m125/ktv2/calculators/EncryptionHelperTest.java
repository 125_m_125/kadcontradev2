package de._125m125.ktv2.calculators;

import static de._125m125.ktv2.calculators.EncryptionHelper.PasswordResult.STATE.FAILURE;
import static de._125m125.ktv2.calculators.EncryptionHelper.PasswordResult.STATE.SUCCESS;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.EncryptionHelper.EncryptedData;
import de._125m125.ktv2.calculators.EncryptionHelper.PasswordCheckResult;
import de._125m125.ktv2.calculators.EncryptionHelper.PasswordResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EncryptionHelper.class })
@PowerMockIgnore({ "javax.crypto.*" })
public class EncryptionHelperTest {

    private SecureRandom secureRandom;

    @BeforeClass
    public static void beforeClassEncryptionHelperTest()
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        final String keyStr = "secret";
        byte[] key = (keyStr).getBytes("UTF-8");
        final MessageDigest sha = MessageDigest.getInstance("SHA-256");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        final SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        Whitebox.setInternalState(Variables.class, SecretKey.class, secretKeySpec);
    }

    @Before
    public void beforeEncryptionHelperTest() {
        this.secureRandom = mock(SecureRandom.class);
        Whitebox.setInternalState(EncryptionHelper.class, SecureRandom.class, this.secureRandom);
    }

    @AfterClass
    public static void afterClassEncryptionHelperTest() {
        Whitebox.setInternalState(EncryptionHelper.class, SecureRandom.class, new SecureRandom());
    }

    @Test
    public void testGenAndCompare() {
        final PasswordResult result = EncryptionHelper.create(",.-klöIOP890", "125m125");

        assertEquals(SUCCESS, result.getState());
        assertNull(result.getMessage());
        System.out.println(result.getHashedPassword());
        final PasswordCheckResult checkPw = EncryptionHelper.checkPw(",.-klöIOP890",
                result.getHashedPassword());
        assertTrue(checkPw.isValid());
        assertFalse(checkPw.isOutdated());
        assertFalse(checkPw.getRehashedPassword().isPresent());
    }

    @Test
    public void testGenAndCompare_wrong() {
        final PasswordResult result = EncryptionHelper.create(",.-klöIOP890", "125m125");

        assertEquals(SUCCESS, result.getState());
        assertNull(result.getMessage());
        assertFalse(EncryptionHelper.checkPw("wrong", result.getHashedPassword()).isValid());
    }

    @Test
    public void testCreate_tooShort() {
        final PasswordResult result = EncryptionHelper.create(",kI8", "125m125");

        assertEquals(FAILURE, result.getState());
        assertEquals("passwordTooShort", result.getMessage());
        assertNull(result.getHashedPassword());
    }

    @Test
    public void testCreate_nameInPW() {
        final PasswordResult result = EncryptionHelper.create("a125m125A", "125m125");

        assertEquals(FAILURE, result.getState());
        assertEquals("nameInPassword", result.getMessage());
        assertNull(result.getHashedPassword());
    }

    @Test
    public void testCreate_weakPW() {
        final PasswordResult result = EncryptionHelper.create("abcdefghijk", "125m125");

        assertEquals(FAILURE, result.getState());
        assertEquals("insecure", result.getMessage());
        assertNull(result.getHashedPassword());
    }

    @Test
    public void testSupportsOldPasswordsAndSuggestsUpdate() {
        final PasswordCheckResult result = EncryptionHelper.checkPw(",.-klöIOP890",
                "$2a$04$Zs7uLSeMch42LEucXYHmAeNQ1VesFmSdwZbcOdhXFsCB8cGa4OSve");
        assertTrue(result.isValid());
        assertTrue(result.isOutdated());
        assertTrue(result.getRehashedPassword().isPresent());
    }

    byte[] encrypted = { -63, 80, -30, -103, 47, -118, -75, 67, -73, -111, -19, -103, -6, -84, -93,
            -28 };
    byte[] iv        = { (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA,
            (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA,
            (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA };

    @Test
    public void testAesEncrypt() throws Exception {
        doAnswer(invocation -> {
            System.arraycopy(this.iv, 0, invocation.getArgumentAt(0, byte[].class), 0, 16);
            return null;
        }).when(this.secureRandom).nextBytes(any());

        final EncryptedData encryptedData = EncryptionHelper.aesEncrypt("Test");
        assertArrayEquals(this.iv, encryptedData.getIv());
        assertArrayEquals(this.encrypted, encryptedData.getCyphertext());
    }

    @Test
    public void testAesDecrypt() throws Exception {
        final String aesDecrypt = EncryptionHelper
                .aesDecrypt(new EncryptedData(this.encrypted, this.iv));

        assertEquals("Test", aesDecrypt);
    }

}
