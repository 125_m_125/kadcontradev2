package de._125m125.ktv2;

import static com.ninja_squad.dbsetup.Operations.deleteAllFrom;

import javax.servlet.UnavailableException;
import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.logging.Log;

public class DatabaseTester {

    protected static Connector       c;

    protected static final Operation DELETE_ALL = deleteAllFrom(Connector.TABLES);
    protected static DataSource      ds;

    static {
        Variables.dbname = "kttest";
        Variables.dbpass = "kttupass";
        Variables.dbuser = "kttu";
        Variables.setPoolSize(1);

        final Connector c = new Connector();

        final DataSource ds = c.getDataSource();

        c.executeUpdate("DROP DATABASE kttest");
        c.executeUpdate("CREATE DATABASE kttest");
        c.execute("use kttest");
        c.checktables();

        c.close();
    }

    @BeforeClass
    public static void setUpDatabase() throws UnavailableException {
        Variables.dbname = "kttest";
        Variables.dbpass = "kttupass";
        Variables.dbuser = "kttu";
        Variables.setPoolSize(1);

        DatabaseTester.c = new Connector();
        DatabaseTester.ds = DatabaseTester.c.getDataSource();

        new Main(false);
        Log.disable();
        Main.instance().setConnector(DatabaseTester.c);
    }

    @AfterClass
    public static void closeDatabase() {
        Log.enable();
        DatabaseTester.c.close();
    }

    @Before
    public void prepareDB() {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), DatabaseTester.DELETE_ALL);

        dbSetup.launch();
    }

}
