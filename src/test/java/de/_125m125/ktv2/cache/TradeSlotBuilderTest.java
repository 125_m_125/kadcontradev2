package de._125m125.ktv2.cache;

import org.junit.Assert;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.TradeSlot;
import de._125m125.ktv2.beans.TradeSlotList;

public class TradeSlotBuilderTest extends DatabaseTester {
    @Test
    public void testLoad_empty() throws Exception {
        final TradeSlotBuilder sb = new TradeSlotBuilder();

        final TradeSlotList result = sb.load("1");

        Assert.assertEquals(new TradeSlotList().getTsvString(true), result.getTsvString(true));
    }

    @Test
    public void testLoad() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "traderid", "type", "buysell", "material", "amount", "price", "sold")
                .values(1, 1, true, true, 1, 2, 300000, 1).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final TradeSlotList expected = new TradeSlotList();
        final TradeSlot e = new TradeSlot();
        e.setTradeid(1);
        e.setActive(true);
        e.setBuy(true);
        e.setCurrent(1);
        e.setItem(new Item("1"));
        e.setMax(2);
        e.setPrice("0.3000");
        expected.addEntry(e);

        final TradeSlotBuilder sb = new TradeSlotBuilder();

        final TradeSlotList result = sb.load("1");

        Assert.assertEquals(expected.getTsvString(true), result.getTsvString(true));
    }
}
