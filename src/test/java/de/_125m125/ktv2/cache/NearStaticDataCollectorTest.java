package de._125m125.ktv2.cache;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;

public class NearStaticDataCollectorTest {
    private static HttpServletRequest request = mock(HttpServletRequest.class);
    private static HttpSession        s       = mock(HttpSession.class);

    @Before
    public void before() {

        reset(NearStaticDataCollectorTest.s);
        when(NearStaticDataCollectorTest.s.getAttribute("id")).thenReturn(1);

        reset(NearStaticDataCollectorTest.request);
        when(NearStaticDataCollectorTest.request.getSession()).thenReturn(NearStaticDataCollectorTest.s);
    }

}
