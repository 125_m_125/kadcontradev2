package de._125m125.ktv2.cache.stats;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.OrderBook;
import de._125m125.ktv2.servlets.api.keys.OrderBookKey;

public class OrderBookBuilderTest extends DatabaseTester {

    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.clear();
        Variables.ID_TO_DN.put("1", "Stone(1)");
    }

    @Test
    public void testLoad_empty() throws Exception {
        final OrderBookBuilder uut = new OrderBookBuilder();

        final OrderBook result = uut.load(new OrderBookKey("1", 9, true));

        assertEquals("type\tprice\tamount\r\n", result.getTsvString(true));
    }

    @Test
    public void testLoad() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final OrderBookBuilder uut = new OrderBookBuilder();

        final OrderBook result = uut.load(new OrderBookKey("1", 9, true));
        assertEquals(
                "type\tprice\tamount\r\nbuy\t0.000019\t10\r\nbuy\t0.000020\t20\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad_limitSummarize() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).values(8, 1, 1, 0, 5, 23, 0).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final OrderBookBuilder uut = new OrderBookBuilder();

        final OrderBook result = uut.load(new OrderBookKey("1", 1, true));

        assertEquals(
                "type\tprice\tamount\r\nbuy\t<0.000021\t30\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\nsell\t>0.000022\t5\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad_limit() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final OrderBookBuilder uut = new OrderBookBuilder();

        final OrderBook result = uut.load(new OrderBookKey("1", 1, false));

        assertEquals("type\tprice\tamount\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                result.getTsvString(true));
    }
}
