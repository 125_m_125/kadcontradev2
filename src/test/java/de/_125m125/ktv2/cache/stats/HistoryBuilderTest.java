package de._125m125.ktv2.cache.stats;

import java.sql.Date;
import java.time.LocalDate;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.History;
import de._125m125.ktv2.servlets.api.keys.HistoryKey;

public class HistoryBuilderTest extends DatabaseTester {

    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.clear();
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    @Test
    public void testLoad_empty() throws Exception {
        final HistoryBuilder uut = new HistoryBuilder();

        final History result = uut.load(new HistoryKey("1", 30, 0));

        Assert.assertEquals("date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad() throws Exception {
        final HistoryBuilder uut = new HistoryBuilder();
        final LocalDate ld = LocalDate.now();
        Date d = Date.valueOf(ld);
        Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 1).values(d2, 1, 4, 11, 12, 2).values(d, 1, 3, 12, 13, 3)
                .values(d, 0, 4, 14, 15, 4).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final History result = uut.load(new HistoryKey("4", 30, 0));

        d = Date.valueOf(ld.minusDays(1));
        d2 = Date.valueOf(ld.minusDays(2));
        Assert.assertEquals(
                "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n" + d.toString()
                        + "\t0.000011\t0.000110\t0.000100\t0.000010\t13\t0.000001\r\n"
                        + d2.toString() + "\t\t\t\t0.000011\t12\t0.000002\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad_limit() throws Exception {
        final HistoryBuilder uut = new HistoryBuilder();
        final LocalDate ld = LocalDate.now();
        Date d = Date.valueOf(ld);
        Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 1).values(d2, 1, 4, 11, 12, 2).values(d, 1, 3, 12, 13, 3)
                .values(d, 0, 4, 14, 15, 4).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final History result = uut.load(new HistoryKey("4", 1, 0));

        d = Date.valueOf(ld.minusDays(1));
        d2 = Date.valueOf(ld.minusDays(2));
        Assert.assertEquals(
                "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n" + d.toString()
                        + "\t0.000011\t0.000110\t0.000100\t0.000010\t13\t0.000001\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad_offset() throws Exception {
        final HistoryBuilder uut = new HistoryBuilder();
        final LocalDate ld = LocalDate.now();
        Date d = Date.valueOf(ld);
        Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 1).values(d2, 1, 4, 11, 12, 2).values(d, 1, 3, 12, 13, 3)
                .values(d, 0, 4, 14, 15, 4).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final History result = uut.load(new HistoryKey("4", 30, 1));

        d = Date.valueOf(ld.minusDays(1));
        d2 = Date.valueOf(ld.minusDays(2));
        Assert.assertEquals("date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n"
                + d2.toString() + "\t\t\t\t0.000011\t12\t0.000002\r\n", result.getTsvString(true));
    }

    @Test
    public void testLoad_unknown() throws Exception {
        final HistoryBuilder uut = new HistoryBuilder();
        final LocalDate ld = LocalDate.now();
        final Date d = Date.valueOf(ld);
        final Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount").values(d, 1, 2, 10, 13)
                .values(d2, 1, 2, 11, 12).values(d, 1, 3, 12, 13).values(d, 0, 2, 14, 15).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 2, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 2, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final History result = uut.load(new HistoryKey("2", 30, 0));

        Assert.assertEquals("date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n",
                result.getTsvString(true));
    }
}
