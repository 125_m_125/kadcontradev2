package de._125m125.ktv2.cache.payout;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.PayoutList;
import de._125m125.ktv2.servlets.api.keys.PayoutKey;

public class PayoutBuilderTest extends DatabaseTester {

    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.clear();
        Variables.ID_TO_DN.put("1", "Stone(1)");
    }

    @Test
    public void testLoad_empty() throws Exception {
        final PayoutBuilder uut = new PayoutBuilder();

        final PayoutList result = uut.load(new PayoutKey(1, false, 30, 0));

        assertEquals("id\tmaterial\tmaterialName\tamount\tstate\tpayoutType\tdate\tmessage\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "payoutType", "date",
                        "message")
                .values(1, 1, 1, 1, 1, true, false, false, true, false, "kadcon", "2016-01-01 01:23:45", "test")
                .values(2, 2, 1, 1, 1, true, false, false, true, false, "vkba", "2016-01-01 01:23:45", "test")
                .values(3, 1, 1, 2, 10, false, true, true, false, false, "vkba", "2016-02-01 01:24:45", "test").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final PayoutBuilder uut = new PayoutBuilder();

        final PayoutList result = uut.load(new PayoutKey(1, false, 30, 0));

        final long t1 = Timestamp.valueOf("2016-01-01 01:23:45").getTime();
        final long t2 = Timestamp.valueOf("2016-02-01 01:24:45").getTime();

        assertEquals(
                "id\tmaterial\tmaterialName\tamount\tstate\tpayoutType\tdate\tmessage\r\n1\t1\tStone(1)\t1\tOffen\tkadcon\t"
                        + t1 + "\ttest\r\n3\t2\tnull\t10\tErfolgreich\tvkba\t" + t2 + "\ttest\r\n",
                result.getTsvString(true));
    }

}
