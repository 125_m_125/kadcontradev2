package de._125m125.ktv2.cache.payout;

import org.junit.Assert;
import org.junit.Test;

import de._125m125.ktv2.DatabaseTester;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;

import de._125m125.ktv2.cache.payout.PayoutSettings;
import de._125m125.ktv2.cache.payout.PayoutSettingsCache;

public class PayoutSettingsCacheTest extends DatabaseTester {

    @Test
    public void testGet_empty() throws Exception {
        final PayoutSettingsCache uut = new PayoutSettingsCache();
        Assert.assertEquals(new PayoutSettings(1L), uut.get(1L));
    }

    @Test
    public void testGet() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), Insert.into("payoutsettings")
                .columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6).build());
        dbSetup.launch();

        final PayoutSettingsCache uut = new PayoutSettingsCache();
        Assert.assertEquals(new PayoutSettings(1, 2, 3, 4, 5, "6"), uut.get(1L));
    }

    @Test
    public void testInvalidate() throws Exception {

        final PayoutSettingsCache uut = new PayoutSettingsCache();
        uut.get(1L);

        uut.invalidate(1L);

        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), Insert.into("payoutsettings")
                .columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6).build());
        dbSetup.launch();

        Assert.assertEquals(new PayoutSettings(1, 2, 3, 4, 5, "6"), uut.get(1L));
    }

}
