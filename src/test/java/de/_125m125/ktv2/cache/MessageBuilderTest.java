package de._125m125.ktv2.cache;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.beans.MessageList;
import de._125m125.ktv2.servlets.api.keys.MessageKey;

public class MessageBuilderTest extends DatabaseTester {
    @Test
    public void testLoad() throws Exception {
        final Operation op = insertInto("messages").columns("uid", "message", "time")
                .values(1, "Test", "2016-01-01 00:00:00").values(2, "TestFailed", "2016-02-02 00:00:00")
                .values(1, "Test2,?!", "2016-03-03 00:00:00").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final MessageBuilder uut = new MessageBuilder();

        final MessageList result = uut.load(new MessageKey(1, 10, 0));

        assertEquals(
                "timestamp\tmessage\r\n" + Timestamp.valueOf("2016-03-03 00:00:00.0").getTime() + "\tTest2,?!\r\n"
                        + Timestamp.valueOf("2016-01-01 00:00:00.0").getTime() + "\tTest\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad_empty() throws Exception {
        final MessageBuilder uut = new MessageBuilder();

        final MessageList result = uut.load(new MessageKey(1, 10, 0));

        assertEquals("timestamp\tmessage\r\n", result.getTsvString(true));
    }

    @Test
    public void testLoad_offset() throws Exception {
        final Operation op = insertInto("messages").columns("uid", "message", "time")
                .values(1, "Test", "2016-01-01 00:00:00").values(2, "TestFailed", "2016-02-02 00:00:00")
                .values(1, "Test2,?!", "2016-03-03 00:00:00").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final MessageBuilder uut = new MessageBuilder();

        final MessageList result = uut.load(new MessageKey(1, 10, 1));

        assertEquals("timestamp\tmessage\r\n" + Timestamp.valueOf("2016-01-01 00:00:00.0").getTime() + "\tTest\r\n",
                result.getTsvString(true));
    }

    @Test
    public void testLoad_limit() throws Exception {
        final Operation op = insertInto("messages").columns("uid", "message", "time")
                .values(1, "Test", "2016-01-01 00:00:00").values(2, "TestFailed", "2016-02-02 00:00:00")
                .values(1, "Test2,?!", "2016-03-03 00:00:00").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final MessageBuilder uut = new MessageBuilder();

        final MessageList result = uut.load(new MessageKey(1, 1, 0));

        assertEquals("timestamp\tmessage\r\n" + Timestamp.valueOf("2016-03-03 00:00:00.0").getTime() + "\tTest2,?!\r\n",
                result.getTsvString(true));
    }
}
