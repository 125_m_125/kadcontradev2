package de._125m125.ktv2.cache;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.ItemList;

public class ItemBuilderTest extends DatabaseTester {

    private static Item           i1 = new Item(), i2 = new Item(), i3 = new Item();
    private static final ItemList il = new ItemList();

    static {
        ItemBuilderTest.il.addEntry(ItemBuilderTest.i1);
        ItemBuilderTest.il.addEntry(ItemBuilderTest.i2);
        ItemBuilderTest.il.addEntry(ItemBuilderTest.i3);
    }

    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.put("-1", "Kadis(-1)");
        Variables.ID_TO_DN.put("1", "Stone(1)");
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    @Test
    public void testLoad_QueryReturnedNull() throws Exception {
        ItemBuilderTest.i1.setId("-1");
        ItemBuilderTest.i1.setAmount(0);

        ItemBuilderTest.i2.setId("1");
        ItemBuilderTest.i2.setAmount(0);

        ItemBuilderTest.i3.setId("4");
        ItemBuilderTest.i3.setAmount(0);

        final ItemBuilder ib = new ItemBuilder();

        assertEquals(ItemBuilderTest.il.getTsvString(true), ib.load("1").getTsvString(true));
    }

    @Test
    public void testCreateItems_SuccessfulQuery() throws Exception {
        ItemBuilderTest.i1.setId("-1");
        ItemBuilderTest.i1.setAmount(10000);

        ItemBuilderTest.i2.setId("1");
        ItemBuilderTest.i2.setAmount(20);

        ItemBuilderTest.i3.setId("4");
        ItemBuilderTest.i3.setAmount(0);

        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount").values(1, true, -1, 10000)
                .values(1, true, 1, 20).values(2, true, 4, 100).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final ItemBuilder ib = new ItemBuilder();

        assertEquals(ItemBuilderTest.il.getTsvString(true), ib.load("1").getTsvString(true));
    }
}
