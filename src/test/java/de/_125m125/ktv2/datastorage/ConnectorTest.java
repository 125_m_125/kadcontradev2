package de._125m125.ktv2.datastorage;

import static com.ninja_squad.dbsetup.Operations.deleteAllFrom;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.UnavailableException;
import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.logging.Log;

public class ConnectorTest {
    private static Connector c;

    public static final Operation DELETE_ALL = deleteAllFrom(Connector.TABLES);
    private static DataSource     ds;

    @AfterClass
    public static void afterClass() {
        Log.enable();
        ConnectorTest.c.close();
    }

    @BeforeClass
    public static void beforeClass() throws UnavailableException {
        Variables.dbname = "kttest";
        Variables.dbpass = "kttupass";
        Variables.dbuser = "kttu";
        Variables.setPoolSize(1);

        new Main(false);
        Log.disable();
        ConnectorTest.c = new Connector();
        Main.instance().setConnector(ConnectorTest.c);

        ConnectorTest.ds = ConnectorTest.c.getDataSource();

        ConnectorTest.c.executeUpdate("DROP DATABASE kttest");
        ConnectorTest.c.executeUpdate("CREATE DATABASE kttest");
        ConnectorTest.c.execute("use kttest");
        ConnectorTest.c.checktables();
    }

    @Before
    public void prepareDB() {
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(ConnectorTest.ds), ConnectorTest.DELETE_ALL);
        dbSetup.launch();
    }

    @Test
    public void testExecuteAtomicUpdate_success() {
        final ArrayList<String[]> querys = new ArrayList<>();
        String[] q = { "INSERT INTO system(k,v) VALUES('testKey','testValue')" };
        querys.add(q);
        q = new String[] { "UPDATE system SET v=? WHERE k=?", "testValue2", "testKey" };
        querys.add(q);

        final boolean b = ConnectorTest.c.executeAtomicUpdate(querys);

        final ArrayList<HashMap<String, Object>> result =
                ConnectorTest.c.executeQuery("SELECT * FROM system");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("testKey", result.get(0).get("k"));
        assertEquals("testValue2", result.get(0).get("v"));
        assertTrue(b);
    }

    @Test
    public void testExecuteAtomicUpdate_fail() {
        final PrintStream tmp = System.err;
        System.setErr(new PrintStream(new OutputStream() {

            @Override
            public void write(final int b) throws IOException {

            }
        }));
        final ArrayList<String[]> querys = new ArrayList<>();
        String[] q = { "INSERT INTO system(k,v) VALUES('testKey','testValue')" };
        querys.add(q);
        q = new String[] { "UPDATE system SET v=? WHERE k=?", "testValue2", "testKey" };
        querys.add(q);
        q = new String[] { "FAILING IS ALWAYS AN OPTION" };
        querys.add(q);

        final boolean b = ConnectorTest.c.executeAtomicUpdate(querys);

        final ArrayList<HashMap<String, Object>> result =
                ConnectorTest.c.executeQuery("SELECT * FROM system");
        assertTrue(result == null || result.size() == 0);
        assertFalse(b);

        System.setErr(tmp);
    }

    @Test
    public void testLogin_banned() throws Exception {
        final Operation op = sequenceOf(
                insertInto("login").columns("id", "Name", "Password", "IP")
                        .values(1, "testuser",
                                "$2a$06$bobemiKcFCiJF4Q6t8swD.IC8ikosBtcz5xPFr5JeHNPVa4x2f73G",
                                "0:0:0:0:0:0:0:1")
                        .build(),
                insertInto("banned").columns("id", "reason").values(1, "testban").build());
        new DbSetup(new DataSourceDestination(ConnectorTest.ds), op).launch();
        UserHelper.get("testuser");
        final String result = ConnectorTest.c.login("testuser", "testpw", "0:0:0:0:0:0:0:1");

        assertEquals("banned testban", result);
    }

    @Test
    public void testLogin_banned_wrongPw() throws Exception {
        final Operation op = sequenceOf(
                insertInto("login").columns("id", "Name", "Password", "IP")
                        .values(1, "testuser",
                                "$2a$06$bobemiKcFCiJF4Q6t8swD.IC8ikosBtcz5xPFr5JeHNPVa4x2f73G",
                                "0:0:0:0:0:0:0:1")
                        .build(),
                insertInto("banned").columns("id", "reason").values(1, "testban").build());
        new DbSetup(new DataSourceDestination(ConnectorTest.ds), op).launch();

        final String result = ConnectorTest.c.login("testuser", "testpw2", "0:0:0:0:0:0:0:1");

        assertEquals("PasswordWrong", result);
    }

    @Test
    public void testCreateUser_materials() throws Exception {
        final Operation op = insertInto("unknownPayins")
                .columns("username", "type", "item", "amount").values("test", true, "-1", "5")
                .values("test", true, "10", "20").values("test2", true, "11", "20").build();
        final Operation op2 = insertInto("materials").columns("id", "type", "matid", "amount")
                .values(1, true, -1, 1000000000000L).values(1, true, 4, 100).build();
        new DbSetup(new DataSourceDestination(ConnectorTest.ds), sequenceOf(op, op2)).launch();

        final String s = ConnectorTest.c.createUser("test", "a", "a", 0, "127.0.0.1", null, null);
        final long id = Long.parseLong(s.substring(7));

        final ArrayList<HashMap<String, Object>> executeQuery =
                ConnectorTest.c.executeQuery("SELECT * FROM materials ORDER BY id,type,matid");
        assertNotNull(executeQuery);
        assertEquals(5, executeQuery.size());

        assertEquals(1l, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(999950000000L, executeQuery.get(0).get("amount"));

        assertEquals(1l, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("4", executeQuery.get(1).get("matid"));
        assertEquals(36L, executeQuery.get(1).get("amount"));

        assertEquals(id, executeQuery.get(2).get("id"));
        assertEquals(true, executeQuery.get(2).get("type"));
        assertEquals("-1", executeQuery.get(2).get("matid"));
        assertEquals(50000005L, executeQuery.get(2).get("amount"));

        assertEquals(id, executeQuery.get(3).get("id"));
        assertEquals(true, executeQuery.get(3).get("type"));
        assertEquals("10", executeQuery.get(3).get("matid"));
        assertEquals(20L, executeQuery.get(3).get("amount"));

        assertEquals(id, executeQuery.get(4).get("id"));
        assertEquals(true, executeQuery.get(4).get("type"));
        assertEquals("4", executeQuery.get(4).get("matid"));
        assertEquals(64L, executeQuery.get(4).get("amount"));

        assertEquals(1, ConnectorTest.c.executeQuery("SELECT * FROM unknownPayins").size());
    }
}
