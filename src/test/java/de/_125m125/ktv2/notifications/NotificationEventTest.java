package de._125m125.ktv2.notifications;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.sql.Timestamp;

import org.junit.Test;

import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class NotificationEventTest {

    @Test
    public void testGetContentsWhenContentsArePresent() throws Exception {
        final Object[] contents = new Object[] { new Message(new Timestamp(15000), "Hello World"),
                new Message(new Timestamp(16000), "Bye World!") };
        final NotificationEvent ne = new NotificationEvent(true, 1, "", "",
                Permission.READ_ITEMLIST_PERMISSION, contents);

        assertArrayEquals(contents, ne.getContents());
    }

    @Test
    public void testGetContentsWhenContentsAreNotPresent() throws Exception {
        final NotificationEvent ne = new NotificationEvent(true, 1, "", "",
                Permission.READ_ITEMLIST_PERMISSION);

        assertNull(ne.getContents());
    }

    @Test
    public void testGetContentsWhenContentsAreNotPresentButSupplierExists() throws Exception {
        final Object[] contents = new Object[] { new Message(new Timestamp(15000), "Hello World"),
                new Message(new Timestamp(16000), "Bye World!") };
        final NotificationEvent ne = new NotificationEvent(true, 1, "", "",
                Permission.READ_ITEMLIST_PERMISSION,
                () -> new Object[] { contents[0], contents[1] });

        final Object[] contents2 = ne.getContents();
        assertArrayEquals(contents, contents2);
        assertNotSame(contents, contents2);
        assertSame(contents2, ne.getContents());
    }
}
