package de._125m125.ktv2.websocket;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;

@RunWith(PowerMockRunner.class)
@PrepareForTest(WebSocket.class)
@PowerMockIgnore("javax.management.*")
public class WebSocketTest extends DatabaseTester {
    private final Gson                      gson         = new Gson();

    @SuppressWarnings("unchecked")
    private final Map<String, Set<Session>> sessions     = Whitebox
            .getInternalState(WebSocket.class, "sessions");
    @SuppressWarnings("unchecked")
    private final Set<Session>              unregistered = Whitebox
            .getInternalState(WebSocket.class, Set.class);

    private WebSocket                       uut;
    private Session                         session;
    private Basic                           basicRemote;

    @BeforeClass
    public static void beforeClassWebSocketTest() {
        Whitebox.setInternalState(WebSocket.class,
                Clock.fixed(Instant.parse("2007-12-03T10:15:30.00Z"), ZoneId.systemDefault()));
        Variables.ID_TO_DN.clear();
        Variables.ID_TO_DN.put("4", "Cobblestone");
    }

    @Before
    public void beforeWebSocketTests() {
        this.sessions.clear();
        this.unregistered.clear();

        this.uut = new WebSocket();
        this.session = mock(Session.class);
        this.basicRemote = mock(Basic.class);

        when(this.session.isOpen()).thenReturn(true);
        when(this.session.getBasicRemote()).thenReturn(this.basicRemote);
        when(this.session.getId()).thenReturn("12345");
    }

    public void verifySendText(final Map<String, Object> expected, final Basic basicRemote)
            throws IOException {
        final ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(this.basicRemote, atLeast(1)).sendText(argumentCaptor.capture());
        final String s = argumentCaptor.getValue();
        final Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        final Map<String, Object> result = this.gson.fromJson(s, type);
        for (final Entry<String, Object> entry : expected.entrySet()) {
            assertEquals(entry.getValue(), result.get(entry.getKey()));
        }
    }

    @Test
    public void testOnMessage_ping() throws Exception {
        this.uut.onMessage(this.session, "{\"rid\":1234567,\"ping\":1196676910000}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("pong", 1196676930000D);// Gson parses to double
        verifySendText(expected, this.basicRemote);
    }

    @Test
    public void testOnMessage_pingContainsRid() throws Exception {
        this.uut.onMessage(this.session, "{\"rid\":1234567,\"ping\":1196676910000}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("rid", 1234567D);// Gson parses to double
        verifySendText(expected, this.basicRemote);
    }

    @Test
    public void testOnMessage_unknownRequest() throws Exception {
        this.uut.onMessage(this.session, "{\"rid\":1234567,\"asdf\":1196676910000}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "unknownRequest");
        verifySendText(expected, this.basicRemote);
    }

    @Test
    public void testOnMessage_unknownRequestContainsRid() throws Exception {
        this.uut.onMessage(this.session, "{\"rid\":1234567,\"asdf\":1196676910000}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("rid", 1234567D);// Gson parses to double
        verifySendText(expected, this.basicRemote);
    }

    @Test
    public void testOnMessage_subscribe_noChannel() throws Exception {
        this.uut.onMessage(this.session, "{\"rid\":\"abcdef\",\"subscribe\":{}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "invalidChannel");
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.isEmpty());
    }

    @Test
    public void testOnMessage_subscribe_noMap() throws Exception {
        this.uut.onMessage(this.session, "{\"rid\":\"abcdef\",\"subscribe\":\"notAMap\"}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "invalidSubscribeRequest");
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.isEmpty());
    }

    @Test
    public void testOnMessage_subscribe_public() throws Exception {
        this.uut.onMessage(this.session,
                "{\"rid\":\"abcdef\",\"subscribe\":{\"channel\":\"orderbook\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", false);
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.get("orderbook").contains(this.session));
    }

    @Test
    public void testOnMessage_subscribe_publicWithItem() throws Exception {
        this.uut.onMessage(this.session,
                "{\"rid\":\"abcdef\",\"subscribe\":{\"channel\":\"orderbook\",\"item\":\"4\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", false);
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.get("orderbook-4").contains(this.session));
    }

    @Test
    public void testOnMessage_subscribe_publicWithUnknownItem() throws Exception {
        this.uut.onMessage(this.session,
                "{\"rid\":\"abcdef\",\"subscribe\":{\"channel\":\"orderbook\",\"item\":\"5\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "unknownItem");
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);
    }

    @Test
    public void testOnMessage_subscribe_noAuth() throws Exception {
        this.uut.onMessage(this.session,
                "{\"rid\":\"abcdef\",\"subscribe\":{\"channel\":\"rMessages\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "missingUid");
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.isEmpty());
    }

    @Test
    public void testOnMessage_subscribe_private() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1, 2, 15, 4).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        this.uut.onMessage(this.session,
                "{\"rid\":\"abcdef\",\"subscribe\":{\"channel\":\"rMessages\",\"uid\":\"1\",\"tid\"=\"2\",\"tkn\":\"4\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", false);
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.get("1-rMessages").contains(this.session));
    }

    @Test
    public void testOnMessage_subscribe_private_noPermission() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1, 2, 7, 4).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        this.uut.onMessage(this.session,
                "{\"rid\":\"abcdef\",\"subscribe\":{\"channel\":\"rMessages\",\"uid\":\"1\",\"tid\"=\"2\",\"tkn\":\"4\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "insufficientPermissions");
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.isEmpty());
    }

    @Test
    public void testOnMessage_subscribe_addsToExistingList() throws Exception {
        final Session s2 = mock(Session.class);
        final HashSet<Session> sessionList = new HashSet<>();
        sessionList.add(s2);
        this.sessions.put("orderbook", sessionList);

        this.uut.onMessage(this.session,
                "{\"rid\":\"abcdef\",\"subscribe\":{\"channel\":\"orderbook\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", false);
        expected.put("rid", "abcdef");
        verifySendText(expected, this.basicRemote);

        assertTrue(this.sessions.get("orderbook").contains(this.session));
        assertTrue(this.sessions.get("orderbook").contains(s2));
    }

    @Test
    public void testClose_unsubscribed() {
        final Session s2 = mock(Session.class);
        this.unregistered.add(this.session);
        this.unregistered.add(s2);

        this.uut.onClose(this.session);

        assertFalse(this.unregistered.contains(this.session));
        assertTrue(this.unregistered.contains(s2));
    }

    @Test
    public void testClose_subscribed() {
        final Session s2 = mock(Session.class);
        this.unregistered.add(this.session);
        this.unregistered.add(s2);

        this.uut.onClose(this.session);

        assertFalse(this.unregistered.contains(this.session));
        assertTrue(this.unregistered.contains(s2));
    }

    private final Type     responseType   = new TypeToken<Map<String, Object>>() {
                                          }.getType();
    ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

    private Object getFromSessionResponse(final String response, final String attribute) {
        final Map<String, Object> result = this.gson.fromJson(response, this.responseType);
        final Map<String, Object> object = (Map<String, Object>) result.get("session");
        return object.get(attribute);
    }

    private Object getFromSessionResponse(final int expectedCount, final String attribute)
            throws IOException {
        return getFromSessionResponse(this.basicRemote, expectedCount, attribute);
    }

    private Object getFromSessionResponse(final Basic basicRemote2, final int expectedCount,
            final String attribute) throws IOException {
        verify(basicRemote2, times(expectedCount)).sendText(this.argumentCaptor.capture());
        final String s2 = this.argumentCaptor.getValue();
        return getFromSessionResponse(s2, attribute);
    }

    @Test
    public void testSessionStart_oncePerSession() throws Exception {
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"start\"}}");
        verify(this.basicRemote).sendText(this.argumentCaptor.capture());
        final String s = this.argumentCaptor.getValue();

        final Map<String, Object> result1 = this.gson.fromJson(s, this.responseType);

        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"start\"}}");
        verify(this.basicRemote, times(2)).sendText(this.argumentCaptor.capture());
        final String s2 = this.argumentCaptor.getValue();
        final Map<String, Object> result2 = this.gson.fromJson(s2, this.responseType);

        assertEquals(result1, result2);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testSessionStatus_containsSubscriptions() throws Exception {
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"start\"}}");
        this.uut.onMessage(this.session,
                "{\"rid\":\"a\",\"subscribe\":{\"channel\":\"orderbook\"}}");
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"start\"}}");

        final Collection<String> subscriptions = (Collection<String>) getFromSessionResponse(3,
                "subscriptions");
        assertTrue(subscriptions.contains("orderbook"));
    }

    @Test
    public void testSessionResumption() throws Exception {
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"start\"}}");
        final Object object = getFromSessionResponse(1, "id");

        this.uut.onMessage(this.session,
                "{\"rid\":\"a\",\"subscribe\":{\"channel\":\"orderbook\"}}");

        final Session s2 = mock(Session.class);

        final Basic basicRemote2 = mock(Basic.class);
        when(s2.isOpen()).thenReturn(true);
        when(s2.getBasicRemote()).thenReturn(basicRemote2);
        when(s2.getId()).thenReturn("22345");
        this.uut.onMessage(s2, "{\"session\":{\"request\":\"resume\",\"id\":\"" + object + "\"}}");

        final Collection<String> subscriptions = (Collection<String>) getFromSessionResponse(
                basicRemote2, 1, "subscriptions");
        assertTrue("response should have contained the subscription to channel orderbook",
                subscriptions.contains("orderbook"));

        assertTrue("base session should have been subscribed to orderbook",
                this.sessions.get("orderbook").contains(this.session));
        assertTrue("resumed session should have been subscribed to orderbook",
                this.sessions.get("orderbook").contains(s2));
    }

    @Test
    public void testSessionResumtion_failsOnUnknownSession() throws Exception {
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"resume\",\"id\":\"???\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "unknownSessionId");
        verifySendText(expected, this.basicRemote);

        assertEquals(0, this.sessions.size());
    }

    @Test
    public void testSessionResumtion_failsIfAlreadyInSession() throws Exception {
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"start\"}}");
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"resume\",\"id\":\"???\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "sessionAlreadyStarted");
        verifySendText(expected, this.basicRemote);

        assertEquals(0, this.sessions.size());
    }

    @Test
    public void testSessionResumtion_noSessionId() throws Exception {
        this.uut.onMessage(this.session, "{\"session\":{\"request\":\"resume\"}}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "missingSessionId");
        verifySendText(expected, this.basicRemote);

        assertEquals(0, this.sessions.size());
    }

    @Test
    public void testInvalidJson() throws Exception {
        this.uut.onMessage(this.session, "{\"ping\"}");

        final Map<String, Object> expected = new HashMap<>();
        expected.put("error", "invalidJson");
        verifySendText(expected, this.basicRemote);
    }

}
