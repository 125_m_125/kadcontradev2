package de._125m125.ktv2.twoFactorAuth;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.junit.BeforeClass;
import org.powermock.reflect.Whitebox;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;

public class AuthStateTest extends DatabaseTester {

    @BeforeClass
    public static void beforeClassAuthStateTest() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final String keyStr = "secret";
        byte[] key = (keyStr).getBytes("UTF-8");
        final MessageDigest sha = MessageDigest.getInstance("SHA-256");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        final SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        Whitebox.setInternalState(Variables.class, SecretKey.class, secretKeySpec);
    }

}
