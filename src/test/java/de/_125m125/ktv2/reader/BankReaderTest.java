package de._125m125.ktv2.reader;

import static com.ninja_squad.dbsetup.Operations.deleteAllFrom;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import javax.servlet.UnavailableException;
import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.logging.Log;

public class BankReaderTest {

    private static Connector c;

    public static final Operation DELETE_ALL = deleteAllFrom(Connector.TABLES);
    private static DataSource     ds;

    @AfterClass
    public static void afterClass() {
        Log.enable();
        BankReaderTest.c.close();
    }

    @BeforeClass
    public static void beforeClass() throws UnavailableException {
        Variables.dbname = "kttest";
        Variables.dbpass = "kttupass";
        Variables.dbuser = "kttu";
        Variables.setPoolSize(1);

        new Main(false);
        Log.disable();
        BankReaderTest.c = new Connector();
        Main.instance().setConnector(BankReaderTest.c);

        BankReaderTest.ds = BankReaderTest.c.getDataSource();
        BankReaderTest.c.executeUpdate("DROP DATABASE kttest");
        BankReaderTest.c.executeUpdate("CREATE DATABASE kttest");
        BankReaderTest.c.execute("use kttest");
        BankReaderTest.c.checktables();

        Variables.ID_TO_DN.put("4", "Cobblestone");
        Variables.ID_TO_DN.put("JuniCrate5", "JuniCrate5");
    }

    @Before
    public void prepareDB() {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds),
                BankReaderTest.DELETE_ALL);
        dbSetup.launch();
    }

    @Test
    public void testExtractEntries_error() {
        final Scanner s =
                new Scanner(BankReaderTest.class.getResourceAsStream("/noAccess.bankoutput"));
        assertEquals(new ArrayList<String>(), new BankReader(null).extractEntries(s));
    }

    @Test
    public void testUserRegistration_insertsIntoLogin() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "referrer", "id")
                .values("test", "testpass", "127.0.0.1", "comment", "2", "0").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "register0 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM login");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertThat(entry.get("id"), instanceOf(Long.class));
        assertEquals("test", entry.get("Name"));
        assertEquals("testpass", entry.get("Password"));
        assertEquals("127.0.0.1", entry.get("IP"));
    }

    @Test
    public void testUserRegistration_referrer() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "id", "referrer")
                .values("test", "testpass", "127.0.0.1", "comment", "0", 2).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "register0 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM referrer");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertThat(entry.get("id"), instanceOf(Long.class));
        assertEquals(2L, entry.get("ref"));
    }

    @Test
    public void testUserRegistration_insertsIntoMaterials() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "referrer", "id")
                .values("test", "testpass", "127.0.0.1", "comment", "2", "0").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "register0 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> materials =
                BankReaderTest.c.executeQuery("SELECT * FROM materials ORDER BY id,type,matid");
        assertNotNull(materials);
        assertEquals(4, materials.size());
        assertEquals(1l, materials.get(0).get("id"));
        assertEquals(true, materials.get(0).get("type"));
        assertEquals("-1", materials.get(0).get("matid"));
        assertEquals(-50000000L, materials.get(0).get("amount"));

        assertEquals(1l, materials.get(1).get("id"));
        assertEquals(true, materials.get(1).get("type"));
        assertEquals("4", materials.get(1).get("matid"));
        assertEquals(-64L, materials.get(1).get("amount"));

        assertEquals(true, materials.get(2).get("type"));
        assertEquals("-1", materials.get(2).get("matid"));
        assertEquals(50000000L, materials.get(2).get("amount"));

        assertEquals(true, materials.get(3).get("type"));
        assertEquals("4", materials.get(3).get("matid"));
        assertEquals(64L, materials.get(3).get("amount"));
    }

    @Test
    public void testUserRegistration_choosesCorrect() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "referrer", "id")
                .values("test", "testpass1", "127.0.0.1", "comment", "2", "0")
                .values("test", "testpass2", "127.0.0.1", "comment", "2", "1")
                .values("test", "testpass3", "127.0.0.1", "comment", "2", "2")
                .values("test2", "evilpass1", "128.0.0.1", "comment2", "2", "0")
                .values("test2", "evilpass2", "128.0.0.1", "comment2", "2", "1")
                .values("test2", "evilpass3", "128.0.0.1", "comment2", "2", "2").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "register1 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM login");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertThat(entry.get("id"), instanceOf(Long.class));
        assertEquals("test", entry.get("Name"));
        assertEquals("testpass2", entry.get("Password"));
        assertEquals("127.0.0.1", entry.get("IP"));
    }

    @Test
    public void testUserRegistration_missingPossibleRegister() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "referrer", "id")
                .values("test", "testpass1", "127.0.0.1", "comment", "2", "0")
                .values("test", "testpass2", "127.0.0.1", "comment", "2", "1")
                .values("test", "testpass3", "127.0.0.1", "comment", "2", "2")
                .values("test2", "evilpass1", "128.0.0.1", "comment2", "2", "0")
                .values("test2", "evilpass2", "128.0.0.1", "comment2", "2", "1")
                .values("test2", "evilpass3", "128.0.0.1", "comment2", "2", "2").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "register4 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM login");
        assertNull(login);
    }

    @Test
    public void testUserRegistration_missingPossibleRegisterUser() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "referrer", "id")
                .values("test", "testpass1", "127.0.0.1", "comment", "2", "0")
                .values("test", "testpass2", "127.0.0.1", "comment", "2", "1")
                .values("test", "testpass3", "127.0.0.1", "comment", "2", "2")
                .values("test2", "evilpass1", "128.0.0.1", "comment2", "2", "0")
                .values("test2", "evilpass2", "128.0.0.1", "comment2", "2", "1")
                .values("test2", "evilpass3", "128.0.0.1", "comment2", "2", "2").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test3",
                                "PowerSign", "register1 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM login");
        assertNull(login);
    }

    @Test
    public void testUserRegistration_removesFromPossibles() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "referrer", "id")
                .values("test", "testpass1", "127.0.0.1", "comment", "2", "0")
                .values("test", "testpass2", "127.0.0.1", "comment", "2", "1")
                .values("test2", "evilpass", "128.0.0.1", "comment2", "2", "0").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "register0 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM possibleRegister");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertEquals("test2", entry.get("name"));
        assertEquals("evilpass", entry.get("Password"));
        assertEquals("128.0.0.1", entry.get("IP"));
        assertEquals("comment2", entry.get("itemcomment"));
        assertEquals(0, entry.get("id"));
    }

    @Test
    public void testUserPWChange_changesPassword() {
        final Operation op = sequenceOf(
                insertInto("possiblepwchange").columns("name", "Password", "id")
                        .values("test", "changedpass", "0").build(),
                insertInto("login").columns("id", "Name", "Password", "IP")
                        .values("1", "test", "testpass", "127.0.0.1").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "changepw0 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM login");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertEquals(1L, entry.get("id"));
        assertEquals("test", entry.get("Name"));
        assertEquals("changedpass", entry.get("Password"));
        assertEquals("127.0.0.1", entry.get("IP"));
    }

    @Test
    public void testUserPWChange_removesPossibles() {
        final Operation op = sequenceOf(
                insertInto("possiblepwchange").columns("name", "Password", "id")
                        .values("test", "changedpass", "0").values("test", "changedpass", "1")
                        .values("other", "evilpass", "0").build(),
                insertInto("login").columns("id", "Name", "Password", "IP")
                        .values("1", "test", "testpass", "127.0.0.1").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "changepw0 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM possiblepwchange");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertEquals("other", entry.get("name"));
        assertEquals("evilpass", entry.get("Password"));
        assertEquals(0, entry.get("id"));
    }

    @Test
    public void testUserPWChange_choosesCorrectly() {
        final Operation op = sequenceOf(
                insertInto("possiblepwchange").columns("name", "Password", "id")
                        .values("test", "changedpass1", "0").values("test", "changedpass2", "1")
                        .values("test", "changedpass2", "2").values("other", "evilpass0", "0")
                        .values("other", "evilpass1", "1").values("other", "evilpass2", "2")
                        .build(),
                insertInto("login").columns("id", "Name", "Password", "IP")
                        .values("1", "test", "testpass", "127.0.0.1").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "changepw1 aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM login");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertEquals(1L, entry.get("id"));
        assertEquals("test", entry.get("Name"));
        assertEquals("changedpass2", entry.get("Password"));
        assertEquals("127.0.0.1", entry.get("IP"));
    }

    @Test
    public void testClear_removesPwchange() {
        final Operation op = sequenceOf(
                insertInto("possiblepwchange").columns("name", "Password", "id")
                        .values("test", "changedpass", "0").values("test", "changedpass", "1")
                        .values("other", "evilpass", "0").build(),
                insertInto("login").columns("id", "Name", "Password", "IP")
                        .values("1", "test", "testpass", "127.0.0.1").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "clear aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM possiblepwchange");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertEquals("other", entry.get("name"));
        assertEquals("evilpass", entry.get("Password"));
        assertEquals(0, entry.get("id"));
    }

    @Test
    public void testClear_removesRegister() {
        final Operation op = insertInto("possibleRegister")
                .columns("name", "Password", "IP", "itemcomment", "id")
                .values("test", "testpass1", "127.0.0.1", "comment", "0")
                .values("test", "testpass2", "127.0.0.1", "comment", "1")
                .values("test2", "evilpass", "128.0.0.1", "comment2", "0").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "&#8592;", "0,00", "", "test",
                                "PowerSign", "clear aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> login =
                BankReaderTest.c.executeQuery("SELECT * FROM possibleRegister");
        assertNotNull(login);
        assertEquals(1, login.size());
        final HashMap<String, Object> entry = login.get(0);
        assertNotNull(entry);
        assertEquals("test2", entry.get("name"));
        assertEquals("evilpass", entry.get("Password"));
        assertEquals("128.0.0.1", entry.get("IP"));
        assertEquals("comment2", entry.get("itemcomment"));
        assertEquals(0, entry.get("id"));
    }

    @Test
    public void testChestshop_success_new() {
        final Operation op = insertInto("login").columns("id", "name").values(1, "test").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "", "0,63", "&#8594;", "test",
                                "KShop", "64 Cobblestone (4:0)", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> executeQuery =
                BankReaderTest.c.executeQuery("SELECT * FROM materials ORDER BY id,type,matid");
        assertNotNull(executeQuery);
        assertEquals(2, executeQuery.size());

        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(-630000L, executeQuery.get(0).get("amount"));

        assertEquals(1L, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("4", executeQuery.get(1).get("matid"));
        assertEquals(64L, executeQuery.get(1).get("amount"));
    }

    @Test
    public void testChestshop_success_existing() {
        final Operation op = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, -1, 1000000).values(1, true, 4, 90).build(),
                insertInto("login").columns("id", "name").values(1, "test").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "", "0,64", "&#8594;", "test",
                                "KShop", "64 Cobblestone (4:0)", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> executeQuery =
                BankReaderTest.c.executeQuery("SELECT * FROM materials ORDER BY id,type,matid");
        assertNotNull(executeQuery);
        assertEquals(2, executeQuery.size());

        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(360000L, executeQuery.get(0).get("amount"));

        assertEquals(1L, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("4", executeQuery.get(1).get("matid"));
        assertEquals(154L, executeQuery.get(1).get("amount"));
    }

    @Test
    public void testChestshop_success_crates() {
        final Operation op = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, -1, 1000000).build(),
                insertInto("login").columns("id", "name").values(1, "test").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "", "0,01", "&#8594;", "test",
                                "KShop", "1 c_JuniCrate5 Heads (397:3)", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> executeQuery =
                BankReaderTest.c.executeQuery("SELECT * FROM materials ORDER BY id,type,matid");
        assertNotNull(executeQuery);
        assertEquals(2, executeQuery.size());

        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(990000L, executeQuery.get(0).get("amount"));

        assertEquals(1L, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("JuniCrate5", executeQuery.get(1).get("matid"));
        assertEquals(1L, executeQuery.get(1).get("amount"));
    }

    @Test
    public void testChestshop_success_unknownUser() {
        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "", "0,01", "&#8594;", "test",
                                "KShop", "1 c_JuniCrate5 Heads (397:3)", "Server 2", "1000000" },
                        { "2001-01-01 01:01:02", "*kadcontrade", "", "0,02", "&#8594;", "test",
                                "KShop", "2 c_JuniCrate5 Heads (397:3)", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> executeQuery = BankReaderTest.c
                .executeQuery("SELECT * FROM unknownPayins ORDER BY username,type,item");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());

        assertEquals("test", executeQuery.get(0).get("username"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("JuniCrate5", executeQuery.get(0).get("item"));
        assertEquals(3L, executeQuery.get(0).get("amount"));
    }

    @Test
    public void testPayin_success_existing() {
        final Operation op = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, -1, 1000000).values(1, true, 4, 90).build(),
                insertInto("login").columns("id", "name").values(1, "test").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] { { "2001-01-01 01:01:01", "*kadcontrade", "%#8592;",
                        "10,12", "", "test", "KBank", "", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> executeQuery =
                BankReaderTest.c.executeQuery("SELECT * FROM materials ORDER BY id,type,matid");
        assertNotNull(executeQuery);
        assertEquals(2, executeQuery.size());

        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(11120000L, executeQuery.get(0).get("amount"));

        assertEquals(1L, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("4", executeQuery.get(1).get("matid"));
        assertEquals(90L, executeQuery.get(1).get("amount"));
    }

    @Test
    public void testPayin_success_unknownUser() {
        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] { { "2001-01-01 01:01:01", "*kadcontrade", "%#8592;",
                        "10,12", "", "test", "KBank", "", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> executeQuery = BankReaderTest.c
                .executeQuery("SELECT * FROM unknownPayins ORDER BY username,type,amount");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());

        assertEquals("test", executeQuery.get(0).get("username"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("item"));
        assertEquals(10120000L, executeQuery.get(0).get("amount"));
    }

    @Test
    public void testPayin_success_powersign() {
        final Operation op = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, -1, 1000000).values(1, true, 4, 90).build(),
                insertInto("login").columns("id", "name").values(1, "test").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(BankReaderTest.ds), op);
        dbSetup.launch();

        new BankReader(BankReaderTest.c).analyseEntries(
                Arrays.asList(new String[][] {
                        { "2001-01-01 01:01:01", "*kadcontrade", "%#8592;", "10,12", "", "test",
                                "Powersign", "payin aktiviert", "Server 2", "1000000" } }),
                new Timestamp(0), "null");

        final ArrayList<HashMap<String, Object>> executeQuery =
                BankReaderTest.c.executeQuery("SELECT * FROM materials ORDER BY id,type,matid");
        assertNotNull(executeQuery);
        assertEquals(2, executeQuery.size());

        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(11120000L, executeQuery.get(0).get("amount"));

        assertEquals(1L, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("4", executeQuery.get(1).get("matid"));
        assertEquals(90L, executeQuery.get(1).get("amount"));
    }

}