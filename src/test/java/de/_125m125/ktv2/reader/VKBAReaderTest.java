package de._125m125.ktv2.reader;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.doCallRealMethod;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.json.Json;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({ VKBAReader.class })
public class VKBAReaderTest extends DatabaseTester {

    @Test
    public void testAnalyse_noId() throws Exception {
        final Operation op = sequenceOf(insertInto("login").columns("id", "name").values(1, "test").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();
        Whitebox.setInternalState(Variables.class, "vkbaName", "kadcontrade");

        final boolean result;
        try (InputStream is = VKBAReaderTest.class.getResourceAsStream("/vkbaTest.json")) {
            result = Whitebox.invokeMethod(VKBAReader.class, Json.createReader(is).readArray(), "noMatch");
        }

        assertFalse(result);

        final ArrayList<HashMap<String, Object>> executeQuery = DatabaseTester.c
                .executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(99990000L, executeQuery.get(0).get("amount"));
    }

    @Test
    public void testAnalyse_Id() throws Exception {
        final Operation op = sequenceOf(insertInto("login").columns("id", "name").values(1, "test").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();
        Whitebox.setInternalState(Variables.class, "vkbaName", "kadcontrade");

        final boolean result;
        try (InputStream is = VKBAReaderTest.class.getResourceAsStream("/vkbaTest.json")) {
            result = Whitebox.invokeMethod(VKBAReader.class, Json.createReader(is).readArray(), "848970419");
        }

        assertTrue(result);

        final ArrayList<HashMap<String, Object>> executeQuery = DatabaseTester.c
                .executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(11110000L, executeQuery.get(0).get("amount"));
    }

    @Test
    public void testMaxRequests() throws Exception {
        final Operation op = sequenceOf(
                insertInto("system").columns("k", "v").values("last_VKBA_ID", 12345678).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        mockStatic(VKBAReader.class);
        when(VKBAReader.class, "executeVkbaRequest", anyInt(), anyInt()).thenReturn(
                Json.createArrayBuilder().add(Json.createObjectBuilder().add("tID", "123456").build()).build());
        when(VKBAReader.class, "analyse", any(), any()).thenReturn(false);
        doCallRealMethod().when(VKBAReader.class);
        VKBAReader.run();

        VKBAReader.run();

        verifyPrivate(VKBAReader.class, times(10)).invoke("executeVkbaRequest", anyInt(), anyInt());
    }

    @Test
    public void testError() throws Exception {
        final Operation op = sequenceOf(
                insertInto("system").columns("k", "v").values("last_VKBA_ID", 12345678).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        mockStatic(VKBAReader.class);
        when(VKBAReader.class, "executeVkbaRequest", anyInt(), anyInt()).thenReturn(null);
        doCallRealMethod().when(VKBAReader.class);
        VKBAReader.run();

        VKBAReader.run();

        verifyPrivate(VKBAReader.class, times(0)).invoke("analyse", any(), any());
    }
}
