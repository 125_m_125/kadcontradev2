package de._125m125.ktv2.servlets;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.reader.VKBAReader;

public class PayoutServletTest extends DatabaseTester {
    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.put("1", "1");
    }

    @Test
    public void testCreatePayout() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("lieferung");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("info", Variables.translate("payoutSuccess"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(900L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(100L, executeQuery.get(0).get("amount"));
        assertEquals(true, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(true, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("", executeQuery.get(0).get("message"));
        assertEquals("lieferung", executeQuery.get(0).get("payoutType"));
    }

    @Test
    public void testCreatePayout_box() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build(),
                insertInto("payoutboxes")
                        .columns("cid", "server", "x", "y", "z", "owner", "verified")
                        .values(15, 2, 3, 4, 5, 1, true).values(19, 1, 6, 7, 8, 2, true).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("bs2");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("info", Variables.translate("payoutSuccess"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(900L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(100L, executeQuery.get(0).get("amount"));
        assertEquals(true, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(true, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("", executeQuery.get(0).get("message"));
        assertEquals("bs2", executeQuery.get(0).get("payoutType"));
    }

    @Test
    public void testCreatePayout_boxNotVerified() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build(),
                insertInto("payoutboxes")
                        .columns("cid", "server", "x", "y", "z", "owner", "verified")
                        .values(15, 2, 3, 4, 5, 1, false).values(19, 1, 6, 7, 8, 2, true).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("bs2");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("noBoxOnThisServer"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.isEmpty());
    }

    @Test
    public void testCreatePayout_nobox() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build(),
                insertInto("payoutboxes")
                        .columns("cid", "server", "x", "y", "z", "owner", "verified")
                        .values(15, 2, 3, 4, 5, 1, true).values(19, 1, 6, 7, 8, 2, true).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("bs1");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("noBoxOnThisServer"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.isEmpty());
    }

    @Test
    public void testCreatePayout_everything() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 100).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("lieferung");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("info", Variables.translate("payoutSuccess"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(0L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(100L, executeQuery.get(0).get("amount"));
        assertEquals(true, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(true, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("", executeQuery.get(0).get("message"));
    }

    @Test
    public void testCreatePayout_tooHighAmount() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("10000");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("lieferung");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("notenoughmaterials"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.size() == 0);
    }

    @Test
    public void testCreatePayout_negativeAmount() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("-100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("lieferung");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("notenoughmaterials"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");

        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.size() == 0);
    }

    @Test
    public void testCreatePayout_zeroAmount() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("0");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("lieferung");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("notenoughmaterials"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");

        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.size() == 0);
    }

    @Test
    public void testCreatePayout_nonexistingItem() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getParameter("item")).thenReturn("2");
        when(request.getParameter("amount")).thenReturn("-100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("kadcon");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("unknownMaterial"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.size() == 0);
    }

    @Test
    public void testCreatePayout_invalidType() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("invalid");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("invalidType"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.size() == 0);
    }

    @Test
    public void testCreatePayout_missingSettings() throws Exception {
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), insertInto("materials")
                        .columns("id", "type", "matid", "amount").values(1, 1, 1, 1000).build());
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getParameter("item")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("-100");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("lieferung");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("noSettingsSubmitted"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("1", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertTrue(executeQuery == null || executeQuery.size() == 0);
    }

    @Test
    public void testCreatePayout_kadis() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).values(1, 1, -1, 1001000000).values(2, 1, -1, 1002)
                        .build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getParameter("item")).thenReturn("-1");
        when(request.getParameter("amount")).thenReturn("100.01");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("kadcon");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("info", Variables.translate("payoutSuccess"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(executeQuery);
        assertEquals(3, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(900990000L, executeQuery.get(0).get("amount"));
        assertEquals(1L, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("1", executeQuery.get(1).get("matid"));
        assertEquals(1000L, executeQuery.get(1).get("amount"));
        assertEquals(2L, executeQuery.get(2).get("id"));
        assertEquals(true, executeQuery.get(2).get("type"));
        assertEquals("-1", executeQuery.get(2).get("matid"));
        assertEquals(1002L, executeQuery.get(2).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(100010000L, executeQuery.get(0).get("amount"));
        assertEquals(true, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(true, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("", executeQuery.get(0).get("message"));
    }

    @Test
    @Ignore
    public void testCreatePayout_kadis_vkba() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, 1, 1, 1000).values(1, 1, -1, 1001000000).values(2, 1, -1, 1002)
                        .build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        PowerMockito.when(VKBAReader.class, "executeVkbaRequest", anyInt(), anyInt())
                .thenReturn("unknown");

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("payout")).thenReturn("payout");
        when(request.getParameter("item")).thenReturn("-1");
        when(request.getParameter("amount")).thenReturn("100.01");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);
        when(request.getParameter("type")).thenReturn("vkba");

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("info", Variables.translate("payoutSuccess"));
        verify(request).setAttribute("id", 1L);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(executeQuery);
        assertEquals(3, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(900990000L, executeQuery.get(0).get("amount"));
        assertEquals(1L, executeQuery.get(1).get("id"));
        assertEquals(true, executeQuery.get(1).get("type"));
        assertEquals("1", executeQuery.get(1).get("matid"));
        assertEquals(1000L, executeQuery.get(1).get("amount"));
        assertEquals(2L, executeQuery.get(2).get("id"));
        assertEquals(true, executeQuery.get(2).get("type"));
        assertEquals("-1", executeQuery.get(2).get("matid"));
        assertEquals(1002L, executeQuery.get(2).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
        assertEquals(100010000L, executeQuery.get(0).get("amount"));
        assertEquals(false, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(true, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(true, executeQuery.get(0).get("unknown"));
        assertEquals("vkba", executeQuery.get(0).get("payoutType"));
        assertEquals("", executeQuery.get(0).get("message"));
    }

    @Test
    public void testCancelPayout() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                insertInto("payout")
                        .columns("id", "uid", "type", "matid", "amount", "userInteraction",
                                "success", "adminInteractionCompleted", "requiresAdminInteraction",
                                "unknown", "date", "message")
                        .values(2, 1, 1, 3, 1000, true, false, false, true, false, new Date(),
                                "TEST")
                        .build());

        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("cancel")).thenReturn("cancel");
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));
        assertEquals(false, executeQuery.get(0).get("userInteraction"));
        assertEquals(true, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(false, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("TEST", executeQuery.get(0).get("message"));
    }

    @Test
    public void testCancelPayout_notOwned() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                insertInto("payout")
                        .columns("id", "uid", "type", "matid", "amount", "userInteraction",
                                "success", "adminInteractionCompleted", "requiresAdminInteraction",
                                "unknown", "date", "message")
                        .values(2, 1, 1, 3, 1000, true, false, false, true, false, new Date(),
                                "TEST")
                        .build());

        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(3L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("cancel")).thenReturn("cancel");
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(3L);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("notYourPayout"));

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertTrue(executeQuery == null || executeQuery.size() == 0);

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));
        assertEquals(true, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(true, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("TEST", executeQuery.get(0).get("message"));
    }

    @Test
    public void testCancelPayout_illegalState() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                insertInto("payout")
                        .columns("id", "uid", "type", "matid", "amount", "userInteraction",
                                "success", "adminInteractionCompleted", "requiresAdminInteraction",
                                "unknown", "date", "message")
                        .values(2, 1, 1, 3, 1000, true, false, true, false, false, new Date(),
                                "TEST")
                        .build());

        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getParameter("cancel")).thenReturn("cancel");
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("invalidPayoutState"));

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertTrue(executeQuery == null || executeQuery.size() == 0);

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));
        assertEquals(true, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(true, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(false, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("TEST", executeQuery.get(0).get("message"));
    }

    @Test
    public void testTakeoutPayout() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                insertInto("payout")
                        .columns("id", "uid", "type", "matid", "amount", "userInteraction",
                                "success", "adminInteractionCompleted", "requiresAdminInteraction",
                                "unknown", "date", "message")
                        .values(2, 1, 1, 3, 1000, true, false, true, false, false, new Date(),
                                "TEST")
                        .build());

        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("takeout")).thenReturn("takeout");
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);
        verify(session).setAttribute("info", Variables.translate("takeoutSuccess"));

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("id"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));
        assertEquals(false, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(true, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(false, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("TEST", executeQuery.get(0).get("message"));
    }

    @Test
    public void testTakoutPayout_notOwned() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                insertInto("payout")
                        .columns("id", "uid", "type", "matid", "amount", "userInteraction",
                                "success", "adminInteractionCompleted", "requiresAdminInteraction",
                                "unknown", "date", "message")
                        .values(2, 1, 1, 3, 1000, true, false, false, true, false, new Date(),
                                "TEST")
                        .build());

        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(3L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("takeout")).thenReturn("takeout");
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(3L);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("notYourPayout"));

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertTrue(executeQuery == null || executeQuery.size() == 0);

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));
        assertEquals(true, executeQuery.get(0).get("userInteraction"));
        assertEquals(false, executeQuery.get(0).get("success"));
        assertEquals(false, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(true, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("TEST", executeQuery.get(0).get("message"));
    }

    @Test
    public void testTakeoutPayout_illegalState() throws Exception {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                insertInto("payout")
                        .columns("id", "uid", "type", "matid", "amount", "userInteraction",
                                "success", "adminInteractionCompleted", "requiresAdminInteraction",
                                "unknown", "date", "message")
                        .values(2, 1, 1, 3, 1000, false, true, true, false, false, new Date(),
                                "TEST")
                        .build());

        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getParameter("takeout")).thenReturn("takeout");
        when(request.getParameter("id")).thenReturn("2");
        when(request.getRequestURI()).thenReturn("/auszahlung");
        when(request.getSession(false)).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        when(request.getAttribute("id")).thenReturn(1L);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doPost(request, response);

        verify(session).setAttribute("error", Variables.translate("invalidPayoutState"));

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertTrue(executeQuery == null || executeQuery.size() == 0);

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM payout");
        assertNotNull(executeQuery);
        assertEquals(1, executeQuery.size());
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(true, executeQuery.get(0).get("type"));
        assertEquals("3", executeQuery.get(0).get("matid"));
        assertEquals(1000L, executeQuery.get(0).get("amount"));
        assertEquals(false, executeQuery.get(0).get("userInteraction"));
        assertEquals(true, executeQuery.get(0).get("success"));
        assertEquals(true, executeQuery.get(0).get("adminInteractionCompleted"));
        assertEquals(false, executeQuery.get(0).get("requiresAdminInteraction"));
        assertEquals(false, executeQuery.get(0).get("unknown"));
        assertEquals("TEST", executeQuery.get(0).get("message"));
    }

    @Test
    public void testGet() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp")
                        .values(1, 2, 3, 4, 5, 6).build(),
                insertInto("payoutboxes")
                        .columns("cid", "server", "x", "y", "z", "owner", "verified")
                        .values(15, 2, 3, 4, 5, 1, true).values(19, 1, 6, 7, 8, 2, true)
                        .values(20, 3, 9, 10, 11, 1, false).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final RequestDispatcher rd = mock(RequestDispatcher.class);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher(any())).thenReturn(rd);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doGet(request, response);

        verify(request).setAttribute(eq("payoutTypes"),
                aryEq(new boolean[] { true, false, true, false }));
    }

    @Test
    public void testGet_noPayouts() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final RequestDispatcher rd = mock(RequestDispatcher.class);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher(any())).thenReturn(rd);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));

        new PayoutServlet().doGet(request, response);

        verify(request).setAttribute(eq("payoutTypes"),
                aryEq(new boolean[] { false, false, false, false }));
    }

}
