package de._125m125.ktv2.servlets.admin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.filter.MaintainanceFilter;
import de._125m125.ktv2.filter.MaintainanceFilter.Settings;
import de._125m125.ktv2.logging.Log;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PostRedirectGet.class })
public class MaintainanceServletTest {

    private static HttpSession session = mock(HttpSession.class);

    @BeforeClass
    public static void beforeClass() {
        Log.disable();

        when(MaintainanceServletTest.session.getAttribute("id")).thenReturn(1L);
    }

    @AfterClass
    public static void afterClass() {
        Log.disable();
    }

    @Before
    public void before() throws IOException {
        mockStatic(PostRedirectGet.class);
    }

    @Test
    public void testDoPost_deactivate() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("info", Variables.translate("saveSuccess"));
        assertFalse(MaintainanceFilter.isActive());
        assertNull(MaintainanceFilter.getSettings());

    }

    @Test
    public void testDoPost_all() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("all")).thenReturn("on");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("info", Variables.translate("saveSuccess"));
        assertTrue(MaintainanceFilter.isActive());
        assertEquals(new Settings(false, "."), MaintainanceFilter.getSettings());
    }

    @Test
    public void testDoPost_all_admin() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("all")).thenReturn("on");
        when(request.getParameter("lockAdmins")).thenReturn("on");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("info", Variables.translate("saveSuccess"));
        assertTrue(MaintainanceFilter.isActive());
        assertEquals(new Settings(true, "."), MaintainanceFilter.getSettings());
    }

    @Test
    public void testDoPost_activateSimple() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("regex");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("info", Variables.translate("saveSuccess"));
        assertTrue(MaintainanceFilter.isActive());
        assertEquals(new Settings(false, "regex"), MaintainanceFilter.getSettings());
    }

    @Test
    public void testDoPost_activateAdmin() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("regex");
        when(request.getParameter("lockAdmins")).thenReturn("on");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("info", Variables.translate("saveSuccess"));
        assertTrue(MaintainanceFilter.isActive());
        assertEquals(new Settings(true, "regex"), MaintainanceFilter.getSettings());
    }

    @Test
    public void testDoPost_activateLogin() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("login");
        when(request.getParameter("login")).thenReturn("on");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("info", Variables.translate("saveSuccess"));
        assertTrue(MaintainanceFilter.isActive());
        assertEquals(new Settings(false, "login"), MaintainanceFilter.getSettings());
    }

    @Test
    public void testDoPost_activateLogin_fail() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("login");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("error",
                Variables.translate("invalidRegex") + ":" + Variables.translate("blockingLogin"));
    }

    @Test
    public void testDoPost_activateResources() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("static");
        when(request.getParameter("resources")).thenReturn("on");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("info", Variables.translate("saveSuccess"));
        assertTrue(MaintainanceFilter.isActive());
        assertEquals(new Settings(false, "static"), MaintainanceFilter.getSettings());
    }

    @Test
    public void testDoPost_activateResources_fail() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("static");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("error",
                Variables.translate("invalidRegex") + ":" + Variables.translate("blockingResources"));
    }

    @Test
    public void testDoPost_activateMaintainance() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("/admin/maintainance");
        when(request.getParameter("lockAdmins")).thenReturn("on");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);

        verify(request).setAttribute("error",
                Variables.translate("invalidRegex") + ":" + Variables.translate("blockingMaintainance"));
    }

    @Test
    public void testDoPost_emptyRegex() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);
        verify(request).setAttribute("error", Variables.translate("invalidRegex"));
    }

    @Test
    public void testDoPost_invalidRegex() throws Exception {
        final MaintainanceServlet uut = new MaintainanceServlet();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(MaintainanceServletTest.session);
        when(request.getParameter("active")).thenReturn("on");
        when(request.getParameter("regex")).thenReturn("(.");

        final HttpServletResponse response = mock(HttpServletResponse.class);

        uut.doPost(request, response);
        verify(request).setAttribute(eq("error"), any(String.class));
    }
}
