package de._125m125.ktv2.servlets.admin;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.powermock.reflect.Whitebox;

import com.google.common.collect.ImmutableMap;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.servlets.admin.TSVFileGenerator.MysqlQuery;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

@RunWith(JUnitParamsRunner.class)
public class TSVFileGeneratorTest extends DatabaseTester {
    @Mock
    private HttpServletRequest  request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession         session;
    @Mock
    private RequestDispatcher   dispacher;
    @Mock
    private MySQLHelper         helper;

    private TSVFileGenerator uut;

    @BeforeClass
    public static void beforeClassTSVFileGeneratorTest() {
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    @Before
    public void beforeTSVFileGeneratorTest() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.uut = new TSVFileGenerator(this.helper);

        when(this.request.getRequestURI()).thenReturn("/admin/mysqlexporter");
        when(this.request.getRequestDispatcher(any())).thenReturn(this.dispacher);
        when(this.request.getSession()).thenReturn(this.session);

        when(this.helper.resultSetToTsv(any(), any(), eq(null))).thenReturn("fileName.tsv");
        when(this.helper.resultSetToTsv(any(String.class), any())).thenReturn("fileName.tsv");
    }

    @Test
    @Parameters(method = "tables")
    @TestCaseName("testQueriesValidWithoutArguments_{0}")
    public void testQueriesValidWithoutArguments(final String table) throws Exception {

        when(this.request.getParameter("generate")).thenReturn("generate");
        when(this.request.getParameterValues("tables")).thenReturn(new String[] { table });
        when(this.request.getParameter("anonymous")).thenReturn("true");

        this.uut.doPost(this.request, this.response);

        verify(this.request, times(1)).setAttribute("info",
                Variables.translate("generationSuccess"));
        @SuppressWarnings("rawtypes")
        final ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);
        verify(this.session, times(1)).setAttribute(eq("adminFilenames"), captor.capture());
        assertEquals(ImmutableMap.of(table, "fileName.tsv"), captor.getValue());
    }

    @Test
    @Parameters(method = "tables")
    @TestCaseName("testQueriesValidWithAllArguments_{0}")
    public void testQueriesValidWithAllArguments(final String table) throws Exception {

        when(this.request.getParameter("generate")).thenReturn("generate");
        when(this.request.getParameterValues("tables")).thenReturn(new String[] { table });
        when(this.request.getParameter("user")).thenReturn("1");
        when(this.request.getParameter("material")).thenReturn("4");
        when(this.request.getParameter("anonymous")).thenReturn("false");
        when(this.request.getParameter("after")).thenReturn("2018-01-01");
        when(this.request.getParameter("before")).thenReturn("2018-02-03");

        this.uut.doPost(this.request, this.response);

        verify(this.request, times(1)).setAttribute("info",
                Variables.translate("generationSuccess"));
        @SuppressWarnings("rawtypes")
        final ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);
        verify(this.session, times(1)).setAttribute(eq("adminFilenames"), captor.capture());
        assertEquals(ImmutableMap.of(table, "fileName.tsv"), captor.getValue());
    }

    @Test
    public void testCorrectAmountOfTables() {
        @SuppressWarnings("unchecked")
        final Map<String, ?> internalState =
                Whitebox.getInternalState(TSVFileGenerator.class, Map.class);
        // referrer and tradesDoneHistory have two queries. tradesdone is skipped
        // three more for lottery
        assertEquals(32 + 3 - 1 + 2, internalState.size());
    }

    @Test
    @Parameters(method = "queries")
    @TestCaseName("testIdContainedInQuery_{0}")
    public void testIdContainedInQuery(final Map.Entry<String, MysqlQuery> entry) {
        if (entry.getKey().startsWith("lottery")) {
            return;
        }
        assertTrue(
                entry.getValue().getRawQuery().contains(entry.getKey().replaceAll("[0-9]+", "")));
    }

    @Test
    public void testVerifiesId() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "time", "traderid", "type", "buysell", "material", "amount", "price",
                        "sold")
                .values(1, "2018-01-01", 3, true, true, 1, 2, 300000, 1)
                .values(2, "2018-01-02", 4, true, true, 1, 2, 300000, 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        final AtomicInteger calls = new AtomicInteger(0);

        final Answer<?> answer = invocation -> {
            final ResultSet rs = (ResultSet) invocation.getArguments()[1];
            assertTrue(rs.next());
            assertEquals(1L, rs.getInt("id"));
            assertFalse(rs.next());

            calls.incrementAndGet();

            return "fileName.tsv";
        };
        when(this.helper.resultSetToTsv(any(), any(), eq(null))).then(answer);

        when(this.request.getParameter("generate")).thenReturn("generate");
        when(this.request.getParameterValues("tables")).thenReturn(new String[] { "trade" });
        when(this.request.getParameter("user")).thenReturn("3");

        this.uut.doPost(this.request, this.response);

        assertEquals(1, calls.get());
    }

    public Object[] tables() {
        @SuppressWarnings("unchecked")
        final Map<String, ?> internalState =
                Whitebox.getInternalState(TSVFileGenerator.class, Map.class);
        return internalState.keySet().stream().map(e -> new Object[] { e })
                .collect(Collectors.toList()).toArray(new Object[internalState.size()]);
    }

    public Object[] queries() {
        @SuppressWarnings("unchecked")
        final Map<String, MysqlQuery> internalState =
                Whitebox.getInternalState(TSVFileGenerator.class, Map.class);
        return internalState.entrySet().toArray(new Map.Entry[internalState.size()]);
    }
}
