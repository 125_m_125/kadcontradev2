package de._125m125.ktv2.servlets.admin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.market.TradeManager;

public class UserAdminServletTest extends DatabaseTester {
    private UserAdminServlet uut;

    @Mock
    private HttpServletRequest  request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession         session;
    @Mock
    private RequestDispatcher   dispacher;

    @BeforeClass
    public static void beforeClassUserAdminServletTest() {
        Whitebox.setInternalState(Main.instance(), new TradeManager());
        Variables.ID_TO_DN.put("4", "Cobblestone");
    }

    @AfterClass
    public static void afterClassUserAdminServletTest() {
        Main.instance().getTradeManager().stop();
    }

    @Before
    public void beforeUserAdminServletTest() {
        MockitoAnnotations.initMocks(this);
        this.uut = new UserAdminServlet();

        when(this.request.getRequestURI()).thenReturn("/admin/payout");
        when(this.request.getRequestDispatcher(any())).thenReturn(this.dispacher);
        when(this.request.getSession()).thenReturn(this.session);

        final Operation insert = Insert.into("trade")
                .columns("id", "time", "traderid", "type", "buysell", "material", "amount", "price",
                        "sold")
                .values(1, "2017-01-01 00:00:00", 2, true, true, 4, 100, 1230000, 10).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert);
        dbSetup.launch();
    }

    @Test
    public void testDoPost_cancelOrder() throws Exception {
        when(this.request.getParameter("cancelOrder")).thenReturn("cancelorder");
        when(this.request.getParameter("id")).thenReturn("1");
        when(this.request.getParameter("uid")).thenReturn("2");
        when(this.session.getAttribute("id")).thenReturn(5L);
        when(this.request.getParameterValues("uid")).thenReturn(new String[] { "1" });

        this.uut.doPost(this.request, this.response);

        verify(this.request).setAttribute("info", Variables.translate("tradecancelsuccess"));

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNull(executeQuery);

        // final HashMap<String, Object> entry = executeQuery.get(0);
        // assertEquals(1L, entry.get("id"));
        // assertEquals(Timestamp.valueOf("2017-01-01 00:00:00"), entry.get("time"));
        // assertEquals(2L, entry.get("traderid"));
        // assertEquals(true, entry.get("type"));
        // assertEquals(true, entry.get("buysell"));
        // assertEquals("4", entry.get("material"));
        // assertEquals(100L, entry.get("amount"));
        // assertEquals(1230000L, entry.get("price"));
        // assertEquals(100L, entry.get("sold"));
        // assertEquals(110700000L, entry.get("toTake0"));
        // assertEquals(10, entry.get("toTake1"));
        // assertEquals(true, entry.get("cancelled"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM messages");
        assertEquals(1, executeQuery.size());

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals(110700000L, executeQuery.get(0).get("amount"));
        assertEquals("-1", executeQuery.get(0).get("matid"));
    }

    @Test
    @Ignore
    public void testDoPost_forcePayout() throws Exception {
        DatabaseTester.c
                .executeUpdate("INSERT INTO materials(id,type,matid,amount) VALUES(2,1,4,11)");

        when(this.request.getParameter("payout")).thenReturn("payout");
        when(this.request.getParameter("uid")).thenReturn("2");
        when(this.request.getParameter("matid")).thenReturn("4");
        when(this.request.getParameter("amount")).thenReturn("8");
        when(this.request.getParameter("message")).thenReturn("someMessage");
        when(this.session.getAttribute("id")).thenReturn(5L);
        when(this.request.getParameterValues("uid")).thenReturn(new String[] { "1" });

        this.uut.doPost(this.request, this.response);

        verify(this.request).setAttribute("info", Variables.translate("payoutsuccess"));

        ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM payout ORDER BY id");
        assertEquals(1, executeQuery.size());

        final HashMap<String, Object> entry = executeQuery.get(0);
        assertEquals(2L, entry.get("uid"));
        assertEquals("4", entry.get("matid"));
        assertEquals(8L, entry.get("amount"));
        assertEquals(false, entry.get("userInteraction"));
        assertEquals(false, entry.get("success"));
        assertEquals(false, entry.get("adminInteractionCompleted"));
        assertEquals(true, entry.get("requiresAdminInteraction"));
        assertEquals(false, entry.get("unknown"));
        assertEquals("lieferung", entry.get("payouType"));
        assertEquals("someMessage", entry.get("message"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM messages");
        assertEquals(1, executeQuery.size());
        assertEquals(2L, executeQuery.get(0).get("uid"));

        executeQuery = DatabaseTester.c.executeQuery("SELECT * FROM materials");
        assertEquals(2L, executeQuery.get(0).get("id"));
        assertEquals("4", executeQuery.get(0).get("matid"));
        assertEquals(3L, executeQuery.get(0).get("amount"));
    }

}
