package de._125m125.ktv2.servlets.admin;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;

public class PayoutAdminServletTest extends DatabaseTester {

    PayoutAdminServlet  uut;

    @Mock
    HttpSession         session;
    @Mock
    HttpServletRequest  request;
    @Mock
    HttpServletResponse response;
    @Mock
    RequestDispatcher   dispacher;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        this.uut = new PayoutAdminServlet();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(false)).thenReturn(this.session);
        when(this.request.getRequestURI()).thenReturn("/admin/payout");
        when(this.request.getRequestDispatcher(any())).thenReturn(this.dispacher);
        when(this.session.getAttribute("id")).thenReturn(1L);
    }

    @Test
    public void testDoPost_request_success() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent",
                        "payoutType")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5, "lieferung")
                .values(2, 2, 1, "1", 100, true, false, false, true, false, "2016-01-01 01:00:00", null, "lieferung")
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null, "lieferung")
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1, "lieferung")
                .values(5, 2, 1, "1", 100, true, false, false, true, false, "2016-01-01 04:00:00", null, "lieferung")
                .values(6, 2, 1, "1", 100, true, false, false, true, false, "2016-01-01 05:00:00", null, "box1")
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("request")).thenReturn("request");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(0)).setAttribute(eq("error"), any());
        verify(this.session, times(1)).setAttribute("info", Variables.translate("payoutRequestSuccess"));

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L },
                { 6L, 2L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 05:00:00"),
                        null, null }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(6, result.size());
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i][0], result.get(i).get("id"));
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testDoPost_request_alreadyRequested() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, true, false, false, true, false, "2016-01-01 04:00:00", null).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("request")).thenReturn("request");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(0)).setAttribute(eq("info"), any());
        verify(this.session, times(1)).setAttribute("error", Variables.translate("payoutsAlreadyAssigned"));

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, null }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i][0], result.get(i).get("id"));
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testPartialUpdate_invalidState() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("message")).thenReturn("message");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":25}");
        when(this.request.getParameter("state")).thenReturn("invalid");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("error", Variables.translate("invalidPayoutState"));
        verify(this.session, times(0)).setAttribute(eq("info"), any());

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i][0], result.get(i).get("id"));
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_messageTooLong() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("message")).thenReturn(
                "this message is too long this message is too long this message is too long this message is too long this message is too long this message is too long");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":25}");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("error", Variables.translate("messageTooLong"));
        verify(this.session, times(0)).setAttribute(eq("info"), any());

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i][0], result.get(i).get("id"));
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_missingState() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("update");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":25}");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("error", Variables.translate("invalidPayoutState"));
        verify(this.session, times(0)).setAttribute(eq("info"), any());

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i][0], result.get(i).get("id"));
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_noMessageSuccess() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("update");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":100,\"4\":100,\"5\":100}");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(0)).setAttribute(eq("error"), any());
        verify(this.session, times(1)).setAttribute("info", Variables.translate("payoutUpdateSuccess"));

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        "", 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        "", 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i][0], result.get(i).get("id"));
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testDoGet_invalidTime_231512() throws Exception {
        Whitebox.setInternalState(this.uut, Clock.class,
                Clock.fixed(Instant.ofEpochSecond(1451686512), ZoneId.systemDefault()));

        this.uut.doGet(this.request, this.response);

        verify(this.request, times(1)).setAttribute("error", Variables.translate("noPayoutsBetween23And1"));
        verify(this.request, times(1)).getRequestDispatcher("/empty.jsp");
    }

    @Test
    public void testDoGet_invalidTime_004514() throws Exception {
        Whitebox.setInternalState(this.uut, Clock.class,
                Clock.fixed(Instant.ofEpochSecond(1462056314), ZoneId.systemDefault()));

        this.uut.doGet(this.request, this.response);

        verify(this.request, times(1)).setAttribute("error", Variables.translate("noPayoutsBetween23And1"));
        verify(this.request, times(1)).getRequestDispatcher("/empty.jsp");
    }

    @Test
    public void testHandlePartialUpdate_success() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "payoutType", "date",
                        "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "lieferung", "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "lieferung", "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "lieferung", "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "lieferung", "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "lieferung", "2016-01-01 04:00:00", 1)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":25}");
        when(this.request.getParameter("message")).thenReturn("message");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(0)).setAttribute(eq("error"), any());
        verify(this.session, times(1)).setAttribute("info", Variables.translate("payoutUpdateSuccess"));

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, "lieferung",
                        Timestamp.valueOf("2016-01-01 00:00:00"), null, 5L },
                { 2L, 2L, true, "1", 100L, false, true, true, false, true, "lieferung",
                        Timestamp.valueOf("2016-01-01 01:00:00"), null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, "lieferung",
                        Timestamp.valueOf("2016-01-01 02:00:00"), null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, "lieferung",
                        Timestamp.valueOf("2016-01-01 03:00:00"), null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, "lieferung",
                        Timestamp.valueOf("2016-01-01 04:00:00"), null, 1L },
                { null, 2L, true, "1", 25L, false, true, true, false, false, "lieferung",
                        Timestamp.valueOf("2016-01-01 01:00:00"), "message", 1L },
                { null, 2L, true, "1", 75L, false, false, false, false, false, "lieferung",
                        Timestamp.valueOf("2016-01-01 01:00:00"), "message", 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(7, result.size());
        if ((long) result.get(6).get("amount") == 25L) {
            result.add(result.remove(5));
        }
        for (int i = 0; i < expected.length; i++) {
            if (expected[i][0] != null) {
                assertEquals(expected[i][0], result.get(i).get("id"));
            }
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("payoutType"));
            assertEquals(expected[i][11], result.get(i).get("date"));
            if (expected[i][12] != null) {
                assertEquals(expected[i][12], result.get(i).get("message"));
            }
            assertEquals(expected[i][13], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_negativeAmount() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":-25}");
        when(this.request.getParameter("message")).thenReturn("message");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("error", Variables.translate("invalidPartialPayoutUpdateRequest"));
        verify(this.session, times(0)).setAttribute(eq("info"), any());

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            if (expected[i][0] != null) {
                assertEquals(expected[i][0], result.get(i).get("id"));
            }
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_NaNAmount() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":\"test\"}");
        when(this.request.getParameter("message")).thenReturn("message");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("error", Variables.translate("invalidPartialPayoutUpdateRequest"));
        verify(this.session, times(0)).setAttribute(eq("info"), any());

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            if (expected[i][0] != null) {
                assertEquals(expected[i][0], result.get(i).get("id"));
            }
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_tooHighAmount() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":101}");
        when(this.request.getParameter("message")).thenReturn("message");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("error", Variables.translate("invalidPartialPayoutUpdateRequest"));
        verify(this.session, times(0)).setAttribute(eq("info"), any());

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            if (expected[i][0] != null) {
                assertEquals(expected[i][0], result.get(i).get("id"));
            }
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_success_full() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":100}");
        when(this.request.getParameter("message")).thenReturn("message");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(0)).setAttribute(eq("error"), any());
        verify(this.session, times(1)).setAttribute("info", Variables.translate("payoutUpdateSuccess"));

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        "message", 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            if (expected[i][0] != null) {
                assertEquals(expected[i][0], result.get(i).get("id"));
            }
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

    @Test
    public void testHandlePartialUpdate_success_0() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "date", "agent")
                .values(1, 3, 1, "1", 100, false, false, false, false, false, "2016-01-01 00:00:00", 5)
                .values(2, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 01:00:00", 1)
                .values(3, 4, 1, "1", 100, true, false, false, true, false, "2016-01-01 02:00:00", null)
                .values(4, 2, 1, "1", 100, false, true, true, false, false, "2016-01-01 03:00:00", 1)
                .values(5, 2, 1, "1", 100, false, false, false, false, false, "2016-01-01 04:00:00", 1).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("partialUpdate")).thenReturn("partialUpdate");
        when(this.request.getParameter("materials")).thenReturn("{\"2\":0}");
        when(this.request.getParameter("message")).thenReturn("message");
        when(this.request.getParameter("state")).thenReturn("SUCCESS");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(0)).setAttribute(eq("error"), any());
        verify(this.session, times(1)).setAttribute("info", Variables.translate("payoutUpdateSuccess"));

        final Object[][] expected = {
                { 1L, 3L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 00:00:00"),
                        null, 5L },
                { 2L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 01:00:00"),
                        null, 1L },
                { 3L, 4L, true, "1", 100L, true, false, false, true, false, Timestamp.valueOf("2016-01-01 02:00:00"),
                        null, null },
                { 4L, 2L, true, "1", 100L, false, true, true, false, false, Timestamp.valueOf("2016-01-01 03:00:00"),
                        null, 1L },
                { 5L, 2L, true, "1", 100L, false, false, false, false, false, Timestamp.valueOf("2016-01-01 04:00:00"),
                        null, 1L }, };

        final ArrayList<HashMap<String, Object>> result = DatabaseTester.c
                .executeQuery("SELECT * FROM payout ORDER BY id");
        assertNotNull(result);
        assertEquals(5, result.size());
        for (int i = 0; i < expected.length; i++) {
            if (expected[i][0] != null) {
                assertEquals(expected[i][0], result.get(i).get("id"));
            }
            assertEquals(expected[i][1], result.get(i).get("uid"));
            assertEquals(expected[i][2], result.get(i).get("type"));
            assertEquals(expected[i][3], result.get(i).get("matid"));
            assertEquals(expected[i][4], result.get(i).get("amount"));
            assertEquals(expected[i][5], result.get(i).get("userInteraction"));
            assertEquals(expected[i][6], result.get(i).get("success"));
            assertEquals(expected[i][7], result.get(i).get("adminInteractionCompleted"));
            assertEquals(expected[i][8], result.get(i).get("requiresAdminInteraction"));
            assertEquals(expected[i][9], result.get(i).get("unknown"));
            assertEquals(expected[i][10], result.get(i).get("date"));
            assertEquals(expected[i][11], result.get(i).get("message"));
            assertEquals(expected[i][12], result.get(i).get("agent"));
        }
    }

}
