package de._125m125.ktv2.servlets.apiv2;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class ItemRestAPITest extends DatabaseTester {

    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.put("-1", "Kadis(-1)");
        Variables.ID_TO_DN.put("1", "Stone(1)");
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    private ItemRestAPI        uut;
    private HttpServletRequest servletRequest;
    private Request            request;

    @Before
    public void before() {
        ItemRestAPI.invalidateAll();
        this.uut = new ItemRestAPI();

        this.servletRequest = mock(HttpServletRequest.class);
        Whitebox.setInternalState(this.uut, this.servletRequest);

        this.request = mock(Request.class);
    }

    @Test
    public void testGetItemList_empty() throws Exception {
        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getItemList("1", this.request);

        assertEquals(200, response.getStatus());
        assertEquals("id\tname\tamount\r\n-1\tKadis(-1)\t0.000000\r\n1\tStone(1)\t0\r\n4\tCobblestone(4)\t0\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetItemList() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount").values(1, 1, 1, 100)
                .values(1, 1, -1, 2000000).values(2, 1, 1, 200).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getItemList("1", this.request);

        assertEquals(200, response.getStatus());
        assertEquals("id\tname\tamount\r\n-1\tKadis(-1)\t2.000000\r\n1\tStone(1)\t100\r\n4\tCobblestone(4)\t0\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetItemList_decimal() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount").values(1, 1, 1, 100)
                .values(1, 1, -1, 2000000).values(2, 1, 1, 200).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getItemList("D1", this.request);

        assertEquals(200, response.getStatus());
        assertEquals("id\tname\tamount\r\n-1\tKadis(-1)\t2.000000\r\n1\tStone(1)\t100\r\n4\tCobblestone(4)\t0\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test(expected = ForbiddenException.class)
    public void testGetItemList_noPermissions() throws Exception {
        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_PERMISSIONS.getInteger() ^ Permission.READ_ITEMLIST_PERMISSION.getInteger());

        this.uut.getItemList("1", this.request);
    }

    @Test
    public void testGetItem() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount").values(1, 1, 1, 100)
                .values(1, 1, -1, 2000000).values(2, 1, 1, 200).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getItem("1", "4", this.request);

        assertEquals(200, response.getStatus());
        assertEquals("id\tname\tamount\r\n4\tCobblestone(4)\t0\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetItem_decimal() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount").values(1, 1, 1, 100)
                .values(1, 1, -1, 2000000).values(2, 1, 1, 200).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getItem("D1", "4", this.request);

        assertEquals(200, response.getStatus());
        assertEquals("id\tname\tamount\r\n4\tCobblestone(4)\t0\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test(expected = NotFoundException.class)
    public void testGetItem_notExisting() throws Exception {
        final Operation op = insertInto("materials").columns("id", "type", "matid", "amount").values(1, 1, 1, 100)
                .values(1, 1, -1, 2000000).values(2, 1, 1, 200).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        this.uut.getItem("1", "5", this.request);
    }

    @Test(expected = ForbiddenException.class)
    public void testGetItem_noPermissions() throws Exception {
        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_PERMISSIONS.getInteger() ^ Permission.READ_ITEMLIST_PERMISSION.getInteger());

        this.uut.getItem("1", "4", this.request);
    }
}
