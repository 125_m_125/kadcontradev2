package de._125m125.ktv2.servlets.apiv2;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.cache.Tsvable;

public class HistoryRestAPITest extends DatabaseTester {
    private HistoryRestAPI     uut;
    private HttpServletRequest servletRequest;
    private Request            request;

    @BeforeClass
    public static void beforeClass() {
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    @Before
    public void before() {
        HistoryRestAPI.invalidateAll();

        this.uut = new HistoryRestAPI();

        this.servletRequest = mock(HttpServletRequest.class);
        Whitebox.setInternalState(this.uut, this.servletRequest);

        this.request = mock(Request.class);
    }

    @Test
    public void testGetHistory() throws Exception {
        final LocalDate ld = LocalDate.now();
        Date d = Date.valueOf(ld);
        Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 3).values(d2, 1, 4, 11, 12, 4).values(d, 1, 3, 12, 13, 5)
                .values(d, 0, 4, 14, 15, 6).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final Response history = this.uut.getHistory("4", null, null, this.request);

        assertEquals(200, history.getStatus());
        d = Date.valueOf(ld.minusDays(1));
        d2 = Date.valueOf(ld.minusDays(2));
        assertEquals(
                "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n" + d.toString()
                        + "\t0.000011\t0.000110\t0.000100\t0.000010\t13\t0.000003\r\n"
                        + d2.toString() + "\t\t\t\t0.000011\t12\t0.000004\r\n",
                ((Tsvable) history.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetHistory_limit() throws Exception {
        final LocalDate ld = LocalDate.now();
        Date d = Date.valueOf(ld);
        Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 3).values(d2, 1, 4, 11, 12, 4).values(d, 1, 3, 12, 13, 5)
                .values(d, 0, 4, 14, 15, 6).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final Response history = this.uut.getHistory("4", 1, null, this.request);

        assertEquals(200, history.getStatus());
        d = Date.valueOf(ld.minusDays(1));
        d2 = Date.valueOf(ld.minusDays(2));
        assertEquals(
                "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n" + d.toString()
                        + "\t0.000011\t0.000110\t0.000100\t0.000010\t13\t0.000003\r\n",
                ((Tsvable) history.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetHistory_offset() throws Exception {
        final LocalDate ld = LocalDate.now();
        Date d = Date.valueOf(ld);
        Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 3).values(d2, 1, 4, 11, 12, 4).values(d, 1, 3, 12, 13, 5)
                .values(d, 0, 4, 14, 15, 6).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final Response history = this.uut.getHistory("4", 1, 1, this.request);

        assertEquals(200, history.getStatus());
        d = Date.valueOf(ld.minusDays(1));
        d2 = Date.valueOf(ld.minusDays(2));
        assertEquals(
                "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n" + d2.toString()
                        + "\t\t\t\t0.000011\t12\t0.000004\r\n",
                ((Tsvable) history.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetHistory_empty() throws Exception {
        final Response history = this.uut.getHistory("4", null, null, this.request);

        assertEquals(200, history.getStatus());
        assertEquals("date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n",
                ((Tsvable) history.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetHistory_negativeLimit() throws Exception {
        final LocalDate ld = LocalDate.now();
        final Date d = Date.valueOf(ld);
        final Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 3).values(d2, 1, 4, 11, 12, 4).values(d, 1, 3, 12, 13, 5)
                .values(d, 0, 4, 14, 15, 6).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final Response history = this.uut.getHistory("4", -5, null, this.request);

        assertEquals(200, history.getStatus());
        assertEquals("date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n",
                ((Tsvable) history.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetHistory_negativeOffset() throws Exception {
        final LocalDate ld = LocalDate.now();
        Date d = Date.valueOf(ld);
        Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 3).values(d2, 1, 4, 11, 12, 4).values(d, 1, 3, 12, 13, 5)
                .values(d, 0, 4, 14, 15, 6).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final Response history = this.uut.getHistory("4", null, -5, this.request);

        assertEquals(200, history.getStatus());
        d = Date.valueOf(ld.minusDays(1));
        d2 = Date.valueOf(ld.minusDays(2));
        assertEquals(
                "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n" + d.toString()
                        + "\t0.000011\t0.000110\t0.000100\t0.000010\t13\t0.000003\r\n"
                        + d2.toString() + "\t\t\t\t0.000011\t12\t0.000004\r\n",
                ((Tsvable) history.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetHistory_negativeOffsetLowLimit() throws Exception {
        final LocalDate ld = LocalDate.now();
        final Date d = Date.valueOf(ld);
        final Date d2 = Date.valueOf(ld.minusDays(1));
        final Operation op = Operations.insertInto("history")
                .columns("time", "type", "material", "value", "amount", "total")
                .values(d, 1, 4, 10, 13, 3).values(d2, 1, 4, 11, 12, 4).values(d, 1, 3, 12, 13, 5)
                .values(d, 0, 4, 14, 15, 6).build();
        final Operation op2 = Operations.insertInto("minmax")
                .columns("time", "type", "material", "min", "max").values(d, 1, 4, 100, 110)
                .values(d, 1, 3, 120, 130).values(d, 0, 4, 140, 150).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final Response history = this.uut.getHistory("4", 6, -5, this.request);

        assertEquals(200, history.getStatus());
        assertEquals(
                "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume\r\n" + d2.toString()
                        + "\t0.000011\t0.000110\t0.000100\t0.000010\t13\t0.000003\r\n",
                ((Tsvable) history.getEntity()).getTsvString(true));
    }

}
