package de._125m125.ktv2.servlets.apiv2;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Request;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class PermissionsRestAPITest {
    private Request            request;
    private HttpServletRequest servletRequest;
    private PermissionsRestAPI uut;

    @Before
    public void prepareRequestResponse() throws IOException {
        this.request = mock(Request.class);
        this.servletRequest = mock(HttpServletRequest.class);
        this.uut = new PermissionsRestAPI();

        Whitebox.setInternalState(this.uut, this.servletRequest);
    }

    @Test
    public void testLogin_success() throws Exception {
        when(this.servletRequest.getAttribute("permissions")).thenReturn(3);

        final Map<String, Boolean> result = this.uut.getCurrentPermissions("1", this.request);

        assertEquals(false, result.get("rPayouts"));
        assertEquals(false, result.get("rMessages"));
        assertEquals(true, result.get("rItems"));
        assertEquals(true, result.get("rOrders"));
        assertEquals(false, result.get("wPayouts"));
        assertEquals(false, result.get("wOrders"));
    }
}
