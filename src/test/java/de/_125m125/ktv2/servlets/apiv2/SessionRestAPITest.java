package de._125m125.ktv2.servlets.apiv2;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import de._125m125.ktv2.servlets.api.helper.Permission;

public class SessionRestAPITest {
    private SessionRestAPI     uut;
    private HttpServletRequest servletRequest;
    private Request            request;

    @Before
    public void before() {
        this.uut = new SessionRestAPI();

        this.servletRequest = mock(HttpServletRequest.class);
        Whitebox.setInternalState(this.uut, this.servletRequest);

        this.request = mock(Request.class);
    }

    @Test
    public void testLogin_success() {
        final HttpSession s = mock(HttpSession.class);

        when(this.servletRequest.getSession()).thenReturn(s);

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("tknid")).thenReturn(2L);
        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.READ_MESSAGES_PERMISSION.getInteger());

        final Response login = this.uut.login("1", true, this.request);

        verify(s).setAttribute("tbid", 1L);
        verify(s).setAttribute("tknid", 2L);
        verify(s).setAttribute("permissions", 8);
        verify(s).setAttribute("noCsrfProtection", false);

        assertEquals(200, login.getStatus());
        assertEquals(Permission.generateComPermissionMap(8), login.getEntity());
    }

    @Test(expected = WebApplicationException.class)
    public void testLogin_duplicate() {
        final HttpSession s = mock(HttpSession.class);
        when(this.servletRequest.getSession(false)).thenReturn(s);

        final Response login = this.uut.login("1", true, this.request);
    }

    @Test
    public void testLogout_success() {
        final HttpSession session = mock(HttpSession.class);
        when(this.servletRequest.getSession(false)).thenReturn(session);

        this.uut.logout();

        verify(session).invalidate();
    }

    @Test
    public void testLogout_noSession() {
        this.uut.logout();
    }
}
