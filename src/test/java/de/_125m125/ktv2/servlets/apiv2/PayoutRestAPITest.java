package de._125m125.ktv2.servlets.apiv2;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Payout;
import de._125m125.ktv2.beans.Payout.PAYOUT_STATE;
import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.servlets.api.helper.HelperResult;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class PayoutRestAPITest extends DatabaseTester {
    private PayoutRestAPI      uut;
    private HttpServletRequest servletRequest;
    private Request            request;

    @BeforeClass
    public static void beforeClassPayoutRestAPITest() {
        Variables.ID_TO_DN.clear();
        Variables.ID_TO_DN.put("1", "Stone(1)");
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    @Before
    public void before() {
        PayoutRestAPI.invalidateAll();
        this.uut = new PayoutRestAPI();

        this.servletRequest = mock(HttpServletRequest.class);
        Whitebox.setInternalState(this.uut, this.servletRequest);

        this.request = mock(Request.class);
    }

    @Test
    public void testGetPayoutList_empty() throws Exception {
        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.READ_PAYOUTS_PERMISSION.getInteger());

        final Response itemList = this.uut.getPayoutList("1", null, null, null, this.request);

        assertEquals(200, itemList.getStatus());
        assertEquals("id\tmaterial\tmaterialName\tamount\tstate\tpayoutType\tdate\tmessage\r\n",
                ((Tsvable) itemList.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetPayoutList() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "payoutType", "date",
                        "message")
                .values(1, 1, 1, 1, 1, true, false, false, true, false, "kadcon", "2016-01-01 01:23:45", "test")
                .values(2, 2, 1, 1, 1, true, false, false, true, false, "vkba", "2016-01-01 01:23:45", "test")
                .values(3, 1, 1, 2, 10, false, true, true, false, false, "vkba", "2016-02-01 01:24:45", "test").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.READ_PAYOUTS_PERMISSION.getInteger());

        final Response itemList = this.uut.getPayoutList("1", null, null, null, this.request);

        assertEquals(200, itemList.getStatus());
        assertEquals(
                "id\tmaterial\tmaterialName\tamount\tstate\tpayoutType\tdate\tmessage\r\n1\t1\tStone(1)\t1\tOffen\tkadcon\t"
                        + Timestamp.valueOf("2016-01-01 01:23:45.0").getTime()
                        + "\ttest\r\n3\t2\tnull\t10\tErfolgreich\tvkba\t"
                        + Timestamp.valueOf("2016-02-01 01:24:45.0").getTime() + "\ttest\r\n",
                ((Tsvable) itemList.getEntity()).getTsvString(true));
    }

    @Test(expected = ForbiddenException.class)
    public void testGetPayoutList_noPermissions() throws Exception {
        final Operation op = insertInto("payout")
                .columns("id", "uid", "type", "matid", "amount", "userInteraction", "success",
                        "adminInteractionCompleted", "requiresAdminInteraction", "unknown", "payoutType", "date",
                        "message")
                .values(1, 1, 1, 1, 1, true, false, false, true, false, "kadcon", "2016-01-01 01:23:45", "test")
                .values(2, 2, 1, 1, 1, true, false, false, true, false, "vkba", "2016-01-01 01:23:45", "test")
                .values(3, 1, 1, 2, 10, false, true, true, false, false, "vkba", "2016-02-01 01:24:45", "test").build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_PERMISSIONS.getInteger() ^ Permission.ALL_PERMISSIONS.getInteger());

        this.uut.getPayoutList("1", null, null, null, this.request);
    }

    @Test
    public void testCreatePayout_itemDelivery() throws Exception {
        final Operation op = sequenceOf(
                insertInto("materials").columns("id", "type", "matid", "amount").values("1", true, "4", 100).build(),
                insertInto("payoutsettings").columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6)
                        .build());
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.WRITE_PAYOUTS_PERMISSION.getInteger());

        final Response createPayout = this.uut.createPayout("1", "4", "10", "lieferung", this.request);

        try (Connection connection = DatabaseTester.c.getConnection();
                Statement createStatement = connection.createStatement();
                ResultSet rs = createStatement.executeQuery("SELECT * FROM materials")) {
            assertTrue(rs.next());
            assertEquals(1, rs.getInt(1));
            assertEquals(true, rs.getBoolean(2));
            assertEquals("4", rs.getString(3));
            assertEquals(90, rs.getInt(4));
        }

        final long id;
        final Timestamp d;
        try (Connection connection = DatabaseTester.c.getConnection();
                Statement createStatement = connection.createStatement();
                ResultSet rs = createStatement.executeQuery("SELECT * FROM payout")) {
            assertTrue(rs.next());
            id = rs.getLong(1);
            assertEquals(1, rs.getInt("uid"));
            assertEquals(true, rs.getBoolean("type"));
            assertEquals("4", rs.getString("matid"));
            assertEquals(10, rs.getInt("amount"));
            assertEquals(true, rs.getBoolean("userInteraction"));
            assertEquals(false, rs.getBoolean("success"));
            assertEquals(false, rs.getBoolean("adminInteractionCompleted"));
            assertEquals(true, rs.getBoolean("requiresAdminInteraction"));
            assertEquals(false, rs.getBoolean("unknown"));
            assertEquals("lieferung", rs.getString("payoutType"));
            d = rs.getTimestamp("date");
            assertEquals("", rs.getString("message"));
        }

        assertEquals(new HelperResult<>(true, 201, "payoutSuccess",
                new Payout(id, 1, "4", 10, PAYOUT_STATE.OPEN, "lieferung", d, "")), createPayout.getEntity());
    }

}
