package de._125m125.ktv2.servlets.apiv2;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.servlets.api.keys.OrderBookKey.MODE;

public class OrderBookRestAPITest extends DatabaseTester {

    private OrderBookRestAPI   uut;
    private HttpServletRequest servletRequest;
    private Request            request;

    @BeforeClass
    public static void beforeClassOrderRestAPITest() {
        Variables.ID_TO_DN.put("1", "Stone(1)");
        Variables.ID_TO_DN.put("4", "Cobblestone(4)");
    }

    @Before
    public void beforeOrderRestAPITest() {
        MessageRestAPI.invalidateAll();

        this.uut = new OrderBookRestAPI();

        this.servletRequest = mock(HttpServletRequest.class);
        Whitebox.setInternalState(this.uut, this.servletRequest);

        this.request = mock(Request.class);
    }

    @Test
    public void testGetOrderBook_empty() throws Exception {
        final Response orderBook = this.uut.getOrderBook("4", null, null, null, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals("type\tprice\tamount\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetOrderBook() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook = this.uut.getOrderBook("1", null, null, null, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals(
                "type\tprice\tamount\r\nbuy\t0.000019\t10\r\nbuy\t0.000020\t20\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetOrderBook_negativeAmount() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook = this.uut.getOrderBook("1", -2, null, null, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals("type\tprice\tamount\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetOrderBook_limit() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook = this.uut.getOrderBook("1", 2, false, null, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals(
                "type\tprice\tamount\r\nbuy\t0.000020\t20\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetOrderBook_summarize() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook = this.uut.getOrderBook("1", 1, true, null, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals(
                "type\tprice\tamount\r\nbuy\t<0.000021\t30\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetOrderBook_summarize_implied() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook = this.uut.getOrderBook("1", 1, null, null, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals(
                "type\tprice\tamount\r\nbuy\t<0.000021\t30\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetOrderBook_buyOnly() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook =
                this.uut.getOrderBook("1", null, null, MODE.BUY_ONLY, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals(
                "type\tprice\tamount\r\nbuy\t0.000019\t10\r\nbuy\t0.000020\t20\r\nbuy\t0.000021\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetOrderBook_sellOnly() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook =
                this.uut.getOrderBook("1", null, null, MODE.SELL_ONLY, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals("type\tprice\tamount\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test(expected = NotFoundException.class)
    public void testGetOrderBook_unknownMaterial() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        this.uut.getOrderBook("2", null, null, MODE.SELL_ONLY, this.request);
    }

    @Test
    public void testGetBestOrderBook() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook = this.uut.getBestOrderBook("1", this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals("type\tprice\tamount\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test(expected = NotFoundException.class)
    public void testGetBestOrderBook_unknownMaterial() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        this.uut.getBestOrderBook("2", this.request);
    }

    @Test
    public void testGetBestOrderBookByType_both() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook = this.uut.getBestOrderBookByType("1", MODE.BOTH, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals("type\tprice\tamount\r\nbuy\t0.000021\t10\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetBestOrderBookByType_buy() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook =
                this.uut.getBestOrderBookByType("1", MODE.BUY_ONLY, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals("type\tprice\tamount\r\nbuy\t0.000021\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetBestOrderBookByType_sell() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Response orderBook =
                this.uut.getBestOrderBookByType("1", MODE.SELL_ONLY, this.request);

        assertEquals(200, orderBook.getStatus());
        assertEquals("type\tprice\tamount\r\nsell\t0.000022\t10\r\n",
                ((Tsvable) orderBook.getEntity()).getTsvString(true));
    }

    @Test(expected = NotFoundException.class)
    public void testGetBestOrderBookByType_unknownMaterial() throws Exception {
        final Operation op = Operations.insertInto("trade")
                .columns("id", "type", "material", "buysell", "amount", "price", "sold")
                .values(1, 1, 1, 1, 10, 20, 0).values(2, 1, 1, 1, 11, 21, 1)
                .values(3, 1, 1, 1, 12, 19, 2).values(4, 1, 1, 1, 13, 20, 3)
                .values(7, 1, 1, 1, 13, 20, 13).values(5, 1, 1, 0, 14, 22, 4)
                .values(6, 1, 2, 0, 15, 30, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        this.uut.getBestOrderBookByType("2", MODE.BOTH, this.request);
    }
}
