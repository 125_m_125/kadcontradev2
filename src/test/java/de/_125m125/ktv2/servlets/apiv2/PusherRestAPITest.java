package de._125m125.ktv2.servlets.apiv2;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.locks.StampedLock;

import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.google.gson.Gson;
import com.pusher.rest.Pusher;
import com.pusher.rest.data.Result;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.ThreadManager;
import de._125m125.ktv2.beans.PusherResult;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.Permission;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PusherRestAPI.class)
public class PusherRestAPITest {
    private static final NotificationObservable no = mock(NotificationObservable.class);
    @Mock
    private HttpServletRequest                  servletrequest;
    @Mock
    private Request                             request;
    @Mock
    private Pusher                              pusher;

    private PusherRestAPI                       uut;

    @BeforeClass
    public static void beforeClass() throws UnavailableException {
        Whitebox.setInternalState(Main.class, new Main(false));
        Whitebox.setInternalState(Main.instance(), PusherRestAPITest.no);
        final ThreadManager tm = mock(ThreadManager.class);
        Whitebox.setInternalState(ThreadManager.class, tm);
        when(tm.submit(any())).then((iom) -> {
            iom.getArgumentAt(0, Runnable.class).run();
            return null;
        });
    }

    @AfterClass
    public static void afterClass() {
        Whitebox.setInternalState(Main.class, Main.class, (Main) null);
        Whitebox.setInternalState(ThreadManager.class, ThreadManager.class, (ThreadManager) null);
    }

    @Before
    public void before() throws IOException {
        Whitebox.setInternalState(PusherRestAPI.class, this.pusher);
        Whitebox.setInternalState(PusherRestAPI.class, "initialized", true);

        Whitebox.setInternalState(PusherRestAPI.class, new HashSet<>());
        Whitebox.setInternalState(PusherRestAPI.class, "channelSetExpiration", 0);
        Whitebox.setInternalState(PusherRestAPI.class, new StampedLock());

        this.uut = new PusherRestAPI();
        Whitebox.setInternalState(this.uut, this.servletrequest);

        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        when(this.pusher.authenticate("mySocket", "private-1_" + Permission.READ_ITEMLIST_PERMISSION.getInteger()))
                .thenReturn("success");
        when(this.pusher.authenticate("mySocket",
                "private-1_" + Permission.READ_ITEMLIST_PERMISSION.getInteger() + ".selfCreated"))
                        .thenReturn("success");
    }

    @Test(expected = BadRequestException.class)
    public void testAuthenticateSimple_nonprivateChannel() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "notPrivate", "mySocket", this.request);
        } catch (final BadRequestException e) {
            assertEquals("notPrivate", e.getMessage());
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void testAuthenticateSimple_nounderscoreInChannel() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "private-1", "mySocket", this.request);
        } catch (final BadRequestException e) {
            assertEquals("invalidChannelname. Format: private-<base32-uid>_<permission>[.selfCreated]", e.getMessage());
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void testAuthenticateSimple_noUid() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "private-_rItems", "mySocket", this.request);
        } catch (final BadRequestException e) {
            assertEquals("invalidChannelname. Format: private-<base32-uid>_<permission>[.selfCreated]", e.getMessage());
            throw e;
        }
    }

    @Test(expected = ForbiddenException.class)
    public void testAuthenticateSimple_usernamesNotMatching() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "private-2_rItems", "mySocket", this.request);
        } catch (final ForbiddenException e) {
            assertEquals("invalidBase32Userid", e.getMessage());
            throw e;
        }
    }

    @Test(expected = ForbiddenException.class)
    public void testAuthenticateSimple_invalidUsername() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "private-z_rItems", "mySocket", this.request);
        } catch (final ForbiddenException e) {
            assertEquals("invalidBase32Userid", e.getMessage());
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void testAuthenticateSimple_permissionNotExisting() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "private-1_rNonexisting", "mySocket", this.request);
        } catch (final BadRequestException e) {
            assertEquals("unknownPermission", e.getMessage());
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void testAuthenticateSimple_writePermission() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "private-1_wOrders", "mySocket", this.request);
        } catch (final BadRequestException e) {
            assertEquals("notReadPermission", e.getMessage());
            throw e;
        }
    }

    @Test(expected = ForbiddenException.class)
    public void testAuthenticateSimple_missingPermission() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.WRITE_ORDERS_PERMISSION.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        try {
            this.uut.authenticateSimple("1", "private-1_rItems", "mySocket", this.request);
        } catch (final ForbiddenException e) {
            assertEquals("permissionNotPresent", e.getMessage());
            throw e;
        }
    }

    @Test
    public void testAuthenticateSimple_success() throws Exception {
        when(this.servletrequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_NONADMIN_PERMISSIONS.getInteger());
        when(this.servletrequest.getAttribute("id")).thenReturn(1L);

        when(this.pusher.authenticate(any(), any())).thenReturn("success");

        final Response authenticateSimple = this.uut.authenticateSimple("1", "private-1_rItems", "mySocket",
                this.request);

        verify(this.pusher).authenticate("mySocket", "private-1_rItems");
        assertEquals(new PusherResult("success", "private-1_rItems"), authenticateSimple.getEntity());
    }

    @Test
    public void testUpdate_success() throws Exception {
        final Result requestResult = mock(Result.class);
        when(requestResult.getMessage()).thenReturn(
                "{\"channels\":{\"private-f_" + Permission.READ_MESSAGES_PERMISSION.getComName() + "\":{}}}");
        when(this.pusher.get("/channels")).thenReturn(requestResult);
        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "messages",
                Permission.READ_MESSAGES_PERMISSION);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher).trigger("private-f_" + Permission.READ_MESSAGES_PERMISSION.getComName(), "update",
                new Gson().toJson(ne));
    }

    @Test
    public void testUpdate_success_oldExpired() throws Exception {
        Whitebox.setInternalState(PusherRestAPI.class, new HashSet<>(
                Arrays.asList(new String[] { "private-f_" + Permission.READ_ITEMLIST_PERMISSION.getComName() })));
        Whitebox.setInternalState(PusherRestAPI.class, "channelSetExpiration", System.currentTimeMillis() - 1000);

        final Result requestResult = mock(Result.class);
        when(requestResult.getMessage()).thenReturn(
                "{\"channels\":{\"private-f_" + Permission.READ_MESSAGES_PERMISSION.getComName() + "\":{}}}");
        when(this.pusher.get("/channels")).thenReturn(requestResult);

        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "messages",
                Permission.READ_MESSAGES_PERMISSION);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher).trigger("private-f_" + Permission.READ_MESSAGES_PERMISSION.getComName(), "update",
                new Gson().toJson(ne));
    }

    @Test
    public void testUpdate_success_cached() throws Exception {
        Whitebox.setInternalState(PusherRestAPI.class, new HashSet<>(
                Arrays.asList(new String[] { "private-f_" + Permission.READ_MESSAGES_PERMISSION.getComName() })));
        Whitebox.setInternalState(PusherRestAPI.class, "channelSetExpiration", Long.MAX_VALUE);

        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "messages",
                Permission.READ_MESSAGES_PERMISSION);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher).trigger("private-f_" + Permission.READ_MESSAGES_PERMISSION.getComName(), "update",
                new Gson().toJson(ne));
    }

    @Test
    public void testUpdate_channelVacated() throws Exception {
        final Result requestResult = mock(Result.class);
        when(requestResult.getMessage()).thenReturn(
                "{\"channels\":{\"private-f_" + Permission.READ_ITEMLIST_PERMISSION.getComName() + "\":{}}}");
        when(this.pusher.get("/channels")).thenReturn(requestResult);
        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "messages",
                Permission.READ_MESSAGES_PERMISSION);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher, times(0)).trigger(anyString(), anyString(), any());
    }

    @Test
    public void testUpdate_channelVacated_cached() throws Exception {
        Whitebox.setInternalState(PusherRestAPI.class, new HashSet<>(
                Arrays.asList(new String[] { "private-f_" + Permission.READ_ITEMLIST_PERMISSION.getComName() })));
        Whitebox.setInternalState(PusherRestAPI.class, "channelSetExpiration", Long.MAX_VALUE);

        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "messages",
                Permission.READ_MESSAGES_PERMISSION);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher, times(0)).trigger(anyString(), anyString(), any());
    }

    @Test
    public void testUpdate_selfCreated() throws Exception {
        final Result requestResult = mock(Result.class);
        when(requestResult.getMessage()).thenReturn("{\"channels\":{\"private-f_"
                + Permission.READ_MESSAGES_PERMISSION.getComName() + ".selfCreated\":{}}}");
        when(this.pusher.get("/channels")).thenReturn(requestResult);

        final NotificationEvent ne = new NotificationEvent(true, 15, "update", "messages",
                Permission.READ_MESSAGES_PERMISSION);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher).trigger("private-f_" + Permission.READ_MESSAGES_PERMISSION.getComName() + ".selfCreated",
                "update", new Gson().toJson(ne));
    }

    @Test
    public void testUpdate_noNotificationEvent() throws Exception {
        final Object o = new Object();

        PusherRestAPI.update(PusherRestAPITest.no, o);

        verify(this.pusher, times(0)).trigger(any(String.class), any(String.class), any());
    }

    @Test
    public void testUpdate_invalidSource() throws Exception {
        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "messages",
                Permission.READ_MESSAGES_PERMISSION);
        final NotificationObservable no2 = mock(NotificationObservable.class);

        PusherRestAPI.update(no2, ne);
        verify(this.pusher, times(0)).trigger(any(String.class), any(String.class), any());

    }

    @Test
    public void testUpdate_publicSuccess() throws Exception {
        final Result requestResult = mock(Result.class);
        when(requestResult.getMessage()).thenReturn("{\"channels\":{\"order\":{}}}");
        when(this.pusher.get("/channels")).thenReturn(requestResult);
        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "order", "4",
                Permission.NO_PERMISSIONS);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher).trigger("order", "update", new Gson().toJson(ne));
    }

    @Test
    public void testUpdate_publicVacated() throws Exception {
        final Result requestResult = mock(Result.class);
        when(requestResult.getMessage()).thenReturn("{\"channels\":{}}");
        when(this.pusher.get("/channels")).thenReturn(requestResult);
        final NotificationEvent ne = new NotificationEvent(false, 15, "update", "order", "4",
                Permission.NO_PERMISSIONS);

        PusherRestAPI.update(PusherRestAPITest.no, ne);

        verify(this.pusher, times(0)).trigger(any(String.class), any(String.class), any());
    }

}
