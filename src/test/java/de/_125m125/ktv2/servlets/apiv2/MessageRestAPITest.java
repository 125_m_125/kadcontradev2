package de._125m125.ktv2.servlets.apiv2;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class MessageRestAPITest extends DatabaseTester {

    private MessageRestAPI     uut;
    private HttpServletRequest servletRequest;
    private Request            request;

    @Before
    public void before() {
        MessageRestAPI.invalidateAll();

        this.uut = new MessageRestAPI();

        this.servletRequest = mock(HttpServletRequest.class);
        Whitebox.setInternalState(this.uut, this.servletRequest);

        this.request = mock(Request.class);
    }

    @Test
    public void testGetMessages_empty() throws Exception {
        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getMessages("1", this.request, null, null);

        assertEquals(200, response.getStatus());
        assertEquals("timestamp\tmessage\r\n", ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetMessages() throws Exception {
        final Operation op = insertInto("messages").columns("uid", "message", "time")
                .values(1, "test1", "2017-01-01T00:00:00").values(2, "test2", "2017-02-01T10:02:00")
                .values(1, "test3", "2017-03-01T10:03:32").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getMessages("1", this.request, null, null);

        assertEquals(200, response.getStatus());
        assertEquals(
                "timestamp\tmessage\r\n" + Timestamp.valueOf("2017-03-01 10:03:32.0").getTime() + "\ttest3\r\n"
                        + Timestamp.valueOf("2017-01-01 00:00:00.0").getTime() + "\ttest1\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetMessages_limit() throws Exception {
        final Operation op = insertInto("messages").columns("uid", "message", "time")
                .values(1, "test1", "2017-01-01T00:00:00").values(2, "test2", "2017-02-01T10:02:00")
                .values(1, "test3", "2017-03-01T10:03:32").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getMessages("1", this.request, 1, null);

        assertEquals(200, response.getStatus());
        assertEquals("timestamp\tmessage\r\n" + Timestamp.valueOf("2017-03-01 10:03:32.0").getTime() + "\ttest3\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test
    public void testGetMessages_offset() throws Exception {
        final Operation op = insertInto("messages").columns("uid", "message", "time")
                .values(1, "test1", "2017-01-01T00:00:00").values(2, "test2", "2017-02-01T10:02:00")
                .values(1, "test3", "2017-03-01T10:03:32").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions")).thenReturn(Permission.ALL_PERMISSIONS.getInteger());

        final Response response = this.uut.getMessages("1", this.request, null, 1);

        assertEquals(200, response.getStatus());
        assertEquals("timestamp\tmessage\r\n" + Timestamp.valueOf("2017-01-01 00:00:00.0").getTime() + "\ttest1\r\n",
                ((Tsvable) response.getEntity()).getTsvString(true));
    }

    @Test(expected = ForbiddenException.class)
    public void testGetMessages_noPermission() throws Exception {
        final Operation op = insertInto("messages").columns("uid", "message", "time")
                .values(1, "test1", "2017-01-01T00:00:00").values(2, "test2", "2017-02-01T10:02:00")
                .values(1, "test3", "2017-03-01T10:03:32").build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        when(this.servletRequest.getAttribute("id")).thenReturn(1L);
        when(this.servletRequest.getAttribute("permissions"))
                .thenReturn(Permission.ALL_PERMISSIONS.getInteger() ^ Permission.READ_MESSAGES_PERMISSION.getInteger());

        this.uut.getMessages("1", this.request, null, 1);
    }

}
