package de._125m125.ktv2.servlets.api.helper;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Insert;

import de._125m125.ktv2.DatabaseTester;

public class APIAuthHelperTest extends DatabaseTester {

    @Test
    public void testLoginWithSignature() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "tkn", "permissions").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final String signature = "ad761bc1daa3ac1c337c7078297e8edfd46f0fcb253ab987d69f90235bde9acb";

        final LoginResult result = APIAuthHelper.login("1", "2", signature, "dataToSign");

        assertEquals(new LoginResult(1, 2, 4, "sig-2"), result);
    }

    @Test
    public void testLoginWithSignature_signatureNotMatching() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "tkn", "permissions").values(1, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final String signature = "ad761bc1daa3ac1c337c7078297e8edfd46f0fcb253ab987d69f90235bde9acc";

        final LoginResult result = APIAuthHelper.login("1", "2", signature, "dataToSign");

        assertEquals(new LoginResult(0, 0, 0, "none"), result);
    }

    @Test
    public void testLoginWithSignature_userNotExisting() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "tkn", "permissions").values(2, 2, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final String signature = "ad761bc1daa3ac1c337c7078297e8edfd46f0fcb253ab987d69f90235bde9acb";

        final LoginResult result = APIAuthHelper.login("1", "2", signature, "dataToSign");

        assertEquals(new LoginResult(0, 0, 0, "none"), result);
    }

    @Test
    public void testLoginWithSignature_tidNotExisting() throws Exception {
        final Insert insert = insertInto("token").columns("uid", "tid", "tkn", "permissions").values(1, 1, 3, 4)
                .build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), insert).launch();

        final String signature = "ad761bc1daa3ac1c337c7078297e8edfd46f0fcb253ab987d69f90235bde9acb";

        final LoginResult result = APIAuthHelper.login("1", "2", signature, "dataToSign");

        assertEquals(new LoginResult(0, 0, 0, "none"), result);
    }

}
