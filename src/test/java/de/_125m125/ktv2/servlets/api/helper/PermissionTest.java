package de._125m125.ktv2.servlets.api.helper;

import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class PermissionTest {
    @Test
    public void testNoDuplicateComNames() {
        final Set<String> permissionNames = new HashSet<>();
        for (final Permission p : Permission.values()) {
            if (permissionNames.contains(p.getComName())) {
                fail("duplicate: " + p.getComName());
            }
            permissionNames.add(p.getComName());
        }
    }

    @Test
    public void testNoDuplicateIds() {
        final Set<Integer> permissionIds = new HashSet<>();
        for (final Permission p : Permission.values()) {
            if (permissionIds.contains(p.getInteger())) {
                fail("duplicate: " + p.getInteger() + " at " + p.getComName());
            }
            permissionIds.add(p.getInteger());
        }
    }

    @Test
    public void testBasePermissionsArePowersOfTwo() {
        for (final Permission p : Permission.values()) {
            if (p.isUtility()) {
                continue;
            }
            final int val = p.getInteger();
            if (val == 0 || (val & (val - 1)) != 0) {
                fail("invalid: " + p + " with " + val);
            }
        }
    }

    public static void main(String[] args) {
        final Set<Integer> permissionIds = new HashSet<>();
        int free = 0b01111111111111111111111111111111;
        for (final Permission p : Permission.values()) {
            if (p.isUtility() || permissionIds.contains(p.getInteger())) {
                continue;
            }
            free ^= p.getInteger();
            permissionIds.add(p.getInteger());
        }
        System.out.println(
                String.format("0b%32s", Integer.toBinaryString(free)).replaceAll(" ", "0"));
        System.out.println(
                String.format("0b%32s", Integer.toBinaryString(Integer.highestOneBit(free)))
                        .replaceAll(" ", "0"));
        System.out
                .println(String.format("0b%32s", Integer.toBinaryString(Integer.lowestOneBit(free)))
                        .replaceAll(" ", "0"));
    }
}
