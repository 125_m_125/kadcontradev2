package de._125m125.ktv2.servlets.api.helper;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.servlets.api.helper.UserBasedSvgAPIServletWithPost;

@RunWith(MockitoJUnitRunner.class)
public class UserBasedSvgAPIServletWithPostTest {

    @Mock
    private HttpServletRequest                      request;

    @Mock
    private HttpServletResponse                     response;

    @Mock
    private LoadingCache<String, Tsvable>           cache;

    private final String                            filename        = "filename.csv";

    private final int                               readPermission  = 0b01;

    private final int                               writePermission = 0b10;

    private UserBasedSvgAPIServletWithPost<Tsvable> uut;

    @SuppressWarnings("unchecked")
    @Before
    public void before() throws IOException {
        this.uut = mock(UserBasedSvgAPIServletWithPost.class);
        doCallRealMethod().when(this.uut).doPost(any(), any());

        Whitebox.setInternalState(this.uut, LoadingCache.class, this.cache);
        Whitebox.setInternalState(this.uut, String.class, this.filename);
        Whitebox.setInternalState(this.uut, "permission", this.readPermission);
        Whitebox.setInternalState(this.uut, "writePermission", this.writePermission);

    }

    @Test
    public void testDoPost_missingPermissionAttribute() throws Exception {
        this.uut.doPost(this.request, this.response);

        verify(this.uut, times(0)).handlePost(any(), any());
        verify(this.response).sendError(401);
    }

    @Test
    public void testDoPost_wrongPermissions() throws Exception {
        when(this.request.getAttribute("permissions")).thenReturn(0b1101);

        this.uut.doPost(this.request, this.response);

        verify(this.uut, times(0)).handlePost(any(), any());
        verify(this.response).sendError(401);
    }

    @Test
    public void testDoPost_success() throws Exception {
        when(this.request.getAttribute("permissions")).thenReturn(0b10);
        this.uut.doPost(this.request, this.response);

        verify(this.uut, times(1)).handlePost(any(), any());
    }
}
