package de._125m125.ktv2.servlets;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.twoFactorAuth.U2FRegisterVerifier;
import de._125m125.ktv2.twoFactorAuth.Verifier;
import de._125m125.ktv2.twoFactorAuth.VerifierData;

@RunWith(MockitoJUnitRunner.class)
public class SettingsServletTest extends DatabaseTester {

    @Mock
    private RequestDispatcher   rd;
    @Mock
    private HttpSession         session;
    @Mock
    private HttpServletRequest  request;
    @Mock
    private HttpServletResponse response;
    private SettingsServlet     uut;

    @Before
    public void before() {
        this.uut = new SettingsServlet();

        when(this.request.getSession()).thenReturn(this.session);
        when(this.request.getSession(false)).thenReturn(this.session);
        when(this.session.getAttribute("id")).thenReturn(1L);
        when(this.request.getRequestDispatcher("/settings.jsp")).thenReturn(this.rd);
        when(this.request.getRequestURI()).thenReturn("/einstellungen");
    }

    @Test
    public void testDoPost_create() throws Exception {
        when(this.request.getParameter("newToken")).thenReturn("newToken");
        when(this.request.getParameter("rItems")).thenReturn("on");
        when(this.request.getParameter("wOrders")).thenReturn("on");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenCreationSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).get("uid"));
        assertNotNull(result.get(0).get("tid"));
        assertNotNull(result.get(0).get("tkn"));
        assertEquals(
                Permission.READ_ITEMLIST_PERMISSION.getInteger()
                        | Permission.WRITE_ORDERS_PERMISSION.getInteger(),
                result.get(0).get("permissions"));
    }

    @Test
    public void testDoPost_createWithAdminAsNonadmin() throws Exception {
        when(this.request.getParameter("newToken")).thenReturn("newToken");
        when(this.request.getParameter("rItems")).thenReturn("on");
        when(this.request.getParameter("wOrders")).thenReturn("on");
        when(this.request.getParameter("awItems")).thenReturn("on");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenCreationSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).get("uid"));
        assertNotNull(result.get(0).get("tid"));
        assertNotNull(result.get(0).get("tkn"));
        assertEquals(
                Permission.READ_ITEMLIST_PERMISSION.getInteger()
                        | Permission.WRITE_ORDERS_PERMISSION.getInteger(),
                result.get(0).get("permissions"));
    }

    @Test
    public void testDoPost_createWithAdminAsAdmin() throws Exception {
        when(this.request.getParameter("newToken")).thenReturn("newToken");
        when(this.request.getParameter("rItems")).thenReturn("on");
        when(this.request.getParameter("wOrders")).thenReturn("on");
        when(this.request.getParameter("awItems")).thenReturn("on");
        when(this.session.getAttribute("admin")).thenReturn(true);

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenCreationSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).get("uid"));
        assertNotNull(result.get(0).get("tid"));
        assertNotNull(result.get(0).get("tkn"));
        assertEquals(
                Permission.READ_ITEMLIST_PERMISSION.getInteger()
                        | Permission.WRITE_ORDERS_PERMISSION.getInteger()
                        | Permission.WRITE_ADMIN_USERITEMS_PERMISSION.getInteger(),
                result.get(0).get("permissions"));
    }

    @Test
    public void testDoPost_update() throws Exception {
        final Operation op = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1, 2, 3, 4).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("editToken")).thenReturn("editToken");
        when(this.request.getParameter("tid")).thenReturn("2");
        when(this.request.getParameter("rItems")).thenReturn("on");
        when(this.request.getParameter("wPayouts")).thenReturn("on");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenUpdateSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).get("uid"));
        assertEquals(2L, result.get(0).get("tid"));
        assertEquals(BigInteger.valueOf(4), result.get(0).get("tkn"));
        assertEquals(
                Permission.READ_ITEMLIST_PERMISSION.getInteger()
                        | Permission.WRITE_PAYOUTS_PERMISSION.getInteger(),
                result.get(0).get("permissions"));
    }

    @Test
    public void testDoPost_updateAdminAsNonadmin() throws Exception {
        final Operation op = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1, 2, 3, 4).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("editToken")).thenReturn("editToken");
        when(this.request.getParameter("tid")).thenReturn("2");
        when(this.request.getParameter("rItems")).thenReturn("on");
        when(this.request.getParameter("wPayouts")).thenReturn("on");
        when(this.request.getParameter("awItems")).thenReturn("on");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenUpdateSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).get("uid"));
        assertEquals(2L, result.get(0).get("tid"));
        assertEquals(BigInteger.valueOf(4), result.get(0).get("tkn"));
        assertEquals(
                Permission.READ_ITEMLIST_PERMISSION.getInteger()
                        | Permission.WRITE_PAYOUTS_PERMISSION.getInteger(),
                result.get(0).get("permissions"));
    }

    @Test
    public void testDoPost_updateAdminAsAdmin() throws Exception {
        final Operation op = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1, 2, 3, 4).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("editToken")).thenReturn("editToken");
        when(this.request.getParameter("tid")).thenReturn("2");
        when(this.request.getParameter("rItems")).thenReturn("on");
        when(this.request.getParameter("wPayouts")).thenReturn("on");
        when(this.request.getParameter("awItems")).thenReturn("on");
        when(this.session.getAttribute("admin")).thenReturn(true);

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenUpdateSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).get("uid"));
        assertEquals(2L, result.get(0).get("tid"));
        assertEquals(BigInteger.valueOf(4), result.get(0).get("tkn"));
        assertEquals(
                Permission.READ_ITEMLIST_PERMISSION.getInteger()
                        | Permission.WRITE_PAYOUTS_PERMISSION.getInteger()
                        | Permission.WRITE_ADMIN_USERITEMS_PERMISSION.getInteger(),
                result.get(0).get("permissions"));
    }

    @Test
    public void testDoPost_delete() throws Exception {
        final Operation op = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1, 2, 3, 4).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("editToken")).thenReturn("editToken");
        when(this.request.getParameter("tid")).thenReturn("2");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenRemoveSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertTrue(result == null || result.size() == 0);
    }

    @Test
    public void testDoPost_delete_longToken() throws Exception {
        final Operation op = insertInto("token").columns("uid", "tid", "permissions", "tkn")
                .values(1, 2125072798, 3, 5379801186794422809L).build();
        new DbSetup(new DataSourceDestination(DatabaseTester.ds), op).launch();

        when(this.request.getParameter("editToken")).thenReturn("editToken");
        when(this.request.getParameter("tid")).thenReturn("1vak2cu");

        this.uut.doPost(this.request, this.response);

        verify(this.session, times(1)).setAttribute("info",
                Variables.translate("tokenRemoveSuccess"));

        final ArrayList<HashMap<String, Object>> result =
                DatabaseTester.c.executeQuery("SELECT * FROM token ORDER BY uid,tid");
        assertTrue(result == null || result.size() == 0);
    }

    @Test
    public void testChangepayoutSettings_success() throws Exception {
        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("server")).thenReturn("2");
        when(request.getParameter("x")).thenReturn("3");
        when(request.getParameter("y")).thenReturn("4");
        when(request.getParameter("z")).thenReturn("5");
        when(request.getParameter("warp")).thenReturn("6");

        Whitebox.invokeMethod(new SettingsServlet(), "changePayoutSettings", request);

        verify(session).setAttribute("info", Variables.translate("settingsSuccess"));

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM payoutsettings ORDER BY uid");
        assertNotNull(executeQuery == null);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(2, executeQuery.get(0).get("server"));
        assertEquals(3, executeQuery.get(0).get("x"));
        assertEquals(4, executeQuery.get(0).get("y"));
        assertEquals(5, executeQuery.get(0).get("z"));
        assertEquals("6", executeQuery.get(0).get("warp"));
    }

    @Test
    public void testChangepayoutSettings_success_replace() throws Exception {
        final Operation operation = sequenceOf(insertInto("payoutsettings")
                .columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("server")).thenReturn("3");
        when(request.getParameter("x")).thenReturn("-100");
        when(request.getParameter("y")).thenReturn("97");
        when(request.getParameter("z")).thenReturn("-1234");
        when(request.getParameter("warp")).thenReturn("' or '1'='1");

        Whitebox.invokeMethod(new SettingsServlet(), "changePayoutSettings", request);

        verify(session).setAttribute("info", Variables.translate("settingsSuccess"));

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM payoutsettings ORDER BY uid");
        assertNotNull(executeQuery == null);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(3, executeQuery.get(0).get("server"));
        assertEquals(-100, executeQuery.get(0).get("x"));
        assertEquals(97, executeQuery.get(0).get("y"));
        assertEquals(-1234, executeQuery.get(0).get("z"));
        assertEquals("' or '1'='1", executeQuery.get(0).get("warp"));
    }

    @Test
    public void testChangepayoutSettings_invalidServer() throws Exception {
        final Operation operation = sequenceOf(insertInto("payoutsettings")
                .columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("server")).thenReturn("5");
        when(request.getParameter("x")).thenReturn("-100");
        when(request.getParameter("y")).thenReturn("97");
        when(request.getParameter("z")).thenReturn("-1234");
        when(request.getParameter("warp")).thenReturn("' or '1'='1");

        Whitebox.invokeMethod(new SettingsServlet(), "changePayoutSettings", request);

        verify(session).setAttribute("error", Variables.translate("invalidServer"));

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM payoutsettings ORDER BY uid");
        assertNotNull(executeQuery == null);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(2, executeQuery.get(0).get("server"));
        assertEquals(3, executeQuery.get(0).get("x"));
        assertEquals(4, executeQuery.get(0).get("y"));
        assertEquals(5, executeQuery.get(0).get("z"));
        assertEquals("6", executeQuery.get(0).get("warp"));
    }

    @Test
    public void testChangepayoutSettings_invalidKoordinate() throws Exception {
        final Operation operation = sequenceOf(insertInto("payoutsettings")
                .columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("server")).thenReturn("3");
        when(request.getParameter("x")).thenReturn("asdf");
        when(request.getParameter("y")).thenReturn("97");
        when(request.getParameter("z")).thenReturn("-1234");
        when(request.getParameter("warp")).thenReturn("' or '1'='1");

        Whitebox.invokeMethod(new SettingsServlet(), "changePayoutSettings", request);

        verify(session).setAttribute("error", Variables.translate("invalidX"));

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM payoutsettings ORDER BY uid");
        assertNotNull(executeQuery == null);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(2, executeQuery.get(0).get("server"));
        assertEquals(3, executeQuery.get(0).get("x"));
        assertEquals(4, executeQuery.get(0).get("y"));
        assertEquals(5, executeQuery.get(0).get("z"));
        assertEquals("6", executeQuery.get(0).get("warp"));
    }

    @Test
    public void testChangepayoutSettings_koordinateOutOfRange() throws Exception {
        final Operation operation = sequenceOf(insertInto("payoutsettings")
                .columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("server")).thenReturn("3");
        when(request.getParameter("x")).thenReturn("-100");
        when(request.getParameter("y")).thenReturn("266");
        when(request.getParameter("z")).thenReturn("-1234");
        when(request.getParameter("warp")).thenReturn("' or '1'='1");

        Whitebox.invokeMethod(new SettingsServlet(), "changePayoutSettings", request);

        verify(session).setAttribute("error", Variables.translate("invalidY"));

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM payoutsettings ORDER BY uid");
        assertNotNull(executeQuery == null);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(2, executeQuery.get(0).get("server"));
        assertEquals(3, executeQuery.get(0).get("x"));
        assertEquals(4, executeQuery.get(0).get("y"));
        assertEquals(5, executeQuery.get(0).get("z"));
        assertEquals("6", executeQuery.get(0).get("warp"));
    }

    @Test
    public void testChangepayoutSettings_warpTooLong() throws Exception {
        final Operation operation = sequenceOf(insertInto("payoutsettings")
                .columns("uid", "server", "x", "y", "z", "warp").values(1, 2, 3, 4, 5, 6).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("id")).thenReturn(1L);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("server")).thenReturn("3");
        when(request.getParameter("x")).thenReturn("-100");
        when(request.getParameter("y")).thenReturn("97");
        when(request.getParameter("z")).thenReturn("-1234");
        when(request.getParameter("warp")).thenReturn("12345678901234567890");

        Whitebox.invokeMethod(new SettingsServlet(), "changePayoutSettings", request);

        verify(session).setAttribute("error", Variables.translate("invalidWarp"));

        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM payoutsettings ORDER BY uid");
        assertNotNull(executeQuery == null);
        assertEquals(1, executeQuery.size());
        assertEquals(1L, executeQuery.get(0).get("uid"));
        assertEquals(2, executeQuery.get(0).get("server"));
        assertEquals(3, executeQuery.get(0).get("x"));
        assertEquals(4, executeQuery.get(0).get("y"));
        assertEquals(5, executeQuery.get(0).get("z"));
        assertEquals("6", executeQuery.get(0).get("warp"));
    }

    @Test
    public void testStore2Fa_removesOldIps() throws Exception {
        final Operation operation = sequenceOf(insertInto("twoFactorIP").columns("id", "ip")
                .values(1, "1").values(2, "2").build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final Verifier verifier = mock(Verifier.class);
        final VerifierData data = mock(VerifierData.class);
        when(verifier.verify(any(), anyBoolean())).thenReturn(true);
        when(verifier.getData()).thenReturn(data);

        when(this.session.getAttribute("2faVerifierRequest")).thenReturn(verifier);

        when(this.request.getParameter("verify2faData")).thenReturn("verify");

        final ServletOutputStream os = mock(ServletOutputStream.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getOutputStream()).thenReturn(os);

        new SettingsServlet().doPost(this.request, response);

        try (Connection con = DatabaseTester.c.getConnection();
                Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM twoFactorIP ORDER BY id")) {
                assertTrue(rs.next());
                assertEquals(2L, rs.getLong("id"));
                assertEquals("2", rs.getString("ip"));
                assertFalse(rs.next());
            }
        }
    }

    @Test
    public void testStore2Fa_removesOldCookie() throws Exception {
        final Operation operation =
                sequenceOf(insertInto("twoFactorCookie").columns("id", "cookie").values(1, "1")
                        .values(2, "2222222222222222".getBytes(StandardCharsets.UTF_8)).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        final Verifier verifier = mock(Verifier.class);
        final VerifierData data = mock(VerifierData.class);
        when(verifier.verify(any(), anyBoolean())).thenReturn(true);
        when(verifier.getData()).thenReturn(data);

        when(this.session.getAttribute("2faVerifierRequest")).thenReturn(verifier);

        when(this.request.getParameter("verify2faData")).thenReturn("verify");

        final ServletOutputStream os = mock(ServletOutputStream.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getOutputStream()).thenReturn(os);

        new SettingsServlet().doPost(this.request, response);

        try (Connection con = DatabaseTester.c.getConnection();
                Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM twoFactorCookie ORDER BY id")) {
                assertTrue(rs.next());
                assertEquals(2L, rs.getLong("id"));
                assertEquals("2222222222222222",
                        new String(rs.getBytes("cookie"), StandardCharsets.UTF_8));
                assertFalse(rs.next());
            }
        }
    }

    @Test
    public void testRequestPOBox_success() throws Exception {
        final Operation operation = insertInto("payoutboxes")
                .columns("cid", "server", "x", "y", "z", "owner", "verified")
                .values(1, 1, 2, 3, 4, null, false).values(2, 2, 3, 4, 5, 1, false).build();
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        when(this.request.getParameter("requestPOBox")).thenReturn("requestPOBox");
        when(this.request.getParameter("server")).thenReturn("1");

        this.uut.doPost(this.request, this.response);

        try (Connection con = DatabaseTester.c.getConnection();
                Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM payoutboxes ORDER BY cid")) {
                assertTrue(rs.next());
                assertEquals(1L, rs.getLong("cid"));
                assertEquals(1L, rs.getLong("owner"));
                assertEquals(false, rs.getBoolean("verified"));

                assertTrue(rs.next());
                assertEquals(2L, rs.getLong("cid"));
                assertEquals(1L, rs.getObject("owner"));
                assertEquals(false, rs.getBoolean("verified"));

                assertFalse(rs.next());
            }
        }
        verify(this.request).setAttribute("info", Variables.translate("poboxRequestSuccess"));
    }

    @Test
    public void testRequestPOBox_noFree() throws Exception {
        final Operation operation = insertInto("payoutboxes")
                .columns("cid", "server", "x", "y", "z", "owner", "verified")
                .values(1, 1, 2, 3, 4, null, false).values(2, 2, 3, 4, 5, 1, false).build();
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        when(this.request.getParameter("requestPOBox")).thenReturn("requestPOBox");
        when(this.request.getParameter("server")).thenReturn("3");

        this.uut.doPost(this.request, this.response);

        try (Connection con = DatabaseTester.c.getConnection();
                Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM payoutboxes ORDER BY cid")) {
                assertTrue(rs.next());
                assertEquals(1L, rs.getLong("cid"));
                assertEquals(null, rs.getObject("owner"));
                assertEquals(false, rs.getBoolean("verified"));

                assertTrue(rs.next());
                assertEquals(2L, rs.getLong("cid"));
                assertEquals(1L, rs.getObject("owner"));
                assertEquals(false, rs.getBoolean("verified"));

                assertFalse(rs.next());
            }
        }
        verify(this.request).setAttribute("error", Variables.translate("noFreeBox"));
    }

    @Test
    public void testRequestPOBox_alreadyOwned() throws Exception {
        final Operation operation = insertInto("payoutboxes")
                .columns("cid", "server", "x", "y", "z", "owner", "verified")
                .values(1, 1, 2, 3, 4, null, false).values(2, 2, 3, 4, 5, 1, false).build();
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        when(this.request.getParameter("requestPOBox")).thenReturn("requestPOBox");
        when(this.request.getParameter("server")).thenReturn("2");

        this.uut.doPost(this.request, this.response);

        try (Connection con = DatabaseTester.c.getConnection();
                Statement st = con.createStatement()) {
            try (ResultSet rs = st.executeQuery("SELECT * FROM payoutboxes ORDER BY cid")) {
                assertTrue(rs.next());
                assertEquals(1L, rs.getLong("cid"));
                assertEquals(null, rs.getObject("owner"));
                assertEquals(false, rs.getBoolean("verified"));

                assertTrue(rs.next());
                assertEquals(2L, rs.getLong("cid"));
                assertEquals(1L, rs.getObject("owner"));
                assertEquals(false, rs.getBoolean("verified"));

                assertFalse(rs.next());
            }
        }
        verify(this.request).setAttribute("error",
                Variables.translate("poboxAlreadyOwnedForServer"));
    }

    @Test
    public void testCanRequestRegistration() throws Exception {
        when(this.request.getParameter("request2fa")).thenReturn("request2fa");
        when(this.request.getParameter("type")).thenReturn("U2F");
        when(this.response.getOutputStream()).thenReturn(mock(ServletOutputStream.class));

        this.uut.doPost(this.request, this.response);

        verify(this.session).setAttribute(eq("2faVerifierRequest"), any(U2FRegisterVerifier.class));
    }

    @Test
    public void testGrantPL_success() throws Exception {
        when(this.request.getParameter("modifyGdpr")).thenReturn("1");
        when(this.request.getParameter("grant")).thenReturn("PL");

        this.uut.doPost(this.request, this.response);

        verify(this.request, times(0)).setAttribute(eq("error"), any());
        verify(this.request).setAttribute("info", Variables.translate("grantSuccess"));
        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM gdpr");
        assertNotNull(executeQuery);
        assertEquals(1L, executeQuery.get(0).get("user"));
        assertEquals("PL", executeQuery.get(0).get("type"));
    }

    @Test
    public void testRevokePL_success() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("gdpr").columns("user", "type").values("1", "PL").build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "-2", 100).build(),
                insertInto("trade")
                        .columns("traderid", "buysell", "material", "amount", "price", "sold")
                        .values(2, false, -2, 10, 20, 10).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        when(this.request.getParameter("modifyGdpr")).thenReturn("1");
        when(this.request.getParameter("revoke")).thenReturn("PL");

        this.uut.doPost(this.request, this.response);

        verify(this.request, times(0)).setAttribute(eq("error"), any());
        verify(this.request).setAttribute("info", Variables.translate("revokeSuccess"));
        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM gdpr");
        assertNull(executeQuery);
    }

    @Test
    public void testRevokePL_itemsInInventory() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("gdpr").columns("user", "type").values("1", "PL").build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, "-2", 100).build(),
                insertInto("trade")
                        .columns("traderid", "buysell", "material", "amount", "price", "sold")
                        .values(2, true, -2, 10, 20, 10).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        when(this.request.getParameter("modifyGdpr")).thenReturn("1");
        when(this.request.getParameter("revoke")).thenReturn("PL");

        this.uut.doPost(this.request, this.response);

        verify(this.request, times(0)).setAttribute(eq("info"), any());
        verify(this.request).setAttribute("error", Variables.translate("lotteryItemsInInventory"));
        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM gdpr");
        assertEquals(1, executeQuery.size());
    }

    @Test
    public void testRevokePL_itemsInOrders() throws Exception {
        final Operation operation = sequenceOf(
                insertInto("gdpr").columns("user", "type").values("1", "PL").build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "-2", 100).build(),
                insertInto("trade")
                        .columns("traderid", "buysell", "material", "amount", "price", "sold")
                        .values(1, true, -2, 10, 20, 10).build());
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(DatabaseTester.ds), operation);
        dbSetup.launch();

        when(this.request.getParameter("modifyGdpr")).thenReturn("1");
        when(this.request.getParameter("revoke")).thenReturn("PL");

        this.uut.doPost(this.request, this.response);

        verify(this.request, times(0)).setAttribute(eq("info"), any());
        verify(this.request).setAttribute("error", Variables.translate("lotteryItemsInInventory"));
        final ArrayList<HashMap<String, Object>> executeQuery =
                DatabaseTester.c.executeQuery("SELECT * FROM gdpr");
        assertEquals(1, executeQuery.size());
    }

}
