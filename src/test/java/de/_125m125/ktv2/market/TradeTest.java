package de._125m125.ktv2.market;

import static com.ninja_squad.dbsetup.Operations.insertInto;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.DatabaseTester;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.market.Trade.buySell;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

@RunWith(JUnitParamsRunner.class)
public class TradeTest extends DatabaseTester {

    @Test
    @Parameters
    @TestCaseName("testCkeckResources_{0}")
    public void testCheckResources(final String name, final Operation op, final Trade uut,
            final boolean result) {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();
        assertEquals(result, uut.checkResources());
    }

    public Object[] parametersForTestCheckResources() {
        return new Object[][] { { "buyFail_noEntry",
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values("2", true, "-1", "1000").values("1", true, "2", "10000").build(),
                new Trade(1, 10, buySell.BUY, "17", 1, 100, 0, ITEM_OR_STOCK.STOCK), false }, // buyFail_noEntry
                { "buyItemFail",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", true, "-1", "999").build(),
                        new Trade(1, 10, buySell.BUY, "1", 1, 100, 0, ITEM_OR_STOCK.ITEM), false }, // buyItemFail
                { "buyItemSuccess",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", true, "-1", "100000").build(),
                        new Trade(1, 10, buySell.BUY, "1", 1, 100, 0, ITEM_OR_STOCK.ITEM), true }, // buyItemSuccess
                { "buyStockFail",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", true, "-1", "999").build(),
                        new Trade(1, 10, buySell.BUY, "17", 1, 100, 0, ITEM_OR_STOCK.STOCK),
                        false }, // buyStockFail
                { "buyStockSuccess",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", true, "-1", "1000").build(),
                        new Trade(1, 10, buySell.BUY, "17", 1, 100, 0, ITEM_OR_STOCK.STOCK), true }, // buyStockSuccess
                { "sellItemFail",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", true, "1", "9").build(),
                        new Trade(1, 10, buySell.SELL, "1", 1, 100, 0, ITEM_OR_STOCK.ITEM), false }, // sellItemFail
                { "sellItemFail_noEntry",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("2", true, "-1", "1000").values("1", true, "2", "10000")
                                .build(),
                        new Trade(1, 10, buySell.BUY, "3", 1, 100, 0, ITEM_OR_STOCK.ITEM), false }, // sellItemFail_noEntry
                { "sellItemSuccess",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", true, "1", "100000").build(),
                        new Trade(1, 100, buySell.SELL, "1", 1, 100, 0, ITEM_OR_STOCK.ITEM), true }, // sellItemSuccess
                { "sellStockFail",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", false, "17", "999").build(),
                        new Trade(1, 1000, buySell.SELL, "17", 1, 10, 0, ITEM_OR_STOCK.STOCK),
                        false }, // sellStockFail
                { "sellStockFail_noEntry",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("2", false, "0", "1000").values("1", false, "17", "10000")
                                .build(),
                        new Trade(1, 10, buySell.BUY, "17", 1, 100, 0, ITEM_OR_STOCK.STOCK),
                        false }, // sellStockFail_noEntry
                { "sellStockSuccess",
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values("1", false, "17", "100").build(),
                        new Trade(1, 100, buySell.SELL, "17", 1, 110, 0, ITEM_OR_STOCK.STOCK),
                        true } // sellStockSuccess
        };
    }

    @Test
    @Parameters
    @TestCaseName("testTrade_{0}")
    public void testTrade(final String testname, final Trade trade, final int amount,
            final int realPrice, final long expectedSold, final long[] expectedResult)
            throws Exception {
        try (Connection c = DatabaseTester.c.getConnection()) {
            trade.trade(c, amount, realPrice, (long) Math.ceil(amount * realPrice * 0.002));
        }

        assertEquals(expectedSold, trade.getSold());
        assertArrayEquals(expectedResult,
                new long[] { trade.getMoneyUpdate(), trade.getItemUpdate() });
    }

    public Object[] parametersForTestTrade() {
        return new Object[][] {
                { "buy_exactMatch",
                        new Trade(1, 100, buySell.BUY, "17", 1, 1000000, 0, ITEM_OR_STOCK.STOCK),
                        10, 1000000, 10L, new long[] { (-20000), 10 } }, // buy_exactMatch
                { "buy_differentInt",
                        new Trade(1, 20, buySell.BUY, "17", 1, 2000000, 10, ITEM_OR_STOCK.STOCK), 1,
                        1500000, 11, new long[] { 497000, 1 } }, // buy_differentInt
                { "buy_differentDouble",
                        new Trade(1, 10, buySell.BUY, "17", 1, 20000, 0, ITEM_OR_STOCK.STOCK), 1,
                        15000, 1, new long[] { 4970, 1 } }, // buy_differentDouble
                { "sell_exactMatch",
                        new Trade(1, 100, buySell.SELL, "17", 1, 1000000, 0, ITEM_OR_STOCK.STOCK),
                        10, 1000000, 10, new long[] { (9980000), 0 } }, // sell_exactMatch
                { "sell_differentInt",
                        new Trade(1, 10, buySell.SELL, "17", 1, 1000000, 1, ITEM_OR_STOCK.STOCK), 1,
                        2000000, 2, new long[] { 1996000, 0 } }, // sell_differentInt
                { "sell_differentDouble",
                        new Trade(1, 10, buySell.SELL, "17", 1, 10000, 0, ITEM_OR_STOCK.STOCK), 1,
                        15000, 1, new long[] { 14970, 0 } } // sell_differentDouble

        };

    }

    @Test
    public void testCancelBuy() {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "material", "amount", "price", "sold")
                .values(1, 1, true, "1", 10, 2, 4).values(2, 1, true, "1", 10, 1, 1).build();

        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Trade t =
                new Trade(DatabaseTester.c.executeQuery("SELECT * FROM trade WHERE id=1").get(0),
                        ITEM_OR_STOCK.ITEM);

        t.cancel();

        ArrayList<HashMap<String, Object>> query =
                DatabaseTester.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertTrue(query != null && query.size() == 1);
        assertEquals(2L, query.get(0).get("id"));

        query = DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(1, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("-1", query.get(0).get("matid"));
        assertEquals(12L, query.get(0).get("amount"));
    }

    @Test
    public void testCancelSell() {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "material", "amount", "price", "sold")
                .values(1, 1, false, "1", 10, 2, 4).values(2, 1, false, "1", 10, 1, 1).build();

        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds), op);
        dbSetup.launch();

        final Trade t =
                new Trade(DatabaseTester.c.executeQuery("SELECT * FROM trade WHERE id=1").get(0),
                        ITEM_OR_STOCK.ITEM);

        t.cancel();

        ArrayList<HashMap<String, Object>> query =
                DatabaseTester.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertTrue(query != null && query.size() == 1);

        assertEquals(2L, query.get(0).get("id"));

        query = DatabaseTester.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(1, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("1", query.get(0).get("matid"));
        assertEquals(6L, query.get(0).get("amount"));
    }

    @Test
    public void testCancelUpdatesTradeHistory() {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "material", "amount", "price", "sold")
                .values(1, 1, false, "1", 10, 2, 4).values(2, 1, false, "1", 10, 1, 1).build();
        final Operation op2 = insertInto("tradeHistory")
                .columns("id", "traderid", "buysell", "material", "amount", "price", "cancelled")
                .values(1, 1, false, "1", 10, 2, null).values(2, 1, false, "1", 10, 1, null)
                .build();

        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(DatabaseTester.ds),
                Operations.sequenceOf(op, op2));
        dbSetup.launch();

        final Trade t =
                new Trade(DatabaseTester.c.executeQuery("SELECT * FROM trade WHERE id=1").get(0),
                        ITEM_OR_STOCK.ITEM);

        t.cancel();

        final ArrayList<HashMap<String, Object>> query =
                DatabaseTester.c.executeQuery("SELECT * FROM tradeHistory ORDER BY id");
        assertTrue(query != null && query.size() == 2);

        assertEquals(1L, query.get(0).get("id"));
        assertTrue(new Timestamp(System.currentTimeMillis() - 1000)
                .before((Timestamp) query.get(0).get("cancelled")));
        assertTrue(new Timestamp(System.currentTimeMillis() + 1000)
                .after((Timestamp) query.get(0).get("cancelled")));

        assertEquals(2L, query.get(1).get("id"));
        assertNull(query.get(1).get("cancelled"));
    }

}
