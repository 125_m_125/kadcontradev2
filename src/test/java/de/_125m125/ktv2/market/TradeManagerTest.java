package de._125m125.ktv2.market;

import static com.ninja_squad.dbsetup.Operations.deleteAllFrom;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static com.ninja_squad.dbsetup.Operations.truncate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.UnavailableException;
import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.LotteryItemConfig;
import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.market.Trade.buySell;

public class TradeManagerTest {
    private static Connector c;

    public static final Operation DELETE_ALL = deleteAllFrom(Connector.TABLES);
    private static DataSource     ds;

    private static final TradeManager tm = new TradeManager(0.002, 0);

    @AfterClass
    public static void afterClass() {
        Log.enable();
        TradeManagerTest.c.close();
        Whitebox.setInternalState(Variables.class, "lotteryItemConfig",
                (Map<String, LotteryItemConfig>) null);
    }

    @BeforeClass
    public static void beforeClass() throws UnavailableException {
        Variables.dbname = "kttest";
        Variables.dbpass = "kttupass";
        Variables.dbuser = "kttu";
        Variables.setPoolSize(1);

        new Main(false);
        Log.disable();
        TradeManagerTest.c = new Connector();
        Main.instance().setConnector(TradeManagerTest.c);

        TradeManagerTest.ds = TradeManagerTest.c.getDataSource();

        TradeManagerTest.c.executeUpdate("DROP DATABASE kttest");
        TradeManagerTest.c.executeUpdate("CREATE DATABASE kttest");
        TradeManagerTest.c.execute("use kttest");
        TradeManagerTest.c.checktables();

        final LotteryItemConfig lotteryItemConfig = new LotteryItemConfig(2, "-2",
                LocalDate.now().minusDays(5), LocalDate.now().plusDays(5), 10,
                LocalDate.now().minusDays(10), LocalDate.now().plusDays(10), 5);
        final HashMap<String, LotteryItemConfig> lic = new HashMap<>();
        lic.put("-2", lotteryItemConfig);
        Whitebox.setInternalState(Variables.class, "lotteryItemConfig", lic);
    }

    @Before
    public void prepareDB() {
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds),
                TradeManagerTest.DELETE_ALL);

        dbSetup.launch();
    }

    @Test
    public void testCreate_insertsTrade() throws Exception {
        final Operation op = sequenceOf(truncate("trade"),
                insertInto("trade")
                        .columns("traderid", "buysell", "material", "amount", "price", "sold")
                        .values(1, false, 1, 10, 20, 10).build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(10, buySell.SELL, "1", 2, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        assertEquals("creationsuccess", actual.getResultString());

        ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(2, query.size());

        HashMap<String, Object> entry = query.get(0);
        assertEquals(1L, entry.get("id"));
        assertEquals(1L, entry.get("traderid"));

        entry = query.get(1);
        final long id = (long) entry.get("id");
        assertEquals(2L, entry.get("traderid"));
        assertEquals(false, entry.get("buysell"));
        assertEquals("1", entry.get("material"));
        assertEquals(10L, entry.get("amount"));
        assertEquals(11L, entry.get("price"));
        assertEquals(0L, entry.get("sold"));

        query = TradeManagerTest.c.executeQuery("SELECT * FROM tradeHistory ORDER BY id");
        assertNotNull(query);
        assertEquals(1, query.size());

        entry = query.get(0);
        assertEquals(id, entry.get("id"));
        assertEquals(2L, entry.get("traderid"));
        assertEquals(false, entry.get("buysell"));
        assertEquals("1", entry.get("material"));
        assertEquals(10L, entry.get("amount"));
        assertEquals(11L, entry.get("price"));
    }

    @Test
    public void testCreate_blocksLotteryTradeIfNotConsented() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("materials")
                .columns("id", "type", "matid", "amount").values(2, true, "-2", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(3, buySell.SELL, "-2", 2, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertFalse(actual.isSuccess());
        assertEquals("lotteryPermissionRequired", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNull(query);
    }

    @Test
    public void testCreate_allowsLotteryTradeIfConsented() throws Exception {
        final Operation op = sequenceOf(truncate("trade"),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "-2", 100).build(),
                insertInto("gdpr").columns("user", "type").values("2", "PL").build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(3, buySell.SELL, "-2", 2, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertEquals("creationsuccess", actual.getResultString());
        assertTrue(actual.isSuccess());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
    }

    @Test
    public void testCreate_subtractsItems() throws Exception {
        final Operation op = sequenceOf(truncate("trade"),
                insertInto("trade").columns("traderid", "buysell", "type", "material", "amount",
                        "price", "sold").values(1, false, true, 1, 10, 20, 10).build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(10, buySell.SELL, "1", 2, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        assertEquals("creationsuccess", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials");
        assertNotNull(query);
        assertEquals(1, query.size());

        final HashMap<String, Object> entry = query.get(0);
        assertEquals(2L, entry.get("id"));
        assertEquals("1", entry.get("matid"));
        assertEquals(90L, entry.get("amount"));
    }

    @Test
    public void testCreate_tradesWithMatching() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, true, true, "1", 10, 20, 5).values(2, true, true, "1", 12, 30, 0)
                .values(3, true, true, "2", 12, 30, 0).values(4, true, true, "1", 12, 5, 0)
                .values(5, true, false, "1", 10, 20, 5).values(6, true, false, "1", 12, 30, 0)
                .values(7, true, false, "2", 12, 30, 0).values(8, true, false, "1", 12, 5, 0)
                .build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(20, buySell.SELL, "1", 2, 10, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        final long id = actual.getTradeResult().getTradeid();
        assertEquals("creationsuccess", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(7, query.size());

        // HashMap<String, Object> entry = query.get(0);
        // assertEquals(1L, entry.get("id"));
        // assertEquals(1L, entry.get("traderid"));
        // assertEquals(10L, entry.get("sold"));
        // assertEquals(34L, entry.get("toTake0"));
        // assertEquals(15, entry.get("toTake1"));

        // entry = query.get(1);
        // assertEquals(2L, entry.get("id"));
        // assertEquals(2L, entry.get("traderid"));
        // assertEquals(12L, entry.get("sold"));
        // assertEquals(119L, entry.get("toTake0"));
        // assertEquals(12, entry.get("toTake1"));

        HashMap<String, Object> entry = query.get(0);
        assertEquals(3L, entry.get("id"));
        assertEquals(3L, entry.get("traderid"));
        assertEquals(0L, entry.get("sold"));
        // assertEquals(0L, entry.get("toTake0"));
        // assertEquals(0, entry.get("toTake1"));

        entry = query.get(1);
        assertEquals(4L, entry.get("id"));
        assertEquals(4L, entry.get("traderid"));
        assertEquals(0L, entry.get("sold"));

        entry = query.get(2);
        assertEquals(5L, entry.get("id"));
        assertEquals(5L, entry.get("traderid"));
        assertEquals(5L, entry.get("sold"));

        entry = query.get(3);
        assertEquals(6L, entry.get("id"));
        assertEquals(6L, entry.get("traderid"));
        assertEquals(0L, entry.get("sold"));

        entry = query.get(4);
        assertEquals(7L, entry.get("id"));
        assertEquals(7L, entry.get("traderid"));
        assertEquals(0L, entry.get("sold"));

        entry = query.get(5);
        assertEquals(8L, entry.get("id"));
        assertEquals(8L, entry.get("traderid"));
        assertEquals(0L, entry.get("sold"));

        entry = query.get(6);
        assertEquals(id, entry.get("id"));
        assertEquals(2L, entry.get("traderid"));
        assertEquals(false, entry.get("buysell"));
        assertEquals("1", entry.get("material"));
        assertEquals(20L, entry.get("amount"));
        assertEquals(10L, entry.get("price"));
        assertEquals(17L, entry.get("sold"));
    }

    @Test
    public void testCreate_paysOutWhenTradingWithMatching() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, true, true, "1", 10, 20, 5).values(2, true, true, "1", 12, 30, 0)
                .values(3, true, true, "2", 12, 30, 0).values(4, true, true, "1", 12, 5, 0)
                .values(5, true, false, "1", 10, 20, 5).values(6, true, false, "1", 12, 30, 0)
                .values(7, true, false, "2", 12, 30, 0).values(8, true, false, "1", 12, 5, 0)
                .build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(20, buySell.SELL, "1", 2, 10, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        final long id = actual.getTradeResult().getTradeid();
        assertEquals("creationsuccess", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(4, query.size());

        HashMap<String, Object> entry = query.get(0);
        assertEquals(1L, entry.get("id"));
        assertEquals("-1", entry.get("matid"));
        assertEquals(24L, entry.get("amount"));

        entry = query.get(1);
        assertEquals(1L, entry.get("id"));
        assertEquals("1", entry.get("matid"));
        assertEquals(5L, entry.get("amount"));

        entry = query.get(2);
        assertEquals(2L, entry.get("id"));
        assertEquals("-1", entry.get("matid"));
        assertEquals(432L, entry.get("amount"));

        entry = query.get(3);
        assertEquals(2L, entry.get("id"));
        assertEquals("1", entry.get("matid"));
        assertEquals(92L, entry.get("amount"));
    }

    @Test
    public void testCreate_missingRessources() throws Exception {
        final Operation op = sequenceOf(truncate("trade"),
                insertInto("trade")
                        .columns("traderid", "buysell", "material", "amount", "price", "sold")
                        .values(1, false, 1, 10, 20, 10).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(10, buySell.SELL, "1", 2, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertFalse(actual.isSuccess());
        assertEquals("notenoughmaterials", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(1, query.size());

        final HashMap<String, Object> entry = query.get(0);
        assertEquals(1L, entry.get("id"));
        assertEquals(1L, entry.get("traderid"));
    }

    @Test
    public void testCreate_over6Allowed_buysell() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, true, true, 1, 10, 20, 10).values(1, true, true, 2, 20, 20, 10)
                .values(1, false, true, 3, 30, 20, 10).values(1, true, true, 4, 40, 20, 10)
                .values(1, false, true, 5, 50, 20, 10).values(1, true, true, 6, 60, 20, 10).build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(10, buySell.SELL, "1", 1, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        assertEquals("creationsuccess", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(7, query.size());
    }

    @Test
    public void testCreate_over6Failed() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, false, true, 1, 10, 20, 10).values(1, true, true, 2, 20, 20, 10)
                .values(1, false, true, 3, 30, 20, 10).values(1, true, true, 4, 40, 20, 10)
                .values(1, false, true, 5, 50, 20, 10).values(1, true, true, 6, 60, 20, 10).build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(10, buySell.SELL, "1", 1, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertFalse(actual.isSuccess());
        assertEquals("nofreeslots", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(6, query.size());
    }

    @Test
    public void testCreate_under6Failed() throws Exception {
        final Operation op = sequenceOf(truncate("trade"),
                insertInto("trade").columns("traderid", "buysell", "type", "material", "amount",
                        "price", "sold").values(1, false, true, 1, 10, 20, 10).build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(1, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(10, buySell.SELL, "1", 1, 11, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertFalse(actual.isSuccess());
        assertEquals("nofreeslots", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(1, query.size());
    }

    @Test
    public void testCreate_ignoresOther() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, false, true, 1, 10, 20, 10).values(1, true, true, 2, 20, 20, 10)
                .values(1, false, true, 3, 30, 20, 10).values(1, true, true, 4, 40, 20, 10)
                .values(1, false, true, 5, 50, 20, 10).values(1, true, true, 6, 60, 20, 10)
                .build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(10, buySell.SELL, "1", 1, 11, ITEM_OR_STOCK.STOCK, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertFalse(actual.isSuccess());
        assertEquals("notenoughmaterials", actual.getResultString());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(6, query.size());
    }

    @Test
    public void testRefBonuses_noRefs() throws Exception {
        try (Connection con = TradeManagerTest.c.getConnection()) {
            Whitebox.invokeMethod(TradeManagerTest.tm, "refBonuses", con, 10000L, 1L);
        }
        final ArrayList<HashMap<String, Object>> result =
                Main.instance().getConnector().executeQuery("SELECT * FROM reftaxes");
        assertNull(result);
    }

    @Test
    public void testRefBonuses_master() throws Exception {
        final Operation op = insertInto("referrer").columns("id", "ref").values(2, 1).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        try (Connection con = TradeManagerTest.c.getConnection()) {
            Whitebox.invokeMethod(TradeManagerTest.tm, "refBonuses", con, 10000L, 1L);
        }
        final ArrayList<HashMap<String, Object>> result =
                Main.instance().getConnector().executeQuery("SELECT * FROM reftaxes");
        assertEquals(1, result.size());
        assertEquals(2L, result.get(0).get("id"));
        assertEquals(2500L, result.get(0).get("amount"));
    }

    @Test
    public void testRefBonuses_slave() throws Exception {
        final Operation op =
                insertInto("referrer").columns("id", "ref").values(1, 2).values(1, 3).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();
        try (Connection con = TradeManagerTest.c.getConnection()) {
            Whitebox.invokeMethod(TradeManagerTest.tm, "refBonuses", con, 2000L, 1L);
        }

        final ArrayList<HashMap<String, Object>> result =
                Main.instance().getConnector().executeQuery("SELECT * FROM reftaxes ORDER BY id");
        assertEquals(2, result.size());
        assertEquals(2L, result.get(0).get("id"));
        assertEquals(250L, result.get(0).get("amount"));
        assertEquals(3L, result.get(1).get("id"));
        assertEquals(250L, result.get(0).get("amount"));
    }

    @Test
    public void testRefBonuses_integration() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, false, true, 1, 100, 70000, 000)
                // quarttaxes=187
                .values(2, false, true, 1, 300, 80000, 100).build(), // quarttaxes=200
                insertInto("referrer").columns("id", "ref").values(1, 2)// 1:187+2*200
                                                                        // 2:93
                        .values(1, 3)// 3:93
                        .values(4, 1)// 4:187
                        .values(2, 3)// 3:187+2*200
                        .build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, -1, 100000000).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> create = TradeManagerTest.tm.create(200, buySell.BUY, "1", 2,
                80000, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = create.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        assertEquals("creationsuccessCompleted", actual.getResultString());

        final ArrayList<HashMap<String, Object>> result =
                Main.instance().getConnector().executeQuery("SELECT * FROM reftaxes ORDER BY id");
        assertEquals(4, result.size());

        assertEquals(1L, result.get(0).get("id"));
        assertEquals(11750L, result.get(0).get("amount"));
        assertEquals(2L, result.get(1).get("id"));
        assertEquals(1875L, result.get(1).get("amount"));
        assertEquals(3L, result.get(2).get("id"));
        assertEquals(13625L, result.get(2).get("amount"));
        assertEquals(4L, result.get(3).get("id"));
        assertEquals(3750L, result.get(3).get("amount"));
    }

    @Test
    public void testStockBonusesNoRef() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, false, true, 1, 100, 70000, 000)
                .values(2, false, true, 1, 300, 80000, 100).build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, -1, 100000000).values(5, false, -1, 40)
                        .values(6, false, -1, 30).values(7, false, -1, 20).values(8, false, -1, 10)
                        .build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> create = TradeManagerTest.tm.create(200, buySell.BUY, "1", 2,
                80000, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = create.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        assertEquals("creationsuccessCompleted", actual.getResultString());

        final ArrayList<HashMap<String, Object>> result =
                Main.instance().getConnector().executeQuery("SELECT * FROM reftaxes ORDER BY id");
        assertEquals(4, result.size());

        assertEquals(5L, result.get(0).get("id"));
        assertEquals(24800L, result.get(0).get("amount"));
        assertEquals(6L, result.get(1).get("id"));
        assertEquals(18600L, result.get(1).get("amount"));
        assertEquals(7L, result.get(2).get("id"));
        assertEquals(12400L, result.get(2).get("amount"));
        assertEquals(8L, result.get(3).get("id"));
        assertEquals(6200L, result.get(3).get("amount"));
    }

    @Test
    public void testStockBonusesWithRef() throws Exception {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, false, true, 1, 100, 70000, 000)
                .values(3, false, true, 1, 300, 80000, 100).build(),
                insertInto("referrer").columns("id", "ref").values(2, 4).build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, -1, 100000000).values(5, false, -1, 40)
                        .values(6, false, -1, 30).values(7, false, -1, 20).values(8, false, -1, 10)
                        .build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> create = TradeManagerTest.tm.create(200, buySell.BUY, "1", 2,
                80000, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = create.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        assertEquals("creationsuccessCompleted", actual.getResultString());

        final ArrayList<HashMap<String, Object>> result =
                Main.instance().getConnector().executeQuery("SELECT * FROM reftaxes ORDER BY id");
        System.out.println(result);
        assertEquals(5, result.size());

        assertEquals(4L, result.get(0).get("id"));
        assertEquals(7750L, result.get(0).get("amount"));
        assertEquals(5L, result.get(1).get("id"));
        assertEquals(21700L, result.get(1).get("amount"));
        assertEquals(6L, result.get(2).get("id"));
        assertEquals(16275L, result.get(2).get("amount"));
        assertEquals(7L, result.get(3).get("id"));
        assertEquals(10850L, result.get(3).get("amount"));
        assertEquals(8L, result.get(4).get("id"));
        assertEquals(5425L, result.get(4).get("amount"));
    }

    @Test
    public void testCreate_TradesDoneHistory()
            throws InterruptedException, ExecutionException, TimeoutException {
        final Operation op = sequenceOf(truncate("trade"), insertInto("trade")
                .columns("traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(1, true, true, "1", 10, 20, 5).values(2, true, true, "1", 12, 30, 0)
                .values(3, true, true, "2", 12, 30, 0).values(4, true, true, "1", 12, 5, 0)
                .values(5, true, false, "1", 10, 20, 5).values(6, true, false, "1", 12, 30, 0)
                .values(7, true, false, "2", 12, 30, 0).values(8, true, false, "1", 12, 5, 0)
                .build(),
                insertInto("materials").columns("id", "type", "matid", "amount")
                        .values(2, true, "1", 100).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result =
                TradeManagerTest.tm.create(20, buySell.SELL, "1", 2, 10, ITEM_OR_STOCK.ITEM, true);

        final TradeResult actual = result.get(2, TimeUnit.SECONDS);
        assertTrue(actual.isSuccess());
        final long id = actual.getTradeResult().getTradeid();
        assertEquals("creationsuccess", actual.getResultString());

        final ArrayList<HashMap<String, Object>> dbresult =
                TradeManagerTest.c.executeQuery("SELECT * FROM tradesDoneHistory ORDER BY id1,id2");

        assertNotNull(dbresult);
        assertEquals(2, dbresult.size());

        assertEquals(1L, dbresult.get(0).get("id1"));
        assertEquals(id, dbresult.get(0).get("id2"));
        assertEquals(5L, dbresult.get(0).get("amount"));

        assertEquals(2L, dbresult.get(1).get("id1"));
        assertEquals(id, dbresult.get(1).get("id2"));
        assertEquals(12L, dbresult.get(1).get("amount"));
    }

    @Test
    public void testCancelDeletes() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(777, 1, false, true, 1, 10, 20, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result = TradeManagerTest.tm.cancel(777, 1, ITEM_OR_STOCK.ITEM);

        assertTrue(result.get().isSuccess());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNull(query);
    }

    @Test
    public void testCancelInsertsItems() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(777, 1, false, true, 5, 10, 20, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result = TradeManagerTest.tm.cancel(777, 1, ITEM_OR_STOCK.ITEM);

        assertTrue(result.get().isSuccess());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(1, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("5", query.get(0).get("matid"));
        assertEquals(5L, query.get(0).get("amount"));
    }

    @Test
    public void testCancelUpdatesItems() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(777, 1, false, true, 5, 10, 20, 5).build();

        final Operation op2 = insertInto("materials").columns("id", "type", "matid", "amount")
                .values(1, true, "5", 100).build();
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(TradeManagerTest.ds), sequenceOf(op, op2));
        dbSetup.launch();

        final Future<TradeResult> result = TradeManagerTest.tm.cancel(777, 1, ITEM_OR_STOCK.ITEM);

        assertTrue(result.get().isSuccess());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(1, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("5", query.get(0).get("matid"));
        assertEquals(105L, query.get(0).get("amount"));
    }

    @Test
    public void testCancelInsertsMoney() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(777, 1, true, true, 5, 10, 20, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result = TradeManagerTest.tm.cancel(777, 1, ITEM_OR_STOCK.ITEM);

        assertTrue(result.get().isSuccess());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(1, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("-1", query.get(0).get("matid"));
        assertEquals(100L, query.get(0).get("amount"));
    }

    @Test
    public void testCancelUpdatesMoney() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(777, 1, true, true, 5, 10, 20, 5).build();
        final Operation op2 = insertInto("materials").columns("id", "type", "matid", "amount")
                .values(1, true, "-1", 100).build();
        final DbSetup dbSetup =
                new DbSetup(new DataSourceDestination(TradeManagerTest.ds), sequenceOf(op, op2));
        dbSetup.launch();

        final Future<TradeResult> result = TradeManagerTest.tm.cancel(777, 1, ITEM_OR_STOCK.ITEM);

        assertTrue(result.get().isSuccess());

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(1, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("-1", query.get(0).get("matid"));
        assertEquals(200L, query.get(0).get("amount"));
    }

    @Test
    public void testCancel_unknownTrade_id() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(777, 1, false, true, 1, 10, 20, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result = TradeManagerTest.tm.cancel(778, 1, ITEM_OR_STOCK.ITEM);

        assertFalse(result.get().isSuccess());

        ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(1, query.size());

        final HashMap<String, Object> entry = query.get(0);
        assertEquals(777L, entry.get("id"));
        assertEquals(1L, entry.get("traderid"));
        assertEquals(false, entry.get("buysell"));
        assertEquals("1", entry.get("material"));
        assertEquals(10L, entry.get("amount"));
        assertEquals(20L, entry.get("price"));
        assertEquals(5L, entry.get("sold"));

        query = TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id");
        assertNull(query);
    }

    @Test
    public void testCancel_unknownTrade_traderid() throws Exception {
        final Operation op = insertInto("trade")
                .columns("id", "traderid", "buysell", "type", "material", "amount", "price", "sold")
                .values(777, 1, false, true, 1, 10, 20, 5).build();
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final Future<TradeResult> result = TradeManagerTest.tm.cancel(777, 2, ITEM_OR_STOCK.ITEM);

        assertFalse(result.get().isSuccess());

        ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM trade ORDER BY id");
        assertNotNull(query);
        assertEquals(1, query.size());

        final HashMap<String, Object> entry = query.get(0);
        assertEquals(777L, entry.get("id"));
        assertEquals(1L, entry.get("traderid"));
        assertEquals(false, entry.get("buysell"));
        assertEquals("1", entry.get("material"));
        assertEquals(10L, entry.get("amount"));
        assertEquals(20L, entry.get("price"));
        assertEquals(5L, entry.get("sold"));

        query = TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id");
        assertNull(query);
    }

    @Test
    public void testDifferenceTaxWithSamePrice() throws Exception {
        final Operation op =
                sequenceOf(
                        insertInto("trade")
                                .columns("traderid", "buysell", "type", "material", "amount",
                                        "price", "sold")
                                .values(1, true, true, 1, 100, 70000, 000).build(),
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values(2, true, "1", 101).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final TradeManager uut = new TradeManager(0, 0.1);
        final Future<TradeResult> create =
                uut.create(100, buySell.SELL, "1", 2, 70000, ITEM_OR_STOCK.ITEM);
        final TradeResult tradeResult = create.get();

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(3, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("1", query.get(0).get("matid"));
        assertEquals(100L, query.get(0).get("amount"));
        assertEquals(2L, query.get(1).get("id"));
        assertEquals("-1", query.get(1).get("matid"));
        assertEquals(7000000L, query.get(1).get("amount"));
        assertEquals(2L, query.get(2).get("id"));
        assertEquals("1", query.get(2).get("matid"));
        assertEquals(1L, query.get(2).get("amount"));
    }

    @Test
    public void testDifferenceTaxWithDifferentPriceSell() throws Exception {
        final Operation op =
                sequenceOf(
                        insertInto("trade")
                                .columns("traderid", "buysell", "type", "material", "amount",
                                        "price", "sold")
                                .values(1, true, true, 1, 100, 70000, 000).build(),
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values(2, true, "1", 101).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final TradeManager uut = new TradeManager(0, 0.01);
        final Future<TradeResult> create =
                uut.create(100, buySell.SELL, "1", 2, 50000, ITEM_OR_STOCK.ITEM);
        final TradeResult tradeResult = create.get();

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(4, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("-1", query.get(0).get("matid"));
        assertEquals(990000L, query.get(0).get("amount"));
        assertEquals(1L, query.get(1).get("id"));
        assertEquals("1", query.get(1).get("matid"));
        assertEquals(100L, query.get(1).get("amount"));
        assertEquals(2L, query.get(2).get("id"));
        assertEquals("-1", query.get(2).get("matid"));
        assertEquals(5990000L, query.get(2).get("amount"));
        assertEquals(2L, query.get(3).get("id"));
        assertEquals("1", query.get(3).get("matid"));
        assertEquals(1L, query.get(3).get("amount"));
    }

    @Test
    public void testDifferenceTaxWithDifferentPriceBuy() throws Exception {
        final Operation op =
                sequenceOf(
                        insertInto("trade")
                                .columns("traderid", "buysell", "type", "material", "amount",
                                        "price", "sold")
                                .values(1, false, true, 1, 100, 50000, 000).build(),
                        insertInto("materials").columns("id", "type", "matid", "amount")
                                .values(2, true, "-1", 7000000).build());
        final DbSetup dbSetup = new DbSetup(new DataSourceDestination(TradeManagerTest.ds), op);
        dbSetup.launch();

        final TradeManager uut = new TradeManager(0, 0.01);
        final Future<TradeResult> create =
                uut.create(100, buySell.BUY, "1", 2, 70000, ITEM_OR_STOCK.ITEM);
        final TradeResult tradeResult = create.get();

        final ArrayList<HashMap<String, Object>> query =
                TradeManagerTest.c.executeQuery("SELECT * FROM materials ORDER BY id,matid");
        assertNotNull(query);
        assertEquals(3, query.size());

        assertEquals(1L, query.get(0).get("id"));
        assertEquals("-1", query.get(0).get("matid"));
        assertEquals(5990000L, query.get(0).get("amount"));
        assertEquals(2L, query.get(1).get("id"));
        assertEquals("-1", query.get(1).get("matid"));
        assertEquals(990000L, query.get(1).get("amount"));
        assertEquals(2L, query.get(2).get("id"));
        assertEquals("1", query.get(2).get("matid"));
        assertEquals(100L, query.get(2).get("amount"));
    }

}
