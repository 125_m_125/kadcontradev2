package de._125m125.ktv2;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.logging.Level;

import javax.servlet.UnavailableException;

import org.bouncycastle.operator.OperatorCreationException;

import de._125m125.ktv2.calculators.CertificateManager;
import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.market.TradeManager;
import de._125m125.ktv2.notifications.FirebaseNotificationManager;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.reader.BankReader;
import de._125m125.ktv2.servlets.apiv2.PusherRestAPI;
import de._125m125.ktv2.websocket.WebSocket;

public class Main {
    private static Main main;

    public static Main instance() {
        return Main.main;
    }

    private BankReader             bankReader;

    private Connector              connector;

    private TradeManager           tradeManager;

    private NotificationObservable notificationObservable;

    public Main() throws UnavailableException {
        this(true);
    }

    public Main(final boolean b) throws UnavailableException {
        if (b) {
            Variables.prepare();
            try {
                this.connector = new Connector();
                if (!this.connector.canConnect()) {
                    this.connector.close();
                    throw new UnavailableException("mysql not reachable");
                }
            } catch (final Exception e) {
                if (this.connector != null) {
                    this.connector.close();
                }
                throw new UnavailableException("mysql not reachable" + e.getCause() + " " + e.getMessage());
            }
            this.connector.checktables();
            this.bankReader = new BankReader(this.connector);
            this.tradeManager = new TradeManager();
            this.notificationObservable = new NotificationObservable();
            PusherRestAPI.initPusher(this.notificationObservable);
            WebSocket.initWebSocket(this.notificationObservable);
            FirebaseNotificationManager.instance().setObservable(this.notificationObservable);
            try {
                CertificateManager.instance().initialize();
            } catch (NoSuchAlgorithmException | OperatorCreationException | CertificateException
                    | NoSuchProviderException | IOException | IllegalStateException e) {
                e.printStackTrace();
                throw new UnavailableException("Failed Certificate Creation");
            }
        }
        Main.main = this;
    }

    public void close() {
        Log.SYSTEM.log(Level.INFO, "kadcontrade is stopping");
        this.tradeManager.stop();
        Log.close();
        this.connector.close();
    }

    public BankReader getBankReader() {
        return this.bankReader;
    }

    public Connector getConnector() {
        return this.connector;
    }

    public void setBankReader(final BankReader bankReader) {
        this.bankReader = bankReader;
    }

    public void setConnector(final Connector c) {
        this.connector = c;
    }

    public TradeManager getTradeManager() {
        return this.tradeManager;
    }

    public NotificationObservable getNotificationObservable() {
        return this.notificationObservable;
    }
}
