package de._125m125.ktv2.reader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;

public class VKBAReader {

    private static final long DELAY     = 60000 * 3;
    private static final int  LIMIT     = 10;
    private static final int  MAXOFFSET = VKBAReader.LIMIT * 9;

    private static long nextRead = 0;

    public static synchronized boolean read() {
        if (VKBAReader.nextRead > System.currentTimeMillis()) {
            return false;
        }
        VKBAReader.nextRead = System.currentTimeMillis() + VKBAReader.DELAY;
        new Thread(VKBAReader::run).start();
        return true;
    }

    public static void run() {
        final String lastId = (String) Main.instance().getConnector().getColumn("system", "v",
                "k='last_VKBA_ID'");
        String newLastId = null;
        if (lastId == null) {
            final JsonArray executeRequest = executeVkbaRequest(0, 1);
            if (executeRequest == null || executeRequest.size() == 0) {
                return;
            }
            newLastId = executeRequest.getJsonObject(0).getString("tID");
        } else {
            boolean first = true;
            boolean found = false;
            int offset = 0;
            while (!found) {
                final JsonArray executeRequest = executeVkbaRequest(offset, VKBAReader.LIMIT);
                if (executeRequest == null || executeRequest.isEmpty()) {
                    // something went wrong
                    break;
                }
                if (first) {
                    newLastId = executeRequest.getJsonObject(0).getString("tID");
                    first = false;
                }
                found = analyse(executeRequest, lastId);
                if ((offset += VKBAReader.LIMIT) > VKBAReader.MAXOFFSET) {
                    // too many requests, looks like something went wrong
                    break;
                }
            }
        }
        Main.instance().getConnector().executeUpdate(
                "INSERT INTO system(k,v) VALUES('last_VKBA_ID',?) ON DUPLICATE KEY UPDATE v=?",
                newLastId, newLastId);
    }

    private static boolean analyse(final JsonArray executeRequest, final String lastId) {
        for (int i = 0; i < executeRequest.size(); i++) {
            final JsonObject current = executeRequest.getJsonObject(i);
            if (current.getString("tID").equals(lastId)) {
                return true;
            }
            if (current.getString("tAdress").equals(Variables.getVkbaName())) {
                ReaderExecutor.payin(current.getString("tSender"), current.getString("tID"), "-1",
                        current.getString("tAmount"), null);
            }
        }
        return false;
    }

    private static JsonArray executeVkbaRequest(final int offset, final int limit) {
        final String urlString = "https://vkba.legendskyfall.de/api/transactions/?limit=" + limit
                + "&offset=" + offset + "&" + Variables.getVkbaAuth();
        try {
            final URL url = new URL(urlString);
            final URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", Variables.getAppName());
            connection.connect();
            try (InputStream stream = connection.getInputStream()) {
                return Json.createReader(stream).readArray();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        } catch (final IOException e) {
            e.printStackTrace();
        } catch (final JsonParsingException e) {
        }
        return null;
    }
}
