package de._125m125.ktv2.reader;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.apiv2.ItemRestAPI;
import de._125m125.ktv2.servlets.apiv2.MessageRestAPI;

public class ReaderExecutor {
    public static void payin(final String name, final String identificator, final String item,
            final String amount, final String substractMoneyAmount) {
        final long id = UserHelper.get(name);
        long d;
        try {
            if ("-1".equals(item)) {
                d = IDSConverter.stringToLong(amount);
            } else {
                d = Integer.parseInt(amount);
            }
        } catch (final NumberFormatException ex) {
            Log.BANK_READER.log(Level.WARNING,
                    "unknown resourceamount: " + name + ":" + identificator + ":" + amount, id);
            return;
        }
        if (!Variables.resourceExistsOrIsMoney(item)) {
            Log.BANK_READER.log(Level.WARNING, "unknown resource: " + item + " at " + identificator,
                    id);
            return;
        }
        if (id == -1) {
            final boolean successful = Main.instance().getConnector().executeAtomic(c -> {
                Log.BANK_READER.executeLog(c, Level.INFO,
                        "unknown user transfered money: " + name + " at " + identificator);
                try (PreparedStatement ps = c.prepareStatement(
                        "INSERT INTO unknownPayins(username, type, item, amount) VALUES(?,true,?,?) ON DUPLICATE KEY UPDATE amount=amount+VALUES(amount)")) {
                    ps.setString(1, name);
                    ps.setString(2, item);
                    ps.setLong(3, d);
                    ps.executeUpdate();
                }
                return () -> true;
            }).wasSuccessful();
            if (!successful) {
                Log.BANK_READER.log(Level.WARNING,
                        "failed to store payin of unknown user: " + name + " at " + identificator);
            }
            return;
        }
        final List<Message> messages = new ArrayList<>(2);
        final String stringId = String.valueOf(id);
        final Successindicator executeAtomic = Main.instance().getConnector().executeAtomic(con -> {
            messages.add(MySQLHelper.addResource(con, id, true, item, d,
                    Variables.translate("payin", identificator), true));
            if (substractMoneyAmount != null) {
                messages.add(MySQLHelper.addResource(con, id, true, "-1",
                        -IDSConverter.stringToLong(substractMoneyAmount),
                        Variables.translate("payinCosts", identificator), true));
            }
            Log.BANK_READER.executeLog(con, Level.INFO,
                    "payin: " + identificator + ":" + name + ":" + amount + " Kadis", id);
            return () -> true;
        });
        if (executeAtomic.wasSuccessful()) {
            ItemRestAPI.invalidate(stringId, () -> Item.getItems(id, item), true);
            MessageRestAPI.invalidate(stringId, messages, false);
        } else {
            Log.BANK_READER.log(Level.WARNING,
                    "update of usermaterials failed: " + name + ":" + identificator, id);
        }
    }
}
