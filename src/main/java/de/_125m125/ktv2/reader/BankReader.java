package de._125m125.ktv2.reader;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.logging.Log;

public class BankReader implements Runnable {
    private static final int STEP_SIZE  = 25;
    private static final int MAX_OFFSET = BankReader.STEP_SIZE * 9;

    private static Pattern   PATTERN_FOR_CHESTSHOP_MATID = Pattern.compile("\\(.+?\\)");
    public static final long DELTA                       = 60000 * 3;
    private final Connector  c;
    /* package */long        lastRead                    = 0;

    public BankReader(final Connector c) {
        this.c = c;
    }

    /*
     * <tr class="hover">0<td>time</td> compare to last checked
     * 1<td>ownAccount</td> -2<td .*">in?</td> not empty for
     * KBank/PowerSign,
     * empty for ChestShop3<td .*">amount</td> -4<td .*">out?</td> -
     * 5<td>otherAccount</td> user + compare to last user6<td>Type</td>
     * ChestShop/PowerSign/KBank7<td>reason</td> ChestShop?->split(" "
     * ,2)->add
     * split0 to resource split18<td>Location</td> -9<td>new
     * balance</td> -
     * </tr>
     */
    /**
     * analyzes the entries that were read
     *
     * @param al
     *            the list that was read
     * @param t
     *            the newest timestamp that was read before
     * @param user
     *            the user that was read before
     * @return true if this was the last page that has to be read, else false
     */
    /* package */boolean analyseEntries(final List<String[]> al, final Timestamp t,
            final String user) {
        if (al == null || al.isEmpty()) {
            return true;
        }
        for (final String[] e : al) {
            Log.BANK_READER.log(Level.FINE, "bank received String: " + Arrays.toString(e));
            final int k = Timestamp.valueOf(e[0]).compareTo(t);
            if (k < 0) {
                return true;
            } else if (k == 0) {
                if (e[5].equals(user)) {
                    return true;
                }
            }
            if (!e[1].equals("*kadcontrade")) {
                continue;
            }
            switch (e[6].toLowerCase()) {
            case "powersign":
                if (e[2].equals("")) {
                    continue;
                }
                if (e[7].matches("register. aktiviert")) {
                    register(e);
                } else if (e[7].matches("changepw. aktiviert")) {
                    pwchange(e);
                } else if (e[7].equals("clear aktiviert")) {
                    clearRequests(e);
                } else if (e[7].equals("payin aktiviert")) {
                    payin(e);
                }
                break;
            case "kshop":
                chestshop(e);
                break;
            case "kbank":
                payin(e);
                break;
            }
        }
        return false;
    }

    private void payin(final String[] e) {
        if (e[2].equals("")) {
            return;
        }
        ReaderExecutor.payin(e[5], e[0], "-1", e[3], null);
    }

    private void chestshop(final String[] e) {
        final Matcher m = BankReader.PATTERN_FOR_CHESTSHOP_MATID.matcher(e[7]);
        if (!m.find()) {
            Log.BANK_READER.log(Level.WARNING,
                    "unknown message: " + e[5] + ":" + e[7] + " at " + e[0]);
            return;
        }
        String res = e[7].substring(m.start() + 1, m.end() - 1);
        if (res.endsWith(":0")) {
            res = res.substring(0, res.length() - 2);
        } else if (res.equals("397:3")) {
            final String tmp = e[7].split(" ", 3)[1];
            if (tmp.startsWith("c_")) {
                res = tmp.substring(2);
            }
        }
        ReaderExecutor.payin(e[5], e[0], res, e[7].split(" ", 2)[0], e[3]);
    }

    private void clearRequests(final String[] e) {
        this.c.executeUpdate("DELETE FROM possibleRegister WHERE name=?", e[5]);
        this.c.executeUpdate("DELETE FROM possiblepwchange WHERE name=?", e[5]);
        Log.BANK_READER.log(Level.INFO, e[5] + "cleared his proposals");
    }

    private void pwchange(final String[] e) {
        final ArrayList<HashMap<String, Object>> z = this.c.executeQuery(
                "SELECT * FROM possiblepwchange WHERE name=? AND id=?", e[5], e[7].substring(8, 9));
        if (z != null && z.size() > 0 && z.get(0) != null) {
            final String c = this.c.changePass((String) z.get(0).get("name"),
                    (String) z.get(0).get("Password"));
            Log.BANK_READER.log(Level.INFO, z.get(0).get("name") + "changed his password." + c);
        }
    }

    private void register(final String[] e) {
        final ArrayList<HashMap<String, Object>> z = this.c.executeQuery(
                "SELECT * FROM possibleRegister WHERE name=? AND id=?", e[5], e[7].substring(8, 9));
        if (z == null) {
            return;
        }
        final HashMap<String, Object> hashMap = z.get(0);
        if (z != null && z.size() > 0 && hashMap != null) {
            final String c = this.c.createUser((String) hashMap.get("name"),
                    (String) hashMap.get("Password"), (String) hashMap.get("itemcomment"),
                    (Long) hashMap.get("referrer"), (String) hashMap.get("IP"), null, null);
            this.c.executeUpdate("DELETE FROM possibleRegister WHERE name=?", e[5]);
            Log.BANK_READER.log(Level.INFO, hashMap.get("name") + "finished registering." + c);
        }
    }

    /**
     * checks if the scanner contains a error-message
     *
     * @param s
     *            the scanner to check
     * @return true if and only if there is no error-message
     * @see Scanner#hasNext()
     */
    public boolean checkResult(final Scanner s) {
        s.useDelimiter("(</div>)?<div class=\"Box Body Error\" id=\"Error\">");
        final boolean b = !s.hasNext();
        if (!b) {
            return true;
        }
        final String error = s.next();
        if (!error.startsWith(
                "Es sind keine Daten vorhanden, die den gewählten Filter-Einstellungen entsprechen.")) {
            Log.BANK_READER.log(Level.SEVERE, "the query resulted in an Exception: " + error);
        }
        return false;
    }

    /**
     * excracts the Entries from the report
     *
     * @param s
     *            the scanner to use to extract
     * @return an arraylist with the found entries
     */
    /* package */ArrayList<String[]> extractEntries(final Scanner s) {
        final ArrayList<String[]> al = new ArrayList<>();
        if (!checkResult(s)) {
            return al;
        }
        s.useDelimiter("(</tr>)?\\W*<tr class=\"hover\">");
        while (s.hasNext()) {
            String b = s.next();
            b = b.replaceAll("\n", "").replaceFirst("\\W*<td>", "");
            b = b.substring(0, b.lastIndexOf("</td>"));
            if (b.endsWith("\t")) {
                b = b.substring(0, b.lastIndexOf("</td>"));
            }
            final String[] split = (b.split("(</a>)?(</td>)?\\W*?((<td>)|(<td .*?\">))(<a.*?>)?"));
            if (split.length != 10) {
                continue;
            }
            al.add(split);
        }
        return al;
    }

    /**
     * gets the cookie that should be used to read the data
     *
     * @return the value of the cookie to use
     * @throws IOException
     *             when an IOException occured during readout
     * @see URL
     * @see HttpURLConnection
     */
    /* package */String firstQuery(final URL url) throws IOException {
        final HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
        urlConn.setInstanceFollowRedirects(false);
        urlConn.connect();
        String cookie = null;
        String headerName = null;
        for (int i = 1; (headerName = urlConn.getHeaderFieldKey(i)) != null; i++) {
            if (headerName.equals("Set-Cookie")) {
                cookie = urlConn.getHeaderField(i);
                cookie = cookie.substring(0, cookie.indexOf(";"));
                return cookie;
            }
        }
        Log.BANK_READER.log(Level.WARNING, "the query returned no cookie");
        throw new RuntimeException("no cookies");
    }

    /**
     * @return the time until the next readout can be performed
     */
    public int getRemainingTime() {
        return (int) ((this.lastRead + BankReader.DELTA - System.currentTimeMillis()) / 1000);
    }

    /**
     * reads out the data
     *
     * @param nr
     *            the startentry of the last page that was read
     * @param cookie
     *            the cookie to use to get the data
     * @return a ArrayList of Stringarrays where each Array represents one line
     *         (time,ownAccount,in,amount,out,otherAccout,type,comment,Location, newBalance)
     * @throws IOException
     */
    private ArrayList<String[]> nextQuery(final int nr, final String cookie) throws IOException {
        final URL url = new URL(Variables.BANKQUERY.replaceAll("%d%", String.valueOf(nr)));
        final URLConnection urlConn = url.openConnection();
        urlConn.setRequestProperty("Cookie", cookie);
        urlConn.setRequestProperty("User-Agent", Variables.getAppName());
        urlConn.connect();
        ArrayList<String[]> al = null;
        try (Scanner s = new Scanner(urlConn.getInputStream())) {
            al = extractEntries(s);
        }
        return al;
    }

    /**
     * starts the reading-process
     *
     * @return true if the reading started, false if its still sleeping
     */
    public boolean read() {
        if (this.lastRead + BankReader.DELTA < System.currentTimeMillis()) {
            new Thread(this).start();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void run() {
        this.lastRead = System.currentTimeMillis();
        ArrayList<HashMap<String, Object>> k =
                this.c.executeQuery("SELECT v FROM system WHERE k='lastCheck'");
        if (k == null || k.isEmpty()) {
            Log.BANK_READER.log(Level.WARNING, "no last bank-check availabe.");
            return;
        }
        final Timestamp t = Timestamp.valueOf((String) k.get(0).get("v"));
        k = this.c.executeQuery("SELECT v FROM system WHERE k='lastCheckUser'");
        if (k == null || k.isEmpty()) {
            Log.BANK_READER.log(Level.WARNING, "no last bank-check-user availabe.");
            return;
        }
        final String user = (String) k.get(0).get("v");
        ArrayList<String[]> al = new ArrayList<>();
        String cookie;
        try {
            final URL url = new URL("https://bank.kadcon.de/?token=" + Variables.getKbankToken());
            cookie = firstQuery(url);
        } catch (final IOException e) {
            e.printStackTrace();
            return;
        }
        boolean end = false;
        int nr = -BankReader.STEP_SIZE;
        String nu = null, nt = null;
        while (!end) {
            try {
                al = nextQuery(nr, cookie);
                if (nr == -BankReader.STEP_SIZE) {
                    nt = al.get(0)[0];
                    nu = al.get(0)[5];
                }
            } catch (final IOException e) {
                e.printStackTrace();
                return;
            }
            end = analyseEntries(al, t, user);
            nr += BankReader.STEP_SIZE;
            if (nr > BankReader.MAX_OFFSET) {
                // too many requests, looks like something went wrong
                break;
            }
        }
        this.c.executeUpdate("UPDATE system SET v=? WHERE k='lastcheck'", new String[] { nt });
        this.c.executeUpdate("UPDATE system SET v=? WHERE k='lastCheckUser'", new String[] { nu });
    }
}