package de._125m125.ktv2.mail;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Clock;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.bouncycastle.openpgp.PGPPublicKey;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.EncryptionHelper;
import de._125m125.ktv2.calculators.EncryptionHelper.EncryptedData;
import net.markenwerk.utils.mail.smime.SmimeUtil;
import net.suberic.crypto.EncryptionManager;
import net.suberic.crypto.EncryptionUtils;
import net.suberic.crypto.bouncycastlepgp.BCPGPEncryptionKey;

public class MailManager {
    private static final long MAIL_READ_DELAY = 60000L;
    private final Clock       clock           = Clock.systemDefaultZone();

    public void sendMail(final String receiver, final String subject, final String message)
            throws MailSendException {
        sendMail(receiver, subject, message, EncryptionType.NONE, null);
    }

    public void sendMail(final String receiver, final String subject, final String content,
            final EncryptionType et, final Object certificate) throws MailSendException {
        final long delayStatus = checkAndUpdateEmailDelayStatus(receiver);
        if (delayStatus != 0) {
            if (delayStatus > 0) {
                throw new MailOnDelayException(delayStatus);
            } else if (delayStatus == -1) {
                throw new MailLockedException();
            } else {
                throw new RuntimeException("unexpected delayStatus");
            }
        }

        final Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", Variables.getString("mailHost"));
        props.put("mail.smtp.port", Variables.getString("mailPort"));

        final Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Variables.getString("mailUsername"),
                        Variables.getString("mailPassword"));
            }
        });

        final EncryptedData aesEncrypt = EncryptionHelper.aesEncrypt(receiver);

        final String encodedCypherText =
                Base64.getUrlEncoder().withoutPadding().encodeToString(aesEncrypt.getCyphertext());
        final String encodedIV =
                Base64.getUrlEncoder().withoutPadding().encodeToString(aesEncrypt.getIv());
        final String contentWithUnsub = content.concat(Variables.translate("unsubscribeMail",
                Variables.getCoreUrl(), encodedCypherText, encodedIV));

        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(Variables.getString("mailUsername"),
                    Variables.getAppName() + " 2FA Service"));
            message.setRecipient(RecipientType.TO, new InternetAddress(receiver, true));
            message.setSubject(subject);
            message.setHeader("List-Unsubscribe", "<mailto:" + Variables.getString("mailUsername")
                    + "?subject=Sperrungsanfrage>,<" + Variables.getCoreUrl()
                    + "/p/lockMail?lock=lock&mail=" + encodedCypherText + "&iv=" + encodedIV + ">");
            message.setContent(contentWithUnsub, "text/plain; charset=utf-8");
            switch (et) {
            case NONE:
                break;
            case SMIME:
                message = SmimeUtil.encrypt(session, message, (X509Certificate) certificate);
                break;
            case PGP:
                final EncryptionUtils pgpUtils =
                        EncryptionManager.getEncryptionUtils(EncryptionManager.PGP);
                message = pgpUtils.encryptMessage(session, message,
                        new Key[] { new BCPGPEncryptionKey((PGPPublicKey) certificate) });
                break;
            }
            Transport.send(message);
        } catch (MessagingException | IOException | GeneralSecurityException e) {
            e.printStackTrace();
            throw new MailSendException(e);
        }
    }

    public long checkAndUpdateEmailDelayStatus(final String receiver) {
        try (Connection connection = Main.instance().getConnector().getConnection();
                PreparedStatement st =
                        connection.prepareStatement("SELECT * FROM mailcounter WHERE mail=?")) {
            st.setString(1, receiver);
            try (ResultSet rs = st.executeQuery()) {
                if (!rs.next()) {
                    final PreparedStatement insert = connection.prepareStatement(
                            "INSERT INTO mailcounter(mail,count) VALUES(?,1) ON DUPLICATE KEY UPDATE count=count+1");
                    insert.setString(1, receiver);
                    insert.executeUpdate();
                    return 0;
                }
                if (!rs.getBoolean("active")) {
                    return -1;
                }
                final Timestamp last = rs.getTimestamp("lastSent");
                final int c = rs.getInt("count");
                final long remaining = getNextSend(last, c) - this.clock.millis();
                if (remaining < 0) {
                    final PreparedStatement insert = connection.prepareStatement(
                            "INSERT INTO mailcounter(mail,count) VALUES(?,1) ON DUPLICATE KEY UPDATE count=count+1");
                    insert.setString(1, receiver);
                    insert.executeUpdate();
                    return 0;
                } else {
                    return remaining;
                }
            }
        } catch (final SQLException e) {
            return -2;
        }
    }

    private long getNextSend(final Timestamp last, final int c) {
        final double toAdd = Math.pow(5, c) * 1000;
        return (long) (last.getTime() + toAdd);
    }

    public boolean resetCount(final String receiver) {
        try (Connection connection = Main.instance().getConnector().getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "UPDATE mailcounter SET count=0 WHERE mail=? AND active=TRUE")) {
            st.setString(1, receiver);
            st.executeUpdate();
            return true;
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deactivate(final String receiver) {
        try (Connection connection = Main.instance().getConnector().getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "INSERT INTO mailcounter(mail,active) VALUES(?,FALSE) ON DUPLICATE KEY UPDATE active=FALSE")) {
            st.setString(1, receiver);
            st.executeUpdate();
            return true;
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean activate(final String receiver) {
        try (Connection connection = Main.instance().getConnector().getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "UPDATE mailcounter SET active=TRUE, count=0 WHERE mail=?")) {
            st.setString(1, receiver);
            st.executeUpdate();
            return true;
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized long read() {
        long lastRead = -1, lastMail = -1;
        try (Connection connection = Main.instance().getConnector().getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "SELECT k,v FROM system WHERE k=\"lastMailRead\" OR k=\"lastMailDate\"")) {
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                switch (rs.getString("k")) {
                case "lastMailRead":
                    lastRead = rs.getLong("v");
                    break;
                case "lastMailDate":
                    lastMail = rs.getLong("v");
                    break;
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return Long.MAX_VALUE;
        }
        final long remaining = lastRead + MailManager.MAIL_READ_DELAY - this.clock.millis();
        if (remaining > 0) {
            return remaining;
        }

        long newLastMail = lastMail;
        Folder emailFolder = null;
        Store store = null;
        try {
            final Properties properties = new Properties();

            properties.put("mail.store.protocol", "pop3");
            properties.put("mail.pop3.host", Variables.getString("mailReadHost"));
            properties.put("mail.pop3.port", Variables.getString("mailReadPort"));
            properties.put("mail.pop3.starttls.enable", "true");
            final Session emailSession = Session.getDefaultInstance(properties);

            store = emailSession.getStore("pop3s");
            store.connect(Variables.getString("mailReadHost"), Variables.getString("mailUsername"),
                    Variables.getString("mailPassword"));

            emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);

            // retrieve the messages from the folder in an array and print it
            final Message[] messages = emailFolder.getMessages();

            for (final Message message : messages) {
                if (message.getSentDate().after(new Date(lastMail))) {
                    analyzeMail(message);
                    newLastMail = message.getSentDate().getTime();
                }
            }
        } catch (final MessagingException e) {
            e.printStackTrace();
        }
        // close the store and folder objects
        if (emailFolder != null && emailFolder.isOpen()) {
            try {
                emailFolder.close(false);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        if (store != null) {
            try {
                store.close();
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        try (Connection connection = Main.instance().getConnector().getConnection();
                PreparedStatement st = connection.prepareStatement(
                        "INSERT INTO system(k,v) VALUES(\"lastMailRead\",?),(\"lastMailDate\",?) ON DUPLICATE KEY UPDATE v=VALUES(v)")) {
            st.setLong(1, this.clock.millis());
            st.setLong(2, newLastMail);
            st.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
            return Long.MAX_VALUE;
        }
        return 0;
    }

    private void analyzeMail(final Message message) throws MessagingException {
        switch (message.getSubject().toLowerCase(Locale.ENGLISH)) {
        case "entsperrungsanfrage":
        case "entsperren":
        case "erlauben":
        case "subscribe":
        case "unlock":
            for (final Address address : message.getFrom()) {
                if (address instanceof InternetAddress) {
                    activate(((InternetAddress) address).getAddress());
                }
            }
            break;
        case "sperrungsanfrage":
        case "sperren":
        case "verbieten":
        case "unsubscribe":
        case "lock":
            for (final Address address : message.getFrom()) {
                if (address instanceof InternetAddress) {
                    deactivate(((InternetAddress) address).getAddress());
                }
            }
            break;
        }

    }
}
