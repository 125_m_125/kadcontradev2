package de._125m125.ktv2.mail;

public class MailSendException extends Exception {

    private static final long serialVersionUID = -6008145325201113033L;

    public MailSendException() {
        super();
    }

    public MailSendException(final String message, final Throwable cause, final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MailSendException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public MailSendException(final String message) {
        super(message);
    }

    public MailSendException(final Throwable cause) {
        super(cause);
    }

}
