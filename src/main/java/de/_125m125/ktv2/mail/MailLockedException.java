package de._125m125.ktv2.mail;

public class MailLockedException extends MailSendException {
    private static final long serialVersionUID = 4290497708756665697L;

    public MailLockedException() {
        super();
    }
}
