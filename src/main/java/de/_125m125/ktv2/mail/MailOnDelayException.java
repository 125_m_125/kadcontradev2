package de._125m125.ktv2.mail;

public class MailOnDelayException extends MailSendException {

    private static final long serialVersionUID = -334310693757545198L;
    private final long        nextMessage;

    public MailOnDelayException(final long nextMessage) {
        super(Long.toString(nextMessage, 10));
        this.nextMessage = nextMessage;
    }

    public long getNextMessage() {
        return this.nextMessage;
    }

}
