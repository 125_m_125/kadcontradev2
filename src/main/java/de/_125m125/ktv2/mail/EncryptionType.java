package de._125m125.ktv2.mail;

public enum EncryptionType {
    NONE,
    SMIME,
    PGP
}
