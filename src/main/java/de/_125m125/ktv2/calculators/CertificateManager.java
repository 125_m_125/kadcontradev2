package de._125m125.ktv2.calculators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.Attribute;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.ClientCertificate;
import de._125m125.ktv2.beans.ClientCertificate.Type;
import de._125m125.ktv2.beans.GeneratedCertificate;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class CertificateManager {
    private static final CertificateManager instance = new CertificateManager();

    private final File      crl = new File("/home/kadcontrade/revocations.crl");
    private KeyPair         keyPair;
    private X509Certificate root;

    public static CertificateManager instance() {
        return CertificateManager.instance;
    }

    public synchronized void initialize() throws FileNotFoundException, IOException,
            CertificateEncodingException, NoSuchAlgorithmException, OperatorCreationException,
            CertificateException, NoSuchProviderException {
        if (this.keyPair != null) {
            return;
        }
        final File f = new File("/home/kadcontrade/keyPair.pem");
        final File f2 = new File("/home/kadcontrade/rootCertificate.crt");
        if (f.exists()) {
            this.keyPair = loadPair(f);
        } else {
            if (f2.exists()) {
                throw new IllegalStateException(
                        "keypair is missing but certificate is existing. Aborting");
            } else {
                Log.SYSTEM.log(Level.WARNING, "Root keypair missing. Generating new...");
                this.keyPair = generateKeyPair(f);
                Log.SYSTEM.log(Level.WARNING, "Root keypair generated successfully");
            }
        }
        if (f2.exists()) {
            this.root = loadCertificate(f2);
        } else {
            Log.SYSTEM.log(Level.WARNING, "Root certificate missing. Generating new...");
            this.root = generateRootCertificate(this.keyPair);
            try (JcaPEMWriter pemWriter = new JcaPEMWriter(new FileWriter(f2))) {
                pemWriter.writeObject(this.root);
            }
            Log.SYSTEM.log(Level.WARNING, "Root certificate generated successfully");
        }
    }

    public GeneratedCertificate generateCertificate(final long uid, final String csrString,
            final int duration, final Boolean expect2FA) throws OperatorCreationException,
            NoSuchAlgorithmException, IOException, CertificateException, SQLException {
        final GeneratedCertificate result = new GeneratedCertificate();
        PKCS10CertificationRequest csr;
        try (final Reader reader = new StringReader(csrString);
                PEMParser parser = new PEMParser(reader)) {
            Object readObject;
            readObject = parser.readObject();
            if (readObject instanceof PKCS10CertificationRequest) {
                csr = (PKCS10CertificationRequest) readObject;
            } else {
                throw new IllegalArgumentException("Invalid CSR");
            }
        } catch (final IOException e) {
            throw new IllegalArgumentException("Invalid CSR");
        }

        final BigInteger serial = new BigInteger(159, new SecureRandom());

        final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
        final PublicKey signee = converter.getPublicKey(csr.getSubjectPublicKeyInfo());

        // fill in certificate fields
        final X500Name subject = csr.getSubject();
        final RDN[] rdn = subject.getRDNs(BCStyle.UID);
        if (rdn.length != 1 || rdn[0].isMultiValued()) {
            throw new IllegalArgumentException("missingUID");
        }
        final String valueToString = IETFUtils.valueToString(rdn[0].getFirst().getValue());
        try {
            if (Token.getBigInt(valueToString).longValue() != uid) {
                throw new IllegalArgumentException("notOwnUID");
            }
        } catch (final NumberFormatException e) {
            throw new IllegalArgumentException("invalidUID");
        }
        final X500Name issuer = new JcaX509CertificateHolder(this.root).getSubject();

        final X509v3CertificateBuilder certificate = new JcaX509v3CertificateBuilder(issuer, serial,
                Date.from(LocalDate.now().atStartOfDay(ZoneOffset.UTC).toInstant()),
                Date.from(LocalDate.now().plusDays(duration).atStartOfDay(ZoneOffset.UTC)
                        .toInstant()),
                subject, signee);
        certificate.addExtension(Extension.subjectKeyIdentifier, false,
                new JcaX509ExtensionUtils().createSubjectKeyIdentifier(signee));
        certificate.addExtension(Extension.authorityKeyIdentifier, false,
                new JcaX509ExtensionUtils().createAuthorityKeyIdentifier(this.root));

        final BasicConstraints constraints = new BasicConstraints(false);
        certificate.addExtension(Extension.basicConstraints, false, constraints.getEncoded());

        final Attribute[] proposedUsage =
                csr.getAttributes(PKCSObjectIdentifiers.pkcs_9_at_extensionRequest);
        if (proposedUsage.length != 1) {
            throw new IllegalArgumentException("extendedKeyUsageNotRequested");
        }
        final Extensions extensions =
                Extensions.getInstance(proposedUsage[0].getAttrValues().getObjectAt(0));
        for (final ASN1ObjectIdentifier i : extensions.getCriticalExtensionOIDs()) {
            if (!Extension.extendedKeyUsage.equals(i)) {
                throw new IllegalArgumentException("unknownCriticalExtension " + i.getId());
            }
        }
        final ExtendedKeyUsage suggestedExtendedKeyUsage =
                ExtendedKeyUsage.fromExtensions(extensions);
        boolean clientAuth = false;
        Boolean tfa = null;
        for (final KeyPurposeId keyPurposeId : suggestedExtendedKeyUsage.getUsages()) {
            if (keyPurposeId.equals(KeyPurposeId.id_kp_clientAuth)) {
                clientAuth = true;
                continue;
            }
            final Permission p = Permission.ofOID(keyPurposeId.getId());
            if (p == null || p.isAdmin() || p.isUtility()) {
                throw new IllegalArgumentException(
                        "illegalExtendedkeyUsage " + keyPurposeId.getId());
            }
            if (Permission.TWO_FA_PERMISSION == p) {
                if (Boolean.FALSE.equals(tfa)) {
                    throw new IllegalArgumentException("2faAndAPI");
                }
                tfa = true;
            } else {
                if (Boolean.TRUE.equals(tfa)) {
                    throw new IllegalArgumentException("2faAndAPI");
                }
                tfa = false;
            }
            result.addPermission(p.getComName());
        }
        if (tfa == null || expect2FA != null && !expect2FA.equals(tfa)) {
            throw new IllegalArgumentException("unexpectedPermissions");
        }
        if (!clientAuth) {
            throw new IllegalArgumentException("clientAuthMissing");
        }
        result.setDetails(new ClientCertificate(serial, tfa ? Type.TWO_FACTOR_AUTH : Type.API));

        certificate.addExtension(extensions.getExtension(Extension.extendedKeyUsage));

        // build BouncyCastle certificate
        final ContentSigner cSigner =
                new JcaContentSignerBuilder("SHA256withRSA").build(this.keyPair.getPrivate());
        final X509CertificateHolder holder = certificate.build(cSigner);

        // convert to JRE certificate
        final JcaX509CertificateConverter converter2 = new JcaX509CertificateConverter();
        converter2.setProvider(new BouncyCastleProvider());
        final X509Certificate x509 = converter2.getCertificate(holder);

        result.setCertificate(x509);
        result.setValidity(duration);

        Main.instance().getConnector().executeAtomic(c -> {
            try (PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO certificates(user,serialNr,type) VALUES(?,?,?)")) {
                ps.setLong(1, uid);
                ps.setBytes(2, result.getDetails().getSerialNo().toByteArray());
                ps.setInt(3, result.getDetails().getType().getMysqlId());
                ps.executeUpdate();
            }
            Log.USER.executeLog(c, Level.INFO,
                    "Th user created a new certificate of type "
                            + result.getDetails().getType().getMysqlId() + ": "
                            + result.getDetails().getSerialNo(),
                    uid);
            return () -> true;
        });

        return result;
    }

    private X509Certificate loadCertificate(final File file)
            throws FileNotFoundException, IOException, CertificateException {
        try (final Reader reader = new FileReader(file); PEMParser parser = new PEMParser(reader)) {
            return loadCertificate(parser);
        }
    }

    private X509Certificate loadCertificate(final String s)
            throws FileNotFoundException, IOException, CertificateException {
        try (final Reader reader = new StringReader(s); PEMParser parser = new PEMParser(reader)) {
            return loadCertificate(parser);
        }
    }

    private X509Certificate loadCertificate(final PEMParser parser)
            throws IOException, CertificateException {
        final Object readObject = parser.readObject();
        if (readObject instanceof X509CertificateHolder) {
            return new JcaX509CertificateConverter()
                    .getCertificate((X509CertificateHolder) readObject);
        } else {
            throw new IllegalStateException("Invalid Certificate");
        }
    }

    public int getCertificatePermissions(final long user, final HttpServletRequest request) {
        final CertificateDetails certificateDetails = getCertificateDetails(request);
        return certificateDetails.getUser() == user ? certificateDetails.getPermissions() : 0;
    }

    public CertificateDetails getCertificateDetails(final HttpServletRequest request) {
        final String header = request.getHeader("X-SSL-CERT");
        final long user;
        final BigInteger serial;
        final int permissions;
        if (header == null) {
            System.out.println("noHeader");
            return CertificateDetails.INVALID;
        }
        try {
            final X509Certificate certificate =
                    loadCertificate(URLDecoder.decode(header, StandardCharsets.UTF_8.name()));
            final JcaX509CertificateHolder jcaX509CertificateHolder =
                    new JcaX509CertificateHolder(certificate);
            serial = certificate.getSerialNumber();
            final RDN[] rdn = jcaX509CertificateHolder.getSubject().getRDNs(BCStyle.UID);
            if (rdn.length != 1 || rdn[0].isMultiValued()) {
                System.out.println("noUid");
                return CertificateDetails.INVALID;
            }
            final String valueToString = IETFUtils.valueToString(rdn[0].getFirst().getValue());
            try {
                user = Token.getBigInt(valueToString).longValue();
            } catch (final NumberFormatException e) {
                System.out.println("invalid token");
                return CertificateDetails.INVALID;
            }
            try (Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps = c.prepareStatement(
                            "SELECT type FROM certificates WHERE user=? AND serialNr=?")) {
                ps.setLong(1, user);
                ps.setBytes(2, serial.toByteArray());
                try (ResultSet rs = ps.executeQuery()) {
                    if (!rs.next() || rs.getInt("type") == Type.REVOKED.getMysqlId()) {
                        System.out.println("revoked");
                        return CertificateDetails.INVALID;
                    }
                }
            } catch (final SQLException e) {
                e.printStackTrace();
                return CertificateDetails.INVALID;
            }
            final Extension ext = jcaX509CertificateHolder.getExtension(Extension.extendedKeyUsage);
            if (ext == null) {
                System.out.println("noExt");
                return CertificateDetails.INVALID;
            }
            final ExtendedKeyUsage extKey = ExtendedKeyUsage.getInstance(ext.getParsedValue());
            permissions = Arrays.stream(extKey.getUsages()).map(KeyPurposeId::getId)
                    .map(Permission::ofOID).filter(Objects::nonNull)
                    .reduce(0, (prev, perm) -> perm.appendTo(prev), (a, b) -> a | b);
            return new CertificateDetails(user, serial, permissions);
        } catch (final CertificateException | IOException e) {
            e.printStackTrace();
            System.out.println("invCert");
            return CertificateDetails.INVALID;
        }
    }

    private KeyPair generateKeyPair(final File file)
            throws IOException, NoSuchAlgorithmException, NoSuchProviderException {
        // Create the public and private keys
        final KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");

        final SecureRandom random = new SecureRandom();
        generator.initialize(4096, random);

        final KeyPair pair = generator.generateKeyPair();

        try (JcaPEMWriter pemWriter = new JcaPEMWriter(new FileWriter(file))) {
            pemWriter.writeObject(pair);
        }

        // protect files from accidental modification
        final Set<PosixFilePermission> perms = new HashSet<>();
        perms.add(PosixFilePermission.OWNER_READ);
        Files.setPosixFilePermissions(file.toPath(), perms);

        return pair;
    }

    private KeyPair loadPair(final File path) throws FileNotFoundException, IOException {
        final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
        try (final Reader reader = new FileReader(path); PEMParser parser = new PEMParser(reader)) {
            Object readObject;
            readObject = parser.readObject();
            final KeyPair key;
            if (readObject instanceof PEMKeyPair) {
                key = converter.getKeyPair((PEMKeyPair) readObject);
            } else {
                throw new IllegalStateException("Invalid Key Pair");
            }
            return key;
        }
    }

    private X509Certificate generateRootCertificate(final KeyPair pair)
            throws CertIOException, NoSuchAlgorithmException, CertificateEncodingException,
            IOException, OperatorCreationException, CertificateException {
        final SecureRandom random = new SecureRandom();

        // fill in certificate fields
        final X500Name subject = new X500NameBuilder(RFC4519Style.INSTANCE).addRDN(BCStyle.C, "DE")
                .addRDN(BCStyle.ST, "Hessen").addRDN(BCStyle.L, "Darmstadt")
                .addRDN(BCStyle.O, "125m125 Inc").addRDN(BCStyle.OU, Variables.getAppName())
                .addRDN(BCStyle.CN, Variables.getAppName() + "_Client_Root").build();

        final BigInteger serial = new BigInteger(159, random);
        final X509v3CertificateBuilder certificate = new JcaX509v3CertificateBuilder(subject,
                serial, Date.from(LocalDate.now().atStartOfDay(ZoneOffset.UTC).toInstant()),
                Date.from(LocalDate.now().plusYears(5).atStartOfDay(ZoneOffset.UTC).toInstant()),
                subject, pair.getPublic());
        certificate.addExtension(Extension.subjectKeyIdentifier, false,
                new JcaX509ExtensionUtils().createSubjectKeyIdentifier(pair.getPublic()));
        final KeyUsage ku = new KeyUsage(KeyUsage.keyCertSign | KeyUsage.cRLSign);
        certificate.addExtension(Extension.keyUsage, true, ku);

        final BasicConstraints constraints = new BasicConstraints(true);
        certificate.addExtension(Extension.basicConstraints, true, constraints.getEncoded());

        // build BouncyCastle certificate
        final ContentSigner cSigner =
                new JcaContentSignerBuilder("SHA256withRSA").build(pair.getPrivate());
        final X509CertificateHolder holder = certificate.build(cSigner);

        // convert to JRE certificate
        final JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
        converter.setProvider(new BouncyCastleProvider());
        final X509Certificate x509 = converter.getCertificate(holder);

        return x509;
    }

    public synchronized void revoke(final BigInteger serialNr)
            throws CertificateEncodingException, IOException, OperatorCreationException {
        final X509v2CRLBuilder x509v2crlBuilder =
                new X509v2CRLBuilder(new JcaX509CertificateHolder(this.root).getSubject(),
                        Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        try (final Reader reader = new FileReader(this.crl);
                PEMParser parser = new PEMParser(reader)) {
            final Object readObject = parser.readObject();
            if (readObject instanceof X509CRLHolder) {
                x509v2crlBuilder.addCRL((X509CRLHolder) readObject);
            } else {
                throw new IllegalStateException("Invalid Certificate");
            }
        } catch (final FileNotFoundException e) {
            // no previous crl, so nothing to do
        }
        x509v2crlBuilder.addCRLEntry(serialNr,
                Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)), CRLReason.unspecified);
        final ContentSigner cSigner =
                new JcaContentSignerBuilder("SHA256withRSA").build(this.keyPair.getPrivate());
        final X509CRLHolder build = x509v2crlBuilder.build(cSigner);

        try (JcaPEMWriter pemWriter = new JcaPEMWriter(new FileWriter(this.crl))) {
            pemWriter.writeObject(build);
        }
    }

    public static class CertificateDetails {
        public static CertificateDetails INVALID = new CertificateDetails(0L, BigInteger.ZERO, 0);

        private final long       user;
        private final BigInteger serial;
        private final int        permissions;

        public CertificateDetails(final long user, final BigInteger serial, final int permissions) {
            super();
            this.user = user;
            this.serial = serial;
            this.permissions = permissions;
        }

        public long getUser() {
            return this.user;
        }

        public BigInteger getSerial() {
            return this.serial;
        }

        public int getPermissions() {
            return this.permissions;
        }

        @Override
        public String toString() {
            return "CertificateDetails [user=" + this.user + ", serial=" + this.serial
                    + ", permissions=" + this.permissions + "]";
        }

    }
}
