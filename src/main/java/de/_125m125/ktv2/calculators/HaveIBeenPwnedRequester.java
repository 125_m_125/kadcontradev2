package de._125m125.ktv2.calculators;

import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class HaveIBeenPwnedRequester {
    private final class CacheLoaderExtension extends CacheLoader<String, Map<String, Integer>> {
        @Override
        public Map<String, Integer> load(final String key) throws Exception {
            final Map<String, Integer> count = new HashMap<>();
            final String url = "https://api.pwnedpasswords.com/range/" + key;
            final URLConnection urlConn = new URL(url).openConnection();
            urlConn.setRequestProperty("User-Agent", "kt.125m125.de");
            urlConn.connect();
            try (Scanner scanner = new Scanner(urlConn.getInputStream())) {
                while (scanner.hasNextLine()) {
                    final String s = scanner.nextLine();
                    final String[] split = s.split(":");
                    if (split.length != 2) {
                        continue;
                    }
                    final int currentCount = Integer.parseInt(split[1]);
                    count.put(split[0], currentCount);
                }
            }
            return count;
        }
    }

    public static HaveIBeenPwnedRequester                    INSTANCE = new HaveIBeenPwnedRequester();

    private final LoadingCache<String, Map<String, Integer>> hashList = CacheBuilder.newBuilder()
            .maximumSize(100).build(new CacheLoaderExtension());

    public static void setInstance(final HaveIBeenPwnedRequester requester) {
        HaveIBeenPwnedRequester.INSTANCE = requester;
    }

    protected HaveIBeenPwnedRequester() {

    }

    public int getUseCount(final String password) {
        final String hash = DigestUtils.sha1Hex(password).toUpperCase();
        Map<String, Integer> count;
        try {
            count = this.hashList.get(hash.substring(0, 5));
            return count.getOrDefault(hash.substring(5), 0);
        } catch (final ExecutionException e) {
            e.printStackTrace();
            return 0;
        }
    }

}
