package de._125m125.ktv2.calculators;

import static de._125m125.ktv2.calculators.EncryptionHelper.PasswordResult.STATE.FAILURE;
import static de._125m125.ktv2.calculators.EncryptionHelper.PasswordResult.STATE.SUCCESS;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import org.mindrot.jbcrypt.BCrypt;

import de._125m125.ktv2.Variables;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import de.mkammerer.argon2.Argon2Factory.Argon2Types;

public class EncryptionHelper {
    private static final int          AES_KEYLENGTH = 128;
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    private static final Pattern[] PATTERNS  = { Pattern.compile("[0-9]"), Pattern.compile("[a-z]"),
            Pattern.compile("[A-Z]"), Pattern.compile("[^0-9a-zA-Z]") };
    private static final int       mincount  = 3;
    private static final int       minlength = 6;

    public static PasswordResult create(final String password, final String username) {
        if (password.length() > 1000) {
            return new PasswordResult(FAILURE, null, "passwordTooLong");
        }
        if (password.contains(username)) {
            return new PasswordResult(FAILURE, null, "nameInPassword");
        }
        if (password.length() < EncryptionHelper.minlength) {
            return new PasswordResult(FAILURE, null, "passwordTooShort");
        }
        int value = 0;
        for (final Pattern pattern : EncryptionHelper.PATTERNS) {
            if (pattern.matcher(password).find()) {
                value++;
            }
        }
        if (value < EncryptionHelper.mincount) {
            return new PasswordResult(FAILURE, null, "insecure");
        }
        final String hashpw = hash(password);
        return new PasswordResult(SUCCESS, hashpw, null);
    }

    private static String hash(final String password) {
        final Argon2 argon2 = Argon2Factory.create(Argon2Types.ARGON2i);
        final String hash = argon2.hash(Variables.getOrDefault("argon2Iterations", 40),
                Variables.getOrDefault("argon2Memory", 20480),
                Variables.getOrDefault("argon2Parallelism", 4), password, StandardCharsets.UTF_8);
        return hash;
    }

    public static PasswordCheckResult checkPw(final String candidate, final String hashed) {
        boolean valid = false;
        boolean outdated = false;
        final String[] split = hashed.split("\\$", 2);
        if (split.length < 2) {
            return new PasswordCheckResult(false);
        }
        final String algo = split[1];
        if (algo.startsWith("2")) {
            valid = BCrypt.checkpw(candidate, hashed);
            outdated = true;
        } else if (algo.startsWith("argon2")) {
            final Argon2 argon2 = Argon2Factory.create();
            valid = argon2.verify(hashed, candidate, StandardCharsets.UTF_8);
        }
        if (valid && outdated) {
            return new PasswordCheckResult(valid, hash(candidate));
        } else {
            return new PasswordCheckResult(valid);
        }
    }

    public static class PasswordResult {
        public static enum STATE {
            SUCCESS, FAILURE, WARNING;
        }

        private final STATE  state;
        private final String message;
        private final String hashedPassword;

        public PasswordResult(final STATE state, final String hashedPassword,
                final String message) {
            this.hashedPassword = hashedPassword;
            this.state = state;
            this.message = message;
        }

        public STATE getState() {
            return this.state;
        }

        public String getHashedPassword() {
            return this.hashedPassword;
        }

        public String getMessage() {
            return this.message;
        }

    }

    public static EncryptedData aesEncrypt(final String s) {
        try {
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            final byte[] iv = new byte[cipher.getBlockSize()];

            EncryptionHelper.SECURE_RANDOM.nextBytes(iv);
            cipher.init(Cipher.ENCRYPT_MODE, Variables.getAESSecretKey(), new IvParameterSpec(iv));

            final byte[] encrypted = cipher.doFinal(s.getBytes("UTF-8"));
            return new EncryptedData(encrypted, iv);
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
                | NoSuchPaddingException | InvalidKeyException | IOException
                | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            throw new EncryptionFailedException(e);
        }
    }

    public static String aesDecrypt(final EncryptedData encryptionResult) {
        try {
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

            cipher.init(Cipher.DECRYPT_MODE, Variables.getAESSecretKey(),
                    new IvParameterSpec(encryptionResult.getIv()));

            final byte[] decrypted = cipher.doFinal(encryptionResult.getCyphertext());
            return new String(decrypted, "UTF-8");
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
                | NoSuchPaddingException | InvalidKeyException | IOException
                | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            throw new EncryptionFailedException(e);
        }
    }

    public static class EncryptionFailedException extends RuntimeException {

        private static final long serialVersionUID = 774587489648238690L;

        public EncryptionFailedException() {
            super();
        }

        public EncryptionFailedException(final String message, final Throwable cause,
                final boolean enableSuppression, final boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }

        public EncryptionFailedException(final String message, final Throwable cause) {
            super(message, cause);
        }

        public EncryptionFailedException(final String message) {
            super(message);
        }

        public EncryptionFailedException(final Throwable cause) {
            super(cause);
        }

    }

    public static class EncryptedData {
        private final byte[] cyphertext;
        private final byte[] iv;

        public EncryptedData(final byte[] cyphertext, final byte[] iv) {
            super();
            this.cyphertext = cyphertext;
            this.iv = iv;
        }

        public byte[] getCyphertext() {
            return this.cyphertext;
        }

        public byte[] getIv() {
            return this.iv;
        }
    }

    public static class PasswordCheckResult {
        private final boolean          valid;
        private final Optional<String> rehashedPassword;

        public PasswordCheckResult(final boolean valid, final String rehashedPassword) {
            super();
            this.valid = valid;
            this.rehashedPassword = Optional.ofNullable(rehashedPassword);
        }

        public PasswordCheckResult(final boolean valid) {
            this(valid, null);
        }

        public boolean isValid() {
            return this.valid;
        }

        public boolean isOutdated() {
            return this.rehashedPassword.isPresent();
        }

        public Optional<String> getRehashedPassword() {
            return this.rehashedPassword;
        }

    }
}
