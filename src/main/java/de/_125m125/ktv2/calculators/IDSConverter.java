package de._125m125.ktv2.calculators;

public class IDSConverter {

    public static long doubleToLong(final double d) {
        return (long) (d * 1000000);
    }

    public static double longToDouble(final long amount) {
        return amount / 1000000.0;
    }

    public static String longToDoubleString(final long amount) {
        final long absAmount = Math.abs(amount);
        String tmp = String.valueOf(absAmount);
        if (absAmount >= 1000000) {
            tmp = tmp.substring(0, tmp.length() - 6) + "." + tmp.substring(tmp.length() - 6);
        } else {
            switch (tmp.length()) {
            case 1:
                tmp = "0.00000" + tmp;
                break;
            case 2:
                tmp = "0.0000" + tmp;
                break;
            case 3:
                tmp = "0.000" + tmp;
                break;
            case 4:
                tmp = "0.00" + tmp;
                break;
            case 5:
                tmp = "0.0" + tmp;
                break;
            case 6:
                tmp = "0." + tmp;
                break;
            default:
                tmp = "0";
            }
        }
        if (amount < 0) {
            tmp = "-".concat(tmp);
        }
        return tmp;
    }

    public static String longToMachineDoubleString(final long amount) {
        final long absAmount = Math.abs(amount);
        String tmp = String.valueOf(absAmount);
        if (absAmount >= 1000000) {
            tmp = tmp.substring(0, tmp.length() - 6) + "."
                    + tmp.substring(tmp.length() - 6, tmp.length() - 2);
        } else {
            switch (tmp.length()) {
            case 1:
                tmp = "0";
                break;
            case 2:
                tmp = "0";
                break;
            case 3:
                tmp = "0.000" + tmp.substring(0, 1);
                break;
            case 4:
                tmp = "0.00" + tmp.substring(0, 2);
                break;
            case 5:
                tmp = "0.0" + tmp.substring(0, 3);
                break;
            case 6:
                tmp = "0." + tmp.substring(0, 4);
                break;
            default:
                tmp = "0";
            }
        }
        if (amount < 0 && !tmp.equals("0")) {
            tmp = "-".concat(tmp);
        }
        return tmp;
    }

    public static long stringToLong(final String ppi) {
        return IDSConverter.stringToLong(ppi, 6);
    }

    public static long stringToLong(String ppi, final long max) {
        ppi = ppi.replaceFirst("(?<=\\.[0-9]{2})0+$", "");
        int i = ppi.indexOf(",");
        if (i == -1) {
            i = ppi.indexOf(".");
            if (i == -1) {
                return Long.parseLong(ppi) * (int) Math.pow(10, 6);
            } else {
                ppi = ppi.replaceFirst("\\.", "");
            }
        } else {
            ppi = ppi.replaceFirst(",", "");
        }
        if (i < ppi.length() - max) {
            throw new NumberFormatException(
                    "Der eingegebene String ist zu ungenau. Er darf maximal " + max
                            + " Nachkommastellen haben.");
        }
        final int toFill = 6 - (ppi.length() - i);
        switch (toFill) {
        case 1:
            ppi = ppi.concat("0");
            break;
        case 2:
            ppi = ppi.concat("00");
            break;
        case 3:
            ppi = ppi.concat("000");
            break;
        case 4:
            ppi = ppi.concat("0000");
            break;
        case 5:
            ppi = ppi.concat("00000");
            break;
        }
        return Long.parseLong(ppi);
    }
}
