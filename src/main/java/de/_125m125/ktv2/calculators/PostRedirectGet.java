package de._125m125.ktv2.calculators;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PostRedirectGet {
    public static void execute(final HttpServletRequest req, final HttpServletResponse res, final String... ignore)
            throws IOException {
        final Map<String, String[]> params = extractParameters(req, ignore);
        execute(req, res, params);
    }

    public static void execute(final HttpServletRequest req, final HttpServletResponse res,
            final Map<String, String[]> params) throws IOException {
        final HttpSession session = req.getSession(false);
        if (session != null) {
            if (req.getAttribute("error") != null && session.getAttribute("error") == null) {
                session.setAttribute("error", req.getAttribute("error"));
            }
            if (req.getAttribute("info") != null && session.getAttribute("info") == null) {
                session.setAttribute("info", req.getAttribute("info"));
            }
        } else {
            if (req.getAttribute("error") != null) {
                params.putIfAbsent("error", new String[] { (String) req.getAttribute("error") });
            }
            if (req.getAttribute("info") != null) {
                params.putIfAbsent("info", new String[] { (String) req.getAttribute("info") });
            }
        }

        final String red = req.getRequestURI().concat(computeParams(params));

        res.setStatus(HttpServletResponse.SC_SEE_OTHER);
        res.setHeader("Location", red);
    }

    private static Map<String, String[]> extractParameters(final HttpServletRequest req, final String... ignore) {
        final Map<String, String[]> params = new HashMap<>(req.getParameterMap());
        params.remove("csrfPreventionSalt");
        for (final String s : ignore) {
            params.remove(s);
        }
        return params;
    }

    private static String computeParams(final Map<String, String[]> params) {
        final StringBuilder sb = new StringBuilder("?");
        for (final Entry<String, String[]> e : params.entrySet()) {
            sb.append(e.getKey());
            sb.append("=");
            sb.append(e.getValue()[e.getValue().length - 1]);
            sb.append("&");
        }
        return sb.toString();
    }
}
