package de._125m125.ktv2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class UserHelper {
    private static HashMap<String, Long> nameToId = new HashMap<>();
    private static HashMap<Long, String> idToName = new HashMap<>();

    public static long get(final String name) {
        Long k = UserHelper.nameToId.get(name);
        if (k != null) {
            return k;
        }
        k = Main.instance().getConnector().getID(name);
        if (k != -1) {
            UserHelper.nameToId.put(name, k);
            UserHelper.idToName.put(k, name);
        }
        return k;
    }

    public static String get(final long uid) {
        String name = UserHelper.idToName.get(uid);
        if (name != null) {
            return name;
        }
        name = Main.instance().getConnector().getName(uid);
        if (name != null) {
            UserHelper.nameToId.put(name, uid);
            UserHelper.idToName.put(uid, name);
        }
        return name;
    }

    public static boolean isAdmin(final long uid) {
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement("SELECT TRUE FROM admin WHERE id=?")) {
            ps.setLong(1, uid);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next() && rs.getBoolean(1);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
