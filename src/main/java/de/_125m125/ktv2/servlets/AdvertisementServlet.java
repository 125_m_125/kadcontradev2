package de._125m125.ktv2.servlets;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.ThreadManager;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.servlets.apiv2.HistoryRestAPI;

@WebServlet(urlPatterns = { "/static/advertisements/ad.png" })
public class AdvertisementServlet extends HttpServlet {
    private static BufferedImage       BASE_IMAGE;
    private static final int           X_START = 150;
    private static final int           X_END   = 550;
    private static final int           Y_START = 100;
    private static final int           Y_END   = 150;
    private static final NumberFormat  nf      = NumberFormat.getNumberInstance(Locale.GERMANY);
    private static final DecimalFormat nf2     =
            (DecimalFormat) NumberFormat.getPercentInstance(Locale.GERMANY);

    private static final class ImageGenerator extends CacheLoader<String, BufferedImage> {
        @Override
        public BufferedImage load(final String key) throws Exception {
            final String[] items = key.split(";");
            final int x_offset = (AdvertisementServlet.X_END - AdvertisementServlet.X_START) / 3;
            final BufferedImage result = new BufferedImage(
                    AdvertisementServlet.BASE_IMAGE.getWidth(),
                    AdvertisementServlet.BASE_IMAGE.getHeight(), BufferedImage.TYPE_INT_ARGB);
            final Graphics g = result.createGraphics();
            g.drawImage(AdvertisementServlet.BASE_IMAGE, 0, 0, null);
            try (Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps = c.prepareStatement(
                            "SELECT open,value FROM history LEFT JOIN (SELECT (time + INTERVAL 1 DAY) AS time,type,material,value as open FROM history) AS open USING(time,type,material) WHERE type=1 AND material=? ORDER BY time DESC LIMIT 1")) {
                for (int i = 0; i < items.length; i++) {
                    final String s = items[i];
                    ps.setString(1, s);
                    try (ResultSet rs = ps.executeQuery()) {
                        long value = -1;
                        double change = 0;
                        if (rs.next()) {
                            value = rs.getLong("value");
                            if (rs.wasNull()) {
                                value = -1;
                            }
                            final long value2 = rs.getLong("open");
                            if (!rs.wasNull()) {
                                change = ((double) (value - value2)) / value2;
                            }
                        }
                        final BufferedImage img = getItemImage(s);
                        g.drawImage(img, AdvertisementServlet.X_START + i * x_offset,
                                AdvertisementServlet.Y_START + 5,
                                AdvertisementServlet.Y_END - AdvertisementServlet.Y_START - 10,
                                AdvertisementServlet.Y_END - AdvertisementServlet.Y_START - 10,
                                null);
                        final String valueText = (value < 0 ? "??"
                                : AdvertisementServlet.nf.format(value / 1000000.0)) + "K";
                        g.setColor(Color.BLACK);
                        g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
                        g.drawString(valueText, AdvertisementServlet.X_START + i * x_offset
                                + AdvertisementServlet.Y_END - AdvertisementServlet.Y_START - 10,
                                AdvertisementServlet.Y_START + 20);
                        String changeString;
                        if (change > 0) {
                            changeString = AdvertisementServlet.nf2.format(change);
                            g.setColor(Color.GREEN);
                        } else if (change < 0) {
                            changeString = AdvertisementServlet.nf2.format(change);
                            g.setColor(Color.RED);
                        } else {
                            changeString = "+-0%";
                            g.setColor(Color.BLUE);
                        }
                        g.drawString(changeString, AdvertisementServlet.X_START + i * x_offset
                                + AdvertisementServlet.Y_END - AdvertisementServlet.Y_START - 10,
                                AdvertisementServlet.Y_END - 5);

                    }
                }
                g.dispose();
            }
            return result;
        }

        private BufferedImage getItemImage(final String s) throws IOException {
            String name;
            if (s.contains(":")) {
                name = s.replace(":", "-") + ".png";
            } else {
                name = s + "-0.png";
            }
            final File f1 = new File(ItemimageServlet.SOURCE_FOLDER + name);
            if (f1.exists()) {
                return ImageIO.read(new FileInputStream(f1));

            } else {
                final URL url =
                        ThreadManager.context.getResource(ItemimageServlet.RESOURCE_BASE + name);
                if (url != null) {
                    return ImageIO.read(ThreadManager.context
                            .getResourceAsStream(ItemimageServlet.RESOURCE_BASE + name));
                } else {
                    return ImageIO
                            .read(ThreadManager.context.getResourceAsStream(ItemimageServlet.ALT));
                }
            }
        }
    }

    private static final CacheLoader<String, BufferedImage>  imageGenerator = new ImageGenerator();
    private static final LoadingCache<String, BufferedImage> imageCache     =
            CacheBuilder.newBuilder().maximumSize(50).build(AdvertisementServlet.imageGenerator);

    static {
        try {
            AdvertisementServlet.BASE_IMAGE =
                    ImageIO.read(AdvertisementServlet.class.getResourceAsStream("ktad.png"));
        } catch (final IOException e) {
        }
        AdvertisementServlet.nf.setMaximumFractionDigits(2);
        AdvertisementServlet.nf.setGroupingUsed(false);
        AdvertisementServlet.nf2.setMaximumFractionDigits(2);
        AdvertisementServlet.nf2.setGroupingUsed(false);
        AdvertisementServlet.nf2.setPositivePrefix("+");
    }

    private String[]  defaultItems;
    private LocalDate lastDefaultItemsUpdate;

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        if (AdvertisementServlet.BASE_IMAGE == null) {
            resp.sendError(500, Variables.translate("advertisementsNotAvailable"));
        }
        String[] items = req.getParameterValues("items");
        if (items != null && items.length > 0) {
            for (final String s : items) {
                if (!Variables.resourceExists(s)) {
                    resp.sendError(422, Variables.translate("unknownMaterial") + ": " + s);
                }
            }
        } else {
            items = getDefaultItemList();

        }
        final String timeString = req.getParameter("delay");
        long time = 300_000;
        if (!Strings.isNullOrEmpty(timeString)) {
            try {
                time = Long.parseLong(timeString);
                if (time < 1) {
                    resp.sendError(400, Variables.translate("invalidDelay"));
                }
            } catch (final NumberFormatException e) {
                resp.sendError(400, Variables.translate("invalidDelay"));
            }
        }
        final int c = Math.min(3, items.length);
        final long cTime = System.currentTimeMillis() / time;
        final StringBuilder itemString = new StringBuilder();
        if (items.length < 4) {
            for (int i = 0; i < c; i++) {
                itemString.append(items[i]).append(";");
            }
        } else {
            for (int i = 0; i < c; i++) {
                itemString.append(items[(int) ((cTime + i) % items.length)]).append(";");
            }
        }
        itemString.deleteCharAt(itemString.length() - 1);
        final BufferedImage img =
                AdvertisementServlet.imageCache.getUnchecked(itemString.toString());
        resp.setContentType("image/png");
        resp.setHeader("cache-control", "public, max-age=" + time / 1000);
        try (OutputStream os = resp.getOutputStream()) {
            ImageIO.write(img, "png", os);
        }
    }

    private synchronized String[] getDefaultItemList() {
        if (LocalDate.now().equals(this.lastDefaultItemsUpdate)) {
            return this.defaultItems;
        }
        this.defaultItems = Variables.ID_TO_DN.keySet().stream()
                .filter(i -> HistoryRestAPI.getLatestIfPresent(i).isPresent())
                .toArray(String[]::new);
        return this.defaultItems;
    }

}
