package de._125m125.ktv2.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.io.ByteStreams;

@WebServlet(urlPatterns = { "/static/img/itemimages/*" })
public class ItemimageServlet extends HttpServlet {

    public static final String SOURCE_FOLDER = "/home/kadcontrade/itemimages/";
    public static final String RESOURCE_BASE = "/static/img/itemimages2/";

    public static final String ALT           = "/static/img/missingItemimage.png";

    public static final int    URI_OFFSET    = "/static/img/itemimages/".length();

    @Override
    public long getLastModified(final HttpServletRequest request) {
        final String filename = request.getRequestURI()
                .substring(request.getContextPath().length() + ItemimageServlet.URI_OFFSET);

        final File f1 = new File(ItemimageServlet.SOURCE_FOLDER + filename);
        if (f1.exists()) {
            final long lastModified = f1.lastModified();
            if (lastModified == 0) {
                return -1;
            } else {
                return lastModified;
            }
        }
        final URL url = ItemimageServlet.class.getResource(ItemimageServlet.RESOURCE_BASE + filename);
        if (url != null) {
            try {
                final long lastModified = url.openConnection().getLastModified();
                if (lastModified == 0) {
                    return -1;
                } else {
                    return lastModified;
                }
            } catch (final IOException e) {
                return -1;
            }
        }

        return -1;
    }

    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String filename = request.getRequestURI()
                .substring(request.getContextPath().length() + ItemimageServlet.URI_OFFSET);
        if (!filename.equals("coinstack.png") && !filename.matches("^-?[a-zA-Z0-9_]+\\-[0-9]+\\.png$")) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        final File f1 = new File(ItemimageServlet.SOURCE_FOLDER + filename);
        if (f1.exists()) {
            response.setContentType("image/png");
            final OutputStream os = response.getOutputStream();
            try (final InputStream is = new FileInputStream(f1)) {
                ByteStreams.copy(is, os);
            }

        } else {
            final URL url = getServletContext().getResource(ItemimageServlet.RESOURCE_BASE + filename);
            if (url != null) {
                request.getRequestDispatcher(ItemimageServlet.RESOURCE_BASE + filename).forward(request, response);
            } else {
                request.getRequestDispatcher(ItemimageServlet.ALT).forward(request, response);
            }
        }
    }
}
