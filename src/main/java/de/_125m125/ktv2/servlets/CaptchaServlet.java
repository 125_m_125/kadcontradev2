package de._125m125.ktv2.servlets;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.logging.Log;

@WebServlet("/captcha")
public class CaptchaServlet extends HttpServlet {
    private static final String                                    VERIFY_URL     = "https://www.google.com/recaptcha/api/siteverify";
    private static final int                                       COOKIE_MAX_AGE = 30 * 24 * 60
            * 60;
    private static final int                                       CLEANUP_DELAY  = 100;
    private static final Random                                    RANDOM         = new Random();

    private static final ConcurrentHashMap<String, CaptchadEntity> captchad       = new ConcurrentHashMap<>();
    private static int                                             nextCleanup    = CaptchaServlet.CLEANUP_DELAY;

    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final CaptchadEntity ce = getIfCaptchad(request.getRemoteAddr());
        if (ce == null) {
            response.sendRedirect(getServletContext().getContextPath() + "/index");
            return;
        }
        response.setStatus(429);
        Cookie captchaCookie = null;
        if (request.getCookies() != null) {
            for (final Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("allowsCaptcha")) {
                    captchaCookie = cookie;
                    break;
                }
            }
        }
        final HttpSession s = request.getSession();
        if (s.getAttribute("csrfPreventionSalt") == null) {
            s.setAttribute("csrfPreventionSalt",
                    new BigInteger(130, CaptchaServlet.RANDOM).toString(32));
        }
        request.setAttribute("allowsCaptcha",
                captchaCookie != null && "1".equals(captchaCookie.getValue()));
        request.setAttribute("captchaReason", ce.reason);
        request.setAttribute("reCaptchaSiteKey", Variables.getOrDefault("reCaptchaSiteKey", ""));
        getServletContext().getRequestDispatcher("/captcha.jsp").forward(request, response);
    }

    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("submitAccept") != null
                && request.getParameter("accept") != null) {
            final Cookie cookie = new Cookie("allowsCaptcha", "1");
            cookie.setMaxAge(CaptchaServlet.COOKIE_MAX_AGE);
            response.addCookie(cookie);
        } else if (request.getParameter("submit") != null) {
            if (isValid(request.getParameter("g-recaptcha-response"))) {
                CaptchaServlet.captchad.remove(request.getRemoteAddr());
                response.sendRedirect(getServletContext().getContextPath() + "/index");
                return;
            }
        }
        PostRedirectGet.execute(request, response, "submitAccept", "accept", "submit",
                "g-recaptcha-response");
    }

    public static boolean isValid(final String response) {
        if (response == null || "".equals(response)) {
            return false;
        }
        final String postParams = "secret=" + Variables.getOrDefault("reCaptchaSecretKey", "")
                + "&response=" + response;
        HttpsURLConnection con = null;
        try {
            final URL url = new URL(CaptchaServlet.VERIFY_URL);
            con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);

            try (OutputStream os = con.getOutputStream();
                    DataOutputStream dos = new DataOutputStream(os)) {
                dos.writeBytes(postParams);
            }
            try (InputStream is = con.getInputStream();
                    InputStreamReader in = new InputStreamReader(is)) {
                final JsonObject parse = (JsonObject) new JsonParser().parse(in);
                return parse.get("success").getAsBoolean();
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isCaptchad(final String ip) {
        final CaptchadEntity ce = CaptchaServlet.captchad.get(ip);
        if (ce == null) {
            return false;
        }
        if (ce.expires < System.currentTimeMillis()) {
            CaptchaServlet.captchad.remove(ce);
            return false;
        }
        return true;
    }

    private static CaptchadEntity getIfCaptchad(final String ip) {
        final CaptchadEntity ce = CaptchaServlet.captchad.get(ip);
        if (ce == null) {
            return null;
        }
        if (ce.expires < System.currentTimeMillis()) {
            CaptchaServlet.captchad.remove(ce);
            return null;
        }
        return ce;
    }

    public static void captcha(final String ip, final long expires, final String reason) {
        synchronized (CaptchaServlet.class) {
            if (--CaptchaServlet.nextCleanup == 0) {
                cleanup();
                CaptchaServlet.nextCleanup = CaptchaServlet.CLEANUP_DELAY;
            }
        }
        final CaptchadEntity newCe = new CaptchadEntity(ip, expires, reason);
        final CaptchadEntity ce = CaptchaServlet.captchad.putIfAbsent(ip, newCe);
        if (ce != null && ce.expires < newCe.expires) {
            CaptchaServlet.captchad.put(ip, newCe);
        }
    }

    public static void captcha(final String ip, final int duration, final String reason) {
        captcha(ip, System.currentTimeMillis() + duration, reason);
    }

    public static void captcha(final HttpServletRequest request, final HttpServletResponse response,
            final long expires, final String reason) throws IOException {
        captcha(request.getRemoteAddr(), expires, reason);
        response.sendRedirect(request.getServletContext().getContextPath() + ("/captcha"));
    }

    public static void captcha(final HttpServletRequest request, final HttpServletResponse response,
            final int duration, final String reason) throws IOException {
        captcha(request, response, System.currentTimeMillis() + duration, reason);
    }

    private static void cleanup() {
        Log.WEBSOCKET.log(Level.FINE, "Captcha cleanup started");
        final long current = System.currentTimeMillis();
        for (final Iterator<Entry<String, CaptchadEntity>> iterator = CaptchaServlet.captchad
                .entrySet().iterator(); iterator.hasNext();) {
            final Entry<String, CaptchadEntity> entry = iterator.next();
            if (entry.getValue().expires < current) {
                iterator.remove();
            }
        }
        Log.WEBSOCKET.log(Level.FINE, "Captcha cleanup fnished");
    }

    private static class CaptchadEntity {
        private final String ip;
        private final long   expires;
        private final String reason;

        private CaptchadEntity(final String ip, final long expires, final String reason) {
            this.ip = ip;
            this.expires = expires;
            this.reason = reason;
        }
    }
}
