package de._125m125.ktv2.servlets;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.servlets.api.helper.MarketHelper;
import de._125m125.ktv2.servlets.apiv2.TradeRestAPI;

/**
 * Servlet implementation class MarketServlet
 */
@WebServlet(urlPatterns = { "/markt" })
public class MarketServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MarketServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        try {
            request.setAttribute("tradeslots",
                    TradeRestAPI.get(String.valueOf(request.getSession().getAttribute("id"))));
        } catch (final ExecutionException e) {
            request.setAttribute("error", Variables.translate("DataCollecionError"));
            e.printStackTrace();
        }

        final ServletContext sc = getServletContext();
        final RequestDispatcher rd = sc.getRequestDispatcher("/itemmarket.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("id", request.getSession().getAttribute("id"));
        MarketHelper.processPost(request, null);

        PostRedirectGet.execute(request, response, "tradeid", "create", "cancel");
    }

}
