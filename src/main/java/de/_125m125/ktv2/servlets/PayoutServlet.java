package de._125m125.ktv2.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.servlets.api.helper.PayoutHelper;

/**
 * Servlet implementation class PayoutServlet
 */
@WebServlet(urlPatterns = { "/auszahlung" })
public class PayoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayoutServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String uid = String.valueOf(request.getSession().getAttribute("id"));
        final ArrayList<HashMap<String, Object>> methods = Main.instance().getConnector()
                .executeQuery("SELECT server FROM payoutboxes WHERE owner=? AND verified=TRUE", uid);
        final boolean[] supportedType = new boolean[4];
        if (methods != null) {
            for (final HashMap<String, Object> method : methods) {
                switch ((Integer) method.get("server")) {
                case 1:
                    supportedType[1] = true;
                    break;
                case 2:
                    supportedType[2] = true;
                    break;
                case 3:
                    supportedType[3] = true;
                    break;
                }
            }
        }
        final Object column = Main.instance().getConnector().getColumn("payoutsettings", "0", "uid=?", uid);
        if (column != null) {
            supportedType[0] = true;
        }
        request.setAttribute("payoutTypes", supportedType);

        request.getRequestDispatcher("/payout.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("id", request.getSession().getAttribute("id"));
        PayoutHelper.processPost(request, response);
        PostRedirectGet.execute(request, response, "id", "cancel", "payout", "takeout");
    }

}
