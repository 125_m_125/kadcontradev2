package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.servlets.admin.helper.LogQueryBuilder;

@WebServlet(urlPatterns = { "/admin/log/ajax" })
public class LogAjaxServlet extends HttpServlet {

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final JsonObjectBuilder result = Json.createObjectBuilder();

        final String draw = request.getParameter("draw");
        if (draw != null) {
            result.add("draw", draw);
        }
        final LogQueryBuilder lqb = new LogQueryBuilder();
        try {
            final int limit = Integer.parseInt(request.getParameter("length"));
            lqb.setLimit(Math.min(100, Math.max(1, limit)));
        } catch (final NumberFormatException e1) {
        }
        try {
            final int offset = Integer.parseInt(request.getParameter("start"));
            lqb.setOffset(Math.max(0, offset));
        } catch (final NumberFormatException e1) {
        }
        try {
            final long uid = Integer.parseInt(request.getParameter("uid"));
            lqb.setUid(uid);
        } catch (final NumberFormatException e1) {
        }
        try {
            final int level = Integer.parseInt(request.getParameter("level"));
            lqb.setMinLevel(level);
        } catch (final NumberFormatException e1) {
        }
        final String[] types = request.getParameterValues("types[]");
        if (types != null) {
            lqb.setTypes(types);
        }

        final ArrayList<HashMap<String, Object>> entries = lqb.execute();
        final JsonArrayBuilder queryResult = Json.createArrayBuilder();
        if (entries != null) {
            for (final HashMap<String, Object> entry : entries) {
                final JsonObjectBuilder entryJson = Json.createObjectBuilder();
                for (final Entry<String, Object> e : entry.entrySet()) {
                    entryJson.add(e.getKey(), String.valueOf(e.getValue()));
                }
                queryResult.add(entryJson);
            }
        }
        result.add("data", queryResult);
        result.add("recordsFiltered", lqb.getRowCount());
        final String resultString = result.build().toString();
        response.getWriter().append(resultString);
    }
}
