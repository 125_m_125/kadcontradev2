package de._125m125.ktv2.servlets.admin.helper;

import java.util.ArrayList;

import de._125m125.ktv2.servlets.helper.AjaxQueryBuilder;

public class CouponQueryBuilder extends AjaxQueryBuilder {

    public CouponQueryBuilder() {
        super("giftcodes", "preCode,code");
    }

    private String search;

    public CouponQueryBuilder setSearch(final String search) {
        this.search = search;
        return this;
    }

    @Override
    protected ArrayList<String> createWhereContent() {
        final StringBuilder query = new StringBuilder();
        final ArrayList<String> params = new ArrayList<>();
        if (this.search != null) {
            if (this.search.length() < 2) {
                query.append("WHERE preCode LIKE ? OR ");
                params.add(this.search + "%");
            } else if (this.search.length() == 2) {
                query.append("WHERE preCode = ? OR ");
                params.add(this.search);
            } else {
                query.append("WHERE ");
            }
            query.append("material LIKE ?");
            params.add(this.search + "%");
        }
        params.add(0, query.toString());
        return params;
    }

}
