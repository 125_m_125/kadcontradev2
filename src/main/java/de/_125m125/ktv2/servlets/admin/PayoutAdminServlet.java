package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;

import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonParseException;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Payout;
import de._125m125.ktv2.cache.payout.PayoutBuilder;
import de._125m125.ktv2.cache.payout.PayoutSettings;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.SettingsServlet;
import de._125m125.ktv2.servlets.apiv2.PayoutRestAPI;

@WebServlet(urlPatterns = { "/admin/payouts" })
public class PayoutAdminServlet extends HttpServlet {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAILURE = "FAILURE";

    private static final Object lock    = new Object();

    private final Clock         clock   = Clock.systemDefaultZone();

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final LocalDateTime currentTime = LocalDateTime.now(this.clock);
        if (currentTime.getHour() < 1 || currentTime.getHour() >= 23) {
            request.setAttribute("error", Variables.translate("noPayoutsBetween23And1"));
            request.getRequestDispatcher("/empty.jsp").forward(request, response);
            return;
        }
        final long payoutCount = (long) Main.instance().getConnector().getColumn("payout",
                "COUNT(*)", "requiresAdminInteraction=TRUE");
        request.setAttribute("totalPayoutCount", payoutCount);

        final List<Payout> inProgress = new ArrayList<>();

        final ArrayList<HashMap<String, Object>> k = Main.instance().getConnector().executeQuery(
                "SELECT * FROM payout WHERE adminInteractionCompleted=FALSE AND requiresAdminInteraction=FALSE AND agent=?",
                String.valueOf(request.getSession().getAttribute("id")));

        if (k != null && k.size() > 0) {
            for (final HashMap<String, Object> hashMap : k) {
                final Payout payout = PayoutBuilder.createPayoutFromHashMap(hashMap);
                inProgress.add(payout);
            }
            request.setAttribute("payout", inProgress);
            final PayoutSettings payoutSettings =
                    SettingsServlet.getPayoutSettingsCache().get(inProgress.get(0).getUid());
            request.setAttribute("servantId", payoutSettings.getUid());
            request.setAttribute("servantName", UserHelper.get(payoutSettings.getUid()));
            switch (inProgress.get(0).getPayoutType()) {
            case "lieferung":
                request.setAttribute("settings", payoutSettings);
                break;
            case "bs1":
                request.setAttribute("settings", payoutSettings.getPayoutboxes()[1]);
                break;
            case "bs2":
                request.setAttribute("settings", payoutSettings.getPayoutboxes()[2]);
                break;
            case "bs3":
                request.setAttribute("settings", payoutSettings.getPayoutboxes()[3]);
                break;
            }
        }

        request.getRequestDispatcher("/admin/payoutAdmin.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("request") != null) {
            handleRequest(request, response);
            // } else if (request.getParameter("update") != null) {
            // handleUpdate(request, response);
        } else if (request.getParameter("partialUpdate") != null) {
            handlePartialUpdate(request, response);
        } else {
            request.getSession().setAttribute("error", Variables.translate("unknownAction"));
        }
        PostRedirectGet.execute(request, response, "request", "partialUpdate", "materials");
    }

    private void handlePartialUpdate(final HttpServletRequest request,
            final HttpServletResponse response) {
        final String materials = request.getParameter("materials");
        if (materials == null) {
            request.getSession().setAttribute("error", Variables.translate("noMaterialList"));
            return;
        }
        final JsonReader reader = Json.createReader(new StringReader(materials));
        JsonObject changes;
        try {
            changes = reader.readObject();
        } catch (JsonException | JsonParseException e) {
            request.getSession().setAttribute("error", Variables.translate("invalidMaterialList"));
            return;
        }
        final long adminUid = (long) request.getSession().getAttribute("id");
        final String userid = String.valueOf(adminUid);
        String message = request.getParameter("message");
        final String newState = request.getParameter("state");
        if (newState == null) {
            request.getSession().setAttribute("error", Variables.translate("invalidPayoutState"));
            return;
        }
        if (!PayoutAdminServlet.SUCCESS.equals(newState.toUpperCase())
                && !PayoutAdminServlet.FAILURE.equals(newState.toUpperCase())) {
            request.getSession().setAttribute("error", Variables.translate("invalidPayoutState"));
            return;
        }
        if (message == null) {
            message = "";
        } else if (message.length() > 90) {
            request.getSession().setAttribute("error", Variables.translate("messageTooLong"));
            return;
        }
        final String finalMessage = message.replaceAll("\t", " ");

        synchronized (PayoutAdminServlet.lock) {
            final ArrayList<HashMap<String, Object>> openRequests =
                    Main.instance().getConnector().executeQuery(
                            "SELECT * FROM payout WHERE agent=? AND userInteraction=FALSE AND success=FALSE AND adminInteractionCompleted=FALSE AND requiresAdminInteraction=FALSE",
                            userid);
            if (openRequests == null || openRequests.size() == 0) {
                request.getSession().setAttribute("error",
                        Variables.translate("invalidPayoutState"));
                return;
            }
            final List<String> payoutids = new ArrayList<>();
            final long payoutOwner = (long) openRequests.get(0).get("uid");
            final Successindicator executeAtomic =
                    Main.instance().getConnector().executeAtomic(c -> {
                        for (final HashMap<String, Object> entry : openRequests) {
                            final String id = String.valueOf(entry.get("id"));
                            final JsonValue amount = changes.get(id);
                            if (amount == null) {
                                continue;
                            }
                            if (amount.getValueType() != ValueType.NUMBER) {
                                request.getSession().setAttribute("error",
                                        Variables.translate("invalidPartialPayoutUpdateRequest"));
                                return () -> false;
                            }
                            final long longAmount = changes.getJsonNumber(id).longValue();
                            if (longAmount == 0) {
                                continue;
                            } else if (longAmount == (long) entry.get("amount")) {
                                try (PreparedStatement st = c.prepareStatement(
                                        "UPDATE payout SET userInteraction=?,success=?,adminInteractionCompleted=TRUE,requiresAdminInteraction=FALSE,unknown=FALSE,message=? WHERE id=?")) {
                                    switch (newState) {
                                    case SUCCESS:
                                        st.setBoolean(1, false);
                                        st.setBoolean(2, true);
                                        break;
                                    case FAILURE:
                                        st.setBoolean(1, true);
                                        st.setBoolean(2, false);
                                        break;
                                    }
                                    st.setString(3, finalMessage);
                                    st.setString(4, id);
                                    st.executeUpdate();
                                    payoutids.add(id);
                                }
                            } else if (longAmount > (long) entry.get("amount") || longAmount < 0) {
                                request.getSession().setAttribute("error",
                                        Variables.translate("invalidPartialPayoutUpdateRequest"));
                                return () -> false;
                            } else {
                                try (PreparedStatement split = c.prepareStatement(
                                        "UPDATE payout SET userInteraction=FALSE,success=TRUE,adminInteractionCompleted=TRUE,requiresAdminInteraction=FALSE,unknown=TRUE,message=? WHERE id=?");
                                        PreparedStatement closed = c.prepareStatement(
                                                "INSERT INTO payout(id,uid,type,matid,amount,userInteraction,success,adminInteractionCompleted,requiresAdminInteraction,unknown,payoutType,date,message,agent) (SELECT ?,uid,type,matid,?,?,?,TRUE,FALSE,FALSE,payoutType,date,?,agent FROM payout WHERE id=?)");
                                        PreparedStatement open = c.prepareStatement(
                                                "INSERT INTO payout(id,uid,type,matid,amount,userInteraction,success,adminInteractionCompleted,requiresAdminInteraction,unknown,payoutType,date,message,agent) (SELECT ?,uid,type,matid,amount-?,userInteraction,success,adminInteractionCompleted,requiresAdminInteraction,unknown,payoutType,date,?,agent FROM payout WHERE id=?)")) {
                                    final long id1 =
                                            ThreadLocalRandom.current().nextLong(4294967295L);
                                    final long id2 =
                                            ThreadLocalRandom.current().nextLong(4294967295L);
                                    split.setString(1, Variables.translate("splitPayout", id1, id2,
                                            finalMessage));
                                    split.setString(2, id);

                                    closed.setLong(1, id1);
                                    closed.setLong(2, longAmount);
                                    switch (newState) {
                                    case SUCCESS:
                                        closed.setBoolean(3, false);
                                        closed.setBoolean(4, true);
                                        break;
                                    case FAILURE:
                                        closed.setBoolean(3, true);
                                        closed.setBoolean(4, false);
                                        break;
                                    }
                                    closed.setString(5, finalMessage);
                                    closed.setString(6, id);

                                    open.setLong(1, id2);
                                    open.setLong(2, longAmount);
                                    open.setString(3, finalMessage);
                                    open.setString(4, id);

                                    closed.executeUpdate();
                                    open.executeUpdate();
                                    split.executeUpdate();
                                    payoutids.add(String.valueOf(id));
                                    payoutids.add(String.valueOf(id1));
                                    payoutids.add(String.valueOf(id2));
                                }
                            }
                        }
                        request.getSession().setAttribute("info",
                                Variables.translate("payoutUpdateSuccess"));
                        return () -> true;
                    });
            if (executeAtomic.wasSuccessful()) {
                PayoutRestAPI.invalidate(payoutOwner, false, () -> {
                    final List<Payout> payouts = new ArrayList<>(payoutids.size());
                    try (Connection c = Main.instance().getConnector().getConnection();
                            PreparedStatement ps = c.prepareStatement(
                                    "SELECT * FROM payout WHERE id IN(?,?,?,?,?)")) {
                        for (int i = 0; i <= payoutids.size(); i += 5) {
                            for (int j = i; j < i + 5; j++) {
                                if (j < payoutids.size()) {
                                    ps.setString(j - i + 1, payoutids.get(j));
                                } else {
                                    ps.setLong(j - i + 1, -1L);
                                }
                            }
                            try (ResultSet rs = ps.executeQuery()) {
                                payouts.addAll(PayoutBuilder.createPayoutListFromResultSet(rs));
                            }
                        }
                    } catch (final SQLException e) {
                        e.printStackTrace();
                        return new Payout[0];
                    }
                    return payouts.toArray(new Payout[payouts.size()]);
                });
            }

        }
    }

    private void handleRequest(final HttpServletRequest request,
            final HttpServletResponse response) {
        final long adminUid = (long) request.getSession().getAttribute("id");
        synchronized (PayoutAdminServlet.lock) {
            final long[] payoutOwner = new long[1];
            final Successindicator executeAtomic =
                    Main.instance().getConnector().executeAtomic(c -> {
                        try (final PreparedStatement ps = c.prepareStatement(
                                "SELECT * FROM payout WHERE adminInteractionCompleted=FALSE AND requiresAdminInteraction=FALSE and agent=? LIMIT 1")) {
                            ps.setLong(1, adminUid);
                            try (final ResultSet rs = ps.executeQuery()) {
                                if (rs.next()) {
                                    request.getSession().setAttribute("error",
                                            Variables.translate("payoutsAlreadyAssigned"));
                                    return () -> false;
                                }
                            }
                        }
                        try (PreparedStatement get = c.prepareStatement(
                                "SELECT * FROM payout WHERE requiresAdminInteraction=TRUE ORDER BY FIELD(payoutType, \"lieferung\",\"bs1\",\"bs2\",\"bs3\",\"kadcon\") DESC, date ASC LIMIT 1");
                                ResultSet rs = get.executeQuery();
                                PreparedStatement set = c.prepareStatement(
                                        "UPDATE payout SET requiresAdminInteraction=FALSE, userInteraction=FALSE, agent=? WHERE requiresAdminInteraction=TRUE AND uid=? AND payoutType=?")) {
                            if (!rs.next()) {
                                request.getSession().setAttribute("error",
                                        Variables.translate("noOpenPayouts"));
                                return () -> false;
                            }
                            payoutOwner[0] = rs.getLong("uid");
                            final String payoutType = rs.getString("payoutType");
                            set.setLong(1, adminUid);
                            set.setLong(2, payoutOwner[0]);
                            set.setString(3, payoutType);
                            final int k = set.executeUpdate();
                            if (k <= 0) {
                                request.getSession().setAttribute("error",
                                        Variables.translate("payoutRequestSuccessButFailed"));
                                return () -> false;
                            }
                            Log.ADMIN.executeLog(c, Level.INFO,
                                    adminUid + " requested new payouts and got " + payoutOwner[0]
                                            + "'s payouts with type " + payoutType + " after "
                                            + rs.getDate("date"),
                                    adminUid, payoutOwner[0]);
                        }
                        request.getSession().setAttribute("info",
                                Variables.translate("payoutRequestSuccess"));
                        return () -> true;
                    });
            if (executeAtomic.wasSuccessful()) {
                PayoutRestAPI.invalidate(payoutOwner[0], false, () -> {
                    try (Connection c = Main.instance().getConnector().getConnection();
                            PreparedStatement ps = c.prepareStatement(
                                    "SELECT * FROM payout WHERE adminInteractionCompleted=FALSE AND requiresAdminInteraction=FALSE AND agent=?")) {
                        ps.setLong(1, adminUid);
                        try (ResultSet rs = ps.executeQuery()) {
                            final List<Payout> createPayoutListFromResultSet =
                                    PayoutBuilder.createPayoutListFromResultSet(rs);
                            return createPayoutListFromResultSet
                                    .toArray(new Payout[createPayoutListFromResultSet.size()]);
                        }
                    } catch (final SQLException e) {
                        return new Payout[0];
                    }
                });
            }
        }
    }
}
