package de._125m125.ktv2.servlets.admin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipOutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.util.ThrowingFunction;

@WebServlet(urlPatterns = { "/admin/mysqlexporter" })
public class TSVFileGenerator extends HttpServlet {

    private static final Map<String, MysqlQuery> queries;

    static {
        queries = new HashMap<>();
        TSVFileGenerator.queries.put("admin",
                new MysqlQuery("SELECT id FROM admin WHERE {1}", "id"));
        TSVFileGenerator.queries.put("banned",
                new MysqlQuery("SELECT * FROM banned WHERE {1}", "id"));
        TSVFileGenerator.queries.put("certificates",
                new MysqlQuery("SELECT {0}serialNr,type FROM certificates WHERE {1}", "user"));
        TSVFileGenerator.queries.put("errors",
                new MysqlQuery(
                        "SELECT id,{0}message,source,lineno,colno,error FROM errors WHERE {1}",
                        "reporter"));
        TSVFileGenerator.queries.put("gdpr",
                new MysqlQuery("SELECT {0}type FROM gdpr WHERE {1}", "user"));
        TSVFileGenerator.queries.put("giftcodes",
                new MysqlQuery(
                        "SELECT {0}precode,code,amount,material FROM giftcodes WHERE {1} AND {2}",
                        "redeemer", "material"));
        TSVFileGenerator.queries.put("history", new MysqlQuery(
                "SELECT time,type,material,value,amount,total FROM history WHERE {2} AND {3}", null,
                "material", "time"));
        TSVFileGenerator.queries.put("logging",
                new MysqlQuery(
                        "SELECT id,time,type,level,{0}message FROM logging WHERE {1} AND {3}",
                        "uid", null, "time"));
        TSVFileGenerator.queries.put("login",
                new MysqlQuery("SELECT {0}Name,Password,IP FROM login WHERE {1}", "id"));
        TSVFileGenerator.queries.put("mailcounter",
                new MysqlQuery("SELECT mail,active,count,lastSent FROM mailcounter", null));
        TSVFileGenerator.queries.put("materialMovements", new MysqlQuery(
                "SELECT id,time,{0}type,matid,amount,reason FROM materialMovements WHERE {1} AND {2} AND {3}",
                "uid", "matid", "time"));
        TSVFileGenerator.queries.put("materials", new MysqlQuery(
                "SELECT {0}type,matid,amount FROM materials WHERE {1} AND {2}", "id", "matid"));
        TSVFileGenerator.queries.put("messages", new MysqlQuery(
                "SELECT id,{0}message,time FROM messages WHERE {1} AND {3}", "uid", null, "time"));
        TSVFileGenerator.queries.put("minmax",
                new MysqlQuery("SELECT time,type,material,min,max FROM minmax WHERE {2} AND {3}",
                        null, "material", "time"));
        TSVFileGenerator.queries.put("payout", new MysqlQuery(
                "SELECT id,{0}type,matid,amount,userInteraction,success,adminInteractionCompleted,requiresAdminInteraction,unknown,payouttype,date,message,agent FROM payout WHERE {1} AND {2} AND {3}",
                "uid", "matid", "date"));
        TSVFileGenerator.queries.put("payoutboxes", new MysqlQuery(
                "SELECT cid,server,x,y,z,{0}verified FROM payoutboxes WHERE {1}", "owner"));
        TSVFileGenerator.queries.put("payoutsettings",
                new MysqlQuery("SELECT {0}server,x,y,z,warp FROM payoutsettings WHERE {1}", "uid"));
        TSVFileGenerator.queries.put("possibleRegister", new MysqlQuery(
                "SELECT {0}Password,IP,itemcomment,referrer,id FROM possibleRegister WHERE {1}",
                "name"));
        TSVFileGenerator.queries.put("possiblepwchange",
                new MysqlQuery("SELECT {0}Password,id FROM possiblepwchange WHERE {1}", "name"));
        TSVFileGenerator.queries.put("referrer",
                new MysqlQuery("SELECT id,ref FROM referrer WHERE {1}", "id"));
        TSVFileGenerator.queries.put("referrer2",
                new MysqlQuery(2, "SELECT id,ref FROM referrer WHERE {1}", "ref"));
        TSVFileGenerator.queries.put("reftaxes",
                new MysqlQuery("SELECT {0}amount FROM reftaxes WHERE {1}", "id"));
        TSVFileGenerator.queries.put("system", new MysqlQuery("SELECT k,v FROM system", null));
        TSVFileGenerator.queries.put("token",
                new MysqlQuery("SELECT {0}tid,tkn,permissions FROM token WHERE {1}", "uid"));
        TSVFileGenerator.queries.put("trade", new MysqlQuery(
                "SELECT id,time,{0}type,buysell,material,amount,price,sold FROM trade WHERE {1} AND {2} AND {3}",
                "traderid", "material", "time"));
        TSVFileGenerator.queries.put("tradeHistory", new MysqlQuery(
                "SELECT time,id,{0}type,buysell,material,amount,price FROM tradeHistory WHERE {1} AND {2} AND {3}",
                "traderid", "material", "time"));
        TSVFileGenerator.queries.put("tradesDoneHistory", new MysqlQuery(
                "SELECT tradesDoneHistory.time,tradesDoneHistory.id1,tradesDoneHistory.id2,tradesDoneHistory.amount,(tradeHistory1.price+tradeHistory2.price)/2 as price FROM tradesDoneHistory LEFT JOIN tradeHistory as tradeHistory1 ON (tradesDoneHistory.id1 = tradeHistory1.id) LEFT JOIN tradeHistory as tradeHistory2 ON (tradesDoneHistory.id2 = tradeHistory2.id) WHERE {1} AND {2} AND {3}",
                "tradeHistory1.traderid", "tradeHistory1.material", "tradesDoneHistory.time"));
        TSVFileGenerator.queries.put("tradesDoneHistory2", new MysqlQuery(2,
                "SELECT tradesDoneHistory.time,tradesDoneHistory.id1,tradesDoneHistory.id2,tradesDoneHistory.amount,(tradeHistory1.price+tradeHistory2.price)/2 as price FROM tradesDoneHistory LEFT JOIN tradeHistory as tradeHistory1 ON (tradesDoneHistory.id1 = tradeHistory1.id) LEFT JOIN tradeHistory as tradeHistory2 ON (tradesDoneHistory.id2 = tradeHistory2.id) WHERE {1} AND {2} AND {3}",
                "tradeHistory2.traderid", "tradeHistory1.material", "tradesDoneHistory.time"));
        TSVFileGenerator.queries.put("twoFactorAuth",
                new MysqlQuery("SELECT id,{0}type,data,iv FROM twoFactorAuth WHERE {1}", "uid"));
        TSVFileGenerator.queries.put("twoFactorCookie",
                new MysqlQuery("SELECT {0}cookie FROM twoFactorCookie WHERE {1}", "id"));
        TSVFileGenerator.queries.put("twoFactorIP",
                new MysqlQuery("SELECT {0}ip FROM twoFactorIP WHERE {1}", "id"));
        TSVFileGenerator.queries.put("unknownPayins",
                new MysqlQuery("SELECT {0}type,item,amount FROM unknownPayins WHERE {1} AND {2}",
                        "username", "item"));
        TSVFileGenerator.queries.put("webPushSubscriptions", new MysqlQuery(
                "SELECT {0}token,itemlist,messages,orders,payouts FROM webPushSubscriptions WHERE {1}",
                "user"));
        TSVFileGenerator.queries.put("lotteryOrderCreation", new MysqlQuery(
                "SELECT time, COALESCE(Name,\"anonym\") as name, buysell, material, amount, price FROM tradeHistory hist LEFT JOIN (SELECT id,Name FROM login WHERE id IN (SELECT user FROM gdpr WHERE type=\"PL\")) l ON hist.traderid = l.id WHERE {2} ORDER BY time ASC",
                null, "material"));
        TSVFileGenerator.queries.put("lotteryInventory", new MysqlQuery(
                "SELECT COALESCE(Name,\"anonym\") as name, matid, amount FROM materials mats LEFT JOIN (SELECT id,Name FROM login WHERE id IN (SELECT user FROM gdpr WHERE type=\"PL\")) l ON mats.id = l.id WHERE {2} ORDER BY amount DESC",
                null, "matid"));
        TSVFileGenerator.queries.put("lotteryInventory", new MysqlQuery(
                "SELECT COALESCE(Name,\"anonym\") as name, matid, amount FROM materials mats LEFT JOIN (SELECT id,Name FROM login WHERE id IN (SELECT user FROM gdpr WHERE type=\"PL\")) l ON mats.id = l.id WHERE {2} ORDER BY amount DESC",
                null, "matid"));
        TSVFileGenerator.queries.put("lotteryTrades", new MysqlQuery(
                "SELECT time, COALESCE(buy.Name,\"anonym\") as buyer,COALESCE(sell.Name,\"anonym\") as seller, hist.amount, (buy.price+sell.price)/2 as price FROM  tradesDoneHistory hist LEFT JOIN (SELECT buy.id,buy.buysell,buy.material,buyer.Name,buy.price FROM tradeHistory buy LEFT JOIN (SELECT id,Name FROM login WHERE id IN (SELECT user FROM gdpr WHERE type=\"PL\")) buyer ON buy.traderid = buyer.id) buy ON hist.id1=buy.id LEFT JOIN (SELECT sell.id,seller.Name,sell.price FROM tradeHistory sell LEFT JOIN (SELECT id,Name FROM login WHERE id IN (SELECT user FROM gdpr WHERE type=\"PL\")) seller ON sell.traderid = seller.id) sell ON hist.id2=sell.id WHERE buy.buysell=true AND {2} ORDER BY time ASC;",
                null, "buy.material"));
    }

    private final MySQLHelper helper;

    public TSVFileGenerator() {
        this.helper = MySQLHelper.INSTANCE;
    }

    public TSVFileGenerator(final MySQLHelper helper) {
        this.helper = helper;
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("tables", TSVFileGenerator.queries.keySet());
        request.setAttribute("filenames",
                request.getSession().getAttribute("adminFilenames") == null ? new HashMap<>()
                        : request.getSession().getAttribute("adminFilenames"));
        final RequestDispatcher rd = request.getRequestDispatcher("/admin/mysqlexporter.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("generate") != null) {
            if (request.getParameter("zip") == null) {
                generate(request, null);
            } else {
                try {
                    final String zip = generateZipped(request);
                    @SuppressWarnings("unchecked")
                    final Map<String, String> fileNames = (Map<String, String>) request.getSession()
                            .getAttribute("adminFilenames");
                    fileNames.put("zip", zip);
                    if (request.getParameter("checksums") != null) {
                        fileNames.put("checksums", generateChecksumFile(Arrays.asList(zip)));
                    }
                } catch (final IOException e) {
                    e.printStackTrace();
                    request.setAttribute("error", Variables.translate("unknownError"));
                }
            }
        }
        PostRedirectGet.execute(request, response, new HashMap<>());
    }

    private String generateZipped(final HttpServletRequest request) throws IOException {
        final File directory = new File("/home/kadcontrade/generated/tsv/");
        final File zip = File.createTempFile("archive", ".zip", directory);
        try (FileOutputStream fos = new FileOutputStream(zip);
                ZipOutputStream zos = new ZipOutputStream(fos)) {
            generate(request, zos);
        }
        return zip.getName();
    }

    private void generate(final HttpServletRequest request, final ZipOutputStream zos)
            throws IOException {
        final QuerySettings qs;
        try {
            qs = new QuerySettings(request.getParameter("user"), request.getParameter("material"),
                    request.getParameter("anonymous"), request.getParameter("after"),
                    request.getParameter("before"));
        } catch (final IllegalArgumentException e) {
            request.setAttribute("error", "invalid setting: " + e.getMessage());
            return;
        }
        final String[] tables;
        if (request.getParameter("everything") != null) {
            tables = TSVFileGenerator.queries.keySet()
                    .toArray(new String[TSVFileGenerator.queries.size()]);
        } else if (request.getParameter("usertables") != null) {
            tables = TSVFileGenerator.queries.entrySet().stream()
                    .filter(e -> e.getValue().uidWhere.isPresent()).map(Map.Entry::getKey)
                    .toArray(String[]::new);
        } else {
            tables = request.getParameterValues("tables");
        }
        if (tables == null) {
            request.setAttribute("error", Variables.translate("noTablesSelected"));
            return;
        }
        final Map<String, String> fileNames = new HashMap<>();
        for (final String table : tables) {
            final MysqlQuery mysqlQuery = TSVFileGenerator.queries.get(table);
            if (mysqlQuery == null) {
                fileNames.put(table, Variables.translate("unknownTable"));
            } else {
                try {
                    fileNames.put(table, mysqlQuery.generate(this.helper, qs, zos));
                } catch (final SQLException e) {
                    fileNames.put(table, Variables.translate("unknownError"));
                    e.printStackTrace();
                }
            }
        }
        if (request.getParameter("checksums") != null && zos == null) {
            fileNames.put("checksums", generateChecksumFile(fileNames.values()));
        }
        request.getSession().setAttribute("adminFilenames", fileNames);
        request.setAttribute("info", Variables.translate("generationSuccess"));
    }

    List<ThrowingFunction<InputStream, String>> algos = Arrays.asList(DigestUtils::sha512Hex,
            DigestUtils::sha256Hex, DigestUtils::sha1Hex, DigestUtils::md5Hex);

    private String generateChecksumFile(final Collection<String> values)
            throws FileNotFoundException, IOException {
        final File directory = new File("/home/kadcontrade/generated/tsv/");
        final StringBuilder sb = new StringBuilder();
        for (final String name : values) {
            for (final ThrowingFunction<InputStream, String> algo : this.algos) {
                try (InputStream is = new FileInputStream(new File(directory, name))) {
                    sb.append(algo.apply(is)).append("  ").append(name).append("\n");
                }
            }
        }

        final File out;
        if (values.size() == 1) {
            out = new File(directory, values.iterator().next() + ".checksums.txt");
        } else {
            out = File.createTempFile("checksums", ".txt", directory);
        }
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(out))) {
            bw.write(sb.toString());
        }
        return out.getName();
    }

    /* package */ static final class MysqlQuery {
        private static final Pattern namePattern = Pattern.compile("(?<=FROM )[a-zA-Z]+");

        private static String getName(final String query) {
            final Matcher matcher = MysqlQuery.namePattern.matcher(query);
            matcher.find();
            return matcher.group();
        }

        private final MessageFormat    base;
        private final Optional<String> uidName;
        private final Optional<String> uidWhere;
        private final Optional<String> materialWhere;
        private final String           name;
        private final Optional<String> timeWhere;

        public MysqlQuery(final String base, final String uidName) {
            this(0, base, uidName);
        }

        public MysqlQuery(final int i, final String base, final String uidName) {
            this(i, base, uidName, null);
        }

        public MysqlQuery(final String base, final String uidName, final String materialName) {
            this(0, base, uidName, materialName);
        }

        public MysqlQuery(final int i, final String base, final String uidName,
                final String materialName) {
            this(i, base, uidName, materialName, null);
        }

        public MysqlQuery(final String base, final String uidName, final String materialName,
                final String timeName) {
            this(0, base, uidName, materialName, timeName);
        }

        public MysqlQuery(final int i, final String base, final String uidName,
                final String materialName, final String timeName) {
            this(getName(base) + (i == 0 ? "" : i), base, Optional.ofNullable(uidName),
                    Optional.ofNullable(materialName), Optional.ofNullable(timeName));
        }

        public MysqlQuery(final String name, final String base, final Optional<String> uidName,
                final Optional<String> materialName, final Optional<String> timename) {
            super();
            this.name = name;
            this.base = new MessageFormat(base);
            this.uidName = uidName.map(s -> s + ",");
            this.uidWhere = uidName.map(s -> s + "=?");
            this.materialWhere = materialName.map(s -> s + "=?");
            this.timeWhere = timename.map(s -> s + ">? AND " + s + "<?");
        }

        public String generate(final MySQLHelper helper, final Long user, final String material,
                final boolean anonymous, final Timestamp after, final Timestamp before,
                final ZipOutputStream zos) throws SQLException, IOException {
            final String query = this.base.format(new Object[] {
                    anonymous ? "" : this.uidName.orElse(""),
                    user == null ? "TRUE" : this.uidWhere.orElse("TRUE"),
                    material == null ? "TRUE" : this.materialWhere.orElse("TRUE"),
                    after == null && before == null ? "TRUE" : this.timeWhere.orElse("TRUE") });
            try (final Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps = c.prepareStatement(query)) {
                int i = 1;
                if (user != null && this.uidWhere.isPresent()) {
                    ps.setLong(i, user);
                    i++;
                }
                if (material != null && this.materialWhere.isPresent()) {
                    ps.setString(i, material);
                    i++;
                }
                if ((after != null || before != null) && this.timeWhere.isPresent()) {
                    ps.setTimestamp(i, after == null ? new Timestamp(0) : after);
                    ps.setTimestamp(i + 1,
                            before == null ? new Timestamp(System.currentTimeMillis()) : before);
                    i += 2;
                }
                try (ResultSet rs = ps.executeQuery()) {
                    return helper.resultSetToTsv(this.name, rs, zos);
                }
            }
        }

        public String generate(final MySQLHelper helper, final QuerySettings qs,
                final ZipOutputStream zos) throws SQLException, IOException {
            return generate(helper, qs.user, qs.material, qs.anonymous, qs.after, qs.before, zos);
        }

        public String getRawQuery() {
            return this.base.toPattern();
        }
    }

    private static class QuerySettings {
        Long      user;
        String    material;
        boolean   anonymous;
        Timestamp after;
        Timestamp before;

        public QuerySettings(final String userString, final String materialString,
                final String anonymousString, final String afterString, final String beforeString) {
            if (userString != null && !userString.isEmpty()) {
                this.user = UserHelper.get(userString);
                if (this.user <= 0) {
                    try {
                        this.user = Long.parseLong(userString);
                    } catch (final NumberFormatException e) {
                        throw new IllegalArgumentException("user");
                    }
                }
            } else {
                this.user = null;
            }
            if (materialString != null && !materialString.isEmpty()) {
                if (!Variables.resourceExistsOrIsMoney(materialString)) {
                    throw new IllegalArgumentException("material");
                }
                this.material = materialString;
            } else {
                this.material = null;
            }
            if (anonymousString != null && !anonymousString.isEmpty()) {
                this.anonymous = !"false".equalsIgnoreCase(anonymousString);
            } else {
                this.anonymous = false;
            }
            if (afterString != null && !afterString.isEmpty()) {
                try {
                    this.after = new Timestamp(Long.parseLong(afterString));
                } catch (final NumberFormatException e) {
                    try {
                        if (afterString.contains(":")) {
                            this.after = Timestamp.valueOf(afterString);
                        } else {
                            this.after = new Timestamp(Date.valueOf(afterString).getTime());
                        }
                    } catch (final IllegalArgumentException e2) {
                        throw new IllegalArgumentException("after");
                    }
                }
            } else {
                this.after = null;
            }
            if (beforeString != null && !beforeString.isEmpty()) {
                try {
                    this.before = new Timestamp(Long.parseLong(beforeString));
                } catch (final NumberFormatException e) {
                    try {
                        if (beforeString.contains(":")) {
                            this.before = Timestamp.valueOf(beforeString);
                        } else {
                            this.before = new Timestamp(Date.valueOf(beforeString).getTime());
                        }
                    } catch (final IllegalArgumentException e2) {
                        throw new IllegalArgumentException("before");
                    }
                }
            } else {
                this.before = null;
            }
        }
    }
}
