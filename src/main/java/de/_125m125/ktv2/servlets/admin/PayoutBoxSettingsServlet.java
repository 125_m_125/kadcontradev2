package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.SettingsServlet;
import de._125m125.ktv2.servlets.apiv2.AdminPoboxRestAPI;
import de._125m125.ktv2.servlets.helper.ServletHelper;

@WebServlet(urlPatterns = { "/admin/pobox" })
public class PayoutBoxSettingsServlet extends HttpServlet {
    private final class POBoxDuplicationIndicator implements Successindicator {
        @Override
        public boolean wasSuccessful() {
            return false;
        }
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final RequestDispatcher rd = request.getRequestDispatcher("/admin/pobox.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("create") != null) {
            createPobox(request, response);
        } else if (request.getParameter("remove") != null) {
            deleteBox(request, response);
        } else if (request.getParameter("dispossess") != null) {
            dispossessBox(request, response);
        } else if (request.getParameter("verify") != null) {
            verifyBox(request, response);
        }
        PostRedirectGet.execute(request, response, "create");
    }

    private void verifyBox(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "boxid")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        Main.instance().getConnector().executeAtomic(c -> {
            final long owner;
            try (PreparedStatement ps = c
                    .prepareStatement("SELECT owner FROM payoutboxes WHERE cid=? AND owner IS NOT NULL")) {
                ps.setString(1, request.getParameter("boxid"));
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        owner = rs.getLong(1);
                    } else {
                        request.setAttribute("error", Variables.translate("poboxNotExistingOrInvalidState"));
                        Log.ADMIN.executeLog(c, Level.INFO,
                                request.getSession().getAttribute("id") + " failed to verify pobox "
                                        + request.getParameter("boxid"),
                                (long) request.getSession().getAttribute("id"));
                        return () -> true;
                    }
                }
            }
            try (PreparedStatement ps = c
                    .prepareStatement("UPDATE payoutboxes SET verified=TRUE WHERE cid=? AND owner IS NOT NULL")) {
                ps.setString(1, request.getParameter("boxid"));
                ps.executeUpdate();
                MySQLHelper.sendMessage(c, owner, "poboxVerified");
                Log.ADMIN.executeLog(c,
                        Level.INFO, request.getSession().getAttribute("id") + " verified pobox "
                                + request.getParameter("boxid") + "from " + owner,
                        (long) request.getSession().getAttribute("id"), owner);
                request.setAttribute("info", Variables.translate("poboxVerifySuccess"));
                AdminPoboxRestAPI.invalidate();
                SettingsServlet.getPayoutSettingsCache().invalidate(owner);
            }
            return () -> true;
        });
    }

    private void dispossessBox(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        if (!ServletHelper.checkParams(request, "boxid")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        Main.instance().getConnector().executeAtomic(c -> {
            final long owner;
            try (PreparedStatement ps = c
                    .prepareStatement("SELECT owner FROM payoutboxes WHERE cid=? AND owner IS NOT NULL")) {
                ps.setString(1, request.getParameter("boxid"));
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        owner = rs.getLong(1);
                    } else {
                        request.setAttribute("error", Variables.translate("poboxNotExistingOrInvalidState"));
                        Log.ADMIN.executeLog(c, Level.INFO,
                                request.getSession().getAttribute("id") + " failed to dispossess pobox "
                                        + request.getParameter("boxid"),
                                (long) request.getSession().getAttribute("id"));
                        return () -> true;
                    }
                }
            }
            try (PreparedStatement ps = c.prepareStatement(
                    "UPDATE payoutboxes SET owner=NULL, verified=FALSE WHERE cid=? AND owner IS NOT NULL")) {
                ps.setString(1, request.getParameter("boxid"));
                ps.executeUpdate();
                MySQLHelper.sendMessage(c, owner, "poboxDispossessed");
                Log.ADMIN.executeLog(c,
                        Level.INFO, request.getSession().getAttribute("id") + " dispossessed pobox "
                                + request.getParameter("boxid") + "from " + owner,
                        (long) request.getSession().getAttribute("id"), owner);
                request.setAttribute("info", Variables.translate("poboxDispossessSuccess"));
                AdminPoboxRestAPI.invalidate();
                SettingsServlet.getPayoutSettingsCache().invalidate(owner);
            }
            return () -> true;
        });
    }

    private void deleteBox(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "boxid")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        Main.instance().getConnector().executeAtomic(c -> {
            try (PreparedStatement ps = c.prepareStatement("DELETE FROM payoutboxes WHERE cid=? AND owner IS NULL")) {
                ps.setString(1, request.getParameter("boxid"));
                final int amount = ps.executeUpdate();
                if (amount == 1) {
                    Log.ADMIN.executeLog(c, Level.INFO,
                            request.getSession().getAttribute("id") + " deleted pobox " + request.getParameter("boxid"),
                            (long) request.getSession().getAttribute("id"));
                    request.setAttribute("info", Variables.translate("poboxDeleteSuccess"));
                    AdminPoboxRestAPI.invalidate();
                } else {
                    Log.ADMIN
                            .executeLog(c, Level.INFO,
                                    request.getSession().getAttribute("id") + " failed to delet pobox "
                                            + request.getParameter("boxid"),
                                    (long) request.getSession().getAttribute("id"));
                    request.setAttribute("error", Variables.translate("poboxNotExistingOrInvalidState"));
                }
            }
            return () -> true;
        });
    }

    private void createPobox(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "server", "x", "y", "z")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        final int server = Integer.parseInt(request.getParameter("server"));
        final int x = Integer.parseInt(request.getParameter("x"));
        final int y = Integer.parseInt(request.getParameter("y"));
        final int z = Integer.parseInt(request.getParameter("z"));
        if (server < 1 || server > 3) {
            request.setAttribute("error", Variables.translate("invalidServer"));
            return;
        }
        final Successindicator executeAtomic = Main.instance().getConnector().executeAtomic(c -> {
            try (PreparedStatement ps = c.prepareStatement("INSERT INTO payoutboxes(server,x,y,z) VALUES(?,?,?,?)")) {
                ps.setInt(1, server);
                ps.setInt(2, x);
                ps.setInt(3, y);
                ps.setInt(4, z);
                ps.executeUpdate();
                Log.ADMIN.executeLog(c, Level.INFO, request.getSession().getAttribute("id") + " added a new payoutbox: "
                        + server + ":" + x + "|" + y + "|" + z, (long) request.getSession().getAttribute("id"));
            } catch (final SQLException e) {
                if (e.getErrorCode() == 1062) {// duplicate key
                    return new POBoxDuplicationIndicator();
                }
                throw e;
            }
            return () -> true;
        });
        if (executeAtomic.wasSuccessful()) {
            request.setAttribute("info", Variables.translate("boxCreated"));
            AdminPoboxRestAPI.invalidate();
            return;
        } else {
            if (executeAtomic instanceof POBoxDuplicationIndicator) {
                request.setAttribute("error", Variables.translate("boxAlreadyExisting"));
                return;
            }
            request.setAttribute("error", Variables.translate("unknownError"));
            return;
        }
    }
}
