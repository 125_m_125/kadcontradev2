package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.sql.Timestamp;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.beans.HistoricTrade;
import de._125m125.ktv2.beans.HistoricTradeExecution;

@WebServlet(urlPatterns = { "/admin/trade/ajax" })
public class TradeViewerAjaxServlet extends HttpServlet {

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        final long owner;
        final Timestamp t;
        final String material;
        try {
            owner = Long.parseLong(request.getParameter("owner"));
            t = Timestamp.valueOf(request.getParameter("time"));
            material = request.getParameter("material");
            if (material == null) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        } catch (final IllegalArgumentException e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        final JsonObjectBuilder result = Json.createObjectBuilder();
        final JsonArrayBuilder nodes = Json.createArrayBuilder();
        final JsonArrayBuilder edges = Json.createArrayBuilder();

        final HistoricTrade[] historicTrades = HistoricTrade.get(owner, t, material);
        for (final HistoricTrade historicTrade : historicTrades) {
            nodes.add(historicTrade.toJson());
            for (final HistoricTradeExecution historicTradeExecution : historicTrade.getExecutions()) {
                edges.add(historicTradeExecution.toJson());
            }
        }
        result.add("nodes", nodes);
        result.add("edges", edges);
        final String resultString = result.build().toString();
        response.getWriter().append(resultString);
    }
}
