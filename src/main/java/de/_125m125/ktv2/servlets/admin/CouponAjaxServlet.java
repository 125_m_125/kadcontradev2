package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.beans.Coupon;
import de._125m125.ktv2.servlets.admin.helper.CouponQueryBuilder;

@WebServlet(urlPatterns = { "/admin/coupon/ajax" })
public class CouponAjaxServlet extends HttpServlet {

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final JsonObjectBuilder result = Json.createObjectBuilder();

        final String draw = request.getParameter("draw");
        if (draw != null) {
            result.add("draw", draw);
        }
        final CouponQueryBuilder cqb = new CouponQueryBuilder();
        try {
            final int limit = Integer.parseInt(request.getParameter("length"));
            cqb.setLimit(Math.min(100, Math.max(1, limit)));
        } catch (final NumberFormatException e1) {
        }
        try {
            final int offset = Integer.parseInt(request.getParameter("start"));
            cqb.setOffset(Math.max(0, offset));
        } catch (final NumberFormatException e1) {
        }
        final String search = request.getParameter("search[value]");
        if (search != null) {
            cqb.setSearch(search);
        }

        final ArrayList<HashMap<String, Object>> entries = cqb.execute();
        final JsonArrayBuilder queryResult = Json.createArrayBuilder();
        if (entries != null) {
            for (final HashMap<String, Object> entry : entries) {
                final JsonObjectBuilder entryJson = Json.createObjectBuilder();
                entryJson.add("code",
                        Coupon.codeToString((String) entry.get("preCode"), (BigInteger) entry.get("code")));
                entryJson.add("material", (String) entry.get("material"));
                entryJson.add("amount", String.valueOf(entry.get("amount")));
                entryJson.add("redeemer", String.valueOf(entry.get("redeemer")));
                queryResult.add(entryJson);
            }
        }
        result.add("data", queryResult);
        result.add("recordsFiltered", cqb.getRowCount());
        final String resultString = result.build().toString();
        response.getWriter().append(resultString);
    }
}
