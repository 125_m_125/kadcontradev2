package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Coupon;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.servlets.helper.ServletHelper;

/**
 * Servlet implementation class DataServlet
 */
@WebServlet(urlPatterns = { "/admin/coupon" })
public class CouponServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CouponServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final RequestDispatcher rd = request.getRequestDispatcher("/admin/coupon.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("create") != null) {
            createCoupons(request, response);
        }
        PostRedirectGet.execute(request, response, "create");
    }

    private void createCoupons(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        if (!ServletHelper.checkParams(request, "preCode", "material", "materialAmount", "couponCount")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        final String preCode = request.getParameter("preCode");
        if (preCode.length() != 2 || preCode.contains("-")) {
            request.setAttribute("error", Variables.translate("invalidPreCode"));
            return;
        }
        final String material = request.getParameter("material");
        if (!Variables.resourceExists(material)) {
            request.setAttribute("error", Variables.translate("unknownMaterial"));
            return;
        }
        final String materialAmountString = request.getParameter("materialAmount");
        long materialAmount;
        try {
            materialAmount = Long.parseLong(materialAmountString);
        } catch (final NumberFormatException e) {
            request.setAttribute("error", Variables.translate("invalidAmount"));
            return;
        }
        if (materialAmount < 1) {
            request.setAttribute("error", Variables.translate("invalidAmount"));
            return;
        }
        if ("-1".equals(material)) {
            materialAmount *= 1000000;
        }

        final String couponCountString = request.getParameter("couponCount");
        int couponCount;
        try {
            couponCount = Integer.parseInt(couponCountString);
        } catch (final NumberFormatException e) {
            request.setAttribute("error", Variables.translate("invalidAmount"));
            return;
        }
        if (couponCount < 1) {
            request.setAttribute("error", Variables.translate("invalidAmount"));
            return;
        }
        final List<BigInteger> createdCoupons = Coupon.create(preCode, material, materialAmount, couponCount);
        if (createdCoupons.size() == couponCount) {
            request.setAttribute("info", Variables.translate("couponCreationSuccess"));
        } else if (createdCoupons.size() > 0) {
            request.setAttribute("error", Variables.translate("couponCreationPartialSuccess"));
        } else {
            request.setAttribute("error", Variables.translate("unknownError"));
        }
    }
}