package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.util.logging.Level;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.filter.MaintainanceFilter;
import de._125m125.ktv2.filter.MaintainanceFilter.Settings;
import de._125m125.ktv2.logging.Log;

/**
 * Servlet implementation class DataServlet
 */
@WebServlet(urlPatterns = { "/admin/maintainance" })
public class MaintainanceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MaintainanceServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final Settings localSettings = MaintainanceFilter.getSettings();
        if (localSettings != null) {
            request.setAttribute("lockAdmins", localSettings.shouldBlockAdmins());
            request.setAttribute("regex", localSettings.getPatternString());
        }
        final RequestDispatcher rd = request.getRequestDispatcher("/admin/maintainance.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("active") == null) {
            Log.ADMIN.log(Level.INFO, "MaintainanceMode terminated", (long) request.getSession().getAttribute("id"));
            MaintainanceFilter.configure(false, null);
            request.setAttribute("info", Variables.translate("saveSuccess"));
        } else {
            final String regex = request.getParameter("regex");
            if (request.getParameter("all") != null) {
                if (regex != null && !regex.equals("")) {
                    request.setAttribute("error", Variables.translate("nonEmptyRegex"));
                } else {
                    final boolean admins = request.getParameter("lockAdmins") != null;
                    Log.ADMIN.log(Level.INFO, "MaintainanceMode activated: '.', " + admins,
                            (long) request.getSession().getAttribute("id"));
                    MaintainanceFilter.configure(true, new Settings(admins, "."));
                    request.setAttribute("info", Variables.translate("saveSuccess"));
                }
            } else if (regex == null || regex.equals("")) {
                request.setAttribute("error", Variables.translate("invalidRegex"));
            } else {
                try {
                    final Pattern p = Pattern.compile(regex);
                    final boolean admins = request.getParameter("lockAdmins") != null;

                    if (admins && p.matcher("/admin/maintainance")
                            .find()) { throw new RuntimeException(Variables.translate("blockingMaintainance")); }

                    final boolean blockLogin = request.getParameter("login") != null;
                    if (p.matcher("/login").find()
                            && !blockLogin) { throw new RuntimeException(Variables.translate("blockingLogin")); }

                    final boolean resources = request.getParameter("resources") != null;
                    if (p.matcher("/static/css/main.css").find()
                            && !resources) { throw new RuntimeException(Variables.translate("blockingResources")); }

                    MaintainanceFilter.configure(true, new Settings(admins, regex));

                    Log.ADMIN.log(Level.INFO, (long) request.getSession().getAttribute("id"),
                            "MaintainanceMode activated: ?, " + admins, regex);
                    request.setAttribute("info", Variables.translate("saveSuccess"));
                } catch (final RuntimeException ex) {
                    request.setAttribute("error",
                            Variables.translate("invalidRegex").concat(":").concat(ex.getMessage()));
                    Log.ADMIN.log(Level.INFO, (long) request.getSession().getAttribute("id"),
                            "MaintainanceMode failed for ?: ?", regex, ex.getMessage());
                }
            }
        }
        PostRedirectGet.execute(request, response, "active", "all", "regex", "login", "resources", "lockAdmins",
                "maintSubmit");
    }
}