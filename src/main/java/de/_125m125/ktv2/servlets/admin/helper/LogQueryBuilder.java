package de._125m125.ktv2.servlets.admin.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de._125m125.ktv2.servlets.helper.AjaxQueryBuilder;

public class LogQueryBuilder extends AjaxQueryBuilder {
    public LogQueryBuilder() {
        super("logging", "id DESC");
    }

    private Long         uid;
    private Integer      level;
    private Integer      minlevel;
    private List<String> types = new ArrayList<>();

    public LogQueryBuilder setUid(final long uid) {
        this.uid = uid;
        return this;
    }

    public LogQueryBuilder setLevel(final int level) {
        this.level = level;
        return this;
    }

    public LogQueryBuilder setMinLevel(final int minlevel) {
        this.minlevel = minlevel;
        return this;
    }

    public LogQueryBuilder addType(final String type) {
        this.types.add(type);
        return this;
    }

    public LogQueryBuilder setTypes(final String[] newTypes) {
        this.types = Arrays.asList(newTypes);
        return this;
    }

    @Override
    protected ArrayList<String> createWhereContent() {
        final StringBuilder query = new StringBuilder("WHERE ");
        final ArrayList<String> params = new ArrayList<>();
        if (this.uid != null) {
            query.append("uid=").append(this.uid).append(" AND ");
        }
        if (this.level != null) {
            query.append("level=").append(this.level).append(" AND ");
        } else if (this.minlevel != null) {
            query.append("level>").append(this.minlevel).append(" AND ");
        }
        if (this.types.size() > 0) {
            query.append("type IN (");
            this.types.forEach((e) -> query.append("?,"));
            query.deleteCharAt(query.length() - 1);
            query.append(") AND ");
            params.addAll(this.types);
        }

        if (query.substring(query.length() - 4).equals("AND ")) {
            query.delete(query.length() - 4, query.length());
        } else {
            query.delete(query.length() - 6, query.length());
        }
        params.add(0, query.toString());
        return params;
    }
}
