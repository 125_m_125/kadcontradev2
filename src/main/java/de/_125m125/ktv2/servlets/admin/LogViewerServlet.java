package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.servlets.admin.helper.LogQueryBuilder;

@WebServlet(urlPatterns = { "/admin/log" })
public class LogViewerServlet extends HttpServlet {

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/admin/log.jsp").forward(request, response);
    }

    public static ArrayList<HashMap<String, Object>> getLogs(final long uid) {
        return new LogQueryBuilder().setUid(uid).execute();
    }
}
