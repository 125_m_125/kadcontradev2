package de._125m125.ktv2.servlets.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.beans.Payout;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.market.TradeResult;
import de._125m125.ktv2.servlets.SettingsServlet;
import de._125m125.ktv2.servlets.api.helper.HelperResult;
import de._125m125.ktv2.servlets.api.helper.PayoutHelper;
import de._125m125.ktv2.servlets.apiv2.ItemRestAPI;
import de._125m125.ktv2.servlets.apiv2.MessageRestAPI;
import de._125m125.ktv2.servlets.helper.ServletHelper;

@WebServlet(urlPatterns = { "/admin/users" })
public class UserAdminServlet extends HttpServlet {

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (setAttributes(request)) {
            final Long uid = (Long) request.getAttribute("id");
            request.setAttribute("payoutSettings",
                    SettingsServlet.getPayoutSettingsCache().get(uid));
            request.setAttribute("logs", LogViewerServlet.getLogs(uid));
        }

        final RequestDispatcher rd = request.getRequestDispatcher("/admin/playerinspector.jsp");
        rd.forward(request, response);
    }

    private boolean setAttributes(final HttpServletRequest request) {
        final String uidParam = request.getParameter("uid");
        if (uidParam != null) {
            try {
                final String name = UserHelper.get(Long.valueOf(uidParam));
                if (name != null) {
                    request.setAttribute("id", Long.parseLong(uidParam));
                    request.setAttribute("name", name);
                    return true;
                }
            } catch (final NumberFormatException nfe) {
            }
        }
        final String nameParam = request.getParameter("uname");
        if (nameParam != null) {
            final long uid = UserHelper.get(nameParam);
            if (uid > 0) {
                request.setAttribute("id", uid);
                request.setAttribute("name", nameParam);
                return true;
            }
        }
        request.setAttribute("error", Variables.translate("unknownUser"));
        return false;
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String uidParam = request.getParameter("uid");
        if (uidParam == null || UserHelper.get(Long.parseLong(uidParam)) == null) {
            request.setAttribute("error", Variables.translate("missingParameters"));
        }
        // TODO
        if (request.getParameter("cancelOrder") != null) {
            cancelOrder(request);
        } else if (request.getParameter("payout") != null) {
            forcePayout(request);
        } else if (request.getParameter("addItems") != null) {
            addItems(request);
        } else if (request.getParameter("sendMessage") != null) {
            sendMessage(request);
        } else {
            request.setAttribute("error", Variables.translate("unknownAction"));
        }

        final Map<String, String[]> params = new HashMap<>();
        params.put("uid", request.getParameterValues("uid"));
        PostRedirectGet.execute(request, response, params);
    }

    private void sendMessage(final HttpServletRequest request) throws IOException {
        if (!ServletHelper.checkParams(request, "message")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        final String uid = request.getParameter("uid");
        final long userId = Long.parseLong(uid);
        final Successindicator executeAtomic = Main.instance().getConnector().executeAtomic(c -> {
            final Message addResource =
                    MySQLHelper.sendMessage(c, userId, request.getParameter("message"));
            Log.ADMIN.executeLog(c, Level.INFO,
                    "Admin " + request.getSession().getAttribute("id") + " sent message to " + uid,
                    (long) request.getSession().getAttribute("id"), userId);
            MessageRestAPI.invalidate(uid, new Message[] { addResource }, false);
            return () -> true;
        });
        if (executeAtomic.wasSuccessful()) {
            request.getSession().setAttribute("info", "sendMessageSuccess");
        } else {
            request.getSession().setAttribute("error", "unknownError");
        }
    }

    private void addItems(final HttpServletRequest request) throws IOException {
        if (!ServletHelper.checkParams(request, "matid", "amount", "message")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        final String matid = request.getParameter("matid");
        final String uid = request.getParameter("uid");
        final long userId = Long.parseLong(uid);
        final long adminId = (long) request.getSession().getAttribute("id");
        final String message = request.getParameter("message");
        final String stringAmount = request.getParameter("amount");
        final HelperResult<String> addItems =
                addItem(userId, matid, stringAmount, message, adminId);
        if (!addItems.wasSuccessful()) {
            request.setAttribute("error", Variables.translate(addItems.getMessage()));
        } else {
            request.setAttribute("info", Variables.translate(addItems.getMessage()));
        }
    }

    private HelperResult<String> addItem(final long userId, final String matid,
            final String stringAmount, final String message, final long adminId) {
        long amount;
        final String uid = String.valueOf(userId);
        if ("-1".equals(matid)) {
            amount = IDSConverter.stringToLong(stringAmount);
        } else {
            if (!Variables.resourceExists(matid)) {
                return new HelperResult<>(false, 411, "invalidMaterial", null);
            }
            amount = Long.parseLong(stringAmount);
        }
        final Successindicator executeAtomic = Main.instance().getConnector().executeAtomic(c -> {
            final Message addResource =
                    MySQLHelper.addResource(c, userId, true, matid, amount, message, true);
            Log.ADMIN.executeLog(c, Level.INFO, "Admin " + adminId + " added " + amount + " times "
                    + matid + " to the inventory of " + uid, adminId, userId);
            ItemRestAPI.invalidate(uid, () -> Item.getItems(userId, matid), false);
            MessageRestAPI.invalidate(uid, new Message[] { addResource }, false);
            return () -> true;
        });
        if (executeAtomic.wasSuccessful()) {
            return new HelperResult<>(true, 200, "itemAddSuccess", null);
        } else {
            return new HelperResult<>(false, 500, "unknownError", null);
        }
    }

    private void forcePayout(final HttpServletRequest request) throws IOException {
        if (!ServletHelper.checkParams(request, "matid", "amount", "uid", "message")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        final String matid = request.getParameter("matid");
        final HelperResult<Payout> createPayout = PayoutHelper.createPayout(
                "D" + request.getParameter("uid"), matid, request.getParameter("amount"), null,
                (Long) request.getSession().getAttribute("id"), request.getParameter("message"),
                request.getParameter("state"));
        if (createPayout.wasSuccessful()) {
            request.setAttribute("info", createPayout.getHumanReadableMessage());
        } else {
            request.setAttribute("error", createPayout.getHumanReadableMessage());
        }
    }

    private void cancelOrder(final HttpServletRequest request) throws IOException {
        if (!ServletHelper.checkParams(request, "id", "uid")) {
            request.setAttribute("error", Variables.translate("missingParameters"));
            return;
        }
        final Future<TradeResult> result =
                Main.instance().getTradeManager().cancel(Long.parseLong(request.getParameter("id")),
                        Long.parseLong(request.getParameter("uid")), ITEM_OR_STOCK.ITEM,
                        (Long) request.getSession().getAttribute("id"));
        try {
            final TradeResult tradeResult = result.get();
            if (tradeResult.isSuccess()) {
                request.setAttribute("info", Variables.translate(tradeResult.getResultString()));
            } else {
                request.setAttribute("error", Variables.translate(tradeResult.getResultString()));
            }
        } catch (final InterruptedException | ExecutionException e) {
            request.setAttribute("error", Variables.translate("unknownError"));
            e.printStackTrace();
        }
    }

}
