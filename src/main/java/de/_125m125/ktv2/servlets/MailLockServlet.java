package de._125m125.ktv2.servlets;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.EncryptionHelper;
import de._125m125.ktv2.calculators.EncryptionHelper.EncryptedData;
import de._125m125.ktv2.calculators.EncryptionHelper.EncryptionFailedException;
import de._125m125.ktv2.mail.MailManager;
import de._125m125.ktv2.servlets.helper.ServletHelper;

@WebServlet("/p/lockMail")
public class MailLockServlet extends HttpServlet {
    private static final MailManager MAIL_MANAGER = new MailManager();

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        if (!ServletHelper.checkParams(req, "mail", "iv")) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, Variables.translate("missingParameters"));
            return;
        }
        final String aesDecrypt;
        try {
            aesDecrypt = EncryptionHelper
                    .aesDecrypt(new EncryptedData(Base64.getUrlDecoder().decode(req.getParameter("mail")),
                            Base64.getUrlDecoder().decode(req.getParameter("iv").getBytes("UTF-8"))));
        } catch (final EncryptionFailedException | IllegalArgumentException e) {
            req.setAttribute("error", Variables.translate("invalidMailData"));
            getServletContext().getRequestDispatcher("/empty.jsp").forward(req, resp);
            return;
        }
        if (req.getParameter("lock") != null) {
            if (MailLockServlet.MAIL_MANAGER.deactivate(aesDecrypt)) {
                req.setAttribute("info", Variables.translate("mailLockSuccess", aesDecrypt,
                        req.getRequestURL().append('?').append(req.getQueryString().replaceAll("lock", "unlock"))));
            } else {
                req.setAttribute("info", Variables.translate("unknownError"));
            }
        } else if (req.getParameter("unlock") != null) {
            if (MailLockServlet.MAIL_MANAGER.activate(aesDecrypt)) {
                req.setAttribute("info", Variables.translate("mailUnlockSuccess", aesDecrypt));
            } else {
                req.setAttribute("info", Variables.translate("unknownError"));
            }
        }
        getServletContext().getRequestDispatcher("/empty.jsp").forward(req, resp);
    }
}
