package de._125m125.ktv2.servlets.helper;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

public class ServletHelper {
    public static boolean checkParams(final HttpServletRequest request, final String... requiredParams)
            throws IOException {
        for (final String s : requiredParams) {
            if (request.getParameter(s) == null) { return false; }
        }
        return true;
    }
}
