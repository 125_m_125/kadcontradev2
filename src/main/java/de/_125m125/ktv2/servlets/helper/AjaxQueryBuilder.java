package de._125m125.ktv2.servlets.helper;

import java.util.ArrayList;
import java.util.HashMap;

import de._125m125.ktv2.Main;

public abstract class AjaxQueryBuilder {

    private final String toSelect;
    private final String tableName;
    private final String orderBy;
    protected int        limit  = 10;
    protected int        offset = 0;

    public AjaxQueryBuilder(final String tableName, final String orderBy) {
        this("*", tableName, orderBy);
    }

    public AjaxQueryBuilder(final String toSelect, final String tableName, final String orderBy) {
        this.toSelect = toSelect;
        this.tableName = tableName;
        this.orderBy = orderBy;
    }

    public void setLimit(final int limit) {
        this.limit = limit;
    }

    public void setOffset(final int offset) {
        this.offset = offset;
    }

    public long getRowCount() {
        final StringBuilder query = new StringBuilder("SELECT COUNT(*) FROM ").append(this.tableName).append(" ");
        final ArrayList<String> params = createWhereContent();
        query.append(params.remove(0));
        final ArrayList<HashMap<String, Object>> executeQuery = Main.instance().getConnector()
                .executeQuery(query.toString(), params.toArray(new String[params.size()]));
        if (executeQuery != null && executeQuery.size() == 1) {
            return (long) executeQuery.get(0).get("COUNT(*)");
        }
        return 0L;
    }

    public String[] build() {
        final ArrayList<String> createQuery = createQuery();
        return createQuery.toArray(new String[createQuery.size()]);
    }

    public ArrayList<HashMap<String, Object>> execute() {
        final ArrayList<String> createQuery = createQuery();
        final String query = createQuery.remove(0);
        return Main.instance().getConnector().executeQuery(query, createQuery.toArray(new String[createQuery.size()]));
    }

    public ArrayList<String> createQuery() {
        final StringBuilder query = new StringBuilder("SELECT " + this.toSelect + " FROM ").append(this.tableName)
                .append(" ");
        final ArrayList<String> params = createWhereContent();

        query.append(params.get(0));

        query.append(" ORDER BY ").append(this.orderBy).append(" LIMIT ").append(this.limit).append(" OFFSET ")
                .append(this.offset);

        params.set(0, query.toString());
        return params;
    }

    protected abstract ArrayList<String> createWhereContent();
}
