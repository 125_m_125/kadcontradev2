package de._125m125.ktv2.servlets.apiv2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Payoutbox;
import de._125m125.ktv2.beans.PayoutboxList;
import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("admin/poboxes")
public class AdminPoboxRestAPI {

    private static AtomicReference<PayoutboxList> list = new AtomicReference<>();

    @Context
    private HttpServletRequest                    servletRequest;

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getPoBoxes() {
        if (!Permission.ALL_PERMISSIONS
                .isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        return Response.ok(getList()).build();
    }

    public static PayoutboxList getList() {
        PayoutboxList result = AdminPoboxRestAPI.list.get();
        if (result == null) {
            synchronized (AdminPoboxRestAPI.class) {
                result = AdminPoboxRestAPI.list.get();
                if (result == null) {
                    result = buildList();
                    AdminPoboxRestAPI.list.set(result);
                }
            }
        }
        return result;
    }

    public static boolean invalidate() {
        return AdminPoboxRestAPI.list.getAndSet(null) != null;
    }

    private static PayoutboxList buildList() {
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(
                        "SELECT * FROM payoutboxes LEFT JOIN login ON(payoutboxes.owner=login.id)")) {
            final PayoutboxList result = new PayoutboxList();
            final ResultSet results = ps.executeQuery();
            while (results.next()) {
                result.addEntry(new Payoutbox(results.getLong("cid"), results.getInt("server"),
                        results.getInt("x"), results.getInt("y"), results.getInt("z"),
                        results.getLong("owner"), results.getString("Name"),
                        results.getBoolean("verified")));
            }
            return result;
        } catch (final SQLException e) {
            throw new InternalServerErrorException("failed to retrieve payoutboxes");
        }
    }
}
