package de._125m125.ktv2.servlets.apiv2;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("users/{user}/sessions")
public class SessionRestAPI {

    @Context
    private HttpServletRequest servletRequest;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login2(@PathParam("user") final String user,
            @QueryParam("CSRFProtection") @DefaultValue("true") final boolean csrfProtection,
            @Context final Request request) {
        return login(user, csrfProtection, request);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@PathParam("user") final String user,
            @QueryParam("CSRFProtection") @DefaultValue("true") final boolean csrfProtection,
            @Context final Request request) {
        if (this.servletRequest.getSession(false) != null) {
            throw new WebApplicationException("sessionAlreadyExisting", Status.CONFLICT);
        }
        final int permissions = (int) this.servletRequest.getAttribute("permissions");
        this.servletRequest.getSession().setAttribute("tbid", this.servletRequest.getAttribute("id"));
        this.servletRequest.getSession().setAttribute("tknid", this.servletRequest.getAttribute("tknid"));
        this.servletRequest.getSession().setAttribute("permissions", permissions);
        this.servletRequest.getSession().setAttribute("noCsrfProtection", !csrfProtection);

        return Response.ok().entity(Permission.generateComPermissionMap(permissions)).build();
    }

    @DELETE
    public void logout2() {
        logout();
    }

    @POST
    @Path("/logout")
    public void logout() {
        if (this.servletRequest.getSession(false) != null) {
            this.servletRequest.getSession(false).invalidate();
        }
    }
}
