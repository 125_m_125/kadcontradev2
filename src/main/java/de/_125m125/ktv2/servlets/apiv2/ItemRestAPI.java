package de._125m125.ktv2.servlets.apiv2;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.ItemList;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.cache.ItemBuilder;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;

@Path("users/{user}/items")
public class ItemRestAPI {

    private static final LoadingCache<String, ItemList> resources = CacheBuilder.newBuilder()
            .maximumSize(50).expireAfterWrite(30, TimeUnit.MINUTES).build(new ItemBuilder());

    @Context
    private HttpServletRequest servletRequest;

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getItemList(@PathParam("user") final String user,
            @Context final Request request) {
        final String uid = Token.getBigInt(user).toString();
        final ItemList itemlist = CacheManager.getValue(ItemRestAPI.resources, uid,
                this.servletRequest, Permission.READ_ITEMLIST_PERMISSION);
        if (itemlist == null) {
            throw new ForbiddenException();
        }
        return CacheManager.managePrivateCaching(request, itemlist, "items.csv");
    }

    /* package */ Response getItemListUnchecked(final String user, final Request request) {
        final ItemList itemlist = ItemRestAPI.resources.getUnchecked(user);
        if (itemlist == null) {
            throw new NotFoundException();
        }
        return CacheManager.managePrivateCaching(request, itemlist, "items.csv");
    }

    public static void invalidateAll() {
        ItemRestAPI.resources.invalidateAll();
    }

    public static void invalidate(final String id, final Supplier<Item[]> items,
            final boolean selfCreated) {
        ItemRestAPI.resources.invalidate(id);
        final NotificationObservable notificationObservable =
                Main.instance().getNotificationObservable();
        if (notificationObservable != null) {
            final NotificationEvent e = new NotificationEvent(selfCreated, Long.parseLong(id),
                    "update", "items", Permission.READ_ITEMLIST_PERMISSION, items);
            notificationObservable.notifyObservers(e);
        }
    }

    @GET
    @Path("{itemid}")
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getItem(@PathParam("user") final String user,
            @PathParam("itemid") final String itemid, @Context final Request request) {
        final String uid = Token.getBigInt(user).toString();
        final ItemList itemlist = CacheManager.getValue(ItemRestAPI.resources, uid,
                this.servletRequest, Permission.READ_ITEMLIST_PERMISSION);
        if (itemlist == null) {
            throw new ForbiddenException();
        }
        final Optional<Item> item = Arrays.stream(itemlist.getEntries())
                .filter(i -> i.getId().equals(itemid)).findAny();
        if (!item.isPresent()) {
            throw new NotFoundException();
        }
        return CacheManager.managePrivateCaching(request, item.get(), "item-" + itemid + ".csv");
    }
}
