package de._125m125.ktv2.servlets.apiv2.helper;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import de._125m125.ktv2.beans.ErrorResponse;

@Provider
public class WebApplicationExceptionMapper implements ExceptionMapper<WebApplicationException> {

    public WebApplicationExceptionMapper() {
    }

    @Override
    public Response toResponse(final WebApplicationException exception) {
        final Object e = exception.getResponse().getEntity();
        if (e != null) {
            return exception.getResponse();
        } else {
            return Response.fromResponse(exception.getResponse()).type(MediaType.APPLICATION_JSON)
                    .entity(new ErrorResponse(exception)).build();
        }
    }

}
