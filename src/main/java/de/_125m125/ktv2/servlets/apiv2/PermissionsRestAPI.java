package de._125m125.ktv2.servlets.apiv2;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;

import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("permissions/{user}")
public class PermissionsRestAPI {

    @Context
    private HttpServletRequest servletRequest;

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Map<String, Boolean> getCurrentPermissions(@PathParam("user") final String user,
            @Context final Request request) {
        final Map<String, Boolean> permissionMap = Permission
                .generateComPermissionMap((int) this.servletRequest.getAttribute("permissions"));
        return permissionMap;
    }

}
