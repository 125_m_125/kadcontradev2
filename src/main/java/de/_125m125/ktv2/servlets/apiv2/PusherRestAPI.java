package de._125m125.ktv2.servlets.apiv2;

import java.io.StringReader;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Observable;
import java.util.Set;
import java.util.concurrent.locks.StampedLock;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.pusher.rest.Pusher;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.ThreadManager;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.PusherResult;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("pusher")
public class PusherRestAPI {
    private static final long        CHANNEL_UPDATE_DELAY = 5 * 60 * 1000;

    private static Pusher            pusher;
    private static boolean           initialized          = false;

    private static Set<String>       existingChannels     = new HashSet<>();
    private static long              channelSetExpiration = 0;
    private static final StampedLock channelLock          = new StampedLock();

    public static void initPusher(final NotificationObservable notificationObservable) {
        if (!PusherRestAPI.initialized) {
            synchronized (PusherRestAPI.class) {
                if (!PusherRestAPI.initialized) {
                    notificationObservable.addObserver(PusherRestAPI::update);
                    PusherRestAPI.pusher = new Pusher(Variables.getPusherAppId(), Variables.getPusherApiKey(),
                            Variables.getPusherApiSecret());
                    PusherRestAPI.pusher.setCluster("eu");
                    PusherRestAPI.pusher.setEncrypted(true);
                    PusherRestAPI.initialized = true;
                }
            }
        }
    }

    @Context
    private HttpServletRequest servletRequest;

    @POST
    @Path("/users/{user}/channels/{channel}/authentications")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(@PathParam("user") final String user, @PathParam("channel") final String channel,
            @FormParam("socketId") final String socketId,
            @FormParam("selfCreated") @DefaultValue("false") final boolean selfCreated,
            @Context final Request request) {
        if (socketId == null) {
            throw new BadRequestException("Missing Parameter: socketId");
        }
        if (!PusherRestAPI.initialized) {
            throw new IllegalStateException("the PusherAPIServlet was not initialized");
        }
        if (PusherRestAPI.pusher == null) {
            throw new IllegalStateException("the PusherAPIServlet was not initialized");
        }
        if (!channel.startsWith("r")) {
            throw new BadRequestException("notReadPermission");
        }
        final Permission p = Permission.of(channel);
        if (p == null) {
            throw new BadRequestException("unknownPermission");
        }
        if (!p.isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }

        final String base32Uid = Token.getBase32(Token.getBigInt(user));
        if (!user.equals(base32Uid)) {
            throw new BadRequestException("userNotBase32");
        }

        String fullChannel = "private-" + base32Uid + "_" + channel;
        if (selfCreated) {
            fullChannel = fullChannel + ".selfCreated";
        }

        final String authBody = PusherRestAPI.pusher.authenticate(socketId, fullChannel);
        PusherRestAPI.existingChannels.add(fullChannel);
        return Response.ok(new PusherResult(authBody, fullChannel)).build();
    }

    @POST
    @Path("/authenticate")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticateSimple(@QueryParam("user") final String user,
            @FormParam("channel_name") final String channelname, @FormParam("socketId") final String socketId,
            @Context final Request request) {
        if (user == null) {
            throw new BadRequestException("Missing Query Parameter: user");
        }
        if (channelname == null) {
            throw new BadRequestException("Missing Parameter: channel_name");
        }
        if (socketId == null) {
            throw new BadRequestException("Missing Parameter: socketId");
        }
        if (!PusherRestAPI.initialized) {
            throw new IllegalStateException("the PusherAPIServlet was not initialized");
        }
        if (PusherRestAPI.pusher == null) {
            throw new IllegalStateException("the PusherAPIServlet was not initialized");
        }
        if (!channelname.startsWith("private-")) {
            throw new BadRequestException("notPrivate");
        }
        final int split = channelname.indexOf("_");
        if (split < 9) {
            throw new BadRequestException(
                    "invalidChannelname. Format: private-<base32-uid>_<permission>[.selfCreated]");
        }
        final String uid = channelname.substring(8, split);
        try {
            if (!uid.equals(Token.getBase32(BigInteger.valueOf((long) this.servletRequest.getAttribute("id"))))) {
                throw new ForbiddenException("invalidBase32Userid");
            }
        } catch (final NumberFormatException e) {
            throw new BadRequestException("invalidBase32Userid");
        }
        final int split2 = channelname.indexOf(".");
        final String channel;
        if (split2 > split) {
            if (!".selfCreated".equals(channelname.substring(split2))) {
                throw new BadRequestException(
                        "invalidChannelname. Format: private-<base32-uid>_<permission>[.selfCreated]");
            }
            channel = channelname.substring(split + 1, split2);
        } else {
            channel = channelname.substring(split + 1);
        }
        if (!channel.startsWith("r")) {
            throw new BadRequestException("notReadPermission");
        }
        final Permission p = Permission.of(channel);
        if (p == null) {
            throw new BadRequestException("unknownPermission");
        }
        if (!p.isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException("permissionNotPresent");
        }

        final String authBody = PusherRestAPI.pusher.authenticate(socketId, channelname);
        PusherRestAPI.existingChannels.add(channelname);
        return Response.ok(new PusherResult(authBody, channelname)).build();
    }

    private static void triggerEvent(final String channel, final String eventName, final NotificationEvent data) {
        if (PusherRestAPI.channelSetExpiration < System.currentTimeMillis()) {
            final long writeLock = PusherRestAPI.channelLock.writeLock();
            try {
                if (PusherRestAPI.channelSetExpiration < System.currentTimeMillis()) {
                    updateChannels();
                    PusherRestAPI.channelSetExpiration = System.currentTimeMillis()
                            + PusherRestAPI.CHANNEL_UPDATE_DELAY;
                }
            } finally {
                PusherRestAPI.channelLock.unlockWrite(writeLock);
            }
        }
        long readLock;
        boolean found;
        do {
            readLock = PusherRestAPI.channelLock.tryOptimisticRead();
            found = PusherRestAPI.existingChannels.contains(channel);
        } while (!PusherRestAPI.channelLock.validate(readLock));
        if (!found) {
            return;
        }
        PusherRestAPI.pusher.trigger(channel, eventName, Variables.GSON.toJson(data.withoutContents()));
    }

    private static void updateChannels() {
        try (JsonReader reader = Json
                .createReader(new StringReader(PusherRestAPI.pusher.get("/channels").getMessage()))) {
            final JsonObject object = reader.readObject();
            PusherRestAPI.existingChannels = new HashSet<>(object.getJsonObject("channels").keySet());
        }
    }

    public static void triggerEvent(final String string, final String eventName, final NotificationEvent data,
            final Permission permission) {
        final String channel = "private-" + string + "_" + permission.getComName();
        ThreadManager.instance().submit(() -> triggerEvent(channel, eventName, data));
    }

    public static void triggerEvent(final String string, final String eventName, final boolean selfCreated,
            final NotificationEvent data, final Permission permission) {
        final String channel;
        if (selfCreated) {
            channel = "private-" + string + "_" + permission.getComName() + ".selfCreated";
        } else {
            channel = "private-" + string + "_" + permission.getComName();
        }
        ThreadManager.instance().submit(() -> triggerEvent(channel, eventName, data));
    }

    public static void update(final Observable o, final Object event) {
        if (!o.equals(Main.instance().getNotificationObservable()) || !(event instanceof NotificationEvent)) {
            return;
        }
        final NotificationEvent e = (NotificationEvent) event;
        if (e.getPermission() == Permission.NO_PERMISSIONS) {
            ThreadManager.instance().submit(() -> triggerEvent(e.getDetails().get("source"), e.getType(), e));
            ThreadManager.instance().submit(
                    () -> triggerEvent(e.getDetails().get("source") + "_" + e.getDetails().get("key"), e.getType(), e));
        } else {
            triggerEvent(e.getBase32Uid(), e.getType(), e.isSelfCreated(), e, e.getPermission());
        }
    }
}
