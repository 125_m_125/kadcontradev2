package de._125m125.ktv2.servlets.apiv2;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import de._125m125.ktv2.beans.ItemName;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;

@Path("/")
public class ItemlistRestAPI {
    @GET
    @Path("/itemnames")
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getItemlist(@Context final Request request) {
        return CacheManager.managePublicCaching(request, ItemName.getCurrentItemnames(),
                "itemnames");
    }

    @GET
    @Path("/stocknames")
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getStocklist(@Context final Request request,
            @QueryParam("lang") @DefaultValue("def") final String language) {
        return CacheManager.managePublicCaching(request, ItemName.getCurrentStocknames(language),
                "stocknames");
    }
}
