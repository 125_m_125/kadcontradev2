package de._125m125.ktv2.servlets.apiv2;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.TradeExecution;
import de._125m125.ktv2.beans.TradeExecutionWithoutItem;
import de._125m125.ktv2.beans.TsvableList;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.api.keys.TradeExecutionKey;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;
import jersey.repackaged.com.google.common.collect.Sets;

@Path("historicTrades/executed")
public class HistoricTradeExecutionRestAPI {
    private static final LoadingCache<TradeExecutionKey, TsvableList<TradeExecutionWithoutItem>> TRADE_EXECUTIONS;

    private static final MessageFormat                                                           QUERY =
            new MessageFormat(
                    "SELECT hist.time, buy.material material, hist.amount amount, (buy.price+sell.price)/2 as price FROM  tradesDoneHistory hist LEFT JOIN tradeHistory buy ON hist.id1=buy.id LEFT JOIN tradeHistory sell ON hist.id2=sell.id WHERE {0} AND {1} AND {2} AND {3} ORDER BY time DESC");

    static {
        final CacheLoader<TradeExecutionKey, TsvableList<TradeExecutionWithoutItem>> cacheLoader =
                new CacheLoader<TradeExecutionKey, TsvableList<TradeExecutionWithoutItem>>() {
                    @Override
                    public TsvableList<TradeExecutionWithoutItem> load(final TradeExecutionKey key)
                            throws Exception {
                        final TsvableList<TradeExecutionWithoutItem> result =
                                new TsvableList<TradeExecutionWithoutItem>(
                                        key.getMatid() != null ? TradeExecutionWithoutItem.HEADER
                                                : TradeExecution.HEADER) {
                                    @Override
                                    protected TradeExecutionWithoutItem[] createArray(
                                            final int size) {
                                        return new TradeExecutionWithoutItem[size];
                                    }
                                };
                        final String user = "TRUE";
                        final String dateMin = key.getStart() != null ? "hist.time>=?" : "TRUE";
                        final String dateMax = key.getEnd() != null ? "hist.time<?" : "TRUE";
                        final String matid = key.getMatid() != null ? "buy.material=?" : "TRUE";
                        try (Connection c = Main.instance().getConnector().getConnection();
                                PreparedStatement ps = c.prepareStatement(
                                        HistoricTradeExecutionRestAPI.QUERY.format(
                                                new String[] { user, dateMin, dateMax, matid }))) {
                            int i = 1;
                            if (key.getUserid() != null) {
                                ps.setLong(i++, key.getUserid());
                            }
                            if (key.getStart() != null) {
                                ps.setDate(i++, new Date(key.getStart()));
                            }
                            if (key.getEnd() != null) {
                                ps.setDate(i++, new Date(key.getEnd()));
                            }
                            if (key.getMatid() != null) {
                                ps.setString(i++, key.getMatid());
                            }
                            try (ResultSet rs = ps.executeQuery()) {
                                while (rs.next()) {
                                    if (key.getMatid() != null) {
                                        result.addEntry(new TradeExecutionWithoutItem(
                                                rs.getTimestamp("time"), rs.getInt("amount"),
                                                IDSConverter
                                                        .longToDoubleString(rs.getLong("price"))));
                                    } else {
                                        result.addEntry(new TradeExecution(rs.getTimestamp("time"),
                                                rs.getString("material"), rs.getInt("amount"),
                                                IDSConverter
                                                        .longToDoubleString(rs.getLong("price"))));
                                    }
                                }
                            }
                        }
                        return result;
                    }
                };
        TRADE_EXECUTIONS = CacheBuilder.newBuilder().maximumSize(50).build(cacheLoader);
    }

    /**
     * Get the historic statistics for an item
     *
     * @param itemid
     *            the id of the item
     * @param limitQ
     *            the limit of history entries
     * @param offsetQ
     *            days before today for the last entry
     * @param request
     *            the request
     * @return the history
     */
    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getHistory(@QueryParam("material") final String itemid,
            @QueryParam("start") final Long start, @QueryParam("end") final Long end,
            @QueryParam("user") final String user, @Context final Request request,
            @Context final HttpServletRequest servletRequest) {
        if (itemid != null && !Variables.resourceExistsOrIsMoney(itemid)) {
            throw new NotFoundException();
        }
        final TradeExecutionKey key = new TradeExecutionKey(start, end, itemid,
                user != null ? (Long) servletRequest.getAttribute("id") : null);
        final TsvableList<TradeExecutionWithoutItem> tradeList = CacheManager.getValue(
                HistoricTradeExecutionRestAPI.TRADE_EXECUTIONS, key, servletRequest,
                user != null ? Permission.READ_ORDERS_PERMISSION : Permission.NO_PERMISSIONS);
        if (tradeList == null) {
            throw new ForbiddenException();
        }
        if (end != null && end < System.currentTimeMillis()) {
            return CacheManager.managePublicMidnightCaching(request, tradeList,
                    "executedHistoricTrades-" + itemid);
        } else {
            return CacheManager.managePublicCaching(request, tradeList,
                    "executedHistoricTrades-" + itemid);
        }
    }

    public static void invalidate(final String material, final long time) {
        HistoricTradeExecutionRestAPI.TRADE_EXECUTIONS.invalidateAll(
                Sets.filter(HistoricTradeExecutionRestAPI.TRADE_EXECUTIONS.asMap().keySet(),
                        k -> (k.getMatid() == null || material.equals(k.getMatid()))
                                && k.containsTime(time)));
    }
}
