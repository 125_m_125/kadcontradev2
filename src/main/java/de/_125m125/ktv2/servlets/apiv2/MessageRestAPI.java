package de._125m125.ktv2.servlets.apiv2;

import java.util.Collection;
import java.util.function.Predicate;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.beans.MessageList;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.cache.LoadingPartialCache;
import de._125m125.ktv2.cache.MessageBuilder;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.api.keys.MessageKey;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;

@Path("users/{user}/messages")
public class MessageRestAPI {
    private static final LoadingCache<MessageKey, MessageList> MESSAGES;

    static {
        final CacheLoader<MessageKey, MessageList> cacheLoader = new MessageBuilder();
        final Predicate<? super MessageKey> predicate = key -> key.getLimit() == 50
                && key.getOffset() == 0;
        final LoadingCache<MessageKey, MessageList> cache = CacheBuilder.newBuilder()
                .maximumSize(50).build(cacheLoader);
        MESSAGES = new LoadingPartialCache<>(cache, cacheLoader, predicate);
    }

    @Context
    private HttpServletRequest servletRequest;

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getMessages(@PathParam("user") final String user,
            @Context final Request request, @QueryParam("limit") final Integer limitQ,
            @QueryParam("offset") final Integer offsetQ) {
        final long uid = Token.getBigInt(user).longValue();
        final int limit = limitQ != null ? Math.max(
                Math.min(offsetQ == null || offsetQ >= 0 ? limitQ : limitQ + offsetQ, 50), 0) : 50;
        final int offset = offsetQ != null ? Math.max(offsetQ, 0) : 0;
        final MessageKey messageKey = new MessageKey(uid, limit, offset);
        final MessageList messagelist = CacheManager.getValue(MessageRestAPI.MESSAGES, messageKey,
                this.servletRequest, Permission.READ_MESSAGES_PERMISSION);
        if (messagelist == null) {
            throw new ForbiddenException();
        }
        return CacheManager.managePrivateCaching(request, messagelist, "messages.csv");
    }

    public static void invalidateAll() {
        MessageRestAPI.MESSAGES.invalidateAll();
    }

    public static void invalidate(final String id, final Collection<Message> entries,
            final boolean selfCreated) {
        final NotificationEvent e = new NotificationEvent(selfCreated, Long.parseLong(id), "update",
                "messages", Permission.READ_MESSAGES_PERMISSION, entries);
        invalidate(id, e);
    }

    private static void invalidate(final String id, final NotificationEvent e) {
        MessageRestAPI.MESSAGES.invalidate(Sets.filter(MessageRestAPI.MESSAGES.asMap().keySet(),
                (k) -> Long.parseLong(id) == k.getUid()));
        final NotificationObservable notificationObservable = Main.instance()
                .getNotificationObservable();
        if (notificationObservable != null) {
            notificationObservable.notifyObservers(e);
        }
    }

    public static void invalidate(final String id, final Message[] msg, final boolean selfCreated) {
        final NotificationEvent e = new NotificationEvent(selfCreated, Long.parseLong(id), "update",
                "messages", Permission.READ_MESSAGES_PERMISSION, msg);
        invalidate(id, e);
    }
}
