package de._125m125.ktv2.servlets.apiv2.helper;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import de._125m125.ktv2.cache.Tsvable;

@Provider
@Produces({ "text/tsv", "text/tsv;charset=UTF-8" })
public class TsvProvider implements MessageBodyWriter<Tsvable> {
    private static final String UTF_8 = "UTF-8";

    public TsvProvider() {
    }

    @Override
    public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType) {
        return Tsvable.class.isAssignableFrom(type);
    }

    @Override
    public long getSize(final Tsvable t, final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(final Tsvable t, final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType, final MultivaluedMap<String, Object> httpHeaders,
            final OutputStream entityStream) throws IOException, WebApplicationException {
        if (httpHeaders.containsKey("Content-Disposition")) {
            final List<Object> list = httpHeaders.get("Content-Disposition");
            for (int i = 0; i < list.size(); ++i) {
                list.set(i, list.get(i) + ".tsv");
            }
        }
        try (final OutputStreamWriter writer = new OutputStreamWriter(entityStream, TsvProvider.UTF_8)) {
            writer.write(t.getTsvString(true));
        }
    }

}