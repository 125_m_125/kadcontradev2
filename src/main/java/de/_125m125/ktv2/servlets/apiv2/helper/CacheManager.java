package de._125m125.ktv2.servlets.apiv2.helper;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class CacheManager {
    public static <T extends Tsvable> Response managePrivateCaching(final Request request,
            final T value, final String filename) {
        return manageCaching(request, value, filename, true);
    }

    public static <T extends Tsvable> Response managePublicCaching(final Request request,
            final T value, final String filename) {
        return manageCaching(request, value, filename, false);
    }

    public static <T extends Tsvable> Response manageCaching(final Request request, final T value,
            final String filename, final boolean priv) {
        final CacheControl cc = new CacheControl();
        cc.setMaxAge(3600);
        cc.setNoCache(true);
        // cc.setNoStore(true);
        cc.setPrivate(priv);
        cc.setNoTransform(false);

        final EntityTag etag = new EntityTag(value.getETag());
        ResponseBuilder builder = request.evaluatePreconditions(etag);
        if (builder == null) {
            builder = Response.ok(value);
            builder.tag(etag);
            builder.cacheControl(cc);
            builder.header("Content-Disposition", "filename=" + filename);
        }
        return builder.build();
    }

    public static <T extends Tsvable> Response managePublicMidnightCaching(final Request request,
            final T value, final String filename) {
        final CacheControl cc = new CacheControl();
        cc.setNoCache(false);
        cc.setPrivate(false);
        cc.setNoTransform(false);

        final EntityTag etag = new EntityTag(value.getETag());
        ResponseBuilder builder = request.evaluatePreconditions(etag);
        if (builder == null) {
            builder = Response.ok(value);
            builder.tag(etag);
            builder.cacheControl(cc);
            builder.header("Content-Disposition", "filename=" + filename);
            builder.expires(getExpiresAfterMidnight());
        }
        return builder.build();
    }

    public static <K, V> V getValue(final LoadingCache<K, V> resources, final K key,
            final HttpServletRequest request, final Permission requiredPermission) {
        if (key == null || (requiredPermission != Permission.NO_PERMISSIONS
                && (request.getAttribute("permissions") == null || !requiredPermission
                        .isPresentIn((int) request.getAttribute("permissions"))))) {
            return null;
        }
        return resources.getUnchecked(key);
    }

    private static Date getExpiresAfterMidnight() {
        final ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
        if (now.getHour() == 0 && now.getMinute() < 10) {
            return Date.from(now.toInstant());
        }
        ZonedDateTime target = ZonedDateTime.of(now.getYear(), now.getMonthValue(),
                now.getDayOfMonth(), 00, 00, 0, 0, ZoneId.systemDefault());
        if (target.isBefore(now)) {
            target = target.plusDays(1);
        }
        return Date.from(target.toInstant());
    }
}
