package de._125m125.ktv2.servlets.apiv2.helper;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import de._125m125.ktv2.cache.Tsvable;

@Provider
@Produces({ "text/tsv", "text/tsv;charset=UTF-8" })
public class TsvFromMapProvider implements MessageBodyWriter<Object> {
    private static final String UTF_8 = "UTF-8";

    public TsvFromMapProvider() {}

    @Override
    public boolean isWriteable(final Class<?> type, final Type genericType,
            final Annotation[] annotations, final MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(final Object t, final Class<?> type, final Type genericType,
            final Annotation[] annotations, final MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(final Object t, final Class<?> type, final Type genericType,
            final Annotation[] annotations, final MediaType mediaType,
            final MultivaluedMap<String, Object> httpHeaders, final OutputStream entityStream)
            throws IOException, WebApplicationException {
        if (t instanceof Iterable) {
            final Iterable<?> l = (Iterable<?>) t;
            String header = null;
            try (final OutputStreamWriter writer =
                    new OutputStreamWriter(entityStream, TsvFromMapProvider.UTF_8)) {
                for (final Object o : l) {
                    if (o instanceof Tsvable) {
                        final Tsvable tsvable = (Tsvable) o;
                        if (header == null) {
                            writer.write(tsvable.getTsvString(true));
                            header = tsvable.getTsvHeader();
                        } else {
                            if (tsvable.getTsvHeader().equals(header)) {
                                writer.write(tsvable.getTsvString(false));
                            } else {
                                throw new IllegalArgumentException(
                                        "incompatible tsvables in list: expected " + header
                                                + " but got " + tsvable.getTsvHeader());
                            }
                        }
                    } else if (o instanceof Map) {
                        final Map<?, ?> map = (Map<?, ?>) o;
                        final List<SimpleEntry<String, String>> collect = map.entrySet().stream()
                                .map(e -> new AbstractMap.SimpleEntry<>(e.getKey().toString(),
                                        e.getValue().toString()))
                                .sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
                                .collect(Collectors.toList());
                        final String currentHeader = collect.stream().map(Entry::getKey)
                                .collect(Collectors.joining("\t"));
                        if (header != null) {
                            if (!header.equals(currentHeader)) {
                                throw new IllegalArgumentException("incompatible entries: expected "
                                        + header + " but got " + currentHeader);
                            }
                        } else {
                            header = currentHeader;
                            writer.write(header + "\r\n");
                        }
                        writer.write(collect.stream().map(Entry::getValue)
                                .collect(Collectors.joining("\t")) + "\r\n");
                    } else {
                        throw new IllegalArgumentException(
                                "list contains elements that are not tsvables or maps: "
                                        + o.getClass() + ": " + o);
                    }
                }
            }
        }
    }

}
