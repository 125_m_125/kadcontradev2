package de._125m125.ktv2.servlets.apiv2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.beans.WebNotificationSubscriptionState;
import de._125m125.ktv2.notifications.FirebaseNotificationManager;

@Path("users/{user}/webpush")
public class WebPushNotificationAPI {

    @GET
    @Path("/{token}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWPState(@PathParam("token") final String token,
            @Context final HttpServletRequest request) {
        if (!FirebaseNotificationManager.instance().isConnected()) {
            throw new InternalServerErrorException(
                    "The server is currently not connected to firebase. Please try again later.");
        }
        return get((long) request.getAttribute("id"), token);
    }

    @DELETE
    @Path("/{token}")
    public Response deleteWPState(@PathParam("token") final String token,
            @Context final HttpServletRequest request) {
        if (!FirebaseNotificationManager.instance().isConnected()) {
            throw new InternalServerErrorException(
                    "The server is currently not connected to firebase. Please try again later.");
        }
        return remove((long) request.getAttribute("id"), token);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createWP(@FormParam("token") final String token,
            @FormParam("itemlist") @DefaultValue("false") final boolean itemlist,
            @FormParam("messages") @DefaultValue("false") final boolean messages,
            @FormParam("orders") @DefaultValue("false") final boolean orders,
            @FormParam("payouts") @DefaultValue("false") final boolean payouts,
            @FormParam("workers") @DefaultValue("false") final boolean workers,
            @Context final HttpServletRequest request, @Context final UriInfo uriInfo) {
        update(true, (long) request.getAttribute("id"), token, itemlist, messages, orders, payouts,
                workers);
        return Response.created(uriInfo.getAbsolutePathBuilder().path(token).build()).build();
    }

    @PUT
    @Path("/{token}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response setWPState(@PathParam("token") final String token,
            @FormParam("itemlist") @DefaultValue("false") final boolean itemlist,
            @FormParam("messages") @DefaultValue("false") final boolean messages,
            @FormParam("orders") @DefaultValue("false") final boolean orders,
            @FormParam("payouts") @DefaultValue("false") final boolean payouts,
            @FormParam("workers") @DefaultValue("false") final boolean workers,
            @Context final HttpServletRequest request) {
        if (!FirebaseNotificationManager.instance().isConnected()) {
            throw new InternalServerErrorException(
                    "The server is currently not connected to firebase. Please try again later.");
        }
        update(false, (long) request.getAttribute("id"), token, itemlist, messages, orders, payouts,
                workers);
        return Response.ok().build();
    }

    @POST
    @Path("/{token}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response setWPState2(@PathParam("token") final String token,
            @FormParam("itemlist") @DefaultValue("false") final boolean itemlist,
            @FormParam("messages") @DefaultValue("false") final boolean messages,
            @FormParam("orders") @DefaultValue("false") final boolean orders,
            @FormParam("payouts") @DefaultValue("false") final boolean payouts,
            @FormParam("workers") @DefaultValue("false") final boolean workers,
            @Context final HttpServletRequest request) {
        return setWPState(token, itemlist, messages, orders, payouts, workers, request);
    }

    private void update(final boolean insertOnly, final long uid, final String token,
            final boolean itemlist, final boolean messages, final boolean orders,
            final boolean payouts, final boolean workers) {
        manageTopics(uid, token, itemlist, messages, orders, payouts, workers);
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement((insertOnly ? "INSERT" : "REPLACE")
                        + " INTO webPushSubscriptions(user,token,itemlist,messages,orders,payouts,workers) VALUES(?,?,?,?,?,?,?)")) {
            ps.setLong(1, uid);
            ps.setString(2, token);
            ps.setBoolean(3, itemlist);
            ps.setBoolean(4, messages);
            ps.setBoolean(5, orders);
            ps.setBoolean(6, payouts);
            ps.setBoolean(7, workers);
            ps.executeUpdate();
        } catch (final SQLException e) {
            if (e.getErrorCode() == 1062) {
                throw new ClientErrorException(Response.Status.CONFLICT);
            }
            e.printStackTrace();
            throw new InternalServerErrorException();
        }
    }

    private void manageTopics(final long uid, final String token, final boolean itemlist,
            final boolean messages, final boolean orders, final boolean payouts,
            final boolean workers) {
        if (itemlist) {
            FirebaseNotificationManager.instance().subscribe(uid, token, "itemlist");
        } else {
            FirebaseNotificationManager.instance().unsubscribe(uid, token, "itemlist");
        }
        if (messages) {
            FirebaseNotificationManager.instance().subscribe(uid, token, "messages");
        } else {
            FirebaseNotificationManager.instance().unsubscribe(uid, token, "messages");
        }
        if (orders) {
            FirebaseNotificationManager.instance().subscribe(uid, token, "orders");
        } else {
            FirebaseNotificationManager.instance().unsubscribe(uid, token, "orders");
        }
        if (payouts) {
            FirebaseNotificationManager.instance().subscribe(uid, token, "payouts");
        } else {
            FirebaseNotificationManager.instance().unsubscribe(uid, token, "payouts");
        }
        if (UserHelper.isAdmin(uid)) {
            if (workers) {
                FirebaseNotificationManager.instance().subscribe(uid, token, "workers");
            } else {
                FirebaseNotificationManager.instance().unsubscribe(uid, token, "workers");
            }
        }
    }

    private Response get(final long uid, final String token) {
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(
                        "SELECT itemlist,messages,orders,payouts FROM webPushSubscriptions WHERE user=? AND token=?")) {
            ps.setLong(1, uid);
            ps.setString(2, token);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    final WebNotificationSubscriptionState state =
                            new WebNotificationSubscriptionState(rs.getBoolean("itemlist"),
                                    rs.getBoolean("messages"), rs.getBoolean("orders"),
                                    rs.getBoolean("payouts"));
                    return Response.ok(state).build();
                } else {
                    throw new NotFoundException();
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new InternalServerErrorException();
        }
    }

    private Response remove(final long uid, final String token) {
        manageTopics(uid, token, false, false, false, false, false);
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(
                        "DELETE FROM webPushSubscriptions WHERE user=? AND token=?")) {
            ps.setLong(1, uid);
            ps.setString(2, token);
            final int update = ps.executeUpdate();
            if (update == 0) {
                throw new NotFoundException();
            }
            return Response.ok().build();
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new InternalServerErrorException();
        }
    }

}
