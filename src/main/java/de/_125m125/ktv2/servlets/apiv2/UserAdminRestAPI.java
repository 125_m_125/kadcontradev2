package de._125m125.ktv2.servlets.apiv2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.ItemPayinResult;
import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.beans.Payout;
import de._125m125.ktv2.beans.User;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.api.helper.HelperResult;
import de._125m125.ktv2.servlets.api.helper.PayoutHelper;
import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("admins/{admin}/namedUsers")
public class UserAdminRestAPI {

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getUsers(@PathParam("admin") final String requestedAdminId,
            @Context final HttpServletRequest httpRequest) {
        final long adminId = (long) httpRequest.getAttribute("id");
        if (!UserHelper.isAdmin(adminId)) {
            throw new ForbiddenException();
        }
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement("SELECT id,Name FROM login");
                ResultSet rs = ps.executeQuery()) {
            final List<User> users = new ArrayList<>();
            while (rs.next()) {
                users.add(new User(rs.getLong("id"), rs.getString("Name")));
            }
            return Response.ok(users).build();
        } catch (final SQLException e) {
            throw new InternalServerErrorException("unknownError");
        }
    }

    @GET
    @Path("{target}/items")
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getItems(@PathParam("target") final String targetUser,
            @PathParam("admin") final String requestedAdminId,
            @Context final HttpServletRequest httpRequest, @Context final Request jaxrsRequest) {
        final UserAdminRequestData data = createForExistingUser(httpRequest, targetUser,
                Permission.READ_ADMIN_ITEMLIST_PERMISSION);
        return new ItemRestAPI().getItemListUnchecked(String.valueOf(data.target), jaxrsRequest);
    }

    @POST
    @Path("{target}/items")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addItems(@PathParam("target") final String targetUser,
            @PathParam("admin") final String requestedAdminId,
            @QueryParam("message") @DefaultValue("") final String message,
            @QueryParam("nameIds") @DefaultValue("false") final boolean nameIds,
            @Context final HttpServletRequest httpRequest, final Map<String, String> items) {
        final UserAdminRequestData data = createForExistingUser(httpRequest, targetUser,
                Permission.WRITE_ADMIN_USERITEMS_PERMISSION);
        final List<Item> convertedItems;
        if (items != null) {
            convertedItems = items.entrySet().stream().map(e -> {
                final String key = e.getKey().replaceAll(":0", "");
                try {
                    return new Item(
                            nameIds && key != null ? Variables.NEWID_TO_ID.getOrDefault(key, key)
                                    : key,
                            e.getValue());
                } catch (final NumberFormatException ex) {
                    throw new WebApplicationException("InvalidAmount " + e.getValue(), 422);
                }
            }).collect(Collectors.toList());
        } else {
            convertedItems = new ArrayList<>();
        }
        final HelperResult<ItemPayinResult> addItems =
                addItems(data.target, convertedItems, message, data.admin);
        return Response.status(addItems.getStatus()).entity(addItems).build();
    }

    private HelperResult<ItemPayinResult> addItems(final long userId, final List<Item> items,
            final String message, final long adminId) {
        final String uid = String.valueOf(userId);
        final ItemPayinResult result = new ItemPayinResult();
        if (items.isEmpty()) {
            return new HelperResult<>(true, 200, "itemAddSuccess", result);
        }
        final Map<String, Long> collect = items.stream().collect(
                Collectors.groupingBy(Item::getId, Collectors.summingLong(Item::getLongAmount)));
        final List<Message> messages = new ArrayList<>(collect.size());
        final Successindicator executeAtomic = Main.instance().getConnector().executeAtomic(c -> {
            for (final Entry<String, Long> entry : collect.entrySet()) {
                final String matid = entry.getKey();
                if (!Variables.resourceExists(matid)) {
                    result.addFailed(matid);
                    continue;
                }
                final long amount = entry.getValue();
                final Message addResource =
                        MySQLHelper.addResource(c, userId, true, matid, amount, message, true);
                messages.add(addResource);
                Log.ADMIN.executeLog(c, Level.INFO, "Admin " + adminId + " added " + amount
                        + " times " + matid + " to the inventory of " + uid, adminId, userId);
                result.addSucceeded(matid, amount);
            }
            if (result.hasSuccesses()) {
                ItemRestAPI
                        .invalidate(uid,
                                () -> Item.getItems(userId,
                                        Arrays.stream(result.getSucceeded().getEntries())
                                                .map(Item::getId).toArray(String[]::new)),
                                userId == adminId);
            }
            if (!messages.isEmpty()) {
                MessageRestAPI.invalidate(uid, messages.toArray(new Message[messages.size()]),
                        false);
            }
            return () -> true;
        });
        if (executeAtomic.wasSuccessful()) {
            if (!result.hasFailures()) {
                return new HelperResult<>(true, 200, "itemAddSuccess", result);
            } else if (!result.hasSuccesses()) {
                return new HelperResult<>(true, 411, "NoKnownItems", result);
            } else {
                return new HelperResult<>(true, 207, "SomeUnknownItems", result);
            }
        } else {
            return new HelperResult<>(false, 500, "unknownError", null);
        }
    }

    @PUT
    @Path("{target}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(@PathParam("target") final String targetUser,
            @PathParam("admin") final String requestedAdminId,
            @QueryParam("reason") final String reason,
            @Context final HttpServletRequest httpRequest, @Context final UriInfo uriInfo) {
        final UserAdminRequestData data =
                create(httpRequest, targetUser, Permission.WRITE_ADMIN_REGISTER_PERMISSION);
        if (data.target > 0) {
            throw new WebApplicationException("UserAlreadyExists", 409);
        }
        final String createUser = Main.instance().getConnector().createUser(targetUser, "", "", 0,
                "", reason, data.admin);
        if (createUser.startsWith("success")) {
            return Response.created(uriInfo.getAbsolutePath()).entity(
                    new HelperResult<>(true, 201, "userCreationSuccess", createUser.substring(7)))
                    .build();
        } else {
            throw new InternalServerErrorException();
        }
    }

    @POST
    @Path("{target}/payouts")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPayout(@PathParam("target") final String targetUser,
            @PathParam("admin") final String requestedAdminId, @FormParam("item") final String item,
            @FormParam("amount") final String amount, @FormParam("type") final String type,
            @FormParam("state") @DefaultValue("open") final String state,
            @FormParam("message") @DefaultValue("") final String reason,
            @Context final HttpServletRequest httpRequest) {
        final UserAdminRequestData data = createForExistingUser(httpRequest, targetUser,
                Permission.WRITE_ADMIN_PAYOUTS_PERMISSION);
        final HelperResult<Payout> createPayout = PayoutHelper.createPayout(targetUser, item,
                amount, type, data.admin, reason, state);
        return Response.status(createPayout.getStatus()).entity(createPayout).build();
    }

    private UserAdminRequestData createForExistingUser(final HttpServletRequest httpRequest,
            final String targetUser, final Permission permission) {
        final UserAdminRequestData created = create(httpRequest, targetUser, permission);
        if (created.target <= 0) {
            throw new NotFoundException("TargetUserNotExisting");
        }
        return created;
    }

    private UserAdminRequestData create(final HttpServletRequest httpRequest,
            final String targetUser, final Permission permission) {
        final long adminId = (long) httpRequest.getAttribute("id");
        if (!permission.isPresentIn((int) httpRequest.getAttribute("permissions"))
                || !UserHelper.isAdmin(adminId)) {
            throw new ForbiddenException();
        }
        final long targetUserId = UserHelper.get(targetUser);
        return new UserAdminRequestData(adminId, targetUserId);
    }

    private class UserAdminRequestData {

        private final long admin;
        private final long target;

        public UserAdminRequestData(final long admin, final long target) {
            this.admin = admin;
            this.target = target;

        }
    }
}
