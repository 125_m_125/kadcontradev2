package de._125m125.ktv2.servlets.apiv2.helper;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.TsvableList;

@Provider
@Produces({ MediaType.APPLICATION_JSON, "text/json" })
public class GsonJsonProvider implements MessageBodyWriter<Object> {
    private static final String UTF_8 = "UTF-8";

    public GsonJsonProvider() {
    }

    @Override
    public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(final Object t, final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(Object t, final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType, final MultivaluedMap<String, Object> httpHeaders,
            final OutputStream entityStream) throws IOException, WebApplicationException {
        if (t instanceof TsvableList) {
            t = ((TsvableList<?>) t).getEntries();
        }
        if (httpHeaders.containsKey("Content-Disposition")) {
            final List<Object> list = httpHeaders.get("Content-Disposition");
            for (int i = 0; i < list.size(); ++i) {
                list.set(i, list.get(i) + ".json");
            }
        }
        try (final OutputStreamWriter writer = new OutputStreamWriter(entityStream, GsonJsonProvider.UTF_8)) {
            Variables.GSON.toJson(t, writer);
        }
    }

}