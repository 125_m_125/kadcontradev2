package de._125m125.ktv2.servlets.apiv2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.google.common.collect.ImmutableMap;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("workers/{worker}/payouts")
public class WorkerPayoutAPI {

    @GET
    public Response getOpenPayoutRequests(@PathParam("worker") String requestedWorkerId,
            @Context HttpServletRequest servletRequest) {
        final long adminId = (long) servletRequest.getAttribute("id");
        if (!Permission.READ_WORKER_PAYOUTS_PERMISSION
                .isPresentIn((int) servletRequest.getAttribute("permissions"))
                || !UserHelper.isAdmin(adminId)) {
            throw new ForbiddenException();
        }
        final List<Map<String, String>> entries = new ArrayList<>();
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(
                        "SELECT payoutType, COUNT(*) AS count FROM payout WHERE requiresAdminInteraction=true GROUP BY payoutType");
                ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                entries.add(ImmutableMap.of("type", rs.getString("payoutType"), "count",
                        rs.getString("count")));
            }
            return Response.ok(entries).build();
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new InternalServerErrorException("unknownError");
        }
    }
}
