package de._125m125.ktv2.servlets.apiv2;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.beans.TradeSlotList;
import de._125m125.ktv2.beans.UpdatedTradeSlot;
import de._125m125.ktv2.cache.TradeSlotBuilder;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.market.Trade;
import de._125m125.ktv2.market.Trade.buySell;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.HelperResult;
import de._125m125.ktv2.servlets.api.helper.MarketHelper;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;

@Path("users/{user}/orders")
public class TradeRestAPI {
    private static final LoadingCache<String, TradeSlotList> TRADES = CacheBuilder.newBuilder()
            .maximumSize(50).expireAfterWrite(30, TimeUnit.MINUTES).build(new TradeSlotBuilder());

    @Context
    private HttpServletRequest servletRequest;

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTrade(@PathParam("user") final String user,
            @FormParam("buySell") final buySell buySell, @FormParam("item") final String item,
            @FormParam("amount") final int amount, @FormParam("price") final String price) {
        if (!Permission.WRITE_ORDERS_PERMISSION
                .isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        if (user == null) {
            throw new BadRequestException("missing user");
        }
        if (buySell == null) {
            throw new BadRequestException("missingBuySell");
        }
        if (item == null) {
            throw new BadRequestException("missing itemid");
        }
        if (price == null) {
            throw new BadRequestException("missing price");
        }

        final long iPricePerItem;
        final long uid = Token.getBigInt(user).longValue();
        try {
            if (amount <= 0 || amount > 3456) {
                throw new WebApplicationException("amountOutOfBounds", 422);
            }
            iPricePerItem = IDSConverter.stringToLong(price, 2);
            if (iPricePerItem <= 0) {
                throw new WebApplicationException("priceOutOfBounds", 422);
            }
            if (!Variables.resourceExists(item)) {
                throw new WebApplicationException("unknownMaterial", 422);
            }
        } catch (final NumberFormatException e) {
            throw new WebApplicationException(e.getMessage(), 422);
        }
        final HelperResult<UpdatedTradeSlot> result =
                MarketHelper.createTrade(uid, buySell, item, amount, iPricePerItem);
        return Response.status(result.getStatus()).entity(result).build();
    }

    @POST
    @Path("/{orderId}/cancel")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelTrade(@PathParam("user") final String user,
            @PathParam("orderId") final long orderId, @Context final Request request) {
        if (!Permission.WRITE_ORDERS_PERMISSION
                .isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        final HelperResult<UpdatedTradeSlot> result = MarketHelper.cancelTrade(user, orderId);
        return Response.status(result.getStatus()).entity(result).build();
    }

    @DELETE
    @Path("/{orderId}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelTradeByDelete(@PathParam("user") final String user,
            @PathParam("orderId") final long orderId, @Context final Request request) {
        return cancelTrade(user, orderId, request);
    }

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getTrades(@PathParam("user") final String user,
            @Context final Request request) {
        final String uid = Long.toString(Token.getBigInt(user).longValue());
        final TradeSlotList tradeSlotList = CacheManager.getValue(TradeRestAPI.TRADES, uid,
                this.servletRequest, Permission.READ_ORDERS_PERMISSION);
        if (tradeSlotList == null) {
            throw new ForbiddenException();
        }
        return CacheManager.managePrivateCaching(request, tradeSlotList, "trades.csv");
    }

    public static void invalidate(final Trade trade) {
        TradeRestAPI.TRADES.invalidate(Long.toString(trade.getOwner()));
        final NotificationObservable notificationObservable =
                Main.instance().getNotificationObservable();
        if (notificationObservable != null) {
            final NotificationEvent e = new NotificationEvent(true, trade.getID(), "update",
                    "trades", Permission.READ_ORDERS_PERMISSION, new Object[] { trade });
            notificationObservable.notifyObservers(e);
        }
    }

    public static void invalidate(final long sourceUser, final Trade first, final Trade... trades) {
        invalidate(sourceUser, Stream.concat(Stream.of(first), Arrays.stream(trades)));
    }

    public static void invalidate(final long sourceUser, final List<Trade> trades) {
        invalidate(sourceUser, trades.stream());
    }

    public static void invalidate(final long sourceUser, final Stream<Trade> trades) {
        final Map<Long, List<Trade>> collect =
                trades.collect(Collectors.groupingBy(Trade::getOwner));
        final NotificationObservable notificationObservable =
                Main.instance().getNotificationObservable();
        collect.entrySet().forEach(entry -> {
            TradeRestAPI.TRADES.invalidate(entry.getKey().toString());
            if (notificationObservable != null) {
                final NotificationEvent e = new NotificationEvent(entry.getKey().equals(sourceUser),
                        entry.getKey(), "update", "trades", Permission.READ_ORDERS_PERMISSION,
                        entry.getValue().stream().map(Trade::toTradeSlot)
                                .collect(Collectors.toList()));
                notificationObservable.notifyObservers(e);
            }
        });
    }

    public static TradeSlotList get(final String id) throws ExecutionException {
        return TradeRestAPI.TRADES.get(id);
    }

}
