package de._125m125.ktv2.servlets.apiv2;

import java.util.Optional;
import java.util.function.Predicate;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.History;
import de._125m125.ktv2.beans.HistoryEntry;
import de._125m125.ktv2.cache.LoadingPartialCache;
import de._125m125.ktv2.cache.stats.HistoryBuilder;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.api.keys.HistoryKey;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;

@Path("history/{itemid}")
public class HistoryRestAPI {

    private static final LoadingCache<HistoryKey, History> HISTORIES;

    static {
        final CacheLoader<HistoryKey, History> cacheLoader = new HistoryBuilder();
        final Predicate<? super HistoryKey> predicate =
                key -> (key.getLimit() == 30 || key.getLimit() == 1) && key.getOffset() == 0;
        final LoadingCache<HistoryKey, History> cache =
                CacheBuilder.newBuilder().maximumSize(50).build(cacheLoader);
        HISTORIES = new LoadingPartialCache<>(cache, cacheLoader, predicate);
    }

    @Context
    private HttpServletRequest servletRequest;

    /**
     * Get the historic statistics for an item
     *
     * @param itemid
     *            the id of the item
     * @param limitQ
     *            the limit of history entries
     * @param offsetQ
     *            days before today for the last entry
     * @param request
     *            the request
     * @return the history
     */
    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getHistory(@PathParam("itemid") final String itemid,
            @QueryParam("limit") @DefaultValue("30") final Integer limitQ,
            @DefaultValue("0") @QueryParam("offset") final Integer offsetQ,
            @Context final Request request) {
        if (!Variables.resourceExistsOrIsMoney(itemid)) {
            throw new NotFoundException();
        }
        final int limit = limitQ != null ? Math.max(
                Math.min(offsetQ == null || offsetQ >= 0 ? limitQ : limitQ + offsetQ, 30), 0) : 30;
        final int offset = offsetQ != null ? Math.max(offsetQ, 0) : 0;
        final HistoryKey key = new HistoryKey(itemid, limit, offset);
        final History history = CacheManager.getValue(HistoryRestAPI.HISTORIES, key,
                this.servletRequest, Permission.NO_PERMISSIONS);
        if (history == null) {
            throw new ForbiddenException();
        }
        return CacheManager.managePublicMidnightCaching(request, history, "history-" + itemid);
    }

    /**
     * Get the latest statistics for an item
     *
     * @param itemid
     *            the id of the item
     * @param request
     *            the request
     * @return the history
     */
    @GET
    @Path("/latest")
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getLatestHistoryEntry(@PathParam("itemid") final String itemid,
            @Context final Request request) {
        if (!Variables.resourceExistsOrIsMoney(itemid)) {
            throw new NotFoundException();
        }
        final HistoryKey key = new HistoryKey(itemid, 1, 0);
        final History history = CacheManager.getValue(HistoryRestAPI.HISTORIES, key,
                this.servletRequest, Permission.NO_PERMISSIONS);
        if (history == null) {
            throw new ForbiddenException();
        }
        if (history.isEmpty()) {
            return CacheManager.managePublicMidnightCaching(request, new History(),
                    "history-" + itemid);
        }
        return CacheManager.managePublicMidnightCaching(request, history.getEntries()[0],
                "history-" + itemid);
    }

    public static void invalidateAll() {
        HistoryRestAPI.HISTORIES.invalidateAll();

        final NotificationObservable notificationObservable =
                Main.instance().getNotificationObservable();
        if (notificationObservable != null) {
            final NotificationEvent ne = new NotificationEvent(false, 0, "update", "history", "",
                    Permission.NO_PERMISSIONS);
            notificationObservable.notifyObservers(ne);
        }
    }

    public static long getLatest(final String itemid) {
        final HistoryKey key = new HistoryKey(itemid, 1, 0);
        final HistoryEntry[] entries = HistoryRestAPI.HISTORIES.getUnchecked(key).getEntries();
        if (entries.length == 0) {
            return 10_000000;
        }
        return IDSConverter.stringToLong(entries[0].getClose());
    }

    public static Optional<Long> getLatestIfPresent(final String itemid) {
        final HistoryKey key = new HistoryKey(itemid, 1, 0);
        final HistoryEntry[] entries = HistoryRestAPI.HISTORIES.getUnchecked(key).getEntries();
        if (entries.length == 0) {
            return Optional.empty();
        }
        return Optional.of(IDSConverter.stringToLong(entries[0].getClose()));
    }

}
