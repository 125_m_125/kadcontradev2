package de._125m125.ktv2.servlets.apiv2;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Payout;
import de._125m125.ktv2.beans.PayoutList;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.cache.payout.PayoutBuilder;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.HelperResult;
import de._125m125.ktv2.servlets.api.helper.PayoutHelper;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.api.keys.PayoutKey;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;

@Path("users/{user}/payouts")
public class PayoutRestAPI {
    private static final LoadingCache<PayoutKey, PayoutList> PAYOUTS = CacheBuilder.newBuilder()
            .maximumSize(50).expireAfterWrite(30, TimeUnit.MINUTES).build(new PayoutBuilder());

    @Context
    private HttpServletRequest                               servletRequest;

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getPayoutList(@PathParam("user") final String user,
            @QueryParam("offset") final Integer offsetQ, @QueryParam("limit") final Integer limitQ,
            @QueryParam("interaction") final Boolean interactionQ, @Context final Request request) {
        final long uid = Token.getBigInt(user).longValue();
        final int limit = limitQ != null ? Math.max(
                Math.min(offsetQ == null || offsetQ >= 0 ? limitQ : limitQ + offsetQ, 30), 0) : 30;
        final int offset = offsetQ != null ? Math.max(offsetQ, 0) : 0;
        final boolean interaction = Boolean.TRUE.equals(interactionQ);
        final PayoutList payoutList = CacheManager.getValue(PayoutRestAPI.PAYOUTS,
                new PayoutKey(uid, interaction, limit, offset), this.servletRequest,
                Permission.READ_PAYOUTS_PERMISSION);
        if (payoutList == null) {
            throw new ForbiddenException();
        }
        return CacheManager.managePrivateCaching(request, payoutList, "payouts.csv");
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPayout(@PathParam("user") final String user,
            @FormParam("item") final String item, @FormParam("amount") final String amount,
            @FormParam("type") final String type, @Context final Request request) {
        if (!Permission.WRITE_PAYOUTS_PERMISSION
                .isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        final HelperResult<Payout> result = PayoutHelper.createPayout(user, item, amount, type);
        return Response.status(result.getStatus()).entity(result).build();
    }

    @POST
    @Path("{pid}/cancel")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelPayout(@PathParam("user") final String user,
            @PathParam("pid") final String pid, @Context final Request request) {
        if (!Permission.WRITE_PAYOUTS_PERMISSION
                .isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        final HelperResult<Payout> result = PayoutHelper.cancelPayout(user, pid);
        return Response.status(result.getStatus()).entity(result).build();
    }

    @POST
    @Path("{pid}/takeout")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response takeoutPayout(@PathParam("user") final String user,
            @PathParam("pid") final String pid, @Context final Request request) {
        if (!Permission.WRITE_PAYOUTS_PERMISSION
                .isPresentIn((int) this.servletRequest.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        final HelperResult<Payout> result = PayoutHelper.takeoutPayout(user, pid);
        return Response.status(result.getStatus()).entity(result).build();
    }

    public static void invalidateAll() {
        PayoutRestAPI.PAYOUTS.invalidateAll();
    }

    public static void invalidate(final long id, final boolean selfCreated, final Payout p) {
        PayoutRestAPI.PAYOUTS.invalidateAll(
                Sets.filter(PayoutRestAPI.PAYOUTS.asMap().keySet(), k -> id == k.getUid()));
        final NotificationObservable notificationObservable = Main.instance()
                .getNotificationObservable();
        if (notificationObservable != null) {
            final NotificationEvent e = new NotificationEvent(selfCreated, id, "update", "payouts",
                    Permission.READ_PAYOUTS_PERMISSION, new Payout[] { p });
            notificationObservable.notifyObservers(e);
        }
    }

    public static void invalidate(final long id, final boolean selfCreated,
            final Supplier<Payout[]> payoutProducer) {
        PayoutRestAPI.PAYOUTS.invalidateAll(
                Sets.filter(PayoutRestAPI.PAYOUTS.asMap().keySet(), k -> id == k.getUid()));
        final NotificationObservable notificationObservable = Main.instance()
                .getNotificationObservable();
        if (notificationObservable != null) {
            final NotificationEvent e = new NotificationEvent(selfCreated, id, "update", "payouts",
                    Permission.READ_PAYOUTS_PERMISSION, payoutProducer);
            notificationObservable.notifyObservers(e);
        }
    }
}
