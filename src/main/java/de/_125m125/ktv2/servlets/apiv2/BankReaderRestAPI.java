package de._125m125.ktv2.servlets.apiv2;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de._125m125.ktv2.reader.BankReader;
import de._125m125.ktv2.servlets.BankReaderServlet;
import de._125m125.ktv2.servlets.api.helper.HelperResult;

@Path("/bank")
public class BankReaderRestAPI {

    @POST
    @Path("/read")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readBank() {
        if (BankReaderServlet.br.read()) {
            return Response.status(202).entity(new HelperResult<>(true, 202, BankReader.DELTA))
                    .build();
        } else {
            int remainingTime = BankReaderServlet.br.getRemainingTime();
            return Response.status(503)
                    .entity(new HelperResult<>(false, 503, "bankCooldown", remainingTime))
                    .header("Retry-After", remainingTime).build();
        }
    }
}
