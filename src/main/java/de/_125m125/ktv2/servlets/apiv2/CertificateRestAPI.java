package de._125m125.ktv2.servlets.apiv2;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.bouncycastle.operator.OperatorCreationException;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.ClientCertificate;
import de._125m125.ktv2.beans.ClientCertificate.Type;
import de._125m125.ktv2.beans.GeneratedCertificate;
import de._125m125.ktv2.beans.TsvableList;
import de._125m125.ktv2.calculators.CertificateManager;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("users/{user}/certificates")
public class CertificateRestAPI {

    @POST
    @Path("/2fa")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create2FACertificate(@PathParam("user") final String user, @FormParam("csr") final String csr,
            @FormParam("clientCertValidTime") @DefaultValue("30") final int daysValid,
            @Context final HttpServletRequest request, @Context final UriInfo uriInfo) {
        return createCertificate(csr, daysValid, true, request, uriInfo, false);
    }

    @POST
    @Path("/api")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createApiCertificate(@PathParam("user") final String user, @FormParam("csr") final String csr,
            @FormParam("certValidTime") @DefaultValue("30") final int daysValid,
            @Context final HttpServletRequest request, @Context final UriInfo uriInfo) {
        return createCertificate(csr, daysValid, false, request, uriInfo, false);
    }

    @POST
    @Path("/2fa")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({ MediaType.TEXT_PLAIN, "application/x-x509-user-cert", "application/x-pem-file" })
    public Response createPlain2FACertificate(@PathParam("user") final String user, @FormParam("csr") final String csr,
            @FormParam("clientCertValidTime") @DefaultValue("30") final int daysValid,
            @Context final HttpServletRequest request, @Context final UriInfo uriInfo) {
        return createCertificate(csr, daysValid, true, request, uriInfo, true);
    }

    @POST
    @Path("/api")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({ MediaType.TEXT_PLAIN, "application/x-x509-user-cert", "application/x-pem-file" })
    public Response createPlainApiCertificate(@PathParam("user") final String user, @FormParam("csr") final String csr,
            @FormParam("certValidTime") @DefaultValue("30") final int daysValid,
            @Context final HttpServletRequest request, @Context final UriInfo uriInfo) {
        return createCertificate(csr, daysValid, false, request, uriInfo, true);
    }

    private Response createCertificate(final String csr, final int daysValid, final boolean type,
            final HttpServletRequest request, final UriInfo uriInfo, final boolean plain) {
        if (!Permission.ALL_NONADMIN_PERMISSIONS.isPresentIn((int) request.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        try {
            final GeneratedCertificate certificate = CertificateManager.instance()
                    .generateCertificate((long) request.getAttribute("id"), csr, daysValid, type);
            return Response.created(
                    uriInfo.getAbsolutePathBuilder().path(certificate.getDetails().getSerialNo().toString()).build())
                    .entity(plain ? certificate.getCertificate() : certificate).build();
        } catch (final IllegalArgumentException e) {
            throw new BadRequestException(e.getMessage());
        } catch (OperatorCreationException | NoSuchAlgorithmException | CertificateException | IOException
                | SQLException e) {
            e.printStackTrace();
            throw new InternalServerErrorException();
        }
    }

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getAllCertificates(@PathParam("user") final String user, @QueryParam("type") final Type type,
            @Context final HttpServletRequest request) {
        if (!Permission.ALL_NONADMIN_PERMISSIONS.isPresentIn((int) request.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        String query = "SELECT serialNr, type FROM certificates WHERE user=?";
        if (type != null) {
            query += " AND type=?";
        } else {
            query += " AND type>0";
        }
        final TsvableList<ClientCertificate> l = new TsvableList<ClientCertificate>(ClientCertificate.HEADER) {
            @Override
            protected ClientCertificate[] createArray(final int size) {
                return new ClientCertificate[size];
            }
        };
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(query)) {
            ps.setLong(1, (long) request.getAttribute("id"));
            if (type != null) {
                ps.setInt(2, type.getMysqlId());
            }
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    l.addEntry(new ClientCertificate(new BigInteger(rs.getBytes("serialNr")), rs.getInt("type")));
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new InternalServerErrorException();
        }
        return Response.ok(l).build();
    }

    @GET
    @Path("/2fa")
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response get2faCertificates(@PathParam("user") final String user,
            @Context final HttpServletRequest request) {
        return getAllCertificates(user, Type.TWO_FACTOR_AUTH, request);
    }

    @GET
    @Path("/api")
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getApiCertificates(@PathParam("user") final String user,
            @Context final HttpServletRequest request) {
        return getAllCertificates(user, Type.API, request);
    }

    @DELETE
    @Path("/{serialNr}")
    public Response revokeCertificate(@PathParam("user") final String user,
            @PathParam("serialNr") final BigInteger serialNr, @Context final HttpServletRequest request) {
        if (!Permission.ALL_NONADMIN_PERMISSIONS.isPresentIn((int) request.getAttribute("permissions"))) {
            throw new ForbiddenException();
        }
        Main.instance().getConnector().executeAtomic(c -> {
            try (PreparedStatement ps = c
                    .prepareStatement("UPDATE certificates SET type=0 WHERE user=? AND type!=0 AND serialNr=?")) {
                ps.setLong(1, (long) request.getAttribute("id"));
                ps.setBytes(2, serialNr.toByteArray());
                final int executeUpdate = ps.executeUpdate();
                if (executeUpdate != 1) {
                    throw new NotFoundException();
                }
                Log.USER.executeLog(c, Level.INFO, "User revoked certificate " + serialNr,
                        (long) request.getAttribute("id"));
            } catch (final SQLException e) {
                e.printStackTrace();
                throw new InternalServerErrorException();
            }
            try {
                CertificateManager.instance().revoke(serialNr);
            } catch (CertificateEncodingException | OperatorCreationException | IOException e) {
                e.printStackTrace();
                return () -> false;
            }
            return () -> true;
        });
        return Response.noContent().build();
    }
}
