package de._125m125.ktv2.servlets.apiv2;

import java.util.function.Predicate;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.OrderBook;
import de._125m125.ktv2.cache.LoadingPartialCache;
import de._125m125.ktv2.cache.stats.OrderBookBuilder;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.api.keys.OrderBookKey;
import de._125m125.ktv2.servlets.api.keys.OrderBookKey.MODE;
import de._125m125.ktv2.servlets.apiv2.helper.CacheManager;

@Path("orderbook/{itemid}")
public class OrderBookRestAPI {
    private static final LoadingCache<OrderBookKey, OrderBook> ORDERS;

    static {
        final CacheLoader<OrderBookKey, OrderBook> cacheLoader = new OrderBookBuilder();
        final Predicate<? super OrderBookKey> predicate = key -> key.getLimit() == 5 || key.getLimit() == 1;
        final LoadingCache<OrderBookKey, OrderBook> cache = CacheBuilder.newBuilder().maximumSize(50)
                .build(cacheLoader);
        ORDERS = new LoadingPartialCache<>(cache, cacheLoader, predicate);
    }

    @Context
    private HttpServletRequest servletRequest;

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    public Response getOrderBook(@PathParam("itemid") final String itemid, @QueryParam("limit") final Integer limitQ,
            @QueryParam("summarize") final Boolean summarizeQ,
            @QueryParam("mode") @DefaultValue("BOTH") final MODE modeQ, @Context final Request request) {
        if (!Variables.resourceExists(itemid)) {
            throw new NotFoundException();
        }
        final int limit = limitQ != null ? Math.max(0, limitQ) : 5;
        final boolean summarize = !Boolean.FALSE.equals(summarizeQ);
        final MODE mode = modeQ != null ? modeQ : MODE.BOTH;
        final OrderBookKey orderKey = new OrderBookKey(itemid, limit, summarize, mode);
        return getResponseForOrderBookKey(request, orderKey);
    }

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    @Path("best")
    public Response getBestOrderBook(@PathParam("itemid") final String itemid, @Context final Request request) {
        if (!Variables.resourceExists(itemid)) {
            throw new NotFoundException();
        }
        final OrderBookKey orderKey = new OrderBookKey(itemid, 1, false, MODE.BOTH);
        return getResponseForOrderBookKey(request, orderKey);
    }

    @GET
    @Produces({ "text/tsv;charset=UTF-8", MediaType.APPLICATION_JSON })
    @Path("best/{mode}")
    public Response getBestOrderBookByType(@PathParam("itemid") final String itemid, @PathParam("mode") final MODE mode,
            @Context final Request request) {
        if (!Variables.resourceExists(itemid)) {
            throw new NotFoundException();
        }
        final OrderBookKey orderKey = new OrderBookKey(itemid, 1, false, mode);
        return getResponseForOrderBookKey(request, orderKey);
    }

    private Response getResponseForOrderBookKey(final Request request, final OrderBookKey orderKey) {
        final OrderBook orderBook = CacheManager.getValue(OrderBookRestAPI.ORDERS, orderKey, this.servletRequest,
                Permission.NO_PERMISSIONS);
        if (orderBook == null) {
            throw new ForbiddenException();
        }
        return CacheManager.managePublicCaching(request, orderBook, "orderbook.csv");
    }

    public static void invalidateAll() {
        OrderBookRestAPI.ORDERS.invalidateAll();
    }

    public static void invalidate(final String invalidationKey) {
        OrderBookRestAPI.ORDERS.asMap().keySet().stream().filter((key) -> key.getMaterial().equals(invalidationKey))
                .forEach((key) -> OrderBookRestAPI.ORDERS.invalidate(key));

        final NotificationObservable notificationObservable = Main.instance().getNotificationObservable();
        if (notificationObservable != null) {
            final NotificationEvent ne = new NotificationEvent(false, 0, "update", "orderbook", invalidationKey,
                    Permission.NO_PERMISSIONS);
            notificationObservable.notifyObservers(ne);
        }
    }
}
