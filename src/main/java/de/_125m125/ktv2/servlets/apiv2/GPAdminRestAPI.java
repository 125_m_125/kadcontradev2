package de._125m125.ktv2.servlets.apiv2;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.google.gson.Gson;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.beans.OwnedItem;
import de._125m125.ktv2.beans.OwnedItemMovement;
import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.servlets.api.helper.Permission;

@Path("admins/{admin}")
public class GPAdminRestAPI {

    private static final Gson                      GSON           = new Gson();
    private static final Function<Tsvable, String> TSV_CONVERTER  = i -> i.getTsvString(false);
    private static final Function<Tsvable, String> JSON_CONVERTER = GPAdminRestAPI.GSON::toJson;

    @GET
    @Path("items")
    @Produces("text/tsv;charset=UTF-8")
    public Response getCompleteItemListTsv(@PathParam("admin") final String requestedAdminId,
            @Context final HttpServletRequest httpRequest, @Context final Request jaxrsRequest) {
        return getCompleteItemList(requestedAdminId, httpRequest, OwnedItem.TSV_HEADER + "\r\n",
                GPAdminRestAPI.TSV_CONVERTER, "", "");
    }

    @GET
    @Path("items")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCompleteItemListJson(@PathParam("admin") final String requestedAdminId,
            @Context final HttpServletRequest httpRequest) {
        return getCompleteItemList(requestedAdminId, httpRequest, "[",
                GPAdminRestAPI.JSON_CONVERTER, ",", "]");
    }

    public Response getCompleteItemList(final String requestedAdminId,
            final HttpServletRequest httpRequest, final String start,
            final Function<? super OwnedItem, String> converter, final String separator,
            final String end) {
        final SqlThrowingFunction<ResultSet, OwnedItem> itemconvert =
                rs -> new OwnedItem(rs.getLong("id"), rs.getString("matid"),
                        "-1".equals(rs.getString("matid"))
                                ? IDSConverter.longToDoubleString(rs.getLong("amount"))
                                : rs.getString("amount"));
        return getCompleteList("SELECT * FROM materials", itemconvert.andThen(converter),
                requestedAdminId, httpRequest, start, separator, end,
                Permission.READ_ADMIN_ITEMLIST_PERMISSION);
    }

    @GET
    @Path("itemmovements")
    @Produces("text/tsv;charset=UTF-8")
    public Response getCompleteItemMovementsTsv(@PathParam("admin") final String requestedAdminId,
            @Context final HttpServletRequest httpRequest) {
        return getCompleteItemMovements(requestedAdminId, httpRequest,
                OwnedItemMovement.TSV_HEADER + "\r\n", GPAdminRestAPI.TSV_CONVERTER, "", "");
    }

    @GET
    @Path("itemmovements")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCompleteItemMovementsJson(@PathParam("admin") final String requestedAdminId,
            @Context final HttpServletRequest httpRequest) {
        return getCompleteItemMovements(requestedAdminId, httpRequest, "[",
                GPAdminRestAPI.JSON_CONVERTER, ",", "]");
    }

    public Response getCompleteItemMovements(final String requestedAdminId,
            final HttpServletRequest httpRequest, final String start,
            final Function<? super OwnedItemMovement, String> converter, final String separator,
            final String end) {
        final SqlThrowingFunction<ResultSet, OwnedItemMovement> itemconvert =
                rs -> new OwnedItemMovement(rs.getTimestamp("time").getTime(), rs.getLong("uid"),
                        rs.getString("matid"), rs.getLong("amount"), rs.getString("reason"));
        return getCompleteList("SELECT * FROM materialMovements", itemconvert.andThen(converter),
                requestedAdminId, httpRequest, start, separator, end,
                Permission.READ_ADMIN_ITEMMOVEMENTS_PERMISSION);
    }

    public Response getCompleteList(final String query,
            final SqlThrowingFunction<ResultSet, String> rowConverter,
            final String requestedAdminId, final HttpServletRequest httpRequest, final String start,
            final String separator, final String end, final Permission p) {
        final long adminId = (long) httpRequest.getAttribute("id");
        if (!p.isPresentIn((int) httpRequest.getAttribute("permissions"))
                || !UserHelper.isAdmin(adminId)) {
            throw new ForbiddenException();
        }
        final StreamingOutput out = output -> {
            try (Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps =
                            c.prepareStatement(query, java.sql.ResultSet.TYPE_FORWARD_ONLY,
                                    java.sql.ResultSet.CONCUR_READ_ONLY)) {
                ps.setFetchSize(Integer.MIN_VALUE);
                try (final ResultSet rs = ps.executeQuery();
                        Writer writer = new BufferedWriter(new OutputStreamWriter(output))) {
                    boolean first = true;
                    writer.write(start);
                    while (rs.next()) {
                        if (!first) {
                            writer.write(separator);
                        } else {
                            first = false;
                        }
                        writer.write(rowConverter.apply(rs));
                    }
                    writer.write(end);
                    writer.flush();
                }
            } catch (final SQLException e) {
                throw new InternalServerErrorException("unknownError");
            }
        };
        return Response.ok(out).build();
    }

    @FunctionalInterface
    private static interface SqlThrowingFunction<T, R> {
        R apply(T t) throws SQLException;

        default <V> SqlThrowingFunction<T, V> andThen(
                final Function<? super R, ? extends V> after) {
            Objects.requireNonNull(after);
            return (final T t) -> after.apply(apply(t));
        }
    }
}
