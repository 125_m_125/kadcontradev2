package de._125m125.ktv2.servlets.apiv2;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api/v2.0/")
public class ApplicationConfig extends Application {

}
