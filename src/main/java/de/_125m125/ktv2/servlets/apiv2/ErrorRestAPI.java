package de._125m125.ktv2.servlets.apiv2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.ErrorReport;
import de._125m125.ktv2.beans.Token;
import jersey.repackaged.com.google.common.base.Objects;

@Path("users/{user}/errors")
public class ErrorRestAPI {

    @Context
    private HttpServletRequest servletRequest;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addError(@PathParam("user") final String userid, final ErrorReport errorReport,
            @Context final UriInfo uriInfo) {
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(
                        "INSERT INTO errors(reporter,message,source,lineno,colno,error) VALUES(?,?,?,?,?,?)",
                        Statement.RETURN_GENERATED_KEYS)) {
            ps.setLong(1, Token.getBigInt(userid).longValue());
            ps.setString(2, errorReport.getMessage() == null ? null : errorReport.getMessage().replaceAll("\\s+", " "));
            ps.setString(3, errorReport.getSource() == null ? null : errorReport.getSource().replaceAll("\\s+", " "));
            ps.setInt(4, Objects.firstNonNull(errorReport.getLineno(), 0));
            ps.setInt(5, Objects.firstNonNull(errorReport.getColno(), 0));
            ps.setString(6, errorReport.getError() == null ? null : errorReport.getError().replaceAll("\\s+", " "));
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    return Response
                            .created(uriInfo.getAbsolutePathBuilder().path(Integer.toString(rs.getInt(1))).build())
                            .build();
                }
            }
        } catch (final SQLDataException | SQLIntegrityConstraintViolationException e) {
            throw new WebApplicationException("Invalid data", 422);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        throw new InternalServerErrorException();
    }
}
