package de._125m125.ktv2.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.io.ByteStreams;

@WebServlet(urlPatterns = { "/static/json/imageVersions.json" })
public class ItemImageVersionServlet extends HttpServlet {

    private static final File FILE = new File("/home/kadcontrade/imageVersions.json");

    @Override
    public long getLastModified(final HttpServletRequest request) {
        if (ItemImageVersionServlet.FILE.exists()) { return ItemImageVersionServlet.FILE.lastModified(); }
        return 0L;
    }

    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setHeader("Cache-Control", "public, max-age: 86400");

        if (ItemImageVersionServlet.FILE.exists()) {
            final OutputStream os = response.getOutputStream();
            try (final InputStream is = new FileInputStream(ItemImageVersionServlet.FILE)) {
                ByteStreams.copy(is, os);
            }
        } else {
            response.getWriter().write("{}");
        }
    }
}
