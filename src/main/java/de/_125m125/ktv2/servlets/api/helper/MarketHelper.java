package de._125m125.ktv2.servlets.api.helper;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.beans.UpdatedTradeSlot;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.market.Trade.buySell;
import de._125m125.ktv2.market.TradeResult;
import de._125m125.ktv2.servlets.helper.ServletHelper;

public class MarketHelper {
    public static void processPost(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (request.getParameter("create") != null) {
            createTrade(request, response);
        } else if (request.getParameter("cancel") != null) {
            cancelTrade(request, response);
        }
    }

    private static void cancelTrade(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "tradeid")) {
            ApiResultHelper.setStatus(request, response, false,
                    Variables.translate("missingAttributes"));
            return;
        }
        final Future<TradeResult> result =
                Main.instance().getTradeManager().cancel(request.getParameter("tradeid"),
                        (long) request.getAttribute("id"), ITEM_OR_STOCK.ITEM);
        TradeResult rs;
        try {
            rs = result.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            ApiResultHelper.setStatus(request, response, false,
                    Variables.translate("errorUnknown"));
            return;
        }
        if (rs.isSuccess()) {
            ApiResultHelper.setStatus(request, response, true, rs.getResultString(),
                    rs.getTradeResult());
        } else {
            ApiResultHelper.setStatus(request, response, false, rs.getResultString());
        }
    }

    private static void createTrade(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "bs", "item", "count", "price")) {
            ApiResultHelper.setStatus(request, response, false,
                    Variables.translate("missingAttributes"));
            return;
        }
        buySell bs;
        switch (request.getParameter("bs")) {
        case "buy":
            bs = buySell.BUY;
            break;
        case "sell":
            bs = buySell.SELL;
            break;
        default:
            ApiResultHelper.setStatus(request, response, false, "selectBuyOrSell");
            return;
        }
        final Future<TradeResult> result =
                Main.instance().getTradeManager().create(request.getParameter("count"), bs,
                        request.getParameter("item"), (long) request.getAttribute("id"),
                        request.getParameter("price"), ITEM_OR_STOCK.ITEM);
        TradeResult rs;
        try {
            rs = result.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            ApiResultHelper.setStatus(request, response, false,
                    Variables.translate("errorUnknown"));
            return;
        }
        if (rs.isSuccess()) {
            ApiResultHelper.setStatus(request, response, rs);
        } else {
            ApiResultHelper.setStatus(request, response, rs);
        }
    }

    public static HelperResult<UpdatedTradeSlot> createTrade(final long uid,
            @FormParam("buySell") final buySell buySell, @FormParam("item") final String item,
            @FormParam("amount") final int count, @FormParam("price") final long price) {
        final Future<TradeResult> result = Main.instance().getTradeManager().create(count, buySell,
                item, uid, price, ITEM_OR_STOCK.ITEM, true);
        try {
            return result.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new HelperResult<>(false, 500, "errorUnknown");
        }
    }

    public static HelperResult<UpdatedTradeSlot> cancelTrade(final String user,
            final long orderId) {
        final long uid = Token.getBigInt(user).longValue();
        final Future<TradeResult> result =
                Main.instance().getTradeManager().cancel(orderId, uid, ITEM_OR_STOCK.ITEM);
        try {
            return result.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new HelperResult<>(false, 500, "errorUnknown");
        }
    }

}
