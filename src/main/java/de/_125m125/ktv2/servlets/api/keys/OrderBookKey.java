package de._125m125.ktv2.servlets.api.keys;

public class OrderBookKey {
    public static enum MODE {
        BUY_ONLY,
        SELL_ONLY,
        BOTH,;

        public static MODE fromString(final String string) {
            switch (string.toUpperCase()) {
            case "BUY_ONLY":
            case "BUY":
                return MODE.BUY_ONLY;
            case "SELL_ONLY":
            case "SELL":
                return MODE.SELL_ONLY;
            default:
                return BOTH;
            }
        }
    }

    private final String  material;
    private final int     limit;
    private final boolean summarize;
    private final MODE    mode;

    public OrderBookKey(final String material, final int limit, final boolean summarize) {
        this(material, limit, summarize, MODE.BOTH);
    }

    public OrderBookKey(final String material, final int limit, final boolean summarize, final MODE mode) {
        this.material = material;
        this.limit = limit;
        this.summarize = summarize;
        this.mode = mode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.limit;
        result = prime * result + ((this.material == null) ? 0 : this.material.hashCode());
        result = prime * result + ((this.mode == null) ? 0 : this.mode.hashCode());
        result = prime * result + (this.summarize ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderBookKey other = (OrderBookKey) obj;
        if (this.limit != other.limit) {
            return false;
        }
        if (this.material == null) {
            if (other.material != null) {
                return false;
            }
        } else if (!this.material.equals(other.material)) {
            return false;
        }
        if (this.mode != other.mode) {
            return false;
        }
        if (this.summarize != other.summarize) {
            return false;
        }
        return true;
    }

    public String getMaterial() {
        return this.material;
    }

    public int getLimit() {
        return this.limit;
    }

    public boolean isSummarize() {
        return this.summarize;
    }

    public MODE getMode() {
        return this.mode;
    }
}
