package de._125m125.ktv2.servlets.api.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;

import javax.json.stream.JsonParsingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.Payout;
import de._125m125.ktv2.beans.Payout.PAYOUT_STATE;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.cache.payout.PayoutBuilder;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.apiv2.ItemRestAPI;
import de._125m125.ktv2.servlets.apiv2.PayoutRestAPI;
import de._125m125.ktv2.servlets.helper.ServletHelper;

public class PayoutHelper {

    private static PayoutBuilder payoutBuilder = new PayoutBuilder();

    public static void processPost(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (request.getParameter("payout") != null) {
            createPayout(request, response);
        } else if (request.getParameter("cancel") != null) {
            cancelPayout(request, response);
        } else if (request.getParameter("takeout") != null) {
            takeoutPayout(request, response);
        } else {
            ApiResultHelper.setStatus(request, response, false, "unknownAction");
        }
    }

    private static void takeoutPayout(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "id")) {
            ApiResultHelper.setStatus(request, response, false, "missingAttributes");
            return;
        }
        final long uid = (long) request.getAttribute("id");
        ApiResultHelper.setStatus(request, response,
                takeoutPayout("D" + uid, request.getParameter("id")));
    }

    @SuppressWarnings("unchecked")
    public static HelperResult<Payout> takeoutPayout(final String user, final String pid) {
        final long longUid = Token.getBigInt(user).longValue();
        final String uid = String.valueOf(longUid);
        final long payoutid = Long.parseLong(pid);

        final Successindicator atomicResult = Main.instance().getConnector().executeAtomic((c) -> {
            try (PreparedStatement payoutUpdate = c.prepareStatement(
                    "UPDATE payout SET userInteraction=FALSE WHERE userInteraction=TRUE AND requiresAdminInteraction=FALSE AND SUCCESS=FALSE AND id=? AND uid=?")) {
                payoutUpdate.setLong(1, payoutid);
                payoutUpdate.setLong(2, longUid);
                final int updatedRows = payoutUpdate.executeUpdate();
                try (PreparedStatement select =
                        c.prepareStatement("SELECT * FROM payout WHERE id=?")) {
                    select.setLong(1, payoutid);
                    final ResultSet payoutResultSet = select.executeQuery();
                    if (payoutResultSet.next()) {
                        final Payout payout =
                                PayoutBuilder.createPayoutFromCurrentRow(payoutResultSet);
                        if (updatedRows != 1) {
                            if (payout.getUid() != longUid) {
                                return new HelperResult<>(false, 403, "notYourPayout");
                            } else {
                                return new HelperResult<>(false, 422, "invalidPayoutState");
                            }
                        }
                        MySQLHelper.addResource(c, longUid, payout.isItem(), payout.getMaterial(),
                                payout.getAmount(), "payoutTakenOut", false);
                        Log.PAYOUT.executeLog(c, Level.INFO, uid + " took items from payout " + pid,
                                longUid);
                        return new HelperResult<>(true, 200, "takeoutSuccess", payout);
                    } else {
                        return new HelperResult<>(false, 404, "payoutNotExisting");
                    }
                }
            }
        });
        if (atomicResult == null || !(atomicResult instanceof HelperResult<?>)) {
            return new HelperResult<>(false, 500, "unknownError");
        }
        if (atomicResult.wasSuccessful()) {
            final Payout data = ((HelperResult<Payout>) atomicResult).getData();
            PayoutRestAPI.invalidate(longUid, true, data);
            invalidateItem(uid, data);
        }
        try {
            return (HelperResult<Payout>) atomicResult;
        } catch (final ClassCastException e) {
            return new HelperResult<>(false, 500, "unknownError");
        }
    }

    private static void invalidateItem(final String uid, final Payout data) {
        ItemRestAPI.invalidate(uid, () -> {
            final long userId = data.getUid();
            final String material = data.getMaterial();
            return Item.getItems(userId, material);
        }, true);
    }

    private static void cancelPayout(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "id")) {
            ApiResultHelper.setStatus(request, response, false, "missingAttributes");
            return;
        }
        final long uid = (long) request.getAttribute("id");
        ApiResultHelper.setStatus(request, response,
                cancelPayout("D" + uid, request.getParameter("id")));
    }

    @SuppressWarnings("unchecked")
    public static HelperResult<Payout> cancelPayout(final String user, final String pid) {
        final long longUid = Token.getBigInt(user).longValue();
        final String uid = String.valueOf(longUid);
        final long payoutid = Long.parseLong(pid);

        final Successindicator atomicResult = Main.instance().getConnector().executeAtomic((c) -> {
            try (PreparedStatement payoutUpdate = c.prepareStatement(
                    "UPDATE payout SET userInteraction=FALSE, success=TRUE, requiresAdminInteraction=FALSE WHERE userInteraction=TRUE AND requiresAdminInteraction=TRUE AND id=? AND uid=?")) {
                payoutUpdate.setLong(1, payoutid);
                payoutUpdate.setLong(2, longUid);
                final int updatedRows = payoutUpdate.executeUpdate();
                try (PreparedStatement select =
                        c.prepareStatement("SELECT * FROM payout WHERE id=?")) {
                    select.setLong(1, payoutid);
                    final ResultSet payoutResultSet = select.executeQuery();
                    if (payoutResultSet.next()) {
                        final Payout payout =
                                PayoutBuilder.createPayoutFromCurrentRow(payoutResultSet);
                        if (updatedRows != 1) {
                            if (payout.getUid() != longUid) {
                                return new HelperResult<>(false, 403, "notYourPayout");
                            } else {
                                return new HelperResult<>(false, 422, "invalidPayoutState");
                            }
                        }
                        MySQLHelper.addResource(c, longUid, payout.isItem(), payout.getMaterial(),
                                payout.getAmount(), "payoutCancelled", false);
                        Log.PAYOUT.executeLog(c, Level.INFO, uid + " cancelled payout " + pid,
                                longUid);
                        return new HelperResult<>(true, 200, "payoutCancelSuccess", payout);
                    } else {
                        return new HelperResult<>(false, 404, "payoutNotExisting");
                    }
                }
            }
        });
        if (atomicResult == null || !(atomicResult instanceof HelperResult<?>)) {
            return new HelperResult<>(false, 500, "unknownError");
        }
        if (atomicResult.wasSuccessful()) {
            final Payout data = ((HelperResult<Payout>) atomicResult).getData();
            PayoutRestAPI.invalidate(longUid, true, data);
            invalidateItem(uid, data);
        }
        try {
            return (HelperResult<Payout>) atomicResult;
        } catch (final ClassCastException e) {
            return new HelperResult<>(false, 500, "unknownError");
        }
    }

    private static void createPayout(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (!ServletHelper.checkParams(request, "item", "amount", "type")) {
            ApiResultHelper.setStatus(request, response, false, "missingAttributes");
            return;
        }
        final String payoutType = request.getParameter("type");
        final String item = request.getParameter("item");
        final String inAmount = request.getParameter("amount");
        final long longUid = (long) request.getAttribute("id");
        final HelperResult<Payout> result = createPayout("D" + longUid, item, inAmount, payoutType);
        ApiResultHelper.setStatus(request, response, result);
    }

    public static HelperResult<Payout> createPayout(final String string, final String item,
            final String inAmount, final String payoutType) {
        return createPayout(string, item, inAmount, payoutType, null, "", "");
    }

    public static HelperResult<Payout> createPayout(final String user, final String item,
            final String inAmount, String payoutType, final Long forcer, final String forceMessage,
            final String requestedState) {
        final long longUid = Token.getBigInt(user).longValue();
        final String uid = String.valueOf(longUid);
        final boolean money = "-1".equals(item);
        if (!Variables.resourceExistsOrIsMoney(item)) {
            return new HelperResult<>(false, 422, "unknownMaterial");
        }
        int stacks = -1;
        if (money) {
            if (payoutType == null || payoutType.isEmpty()) {
                payoutType = "kadcon";
            } else if (!("kadcon".equals(payoutType) /* || "vkba".equals(payoutType) */)) {
                return new HelperResult<>(false, 422, "invalidType");
            }
        } else {
            if (payoutType == null || payoutType.isEmpty()) {
                final ArrayList<HashMap<String, Object>> poboxes =
                        Main.instance().getConnector().executeQuery(
                                "SELECT server FROM payoutboxes WHERE owner=? AND verified=TRUE ORDER BY server DESC LIMIT 1",
                                uid);
                if (poboxes != null && poboxes.size() > 0) {
                    final long target = (long) poboxes.get(0).get("server");
                    payoutType = "bs" + target;
                } else {
                    payoutType = "protect";
                }
            } else if (payoutType.length() == 3 && payoutType.startsWith("bs")) {
                final String server = payoutType.substring(2, 3);
                final ArrayList<HashMap<String, Object>> hasPayoutbox =
                        Main.instance().getConnector().executeQuery(
                                "SELECT 1 FROM payoutboxes WHERE owner=? AND server=? AND verified=TRUE",
                                uid, server);
                if (hasPayoutbox == null || hasPayoutbox.size() == 0) {
                    return new HelperResult<>(false, 422, "noBoxOnThisServer");
                }
            } else if ("lieferung".equals(payoutType)) {
                final ArrayList<HashMap<String, Object>> hasPayoutSettings =
                        Main.instance().getConnector()
                                .executeQuery("SELECT 1 FROM payoutsettings WHERE uid=?", uid);
                try (Connection c = Main.instance().getConnector().getConnection();
                        PreparedStatement ps = c.prepareStatement(
                                "SELECT SUM(CEIL(amount/64)) as stacks FROM payout WHERE uid=? AND matid!=\"-1\" AND requiresAdminInteraction=true")) {
                    ps.setLong(1, longUid);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            stacks = rs.getInt("stacks");
                        }
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                    return new HelperResult<>(false, 500, "unknownError");
                }
                if (hasPayoutSettings == null || hasPayoutSettings.size() == 0) {
                    return new HelperResult<>(false, 422, "noSettingsSubmitted");
                }
            } else {
                return new HelperResult<>(false, 422, "invalidType");
            }
        }
        long integerAmount;
        String amount;
        try {
            if (money) {
                integerAmount = IDSConverter.stringToLong(inAmount, 2);
                amount = String.valueOf(integerAmount);
            } else {
                integerAmount = Long.parseLong(inAmount);
                amount = inAmount;
            }
            final ArrayList<HashMap<String, Object>> result =
                    Main.instance().getConnector().executeQuery(
                            "SELECT 1 FROM materials WHERE id=? AND type=1 AND matid=? AND amount>=?",
                            uid, item, amount);
            if (result == null || result.size() != 1 || integerAmount <= 0) {
                return new HelperResult<>(false, 422, "notenoughmaterials");
            }
        } catch (final NumberFormatException e) {
            return new HelperResult<>(false, 422, "invalidAmount");
        }
        if (stacks >= 0) {
            final long usedStacks = (integerAmount + 63) / 64;
            if (usedStacks + stacks > 6) {
                return new HelperResult<>(false, 409, "tooManyPayoutStacks");
            }
        }
        final PAYOUT_STATE state;
        if (money && "vkba".equals(payoutType)) {
            state = tryVkbaPayout(longUid, inAmount);
        } else {
            if (requestedState == null || requestedState.isEmpty() || forcer == null
                    || requestedState.equalsIgnoreCase("open")) {
                state = forcer == null ? PAYOUT_STATE.OPEN : PAYOUT_STATE.FORCED;
            } else {
                try {
                    state = PAYOUT_STATE.valueOf(requestedState.toUpperCase());
                } catch (final IllegalArgumentException e) {
                    return new HelperResult<>(false, 422, "illegalState");
                }
                switch (state) {
                case IN_PROGRESS:
                    // make sure admin does not have other open payouts
                    final ArrayList<HashMap<String, Object>> executeQuery =
                            Main.instance().getConnector().executeQuery(
                                    "SELECT * FROM payout WHERE adminInteractionCompleted=FALSE AND requiresAdminInteraction=FALSE AND agent=?",
                                    String.valueOf(forcer));
                    if (executeQuery != null && executeQuery.size() > 0) {
                        return new HelperResult<>(false, 409, "payoutsAlreadyAssigned");
                    }
                    break;
                case SUCCESS:
                    break;
                default:
                    return new HelperResult<>(false, 422, "illegalState");

                }
            }
        }
        final Long handler = state == PAYOUT_STATE.IN_PROGRESS ? forcer : null;
        final long id = ThreadLocalRandom.current().nextLong(4294967295L);
        final String finalPayoutType = payoutType;
        final Successindicator success = Main.instance().getConnector().executeAtomic((c) -> {
            MySQLHelper.addResource(c, longUid, true, item, -integerAmount,
                    forcer != null ? "forced payout created" : "payout created", false);
            if (forcer != null) {
                try (PreparedStatement ps =
                        c.prepareStatement("INSERT INTO messages(uid, message) VALUES(?,?)")) {
                    ps.setString(1, uid);
                    ps.setString(2, Variables.translate("forcePayoutMessage", amount,
                            Variables.ID_TO_DN.get(item), forceMessage));
                }
                Log.PAYOUT.executeLog(c, Level.INFO, forcer + " forced payout of " + amount + "*"
                        + item + " of " + uid + ": " + forceMessage, forcer, longUid);
            } else {
                Log.PAYOUT.executeLog(c, Level.INFO,
                        uid + " created new payout " + amount + "*" + item, longUid);
            }
            try (PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO payout(id, uid, type, matid, amount, userInteraction, success, adminInteractionCompleted, requiresAdminInteraction, unknown, payoutType, message, forcer,agent) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)")) {
                ps.setLong(1, id);
                ps.setLong(2, longUid);
                ps.setBoolean(3, true);
                ps.setString(4, item);
                ps.setLong(5, integerAmount);
                ps.setBoolean(6, state.isUserInteraction());
                ps.setBoolean(7, state.isSuccess());
                ps.setBoolean(8, state.isAdminInteractionCompleted());
                ps.setBoolean(9, state.isRequiresAdminInteraction());
                ps.setBoolean(10, state.isUnknown());
                ps.setString(11, finalPayoutType);
                ps.setString(12, forceMessage);
                if (forcer == null) {
                    ps.setNull(13, Types.INTEGER);
                } else {
                    ps.setLong(13, forcer);
                }
                if (handler == null) {
                    ps.setNull(14, Types.INTEGER);
                } else {
                    ps.setLong(14, handler);
                }
                ps.execute();
            }
            return () -> true;
        });
        if (success != null && success.wasSuccessful()) {
            final Payout result = PayoutHelper.payoutBuilder.loadSingle(longUid, id);
            PayoutRestAPI.invalidate(longUid, true, result);
            invalidateItem(uid, result);
            if (result != null) {
                if (state.isRequiresAdminInteraction()) {
                    final NotificationObservable notificationObservable =
                            Main.instance().getNotificationObservable();
                    if (notificationObservable != null) {
                        final NotificationEvent e = new NotificationEvent(false, id, "update",
                                "payoutworker", Permission.READ_WORKER_PAYOUTS_PERMISSION,
                                new Payout[] { result });
                        notificationObservable.notifyObservers(e);
                    }

                }
                return new HelperResult<>(true, Status.CREATED.getStatusCode(), "payoutSuccess",
                        result);
            }
        }
        return new HelperResult<>(false, 500, "unknownError", null);
    }

    private static PAYOUT_STATE tryVkbaPayout(final long longUid, final String amount) {
        final String urlString =
                "https://vkba.legendskyfall.de/api/payment/?usage=Auszahlung&amount=" + amount
                        + "&receiver=" + UserHelper.get(longUid) + "&" + Variables.getVkbaAuth();
        try {
            final URL url = new URL(urlString);
            final URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", Variables.getAppName());
            connection.connect();
            try (InputStream stream = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
                final String k = reader.readLine();
                if ("true".equals(k)) {
                    return PAYOUT_STATE.SUCCESS;
                } else if ("false".equals(k)) {
                    return PAYOUT_STATE.FAILED;
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        } catch (final IOException e) {
            e.printStackTrace();
        } catch (final JsonParsingException e) {
        }
        return PAYOUT_STATE.UNKNOWN;
    }
}
