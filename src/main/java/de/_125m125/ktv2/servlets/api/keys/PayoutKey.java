package de._125m125.ktv2.servlets.api.keys;

public class PayoutKey {
    private final long    uid;
    private final int     limit;
    private final int     offset;
    private final boolean interactionOnly;

    public PayoutKey(final long uid, final boolean interactionOnly, final int limit,
            final int offset) {
        super();
        this.uid = uid;
        this.interactionOnly = interactionOnly;
        this.limit = limit;
        this.offset = offset;
    }

    public long getUid() {
        return this.uid;
    }

    public int getLimit() {
        return this.limit;
    }

    public int getOffset() {
        return this.offset;
    }

    public boolean isInteractionOnly() {
        return this.interactionOnly;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.interactionOnly ? 1231 : 1237);
        result = prime * result + this.limit;
        result = prime * result + this.offset;
        result = prime * result + (int) (this.uid ^ (this.uid >>> 32));
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PayoutKey other = (PayoutKey) obj;
        if (this.interactionOnly != other.interactionOnly) {
            return false;
        }
        if (this.limit != other.limit) {
            return false;
        }
        if (this.offset != other.offset) {
            return false;
        }
        if (this.uid != other.uid) {
            return false;
        }
        return true;
    }

}
