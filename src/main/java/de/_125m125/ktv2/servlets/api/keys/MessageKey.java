package de._125m125.ktv2.servlets.api.keys;

public class MessageKey {
    private final long uid;
    private final int  limit;
    private final int  offset;

    public MessageKey(final long uid, final int limit, final int offset) {
        super();
        this.uid = uid;
        this.limit = limit;
        this.offset = offset;
    }

    public long getUid() {
        return this.uid;
    }

    public int getLimit() {
        return this.limit;
    }

    public int getOffset() {
        return this.offset;
    }

}
