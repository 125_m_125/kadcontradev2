package de._125m125.ktv2.servlets.api.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public enum Permission {
    NO_PERMISSIONS("nPerm", 0b00000000000000000000000000000000, true, false),
    ALL_NONADMIN_PERMISSIONS("anaPerm", 0b01100000000000000000000000001111, true, false),
    ALL_PERMISSIONS("aPerm", 0b01111111111111111111111111111111, true, true),

    READ_ITEMMOVEMENTS_PERMISSION("rItemmovements", 0b00000000000000000000000100000000, false,
            false),
    READ_ITEMLIST_PERMISSION("rItems", 0b00000000000000000000000000000001, false, false),
    READ_MESSAGES_PERMISSION("rMessages", 0b00000000000000000000000000001000, false, false),
    READ_ORDERS_PERMISSION("rOrders", 0b00000000000000000000000000000010, false, false),
    READ_PAYOUTS_PERMISSION("rPayouts", 0b00000000000000000000000000000100, false, false),
    READ_TRADEEXECUTIONS_PERMISSION("rTradeexecution", 0b00000000000000000000001000000000, false,
            false),

    READ_ADMIN_ITEMMOVEMENTS_PERMISSION("arItemmovements", 0b00000000000000000000000001000000,
            false, true),
    READ_ADMIN_ITEMLIST_PERMISSION("arItems", 0b00000000000000000000000000100000, false, true),
    READ_ADMIN_MESSAGES_PERMISSION("arMessages", 0b00000000000000000000010000000000, false, true),
    READ_ADMIN_ORDERS_PERMISSION("arOrders", 0b00000000000000000000100000000000, false, true),
    READ_ADMIN_PAYOUTS_PERMISSION("arPayouts", 0b00000000000000000001000000000000, false, true),
    READ_ADMIN_TRADEEXECUTION_PERMISSION("arTradeexecutions", 0b00000000000000000000000010000000,
            false, true),

    READ_WORKER_PAYOUTS_PERMISSION("wrPayouts", 0b00000000000000000000000000010000, false, true),

    WRITE_ORDERS_PERMISSION("wOrders", 0b01000000000000000000000000000000, false, false),
    WRITE_PAYOUTS_PERMISSION("wPayouts", 0b00100000000000000000000000000000, false, false),

    WRITE_ADMIN_USERITEMS_PERMISSION("awItems", 0b00010000000000000000000000000000, false, true),
    WRITE_ADMIN_MESSAGES_PERMISSION("awMessages", 0b00000100000000000000000000000000, false, true),
    WRITE_ADMIN_ORDERS_PERMISSION("awOrders", 0b00000001000000000000000000000000, false, true),
    WRITE_ADMIN_REGISTER_PERMISSION("awRegistrations", 0b00001000000000000000000000000000, false,
            true),
    WRITE_ADMIN_PAYOUTS_PERMISSION("awPayouts", 0b00000000100000000000000000000000, false, true),

    WRITE_WORKER_PAYOUTS_PERMISSION("wwPayouts", 0b00000010000000000000000000000000, false, true),

    TWO_FA_PERMISSION("2fa", 0b00000000000000001000000000000000, false, false),
    //
    ;

    private static ASN1ObjectIdentifier base =
            new ASN1ObjectIdentifier("2.25.185209158787762157444538593016840947074");

    public static final Permission[]              PERMISSIONS;
    private static final Map<Integer, Permission> INT_TO_PERMISSION;
    private static final Map<String, Permission>  COM_TO_PERMISSION;
    private static final Map<String, Permission>  OID_TO_PERMISSION;

    static {
        final List<Permission> allPerms = new ArrayList<>();
        INT_TO_PERMISSION = new HashMap<>();
        COM_TO_PERMISSION = new HashMap<>();
        OID_TO_PERMISSION = new HashMap<>();
        for (final Permission p : Permission.values()) {
            if (!p.isUtility()) {
                allPerms.add(p);
                Permission.INT_TO_PERMISSION.put(p.getInteger(), p);
                Permission.COM_TO_PERMISSION.put(p.getComName(), p);
                Permission.OID_TO_PERMISSION
                        .put(Permission.base.branch(Integer.toString(p.getInteger())).getId(), p);
            }
        }
        PERMISSIONS = allPerms.toArray(new Permission[allPerms.size()]);
    }

    public static Set<Permission> generatePermissionSet(final int permissions) {
        final Set<Permission> result = new HashSet<>(Permission.PERMISSIONS.length);
        for (final Permission element : Permission.PERMISSIONS) {
            if (element.isPresentIn(permissions)) {
                result.add(element);
            }
        }
        return result;
    }

    public static Map<String, Boolean> generateComPermissionMap(final int permissions) {
        final Map<String, Boolean> result = new HashMap<>(Permission.PERMISSIONS.length);
        for (final Permission element : Permission.PERMISSIONS) {
            result.put(element.comName, element.isPresentIn(permissions));
        }
        return result;
    }

    public static int createPermissionFromRequest(final HttpServletRequest request,
            final boolean isAdmin) {
        int result = 0;
        for (final Permission p : Permission.PERMISSIONS) {
            if (!p.admin || isAdmin) {
                if (request.getParameter(p.getComName()) != null) {
                    result = p.appendTo(result);
                }
            }
        }
        return result;
    }

    public static Permission of(final int permission) {
        return Permission.INT_TO_PERMISSION.get(permission);
    }

    public static Permission of(final String name) {
        return Permission.COM_TO_PERMISSION.get(name);
    }

    public static Permission ofOID(final String name) {
        return Permission.OID_TO_PERMISSION.get(name);
    }

    private final String  comName;
    private final int     integer;
    private final boolean utility;
    private final boolean admin;

    Permission(final String comName, final int integer, final boolean utility,
            final boolean admin) {
        this.comName = comName;
        this.integer = integer;
        this.utility = utility;
        this.admin = admin;
    }

    public boolean isPresentIn(final int permissions) {
        return (permissions & this.integer) == this.integer;
    }

    public String getComName() {
        return this.comName;
    }

    public int getInteger() {
        return this.integer;
    }

    public boolean isAdmin() {
        return this.admin;
    }

    public boolean isUtility() {
        return this.utility;
    }

    public int appendTo(final int current) {
        return current | this.integer;
    }

}
