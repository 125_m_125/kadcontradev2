package de._125m125.ktv2.servlets.api.helper;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.datastorage.Connector.Successindicator;

public class HelperResult<T> implements Successindicator {
    private final boolean               success;
    private final int                   code;
    private final String                message;
    private final String                humanReadableMessage;
    private final T                     data;

    public HelperResult(final boolean success, final int status, final String message) {
        super();
        this.success = success;
        this.code = status;
        this.message = message;
        this.humanReadableMessage = Variables.translate(message);
        this.data = null;
    }

    public HelperResult(final boolean success, final int status, final T data) {
        super();
        this.success = success;
        this.code = status;
        this.message = null;
        this.humanReadableMessage = null;
        this.data = data;
    }

    public HelperResult(final boolean success, final int status, final String message, final T data) {
        super();
        this.success = success;
        this.code = status;
        this.message = message;
        this.humanReadableMessage = Variables.translate(message);
        this.data = data;
    }

    public int getStatus() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public T getData() {
        return this.data;
    }

    public String toJson() {
        return Variables.GSON.toJson(this);
    }

    @Override
    public boolean wasSuccessful() {
        return this.success;
    }

    public String getHumanReadableMessage() {
        return this.humanReadableMessage;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.code;
        result = prime * result + ((this.data == null) ? 0 : this.data.hashCode());
        result = prime * result + ((this.message == null) ? 0 : this.message.hashCode());
        result = prime * result + (this.success ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof HelperResult)) {
            return false;
        }
        @SuppressWarnings("rawtypes")
        final HelperResult other = (HelperResult) obj;
        if (this.code != other.code) {
            return false;
        }
        if (this.data == null) {
            if (other.data != null) {
                return false;
            }
        } else if (!this.data.equals(other.data)) {
            return false;
        }
        if (this.message == null) {
            if (other.message != null) {
                return false;
            }
        } else if (!this.message.equals(other.message)) {
            return false;
        }
        if (this.success != other.success) {
            return false;
        }
        return true;
    }
}
