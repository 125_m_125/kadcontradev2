package de._125m125.ktv2.servlets.api;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.servlets.api.helper.Permission;

@WebServlet(urlPatterns = { "/api/admin/payoutstats" })
public class PayoutStatsAdminAPIServlet extends HttpServlet {
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getAttribute("permissions") == null || (((int) request.getAttribute("permissions"))
                & Permission.READ_ADMIN_PAYOUTS_PERMISSION.getInteger()) == 0) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        response.setContentType("application/json");
        final JsonObjectBuilder jsonResponse = Json.createObjectBuilder();

        final ArrayList<HashMap<String, Object>> result = Main.instance().getConnector()
                .executeQuery("SELECT COUNT(*) as count,MAX(date) as latest FROM payout WHERE state='open'");
        if (result == null || result.size() != 1) {
            jsonResponse.add("count", 0L);
        } else {
            jsonResponse.add("count", ((long) result.get(0).get("count")));
            if (result.get(0).get("latest") != null) {
                jsonResponse.add("latest", ((Timestamp) result.get(0).get("latest")).getTime());
            }
        }

        response.getWriter().append(jsonResponse.build().toString());
    }
}
