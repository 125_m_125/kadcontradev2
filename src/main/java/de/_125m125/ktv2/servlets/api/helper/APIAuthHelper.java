package de._125m125.ktv2.servlets.api.helper;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Token;

public class APIAuthHelper {

    private static final String SIGNATURE_ALGORITHM = "HmacSHA256";

    public static LoginResult login(final String uid, final String tid, final String signature, final String data) {
        if (uid == null || tid == null || signature == null || data == null) {
            return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
        }
        try {
            final BigInteger decodedTid = Token.getBigInt(tid);
            final BigInteger decodedUid = Token.getBigInt(uid);
            final ArrayList<HashMap<String, Object>> id = Main.instance().getConnector().executeQuery(
                    "SELECT tkn, permissions FROM token WHERE uid=? AND tid=?", decodedUid.toString(),
                    decodedTid.toString());
            if (id != null && id.size() == 1) {
                final String token = Token.getBase32((BigInteger) id.get(0).get("tkn"));
                final SecretKeySpec signingKey = new SecretKeySpec(token.getBytes(), APIAuthHelper.SIGNATURE_ALGORITHM);
                String encoded = null;
                try {
                    final Mac mac = Mac.getInstance(APIAuthHelper.SIGNATURE_ALGORITHM);
                    mac.init(signingKey);
                    final byte[] rawHmac = mac.doFinal(data.getBytes());
                    encoded = DatatypeConverter.printHexBinary(rawHmac).toLowerCase();
                } catch (NoSuchAlgorithmException | InvalidKeyException e) {
                    e.printStackTrace();
                }
                if (signature.equals(encoded)) {
                    return new LoginResult(decodedUid.longValue(), decodedTid.longValue(),
                            (int) id.get(0).get("permissions"), "sig-" + tid);
                }
            }
        } catch (final NumberFormatException e) {
        }
        return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
    }

    public static LoginResult login(final String uid, final String tid, final String tkn) {
        if (uid == null || tkn == null || tid == null) {
            return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
        }
        try {
            final BigInteger decodedTid = Token.getBigInt(tid);
            final BigInteger decodedUid = Token.getBigInt(uid);
            final BigInteger decodedTkn = Token.getBigInt(tkn);
            final ArrayList<HashMap<String, Object>> id = Main.instance().getConnector().executeQuery(
                    "SELECT permissions FROM token WHERE uid=? AND tid=? AND tkn=?", decodedUid.toString(),
                    decodedTid.toString(), decodedTkn.toString());
            if (id != null && id.size() == 1) {
                return new LoginResult(decodedUid.longValue(), decodedTid.longValue(),
                        (int) id.get(0).get("permissions"), "tkn-" + tid);
            }
        } catch (final NumberFormatException e) {
        }
        return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
    }

    public static LoginResult login(final String uid, final String tid, final String tkn, final char pre) {
        if (uid == null || tkn == null || tid == null) {
            return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
        }
        try {
            final BigInteger decodedTid = Token.getBigInt(tid);
            final BigInteger decodedUid = Token.getBigInt(uid);
            final BigInteger decodedTkn = Token.getBigInt(tkn);
            final ArrayList<HashMap<String, Object>> id = Main.instance().getConnector().executeQuery(
                    "SELECT permissions FROM token WHERE uid=? AND tid=? AND tkn=?", decodedUid.toString(),
                    decodedTid.toString(), decodedTkn.toString());
            if (id != null && id.size() == 1) {
                return new LoginResult(decodedUid.longValue(), decodedTid.longValue(),
                        (int) id.get(0).get("permissions"), pre + "tkn-" + tid);
            }
        } catch (final NumberFormatException e) {
        }
        return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
    }

}
