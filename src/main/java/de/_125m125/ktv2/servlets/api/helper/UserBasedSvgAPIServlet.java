package de._125m125.ktv2.servlets.api.helper;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.cache.Tsvable;

public class UserBasedSvgAPIServlet<T extends Tsvable> extends SvgAPIServlet<String, T> {

    private final int permission;

    public UserBasedSvgAPIServlet(final LoadingCache<String, T> cache, final String filename, final int permission) {
        super(cache, filename);
        this.permission = permission;
    }

    @Override
    protected String getUnhandledRequestParameter(final HttpServletRequest request) {
        final String parameter = request.getParameter("uid");
        if (request.getAttribute("permissions") == null
                || (((int) request.getAttribute("permissions")) & this.permission) == 0) { return null; }
        return parameter;
    }

    @Override
    protected String getRequestParameter(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        final Long parameter = (Long) request.getAttribute("id");
        if (parameter == null || request.getAttribute("permissions") == null
                || (((int) request.getAttribute("permissions")) & this.permission) == 0) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
        return String.valueOf(parameter);
    }

    @Override
    protected String getCacheControlHeader() {
        return "private, no-cache, max-age: 3600";
    }

}
