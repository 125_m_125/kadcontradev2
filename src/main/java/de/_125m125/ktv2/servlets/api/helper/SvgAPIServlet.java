package de._125m125.ktv2.servlets.api.helper;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.cache.Tsvable;

public abstract class SvgAPIServlet<K, V extends Tsvable> extends HttpServlet {
    /**
     *
     */
    private static final long        serialVersionUID = -4660445959910017884L;

    private final LoadingCache<K, V> cache;
    private final String             contentDispositionHeader;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SvgAPIServlet(final LoadingCache<K, V> cache, final String filename) {
        super();
        this.cache = cache;
        this.contentDispositionHeader = "Attachment;filename=" + filename + ".csv";
    }

    @Override
    public long getLastModified(final HttpServletRequest request) {
        final K paramValue = getUnhandledRequestParameter(request);
        if (paramValue != null) {
            final V ifPresent = this.cache.getIfPresent(paramValue);
            if (ifPresent != null) {
                return ifPresent.getLastModified();
            }
        }
        return -1;
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        // get cache entry that should be served
        final K paramValue = getRequestParameter(request, response);
        if (paramValue == null) {
            return;
        }
        V value;
        try {
            value = this.cache.get(paramValue);
        } catch (final ExecutionException e) {
            e.printStackTrace();
            request.getSession(false).setAttribute("error", Variables.translate("Datacollectionerror"));
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }

        // check ETag
        final String currentETag = value.getETag();
        if (currentETag != null && currentETag.equals(request.getHeader("If-None-Match"))) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            return;
        }

        // create response
        response.setHeader("ETag", currentETag);
        response.setHeader("Content-Disposition", this.contentDispositionHeader);
        final String cacheControlHeader = getCacheControlHeader();
        if (cacheControlHeader != null) {
            response.setHeader("Cache-Control", cacheControlHeader);
        }
        final Long expiresHeader = getExpiresHeader();
        if (expiresHeader != null) {
            response.setDateHeader("Expires", expiresHeader);
        }
        response.setContentType("text/csv");

        response.getWriter().append(value.getTsvString(true));
    }

    protected abstract K getUnhandledRequestParameter(HttpServletRequest request);

    protected abstract K getRequestParameter(HttpServletRequest request, HttpServletResponse response)
            throws IOException;

    protected String getCacheControlHeader() {
        return null;
    }

    protected Long getExpiresHeader() {
        return null;
    }
}
