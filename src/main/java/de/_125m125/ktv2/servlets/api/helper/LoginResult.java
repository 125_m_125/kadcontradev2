package de._125m125.ktv2.servlets.api.helper;

public class LoginResult {
    private final long   uid;
    private final long   tid;
    private final int    permissions;
    private final String authType;

    public LoginResult(final long uid, final long tid, final int permissions, final String authType) {
        this.uid = uid;
        this.tid = tid;
        this.permissions = permissions;
        this.authType = authType;
    }

    public int getPermissions() {
        return this.permissions;
    }

    public long getUid() {
        return this.uid;
    }

    public long getTid() {
        return this.tid;
    }

    public String getAuthType() {
        return this.authType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.authType == null) ? 0 : this.authType.hashCode());
        result = prime * result + this.permissions;
        result = prime * result + (int) (this.tid ^ (this.tid >>> 32));
        result = prime * result + (int) (this.uid ^ (this.uid >>> 32));
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LoginResult other = (LoginResult) obj;
        if (this.authType == null) {
            if (other.authType != null) {
                return false;
            }
        } else if (!this.authType.equals(other.authType)) {
            return false;
        }
        if (this.permissions != other.permissions) {
            return false;
        }
        if (this.tid != other.tid) {
            return false;
        }
        if (this.uid != other.uid) {
            return false;
        }
        return true;
    }
}