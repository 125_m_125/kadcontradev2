package de._125m125.ktv2.servlets.api.helper;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Variables;

public class ApiResultHelper {

    public static void setStatus(final HttpServletRequest request, final HttpServletResponse response,
            final HelperResult<?> helperResult) throws IOException {
        if (request.getSession(false) != null) {
            request.getSession().setAttribute(helperResult.wasSuccessful() ? "info" : "error",
                    Variables.translate(helperResult.getMessage()));
        }
        if (response != null) {
            response.getWriter().append(helperResult.toJson());
        }
    }

    @Deprecated
    public static void setStatus(final HttpServletRequest request, final HttpServletResponse response,
            final boolean success, final String message) throws IOException {
        setStatus(request, response, new HelperResult<Void>(success, success ? 200 : 400, message));
    }

    @Deprecated
    public static <T> void setStatus(final HttpServletRequest request, final HttpServletResponse response,
            final boolean success, final String message, final T data) throws IOException {
        setStatus(request, response, new HelperResult<>(success, success ? 200 : 400, message, data));

    }
}
