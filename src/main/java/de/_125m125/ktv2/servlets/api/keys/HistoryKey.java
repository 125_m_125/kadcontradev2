package de._125m125.ktv2.servlets.api.keys;

public class HistoryKey {
    private final String material;
    private final int    limit;
    private final int    offset;

    public HistoryKey(final String material, final int limit, final int offset) {
        this.material = material;
        this.limit = limit;
        this.offset = offset;
    }

    public String getMaterial() {
        return this.material;
    }

    public int getLimit() {
        return this.limit;
    }

    public int getOffset() {
        return this.offset;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.limit;
        result = prime * result + ((this.material == null) ? 0 : this.material.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HistoryKey other = (HistoryKey) obj;
        if (this.limit != other.limit) {
            return false;
        }
        if (this.material == null) {
            if (other.material != null) {
                return false;
            }
        } else if (!this.material.equals(other.material)) {
            return false;
        }
        return true;
    }

}
