package de._125m125.ktv2.servlets.api.keys;

public class TradeExecutionKey {
    private final Long   start;
    private final Long   end;
    private final String matid;
    private final Long   userid;

    public TradeExecutionKey(final Long start, final Long end, final String matid,
            final Long userid) {
        super();
        this.start = start;
        this.end = end;
        this.matid = matid;
        this.userid = userid;
    }

    public Long getStart() {
        return this.start;
    }

    public Long getEnd() {
        return this.end;
    }

    public String getMatid() {
        return this.matid;
    }

    public Long getUserid() {
        return this.userid;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.end == null) ? 0 : this.end.hashCode());
        result = prime * result + ((this.matid == null) ? 0 : this.matid.hashCode());
        result = prime * result + ((this.start == null) ? 0 : this.start.hashCode());
        result = prime * result + ((this.userid == null) ? 0 : this.userid.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TradeExecutionKey other = (TradeExecutionKey) obj;
        if (this.end == null) {
            if (other.end != null) {
                return false;
            }
        } else if (!this.end.equals(other.end)) {
            return false;
        }
        if (this.matid == null) {
            if (other.matid != null) {
                return false;
            }
        } else if (!this.matid.equals(other.matid)) {
            return false;
        }
        if (this.start == null) {
            if (other.start != null) {
                return false;
            }
        } else if (!this.start.equals(other.start)) {
            return false;
        }
        if (this.userid == null) {
            if (other.userid != null) {
                return false;
            }
        } else if (!this.userid.equals(other.userid)) {
            return false;
        }
        return true;
    }

    public boolean containsTime(final long time) {
        return (this.start == null || time >= this.start) && (this.end == null || time < this.end);
    }
}
