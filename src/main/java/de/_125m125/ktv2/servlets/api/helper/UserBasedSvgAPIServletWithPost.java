package de._125m125.ktv2.servlets.api.helper;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.cache.Tsvable;

public abstract class UserBasedSvgAPIServletWithPost<T extends Tsvable> extends UserBasedSvgAPIServlet<T> {

    private final int writePermission;

    public UserBasedSvgAPIServletWithPost(final LoadingCache<String, T> cache, final String filename,
            final int readPermission, final int writePermission) {
        super(cache, filename, readPermission);
        this.writePermission = writePermission;
    }

    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (request.getAttribute("permissions") == null
                || (((int) request.getAttribute("permissions")) & this.writePermission) == 0) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        handlePost(request, response);
    }

    public abstract void handlePost(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
