package de._125m125.ktv2.servlets;

import java.io.IOException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.EncryptionHelper;
import de._125m125.ktv2.calculators.EncryptionHelper.PasswordResult;
import de._125m125.ktv2.calculators.EncryptionHelper.PasswordResult.STATE;
import de._125m125.ktv2.calculators.HaveIBeenPwnedRequester;
import de._125m125.ktv2.calculators.PostRedirectGet;

/**
 * Servlet implementation class Main_Servlet
 */
@WebServlet(urlPatterns = { "/index", "/login" }, loadOnStartup = 1)
public class OverviewServlet extends HttpServlet {

    public static final String                               mapping                 =
            "/|/index|/login";
    private static final long                                serialVersionUID        = 1L;
    private static final int                                 MAX_LOGIN_TRIES         = 5;
    private static final int                                 BAN_TIME                =
            10 * 60 * 60 * 1000;
    private static final LoadingCache<String, AtomicInteger> FAILED_LOGIN_COUNTER;
    public static final String                               REMEMBER_COOKIE_NAME    = "autoLogin";
    private static final int                                 REMEMBER_COOKIE_MAX_AGE = 604800;

    static {
        FAILED_LOGIN_COUNTER = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.HOURS)
                .build(new CacheLoader<String, AtomicInteger>() {
                    @Override
                    public AtomicInteger load(final String key) throws Exception {
                        return new AtomicInteger(0);
                    }
                });
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OverviewServlet() {
        super();
    }

    private boolean checkParams(final HttpServletRequest request, final String... requiredParams) {
        for (final String s : requiredParams) {
            if (request.getParameter(s) == null) {
                request.getSession().setAttribute("error",
                        Variables.translate("missingAttributes"));
                return false;
            }
        }
        return true;
    }

    @Override
    public void destroy() {

    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession s = request.getSession(false);
        String url;
        if (s != null && s.getAttribute("id") != null && (long) s.getAttribute("id") != -1) {
            request.setAttribute("matIntro", true);
            url = "/loggedin.jsp";
        } else {
            final Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (final Cookie cookie : cookies) {
                    if (OverviewServlet.REMEMBER_COOKIE_NAME.equals(cookie.getName())) {
                        if (tryRememberLogin(request, response, cookie)) {
                            return;
                        } else {
                            break;
                        }
                    }
                }
            }
            final String ref = request.getParameter("r");
            if (ref != null) {
                s = request.getSession(true);
                s.setAttribute("r", ref);
            }
            final HttpSession session = request.getSession(false);
            if (session != null) {
                request.setAttribute("r", session.getAttribute("r"));
            } else {
                request.setAttribute("r", request.getParameter("r"));
            }
            request.setAttribute("name", "Du bist nicht eingeloggt");
            url = "/login.jsp";
        }
        final ServletContext sc = getServletContext();
        final RequestDispatcher rd = sc.getRequestDispatcher(url);
        request.setAttribute("sessionExists", s != null);
        rd.forward(request, response);
    }

    private boolean tryRememberLogin(final HttpServletRequest request,
            final HttpServletResponse response, final Cookie cookie) throws IOException {
        try {
            final byte[] value = Base64.getUrlDecoder().decode(cookie.getValue());
            try (Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps = c.prepareStatement(
                            "SELECT user,value FROM rememberedLogins WHERE value=?",
                            ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
                ps.setBytes(1, value);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        final long id = rs.getLong("user");
                        afterLogin(request, response, request.getSession(), UserHelper.get(id), id,
                                true, "rem");
                        rs.deleteRow();
                        request.setAttribute("info", Variables.translate("autoLoginSuccess"));
                        return true;
                    }
                }
            }
        } catch (final SQLException e) {
            request.setAttribute("error", Variables.translate("unknownError"));
            e.printStackTrace();
        } catch (final IllegalArgumentException e) {
            // ignore
        }
        request.setAttribute("error", Variables.translate("autoLoginFailed"));
        cookie.setValue("");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        request.setAttribute("authFailure", true);
        return false;
    }

    private void generateRememberToken(final long id, final HttpServletResponse response) {
        final byte[] token = new byte[18];
        new SecureRandom().nextBytes(token);
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(
                        "INSERT INTO rememberedLogins(user, value) VALUES(?,?)")) {
            ps.setLong(1, id);
            ps.setBytes(2, token);
            ps.executeUpdate();
        } catch (final SQLException e) {
            e.printStackTrace();
            return;
        }
        final Cookie c = new Cookie(OverviewServlet.REMEMBER_COOKIE_NAME,
                Base64.getUrlEncoder().encodeToString(token));
        c.setHttpOnly(true);
        c.setSecure(true);
        c.setMaxAge(OverviewServlet.REMEMBER_COOKIE_MAX_AGE);
        response.addCookie(c);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("login") != null) {
            login(request, response);
        } else if (request.getParameter("register") != null) {
            register(request, response);
        } else if (request.getParameter("changepw") != null) {
            changepw(request, response);
        }
    }

    @Override
    public void init() throws UnavailableException {

    }

    private void login(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final HttpSession s = request.getSession();
        if (!checkParams(request, "l_name", "l_pass")) {
            PostRedirectGet.execute(request, response, "l_pass", "login");
        }
        final String stringPwd = request.getParameter("l_pass");
        final String login = Main.instance().getConnector().login(request.getParameter("l_name"),
                stringPwd, request.getRemoteAddr());
        if (login.startsWith("success")) {
            final int useCount = HaveIBeenPwnedRequester.INSTANCE.getUseCount(stringPwd);
            if (useCount > 0) {
                s.setAttribute("info", Variables.translate("pwnedLogin", useCount));
            }
            final String name = request.getParameter("l_name");
            final long id = Long.parseLong(login.substring(7));
            afterLogin(request, response, s, name, id, request.getParameter("remember") != null,
                    "pwd");
        } else {
            request.getSession().setAttribute("error", Variables.translate(login));
            request.setAttribute("authFailure", true);
            if (OverviewServlet.FAILED_LOGIN_COUNTER.getUnchecked(request.getParameter("l_name"))
                    .incrementAndGet() > OverviewServlet.MAX_LOGIN_TRIES) {
                CaptchaServlet.captcha(request, response, OverviewServlet.BAN_TIME,
                        Variables.translate("loginTries"));
            } else {
                PostRedirectGet.execute(request, response, "l_pass", "login");
            }
        }
    }

    private void afterLogin(final HttpServletRequest request, final HttpServletResponse response,
            final HttpSession s, final String name, final long id, final boolean remember,
            final String authType) throws IOException {
        s.setAttribute("id", id);
        s.setAttribute("name", name);
        s.setAttribute("authType", authType);
        final Object column = Main.instance().getConnector().getColumn("admin", "id", "id=" + id);
        if (column != null) {
            s.setAttribute("admin", true);
        }
        if (OverviewServlet.FAILED_LOGIN_COUNTER.getUnchecked(name)
                .get() > OverviewServlet.MAX_LOGIN_TRIES) {
            CaptchaServlet.captcha(request, response, OverviewServlet.BAN_TIME,
                    Variables.translate("loginTries"));
        } else {
            if (remember) {
                generateRememberToken(id, response);
            }
            if (s.getAttribute("source") != null) {
                response.sendRedirect((String) s.getAttribute("source"));
                s.setAttribute("source", null);
            } else {
                response.sendRedirect(request.getRequestURI());
            }
        }
    }

    private void register(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        String login = "";
        if (!checkParams(request, "r_name", "r_pass", "r_pass_2", "r_server", "r_x", "r_y",
                "r_z")) {
            prgRegister(request, response);
            return;
        }
        final String stringPwd = request.getParameter("r_pass");
        if (!stringPwd.equals(request.getParameter("r_pass_2"))) {
            request.getSession().setAttribute("error", Variables.translate("pwsdonotmatch"));
            prgRegister(request, response);
            return;
        }
        if (!"on".equals(request.getParameter("agb"))) {
            request.getSession().setAttribute("error", Variables.translate("agbsnotaccepted"));
            prgRegister(request, response);
            return;
        }
        String ref = (String) request.getSession().getAttribute("r");
        if (ref == null) {
            ref = request.getParameter("r");
            if (ref == null) {
                ref = request.getParameter("ref");
            }
        }
        final int useCount = HaveIBeenPwnedRequester.INSTANCE.getUseCount(stringPwd);
        if (useCount > 10) {
            request.getSession().setAttribute("error", Variables.translate("pwnedHard", useCount));
            prgRegister(request, response);
            return;
        }
        final PasswordResult password =
                EncryptionHelper.create(stringPwd, request.getParameter("r_name"));
        if (password.getState() == STATE.FAILURE) {
            login = password.getMessage();
        } else {
            if (password.getState() == STATE.WARNING) {
                request.getSession().setAttribute("error",
                        Variables.translate(password.getMessage()));
            }
            login = Main.instance().getConnector().possibleRegister(request.getParameter("r_name"),
                    password.getHashedPassword(),
                    "Server: " + request.getParameter("r_server") + " x: "
                            + request.getParameter("r_x") + " y: " + request.getParameter("r_y")
                            + " z: " + request.getParameter("r_z"),
                    ref != null ? ref : request.getParameter("ref"), request.getRemoteAddr());
        }
        if (login.startsWith("success")) {
            String message =
                    Variables.translate("activateRegSign").replaceFirst("%n", login.split(" ")[1]);
            message = message.concat("<br><a href='" + Variables.getBaseLink()
                    + "' target='_blank'>" + Variables.translate("baseLocation")
                    + Variables.getBaseLocation() + "</a>");
            if (login.startsWith("RefFailed", 7)) {
                message = message.concat("<br>" + Variables.translate("refFailed"));
            }
            if (useCount > 0) {
                message = message.concat("<br>" + Variables.translate("pwnedSoft", useCount));
            }
            request.getSession().setAttribute("info", message);
            prgRegister(request, response);
        } else {
            request.getSession().setAttribute("error", Variables.translate(login));
            prgRegister(request, response);
        }
    }

    private void prgRegister(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        PostRedirectGet.execute(request, response, "r_pass", "r_pass_2", "register");
    }

    private void changepw(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (!checkParams(request, "c_name", "c_pass", "c_pass_2")) {
            PostRedirectGet.execute(request, response, "c_pass", "c_pass_2", "changepw");
            return;
        }
        final String stringPwd = request.getParameter("c_pass");
        if (!stringPwd.equals(request.getParameter("c_pass_2"))) {
            request.getSession().setAttribute("error", Variables.translate("pwsdonotmatch"));
            PostRedirectGet.execute(request, response, "c_pass", "c_pass_2", "changepw");
            return;
        }
        final int useCount = HaveIBeenPwnedRequester.INSTANCE.getUseCount(stringPwd);
        if (useCount > 10) {
            request.getSession().setAttribute("error", Variables.translate("pwnedHard", useCount));
            PostRedirectGet.execute(request, response, "c_pass", "c_pass_2", "changepw");
            return;
        }
        String success = "";
        final PasswordResult password =
                EncryptionHelper.create(stringPwd, request.getParameter("c_name"));
        if (password.getState() == STATE.FAILURE) {
            success = password.getMessage();
        } else {
            if (password.getState() == STATE.WARNING) {
                request.getSession().setAttribute("error",
                        Variables.translate(password.getMessage()));
            }
            success = Main.instance().getConnector().possibleChangePass(
                    request.getParameter("c_name"), password.getHashedPassword());
        }
        if (success.startsWith("success")) {
            String message = Variables.translate("activatePassSign").replaceFirst("%n",
                    success.substring(7));
            message = message.concat("<br><a href='" + Variables.getBaseLink()
                    + "' target='_blank'>" + Variables.translate("baseLocation")
                    + Variables.getBaseLocation() + "</a>");
            if (useCount > 0) {
                message = message.concat("<br>" + Variables.translate("pwnedSoft", useCount));
            }
            request.getSession().setAttribute("info", message);
            PostRedirectGet.execute(request, response, "c_pass", "c_pass_2", "changepw");
            return;
        } else {
            request.getSession().setAttribute("error", Variables.translate(success));
            PostRedirectGet.execute(request, response, "c_pass", "c_pass_2", "changepw");
            return;
        }
    }

}
