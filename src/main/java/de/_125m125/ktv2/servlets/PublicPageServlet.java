package de._125m125.ktv2.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.ImmutableMap;
import com.google.common.io.ByteStreams;

/**
 * Servlet implementation class TOSServlet
 */
@WebServlet(urlPatterns = { "/p/*" })
public class PublicPageServlet extends HttpServlet {
    private static final long  serialVersionUID = 1L;

    public static final String mapping          = "/p/.*";

    Map<String, String>        htmls            = ImmutableMap.<String, String>builder()
            .put("about", "pub/about.html").put("impressum", "pub/about.html")
            .put("privacy", "pub/privacy.html").put("datenschutz", "pub/privacy.html")
            .put("tos", "pub/tos.html").put("agb", "pub/tos.html").build();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublicPageServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String file =
                request.getRequestURI().substring(request.getContextPath().length() + 3);
        final String loc = this.htmls.get(file);
        if (loc == null) {
            if (file.endsWith(".asc") || file.endsWith(".crt")) {
                final File f1 = new File("/home/kadcontrade/pub/" + file);
                if (!f1.getCanonicalPath().equals(f1.getAbsolutePath())) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI());
                    return;
                }
                if (f1.exists()) {
                    switch (file.substring(file.length() - 3, file.length())) {
                    case "asc":
                        response.setContentType("application/pgp-keys");
                        break;
                    case "crt":
                        response.setContentType("application/x-x509-user-cert");
                        break;
                    }
                    response.setHeader("Content-Disposition", "filename=" + file);
                    final OutputStream os = response.getOutputStream();
                    try (final InputStream is = new FileInputStream(f1)) {
                        ByteStreams.copy(is, os);
                    }
                    return;
                }
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI());
                return;
            }
        }
        request.setAttribute("file", loc);

        final ServletContext sc = getServletContext();
        final RequestDispatcher rd = sc.getRequestDispatcher("/fileImporter.jsp");
        if (rd == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI());
            return;
        }
        rd.forward(request, response);
    }
}
