package de._125m125.ktv2.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.reader.BankReader;

/**
 * Servlet implementation class BankReaderServlet
 */
@WebServlet("/read")
public class BankReaderServlet extends HttpServlet {
    public static final BankReader br               = new BankReader(
            Main.instance().getConnector());
    private static final long      serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BankReaderServlet() {
        super();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final HttpSession s = request.getSession();
        if (BankReaderServlet.br.read() /*&& VKBAReader.read()*/) {
            s.setAttribute("info", Variables.translate("readSuccess"));
        } else {
            s.setAttribute("error", Variables.translate("readFailed").replaceFirst("%t",
                    String.valueOf(BankReaderServlet.br.getRemainingTime())));
        }
        response.sendRedirect(request.getHeader("referer"));
    }
}
