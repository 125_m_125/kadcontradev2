package de._125m125.ktv2.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;
import org.bouncycastle.openpgp.PGPException;

import com.yubico.u2f.U2F;
import com.yubico.u2f.data.messages.RegisterRequestData;
import com.yubico.u2f.exceptions.U2fBadConfigurationException;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Coupon;
import de._125m125.ktv2.beans.GdprSettings;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.cache.payout.PayoutSettingsCache;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.mail.EncryptionType;
import de._125m125.ktv2.servlets.api.helper.Permission;
import de._125m125.ktv2.servlets.apiv2.AdminPoboxRestAPI;
import de._125m125.ktv2.servlets.helper.ServletHelper;
import de._125m125.ktv2.twoFactorAuth.AuthState;
import de._125m125.ktv2.twoFactorAuth.GoogleAuthenticatorJson;
import de._125m125.ktv2.twoFactorAuth.GoogleAuthenticatorVerifier;
import de._125m125.ktv2.twoFactorAuth.InvalidatedException;
import de._125m125.ktv2.twoFactorAuth.MailAuthenticatorJson;
import de._125m125.ktv2.twoFactorAuth.MailAuthenticatorVerifier;
import de._125m125.ktv2.twoFactorAuth.TwoFactorAuthDatabaseEntry;
import de._125m125.ktv2.twoFactorAuth.U2FAuthenticatorData;
import de._125m125.ktv2.twoFactorAuth.U2FRegisterVerifier;
import de._125m125.ktv2.twoFactorAuth.Verifier;
import de._125m125.ktv2.twoFactorAuth.WebAuthNAuthenticatorData;
import de._125m125.ktv2.twoFactorAuth.WebAuthNRegistrationVerifier;

@WebServlet(urlPatterns = { "/settings", "/einstellungen" })
public class SettingsServlet extends HttpServlet {
    private static final long          serialVersionUID    = 1L;
    private static final Random        rnd                 = new SecureRandom();
    private static PayoutSettingsCache payoutSettingsCache = new PayoutSettingsCache();

    @Override
    public void init() {
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SettingsServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final List<HashMap<String, Object>> tokenQueryResult = Main.instance().getConnector()
                .executeQuery("SELECT tid,permissions,tkn FROM token WHERE uid=?",
                        String.valueOf(request.getSession().getAttribute("id")));
        final List<Token> tokens;
        if (tokenQueryResult != null) {
            tokens = new ArrayList<>(tokenQueryResult.size());
            for (final Map<String, Object> token : tokenQueryResult) {
                tokens.add(new Token((long) token.get("tid"), (BigInteger) token.get("tkn"),
                        (int) token.get("permissions")));
            }
        } else {
            tokens = new ArrayList<>();
        }

        final GdprSettings gdprSettings =
                GdprSettings.loadSettings((long) request.getSession().getAttribute("id"));

        request.setAttribute("tokens", tokens);
        request.setAttribute("payoutSettings", SettingsServlet.payoutSettingsCache
                .get((Long) request.getSession().getAttribute("id")));
        request.setAttribute("sid",
                Long.toString((long) request.getSession().getAttribute("id"), 32));

        request.setAttribute("mfas",
                AuthState.getAuthMethods((long) request.getSession().getAttribute("id")));
        request.setAttribute("gdpr", gdprSettings);

        final ServletContext sc = getServletContext();
        final RequestDispatcher rd = sc.getRequestDispatcher("/settings.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        boolean doPRG = true;
        if (request.getParameter("newToken") != null) {
            createToken(request, response);
        } else if (request.getParameter("editToken") != null) {
            editToken(request, response);
        } else if (request.getParameter("savePayoutSettings") != null) {
            changePayoutSettings(request);
        } else if (request.getParameter("redeemCoupon") != null) {
            redeemCoupon(request);
        } else if (request.getParameter("request2fa") != null) {
            doPRG = request2FA(request, response);
        } else if (request.getParameter("verify2faData") != null) {
            store2Fa(request, response);
            doPRG = false;
        } else if (request.getParameter("requestOTPs") != null) {
            generateOTPs(request, response);
            doPRG = false;
        } else if (request.getParameter("requestPOBox") != null) {
            requestPOBox(request, response);
        } else if (request.getParameter("modifyGdpr") != null) {
            modifyGdpr(request, response);
        } else {
            request.getSession().setAttribute("error", Variables.translate("unknownAction"));
        }
        if (doPRG) {
            PostRedirectGet.execute(request, response, "newToken", "editToken",
                    "savePayoutSettings", "redeemCoupon", "requestPOBox", "requestOTPs", "type",
                    "modifyGdpr", "grant", "revoke");
        }
    }

    private void modifyGdpr(final HttpServletRequest request, final HttpServletResponse response) {
        final String grant = request.getParameter("grant");
        final String revoke = request.getParameter("revoke");
        if (grant != null && revoke != null) {
            request.setAttribute("error", Variables.translate("revokeAndGrant"));
            return;
        }
        final long uid = (long) request.getSession().getAttribute("id");
        if ("PL".equals(grant)) {
            try (Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps =
                            c.prepareStatement("INSERT INTO gdpr(user,type) VALUES(?,?)")) {
                ps.setLong(1, uid);
                ps.setString(2, "PL");
                ps.executeUpdate();
                request.setAttribute("info", Variables.translate("grantSuccess"));
                return;
            } catch (final SQLException e) {
                e.printStackTrace();
                request.setAttribute("error", Variables.translate("unknownError"));
                return;
            }
        }
        if ("PL".equals(revoke)) {
            try (Connection c = Main.instance().getConnector().getConnection()) {
                try (PreparedStatement ps = c.prepareStatement(
                        "SELECT TRUE FROM materials WHERE id=? AND matid LIKE \"-%\" AND matid!=-1 AND amount>0 UNION SELECT TRUE FROM trade WHERE traderid=? AND material LIKE \"-%\" AND material!=-1")) {
                    ps.setLong(1, uid);
                    ps.setLong(2, uid);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            request.setAttribute("error",
                                    Variables.translate("lotteryItemsInInventory"));
                            return;
                        }
                    }
                }
                try (PreparedStatement ps =
                        c.prepareStatement("DELETE FROM gdpr WHERE user=? AND type=?")) {
                    ps.setLong(1, uid);
                    ps.setString(2, "PL");
                    ps.executeUpdate();
                    request.setAttribute("info", Variables.translate("revokeSuccess"));
                    return;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
                request.setAttribute("error", Variables.translate("unknownError"));
                return;
            }
        }
    }

    private void generateOTPs(final HttpServletRequest request,
            final HttpServletResponse response) {
        int amount = 10;
        final String reqAmount = request.getParameter("amount");
        if (reqAmount != null) {
            try {
                amount = Math.max(20, Math.min(1, Integer.parseInt(reqAmount)));
            } catch (final NumberFormatException e) {
                // invalid number -> use default 10
            }
        }
        final List<TwoFactorAuthDatabaseEntry> codes = new ArrayList<>(amount);
        for (int i = 0; i < amount; i++) {
            codes.add(new TwoFactorAuthDatabaseEntry((long) request.getSession().getAttribute("id"),
                    "SC", Integer.toString(SettingsServlet.rnd.nextInt(90_000_000) + 10_000_000)));
        }
        response.addHeader("csrfPreventionSalt",
                (String) request.getSession().getAttribute("csrfPreventionSalt"));
        if (AuthState.replaceEntriesByType((long) request.getSession().getAttribute("id"), codes)) {
            try {
                final ServletOutputStream outputStream = response.getOutputStream();
                outputStream.println(codes.stream().map(TwoFactorAuthDatabaseEntry::getData)
                        .collect(Collectors.joining(",", "[", "]")));
                response.setStatus(200);
            } catch (final IOException e) {
                response.setStatus(500);
                e.printStackTrace();
            }
        } else {
            response.setStatus(500);
        }
    }

    private void requestPOBox(final HttpServletRequest request,
            final HttpServletResponse response) {
        final String serverString = request.getParameter("server");
        if (serverString == null) {
            request.setAttribute("error", Variables.translate("missingAttributes"));
            return;
        }
        int server;
        try {
            server = Integer.parseInt(serverString);
            if (server < 1 || server > 3) {
                request.setAttribute("error", Variables.translate("invalidServer"));
                return;
            }
        } catch (final NumberFormatException e) {
            request.setAttribute("error", Variables.translate("invalidServer"));
            return;
        }
        final long uid = (long) request.getSession().getAttribute("id");
        final Successindicator successindicator =
                Main.instance().getConnector().executeAtomic(c -> {
                    Integer boxid = null;
                    final PreparedStatement check = c.prepareStatement(
                            "SELECT cid FROM payoutboxes WHERE server=? AND owner=?");
                    check.setInt(1, server);
                    check.setLong(2, uid);
                    try (final ResultSet rs = check.executeQuery()) {
                        if (rs.next()) {
                            request.setAttribute("error",
                                    Variables.translate("poboxAlreadyOwnedForServer"));
                            return () -> false;
                        }
                    }
                    final PreparedStatement get = c.prepareStatement(
                            "SELECT cid FROM payoutboxes WHERE server=? AND owner IS NULL");
                    get.setInt(1, server);
                    try (final ResultSet rs = get.executeQuery()) {
                        if (rs.next()) {
                            boxid = rs.getInt(1);
                        } else {
                            request.setAttribute("error", Variables.translate("noFreeBox"));
                            return () -> false;
                        }
                    }
                    final PreparedStatement set = c.prepareStatement(
                            "UPDATE payoutboxes SET owner=? WHERE cid=? AND server=? AND owner IS NULL");
                    set.setLong(1, uid);
                    set.setInt(2, boxid);
                    set.setInt(3, server);
                    final int count = set.executeUpdate();
                    if (count != 1) {
                        request.setAttribute("error", Variables.translate("unknownError"));
                        return () -> false;
                    }
                    request.setAttribute("info", Variables.translate("poboxRequestSuccess"));
                    AdminPoboxRestAPI.invalidate();
                    SettingsServlet.payoutSettingsCache.invalidate(uid);
                    Log.PAYOUT.executeLog(c, Level.INFO,
                            uid + " successfully requested payoutbox " + boxid, uid);
                    return () -> true;
                });
        if (!successindicator.wasSuccessful()) {
            if (request.getAttribute("error") == null) {
                request.setAttribute("error", Variables.translate("unknownError"));
            }
        }
    }

    private boolean store2Fa(final HttpServletRequest request, final HttpServletResponse response) {
        response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
        final Verifier verifierData =
                (Verifier) request.getSession().getAttribute("2faVerifierRequest");
        if (verifierData == null) {
            try (OutputStream os = response.getOutputStream();
                    OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8")) {
                osw.write("{\"success\":false, \"message\":\""
                        + Variables.translate("no2faRequested") + "\"}");
            } catch (final IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        boolean verified;
        try {
            verified = verifierData.verify(request.getParameter("2faToken"), true);
        } catch (final InvalidatedException e1) {
            verified = false;
        }
        response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
        response.addHeader("csrfPreventionSalt",
                (String) request.getSession().getAttribute("csrfPreventionSalt"));
        try (OutputStream os = response.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8")) {
            if (verified) {
                verifierData.getData().store();
                try (Connection c = Main.instance().getConnector().getConnection()) {
                    try (PreparedStatement ip =
                            c.prepareStatement("DELETE FROM twoFactorIP WHERE id=?")) {
                        ip.setLong(1, (long) request.getSession().getAttribute("id"));
                        ip.executeUpdate();
                    }
                    try (PreparedStatement cookie =
                            c.prepareStatement("DELETE FROM twoFactorCookie WHERE id=?")) {
                        cookie.setLong(1, (long) request.getSession().getAttribute("id"));
                        cookie.executeUpdate();
                    }

                } catch (final SQLException e) {
                    e.printStackTrace();
                }
                request.getSession().setAttribute("info", "2faConfigSuccess");
                osw.write("{\"success\":true}");
            } else {
                osw.write("{\"success\":false, \"message\":\""
                        + Variables.translate("invalid2faToken") + "\"}");
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean request2FA(final HttpServletRequest request,
            final HttpServletResponse response) {
        final String type = request.getParameter("type");
        if (type == null) {
            request.setAttribute("error", Variables.translate("missingType"));
            return true;
        }
        switch (type) {
        case "NONE":
            try (Connection c = Main.instance().getConnector().getConnection()) {
                try (PreparedStatement ps =
                        c.prepareStatement("DELETE FROM twoFactorAuth WHERE uid=?")) {
                    ps.setLong(1, (long) request.getSession().getAttribute("id"));
                    ps.executeUpdate();
                }
            } catch (final SQLException e) {

                return true;
            }
            request.setAttribute("info", Variables.translate("2faDeactivated"));
            return true;
        case "TOTP":
            response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
            response.addHeader("csrfPreventionSalt",
                    (String) request.getSession().getAttribute("csrfPreventionSalt"));
            try (OutputStream os = response.getOutputStream();
                    OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8")) {
                final GoogleAuthenticatorJson verifierData = GoogleAuthenticatorJson
                        .createNew((long) request.getSession().getAttribute("id"));
                osw.write(Variables.GSON.toJson(verifierData));
                request.getSession().setAttribute("2faVerifierRequest",
                        new GoogleAuthenticatorVerifier(verifierData));
            } catch (final IOException e) {
                e.printStackTrace();
            }
            return false;
        case "MAIL":
            response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
            response.addHeader("csrfPreventionSalt",
                    (String) request.getSession().getAttribute("csrfPreventionSalt"));
            MailAuthenticatorJson verifierData;
            MailAuthenticatorVerifier mailAuthenticatorVerifier = null;
            String json = "{}";
            EncryptionType encryptionType;
            try {
                encryptionType = EncryptionType.valueOf(request.getParameter("encryption"));
            } catch (final IllegalArgumentException e) {
                encryptionType = null;
                json = "{\"message\":\"" + Variables.translate("invalidEncryptionType") + "\"}";
            }
            if (encryptionType != null) {
                try {
                    verifierData = new MailAuthenticatorJson(
                            (long) request.getSession().getAttribute("id"),
                            request.getParameter("mail"), encryptionType,
                            request.getParameter("publickey"));
                    mailAuthenticatorVerifier = new MailAuthenticatorVerifier(verifierData);
                    mailAuthenticatorVerifier.initialize();
                    if (mailAuthenticatorVerifier.getErrorMessage() != null) {
                        json = "{\"message\":\"" + mailAuthenticatorVerifier.getErrorMessage()
                                + "\"}";
                    } else {
                        json = Variables.GSON.toJson(verifierData);
                    }
                } catch (final CertificateException | IOException | PGPException e) {
                    e.printStackTrace();
                    json = "{\"message\":\"" + Variables.translate("invalidCertificate") + "\"}";
                } catch (final RuntimeException e) {
                    final Throwable cause = e.getCause();
                    if (cause == null) {
                        throw (e);
                    }
                    if (cause instanceof MessagingException) {
                        e.printStackTrace();
                        json = "{\"message\":\"" + Variables.translate("invalidMail") + "\"}";
                    }
                }
            }

            try (OutputStream os = response.getOutputStream();
                    OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8")) {
                osw.write(json);
                request.getSession().setAttribute("2faVerifierRequest", mailAuthenticatorVerifier);
            } catch (final IOException e) {
                e.printStackTrace();
            }
            return false;
        case "U2F":
            final U2F u2f = new U2F();
            response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
            response.addHeader("csrfPreventionSalt",
                    (String) request.getSession().getAttribute("csrfPreventionSalt"));
            final U2FAuthenticatorData uf2AuthData = U2FAuthenticatorData.forUser(
                    (long) request.getSession().getAttribute("id"), request.getServerName());
            try {
                final RegisterRequestData startRegistration = u2f.startRegistration(
                        "https://" + request.getServerName(), uf2AuthData.getDevices());
                final U2FRegisterVerifier u2fRegisterVerifier = new U2FRegisterVerifier(u2f,
                        (long) request.getSession().getAttribute("id"), startRegistration);
                request.getSession().setAttribute("2faVerifierRequest", u2fRegisterVerifier);
                try (ServletOutputStream os = response.getOutputStream()) {
                    os.println("{\"success\":true, \"registrationData\": "
                            + startRegistration.toJson() + "}");
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            } catch (final U2fBadConfigurationException e) {
                throw new RuntimeException(e);
            }
            return false;
        case "WAN":
            response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
            response.addHeader("csrfPreventionSalt",
                    (String) request.getSession().getAttribute("csrfPreventionSalt"));
            final WebAuthNAuthenticatorData webAuthNAuthenticatorData = WebAuthNAuthenticatorData
                    .forUser((long) request.getSession().getAttribute("id"),
                            request.getServerName());
            final WebAuthNRegistrationVerifier webAuthRegistrationVerifier =
                    new WebAuthNRegistrationVerifier(webAuthNAuthenticatorData);
            request.getSession().setAttribute("2faVerifierRequest", webAuthRegistrationVerifier);
            try (ServletOutputStream os = response.getOutputStream()) {
                os.println("{\"success\":true, \"registrationData\": "
                        + WebAuthNAuthenticatorData.jsonMapper.writeValueAsString(
                                webAuthRegistrationVerifier.getRequest())
                        + "}");
            } catch (final IOException e) {
                e.printStackTrace();
            }
        default:
            request.setAttribute("error", "unknown2FAType");
        }
        return true;
    }

    private void editToken(final HttpServletRequest request, final HttpServletResponse response) {
        final String tid = request.getParameter("tid");
        if (tid == null) {
            request.setAttribute("error", Variables.translate("missingTokenId"));
            return;
        }
        final String decodedTid;
        try {
            decodedTid = Token.getBigInt(tid).toString();
        } catch (final NumberFormatException ex) {
            request.setAttribute("error", Variables.translate("tokenNotExisting"));
            return;
        }
        final long uid = (long) request.getSession().getAttribute("id");
        final String stringUid = String.valueOf(uid);
        final ArrayList<HashMap<String, Object>> check = Main.instance().getConnector()
                .executeQuery("SELECT tid FROM token WHERE uid=? AND tid=?", stringUid, decodedTid);
        if (check == null || check.size() != 1) {
            request.setAttribute("error", Variables.translate("tokenNotExisting"));
            return;
        }
        final int permissions = Permission.createPermissionFromRequest(request,
                Boolean.TRUE.equals(request.getSession().getAttribute("admin")));
        if (permissions == 0) {
            final List<String[]> queries = new ArrayList<>();
            queries.add(new String[] { "DELETE FROM token WHERE uid=? AND tid=?", stringUid,
                    decodedTid });
            queries.addAll(Log.TOKEN.getLogQuerys(Level.INFO,
                    "set token permissions to " + permissions + " for token " + decodedTid, uid));
            Main.instance().getConnector().executeAtomicUpdate(queries);
            request.getSession().setAttribute("info", Variables.translate("tokenRemoveSuccess"));
        } else {
            final List<String[]> queries = new ArrayList<>();
            queries.add(new String[] { "UPDATE token SET permissions=? WHERE uid=? AND tid=?",
                    String.valueOf(permissions), stringUid, decodedTid });
            queries.addAll(Log.TOKEN.getLogQuerys(Level.INFO,
                    "set token permissions to " + permissions + " for token " + decodedTid, uid));
            Main.instance().getConnector().executeAtomicUpdate(queries);
            request.getSession().setAttribute("info", Variables.translate("tokenUpdateSuccess"));
        }
    }

    private void createToken(final HttpServletRequest request, final HttpServletResponse response) {
        final int permissions = Permission.createPermissionFromRequest(request,
                Boolean.TRUE.equals(request.getSession().getAttribute("admin")));
        if (permissions == 0) {
            request.getSession().setAttribute("error", Variables.translate("emptyPermissions"));
            return;
        }
        final String token = new BigInteger(64, SettingsServlet.rnd).toString();
        final String tokenid = new BigInteger(32, SettingsServlet.rnd).toString();
        final List<String[]> queries = new ArrayList<>();
        final long uid = (long) request.getSession().getAttribute("id");
        queries.add(new String[] { "INSERT INTO token(uid, tid, permissions,tkn) VALUES(?,?,?,?)",
                String.valueOf(uid), tokenid, String.valueOf(permissions), token });
        queries.addAll(Log.TOKEN.getLogQuerys(Level.INFO,
                "new token with permissions " + permissions + " created: " + token, uid));
        Main.instance().getConnector().executeAtomicUpdate(queries);
        request.getSession().setAttribute("info", Variables.translate("tokenCreationSuccess"));
    }

    private void changePayoutSettings(final HttpServletRequest request) throws IOException {
        if (!ServletHelper.checkParams(request, "server", "x", "y", "z", "warp")) {
            request.getSession().setAttribute("error", Variables.translate("missingAttributes"));
            return;
        }
        final long integerUid = (long) request.getSession().getAttribute("id");
        final String uid = String.valueOf(integerUid);
        final String serverString = request.getParameter("server");
        int server;
        final String xString = request.getParameter("x");
        int x;
        final String yString = request.getParameter("y");
        int y;
        final String zString = request.getParameter("z");
        int z;
        final String warp = request.getParameter("warp").replaceAll("\t", " ");
        try {
            server = Integer.parseInt(serverString);
            if (server < 1 || server > 3) {
                throw new NumberFormatException();
            }
        } catch (final NumberFormatException e) {
            request.getSession().setAttribute("error", Variables.translate("invalidServer"));
            return;
        }
        try {
            x = Integer.parseInt(xString);
            if (x < -10000 || x > 10000) {
                throw new NumberFormatException();
            }
        } catch (final NumberFormatException e) {
            request.getSession().setAttribute("error", Variables.translate("invalidX"));
            return;
        }
        try {
            y = Integer.parseInt(yString);
            if (y < 1 || y > 255) {
                throw new NumberFormatException();
            }
        } catch (final NumberFormatException e) {
            request.getSession().setAttribute("error", Variables.translate("invalidY"));
            return;
        }
        try {
            z = Integer.parseInt(zString);
            if (z < -10000 || z > 10000) {
                throw new NumberFormatException();
            }
        } catch (final NumberFormatException e) {
            request.getSession().setAttribute("error", Variables.translate("invalidZ"));
            return;
        }
        if (warp.length() > 15) {
            request.getSession().setAttribute("error", Variables.translate("invalidWarp"));
            return;
        }
        if (Main.instance().getConnector().executeUpdate(
                "REPLACE INTO payoutsettings VALUES(?,?,?,?,?,?)", uid, serverString, xString,
                yString, zString, warp)) {
            SettingsServlet.payoutSettingsCache.invalidate(integerUid);
            request.getSession().setAttribute("info", Variables.translate("settingsSuccess"));
            return;
        } else {
            request.getSession().setAttribute("error", Variables.translate("unknownError"));
            return;
        }
    }

    private void redeemCoupon(final HttpServletRequest request) throws IOException {
        if (!ServletHelper.checkParams(request, "code")) {
            request.getSession().setAttribute("error", Variables.translate("missingAttributes"));
            return;
        }
        final Coupon coupon = Coupon.redeemIfPossible(
                (long) request.getSession().getAttribute("id"), request.getParameter("code"));
        if (coupon == null) {
            request.getSession().setAttribute("error", Variables.translate("couponNotExisting"));
            return;
        } else {
            final String message = MessageFormat.format(Variables.translate("couponRedeemed"),
                    coupon.getAmount(), Variables.getDisplayNameForMatId(coupon.getMaterial()));
            request.getSession().setAttribute("info", message);
            return;
        }
    }

    public static PayoutSettingsCache getPayoutSettingsCache() {
        return SettingsServlet.payoutSettingsCache;
    }

}
