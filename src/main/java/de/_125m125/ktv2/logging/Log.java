package de._125m125.ktv2.logging;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;

public enum Log {
    BANK_READER("br"),
    MISSING_TRANSLATION("mt"),
    PAYOUT("pa"),
    SERVLET_REQUESTS("sr"),
    SYSTEM("sy"),
    TRADE("tr"),
    ATTACKS("at"),
    ADMIN("ad"),
    WEBSOCKET("ws"),
    TOKEN("to"),
    COUPON("co"),
    USER("us"),;

    private static boolean          active = true;
    private static final DateFormat df;
    private static final Logger     GENERAL;
    private static final Logger     SEVERE;

    public static void close() {
        for (final Handler h : Log.GENERAL.getHandlers()) {
            h.close();
        }
        for (final Handler h : Log.SEVERE.getHandlers()) {
            h.close();
        }
        for (final Log l : Log.values()) {
            for (final Handler h : l.LOGGER.getHandlers()) {
                h.close();
            }
        }
    }

    static {
        GENERAL = Logger.getLogger("ge");

        SEVERE = Logger.getLogger("ev");

        df = DateFormat.getDateTimeInstance(2, 2, Locale.GERMAN);

        if (new File(Variables.LOGPATH).mkdir()) {
            System.out.println("created dir for logging");
        }
        FileHandler fh = null;
        try {
            fh = new FileHandler(Variables.LOGPATH + Log.GENERAL.getName(), 0, 1, true);
            fh.setFormatter(new Formatter() {
                @Override
                public String format(final LogRecord record) {
                    return formatForFile(record);
                }
            });
            Log.GENERAL.addHandler(fh);
            Log.GENERAL.setUseParentHandlers(false);
            fh = new FileHandler(Variables.LOGPATH + Log.SEVERE.getName(), 0, 1, true);
            fh.setFormatter(new Formatter() {
                @Override
                public String format(final LogRecord record) {
                    return formatForFile(record);
                }
            });
            Log.SEVERE.addHandler(fh);
            Log.SEVERE.setUseParentHandlers(false);
        } catch (SecurityException | IOException e) {
            System.err.println("Erstellen des Loggers fehlgeschlagen!");
            if (fh != null) {
                fh.close();
            }
        }
    }

    public static void disable() {
        Log.active = false;
    }

    public static void enable() {
        Log.active = true;
    }

    public static String formatForFile(final LogRecord record) {
        return record.getLevel().toString() + ": " + Log.df.format(new Date(record.getMillis())) + "\r\n" + "\t"
                + record.getMessage() + "\r\n";
    }

    private boolean activated = false;

    private Logger  LOGGER;

    private Log(final String arg) {
        this.LOGGER = Logger.getLogger(arg);
        FileHandler fh = null;
        try {
            fh = new FileHandler(Variables.LOGPATH + this.LOGGER.getName(), 0, 1, true);
            fh.setFormatter(new Formatter() {
                @Override
                public String format(final LogRecord record) {
                    return formatForFile(record);
                }
            });
            this.LOGGER.addHandler(fh);
            this.LOGGER.setUseParentHandlers(false);
            this.activated = true;

        } catch (SecurityException | IOException e) {
            System.err.println("Erstellen des Loggers fehlgeschlagen!");
            System.err.println(e.getCause());
            if (fh != null) {
                fh.close();
            }
        }
    }

    public void log(final Level level, final String arg, final long... user) {
        if (!Log.active) {
            return;
        }
        if (!this.activated) {
            System.err.println("Kann log-datei nicht verwenden!");
        }
        this.LOGGER.log(level, arg);
        Log.GENERAL.log(level, arg);
        // System.out.println("logging things");
        if (level.intValue() > Level.INFO.intValue()) {
            Log.SEVERE.log(level, arg);
        }
        if (Main.instance() != null && Main.instance().getConnector() != null) {
            for (final long i : user) {
                Main.instance().getConnector()
                        .executeUpdate("INSERT INTO logging(type,level,uid,message) VALUES(?,?,?,?)", new String[] {
                                this.LOGGER.getName(), String.valueOf(level.intValue()), String.valueOf(i), arg });
            }
            if (user.length == 0) {
                Main.instance().getConnector().executeUpdate(
                        "INSERT INTO logging(type,level,uid,message) VALUES(?,?,?,?)",
                        new String[] { this.LOGGER.getName(), String.valueOf(level.intValue()), "0", arg });
            }
        } else {
            System.out.println(arg);
        }
    }

    public void log(final Level level, final long user, String text, final String... args) {
        for (final String arg : args) {
            text = text.replaceFirst("\\?", arg);
        }
        log(level, text, user);
    }

    public List<String[]> getLogQuerys(final Level level, final String arg, final long... owner) {
        final List<String[]> ret = new ArrayList<>();
        for (final long i : owner) {
            ret.add(new String[] { "INSERT INTO logging(type,level,uid,message) VALUES(?,?,?,?)", this.LOGGER.getName(),
                    String.valueOf(level.intValue()), String.valueOf(i), arg });
        }
        return ret;
    }

    public void executeLog(final Connection c, final Level level, final String arg, long... owner) throws SQLException {
        try (PreparedStatement ps = c.prepareStatement("INSERT INTO logging(type,level,uid,message) VALUES(?,?,?,?)")) {
            ps.setString(1, this.LOGGER.getName());
            ps.setInt(2, level.intValue());
            ps.setString(4, arg);
            if (owner.length == 0) {
                owner = new long[] { 0L };
            }
            for (final long i : owner) {
                ps.setLong(3, i);
                ps.execute();
            }
        }
    }
}