package de._125m125.ktv2.filter;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.logging.Level;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.logging.Log;

/**
 * Servlet Filter implementation class CSRFPreventionFilter
 */
@WebFilter(filterName = "SecurityFilter", value = "*")
public class SecurityFilter implements Filter {
    private final SecureRandom random = new SecureRandom();

    /**
     * Default constructor.
     */
    public SecurityFilter() {
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(final FilterConfig fConfig) throws ServletException {
    }

    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            final HttpServletRequest req = (HttpServletRequest) request;
            final HttpServletResponse res = (HttpServletResponse) response;
            res.addHeader("X-FRAME-OPTIONS", "DENY");
            res.addHeader("X-XSS-Protection", "1;mode=block");
            res.addHeader("X-Content-Type-Options", "nosniff");
            res.addHeader("X-Permitted-Cross-Domain-Policies", "none");
            final String domain = request.getServerName().replaceAll(".*\\.(?=.*\\.)", "");
            if (!Variables.getAllowedServers().contains(domain) && !"localhost".equals(domain)) {
                denyedServer(request, response, req, res, domain);
                return;
            }
            final HttpSession session = req.getSession(false);
            if (session != null) {
                if ("POST".equals(req.getMethod())) {
                    if ((session.getAttribute("id") != null || session.getAttribute("tknid") != null)
                            && !checkCSRF(req, session)) {
                        detectedCSRF(req, res, session);
                        return;
                    }
                }
                if (session.getAttribute("id") != null || session.getAttribute("tknid") != null) {
                    generateCSRFPreventionSalt(req, res, session);
                }
            }
        }
        chain.doFilter(request, response);
        if (request instanceof HttpServletRequest) {
            final HttpServletRequest req = (HttpServletRequest) request;
            final HttpServletResponse res = (HttpServletResponse) response;
            final HttpSession session = req.getSession(false);
            if (session != null && (session.getAttribute("id") != null || session.getAttribute("tknid") != null)) {
                generateCSRFPreventionSalt(req, res, session);
            }
        }
    }

    private void denyedServer(final ServletRequest request, final ServletResponse response,
            final HttpServletRequest req, final HttpServletResponse res, final String domain)
            throws ServletException, IOException {
        Log.ATTACKS.log(Level.INFO,
                "Access via unknown Server: " + req.getRequestURL() + " vic-ip: " + req.getRemoteAddr());

        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
        req.setAttribute("bh", domain);
        req.setAttribute("allowed", Variables.getALLOWED_HOSTS());

        final RequestDispatcher rd = req.getRequestDispatcher("/forbiddenHost.jsp");
        rd.forward(request, response);
        return;
    }

    private void detectedCSRF(final HttpServletRequest req, final HttpServletResponse res, final HttpSession session)
            throws IOException {
        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
        Long id = (Long) session.getAttribute("id");
        if (id == null) {
            id = (Long) session.getAttribute("tknid");
        }
        if (id != null) {
            Log.ATTACKS.log(Level.INFO, "Possible csrf-attack detected. Referrer: " + req.getHeader("referer")
                    + " target; " + req.getRequestURL() + " vic-ip: " + req.getRemoteAddr(), id);
        } else {
            Log.ATTACKS.log(Level.INFO, "Possible csrf-attack detected. Referrer: " + req.getHeader("referer")
                    + " target; " + req.getRequestURL() + " vic-ip: " + req.getRemoteAddr());
        }
        session.invalidate();
        req.getSession().setAttribute("error", Variables.translate("csrfDetected"));
        res.sendRedirect("login");
    }

    public boolean checkCSRF(final HttpServletRequest req, final HttpSession session)
            throws IOException, ServletException {
        if (!isInternalSource(req)) {
            return false;
        }
        return Boolean.TRUE.equals(session.getAttribute("noCsrfProtection")) || checkCSRFPreventionSalt(req, session);
    }

    public boolean isInternalSource(final HttpServletRequest req) {
        final String origin = req.getHeader("origin");
        if (origin != null) {
            if (!origin.matches("^https?://localhost.*$")) {
                boolean found = false;
                for (final String allowedHost : Variables.getALLOWED_HOSTS()) {
                    if (origin.contains(allowedHost)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }
        }
        final String referer = req.getHeader("referer");
        if (referer != null) {
            if (!referer.matches("^https?://localhost.*$")) {
                boolean found = false;
                for (final String allowedHost : Variables.getALLOWED_HOSTS()) {
                    if (referer.contains(allowedHost)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkCSRFPreventionSalt(final HttpServletRequest req, final HttpSession session)
            throws IOException, ServletException {
        // Get the salt sent with the request
        final String salt = req.getParameter("csrfPreventionSalt");
        if (salt == null) {
            return false;
        }

        final String csrfPreventionSalt = (String) session.getAttribute("csrfPreventionSalt");
        if (salt.equals(csrfPreventionSalt)) {
            if (!req.getRequestURI().endsWith("errors")) {
                session.removeAttribute("csrfPreventionSalt");
            }
            return true;
        }
        return false;
    }

    private void generateCSRFPreventionSalt(final HttpServletRequest req, final HttpServletResponse res,
            final HttpSession session) {
        String csrfPreventionSalt = (String) session.getAttribute("csrfPreventionSalt");
        if (csrfPreventionSalt == null || csrfPreventionSalt.equals("")) {
            csrfPreventionSalt = new BigInteger(130, this.random).toString(32);
            session.setAttribute("csrfPreventionSalt", csrfPreventionSalt);
        }
        req.setAttribute("csrfPreventionSalt", csrfPreventionSalt);
        if (session.getAttribute("tknid") != null
                && req.getRequestURI().startsWith("/api", req.getContextPath().length())
                && !csrfPreventionSalt.equals(res.getHeader("csrfPreventionSalt"))) {
            res.setHeader("csrfPreventionSalt", csrfPreventionSalt);
        }
    }

}
