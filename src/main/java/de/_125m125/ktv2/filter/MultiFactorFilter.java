package de._125m125.ktv2.filter;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bouncycastle.util.Arrays;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.PostRedirectGet;
import de._125m125.ktv2.twoFactorAuth.AuthState;
import de._125m125.ktv2.twoFactorAuth.InvalidatedException;

@WebFilter(filterName = "MultiFactorFilter", value = "*")
public class MultiFactorFilter implements Filter {

    private static final Cache<String, AuthState>                  authStates          =
            CacheBuilder.newBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build();
    private static final Map<Long, List<WeakReference<AuthState>>> authStatesPerUserid =
            new ConcurrentHashMap<>();
    private static final Random                                    random              =
            new SecureRandom();

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res,
            final FilterChain chain) throws IOException, ServletException {
        if (!(req instanceof HttpServletRequest)) {
            chain.doFilter(req, res);
            return;
        }
        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;
        final HttpSession session = request.getSession(false);
        if (request.getRequestURI().matches(".*/static/.*\\..*")) {
            chain.doFilter(request, response);
            return;
        }
        if (session == null) {
            chain.doFilter(request, response);
            return;
        }
        final Boolean attribute = (Boolean) session.getAttribute("multiAuth");
        if (!Boolean.TRUE.equals(attribute)) {
            final Long id = (Long) session.getAttribute("id");
            if (id == null || id <= 0) {
                chain.doFilter(request, response);
                return;
            }
            AuthState authState = MultiFactorFilter.authStates.getIfPresent(session.getId());
            if (authState == null) {
                authState = AuthState.createFor(id, request);
                MultiFactorFilter.authStates.put(session.getId(), authState);
                final List<WeakReference<AuthState>> list = MultiFactorFilter.authStatesPerUserid
                        .computeIfAbsent(id, (key) -> new ArrayList<>());
                synchronized (list) {
                    list.add(new WeakReference<>(authState));
                }
            }
            if (authState.isAuthenticated()) {
                session.setAttribute("multiAuth", true);
                chain.doFilter(request, response);
                return;
            } else if (authState.triesLimitReached()) {
                request.setAttribute("error", Variables.translate("triesLimitReached"));
                request.getRequestDispatcher("/empty.jsp").forward(request, response);
                return;
            } else {
                if ("POST".equals(request.getMethod()) && request.getParameter("2FaAuth") != null) {
                    final AuthState finalAuthState = authState;
                    try {
                        final boolean success = request.getParameterMap().entrySet().stream()
                                .filter(e -> e.getValue().length > 0)
                                .filter(e -> !e.getValue()[0].isEmpty())
                                .filter(e -> e.getKey().endsWith("Auth"))
                                .anyMatch(e -> finalAuthState.verify(
                                        e.getKey().substring(0, 2).toUpperCase(Locale.ENGLISH),
                                        e.getValue()[0], true));
                        if (success) {
                            persistAuth(response, id, request.getParameter("cookie"),
                                    request.getParameter("ip"), request.getRemoteAddr());
                            PostRedirectGet.execute(request, response, "2FaAuth", "authData",
                                    "gaAuth", "scAuth", "ufAuth", "maAuth", "waAuth");
                            return;
                        } else {
                            request.setAttribute("error", Variables.translate("2FaFailed"));
                        }
                    } catch (final InvalidatedException e) {
                        authState = AuthState.createFor(id, request);
                        MultiFactorFilter.authStates.put(session.getId(), authState);
                        final List<WeakReference<AuthState>> list =
                                MultiFactorFilter.authStatesPerUserid.computeIfAbsent(id,
                                        (key) -> new ArrayList<>());
                        synchronized (list) {
                            list.add(new WeakReference<>(authState));
                        }
                    }
                }
                request.setAttribute("auths", authState.getPublicInfos());
                request.getRequestDispatcher("/multiFactor.jsp").forward(request, response);
                return;
            }
        } else {
            chain.doFilter(request, response);
            return;
        }
    }

    @Override
    public void destroy() {
    }

    public static boolean auth(final long id, final String type, final String authData,
            final boolean userInvoked) throws InvalidatedException {
        final List<WeakReference<AuthState>> userAuthStates =
                MultiFactorFilter.authStatesPerUserid.get(id);
        if (userAuthStates == null || userAuthStates.isEmpty()) {
            return false;
        }
        synchronized (userAuthStates) {
            for (final Iterator<WeakReference<AuthState>> iterator =
                    userAuthStates.iterator(); iterator.hasNext();) {
                final WeakReference<AuthState> weakReference = iterator.next();
                final AuthState authStatus = weakReference.get();
                if (authStatus == null) {
                    iterator.remove();
                    continue;
                }
                if (authStatus.verify(type, authData, userInvoked)) {
                    iterator.remove();
                    return true;
                }
            }
        }
        return false;
    }

    public void persistAuth(final HttpServletResponse response, final long uid,
            final String cookieRequest, final String ipRequested, final String ip) {
        if (cookieRequest != null) {
            final BigInteger bigInteger = new BigInteger(128, MultiFactorFilter.random);
            final Cookie cookie = new Cookie(AuthState.COOKIE_NAME, bigInteger.toString(32));
            cookie.setHttpOnly(true);
            cookie.setMaxAge(2628000);
            response.addCookie(cookie);
            try (Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps = c.prepareStatement(
                            "INSERT INTO twoFactorCookie(id, cookie) VALUES(?,?)")) {
                ps.setLong(1, uid);
                byte[] byteArray = bigInteger.toByteArray();
                if (byteArray[0] == 0) {
                    byteArray = Arrays.copyOfRange(byteArray, 1, byteArray.length - 1);
                }
                ps.setBytes(2, byteArray);
                ps.executeUpdate();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        if (ipRequested != null) {
            try (Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement ps =
                            c.prepareStatement("INSERT INTO twoFactorIP(id, ip) VALUES(?,?)")) {
                ps.setLong(1, uid);
                ps.setString(2, ip);
                ps.executeUpdate();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
