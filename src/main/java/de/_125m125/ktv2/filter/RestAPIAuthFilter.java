package de._125m125.ktv2.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.message.internal.MediaTypes;
import org.glassfish.jersey.server.ContainerRequest;

import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.calculators.CertificateManager;
import de._125m125.ktv2.calculators.CertificateManager.CertificateDetails;
import de._125m125.ktv2.servlets.api.helper.APIAuthHelper;
import de._125m125.ktv2.servlets.api.helper.LoginResult;
import de._125m125.ktv2.servlets.api.helper.Permission;

@Provider
public class RestAPIAuthFilter implements ContainerRequestFilter {
    private static final long MAX_TIME_DIFF = 5 * 60 * 1000;
    private final Clock       clock         = Clock.systemDefaultZone();

    private final SecurityFilter sf = new SecurityFilter();
    @Context
    private HttpServletRequest   servletRequest;

    @Override
    public void filter(final ContainerRequestContext requestContext) throws IOException {
        if (requestContext.getMethod().equals("POST")) {
            final String headerString = requestContext.getHeaderString("content-type");
            if (headerString == null || headerString.isEmpty()) {
                throw new NotSupportedException("missingContentType");
            }
        }

        final List<String> users = new ArrayList<>();
        List<String> candicates = requestContext.getUriInfo().getPathParameters().get("user");
        if (candicates != null) {
            users.addAll(candicates);
        }
        candicates = requestContext.getUriInfo().getPathParameters().get("admin");
        if (candicates != null) {
            users.addAll(candicates);
        }
        candicates = requestContext.getUriInfo().getPathParameters().get("worker");
        if (candicates != null) {
            users.addAll(candicates);
        }
        candicates = requestContext.getUriInfo().getQueryParameters().get("user");
        if (candicates != null) {
            users.addAll(candicates);
        }
        candicates = requestContext.getUriInfo().getQueryParameters().get("uid");
        if (candicates != null) {
            users.addAll(candicates);
        }
        if (users == null || users.size() == 0) {
            return;
        }
        if (users.size() > 1) {
            throw new BadRequestException("UnexpectedSecondUser");
        }
        final String uidParameter = users.get(0);

        final LoginResult lr = getLoginData(uidParameter, requestContext);
        final long uid = lr.getUid();
        final int permissions = lr.getPermissions();
        final String authType = lr.getAuthType();
        final long requestedUid;
        try {
            requestedUid = Token.getBigInt(uidParameter).longValue();
        } catch (final NumberFormatException e) {
            this.servletRequest.setAttribute("authFailure", true);
            throw new BadRequestException("InvalidUserid");
        }
        if (uid == 0) {
            this.servletRequest.setAttribute("authFailure", true);
            throw new NotAuthorizedException(Response.status(Status.UNAUTHORIZED)
                    .header("WWW-Authenticate", "Basic realm=\"API\"").build());
        }
        if (uid != requestedUid && (!Permission.ALL_PERMISSIONS.isPresentIn(permissions)
                || !"GET".equals(requestContext.getMethod()))) {
            throw new ForbiddenException();
        }
        this.servletRequest.setAttribute("authType", authType);
        this.servletRequest.setAttribute("permissions", permissions);
        this.servletRequest.setAttribute("id", requestedUid);
        this.servletRequest.setAttribute("tknid", lr.getTid());
    }

    private LoginResult getLoginData(final String uid,
            final ContainerRequestContext requestContext) {
        final HttpSession s = this.servletRequest.getSession(false);
        if (s != null) {
            if (s.getAttribute("id") != null && (long) s.getAttribute("id") > 0) {
                if (Boolean.TRUE.equals(s.getAttribute("admin"))) {
                    return new LoginResult((long) s.getAttribute("id"), 0,
                            Permission.ALL_PERMISSIONS.getInteger(), "pwd");
                } else {
                    return new LoginResult((long) s.getAttribute("id"), 0,
                            Permission.ALL_NONADMIN_PERMISSIONS.getInteger(), "pwd");
                }
            }
            final Object tbid = s.getAttribute("tbid");
            final Object tknid = s.getAttribute("tknid");
            if (tknid != null) {
                return new LoginResult((long) tbid, (long) tknid,
                        (int) s.getAttribute("permissions"), "stkn-" + tknid);
            }
        }
        final String authHeader = this.servletRequest.getHeader("Authorization");
        if (authHeader != null) {
            final StringTokenizer st = new StringTokenizer(authHeader);
            if (st.hasMoreTokens()) {
                final String basic = st.nextToken();
                if (basic.equalsIgnoreCase("Basic")) {
                    try {
                        final String credentials =
                                new String(Base64.getDecoder().decode(st.nextToken()), "UTF-8");
                        final int p = credentials.indexOf(":");
                        if (p != -1) {
                            final String tid = credentials.substring(0, p).trim();
                            final String tkn = credentials.substring(p + 1).trim();

                            return APIAuthHelper.login(uid, tid, tkn, 'b');
                        }
                    } catch (final UnsupportedEncodingException e) {
                    }
                }
            }
        }
        if (this.sf.isInternalSource(this.servletRequest)) {
            final CertificateDetails certificatePermissions =
                    CertificateManager.instance().getCertificateDetails(this.servletRequest);
            if (certificatePermissions.getPermissions() > 0 && Permission.TWO_FA_PERMISSION
                    .getInteger() != certificatePermissions.getPermissions()) {
                return new LoginResult(certificatePermissions.getUser(), 0,
                        certificatePermissions.getPermissions(),
                        "cert" + certificatePermissions.getSerial().mod(BigInteger.valueOf(8192)));
            }
        }
        final TreeMap<String, String> parameterMap = new TreeMap<>();
        requestContext.getUriInfo().getQueryParameters()
                .forEach((k, v) -> parameterMap.put(k, v.get(0)));
        if (requestContext instanceof ContainerRequest) {
            final ContainerRequest request = (ContainerRequest) requestContext;

            if (requestContext.hasEntity() && MediaTypes.typeEqual(
                    MediaType.APPLICATION_FORM_URLENCODED_TYPE, request.getMediaType())) {
                request.bufferEntity();
                final Form f = request.readEntity(Form.class);
                f.asMap().forEach((k, v) -> parameterMap.put(k, v.get(0)));
            }
        }
        final String tid = parameterMap.get("tid");
        final String signature = parameterMap.remove("signature");
        if (signature != null) {
            final String timestamp = parameterMap.get("timestamp");
            long longTimestamp;
            try {
                longTimestamp = Long.parseLong(timestamp);
            } catch (final NumberFormatException e) {
                return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
            }
            if (Math.abs(this.clock.millis() - longTimestamp) > RestAPIAuthFilter.MAX_TIME_DIFF) {
                return new LoginResult(0, 0, Permission.NO_PERMISSIONS.getInteger(), "none");
            }
            final StringBuilder result = new StringBuilder();
            parameterMap.forEach((k, v) -> result.append(k).append("=").append(v).append("&"));
            return APIAuthHelper.login(uid, tid, signature,
                    result.deleteCharAt(result.length() - 1).toString());
        }
        final String tkn = parameterMap.get("tkn");
        return APIAuthHelper.login(uid, tid, tkn);
    }

}
