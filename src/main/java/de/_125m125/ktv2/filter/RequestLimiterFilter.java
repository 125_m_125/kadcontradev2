package de._125m125.ktv2.filter;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.CaptchaServlet;

/**
 * Servlet Filter implementation class RequestLimiterFilter
 */
@WebFilter(filterName = "RequestLimiterFilter", value = "*", initParams = {
        @WebInitParam(name = "active", value = "1") })
public class RequestLimiterFilter implements Filter {

    private static final int                              MAX_REQUESTS_PER_MINUTE = Variables
            .getOrDefault("maxRequestsPerMinute", 60);
    private static final int                              MAX_REQUESTS_PER_HOUR   = Variables
            .getOrDefault("maxRequestPerHour", 600);

    private static final String                           TMR_MESSAGE             = "Du hast diese %t bereits zu viele Anfragen gestellt. Um den Server zu entlasten werden weitere Anfragen innerhalb dieser %t geblockt.";

    private static final ConcurrentHashMap<String, int[]> requestcounter          = new ConcurrentHashMap<>(
            50, 0.75f, 5);
    private final TimerTask                               timerTask               = new TimerTaskExtension();
    private Timer                                         t;

    private boolean                                       active                  = false;

    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
        if (this.active) {
            this.timerTask.cancel();
            this.t.cancel();
        }
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response,
            final FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        if (this.active && request instanceof HttpServletRequest) {
            final String page = ((HttpServletRequest) request).getRequestURI()
                    .substring(((HttpServletRequest) request).getContextPath().length());
            if ((CaptchaServlet.isCaptchad(request.getRemoteAddr()))) {
                if (!page.startsWith("/captcha")) {
                    if (response instanceof HttpServletResponse) {
                        ((HttpServletResponse) response).sendRedirect("/captcha");
                    }
                } else {
                    chain.doFilter(request, response);
                }
                return;
            }
            if (page.endsWith("/ping")) {
                ((HttpServletResponse) response).getWriter()
                        .append(String.valueOf(System.currentTimeMillis()));
                ((HttpServletResponse) response).addHeader("Cache-Control", "no-store");
                return;
            }
            if (page.startsWith("/static")) {
                chain.doFilter(request, response);
                return;
            }
            if (!manageRequestCounter((HttpServletRequest) request,
                    (HttpServletResponse) response)) {
                chain.doFilter(request, response);
                if (Boolean.TRUE.equals(request.getAttribute("authFailure"))) {
                    incrementRequestCounter((HttpServletRequest) request,
                            RequestLimiterFilter.MAX_REQUESTS_PER_MINUTE / 10);
                }
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(final FilterConfig fConfig) throws ServletException {
        this.active = !fConfig.getInitParameter("active").equals("0");
        if (this.active) {
            this.t = new Timer();
            this.t.schedule(this.timerTask, 60000L, 60000L);
        }
    }

    private boolean manageRequestCounter(final HttpServletRequest req,
            final HttpServletResponse res) throws IOException {
        final int[] rc = incrementRequestCounter(req, 1);
        if (rc[0] >= RequestLimiterFilter.MAX_REQUESTS_PER_MINUTE) {
            CaptchaServlet.captcha(req, res, System.currentTimeMillis() + 60000,
                    RequestLimiterFilter.TMR_MESSAGE.replaceAll("%t", "Minute"));
            Log.SERVLET_REQUESTS.log(Level.INFO, req.getRemoteAddr()
                    + " got blocked for the rest of the minute(too many requests)");
            rc[0] = 0;
            return true;
        }
        if (rc[1] > RequestLimiterFilter.MAX_REQUESTS_PER_HOUR) {
            CaptchaServlet.captcha(req, res, System.currentTimeMillis() + 360000,
                    RequestLimiterFilter.TMR_MESSAGE.replaceAll("%t", "Stunde"));
            Log.SERVLET_REQUESTS.log(Level.INFO, req.getRemoteAddr()
                    + " got blocked for the rest of the hour(too many requests)");
            rc[1] = 0;
            return true;
        }
        return false;
    }

    private int[] incrementRequestCounter(final HttpServletRequest req, final int count) {
        int[] rc = RequestLimiterFilter.requestcounter.get(req.getRemoteAddr());
        if (rc == null) {
            rc = new int[] { 0, 0 };
            RequestLimiterFilter.requestcounter.put(req.getRemoteAddr(), rc);
        }
        rc[0] = rc[0] + count;
        rc[1] = rc[1] + count;
        return rc;
    }

    private final class TimerTaskExtension extends TimerTask {
        int i = 0;

        @Override
        public void run() {
            if (this.i == 60) {
                RequestLimiterFilter.requestcounter.clear();
                this.i = 0;
                return;
            }
            RequestLimiterFilter.requestcounter.values().forEach(e -> e[0] = 0);
            this.i++;
        }
    }
}
