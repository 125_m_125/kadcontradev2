package de._125m125.ktv2.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.OverviewServlet;

/**
 * Servlet Filter implementation class SessionManager
 */
@WebFilter(filterName = "SessionManagmentFilter", value = "*")
public class SessionManagmentFilter implements Filter {

    private boolean checkUriException(final String requestURI, final String contextPath) {
        final String request = requestURI.substring(contextPath.length());
        return !request.contains(".jsp") && (request.matches(".*\\..*") || request.startsWith("/read")
                || request.matches(OverviewServlet.mapping) || request.startsWith("/p/")
                || request.startsWith("/captcha") || request.startsWith("/api/"));
    }

    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        final long start = System.currentTimeMillis();
        if (request instanceof HttpServletRequest) {
            final HttpServletRequest req = (HttpServletRequest) request;
            final HttpServletResponse res = (HttpServletResponse) response;

            final String requestURI = req.getRequestURI();
            final String contextPath = req.getContextPath();

            final Cookie[] cookies = req.getCookies();
            boolean acceptsCookies = false;
            if (cookies != null) {
                final String theme = Arrays.stream(cookies).filter(c -> c.getName().equals("theme"))
                        .map(c -> c.getValue()).findAny().orElse("0");
                acceptsCookies = Arrays.stream(cookies).filter(c -> c.getName().equals("cookieconsent_dismissed"))
                        .anyMatch(c -> !"deny".equals(c.getValue()));
                req.setAttribute("lightTheme", "0".equals(theme));
                req.setAttribute("blackTheme", "2".equals(theme));
            } else {
                req.setAttribute("lightTheme", true);
                req.setAttribute("blackTheme", false);
            }

            HttpSession session;
            handleDetails(request, response, chain, req, res, requestURI, contextPath, acceptsCookies);
            session = ((HttpServletRequest) request).getSession(false);
            if (!acceptsCookies && session != null) {
                session.invalidate();
                session = null;
            }
            String authType = null;
            Long uid = null;
            if (session != null) {
                authType = (String) session.getAttribute("authType");
                uid = (Long) session.getAttribute("id");
            }
            if (authType == null) {
                authType = (String) request.getAttribute("authType");
                if (authType == null) {
                    authType = "none";
                }
            }
            if (uid == null) {
                uid = (Long) request.getAttribute("id");
                if (uid == null) {
                    uid = 0L;
                }
            }
            String url = req.getRequestURL().toString();
            if (url.length() > 100) {
                url = url.substring(0, 100);
            }
            Log.SERVLET_REQUESTS.log(
                    Level.FINE, req.getRemoteAddr() + " " + req.getMethod() + "-requested " + url + " with auth: "
                            + authType + " -> " + res.getStatus() + " (" + (System.currentTimeMillis() - start) + "ms)",
                    uid);
        } else {
            chain.doFilter(request, response);
            Log.SERVLET_REQUESTS.log(Level.FINE, "time for unknown request: " + (System.currentTimeMillis() - start));
        }
    }

    private void handleDetails(final ServletRequest request, final ServletResponse response, final FilterChain chain,
            final HttpServletRequest req, final HttpServletResponse res, final String requestURI,
            final String contextPath, final boolean acceptsCookies) throws IOException, ServletException {
        HttpSession session = req.getSession(false);
        request.setAttribute("cookies", acceptsCookies);
        if (!acceptsCookies) {
            final String errorRequest = request.getParameter("error");
            if (errorRequest != null) {
                final String translate = Variables.translate(errorRequest);
                if (!errorRequest.equals(translate)) {
                    request.setAttribute("error", translate);
                }
            }
            final String infoRequest = request.getParameter("info");
            if (infoRequest != null) {
                final String translate = Variables.translate(infoRequest);
                if (!infoRequest.equals(translate)) {
                    request.setAttribute("info", translate);
                }
            }
            if (session != null) {
                session.invalidate();
                session = null;
            }
            if (req.getCookies() != null) {
                Arrays.stream(req.getCookies()).forEach(c -> {
                    c.setMaxAge(0);
                    res.addCookie(c);
                });
            }
            if (!("GET".equals(req.getMethod()) || "HEAD".equals(req.getMethod()) || "OPTIONS".equals(req.getMethod()))
                    && !requestURI.contains("/api/")) {
                res.setStatus(HttpServletResponse.SC_SEE_OTHER);
                String concat = contextPath.concat("/login?error=requiresCookies");
                if (request.getParameter("r") != null) {
                    concat = concat.concat("&r=" + request.getParameter("r"));
                }
                if (request.getParameter("ref") != null) {
                    concat = concat.concat("&ref=" + request.getParameter("ref"));
                }
                res.setHeader("Location", concat);
                return;
            }
        }
        // user is not logged in
        if (session == null || session.getAttribute("id") == null || (long) session.getAttribute("id") == -1) {
            // user is not on the login page
            if (!checkUriException(requestURI, contextPath)) {
                if (acceptsCookies) {
                    session = req.getSession(true);
                    res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    session.setAttribute("error", Variables.translate("login!"));
                    session.setAttribute("source",
                            requestURI + (req.getQueryString() != null ? "?" + req.getQueryString() : ""));
                    res.sendRedirect(contextPath.concat("/login"));
                    return;
                } else {
                    res.sendRedirect(contextPath.concat("/login?error=login!"));
                    return;
                }
            } else {
                // user not logged in->no resourcechecks
                chain.doFilter(request, response);
                return;
            }
        } else {
            if ((session == null || session.getAttribute("admin") == null) && isAdminPage(requestURI, contextPath)) {
                res.sendError(HttpServletResponse.SC_NOT_FOUND, requestURI);
                return;
            } else {
                chain.doFilter(request, response);
                return;
            }
        }
    }

    private boolean isAdminPage(final String requestURI, final String contextPath) {
        final String request = requestURI.substring(contextPath.length());
        return request.matches(".*\\.jsp") || request.startsWith("/admin");
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(final FilterConfig fConfig) throws ServletException {
    }
}
