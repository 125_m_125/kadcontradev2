package de._125m125.ktv2.filter;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de._125m125.ktv2.Variables;

@WebFilter(filterName = "MaintainanceFilter", value = "*")
public class MaintainanceFilter implements Filter {

    private static AtomicReference<Settings> settings = new AtomicReference<>();
    private static AtomicBoolean             active   = new AtomicBoolean();

    /**
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(final FilterConfig arg0) throws ServletException {
    }

    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {
        if (!MaintainanceFilter.active.get()) {
            chain.doFilter(req, res);
            return;
        }
        if (req instanceof HttpServletRequest) {
            final HttpServletRequest request = (HttpServletRequest) req;
            final HttpSession session = request.getSession(false);
            final Boolean admin = session != null && Boolean.TRUE.equals(session.getAttribute("admin"));
            final Settings settings = MaintainanceFilter.settings.get();
            if (admin != null && admin && !settings.shouldBlockAdmins()) {
                request.setAttribute("info", Variables.translate("maintainanceAdmin"));
                chain.doFilter(req, res);
                return;
            }
            if (settings.isBlocked(request.getRequestURI().substring(request.getContextPath().length()))) {
                request.setAttribute("info", Variables.translate("maintainance"));
                request.setAttribute("error", Variables.translate("maintainanceError"));
                ((HttpServletResponse) res).setStatus(503);
                final RequestDispatcher rd = request.getRequestDispatcher("/empty.jsp");
                rd.forward(req, res);
                return;
            } else {
                request.setAttribute("info", Variables.translate("maintainance"));
                chain.doFilter(req, res);
                return;
            }
        }
        chain.doFilter(req, res);
        return;
    }

    public static class Settings {
        private final boolean blockAdmins;
        private final Pattern pattern;
        private final String  patternString;

        public Settings(final boolean blockAdmins, final String pattern) {
            this.blockAdmins = blockAdmins;
            this.pattern = Pattern.compile(pattern);
            this.patternString = pattern;
        }

        public boolean shouldBlockAdmins() {
            return this.blockAdmins;
        }

        public boolean isBlocked(final String path) {
            final Matcher m = this.pattern.matcher(path);
            return m.find();
        }

        public String getPatternString() {
            return this.patternString;
        }

        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (!(o instanceof Settings)) {
                return false;
            }
            final Settings s = (Settings) o;
            return this.blockAdmins == s.blockAdmins && this.patternString.equals(s.patternString);
        }

        @Override
        public String toString() {
            return "Settings:{blockAdmins:" + this.blockAdmins + ", patternString:" + this.patternString;
        }
    }

    public static synchronized void configure(final boolean active, final Settings settings) {
        MaintainanceFilter.active.set(active);
        MaintainanceFilter.settings.set(settings);
    }

    public static Settings getSettings() {
        return MaintainanceFilter.settings.get();
    }

    public static boolean isActive() {
        return MaintainanceFilter.active.get();
    }
}
