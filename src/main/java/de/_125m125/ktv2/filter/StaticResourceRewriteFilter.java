package de._125m125.ktv2.filter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.Level;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.hash.Hashing;
import com.google.common.io.Files;

import de._125m125.ktv2.logging.Log;

@WebFilter(filterName = "StaticResourceRewriteFilter", value = "/*")
public class StaticResourceRewriteFilter implements Filter {

    private String hash;
    private String build;

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        try (final InputStream is = filterConfig.getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF")) {
            if (is != null) {
                final Manifest mf = new Manifest();
                mf.read(is);
                final Attributes atts = mf.getMainAttributes();
                this.build = atts.getValue("Implementation-Version");
                if (this.build == null) {
                    Log.SYSTEM.log(Level.INFO, "No build information found in manifest");
                    this.build = "SNAPSHOT";
                }
                this.hash = atts.getValue("Implementation-Commit");
                if (this.hash == null || "UNKNOWN".equals(this.hash)) {
                    Log.SYSTEM.log(Level.INFO, "No hash information found in manifest");
                    this.hash = new BigInteger(130, new Random()).toString(32);
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
            if (this.build == null) {
                Log.SYSTEM.log(Level.WARNING, "Could not extract version from manifest");
                this.build = "SNAPSHOT";
            }
            if (this.hash == null) {
                Log.SYSTEM.log(Level.INFO, "No hash information found in manifest");
                this.hash = new BigInteger(130, new Random()).toString(32);
            }
        }

        final Map<String, String> jsVersions = new HashMap<>();
        final String[] folders = { filterConfig.getServletContext().getRealPath("/static/js2/"),
                filterConfig.getServletContext().getRealPath("/static/lib/"),
                filterConfig.getServletContext().getRealPath("/static/css2/") };
        for (final String foldername : folders) {
            findFileHashes(jsVersions, foldername, "");
        }
        System.out.println(jsVersions);
        Log.SYSTEM.log(Level.INFO, "Application version set to: " + this.build + ", hash set to " + this.hash);

        filterConfig.getServletContext().setAttribute("version", this.build);
        filterConfig.getServletContext().setAttribute("hash", this.hash);
        filterConfig.getServletContext().setAttribute("jsVersions", jsVersions);
    }

    private void findFileHashes(final Map<String, String> jsVersions, final String foldername, final String prepend) {
        final File folder = new File(foldername);
        if (folder.exists()) {
            for (final File f : folder.listFiles()) {
                if (f.isFile()) {
                    try {
                        jsVersions.put(prepend + f.getName(), Files.asByteSource(f).hash(Hashing.md5()).toString());
                    } catch (final IOException e) {
                        e.printStackTrace();
                    }
                } else if (f.isDirectory()) {
                    findFileHashes(jsVersions, f.getAbsolutePath(), prepend + f.getName() + "/");
                }
            }
        }
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {
        if (req instanceof HttpServletRequest) {
            final HttpServletRequest request = (HttpServletRequest) req;
            final String requestURI = request.getRequestURI();
            if (requestURI.startsWith("/static", request.getContextPath().length())) {
                final int index = requestURI.indexOf("/v/");
                final int endindex = requestURI.indexOf("/", index + 3);
                if (index != -1 && endindex != -1) {
                    final String before = requestURI.substring(request.getContextPath().length(), index);
                    final String after = requestURI.substring(endindex);
                    ((HttpServletResponse) res).setHeader("Vary", "Accept-Encoding");
                    req.getRequestDispatcher(before + after).forward(req, res);
                } else {
                    ((HttpServletResponse) res).setHeader("Vary", "Accept-Encoding");
                    chain.doFilter(req, res);
                }
            } else {
                chain.doFilter(req, res);
            }
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void destroy() {
    }

}
