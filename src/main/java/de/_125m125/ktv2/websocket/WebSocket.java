package de._125m125.ktv2.websocket;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.Clock;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.ThreadManager;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.calculators.CertificateManager.CertificateDetails;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.notifications.NotificationEvent;
import de._125m125.ktv2.notifications.NotificationObservable;
import de._125m125.ktv2.servlets.api.helper.APIAuthHelper;
import de._125m125.ktv2.servlets.api.helper.LoginResult;
import de._125m125.ktv2.servlets.api.helper.Permission;
import jersey.repackaged.com.google.common.base.Objects;

@ServerEndpoint(value = "/api/websocket", subprotocols = "ktProtocolV1.1",
        configurator = WebsocketClientCertificateExtractor.class)
public class WebSocket {
    private static final String                     CERTIFICATE       = "cert";
    private static final Clock                      clock             = Clock.systemDefaultZone();
    private static final SecureRandom               rnd               = new SecureRandom();

    private static final Set<Session>               unregistered      = new HashSet<>();
    private static final Map<String, Set<Session>>  sessions          = new HashMap<>();
    private static final ReadWriteLock              sessionsLock      = new ReentrantReadWriteLock();
    private static AtomicInteger                    nextCleanup       = new AtomicInteger(100);

    private static final Cache<String, Set<String>> wsidSubscriptions = CacheBuilder.newBuilder()
            .expireAfterAccess(1, TimeUnit.HOURS).build();
    private static final Map<Session, String>       wsids             = Collections.synchronizedMap(new HashMap<>());

    public static void initWebSocket(final NotificationObservable notificationObservable) {
        notificationObservable.addObserver(WebSocket::update);
    }

    private static synchronized void checkCleanup() {
        if (WebSocket.nextCleanup.decrementAndGet() == 0) {
            new Thread(WebSocket::cleanup).start();
        }
    }

    private static void cleanup() {
        System.out.println("WebSocket.cleanup()");
        Log.WEBSOCKET.log(Level.FINE, "Websocket cleanup started");
        WebSocket.sessionsLock.writeLock().lock();
        try {
            final Iterator<Entry<String, Set<Session>>> iterator = WebSocket.sessions.entrySet().iterator();
            while (iterator.hasNext()) {
                final Entry<String, Set<Session>> next = iterator.next();
                final Set<Session> userSessions = next.getValue();
                userSessions.forEach((session) -> {
                    if (!session.isOpen()) {
                        closeAndRemoveSession(session, false);
                    }
                });
                if (userSessions.isEmpty()) {
                    iterator.remove();
                }
            }
        } finally {
            WebSocket.sessionsLock.writeLock().unlock();
        }
        System.out.println("WebSocket.cleanup()2");

        synchronized (WebSocket.unregistered) {
            System.out.println("WebSocket.cleanup()2.25");
            WebSocket.unregistered.forEach((session) -> {
                System.out.println(session);
                try {
                    session.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
                System.out.println(session + " 2");
            });
            System.out.println("WebSocket.cleanup()2.5");
            WebSocket.unregistered.clear();
            System.out.println("WebSocket.cleanup()2.75");
        }
        System.out.println("WebSocket.cleanup()3");
        WebSocket.wsids.entrySet().removeIf(e -> !e.getKey().isOpen());
        System.out.println("WebSocket.cleanup()4");
        Log.WEBSOCKET.log(Level.FINE, "Websocket cleanup finished");
        WebSocket.nextCleanup.set(100);
    }

    @OnOpen
    public void onOpen(final Session session) {
        checkCleanup();
        session.getUserProperties().put(WebSocket.CERTIFICATE, WebsocketClientCertificateExtractor.INSTANCE.get());
        System.out.println(session.getUserProperties().get(WebSocket.CERTIFICATE));
        WebsocketClientCertificateExtractor.INSTANCE.set(null);
        synchronized (WebSocket.unregistered) {
            WebSocket.unregistered.add(session);
        }
    }

    @OnClose
    public void onClose(final Session session) {
        checkCleanup();
        closeAndRemoveSession(session, true);
    }

    @SuppressWarnings("unchecked")
    @OnMessage
    public void onMessage(final Session session, final String message) {
        checkCleanup();
        final Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        Map<String, Object> request;
        final Map<String, Object> response = new HashMap<>();
        try {
            request = Variables.GSON.fromJson(message, type);
        } catch (final JsonSyntaxException e) {
            send(session, "{\"error\":\"invalidJson\",\"errorSource\":\"" + e.getMessage() + "\"}");
            return;
        }
        if (request.containsKey("rid")) {
            response.put("rid", request.get("rid"));
        }
        boolean found = false;
        if (request.containsKey("ping")) {
            response.put("pong", WebSocket.clock.millis());
            found = true;
        }
        if (request.containsKey("subscribe")) {
            final Object subscribeObject = request.get("subscribe");
            subscribe(session, subscribeObject, response);
            found = true;
        }
        if (request.containsKey("session")) {
            final Object o = request.get("session");
            if (o instanceof Map) {
                onSessionRequest(session, (Map<String, Object>) o, response);
            }
            found = true;
        }
        if (!found) {
            response.put("error", "unknownRequest");
        }
        send(session, Variables.GSON.toJson(response));
    }

    private void onSessionRequest(final Session session, final Map<String, Object> value,
            final Map<String, Object> response) {
        final Object request = value.get("request");
        if ("start".equals(request) || "status".equals(request)) {
            final String sessid = WebSocket.wsids.computeIfAbsent(session,
                    s -> Token.getBase32(new BigInteger(128, WebSocket.rnd)));
            Set<String> subs;
            synchronized (WebSocket.wsidSubscriptions) {
                subs = WebSocket.wsidSubscriptions.getIfPresent(sessid);
            }
            subs = Objects.firstNonNull(subs, new HashSet<>());
            final HashMap<Object, Object> hashMap = new HashMap<>();
            hashMap.put("id", sessid);
            hashMap.put("subscriptions", subs);
            response.put("session", hashMap);
            return;
        } else if ("resume".equals(request)) {
            final Object requestedId = value.get("id");
            if (requestedId == null || !(requestedId instanceof String)) {
                response.put("error", "missingSessionId");
                return;
            }
            final String currid = WebSocket.wsids.computeIfAbsent(session, s -> (String) requestedId);
            if (currid != null && !currid.equals(requestedId)) {
                response.put("error", "sessionAlreadyStarted");
                return;
            }
            final Set<String> subs = WebSocket.wsidSubscriptions.getIfPresent(requestedId);
            if (subs == null) {
                response.put("error", "unknownSessionId");
                return;
            }
            synchronized (subs) {
                WebSocket.sessionsLock.writeLock().lock();
                try {
                    subs.stream().map(channel -> WebSocket.sessions.computeIfAbsent(channel, n -> new HashSet<>()))
                            .forEach(s -> s.add(session));
                } finally {
                    WebSocket.sessionsLock.writeLock().unlock();
                }
            }
            synchronized (WebSocket.unregistered) {
                WebSocket.unregistered.remove(session);
            }
            final HashMap<Object, Object> hashMap = new HashMap<>();
            hashMap.put("id", requestedId);
            hashMap.put("subscriptions", subs);
            response.put("session", hashMap);
            return;
        }
    }

    private void subscribe(final Session session, final Object subscribeObject, final Map<String, Object> response) {
        if (!(subscribeObject instanceof Map)) {
            response.put("error", "invalidSubscribeRequest");
            return;
        }
        final Map<String, Object> subscribeData = (Map<String, Object>) subscribeObject;
        final Object channel = subscribeData.get("channel");
        if (channel == null || !(channel instanceof String)) {
            response.put("error", "invalidChannel");
            return;
        }
        if (channel.equals("orderbook") || channel.equals("history")) {
            if (subscribeData.get("item") != null && subscribeData.get("item") instanceof String) {
                final String item = (String) subscribeData.get("item");
                if (!Variables.resourceExistsOrIsMoney(item)) {
                    response.put("error", "unknownItem");
                    return;
                }
                verifiedSubscribe(session, (String) channel + "-" + item);
                response.put("error", false);
                return;
            } else {
                verifiedSubscribe(session, (String) channel);
                response.put("error", false);
                return;
            }
        }
        final Object uid = subscribeData.get("uid");
        final Object tid = subscribeData.get("tid");
        final Object tkn = subscribeData.get("tkn");
        if (uid == null || !(uid instanceof String)) {
            response.put("error", "missingUid");
            return;
        }
        final long id = Token.getBigInt((String) uid).longValue();
        final int permissions;
        final String authType;
        if (!(tid == null || tkn == null || !(tid instanceof String && tkn instanceof String))) {
            final LoginResult login = APIAuthHelper.login((String) uid, (String) tid, (String) tkn);
            if (id != login.getUid()) {
                response.put("error", "invalidCredentials");
                return;
            }
            permissions = login.getPermissions();
            authType = "tkn" + tid;
        } else {
            final CertificateDetails certificateDetails = (CertificateDetails) session.getUserProperties()
                    .get(WebSocket.CERTIFICATE);
            if (certificateDetails.getUser() == id) {
                permissions = certificateDetails.getPermissions();
                authType = "cert" + certificateDetails.getSerial().mod(BigInteger.valueOf(8192));
            } else {
                response.put("error", "invalidAuthFormat");
                return;
            }
        }
        final String subscribeResult = subscribe(session, (String) channel, id, authType, permissions);
        if (subscribeResult == null) {
            response.put("error", false);
            return;
        } else {
            response.put("error", subscribeResult);
            return;
        }
    }

    private String subscribe(final Session session, final String channel, final long uid, final String authType,
            final int permissions) {
        final String stringUid = String.valueOf(uid);
        final Permission p = Permission.of(channel);
        if (p == null) {
            Log.WEBSOCKET.log(Level.INFO, uid, "Websocket ? failed subscription(invalid Channel): ?:?/?",
                    session.getId(), stringUid, authType);
            return "invalidChannel";
        }

        if (uid == 0) {
            Log.WEBSOCKET.log(Level.INFO, 0, "Websocket ? failed subscription(invalid credentials): ?:?/?",
                    session.getId(), channel, stringUid, authType);
            return "invalidCredentials";
        }
        if (!p.isPresentIn(permissions)) {
            Log.WEBSOCKET.log(Level.INFO, uid, "Websocket ? failed subscription(insufficient Permissions): ?:?/?",
                    session.getId(), channel, stringUid, authType);
            return "insufficientPermissions";
        }
        final String fullChannelname = Token.getBase32(BigInteger.valueOf(uid)) + "-" + channel;
        Log.WEBSOCKET.log(Level.INFO, uid, "Websocket ? subscribed to ?:?/?", session.getId(), channel, stringUid,
                authType);
        verifiedSubscribe(session, fullChannelname);
        return null;
    }

    private void verifiedSubscribe(final Session session, final String channel) {
        WebSocket.sessionsLock.writeLock().lock();
        try {
            Set<Session> userSessions = WebSocket.sessions.get(channel);
            if (userSessions == null) {
                userSessions = new HashSet<>();
                WebSocket.sessions.put(channel, userSessions);
            }
            userSessions.add(session);
        } finally {
            WebSocket.sessionsLock.writeLock().unlock();
        }
        synchronized (WebSocket.unregistered) {
            WebSocket.unregistered.remove(session);
        }
        final String id = WebSocket.wsids.get(session);
        if (id != null) {
            try {
                final Set<String> subs = WebSocket.wsidSubscriptions.get(id, () -> new HashSet<>());
                synchronized (subs) {
                    subs.add(channel);
                }
            } catch (final ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    @OnError
    public void onError(final Throwable throwable) {
    }

    private static boolean send(final Session session, final String message) {
        if (!session.isOpen()) {
            closeAndRemoveSession(session, true);
            return false;
        }
        Log.WEBSOCKET.log(Level.INFO, 0, "Sending message to Websocket ?: ?", session.getId(),
                message.substring(0, Math.min(50, message.length())));
        try {
            session.getBasicRemote().sendText(message);
            return true;
        } catch (final IOException e) {
            closeAndRemoveSession(session, true);
        }
        return false;
    }

    public static void push(final String channelName, final String message) {
        WebSocket.sessionsLock.readLock().lock();
        try {
            final Set<Session> userSessions = WebSocket.sessions.get(channelName);
            if (userSessions != null) {
                userSessions.forEach((session) -> {
                    send(session, message);
                });
            }
        } finally {
            WebSocket.sessionsLock.readLock().unlock();
        }
    }

    public static void push(final String message) {
        WebSocket.sessionsLock.readLock().lock();
        try {
            WebSocket.sessions.forEach((uid, userSessions) -> {
                userSessions.forEach((session) -> {
                    send(session, message);
                });
            });
        } finally {
            WebSocket.sessionsLock.readLock().unlock();
        }
    }

    private static void closeAndRemoveSession(final Session session, final boolean removeOnEmpty) {
        try {
            if (session.isOpen()) {
                session.close();
            }
        } catch (final Exception e) {
        }
        synchronized (WebSocket.unregistered) {
            WebSocket.unregistered.remove(session);
        }
        WebSocket.wsids.remove(session);
        checkCleanup();
        Log.WEBSOCKET.log(Level.INFO, 0, "Websocket ? closed", session.getId());
    }

    public static void update(final Observable o, final Object event) {
        if (!o.equals(Main.instance().getNotificationObservable()) || !(event instanceof NotificationEvent)) {
            return;
        }
        final NotificationEvent e = (NotificationEvent) event;
        if (e.getPermission() == Permission.NO_PERMISSIONS) {
            pushAsync(e, e.getDetails().get("source"));
            pushAsync(e, e.getDetails().get("source") + "-" + e.getDetails().get("key"));
        } else {
            pushAsync(e, e.getBase32Uid() + "-" + e.getPermission().getComName());
        }
    }

    private static void pushAsync(final NotificationEvent e, final String channelName) {
        ThreadManager.instance().submit(() -> {
            e.getContents();
            push(channelName, Variables.GSON.toJson(e));
        });
    }
}
