package de._125m125.ktv2.websocket;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import javax.websocket.server.ServerEndpointConfig.Configurator;

import de._125m125.ktv2.calculators.CertificateManager;
import de._125m125.ktv2.calculators.CertificateManager.CertificateDetails;

public class WebsocketClientCertificateExtractor extends Configurator {
    public static final ThreadLocal<CertificateDetails> INSTANCE = new ThreadLocal<>();

    @Override
    public void modifyHandshake(final ServerEndpointConfig sec, final HandshakeRequest request,
            final HandshakeResponse response) {
        final HttpServletRequest servletRequest = getField(request, HttpServletRequest.class);
        final CertificateDetails certificateDetails = CertificateManager.instance()
                .getCertificateDetails(servletRequest);
        WebsocketClientCertificateExtractor.INSTANCE.set(certificateDetails);
        super.modifyHandshake(sec, request, response);
    }

    @SuppressWarnings("unchecked")
    private static <I, F> F getField(final I instance, final Class<F> fieldType) {
        try {
            for (Class<?> type = instance.getClass(); type != Object.class; type = type
                    .getSuperclass()) {
                for (final Field field : type.getDeclaredFields()) {
                    if (fieldType.isAssignableFrom(field.getType())) {
                        field.setAccessible(true);
                        return (F) field.get(instance);
                    }
                }
            }
        } catch (final Exception e) {
            // Handle?
        }

        return null;
    }
}
