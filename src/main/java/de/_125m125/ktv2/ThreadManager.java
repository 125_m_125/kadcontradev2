package de._125m125.ktv2;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebListener;

import com.google.common.io.Files;

import de._125m125.ktv2.updates.UpdateManager;

@WebListener
public class ThreadManager implements ServletContextListener {

    private static ThreadManager tm;
    public static ServletContext context;

    public static ThreadManager instance() {
        return ThreadManager.tm;
    }

    private ScheduledExecutorService scheduledExecutorService;
    private List<ScheduledFuture<?>> tasks = new ArrayList<>();

    public ScheduledExecutorService getScheduledExecutorService() {
        return this.scheduledExecutorService;
    }

    public synchronized ScheduledFuture<?> schedule(final Callable<?> r, final long delay,
            final TimeUnit timeUnit) {
        final ScheduledFuture<?> task = this.scheduledExecutorService.schedule(r, delay, timeUnit);
        this.tasks = this.tasks.stream().filter((t) -> !t.isDone()).collect(Collectors.toList());
        this.tasks.add(task);
        return task;
    }

    public synchronized ScheduledFuture<?> schedule(final Runnable r, final long delay,
            final TimeUnit timeUnit) {
        final ScheduledFuture<?> task = this.scheduledExecutorService.schedule(r, delay, timeUnit);
        this.tasks = this.tasks.stream().filter((t) -> !t.isDone()).collect(Collectors.toList());
        this.tasks.add(task);
        return task;
    }

    public synchronized ScheduledFuture<?> scheduleAtFixedRate(final Runnable r,
            final long initialDelay, final long period, final TimeUnit unit) {
        final ScheduledFuture<?> task =
                this.scheduledExecutorService.scheduleAtFixedRate(r, initialDelay, period, unit);
        this.tasks = this.tasks.stream().filter((t) -> !t.isDone()).collect(Collectors.toList());
        this.tasks.add(task);
        return task;
    }

    public Future<?> submit(final Runnable r) {
        return this.scheduledExecutorService.submit(r);
    }

    @Override
    public synchronized void contextInitialized(final ServletContextEvent event) {
        ThreadManager.context = event.getServletContext();
        final ScheduledThreadPoolExecutor poolExecutor = new ScheduledThreadPoolExecutor(1);
        poolExecutor.setRemoveOnCancelPolicy(true);
        this.scheduledExecutorService =
                Executors.unconfigurableScheduledExecutorService(poolExecutor);
        if (ThreadManager.tm != null) {
            ThreadManager.tm.contextDestroyed(event);
        }
        ThreadManager.tm = this;

        try {
            new Main();
            new UpdateManager(Main.instance().getConnector());
        } catch (final UnavailableException e) {
            throw new RuntimeException(e);
        }
        event.getServletContext().setAttribute("allItems", Variables.allItems);
        event.getServletContext().setAttribute("appName", Variables.getAppName());
        final File file = new File("/home/kadcontrade/additionalScripts.txt");
        if (file.exists()) {
            try {
                final String additionalScripts = Files.toString(file, Charset.forName("UTF-8"));
                event.getServletContext().setAttribute("additionalScripts", additionalScripts);
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void contextDestroyed(final ServletContextEvent event) {
        ThreadManager.context = null;
        this.scheduledExecutorService.shutdown();
        for (final ScheduledFuture<?> future : this.tasks) {
            if (!future.isDone()) {
                future.cancel(false);
            }
        }
        try {
            this.scheduledExecutorService.awaitTermination(10, TimeUnit.SECONDS);
        } catch (final InterruptedException e) {
            this.scheduledExecutorService.shutdownNow();
            try {
                this.scheduledExecutorService.awaitTermination(10, TimeUnit.SECONDS);
            } catch (final InterruptedException e1) {
            }
        }
        Main.instance().close();
    }

}
