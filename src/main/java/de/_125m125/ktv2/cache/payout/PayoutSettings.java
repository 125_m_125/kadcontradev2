package de._125m125.ktv2.cache.payout;

import de._125m125.ktv2.beans.Payoutbox;

public class PayoutSettings {
    private final long        uid;
    private final int         server;
    private final int         x;
    private final int         y;
    private final int         z;
    private final String      warp;
    private final Payoutbox[] payoutboxes = new Payoutbox[3];

    public PayoutSettings(final long uid, final int server, final int x, final int y, final int z, final String warp) {
        this.uid = uid;
        this.server = server;
        this.x = x;
        this.y = y;
        this.z = z;
        this.warp = warp;
    }

    public PayoutSettings(final long uid) {
        this.uid = uid;
        this.server = 0;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.warp = "spawn";
    }

    public long getUid() {
        return this.uid;
    }

    public int getServer() {
        return this.server;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getZ() {
        return this.z;
    }

    public String getWarp() {
        return this.warp;
    }

    public void addBox(final Payoutbox box) {
        this.payoutboxes[box.getServer() - 1] = box;
    }

    public Payoutbox[] getPayoutboxes() {
        return this.payoutboxes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.server;
        result = prime * result + (int) (this.uid ^ (this.uid >>> 32));
        result = prime * result + ((this.warp == null) ? 0 : this.warp.hashCode());
        result = prime * result + this.x;
        result = prime * result + this.y;
        result = prime * result + this.z;
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PayoutSettings other = (PayoutSettings) obj;
        if (this.server != other.server) {
            return false;
        }
        if (this.uid != other.uid) {
            return false;
        }
        if (this.warp == null) {
            if (other.warp != null) {
                return false;
            }
        } else if (!this.warp.equals(other.warp)) {
            return false;
        }
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        if (this.z != other.z) {
            return false;
        }
        return true;
    }

}
