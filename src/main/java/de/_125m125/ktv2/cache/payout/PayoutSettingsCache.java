package de._125m125.ktv2.cache.payout;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Payoutbox;

public class PayoutSettingsCache {
    private static final class PayoutCacheLoader extends CacheLoader<Long, PayoutSettings> {
        @Override
        public PayoutSettings load(final Long id) throws Exception {
            final ArrayList<HashMap<String, Object>> payouts = Main.instance().getConnector()
                    .executeQuery("SELECT * FROM payoutsettings WHERE uid=" + id);
            if (payouts == null || payouts.size() == 0) {
                return new PayoutSettings(id);
            }
            final HashMap<String, Object> entry = payouts.get(0);
            final PayoutSettings s = new PayoutSettings((long) entry.get("uid"), (int) entry.get("server"),
                    (int) entry.get("x"), (int) entry.get("y"), (int) entry.get("z"), (String) entry.get("warp"));
            try (final Connection c = Main.instance().getConnector().getConnection();
                    PreparedStatement st = c.prepareStatement("SELECT * FROM payoutboxes WHERE owner=?")) {
                st.setLong(1, id);
                final ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    s.addBox(new Payoutbox(rs.getLong("cid"), rs.getInt("server"), rs.getInt("x"), rs.getInt("y"),
                            rs.getInt("z"), rs.getLong("owner"), null, rs.getBoolean("verified")));
                }
            }
            return s;
        }
    }

    private final LoadingCache<Long, PayoutSettings> cache = CacheBuilder.newBuilder().maximumSize(50)
            .build(new PayoutCacheLoader());

    public PayoutSettings get(final Long i) {
        try {
            return this.cache.get(i);
        } catch (final ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void invalidate(final Long integerUid) {
        this.cache.invalidate(integerUid);
    }
}
