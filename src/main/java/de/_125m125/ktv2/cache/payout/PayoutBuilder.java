package de._125m125.ktv2.cache.payout;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.common.cache.CacheLoader;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Payout;
import de._125m125.ktv2.beans.Payout.PAYOUT_STATE;
import de._125m125.ktv2.beans.PayoutList;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.servlets.api.keys.PayoutKey;

public class PayoutBuilder extends CacheLoader<PayoutKey, PayoutList> {

    private static final String SELECT_SINGLE_PAYOUT    = "SELECT * FROM payout WHERE uid=? and id=?";
    private static final String SELECT_INTERACTION_ONLY = "SELECT * FROM payout WHERE uid=? AND userInteraction=TRUE ORDER BY adminInteractionCompleted DESC,date DESC LIMIT ? OFFSET ?";
    private static final String SELECT_ALL              = "SELECT * FROM payout WHERE uid=? ORDER BY userInteraction DESC,requiresAdminInteraction DESC,date DESC LIMIT ? OFFSET ?";

    @Override
    public PayoutList load(final PayoutKey key) throws Exception {
        final PayoutList payoutList = new PayoutList();
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(
                        key.isInteractionOnly() ? PayoutBuilder.SELECT_INTERACTION_ONLY
                                : PayoutBuilder.SELECT_ALL)) {
            ps.setLong(1, key.getUid());
            ps.setInt(2, key.getLimit());
            ps.setInt(3, key.getOffset());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    payoutList.addEntry(createPayoutFromCurrentRow(rs));
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }

        return payoutList;
    }

    public Payout loadSingle(final long uid, final long payoutId) {
        try (Connection c = Main.instance().getConnector().getConnection()) {
            try (PreparedStatement st = c.prepareStatement(PayoutBuilder.SELECT_SINGLE_PAYOUT)) {
                st.setLong(1, uid);
                st.setLong(2, payoutId);
                try (ResultSet rs = st.executeQuery()) {
                    if (rs.next()) {
                        return createPayoutFromCurrentRow(rs);
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Payout createPayoutFromHashMap(final HashMap<String, Object> hashMap) {
        final PAYOUT_STATE payoutState = PAYOUT_STATE.of((boolean) hashMap.get("userInteraction"),
                (boolean) hashMap.get("success"),
                (boolean) hashMap.get("adminInteractionCompleted"),
                (boolean) hashMap.get("requiresAdminInteraction"),
                (boolean) hashMap.get("unknown"));
        return new Payout((long) hashMap.get("id"), (long) hashMap.get("uid"),
                (String) hashMap.get("matid"), (long) hashMap.get("amount"), payoutState,
                (String) hashMap.get("payoutType"), (Timestamp) hashMap.get("date"),
                (String) hashMap.get("message"));
    }

    public static Payout createPayoutFromCurrentRow(final ResultSet rs) throws SQLException {
        final PAYOUT_STATE payoutState = PAYOUT_STATE.of(rs.getBoolean("userInteraction"),
                rs.getBoolean("success"), rs.getBoolean("adminInteractionCompleted"),
                rs.getBoolean("requiresAdminInteraction"), rs.getBoolean("unknown"));
        return new Payout(rs.getLong("id"), rs.getLong("uid"),
                rs.getBoolean("type") ? ITEM_OR_STOCK.ITEM : ITEM_OR_STOCK.STOCK,
                rs.getString("matid"), rs.getLong("amount"), payoutState,
                rs.getString("payoutType"), rs.getTimestamp("date"), rs.getString("message"));
    }

    public static List<Payout> createPayoutListFromResultSet(final ResultSet rs)
            throws SQLException {
        final List<Payout> result = new ArrayList<>();
        while (rs.next()) {
            result.add(createPayoutFromCurrentRow(rs));
        }
        return result;
    }

}
