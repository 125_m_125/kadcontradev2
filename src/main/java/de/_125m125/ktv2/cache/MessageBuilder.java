package de._125m125.ktv2.cache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.common.cache.CacheLoader;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.MessageList;
import de._125m125.ktv2.servlets.api.keys.MessageKey;

public class MessageBuilder extends CacheLoader<MessageKey, MessageList> {

    @Override
    public MessageList load(final MessageKey mk) throws Exception {
        try (Connection c = Main.instance().getConnector().getConnection();
                final PreparedStatement preparedStatement = c.prepareStatement(
                        "SELECT time,message FROM messages WHERE uid=? ORDER BY id desc LIMIT ? OFFSET ?")) {
            preparedStatement.setLong(1, mk.getUid());
            preparedStatement.setInt(2, mk.getLimit());
            preparedStatement.setInt(3, mk.getOffset());
            try (final ResultSet rs = preparedStatement.executeQuery()) {
                return MessageList.of(rs);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return new MessageList();
    }

}
