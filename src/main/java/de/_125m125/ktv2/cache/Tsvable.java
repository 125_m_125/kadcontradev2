package de._125m125.ktv2.cache;

/**
 * Extended by Tsvable objects.
 */
public abstract class Tsvable {

    /** true if a change has been made since last build. */
    private transient boolean change = true;

    /** The tsv string. */
    private transient String  tsvString;

    /** The e tag. */
    private transient String  eTag;

    /** The tsv header. */
    private final String      TSV_HEADER;

    /**
     * Instantiates a new tsvable.
     *
     * @param header
     *            the header for the tsv string
     */
    public Tsvable(final String header) {
        this.TSV_HEADER = header;

    }

    /**
     * Gets the tsv string.
     *
     * @param header
     *            true if the header should be added to the result
     * @return the tsv string
     */
    public synchronized String getTsvString(final boolean header) {
        if (this.change) {
            refresh();
        }
        if (header) {
            return this.getTsvHeader() + "\r\n" + this.tsvString;
        } else {
            return this.tsvString;
        }
    }

    /**
     * Gets the e tag.
     *
     * @return the e tag
     */
    public synchronized String getETag() {
        if (this.change) {
            refresh();
        }
        return this.eTag;
    }

    /**
     * Rebuild the tsvString and the eTag.
     */
    private void refresh() {
        this.tsvString = buildTsvString();
        this.eTag = "W/\"" + this.tsvString.hashCode() + "\"";
    }

    /**
     * Invalidate previous builds. Should be called if the underlying Object
     * changed
     *
     * @return true, if an up to date build existed before
     */
    protected synchronized boolean invalidate() {
        if (this.change) {
            return false;
        }
        this.change = true;
        return true;
    }

    /**
     * Gets the last modified time.
     *
     * @return the last modified time
     */
    public abstract long getLastModified();

    /**
     * Builds the tsv string.
     *
     * @return the string
     */
    protected abstract String buildTsvString();

    public String getTsvHeader() {
        return TSV_HEADER;
    }

}
