package de._125m125.ktv2.cache.stats;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.common.cache.CacheLoader;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.OrderBook;
import de._125m125.ktv2.beans.OrderBookEntry;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.servlets.api.keys.OrderBookKey;
import de._125m125.ktv2.servlets.api.keys.OrderBookKey.MODE;

public class OrderBookBuilder extends CacheLoader<OrderBookKey, OrderBook> {

    @Override
    public OrderBook load(final OrderBookKey key) throws Exception {
        final OrderBook orderList = new OrderBook();

        final String material = key.getMaterial();
        if (!Variables.resourceExists(material)) {
            return orderList;
        }
        final int limit = key.getLimit();
        final boolean summarize = key.isSummarize();
        if (key.getMode() != MODE.SELL_ONLY) {
            final ArrayList<HashMap<String, Object>> buy =
                    Main.instance().getConnector().executeQuery(
                            "SELECT CAST(SUM(amount-sold) AS UNSIGNED) as amount,price FROM trade WHERE buysell=1 AND type=1 AND material=? AND amount!=sold GROUP by price ORDER BY price DESC LIMIT "
                                    + limit,
                            material);
            if (buy != null && buy.size() > 0) {
                for (final HashMap<String, Object> k : buy) {
                    final OrderBookEntry order = new OrderBookEntry();
                    order.setType("buy");
                    order.setPrice(IDSConverter.longToDoubleString((long) k.get("price")));
                    order.setAmount(((BigInteger) k.get("amount")).longValue());
                    orderList.addEntry(0, order);
                }
                if (summarize) {
                    final long min = (long) buy.get(buy.size() - 1).get("price");
                    final ArrayList<HashMap<String, Object>> buyGB =
                            Main.instance().getConnector().executeQuery(
                                    "SELECT CAST(SUM(amount-sold) AS UNSIGNED) as amount FROM trade WHERE buysell=1 AND type=1 AND material=? AND amount!=sold AND price<?",
                                    material, String.valueOf(min));
                    final HashMap<String, Object> k;
                    if (buyGB != null && buyGB.size() == 1
                            && (k = buyGB.get(0)).get("amount") != null) {
                        final OrderBookEntry order = new OrderBookEntry();
                        order.setType("buy");
                        order.setPrice("<" + IDSConverter.longToDoubleString(min));
                        order.setAmount(((BigInteger) k.get("amount")).longValue());
                        orderList.addEntry(0, order);
                    }
                }
            }
        }
        if (key.getMode() != MODE.BUY_ONLY) {
            final ArrayList<HashMap<String, Object>> sell =
                    Main.instance().getConnector().executeQuery(
                            "SELECT CAST(SUM(amount-sold) AS UNSIGNED) as amount,price FROM trade WHERE buysell=0 AND type=1 AND material=? AND amount!=sold GROUP by price ORDER BY price ASC LIMIT "
                                    + limit,
                            material);
            if (sell != null && sell.size() > 0) {
                for (final HashMap<String, Object> k : sell) {
                    final OrderBookEntry order = new OrderBookEntry();
                    order.setType("sell");
                    order.setPrice(IDSConverter.longToDoubleString((long) k.get("price")));
                    order.setAmount(((BigInteger) k.get("amount")).longValue());
                    orderList.addEntry(order);
                }
                if (summarize) {
                    final long max = (long) sell.get(sell.size() - 1).get("price");
                    final ArrayList<HashMap<String, Object>> sellGB =
                            Main.instance().getConnector().executeQuery(
                                    "SELECT CAST(SUM(amount-sold) AS UNSIGNED) as amount FROM trade WHERE buysell=0 AND type=1 AND material=? AND amount!=sold AND price>?",
                                    material, String.valueOf(max));
                    final HashMap<String, Object> k;
                    if (sellGB != null && sellGB.size() == 1
                            && (k = sellGB.get(0)).get("amount") != null) {
                        final OrderBookEntry order = new OrderBookEntry();
                        order.setType("sell");
                        order.setPrice(">" + IDSConverter.longToDoubleString(max));
                        order.setAmount(((BigInteger) k.get("amount")).longValue());
                        orderList.addEntry(order);
                    }
                }
            }
        }

        return orderList;
    }

}
