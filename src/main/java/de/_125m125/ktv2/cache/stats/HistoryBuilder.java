package de._125m125.ktv2.cache.stats;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.common.cache.CacheLoader;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.History;
import de._125m125.ktv2.beans.HistoryEntry;
import de._125m125.ktv2.servlets.api.keys.HistoryKey;

public class HistoryBuilder extends CacheLoader<HistoryKey, History> {

    @Override
    public History load(final HistoryKey key) throws Exception {
        final History h = new History();
        final String material = key.getMaterial();
        final int limit = key.getLimit();
        final int offset = key.getOffset();

        if (!Variables.resourceExists(material) && !"-1".equals(material)) {
            return h;
        }
        // TODO limit and offset as queryParameter
        final ArrayList<HashMap<String, Object>> executeQuery = Main.instance().getConnector().executeQuery(
                "SELECT open,value,min,max,amount,total, DATE_SUB(time, INTERVAL 1 DAY) as time FROM history LEFT JOIN minmax USING(time,type,material) LEFT JOIN (SELECT (time + INTERVAL 1 DAY) AS time,type,material,value as open FROM history) AS open USING(time,type,material) WHERE type=1 AND material=? ORDER BY time DESC LIMIT "
                        + limit + " OFFSET " + offset,
                material);
        if (executeQuery != null) {
            for (final HashMap<String, Object> k : executeQuery) {
                final HistoryEntry he = new HistoryEntry((Long) k.get("open"), (Long) k.get("value"),
                        (Long) k.get("min"), (Long) k.get("max"), (long) k.get("amount"), (long) k.get("total"),
                        (Date) k.get("time"));
                h.addEntry(he);
            }
        }
        return h;
    }

}
