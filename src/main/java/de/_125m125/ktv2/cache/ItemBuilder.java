package de._125m125.ktv2.cache;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.common.cache.CacheLoader;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.ItemList;

public class ItemBuilder extends CacheLoader<String, ItemList> {

    @Override
    public ItemList load(final String id) throws Exception {
        final ItemList il = new ItemList();
        final ArrayList<HashMap<String, Object>> k = Main.instance().getConnector().executeQuery(
                "SELECT matid,amount FROM materials WHERE id=? AND type=TRUE ORDER BY matid", id);
        final HashMap<String, Long> items;
        if (k != null) {
            items = new HashMap<>(k.size());
            for (final HashMap<String, Object> item : k) {
                items.put((String) item.get("matid"), (long) item.get("amount"));
            }
        } else {
            items = new HashMap<>();
        }
        for (final String[] allItem : Variables.allItems) {
            final Item item = new Item();
            final String current = allItem[0];
            final Long amount = items.getOrDefault(current, 0L);
            item.setId(current);
            item.setAmount(amount);
            il.addEntry(item);
        }
        return il;
    }

}
