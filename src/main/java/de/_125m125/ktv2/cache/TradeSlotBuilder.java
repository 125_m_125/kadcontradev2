package de._125m125.ktv2.cache;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.common.cache.CacheLoader;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.TradeSlot;
import de._125m125.ktv2.beans.TradeSlotList;
import de._125m125.ktv2.calculators.IDSConverter;

public class TradeSlotBuilder extends CacheLoader<String, TradeSlotList> {

    @Override
    public TradeSlotList load(final String key) throws Exception {
        final TradeSlotList ret = new TradeSlotList();

        final ArrayList<HashMap<String, Object>> executeQuery =
                Main.instance().getConnector().executeQuery(
                        "SELECT * FROM trade WHERE traderid=? ORDER BY time", String.valueOf(key));
        if (executeQuery != null) {
            for (final HashMap<String, Object> hm : executeQuery) {
                final TradeSlot s = build(hm);
                ret.addEntry(s);
            }
        }
        return ret;
    }

    public TradeSlot build(final HashMap<String, Object> hm) {
        final TradeSlot s = new TradeSlot();
        s.setActive(true);
        s.setBuy((boolean) hm.get("buysell"));
        s.setCurrent((long) hm.get("sold"));
        s.setItem(new Item((String) hm.get("material")));
        s.setMax((long) hm.get("amount"));
        s.setPrice(IDSConverter.longToMachineDoubleString((long) hm.get("price")));
        s.setTradeid((long) hm.get("id"));
        return s;
    }

}
