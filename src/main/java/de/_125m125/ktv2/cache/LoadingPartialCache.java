package de._125m125.ktv2.cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.UncheckedExecutionException;

public class LoadingPartialCache<K, V> implements LoadingCache<K, V> {
    private final LoadingCache<K, V>        cache;
    private final Predicate<? super K>      predicate;
    private final CacheLoader<? super K, V> loader;

    public LoadingPartialCache(final LoadingCache<K, V> cache, final CacheLoader<? super K, V> loader,
            final Predicate<? super K> predicate) {
        this.cache = cache;
        this.loader = loader;
        this.predicate = predicate;
    }

    @Override
    public V getIfPresent(final Object key) {
        return this.cache.getIfPresent(key);
    }

    @Override
    public V get(final K key, final Callable<? extends V> valueLoader) throws ExecutionException {
        if (this.predicate.test(key)) { return this.cache.get(key, valueLoader); }
        try {
            return valueLoader.call();
        } catch (final Exception e) {
            throw new ExecutionException(e.getCause());
        }
    }

    @Override
    public ImmutableMap<K, V> getAllPresent(final Iterable<?> keys) {
        return this.cache.getAllPresent(keys);
    }

    @Override
    public void put(final K key, final V value) {
        if (this.predicate.test(key)) {
            this.cache.put(key, value);
        }
    }

    @Override
    public void putAll(final Map<? extends K, ? extends V> m) {
        this.cache.putAll(m.entrySet().stream().filter(e -> this.predicate.test(e.getKey()))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue)));
    }

    @Override
    public void invalidate(final Object key) {
        this.cache.invalidate(key);
    }

    @Override
    public void invalidateAll(final Iterable<?> keys) {
        this.cache.invalidate(keys);

    }

    @Override
    public void invalidateAll() {
        this.cache.invalidateAll();
    }

    @Override
    public long size() {
        return this.cache.size();
    }

    @Override
    public CacheStats stats() {
        return this.cache.stats();
    }

    @Override
    public void cleanUp() {
        this.cache.cleanUp();
    }

    @Override
    public V get(final K key) throws ExecutionException {
        if (this.predicate.test(key)) { return this.cache.get(key); }
        try {
            return this.loader.load(key);
        } catch (final Exception e) {
            throw new ExecutionException(e.getCause());
        }
    }

    @Override
    public V getUnchecked(final K key) {
        if (this.predicate.test(key)) { return this.cache.getUnchecked(key); }
        try {
            return this.loader.load(key);
        } catch (final Exception e) {
            throw new UncheckedExecutionException(e.getCause());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public ImmutableMap<K, V> getAll(final Iterable<? extends K> keys) throws ExecutionException {
        final Map<K, V> results = new HashMap<>();
        final Set<K> matching = new HashSet<>();
        final Set<K> violating = new HashSet<>();

        for (final K key : keys) {
            if (this.predicate.test(key)) {
                matching.add(key);
            } else {
                violating.add(key);
            }
        }
        results.putAll(this.cache.getAll(matching));
        try {
            results.putAll((Map<K, V>) this.loader.loadAll(violating));
        } catch (final Exception e) {
            throw new UncheckedExecutionException(e.getCause());
        }
        return ImmutableMap.copyOf(results);
    }

    @Override
    public V apply(final K key) {
        return getUnchecked(key);
    }

    @Override
    public void refresh(final K key) {
        if (this.predicate.test(key)) {
            this.cache.refresh(key);
        }
    }

    @Override
    public ConcurrentMap<K, V> asMap() {
        return this.cache.asMap();
    }

}
