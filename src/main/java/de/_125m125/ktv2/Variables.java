package de._125m125.ktv2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.servlet.UnavailableException;

import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

import de._125m125.ktv2.beans.LotteryItemConfig;
import de._125m125.ktv2.logging.Log;

public class Variables {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(Timestamp.class,
                    (JsonDeserializer<Timestamp>) (json, typeOfT,
                            context) -> new Timestamp(json.getAsJsonPrimitive().getAsLong()))
            .registerTypeAdapter(Timestamp.class,
                    (JsonSerializer<Timestamp>) (timestamp, type,
                            jsonSerializationContext) -> new JsonPrimitive(timestamp.getTime()))
            .registerTypeAdapter(Date.class, (JsonSerializer<Date>) (src, typeOfSrc,
                    context) -> src == null ? null : new JsonPrimitive(Variables.sdf.format(src)))
            .create();

    public static final String BANKQUERY = "https://bank.kadcon.de/index.php?limit=" + 25
            + "&accounts%5B%5D=*kadcontrade&last_limit_start=%d%&next_page=true&";
    public static String       dbname;
    public static String       dbpass;
    public static String       dbuser;

    private static String[]    ALLOWED_HOSTS;
    private static Set<String> allowedServers;

    private static int                          mysqlPoolsize = 5;
    public static final int                     DT            = 86400000;
    public static final String                  LOGPATH       = "/home/kadcontrade/log/";
    public static final HashMap<String, String> NEWID_TO_ID   = new HashMap<>();
    public static final HashMap<String, String> ID_TO_DN      = new HashMap<>();
    public static final HashMap<String, String> DN_TO_ID      = new HashMap<>();
    public static final ResourceBundle          text          =
            ResourceBundle.getBundle("language", Locale.GERMAN);
    public static String[][]                    allItems      =
            { { "-1", "Kadis(-1)" }, { "1", "Stone" }, { "4", "Cobblestone" } };
    private static String                       kbankToken;
    private static String                       vkbaAuth;
    private static String                       vkbaName;
    private static String                       baseLocation;
    private static String                       baseLink;
    private static String                       pusherApiSecret;
    private static String                       pusherApiKey;
    private static String                       pusherAppId;
    private static JsonObject                   settings;
    private static SecretKey                    aesSecretKey;

    private static Map<String, LotteryItemConfig> lotteryItemConfig;

    public static void prepare() throws UnavailableException {
        File f = new File("/home/kadcontrade/");
        if (f.exists() || f.mkdir()) {
            f = new File("/home/kadcontrade/itemlist.txt");
            if (!f.exists()) {
                try (BufferedWriter out = new BufferedWriter(new FileWriter(f))) {
                    out.write("id subid displayname\n");
                    out.write("-1 0 Kadis");
                } catch (final Exception x) {
                    throw new UnavailableException(
                            "error while creating itemlist: " + x.getCause());
                }
                throw new UnavailableException(
                        "created itemlist-template. Please insert our data.");
            }
            try (BufferedReader in = new BufferedReader(new FileReader(f))) {
                String s;
                while ((s = in.readLine()) != null) {
                    if (s.isEmpty()) {
                        continue;
                    }
                    final String[] split = s.split(" ", 4);
                    final String id = split[0] + (split[2].equals("0") ? "" : ":" + split[2]);
                    final String newId = split[1] + (split[2].equals("0") ? "" : ":" + split[2]);
                    final String displayname = split[3] + " (" + id + ")";
                    Variables.NEWID_TO_ID.put(newId, id);
                    Variables.DN_TO_ID.put(displayname, id);
                    Variables.ID_TO_DN.put(id, displayname);
                }
            } catch (final Exception x) {
                x.printStackTrace();
                throw new UnavailableException("Error while loading itemlist:" + x.getCause());
            }
            f = new File("/home/kadcontrade/config.json");
            if (!f.exists()) {
                Variables.settings = Json.createObjectBuilder().add("mysqlUser", "mysqlUser")
                        .add("mysqlDatabase", "mysqlDatabase").add("mysqlPassword", "mysqlPassword")
                        .add("kbankToken", "0123456789ABCDEF").add("vkbaAuth", "akey=123456789")
                        .add("baseLocation", "baseLocation")
                        .add("baseLink",
                                "https://map.kadcon.de/?worldname=world&mapname=flatHD&zoom=8&x=0&z=0&nopanel=true")
                        .add("pusherAppId", "pusherAppId").add("pusherApiKey", "pusherApiKey")
                        .add("pusherApiSecret", "pusherApiSecret")
                        .add("hosts", Json.createArrayBuilder().build()).build();
                try (BufferedWriter out = new BufferedWriter(new FileWriter(f))) {
                    out.write(Variables.settings.toString());
                } catch (final Exception x) {
                    throw new UnavailableException(
                            "error while creating database-template: " + x.getCause());
                }
                throw new UnavailableException(
                        "created database-template. Please insert our data.");
            } else {
                try (BufferedReader in = new BufferedReader(new FileReader(f))) {
                    Variables.settings = Json.createReader(in).readObject();
                } catch (final Exception x) {
                    x.printStackTrace();
                    throw new UnavailableException(
                            "Error while loading database-access:" + x.getCause());
                }
            }
            Variables.dbuser = Variables.settings.getString("mysqlUser");
            Variables.dbpass = Variables.settings.getString("mysqlPassword");
            Variables.dbname = Variables.settings.getString("mysqlDatabase");
            Variables.kbankToken = Variables.settings.getString("kbankToken");
            Variables.vkbaAuth = Variables.settings.getString("vkbaAuth");
            Variables.vkbaName = Variables.settings.getString("vkbaName");
            Variables.baseLocation = Variables.settings.getString("baseLocation");
            Variables.baseLink = Variables.settings.getString("baseLink");
            Variables.pusherAppId = Variables.settings.getString("pusherAppId");
            Variables.pusherApiKey = Variables.settings.getString("pusherApiKey");
            Variables.pusherApiSecret = Variables.settings.getString("pusherApiSecret");
            final JsonArray hosts = Variables.settings.getJsonArray("hosts");
            Variables.ALLOWED_HOSTS = new String[hosts.size()];
            for (int i = 0; i < hosts.size(); i++) {
                Variables.ALLOWED_HOSTS[i] = hosts.getString(i);
            }
            Variables.allowedServers = ImmutableSet.copyOf(Variables.ALLOWED_HOSTS);

            f = new File("/home/kadcontrade/lottery.json");
            if (f.exists()) {
                try (BufferedReader in = new BufferedReader(new FileReader(f))) {
                    final LotteryItemConfig[] fromJson = new GsonBuilder()
                            .registerTypeAdapter(LocalDate.class,
                                    new LotteryItemConfig.LocalDateAdapter())
                            .create().fromJson(in, LotteryItemConfig[].class);
                    Variables.lotteryItemConfig = Arrays.stream(fromJson).collect(
                            Collectors.toMap(LotteryItemConfig::getItem, Function.identity()));
                } catch (final Exception x) {
                    x.printStackTrace();
                    throw new UnavailableException(
                            "Error while loading database-access:" + x.getCause());
                }
            } else {
                Variables.lotteryItemConfig = new HashMap<>();
            }

        } else {
            throw new UnavailableException(
                    "Could not create config Folder. please create /home/kadcontrade");
        }
        final Map<String, String> m = new TreeMap<>((o1, o2) -> {
            int i = o1.indexOf(":");
            int k = o2.indexOf(":");
            try {
                int tail = 0;
                if (i == -1 && k == -1) {
                    tail = 0;
                } else if (k == -1) {
                    tail = 1;
                } else if (i == -1) {
                    tail = -1;
                } else {
                    tail = ((Integer) Integer.parseInt(o1.substring(i + 1)))
                            .compareTo(Integer.parseInt(o2.substring(k + 1)));
                }
                i = i == -1 ? o1.length() : i;
                k = k == -1 ? o2.length() : k;
                final int c = ((Integer) Integer.parseInt(o1.substring(0, i)))
                        .compareTo(Integer.parseInt(o2.substring(0, k)));
                if (c != 0) {
                    return c;
                } else {
                    return tail;
                }
            } catch (final NumberFormatException e) {// contains non-integer id
                                                     // of crate
                return o1.compareTo(o2);
            }
        });
        m.putAll(Variables.ID_TO_DN);
        Variables.allItems = new String[m.size()][2];
        final Iterator<Entry<String, String>> it = m.entrySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            final Entry<String, String> next = it.next();
            Variables.allItems[i][0] = next.getKey();
            Variables.allItems[i][1] = next.getValue();
            i++;
        }
    }

    public static boolean resourceExists(final String string) {
        return !"-1".equals(string) && Variables.ID_TO_DN.keySet().contains(string);
    }

    public static boolean resourceExistsOrIsMoney(final String string) {
        return "-1".equals(string) || Variables.ID_TO_DN.keySet().contains(string);
    }

    public static String translate(final String fragment) {
        try {
            return Variables.text.getString(fragment);
        } catch (final MissingResourceException x) {
            System.err.println(x.getMessage());
            Log.MISSING_TRANSLATION.log(Level.INFO, fragment);
        }
        return fragment;
    }

    public static String translate(final String fragment, final Object... arguments) {
        try {
            return MessageFormat.format(Variables.text.getString(fragment), arguments);
        } catch (final MissingResourceException x) {
            System.err.println(x.getMessage());
            Log.MISSING_TRANSLATION.log(Level.INFO, fragment);
        }
        return fragment;
    }

    public static Optional<String> tryTranslate(final String fragment, final Object... arguments) {
        try {
            return Optional.of(MessageFormat.format(Variables.text.getString(fragment), arguments));
        } catch (final MissingResourceException x) {
            return Optional.empty();
        }
    }

    public static void setPoolSize(final int k) {
        Variables.mysqlPoolsize = k;
    }

    public static int getMySQLPoolSize() {
        return Variables.mysqlPoolsize;
    }

    public static String[] getALLOWED_HOSTS() {
        return Variables.ALLOWED_HOSTS;
    }

    public static Set<String> getAllowedServers() {
        return Variables.allowedServers;
    }

    public static String getKbankToken() {
        return Variables.kbankToken;
    }

    public static String getVkbaAuth() {
        return Variables.vkbaAuth;
    }

    public static String getVkbaName() {
        return Variables.vkbaName;
    }

    public static String getBaseLocation() {
        return Variables.baseLocation;
    }

    public static String getBaseLink() {
        return Variables.baseLink;
    }

    public static String getPusherApiSecret() {
        return Variables.pusherApiSecret;
    }

    public static String getPusherApiKey() {
        return Variables.pusherApiKey;
    }

    public static String getPusherAppId() {
        return Variables.pusherAppId;
    }

    public static String getDisplayNameForMatId(final String material) {
        return Variables.ID_TO_DN.get(material);
    }

    public static String getOrDefault(final String name, final String orDefault) {
        try {
            return Variables.settings.getString(name);
        } catch (final NullPointerException e) {
            return orDefault;
        }
    }

    public static Integer getOrDefault(final String name, final Integer orDefault) {
        try {
            return Variables.settings.getInt(name);
        } catch (final NullPointerException e) {
            return orDefault;
        }
    }

    public static String getString(final String name) {
        return Variables.settings.getString(name);
    }

    public static SecretKey getAESSecretKey() throws IOException {
        if (Variables.aesSecretKey == null) {
            return loadOrCreateAESSecretkey();
        }
        return Variables.aesSecretKey;
    }

    private synchronized static SecretKey loadOrCreateAESSecretkey() throws IOException {
        if (Variables.aesSecretKey != null) {
            return Variables.aesSecretKey;
        }

        final File f = new File("/home/kadcontrade/AES.key");

        if (!f.exists()) {
            KeyGenerator keygen;
            try {
                keygen = KeyGenerator.getInstance("AES");
            } catch (final NoSuchAlgorithmException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            keygen.init(128);
            final Key key = keygen.generateKey();

            final byte[] bytes = key.getEncoded();
            try (FileOutputStream keyfos = new FileOutputStream(f)) {
                keyfos.write(bytes);
            }
        }
        try (FileInputStream fis = new FileInputStream(f)) {
            final byte[] encodedKey = new byte[(int) f.length()];
            fis.read(encodedKey);
            final SecretKey key = new SecretKeySpec(encodedKey, "AES");
            Variables.aesSecretKey = key;
            return Variables.aesSecretKey;
        }
    }

    public static String getCoreUrl() {
        return getOrDefault("coreUrl", "https://itemmc.com");
    }

    public static String getFirebaseSecret() {
        return getOrDefault("firebaseSecret", (String) null);
    }

    public static Optional<LotteryItemConfig> getLotteryItemConfig(final String item) {
        return Optional.ofNullable(Variables.lotteryItemConfig.get(item));
    }

    public static List<String> getLotteryItems() {
        return new ArrayList<>(Variables.lotteryItemConfig.keySet());
    }

    public static String getAppName() {
        return getOrDefault("appname", "ItemMC");
    }

}
