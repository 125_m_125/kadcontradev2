package de._125m125.ktv2.updates;

import java.time.LocalDate;
import java.util.ArrayList;

import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.servlets.apiv2.HistoryRestAPI;

public class HistoryUpdater implements Updater {
    private static final ArrayList<String[]> QUERYS = new ArrayList<>();
    static {
        HistoryUpdater.QUERYS.add(new String[] {
                "INSERT INTO history(SELECT ?,type,material,SUM(tmp0.price)/SUM(tmp0.amount) AS value,SUM(tmp0.ramount) AS amount, SUM(total) AS total FROM (((SELECT type,material,amount*price AS price,amount,amount AS ramount, amount*price AS total FROM tradesdone WHERE time<?)UNION ALL (SELECT type,material, (amount-sold)/100*price AS price,(amount-sold)/100 AS amount,0 AS ramount, 0 AS total FROM trade)UNION ALL (SELECT type,material,IF(amount=0,value/10000,amount*value/10000) AS price,IF(amount=0,0.0001,amount/10000) AS amount,0 AS ramount, 0 AS total FROM history WHERE time=(SELECT MAX(time) FROM history))) AS tmp0) GROUP BY tmp0.type,tmp0.material)",
                null, null });
        HistoryUpdater.QUERYS.add(new String[] {
                "DELETE FROM history WHERE time=? AND type=1 AND material='-1'", null });
        HistoryUpdater.QUERYS.add(new String[] {
                "INSERT INTO history(SELECT ?,1,-1,IFNULL((SELECT value FROM history WHERE time=TIMESTAMPADD(DAY,-1,?) AND material='-1'),1000000000)*(1+IFNULL(IF(SUM(n.amount)=0,0,SUM(IF(o.value=0,0,(n.value-o.value)*CAST(n.amount AS SIGNED)/o.value))/SUM(n.amount)),0)),IFNULL(SUM(n.amount),0),IFNULL(SUM(n.total),0) FROM ((SELECT material,value,amount,total FROM history WHERE time=? AND type=1) as n LEFT JOIN (SELECT material,value FROM history WHERE time=TIMESTAMPADD(DAY,-1,?) AND type=1) as o USING(material)))",
                null, null, null, null });

        HistoryUpdater.QUERYS.add(new String[] {
                "INSERT INTO minmax(SELECT ?,type,material,MIN(price),MAX(price) FROM tradesdone WHERE time<? GROUP BY type,material)",
                null, null });
        HistoryUpdater.QUERYS.add(new String[] {
                "INSERT INTO system(k,v) VALUES('lastHistoryUpdate',?) ON DUPLICATE KEY UPDATE v=?",
                null, null });
        HistoryUpdater.QUERYS.add(new String[] { "DELETE FROM tradesdone WHERE time<?", null });
    }

    @Override
    public void execute(final LocalDate d, final Connector c) {
        synchronized (HistoryUpdater.QUERYS) {
            final String dateString = d.toString();
            for (final String[] s : HistoryUpdater.QUERYS) {
                for (int i = 1; i < s.length; i++) {
                    s[i] = dateString;
                }
            }
            c.executeAtomicUpdate(HistoryUpdater.QUERYS);
            HistoryRestAPI.invalidateAll();
        }
    }

}