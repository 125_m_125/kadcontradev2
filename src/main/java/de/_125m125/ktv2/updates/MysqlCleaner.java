package de._125m125.ktv2.updates;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;

import de._125m125.ktv2.datastorage.Connector;

public class MysqlCleaner implements Updater {
    private static final String LOG_CLEANUP            = "DELETE FROM logging WHERE time<DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY)";
    private static final String LOG_CLEANUP_SR         = "DELETE FROM logging WHERE type=\"sr\" AND time<DATE_ADD(CURRENT_DATE, INTERVAL -14 DAY)";
    private static final String MESSAGE_CLEANUP        = "DELETE FROM messages WHERE time<DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY)";
    private static final String MFAIP_CLEANUP          = "DELETE FROM twoFactorIP";
    private static final String REMEMBER_LOGIN_CLEANUP = "DELETE FROM rememberedLogins WHERE creation<DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -7 DAY)";

    @Override
    public void execute(final LocalDate d, final Connector c) {
        try (Connection con = c.getConnection()) {
            try (PreparedStatement st = con.prepareStatement(MysqlCleaner.LOG_CLEANUP)) {
                st.executeUpdate();
            }
            try (PreparedStatement st = con.prepareStatement(MysqlCleaner.LOG_CLEANUP_SR)) {
                st.executeUpdate();
            }
            try (PreparedStatement st = con.prepareStatement(MysqlCleaner.MESSAGE_CLEANUP)) {
                st.executeUpdate();
            }
            try (PreparedStatement st = con.prepareStatement(MysqlCleaner.MFAIP_CLEANUP)) {
                st.executeUpdate();
            }
            try (PreparedStatement st = con.prepareStatement(MysqlCleaner.REMEMBER_LOGIN_CLEANUP)) {
                st.executeUpdate();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }
}
