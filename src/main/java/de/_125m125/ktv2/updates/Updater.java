package de._125m125.ktv2.updates;

import java.time.LocalDate;

import de._125m125.ktv2.datastorage.Connector;

public interface Updater {
    public void execute(final LocalDate ld, Connector c);
}
