package de._125m125.ktv2.updates;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;

import de._125m125.ktv2.datastorage.Connector;

public class InProgressPayoutCleanup implements Updater {
    private final Clock c = Clock.systemDefaultZone();

    @Override
    public void execute(final LocalDate d, final Connector c) {
        final LocalDateTime currentTime = LocalDateTime.now(this.c);
        if (currentTime.getHour() <= 1 || currentTime.getHour() >= 23) {
            c.executeUpdate(
                    "UPDATE payout SET userInteraction=TRUE, requiresAdminInteraction=TRUE WHERE !userInteraction AND !success AND !adminInteractionCompleted AND !requiresAdminInteraction");
        }
    }

}
