package de._125m125.ktv2.updates;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.market.Trade.buySell;

public class LotteryTrader implements Updater {

    @Override
    public void execute(final LocalDate d, final Connector c) {
        Variables.getLotteryItems().stream().map(Variables::getLotteryItemConfig).map(Optional::get)
                .filter(conf -> conf.dailyInsertOn(d)).forEach(conf -> {
                    long price = 0;
                    try {
                        try (Connection con = c.getConnection();
                                PreparedStatement ps = con.prepareStatement(
                                        "SELECT value FROM history WHERE material=? ORDER BY time DESC LIMIT 1")) {
                            ps.setString(1, conf.getItem());
                            try (ResultSet rs = ps.executeQuery()) {
                                price = 10_000000;
                                if (rs.next()) {
                                    price = rs.getLong("value");
                                }
                            }
                        } catch (final SQLException e) {
                            e.printStackTrace();
                        }
                        if (price != 0) {
                            Main.instance().getTradeManager()
                                    .create(conf.getDailyInsertAmount(), buySell.SELL,
                                            conf.getItem(), conf.getSeller(), price,
                                            ITEM_OR_STOCK.ITEM, false)
                                    .get();
                        }
                    } catch (final ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }

}
