package de._125m125.ktv2.updates;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.ThreadManager;
import de._125m125.ktv2.datastorage.Connector;

public class UpdateManager {
    private static final int hour         = 0, min = 0, sec = 0;

    private final Updater[]  updater      = { new UserUpdater(), new MysqlCleaner(),
            new InProgressPayoutCleanup() };

    private final Updater[]  dailyUpdater = { new HistoryUpdater(), new LotteryTrader() };

    public UpdateManager(final Connector c) {
        schedule(0, c);
    }

    private void startDelayedExecution() {
        final long delay = computeNextDelay(UpdateManager.hour, UpdateManager.min,
                UpdateManager.sec);
        schedule(delay, Main.instance().getConnector());
    }

    private void schedule(final long delay, final Connector c) {
        final Runnable taskWrapper = () -> {
            execute(c);
            startDelayedExecution();
        };
        ThreadManager.instance().schedule(taskWrapper, delay, TimeUnit.SECONDS);
    }

    private long computeNextDelay(final int targetHour, final int targetMin, final int targetSec) {
        final LocalDateTime localNow = LocalDateTime.now();
        final ZoneId currentZone = ZoneId.systemDefault();
        final ZonedDateTime zonedNow = ZonedDateTime.of(localNow, currentZone);
        ZonedDateTime zonedNextTarget = zonedNow.withHour(targetHour).withMinute(targetMin)
                .withSecond(targetSec);
        if (zonedNow.compareTo(zonedNextTarget) > 0) {
            zonedNextTarget = zonedNextTarget.plusDays(1);
        }

        final Duration duration = Duration.between(zonedNow, zonedNextTarget);
        return duration.getSeconds();
    }

    private LocalDate[] getMissingDates(final LocalDate end, final Connector c) {
        final String result = (String) c.getColumn("system", "v", "k='lastHistoryUpdate'");
        if (result == null) {
            return new LocalDate[] { end };
        }
        final LocalDate start = LocalDate.parse(result);
        final long days = ChronoUnit.DAYS.between(start, end);
        if (days < 1) {
            return new LocalDate[] {};
        }
        final LocalDate[] ret = new LocalDate[(int) days];
        for (int i = 0; i < days; i++) {
            ret[i] = start.plusDays(i + 1);
        }
        return ret;
    }

    private synchronized void execute(final Connector c) {
        final ZoneId zid = ZoneId.systemDefault();
        final LocalDate ld = LocalDate.now(zid);
        for (final LocalDate date : getMissingDates(ld, c)) {
            for (final Updater vc : this.dailyUpdater) {
                try {
                    vc.execute(date, c);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        }
        for (final Updater vc : this.updater) {
            try {
                vc.execute(ld, c);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

}
