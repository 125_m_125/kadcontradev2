package de._125m125.ktv2.updates;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import de._125m125.ktv2.datastorage.Connector;
import de._125m125.ktv2.datastorage.MySQLHelper;

public class UserUpdater implements Updater {

    @Override
    public synchronized void execute(final LocalDate d, final Connector c) {
        final ArrayList<HashMap<String, Object>> result = c.executeQuery("SELECT * FROM reftaxes");
        if (result == null || result.isEmpty()) {
            return;
        }
        final ArrayList<String[]> querys = new ArrayList<>();
        for (final HashMap<String, Object> line : result) {
            final long id = (long) line.get("id");
            final long amount = (long) line.get("amount");
            querys.addAll(
                    MySQLHelper.addResource(id, true, "-1", amount, "Refferalprogramm", true));
        }
        querys.add(new String[] { "DELETE FROM reftaxes" });
        c.executeAtomicUpdate(querys);
    }

}
