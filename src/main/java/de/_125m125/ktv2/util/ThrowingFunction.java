package de._125m125.ktv2.util;

import java.io.IOException;

@FunctionalInterface
public interface ThrowingFunction<T, R> {
    public R apply(T t) throws IOException;
}
