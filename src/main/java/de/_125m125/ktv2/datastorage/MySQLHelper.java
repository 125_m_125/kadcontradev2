package de._125m125.ktv2.datastorage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.univocity.parsers.tsv.TsvRoutines;
import com.univocity.parsers.tsv.TsvWriterSettings;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.calculators.IDSConverter;

public class MySQLHelper {
    public static final MySQLHelper INSTANCE = new MySQLHelper();

    public static List<String[]> addResource(final long owner, final boolean item,
            final String materialid, final long amount, final String reason) {
        return addResource(owner, item, materialid, amount, reason, false);
    }

    public static List<String[]> addResource(final long owner, final boolean item,
            final String materialid, final long amount, String reason, final boolean sendMessage) {
        reason = reason.replaceAll("\t", " ");
        final List<String[]> ret = new ArrayList<>();
        ret.add(new String[] {
                new StringBuilder("INSERT INTO materials (id,type,matid,amount) VALUES(")
                        .append(owner).append(",").append(item).append(",'").append(materialid)
                        .append("',").append(amount)
                        .append(") ON DUPLICATE KEY UPDATE amount=amount+").append(amount)
                        .toString() });
        ret.add(new String[] { new StringBuilder(
                "INSERT INTO materialMovements(uid,type,matid,amount,reason) VALUES(").append(owner)
                        .append(",").append(item).append(",'").append(materialid).append("',")
                        .append(amount).append(",?)").toString(),
                reason });
        if (sendMessage) {
            String amount2;
            if ("-1".equals(materialid)) {
                amount2 = IDSConverter.longToDoubleString(amount);
            } else {
                amount2 = Long.toString(amount);
            }
            ret.add(new String[] { "INSERT INTO messages (uid, message) VALUES(?,?)",
                    String.valueOf(owner), Variables.translate("addedMaterial", amount2,
                            Variables.ID_TO_DN.get(materialid), reason) });
        }
        return ret;
    }

    public static Message addResource(final Connection c, final long owner, final boolean item,
            final String materialid, final long amount, String reason, final boolean sendMessage,
            final Object... replacements) throws SQLException {
        if (amount == 0) {
            return null;
        }
        reason = reason.replaceAll("\t", " ");
        try (PreparedStatement st = c.prepareStatement(
                "INSERT INTO materials (id,type,matid,amount) VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE amount=amount+?")) {
            st.setLong(1, owner);
            st.setBoolean(2, item);
            st.setString(3, materialid);
            st.setLong(4, amount);
            st.setLong(5, amount);
            st.executeUpdate();
        }
        try (PreparedStatement st = c.prepareStatement(
                "INSERT INTO materialMovements(uid,type,matid,amount,reason) VALUES(?,?,?,?,?)")) {
            st.setLong(1, owner);
            st.setBoolean(2, item);
            st.setString(3, materialid);
            st.setLong(4, amount);
            st.setString(5, reason);
            st.executeUpdate();
        }
        if (!sendMessage) {
            return null;
        }
        return sendMessage(c, owner, "addedMaterial",
                "-1".equals(materialid) ? IDSConverter.longToDoubleString(amount) : amount,
                Variables.ID_TO_DN.get(materialid), Variables.translate(reason, replacements));
    }

    public static Message sendMessage(final Connection c, final long receiver,
            final String messageId, final Object... replacements) throws SQLException {
        final String translate = Variables.translate(messageId, replacements);
        try (PreparedStatement st =
                c.prepareStatement("INSERT INTO messages (uid, message) VALUES(?,?)")) {
            st.setLong(1, receiver);
            st.setString(2, translate);
            st.executeUpdate();
        }
        return new Message(new Timestamp(System.currentTimeMillis()), translate);
    }

    public String resultSetToTsv(final String nameKey, final ResultSet rs) throws IOException {
        final File directory = new File("/home/kadcontrade/generated/tsv/");
        directory.mkdirs();
        final File file = File.createTempFile(nameKey, ".tsv", directory);
        try (OutputStream os = new FileOutputStream(file)) {
            resultSetToTsv(rs, os);
        }
        return file.getName();
    }

    public void resultSetToTsv(final ResultSet rs, final OutputStream os) {
        final TsvWriterSettings writerSettings = new TsvWriterSettings();
        writerSettings.getFormat().setLineSeparator("\n");
        writerSettings.setHeaderWritingEnabled(true);

        final TsvRoutines routines = new TsvRoutines(writerSettings);

        routines.write(rs, os);
    }

    public String resultSetToTsv(final String nameKey, final ResultSet rs,
            final ZipOutputStream zos) throws IOException {
        if (zos == null) {
            return resultSetToTsv(nameKey, rs);
        }
        zos.putNextEntry(new ZipEntry(nameKey));
        final OutputStream proxy = new OutputStream() {

            @Override
            public void write(final int b) throws IOException {
                zos.write(b);
            }

            @Override
            public void flush() throws IOException {
                zos.flush();
            }

        };
        resultSetToTsv(rs, proxy);
        zos.closeEntry();
        return nameKey;
    }
}
