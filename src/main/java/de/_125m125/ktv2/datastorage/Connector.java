package de._125m125.ktv2.datastorage;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Level;

import javax.sql.DataSource;

import com.google.api.client.util.Strings;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.calculators.EncryptionHelper;
import de._125m125.ktv2.calculators.EncryptionHelper.PasswordCheckResult;
import de._125m125.ktv2.logging.Log;

/**
 * this Class is used to perform all MySQL-tasks
 *
 * @author fabian
 *
 */
public class Connector implements Closeable {
    @FunctionalInterface
    public static interface Successindicator {
        public boolean wasSuccessful();
    }

    @FunctionalInterface
    public static interface AtomicExecutor<T extends Successindicator> {
        public T apply(Connection c) throws SQLException;
    }

    public static final String[] TABLES     = { "login", "trade", "materials", "history", "minmax",
            "system", "messages", "tradesdone", "payout", "payoutsettings", "logging", "admin",
            "banned", "possibleRegister", "possiblepwchange", "referrer", "reftaxes", "token",
            "materialMovements", "tradeHistory", "tradesDoneHistory", "payoutboxes", "giftcodes",
            "twoFactorAuth", "mailcounter", "twoFactorIP", "twoFactorCookie", "errors",
            "unknownPayins", "webPushSubscriptions", "gdpr", "certificates", "rememberedLogins",
            "objectnames" };
    private static final Random  rnd        = new SecureRandom();
    private final DataSource     ds;
    private final String         port       = "3306";
    private final String         serverName = "127.0.0.1";

    public Connector() {
        final HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(Variables.getMySQLPoolSize());
        config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        config.addDataSourceProperty("serverName", this.serverName);
        config.addDataSourceProperty("port", this.port);
        config.addDataSourceProperty("databaseName", Variables.dbname);
        config.addDataSourceProperty("user", Variables.dbuser);
        config.addDataSourceProperty("password", Variables.dbpass);
        config.addDataSourceProperty("characterEncoding", "utf8");
        config.addDataSourceProperty("useUnicode", "true");

        this.ds = new HikariDataSource(config);
    }

    public DataSource getDataSource() {
        return this.ds;
    }

    public Connection getConnection() throws SQLException {
        return this.ds.getConnection();
    }

    /**
     * checks if the Program is able to connect to the MySQL-Table
     *
     * @return true if connection is possible, false if not
     * @throws SQLException
     */
    public boolean canConnect() throws SQLException {
        Statement st = null;
        boolean ret = false;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.createStatement();
            rs = st.executeQuery("SELECT VERSION()");
            if (rs.next()) {
                ret = true;
            }
        } catch (final SQLException ex) {
            throw ex;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ret;
    }

    public String changePass(final String name, final String pass) {
        long id;

        if ((id = getID(name)) == -1) {
            return "NameNotInUse";
        }
        executeUpdate("DELETE FROM possiblepwchange WHERE name=?", name);
        update("login", "Password=?", new String[] { pass }, "Name=?", new String[] { name });
        insert("messages", "uid,message", id + ",?",
                new String[] { "Dein Passwort wurde erfolgreich geaendert" });
        return "success" + id;
    }

    /**
     * changes the IP of a user
     *
     * @param name
     *            the name of the user of whom the IP should be changed
     * @param ip
     *            the new IP
     * @param optional
     */
    private void updateUser(final String name, final String ip,
            final Optional<String> newPassword) {
        final String query =
                newPassword.isPresent() ? "UPDATE login SET ip=?,Password=? WHERE Name=?"
                        : "UPDATE login SET ip=? WHERE Name=?";
        executeAtomic(c -> {
            try (PreparedStatement ps = c.prepareStatement(query)) {
                int index = 1;
                ps.setString(index++, ip);
                if (newPassword.isPresent()) {
                    ps.setString(index++, newPassword.get());
                }
                ps.setString(index++, name);
                ps.executeUpdate();
            }
            if (newPassword.isPresent()) {
                Log.USER.executeLog(c, Level.INFO, "updating password algorithm",
                        UserHelper.get(name));
            }
            return () -> true;
        });
    }

    /**
     * checks if the IP is already in use more than i-modifier times
     *
     * @param ip
     *            the IP that should be checked
     * @param modifier
     *            modifies how often the IP is
     * @return false, if the IP is in use to often, true if not
     */
    private boolean checkIP(final String ip, final int modifier) {
        final long i = (long) getColumn("login", "COUNT(*)", "ip=?", new String[] { ip });
        if (i - modifier > 1 || i == -1) {
            return false;
        }
        return true;
    }

    /**
     * Checks if all tables are existing and if one is missing it recreates it. <br>
     * <b>this method does not check if there is data missing and does not refill the tables. It
     * would be better to load a backup!</b>
     *
     */
    public void checktables() {
        Statement st = null;
        final String[] args = {
                "id INTEGER UNSIGNED, Name VARCHAR(25), Password VARCHAR(200) BINARY, IP VARCHAR(45)", // login
                "id BIGINT AUTO_INCREMENT,time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, traderid INTEGER UNSIGNED, type BOOLEAN,buysell BOOLEAN, material VARCHAR(30), amount INTEGER UNSIGNED, price BIGINT, sold INTEGER UNSIGNED", // trade
                "id INTEGER UNSIGNED, type BOOLEAN, matid VARCHAR(30), amount BIGINT", // materials
                "time DATE, type BOOLEAN,material VARCHAR(30),value BIGINT, amount INTEGER UNSIGNED, total BIGINT", // history
                "time DATE, type BOOLEAN,material VARCHAR(30),min BIGINT,max BIGINT", // minmax
                "k VARCHAR(25), v VARCHAR(25)", // system
                "id INTEGER UNSIGNED AUTO_INCREMENT, uid INTEGER UNSIGNED, message VARCHAR(200),time TIMESTAMP default CURRENT_TIMESTAMP", // messages
                "time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, type BOOLEAN, material VARCHAR(30),amount INTEGER UNSIGNED,price BIGINT", // tradesdone
                "id INTEGER UNSIGNED AUTO_INCREMENT, uid INTEGER UNSIGNED, type BOOLEAN,matid VARCHAR(30),amount BIGINT,userInteraction BIT(1),success BIT(1),adminInteractionCompleted BIT(1),requiresAdminInteraction BIT(1),unknown BIT(1),payoutType VARCHAR(10), date TIMESTAMP default CURRENT_TIMESTAMP,message VARCHAR(255), agent INTEGER UNSIGNED, forcer INTEGER UNSIGNED", // payout
                "uid INTEGER UNSIGNED,server TINYINT, x MEDIUMINT, y MEDIUMINT, z MEDIUMINT, warp VARCHAR(15)", // payoutsettings
                "id INTEGER UNSIGNED AUTO_INCREMENT,time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,type CHAR(2),level SMALLINT UNSIGNED,uid INTEGER UNSIGNED,message VARCHAR(500)", // logging
                "id INTEGER UNSIGNED", // admin
                "id INTEGER UNSIGNED, reason VARCHAR(15)", // banned
                "name VARCHAR(25),Password VARCHAR(200) BINARY,IP VARCHAR(45),itemcomment VARCHAR(50),referrer INTEGER UNSIGNED, id TINYINT UNSIGNED", // possibleRegister
                "name VARCHAR(25),Password VARCHAR(200) BINARY,id TINYINT UNSIGNED", // possiblepwchange
                "id INTEGER UNSIGNED, ref INTEGER UNSIGNED", // referrer
                "id INTEGER UNSIGNED, amount INTEGER UNSIGNED", // reftaxes
                "uid INTEGER UNSIGNED, tid INTEGER UNSIGNED, permissions INTEGER, tkn BIGINT UNSIGNED", // token
                "id INTEGER UNSIGNED AUTO_INCREMENT, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, uid INTEGER UNSIGNED, type BOOLEAN, matid VARCHAR(30), amount BIGINT, reason VARCHAR(255)", // materialMovements
                "time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, id BIGINT,traderid INTEGER UNSIGNED, type BOOLEAN,buysell BOOLEAN, material VARCHAR(30), amount INTEGER UNSIGNED, price BIGINT, cancelled TIMESTAMP NULL DEFAULT NULL", // tradeHistory
                "time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, id1 BIGINT, id2 BIGINT, amount INTEGER UNSIGNED, price INTEGER UNSIGNED, taxPerPerson INTEGER UNSIGNED", // tradesDoneHistory
                "cid INTEGER UNSIGNED AUTO_INCREMENT, server TINYINT NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, z INTEGER NOT NULL, owner INTEGER UNSIGNED DEFAULT NULL, verified BOOLEAN DEFAULT FALSE", // payoutboxes
                "preCode CHAR(2) NOT NULL, code BIGINT UNSIGNED NOT NULL, amount BIGINT UNSIGNED NOT NULL, material VARCHAR(30) NOT NULL, redeemer INTEGER UNSIGNED DEFAULT NULL", // giftcodes
                "id INTEGER UNSIGNED AUTO_INCREMENT, uid INTEGER UNSIGNED NOT NULL, type CHAR(2) NOT NULL, data BLOB, iv BINARY(16)", // twoFactorAuth
                "mail VARCHAR(100) NOT NULL, active BOOLEAN NOT NULL DEFAULT TRUE, count TINYINT NOT NULL DEFAULT 0, lastSent TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP", // mailcounter
                "id INTEGER UNSIGNED, ip VARCHAR(45)", // twoFactorIp
                "id INTEGER UNSIGNED, time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, cookie BINARY(16)", // twoFactorCookie
                "id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT, reporter INTEGER UNSIGNED NOT NULL, message VARCHAR(200) NOT NULL, source VARCHAR(200) NOT NULL, lineno INTEGER UNSIGNED NOT NULL DEFAULT 0, colno INTEGER UNSIGNED NOT NULL DEFAULT 0, error TEXT NOT NULL", // errors
                "username VARCHAR(25), type BOOLEAN, item VARCHAR(30), amount BIGINT", // unknownPayins
                "user INTEGER UNSIGNED, token VARCHAR(255) NOT NULL, itemlist BIT(1) NOT NULL, messages BIT(1) NOT NULL,orders BIT(1) NOT NULL,payouts BIT(1) NOT NULL,workers BIT(1) NOT NULL", // webPushSubscriptions
                "user INTEGER UNSIGNED, type CHAR(2)", // GDPR
                "user INTEGER UNSIGNED, serialNr BINARY(20), type TINYINT", // certificates
                "user INTEGER UNSIGNED, value BINARY(18) NOT NULL, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP", // rememberedLogins
                "server INTEGER UNSIGNED, id VARCHAR(100) NOT NULL, language CHAR(3), name VARCHAR(100)", // objectnames
        };

        final String[] special = { ", PRIMARY KEY (id)", // login
                ", PRIMARY KEY (id),INDEX(type,buysell,material)", // trade
                ", PRIMARY KEY (id,type,matid)", // materials
                ", PRIMARY KEY (time,type,material), INDEX(type,material)", // history
                ", PRIMARY KEY (time,type,material), INDEX(type,material)", // minmax
                ", PRIMARY KEY (k)", // system
                ", PRIMARY KEY (id), INDEX(uid)", // messages
                ", INDEX(time,type,material)", // tradesdone
                ", PRIMARY KEY (id), INDEX(uid, type)", // payout
                ", PRIMARY KEY (uid)", // payoutsettings
                ", PRIMARY KEY (id), INDEX(type,uid), INDEX(uid)", // logging
                ", PRIMARY KEY (id)", // admin
                ", PRIMARY KEY (id)", // banned
                ", PRIMARY KEY (name,id)", // possibleRegister
                ", PRIMARY KEY (name,id)", // possiblepwchange
                ", PRIMARY KEY (id,ref), INDEX(ref)", // referrer
                ", PRIMARY KEY (id)", // reftaxes
                ", PRIMARY KEY (tid), INDEX(uid,tid)", // token
                ", PRIMARY KEY (id), INDEX(uid,type,matid)", // materialMovements
                ", PRIMARY KEY (id), INDEX(traderid,type,material)", // tradeHistory
                ", PRIMARY KEY (id1,id2)", // tradesDoneHistory
                ", PRIMARY KEY (cid), INDEX(owner), UNIQUE(server,x,y,z)", // payoutboxes
                ", PRIMARY KEY (preCode, code)", // giftcodes
                ", PRIMARY KEY (id), INDEX(uid)", // twoFactorAuth
                ", PRIMARY KEY (mail)", // mailcounter
                ", PRIMARY KEY (id,ip)", // twoFactorIP
                ", PRIMARY KEY (id,cookie)", // twoFactorCookie
                ", PRIMARY KEY (id)", // errors
                ", PRIMARY KEY (username,type,item)", // unknownPayins
                ", PRIMARY KEY (user,token)", // webPushSubscriptions
                ", PRIMARY KEY (user,type)", // gdpr
                ", PRIMARY KEY (user,type,serialNr), UNIQUE(serialNr)", // certificates
                ", PRIMARY KEY (value)", // rememberedLogins
                ", PRIMARY KEY (server,id)", // objectnames
        };
        final String[] other = { "", // login
                "", // trade
                "", // materials
                "", // history
                "", // minmax
                "", // averages
                "", // deltaL
                "", // deltaS
                "", // system
                "CHARACTER SET utf8 COLLATE utf8_general_ci", // messages
                "", // tradesdone
                "CHARACTER SET utf8 COLLATE utf8_general_ci", // payout
                "", // payoutsettings
                "CHARACTER SET utf8 COLLATE utf8_general_ci", // logging
                "", // admin
                "", // banned
                "", // possibleRegister
                "", // possiblepwchange
                "", // referrer
                "", // reftaxes
                "", // token
                "", // materialMovements
                "", // tradeHistory
                "", // tradesDoneHistory
                "", // payoutboxes
                "", // giftcodes
                "", // twoFactorAuth
                "CHARACTER SET utf8 COLLATE utf8_general_ci", // mailcounter
                "", // twoFactorIP
                "", // twoFactorCookie
                "CHARACTER SET utf8 COLLATE utf8_general_ci", // errors
                "", // unknownPayins
                "", // webPushSubscriptions
                "", // gdpr
                "", // certificates
                "", // rememberedLogins
                "CHARACTER SET utf8 COLLATE utf8_general_ci", // objectnames
        };
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.createStatement();
            for (int i = 0; i < Connector.TABLES.length; i++) {
                st.execute("CREATE TABLE IF NOT EXISTS " + Connector.TABLES[i] + " (" + args[i]
                        + " " + special[i] + ") ENGINE=InnoDB " + other[i]); // ENGINE=InnoDB
                // DEFAULT
                // CHARSET=utf8mb4
                // COLLATE=utf8mb4_unicode_ci");
            }
            long time = System.currentTimeMillis();
            time = time - time % Variables.DT;
            st.executeUpdate("INSERT INTO system VALUES('lastTick','" + time
                    + "'),('lastCheck',CURRENT_TIMESTAMP),('lastCheckUser','none') ON DUPLICATE KEY UPDATE k=k");
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void close() {
        if (this.ds instanceof Closeable) {
            try {
                ((Closeable) this.ds).close();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * creates a new user
     *
     * @param name
     *            the name of the user
     * @param pass
     *            the password of the user
     * @param ip
     * @param reason
     *            TODO
     * @param registeringAdmin
     *            TODO
     * @return "successfull"+userid if the creation was successful, else an errormessage
     */
    public String createUser(final String name, final String pass, final String comment,
            final long ref, final String ip, final String reason, final Long registeringAdmin) {
        final long id = new BigInteger(32, Connector.rnd).longValue();

        final Successindicator executeAtomic = executeAtomic(c -> {
            try (PreparedStatement insertLogin =
                    c.prepareStatement("INSERT INTO login VALUES(?,?,?,?)")) {
                insertLogin.setLong(1, id);
                insertLogin.setString(2, name);
                insertLogin.setString(3, pass);
                insertLogin.setString(4, ip);
                insertLogin.executeUpdate();
            }
            MySQLHelper.sendMessage(c, id, "welcomeMessage");
            MySQLHelper.addResource(c, id, true, "-1", 50000000, "welcomeBonus", true);
            MySQLHelper.addResource(c, id, true, "4", 64, "welcomeBonus", true);
            MySQLHelper.addResource(c, 1, true, "-1", -50000000, "welcomeBonusFor", true,
                    String.valueOf(id));
            MySQLHelper.addResource(c, 1, true, "4", -64, "welcomeBonusFor", true,
                    String.valueOf(id));
            try (PreparedStatement selectUnknownPayins =
                    c.prepareStatement("SELECT * FROM unknownPayins WHERE username=?")) {
                selectUnknownPayins.setString(1, name);
                try (ResultSet rs = selectUnknownPayins.executeQuery()) {
                    while (rs.next()) {
                        MySQLHelper.addResource(c, id, rs.getBoolean("type"), rs.getString("item"),
                                rs.getLong("amount"), "oldPayin", true);
                    }
                }
            }
            Log.USER.executeLog(c, Level.INFO, "User " + name + " finished registration", id);
            if (registeringAdmin != null && registeringAdmin > 0) {
                Log.ADMIN.executeLog(c, Level.INFO,
                        "Admin " + registeringAdmin + " forced registration for " + name,
                        registeringAdmin, id);
            }

            if (ref > 0) {
                try (PreparedStatement referrer =
                        c.prepareStatement("INSERT INTO referrer(id,ref) VALUES(?,?)")) {
                    referrer.setLong(1, id);
                    referrer.setLong(2, ref);
                    referrer.executeUpdate();
                }
            }
            try (PreparedStatement delete =
                    c.prepareStatement("DELETE FROM unknownPayins WHERE username=?")) {
                delete.setString(1, name);
                delete.executeUpdate();
            }
            return () -> true;
        });
        if (executeAtomic.wasSuccessful()) {
            return "success" + id;
        } else {
            return "failed";
        }
    }

    /**
     * executes a String
     *
     * @param string
     *            the string that should be executed
     */
    public void execute(final String string) {
        PreparedStatement st = null;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement(string);
            st.execute(string);
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public ArrayList<HashMap<String, Object>> executeQuery(final String string,
            final String... insert) {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        ArrayList<HashMap<String, Object>> al = new ArrayList<>();
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement(string);
            for (int i = 0; i < insert.length; i++) {
                st.setString(i + 1, insert[i]);
            }
            rs = st.executeQuery();
            while (rs.next()) {
                final HashMap<String, Object> hm = new HashMap<>();
                final ResultSetMetaData rsmd = rs.getMetaData();
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    hm.put(rsmd.getColumnName(i), rs.getObject(i));
                }
                al.add(hm);
            }
            if (al.isEmpty()) {
                al = null;
            }
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
        return al;
    }

    public boolean executeUpdate(final String string) {
        Statement st = null;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.createStatement();
            // System.out.println(string);
            st.executeUpdate(string);
            return true;
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public boolean executeUpdate(final String string, final String... string2) {
        PreparedStatement st = null;
        Connection con = null;
        boolean ret = true;
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement(string);
            for (int i = 0; i < string2.length; i++) {
                st.setString(i + 1, string2[i]);
            }
            // System.out.println(st.toString());
            st.executeUpdate();
        } catch (final SQLException e) {
            ret = false;
            e.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * gets the first matching entry of a column
     *
     * @param table
     *            the table in which the column should be searched
     * @param column
     *            the name of the column
     * @param cond
     *            the condition
     * @param condinsert
     *            the Strings that should be entered into the condition
     * @return the first entry of a column where the condition is true
     */
    public Object getColumn(final String table, final String column, final String cond,
            final String... condinsert) {
        Object ret = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement("SELECT " + column + " FROM " + table
                    + ("".equals(cond) || null == cond ? "" : " WHERE " + cond));
            int i = 0;
            if (null != condinsert) {
                for (final String s : condinsert) {
                    st.setString(++i, s);
                }
            }
            // System.out.println(st.toString());
            rs = st.executeQuery();
            if (rs.next()) {
                ret = rs.getObject(1);
            }
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * gets the Id of the user.
     *
     * @param usr
     *            the name of the user
     * @return the ID of the user
     */
    public long getID(final String usr) {
        PreparedStatement st = null;
        ResultSet rs = null;
        long ret = -1;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement("SELECT id FROM login WHERE Name=?");
            st.setString(1, usr);
            rs = st.executeQuery();
            if (rs.next()) {
                ret = rs.getLong("id");
            }
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ret;
    }

    public String getName(final long uid) {
        PreparedStatement st = null;
        ResultSet rs = null;
        String ret = null;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement("SELECT Name FROM login WHERE id=?");
            st.setLong(1, uid);
            rs = st.executeQuery();
            if (rs.next()) {
                ret = rs.getString("Name");
            }
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * returns the amount of users currently registered
     *
     * @return the amount of registered users
     */
    public int getUserAmount() {
        final int useramount = (int) getColumn("login", "MAX(id)", "");
        return useramount;
    }

    /**
     * inserts Values into selected columns of a table
     *
     * @param table
     *            the table
     * @param columns
     *            the columns
     * @param insert
     *            the data that should be inserted
     * @param values
     *            the values that should be entered into the table
     */
    public void insert(final String table, final String columns, final String insert,
            final String[] values) {
        PreparedStatement st = null;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement(
                    "INSERT INTO " + table + (columns.equals("*") ? "" : " (" + columns + ")")
                            + " VALUES(" + insert + ")");
            int i = 0;
            for (final String s : values) {
                st.setString(++i, s);
            }
            st.executeUpdate();
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private String isbanned(final String id) {
        // System.out.println("banned= "+(executeQuery("SELECT name FROM banned
        // WHERE name=?",name)!=null));
        final ArrayList<HashMap<String, Object>> executeQuery =
                executeQuery("SELECT reason FROM banned WHERE id=?", id);
        if (executeQuery == null || executeQuery.size() < 1) {
            return null;
        }
        return (String) executeQuery.get(0).get("reason");
    }

    /**
     * logs in the user
     *
     * @param name
     *            name of the user
     * @param pass
     *            password of the user
     * @param ip
     *            IP of the user
     * @return "success+id" if successful, error-kay if false
     */
    public String login(final String name, final String pass, final String ip) {
        PreparedStatement st = null;
        String ret;
        ResultSet rs = null;
        Connection con = null;
        if (!checkIP(ip, -1)) {
            // return "IpInUse";//TODO
        }
        String id = "";
        String passHash = "";
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement("SELECT * FROM login WHERE Name=?");
            st.setString(1, name);
            rs = st.executeQuery();
            if (!rs.next()) {
                return "NameUnknown";
            } else {
                id = rs.getString("id");
                passHash = rs.getString("Password");
            }
        } catch (final SQLException ex) {
            ex.printStackTrace();
            return "ErrorUnknown";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
        if (Strings.isNullOrEmpty(passHash)) {
            return "passwordlessAccount";
        }
        final PasswordCheckResult checkPw = EncryptionHelper.checkPw(pass, passHash);
        if (checkPw.isValid()) {
            updateUser(name, ip, checkPw.getRehashedPassword());
            final String banReason = isbanned(id);
            if (banReason != null) {
                ret = "banned " + banReason;
            } else {
                ret = "success" + id;
            }
        } else {
            ret = "PasswordWrong";
        }
        return ret;
    }

    public String possibleRegister(final String name, final String pass, final String comment,
            final String ref, final String ip) {
        if (getID(name) != -1) {
            return "NameInUse";
        }
        int id = 0;
        final Object o = getColumn("possibleRegister", "MAX(id)", "name=?", new String[] { name });
        if (o != null) {
            id = (int) o + 1;
        }
        if (id >= 5) {
            return "TooManyTries";
        }
        boolean refFailed = false;
        long refid = 0L;
        if (ref != null && ref.length() > 0) {
            try {
                refid = Token.getBigInt(ref).longValue();
                if (UserHelper.get(refid) == null) {
                    refid = 0;
                }
            } catch (final NumberFormatException e) {
                refid = 0;
            }
            if (refid == 0) {
                refid = UserHelper.get(ref);
                if (refid <= 0) {
                    refid = 0;
                    refFailed = true;
                }
            }
        }
        executeUpdate("INSERT INTO possibleRegister VALUES(?,?,?,?," + refid + "," + id + ")", name,
                pass, ip, comment);
        if (refFailed) {
            return "successRefFailed " + id;
        }
        return "success " + id;
    }

    public String possibleChangePass(final String name, final String password) {
        if (getID(name) == -1) {
            return "NameNotInUse";
        }
        int id = 0;
        final Object o = getColumn("possiblepwchange", "MAX(id)", "name=?", new String[] { name });
        if (o != null) {
            id = (int) o + 1;
        }
        if (id >= 5) {
            return "TooManyTries";
        }
        executeUpdate("INSERT INTO possiblepwchange VALUES(?,?," + id + ")", name, password);
        return "success" + id;
    }

    /**
     * updates the content of the table
     *
     * @param table
     *            the table which should be changed
     * @param wtc
     *            what should be changed
     * @param values
     *            the values that should be entered
     * @param cond
     *            the condition where something should be changed
     * @param condinsert
     *            the Strings that should be entered into the cond-string
     */
    public void update(final String table, final String wtc, final String[] values,
            final String cond, final String[] condinsert) {
        PreparedStatement st = null;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            st = con.prepareStatement("UPDATE " + table + " SET " + wtc
                    + ("".equals(cond) || null == cond ? "" : " WHERE " + cond));
            int i = 0;
            for (final String s : values) {
                st.setString(++i, s);
            }
            for (final String s : condinsert) {
                st.setString(++i, s);
            }
            st.executeUpdate();
        } catch (final SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void ban(final String name, final String reason) {
        executeUpdate("INSERT INTO banned VALUES(?,?)", name, reason);
    }

    public boolean executeAtomicUpdate(final List<String[]> querys) {
        boolean ret = true;
        Connection con = null;
        try {
            con = this.ds.getConnection();
            con.setAutoCommit(false);

            for (final String[] s : querys) {
                try (PreparedStatement st = con.prepareStatement(s[0])) {
                    for (int i = 1; i < s.length; i++) {
                        st.setString(i, s[i]);
                    }
                    // System.out.println(Arrays.toString(s));
                    st.execute();
                }
            }
            con.commit();
        } catch (final SQLException ex) {
            ret = false;
            ex.printStackTrace();
            try {
                con.rollback();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public Successindicator executeAtomic(final AtomicExecutor<Successindicator> function) {
        Connection con = null;
        try {
            con = this.ds.getConnection();
            con.setAutoCommit(false);

            final Successindicator result = function.apply(con);
            final boolean wasSuccessful = result != null && result.wasSuccessful();
            if (wasSuccessful) {
                con.commit();
            } else {
                con.rollback();
            }
            return result;
        } catch (final SQLException ex) {
            ex.printStackTrace();
            try {
                con.rollback();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            return () -> false;
        } catch (final Exception e) {
            e.printStackTrace();
            try {
                con.rollback();
            } catch (final SQLException ex) {
                ex.printStackTrace();
            }
            throw e;
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

}