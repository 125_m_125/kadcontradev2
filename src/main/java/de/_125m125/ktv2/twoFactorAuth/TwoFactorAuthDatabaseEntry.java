package de._125m125.ktv2.twoFactorAuth;

import de._125m125.ktv2.calculators.EncryptionHelper;
import de._125m125.ktv2.calculators.EncryptionHelper.EncryptedData;

public class TwoFactorAuthDatabaseEntry implements VerifierData {
    private final long    userid;
    private final String  type;
    private EncryptedData encryptedData;
    private String        data;

    public TwoFactorAuthDatabaseEntry(final long userid, final String type, final EncryptedData data) {
        super();
        this.userid = userid;
        this.type = type;
        this.encryptedData = data;
    }

    public TwoFactorAuthDatabaseEntry(final long userid, final String type, final String data) {
        super();
        this.userid = userid;
        this.type = type;
        this.data = data;
    }

    public long getUserid() {
        return this.userid;
    }

    public String getType() {
        return this.type;
    }

    public EncryptedData getEncryptedData() {
        if (this.encryptedData == null) {
            synchronized (this) {
                if (this.encryptedData == null) {
                    this.encryptedData = EncryptionHelper.aesEncrypt(this.data);
                }
            }
        }
        return this.encryptedData;
    }

    public String getData() {
        if (this.data == null) {
            synchronized (this) {
                if (this.data == null) {
                    this.data = EncryptionHelper.aesDecrypt(this.encryptedData);
                }
            }
        }
        return this.data;
    }

    @Override
    public boolean store() {
        return AuthState.storeEntry(this);
    }

}