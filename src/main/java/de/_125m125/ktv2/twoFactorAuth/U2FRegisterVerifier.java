package de._125m125.ktv2.twoFactorAuth;

import com.yubico.u2f.U2F;
import com.yubico.u2f.data.DeviceRegistration;
import com.yubico.u2f.data.messages.RegisterRequestData;
import com.yubico.u2f.data.messages.RegisterResponse;
import com.yubico.u2f.exceptions.U2fBadInputException;
import com.yubico.u2f.exceptions.U2fRegistrationException;

public class U2FRegisterVerifier implements Verifier {

    private final U2F                  u2f;
    private final RegisterRequestData  registerRequestData;
    private final long                 uid;
    private TwoFactorAuthDatabaseEntry twoFactorAuthDatabaseEntry;

    public U2FRegisterVerifier(final U2F u2f, final long uid, final RegisterRequestData registerRequestData) {
        this.u2f = u2f;
        this.uid = uid;
        this.registerRequestData = registerRequestData;
    }

    @Override
    public boolean verify(final String password, final boolean userInvoked) throws InvalidatedException {
        if (!userInvoked) {
            return false;
        }
        try {
            final DeviceRegistration deviceRegistration = this.u2f.finishRegistration(this.registerRequestData,
                    RegisterResponse.fromJson(password));
            this.twoFactorAuthDatabaseEntry = new TwoFactorAuthDatabaseEntry(this.uid, "UF",
                    deviceRegistration.toJson());
            return true;
        } catch (U2fRegistrationException | U2fBadInputException e) {
            return false;
        }
    }

    @Override
    public void initialize() {
    }

    @Override
    public VerifierData getData() {
        return this.twoFactorAuthDatabaseEntry;
    }

}
