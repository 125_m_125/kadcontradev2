package de._125m125.ktv2.twoFactorAuth;

public class AuthMethodList {
    private boolean mail;
    private boolean totp;
    private boolean otp;
    private boolean u2f;
    private boolean cert;
    private boolean webauthn;

    private int     size = 0;

    public boolean isMail() {
        return this.mail;
    }

    public void setMail(final boolean mail) {
        if (this.mail) {
            return;
        }
        this.mail = mail;
        this.size++;
    }

    public boolean isTotp() {
        return this.totp;
    }

    public void setTotp(final boolean totp) {
        if (this.totp) {
            return;
        }
        this.totp = totp;
        this.size++;
    }

    public boolean isOtp() {
        return this.otp;
    }

    public void setOtp(final boolean otp) {
        if (this.otp) {
            return;
        }
        this.otp = otp;
        this.size++;
    }

    public boolean isU2f() {
        return this.u2f;
    }

    public void setU2f(final boolean u2f) {
        if (this.u2f) {
            return;
        }
        this.u2f = u2f;
        this.size++;
    }

    public int getSize() {
        return this.size;
    }

    public boolean isCert() {
        return this.cert;
    }

    public void setCert(final boolean cert) {
        if (this.cert) {
            return;
        }
        this.cert = cert;
        this.size++;
    }

    public boolean isWebauthn() {
        return this.webauthn;
    }

    public void setWebauthn(final boolean b) {
        if (this.webauthn) {
            return;
        }
        this.webauthn = b;
        this.size++;
    }

}
