package de._125m125.ktv2.twoFactorAuth;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.yubico.webauthn.AssertionRequest;
import com.yubico.webauthn.AssertionResult;
import com.yubico.webauthn.FinishAssertionOptions;
import com.yubico.webauthn.StartAssertionOptions;
import com.yubico.webauthn.data.AuthenticatorAssertionResponse;
import com.yubico.webauthn.data.ClientAssertionExtensionOutputs;
import com.yubico.webauthn.data.PublicKeyCredential;
import com.yubico.webauthn.exception.AssertionFailedException;

public class WebAuthNAuthenticationVerifier implements Verifier {

    private final WebAuthNAuthenticatorData data;
    private final AssertionRequest          request;

    public WebAuthNAuthenticationVerifier(final WebAuthNAuthenticatorData data) {
        this.data = data;

        this.request = data.rp.startAssertion(
                StartAssertionOptions.builder().username(Optional.of(data.username)).build());
    }

    @Override
    public void initialize() {
    }

    @Override
    public boolean verify(final String password, final boolean userInvoked)
            throws InvalidatedException {
        try {
            final PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs> pkc = WebAuthNAuthenticatorData.jsonMapper
                    .readValue(password,
                            new TypeReference<PublicKeyCredential<AuthenticatorAssertionResponse, ClientAssertionExtensionOutputs>>() {
                            });

            final AssertionResult result = this.data.rp.finishAssertion(
                    FinishAssertionOptions.builder().request(this.request).response(pkc).build());
            if (result.isSuccess()) {
                return true;
            }
        } catch (final AssertionFailedException | IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    @Override
    public VerifierData getData() {
        return this.data;
    }

    @Override
    public Object getPublicData() {
        try {
            return WebAuthNAuthenticatorData.jsonMapper.writeValueAsString(this.request);
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

}
