package de._125m125.ktv2.twoFactorAuth;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Iterator;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;

import de._125m125.ktv2.mail.EncryptionType;

public class MailAuthenticatorJson implements VerifierData {

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public static MailAuthenticatorJson decrypt(final long uid, final String data) {
        final String[] parts = data.split(":", 3);
        if (parts.length != 3) {
            throw new IllegalArgumentException("expected 3 parts, got " + parts.length);
        }
        String mail;
        final EncryptionType type = EncryptionType.valueOf(parts[1]);
        Object certificate;
        try {
            mail = new String(Base64.getDecoder().decode(parts[0]), "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            throw new IllegalArgumentException("invalid mail in input", e);
        }
        switch (parts[1]) {
        case "NONE":
            certificate = null;
            break;
        case "SMIME":
            try {
                certificate = getSMIMECertificate(parts[2]);
            } catch (final CertificateException e) {
                throw new IllegalArgumentException("invalid certificate", e);
            }
            break;
        case "PGP":
            try {
                certificate = getPGPPublicKey(parts[2]);
            } catch (IOException | PGPException e) {
                throw new IllegalArgumentException("invalid public key", e);
            }
            break;
        default:
            throw new IllegalArgumentException("unknown encryption type");
        }
        return new MailAuthenticatorJson(uid, mail, type, parts[2], certificate);
    }

    private static X509Certificate getSMIMECertificate(final String string)
            throws CertificateException {
        final CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
        return (X509Certificate) certificateFactory.generateCertificate(
                new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
    }

    private static PGPPublicKey getPGPPublicKey(final String string)
            throws IOException, PGPException {
        try (InputStream in = PGPUtil.getDecoderStream(
                new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)))) {
            final JcaPGPPublicKeyRingCollection pgpPub = new JcaPGPPublicKeyRingCollection(in);

            final Iterator<PGPPublicKeyRing> rIt = pgpPub.getKeyRings();
            while (rIt.hasNext()) {
                final PGPPublicKeyRing kRing = rIt.next();
                final Iterator<PGPPublicKey> kIt = kRing.getPublicKeys();
                while (kIt.hasNext()) {
                    final PGPPublicKey k = kIt.next();
                    if (k.isEncryptionKey()) {
                        return k;
                    }
                }
            }
            throw new PGPException("No PublicKey contained...");
        }
    }

    public static String encode(final MailAuthenticatorJson mailAuthenticatorJson) {
        final StringBuilder result = new StringBuilder();
        try {
            result.append(Base64.getEncoder()
                    .encodeToString(mailAuthenticatorJson.mail.getBytes("UTF-8")));
        } catch (final UnsupportedEncodingException e) {
        }
        result.append(":");
        result.append(mailAuthenticatorJson.type);
        result.append(":");
        result.append(mailAuthenticatorJson.encodedCertificate);
        return result.toString();
    }

    private final long           uid;
    private final String         mail;
    private final EncryptionType type;
    private final String         encodedCertificate;
    private final Object         certificate;

    public MailAuthenticatorJson(final long uid, final String mail, final EncryptionType type,
            final String encodedCertificate, final Object certificate) {
        super();
        this.uid = uid;
        this.mail = mail;
        this.type = type;
        this.encodedCertificate = encodedCertificate;
        this.certificate = certificate;
    }

    public MailAuthenticatorJson(final long uid, final String mail, final EncryptionType type,
            final String encodedCertificate)
            throws CertificateException, IOException, PGPException {
        super();
        this.uid = uid;
        this.mail = mail;
        this.type = type;
        this.encodedCertificate = encodedCertificate;
        switch (type) {
        case SMIME:
            final X509Certificate cert = getSMIMECertificate(encodedCertificate);
            this.certificate = cert;
            break;
        case PGP:
            this.certificate = getPGPPublicKey(encodedCertificate);
            break;
        default:
            this.certificate = null;
        }
    }

    /* (non-Javadoc)
     * @see de._125m125.ktv2.twoFactorAuth.StorableVerifierData#store()
     */
    @Override
    public boolean store() {
        return AuthState.replaceEntry(new TwoFactorAuthDatabaseEntry(this.uid, "MA", encode()));
    }

    public String encode() {
        return encode(this);
    }

    public long getUid() {
        return this.uid;
    }

    public String getMail() {
        return this.mail;
    }

    public EncryptionType getType() {
        return this.type;
    }

    public Object getCertificate() {
        return this.certificate;
    }

}
