package de._125m125.ktv2.twoFactorAuth;

import javax.servlet.http.HttpServletRequest;

public interface Verifier {
    public void initialize();

    public boolean verify(String password, boolean userInvoked) throws InvalidatedException;

    public VerifierData getData();

    public default Object getPublicData() {
        return true;
    }

    public default boolean verify(final HttpServletRequest r) {
        return false;
    }

}