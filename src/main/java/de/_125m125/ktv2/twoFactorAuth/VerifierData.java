package de._125m125.ktv2.twoFactorAuth;

public interface VerifierData {

    boolean store();
}