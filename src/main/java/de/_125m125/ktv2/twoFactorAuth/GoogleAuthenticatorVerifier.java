package de._125m125.ktv2.twoFactorAuth;

import com.warrenstrange.googleauth.GoogleAuthenticator;

public class GoogleAuthenticatorVerifier implements Verifier {

    private static final GoogleAuthenticator GOOGLE_AUTHENTICATOR = new GoogleAuthenticator();

    public static GoogleAuthenticator getGoogleAuthenticator() {
        return GoogleAuthenticatorVerifier.GOOGLE_AUTHENTICATOR;
    }

    private final String            secret;
    private GoogleAuthenticatorJson googleAuthenticatorJson;

    public GoogleAuthenticatorVerifier(final String data) {
        this.secret = data;
    }

    public GoogleAuthenticatorVerifier(final GoogleAuthenticatorJson googleAuthenticatorJson) {
        this.googleAuthenticatorJson = googleAuthenticatorJson;
        this.secret = googleAuthenticatorJson.getSecret();
    }

    @Override
    public boolean verify(final String password, final boolean userInvoked) {
        if (password == null) {
            return false;
        }
        try {
            final int verificationCode = Integer.parseInt(password);
            return GoogleAuthenticatorVerifier.getGoogleAuthenticator().authorize(this.secret, verificationCode);
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void initialize() {
    }

    @Override
    public GoogleAuthenticatorJson getData() {
        return this.googleAuthenticatorJson;
    }

}
