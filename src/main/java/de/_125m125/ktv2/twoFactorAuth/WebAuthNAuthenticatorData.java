package de._125m125.ktv2.twoFactorAuth;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.yubico.webauthn.CredentialRepository;
import com.yubico.webauthn.RegisteredCredential;
import com.yubico.webauthn.RegistrationResult;
import com.yubico.webauthn.RelyingParty;
import com.yubico.webauthn.data.ByteArray;
import com.yubico.webauthn.data.PublicKeyCredentialDescriptor;
import com.yubico.webauthn.data.RelyingPartyIdentity;

import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;

public class WebAuthNAuthenticatorData implements CredentialRepository, VerifierData {
    public static final ObjectMapper jsonMapper =
            new ObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                    .setSerializationInclusion(Include.NON_ABSENT).registerModule(new Jdk8Module());

    public static WebAuthNAuthenticatorData forUser(final long uid, final String domain) {
        final List<TwoFactorAuthDatabaseEntry> loadEntries = AuthState.loadEntries(uid);
        final Set<RegistrationResult> registrations =
                loadEntries.stream().filter(e -> "WA".equals(e.getType())).map(e -> {
                    try {
                        return WebAuthNAuthenticatorData.jsonMapper.readValue(e.getData(),
                                RegistrationResult.class);
                    } catch (final IOException e1) {
                        e1.printStackTrace();
                        throw new IllegalStateException(e1);
                    }
                }).collect(Collectors.toSet());
        return new WebAuthNAuthenticatorData(UserHelper.get(uid), uid, domain, registrations);
    }

    public static class Builder {
        private final long                    userId;
        private final Set<RegistrationResult> descriptors = new HashSet<>();
        private final String                  domain;

        public Builder(final long userId, final String domain) {
            this.userId = userId;
            this.domain = domain;
        }

        public void addRegistrationResult(final String data) {
            try {
                this.descriptors.add(WebAuthNAuthenticatorData.jsonMapper.readValue(data,
                        RegistrationResult.class));
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }

        public WebAuthNAuthenticatorData build() {
            if (this.descriptors.isEmpty()) {
                return null;
            }
            return new WebAuthNAuthenticatorData(UserHelper.get(this.userId), this.userId,
                    this.domain, this.descriptors);
        }
    }

    private final Set<RegistrationResult> descriptors;
    public final String                   username;
    public final ByteArray                id;
    public final long                     userId;
    public final RelyingParty             rp;

    public WebAuthNAuthenticatorData(final String username, final long id, final String domain,
            final Set<RegistrationResult> descriptors) {
        this.username = username;
        this.userId = id;
        this.id = new ByteArray(ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(id).array());
        this.descriptors = descriptors;

        final RelyingPartyIdentity rpIdentity = RelyingPartyIdentity.builder()
                .id(domain.replaceAll(".*\\.(?=.*\\.)", "")).name(Variables.getAppName()).build();

        this.rp = RelyingParty.builder().identity(rpIdentity).credentialRepository(this).build();
    }

    @Override
    public Set<PublicKeyCredentialDescriptor> getCredentialIdsForUsername(final String username) {
        if (this.username.equals(username)) {
            return this.descriptors.stream().map(RegistrationResult::getKeyId)
                    .collect(Collectors.toSet());
        }
        return new HashSet<>();
    }

    @Override
    public Optional<ByteArray> getUserHandleForUsername(final String username) {
        if (this.username.equals(username)) {
            return Optional.of(this.id);
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> getUsernameForUserHandle(final ByteArray userHandle) {
        if (userHandle.compareTo(this.id) == 0) {
            return Optional.of(this.username);
        }
        return Optional.empty();
    }

    @Override
    public Optional<RegisteredCredential> lookup(final ByteArray credentialId,
            final ByteArray userHandle) {
        if (this.id.compareTo(userHandle) != 0) {
            return Optional.empty();
        }
        System.out.println("WebAuthNAuthenticatorData.lookup()2");
        return this.descriptors.stream()
                .filter(rr -> credentialId.compareTo(rr.getKeyId().getId()) == 0).findAny()
                .map(rr -> RegisteredCredential.builder().credentialId(rr.getKeyId().getId())
                        .userHandle(this.id).publicKeyCose(rr.getPublicKeyCose()).build());
    }

    @Override
    public Set<RegisteredCredential> lookupAll(final ByteArray credentialId) {
        return this.descriptors.stream()
                .filter(rr -> credentialId.compareTo(rr.getKeyId().getId()) == 0)
                .map(rr -> RegisteredCredential.builder().credentialId(rr.getKeyId().getId())
                        .userHandle(this.id).publicKeyCose(rr.getPublicKeyCose()).build())
                .collect(Collectors.toSet());
    }

    @Override
    public boolean store() {
        throw new UnsupportedOperationException();
    }

}
