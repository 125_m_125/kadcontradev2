package de._125m125.ktv2.twoFactorAuth;

import com.yubico.u2f.U2F;
import com.yubico.u2f.data.messages.SignRequestData;
import com.yubico.u2f.data.messages.SignResponse;
import com.yubico.u2f.exceptions.NoEligibleDevicesException;
import com.yubico.u2f.exceptions.U2fAuthenticationException;
import com.yubico.u2f.exceptions.U2fBadConfigurationException;
import com.yubico.u2f.exceptions.U2fBadInputException;

public class U2FAuthentificationVerifier implements Verifier {
    private final U2FAuthenticatorData data;
    private final U2F                  u2f = new U2F();
    private SignRequestData            signRequestData;

    public U2FAuthentificationVerifier(final U2FAuthenticatorData data) {
        this.data = data;
        try {
            this.signRequestData =
                    this.u2f.startSignature("https://" + data.getDomain(), data.getDevices());
        } catch (NoEligibleDevicesException | U2fBadConfigurationException e) {
            e.printStackTrace();
            this.signRequestData = null;
        }
    }

    @Override
    public void initialize() {
    }

    @Override
    public boolean verify(final String password, final boolean userInvoked)
            throws InvalidatedException {
        final SignResponse signResponse;
        try {
            signResponse = SignResponse.fromJson(password);
        } catch (final U2fBadInputException e) {
            return false;
        }
        try {
            this.u2f.finishSignature(this.signRequestData, signResponse, this.data.getDevices());
            return true;
        } catch (final U2fAuthenticationException e) {
            return false;
        }

    }

    @Override
    public VerifierData getData() {
        return this.data;
    }

    @Override
    public Object getPublicData() {
        return this.signRequestData.toJson();
    }
}
