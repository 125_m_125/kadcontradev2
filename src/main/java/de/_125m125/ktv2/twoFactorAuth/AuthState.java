package de._125m125.ktv2.twoFactorAuth;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.yubico.u2f.data.DeviceRegistration;
import com.yubico.u2f.exceptions.U2fBadInputException;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.beans.ClientCertificate.Type;
import de._125m125.ktv2.calculators.EncryptionHelper.EncryptedData;

public class AuthState {
    public static final String COOKIE_NAME = "mfa";

    public static AuthState createFor(final long uid, final HttpServletRequest request) {
        BigInteger cookieValue = null;
        final Cookie[] cookies = request.getCookies();
        final String domain = request.getServerName();
        if (cookies != null) {
            for (final Cookie cookie : cookies) {
                if (AuthState.COOKIE_NAME.equals(cookie.getName())) {
                    try {
                        cookieValue = new BigInteger(cookie.getValue(), 32);
                    } catch (final NumberFormatException e) {
                    }
                }
            }
        }
        if (isAllowed(uid, request.getRemoteAddr(), cookieValue)) {
            return new AuthState(true);
        }
        final List<TwoFactorAuthDatabaseEntry> entries = loadEntries(uid);
        if (entries == null || entries.isEmpty()) {
            return new AuthState(true);
        }
        final Map<String, Verifier> verifiers = new HashMap<>();
        final List<DeviceRegistration> u2fRegistrations = new ArrayList<>();
        final WebAuthNAuthenticatorData.Builder wanBuilder =
                new WebAuthNAuthenticatorData.Builder(uid, domain);
        for (final TwoFactorAuthDatabaseEntry twoFactorAuthDatabaseEntry : entries) {
            final Verifier verifier;
            switch (twoFactorAuthDatabaseEntry.getType()) {
            case "SC":
                final ScratchCodeVerifier v = (ScratchCodeVerifier) verifiers.computeIfAbsent("SC",
                        s -> new ScratchCodeVerifier());
                v.addScratchCode(twoFactorAuthDatabaseEntry);
                verifier = v;
                break;
            case "GA":
                verifier = new GoogleAuthenticatorVerifier(twoFactorAuthDatabaseEntry.getData());
                verifiers.put("GA", verifier);
                break;
            case "MA":
                verifier = new MailAuthenticatorVerifier(uid, twoFactorAuthDatabaseEntry.getData());
                verifiers.put("MA", verifier);
                break;
            case "UF":
                try {
                    verifier = null;
                    u2fRegistrations
                            .add(DeviceRegistration.fromJson(twoFactorAuthDatabaseEntry.getData()));
                } catch (final U2fBadInputException e1) {
                    throw new IllegalArgumentException(e1);
                }
                break;
            case "WA":
                verifier = null;
                wanBuilder.addRegistrationResult(twoFactorAuthDatabaseEntry.getData());
                break;
            case "CT":
                if (!verifiers.containsKey("CT")) {
                    verifier = new CertificateVerifier();
                    verifiers.put("CT", verifier);
                } else {
                    verifier = null;
                }
                break;
            default:
                verifier = null;
            }
            if (verifier != null) {
                verifier.initialize();
            }
        }
        if (!u2fRegistrations.isEmpty()) {
            final U2FAuthentificationVerifier u2fAuthentificationVerifier =
                    new U2FAuthentificationVerifier(
                            new U2FAuthenticatorData(u2fRegistrations, domain));
            u2fAuthentificationVerifier.initialize();
            verifiers.put("UF", u2fAuthentificationVerifier);
        }
        final WebAuthNAuthenticatorData build = wanBuilder.build();
        if (build != null) {
            final WebAuthNAuthenticationVerifier webAuthNAuthenticationVerifier =
                    new WebAuthNAuthenticationVerifier(build);
            webAuthNAuthenticationVerifier.initialize();
            verifiers.put("WA", webAuthNAuthenticationVerifier);
        }
        if (verifiers.isEmpty()) {
            return new AuthState(false);
        }
        final AuthState authState = new AuthState(verifiers);
        authState.verify(request);
        return authState;
    }

    public static AuthMethodList getAuthMethods(final long uid) {
        final AuthMethodList result = new AuthMethodList();
        try (Connection c = Main.instance().getConnector().getConnection()) {
            try (PreparedStatement ps =
                    c.prepareStatement("SELECT type FROM twoFactorAuth WHERE uid=?")) {
                ps.setLong(1, uid);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        final String string = rs.getString(1);
                        switch (string) {
                        case "SC":
                            result.setOtp(true);
                            break;
                        case "GA":
                            result.setTotp(true);
                            break;
                        case "MA":
                            result.setMail(true);
                            break;
                        case "UF":
                            result.setU2f(true);
                            break;
                        case "WA":
                            result.setWebauthn(true);
                            break;
                        }
                    }
                }
            }
            try (PreparedStatement ps = c.prepareStatement(
                    "SELECT serialNr FROM certificates WHERE user=? AND type=?")) {
                ps.setLong(1, uid);
                ps.setInt(2, Type.TWO_FACTOR_AUTH.getMysqlId());
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        result.setCert(true);
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isAllowed(final long uid, final String ip, final BigInteger cookieValue) {
        try (Connection c = Main.instance().getConnector().getConnection()) {
            if (cookieValue != null) {
                try (PreparedStatement st = c.prepareStatement(
                        "SELECT TRUE FROM twoFactorCookie WHERE id=? AND cookie=?")) {
                    st.setLong(1, uid);
                    byte[] byteArray = cookieValue.toByteArray();
                    if (byteArray[0] == 0) {
                        byteArray = Arrays.copyOfRange(byteArray, 1, byteArray.length - 1);
                    }
                    st.setBytes(2, byteArray);
                    final ResultSet executeQuery = st.executeQuery();
                    if (executeQuery.next()) {
                        return true;
                    }
                }
            }
            if (ip != null) {
                try (PreparedStatement st =
                        c.prepareStatement("SELECT TRUE FROM twoFactorIP WHERE id=? AND ip=?")) {
                    st.setLong(1, uid);
                    st.setString(2, ip);
                    final ResultSet executeQuery = st.executeQuery();
                    if (executeQuery.next()) {
                        return true;
                    }
                }
            }
            return false;
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static List<TwoFactorAuthDatabaseEntry> loadEntries(final long userid) {
        final List<TwoFactorAuthDatabaseEntry> result = new ArrayList<>();
        try (Connection c = Main.instance().getConnector().getConnection()) {
            try (PreparedStatement st =
                    c.prepareStatement("SELECT * FROM twoFactorAuth WHERE uid=?")) {
                st.setLong(1, userid);
                try (ResultSet rs = st.executeQuery()) {
                    while (rs.next()) {
                        result.add(new TwoFactorAuthDatabaseEntry(userid, rs.getString("type"),
                                new EncryptedData(rs.getBytes("data"), rs.getBytes("iv"))));
                    }
                }
                try (PreparedStatement ps = c.prepareStatement(
                        "SELECT serialNr FROM certificates WHERE user=? AND type=?")) {
                    ps.setLong(1, userid);
                    ps.setInt(2, Type.TWO_FACTOR_AUTH.getMysqlId());
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            result.add(new TwoFactorAuthDatabaseEntry(userid, "CT", ""));
                        }
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    public static boolean replaceAllEntries(final TwoFactorAuthDatabaseEntry entry) {
        return replaceAllEntries(entry.getUserid(), Arrays.asList(entry));
    }

    public static boolean storeEntry(final TwoFactorAuthDatabaseEntry entry) {
        return Main.instance().getConnector().executeAtomic((c) -> {
            storeEntries(entry.getUserid(), Collections.singleton(entry), c);
            return () -> true;
        }).wasSuccessful();
    }

    public static boolean replaceAllEntries(final long uid,
            final Iterable<TwoFactorAuthDatabaseEntry> entries) {
        return Main.instance().getConnector().executeAtomic((c) -> {
            try (PreparedStatement ps =
                    c.prepareStatement("DELETE FROM twoFactorAuth WHERE uid=?")) {
                ps.setLong(1, uid);
                ps.executeUpdate();
            }
            storeEntries(uid, entries, c);
            return () -> true;
        }).wasSuccessful();
    }

    private static void storeEntries(final long uid,
            final Iterable<TwoFactorAuthDatabaseEntry> entries, final Connection c)
            throws SQLException {
        try (PreparedStatement ps = c.prepareStatement(
                "INSERT INTO twoFactorAuth(uid, type, data, iv) VALUES(?,?,?,?)")) {
            ps.setLong(1, uid);
            for (final TwoFactorAuthDatabaseEntry entry : entries) {
                ps.setString(2, entry.getType());
                ps.setBytes(3, entry.getEncryptedData().getCyphertext());
                ps.setBytes(4, entry.getEncryptedData().getIv());
                ps.executeUpdate();
            }
        }
    }

    public static boolean replaceEntry(final TwoFactorAuthDatabaseEntry entry) {
        return Main.instance().getConnector().executeAtomic((c) -> {
            try (PreparedStatement ps =
                    c.prepareStatement("DELETE FROM twoFactorAuth WHERE uid=? AND type=?")) {
                ps.setLong(1, entry.getUserid());
                ps.setString(2, entry.getType());
                ps.executeUpdate();
            }
            storeEntries(entry.getUserid(), Collections.singleton(entry), c);
            return () -> true;
        }).wasSuccessful();
    }

    public static boolean replaceEntriesByType(final long uid,
            final Collection<TwoFactorAuthDatabaseEntry> entries) {
        final Set<String> types = entries.stream().map(TwoFactorAuthDatabaseEntry::getType)
                .collect(Collectors.toSet());
        return Main.instance().getConnector().executeAtomic((c) -> {
            try (PreparedStatement ps =
                    c.prepareStatement("DELETE FROM twoFactorAuth WHERE uid=? AND type=?")) {
                ps.setLong(1, uid);
                for (final String type : types) {
                    ps.setString(2, type);
                    ps.addBatch();
                }
                ps.executeBatch();
            }
            storeEntries(uid, entries, c);
            return () -> true;
        }).wasSuccessful();
    }

    private final AtomicInteger         tries = new AtomicInteger(3);
    private final AtomicBoolean         authenticated;
    private final Map<String, Verifier> verifier;
    private final boolean               error;

    public AuthState(final Map<String, Verifier> verifier) {
        this.verifier = verifier;
        this.authenticated = new AtomicBoolean(false);
        this.error = false;
    }

    public AuthState(final boolean verified) {
        this.authenticated = new AtomicBoolean(verified);
        this.verifier = new HashMap<>();
        this.error = !verified;
    }

    public boolean verify(final String type, final String password, final boolean userInvoked)
            throws InvalidatedException {
        final boolean b = this.authenticated.get();
        if (b) {
            return true;
        }
        if (userInvoked) {
            if (this.tries.decrementAndGet() < 0) {
                return false;
            }
        }
        final Verifier verifier = this.verifier.get(type);
        if (verifier == null) {
            return false;
        }
        final boolean result = verifier.verify(password, userInvoked);
        if (result) {
            this.authenticated.set(true);
        }
        return result;
    }

    public boolean verify(final HttpServletRequest request) {
        this.authenticated.compareAndSet(false, this.verifier.values().stream().reduce(false,
                (p, v) -> p || v.verify(request), (a, b) -> a || b));
        return this.authenticated.get();
    }

    public boolean isAuthenticated() {
        return this.authenticated.get();
    }

    public boolean triesLimitReached() {
        return this.tries.get() < 1;
    }

    public boolean hadError() {
        return this.error;
    }

    public Map<String, Object> getPublicInfos() {
        return this.verifier.entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, e -> e.getValue().getPublicData()));
    }

}