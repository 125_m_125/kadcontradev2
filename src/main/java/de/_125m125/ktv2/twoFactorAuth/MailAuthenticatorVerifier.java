package de._125m125.ktv2.twoFactorAuth;

import java.security.SecureRandom;
import java.util.Random;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.mail.MailLockedException;
import de._125m125.ktv2.mail.MailManager;
import de._125m125.ktv2.mail.MailOnDelayException;
import de._125m125.ktv2.mail.MailSendException;

public class MailAuthenticatorVerifier implements Verifier {

    private static final MailManager    MAIL_MANAGER = new MailManager();

    private static final Random         r            = new SecureRandom();

    private String                      expected;
    private final MailAuthenticatorJson authenticatorJson;
    private String                      errorMessage;

    public MailAuthenticatorVerifier(final long uid, final String data) {
        this.authenticatorJson = MailAuthenticatorJson.decrypt(uid, data);
    }

    public MailAuthenticatorVerifier(final MailAuthenticatorJson authenticatorJson) {
        this.authenticatorJson = authenticatorJson;
    }

    @Override
    public boolean verify(final String password, final boolean userInvoked) throws InvalidatedException {
        if (this.errorMessage != null) {
            if (MailAuthenticatorVerifier.MAIL_MANAGER.read() == 0) {
                throw new InvalidatedException();
            }
        }
        final boolean b = this.expected != null && this.expected.equals(password);
        if (b) {
            MailAuthenticatorVerifier.MAIL_MANAGER.resetCount(this.authenticatorJson.getMail());
        }
        return b;
    }

    @Override
    public VerifierData getData() {
        return this.authenticatorJson;
    }

    @Override
    public synchronized void initialize() {
        if (this.expected != null) {
            return;
        }
        this.expected = String.valueOf(MailAuthenticatorVerifier.r.nextInt(1000000));
        sendMail();
    }

    private void sendMail() {
        try {
            MailAuthenticatorVerifier.MAIL_MANAGER.sendMail(this.authenticatorJson.getMail(),
                    Variables.translate("2faMailSubject"), Variables.translate("2faMailContent", this.expected),
                    this.authenticatorJson.getType(), this.authenticatorJson.getCertificate());
        } catch (final MailLockedException e) {
            this.errorMessage = Variables.translate("mailLocked", this.authenticatorJson.getMail(),
                    Variables.getString("mailUsername"));
        } catch (final MailOnDelayException e) {
            this.errorMessage = Variables.translate("mailOnDelay", e.getNextMessage() / 1000 / 60 + 1);
        } catch (final MailSendException e) {
            this.errorMessage = Variables.translate(e.getMessage());
        }
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

}
