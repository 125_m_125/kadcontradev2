package de._125m125.ktv2.twoFactorAuth;

import java.util.List;
import java.util.stream.Collectors;

import com.yubico.u2f.data.DeviceRegistration;
import com.yubico.u2f.exceptions.U2fBadInputException;

public class U2FAuthenticatorData implements VerifierData {

    public static U2FAuthenticatorData forUser(final long id, final String domain) {
        final List<TwoFactorAuthDatabaseEntry> loadEntries = AuthState.loadEntries(id);
        final List<DeviceRegistration> registrations =
                loadEntries.stream().filter(e -> "UF".equals(e.getType())).map(e -> {
                    try {
                        return DeviceRegistration.fromJson(e.getData());
                    } catch (final U2fBadInputException e1) {
                        throw new IllegalArgumentException(e1);
                    }
                }).collect(Collectors.toList());
        return new U2FAuthenticatorData(registrations, domain);
    }

    private final Iterable<? extends DeviceRegistration> registrations;
    private final String                                 domain;

    public U2FAuthenticatorData(final Iterable<? extends DeviceRegistration> registrations,
            final String domain) {
        this.registrations = registrations;
        this.domain = domain;
    }

    @Override
    public boolean store() {
        throw new UnsupportedOperationException();
    }

    public Iterable<? extends DeviceRegistration> getDevices() {
        return this.registrations;
    }

    public String getDomain() {
        return this.domain;
    }

}
