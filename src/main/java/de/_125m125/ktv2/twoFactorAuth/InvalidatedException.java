package de._125m125.ktv2.twoFactorAuth;

public class InvalidatedException extends RuntimeException {

    private static final long serialVersionUID = -6072763278637456956L;

    public InvalidatedException() {
        super();
    }

    public InvalidatedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidatedException(final String message) {
        super(message);
    }

    public InvalidatedException(final Throwable cause) {
        super(cause);
    }

}
