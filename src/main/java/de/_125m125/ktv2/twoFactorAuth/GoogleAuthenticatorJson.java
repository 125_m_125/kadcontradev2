package de._125m125.ktv2.twoFactorAuth;

import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator;

import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;

public class GoogleAuthenticatorJson implements VerifierData {

    public static GoogleAuthenticatorJson createNew(final long id) {
        final GoogleAuthenticatorKey key =
                GoogleAuthenticatorVerifier.getGoogleAuthenticator().createCredentials();
        return new GoogleAuthenticatorJson(id, key.getKey(), GoogleAuthenticatorQRGenerator
                .getOtpAuthTotpURL(Variables.getAppName(), UserHelper.get(id), key));
    }

    private final long   uid;
    private final String secret;
    private final String qrUrl;

    public GoogleAuthenticatorJson(final long uid, final String secret, final String qrUrl) {
        super();
        this.uid = uid;
        this.secret = secret;
        this.qrUrl = qrUrl;
    }

    public String getSecret() {
        return this.secret;
    }

    public String getQrUrl() {
        return this.qrUrl;
    }

    /* (non-Javadoc)
     * @see de._125m125.ktv2.twoFactorAuth.StorableVerifierData#store()
     */
    @Override
    public boolean store() {
        return AuthState.replaceEntry(new TwoFactorAuthDatabaseEntry(this.uid, "GA", this.secret));
    }

}
