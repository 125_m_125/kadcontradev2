package de._125m125.ktv2.twoFactorAuth;

import javax.servlet.http.HttpServletRequest;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.calculators.CertificateManager;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class CertificateVerifier implements Verifier {
    String error = null;

    @Override
    public void initialize() {
    }

    @Override
    public boolean verify(final String password, final boolean userInvoked) throws InvalidatedException {
        return false;
    }

    @Override
    public boolean verify(final HttpServletRequest request) {
        final int certificatePermissions = CertificateManager.instance()
                .getCertificatePermissions((long) request.getSession().getAttribute("id"), request);
        if (Permission.TWO_FA_PERMISSION.isPresentIn(certificatePermissions)) {
            return true;
        } else {
            if (certificatePermissions == 0) {
                this.error = "certificateMissingOrInvalid";
            } else {
                this.error = "certificate2faMissing";
            }
            return false;
        }
    }

    @Override
    public VerifierData getData() {
        return null;
    }

    @Override
    public Object getPublicData() {
        return Variables.translate(this.error);
    }

}
