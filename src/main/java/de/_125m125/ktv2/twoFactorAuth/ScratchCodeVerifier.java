package de._125m125.ktv2.twoFactorAuth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import de._125m125.ktv2.Main;

public class ScratchCodeVerifier implements Verifier {
    private final Map<String, TwoFactorAuthDatabaseEntry> scratchCodes = new HashMap<>();

    public void addScratchCode(final TwoFactorAuthDatabaseEntry entry) {
        this.scratchCodes.put(entry.getData(), entry);
    }

    @Override
    public void initialize() {
    }

    @Override
    public boolean verify(final String password, final boolean userInvoked) throws InvalidatedException {
        final TwoFactorAuthDatabaseEntry scratchCode = this.scratchCodes.get(password);
        if (scratchCode != null) {
            return invalidateScratchCode(scratchCode);
        }
        return false;
    }

    private boolean invalidateScratchCode(final TwoFactorAuthDatabaseEntry scratchCode) {
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c
                        .prepareStatement("DELETE FROM twoFactorAuth WHERE uid=? AND type=? AND data=?")) {
            ps.setLong(1, scratchCode.getUserid());
            ps.setString(2, "SC");
            ps.setBytes(3, scratchCode.getEncryptedData().getCyphertext());
            final int updateCount = ps.executeUpdate();
            return updateCount == 1;
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public VerifierData getData() {
        // TODO Auto-generated method stub
        return null;
    }

}
