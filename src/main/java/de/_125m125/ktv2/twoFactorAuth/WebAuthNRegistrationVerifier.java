package de._125m125.ktv2.twoFactorAuth;

import java.io.IOException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.yubico.webauthn.FinishRegistrationOptions;
import com.yubico.webauthn.RegistrationResult;
import com.yubico.webauthn.StartRegistrationOptions;
import com.yubico.webauthn.data.AuthenticatorAttestationResponse;
import com.yubico.webauthn.data.ClientRegistrationExtensionOutputs;
import com.yubico.webauthn.data.PublicKeyCredential;
import com.yubico.webauthn.data.PublicKeyCredentialCreationOptions;
import com.yubico.webauthn.data.UserIdentity;
import com.yubico.webauthn.exception.RegistrationFailedException;

public class WebAuthNRegistrationVerifier implements Verifier {

    private final PublicKeyCredentialCreationOptions request;
    private final WebAuthNAuthenticatorData          data;
    private TwoFactorAuthDatabaseEntry               newEntry;

    public WebAuthNRegistrationVerifier(final WebAuthNAuthenticatorData data) {
        this.data = data;

        this.request = data.rp.startRegistration(
                StartRegistrationOptions.builder().user(UserIdentity.builder().name(data.username)
                        .displayName(data.username).id(data.id).build()).build());
    }

    @Override
    public void initialize() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean verify(final String password, final boolean userInvoked)
            throws InvalidatedException {
        if (!userInvoked) {
            return false;
        }
        try {
            System.out.println("hi");
            final PublicKeyCredential<AuthenticatorAttestationResponse, ClientRegistrationExtensionOutputs> pkc = WebAuthNAuthenticatorData.jsonMapper
                    .readValue(password,
                            new TypeReference<PublicKeyCredential<AuthenticatorAttestationResponse, ClientRegistrationExtensionOutputs>>() {
                            });
            System.out.println(password);
            final RegistrationResult result = this.data.rp
                    .finishRegistration(FinishRegistrationOptions.builder().request(getRequest())
                            .response(pkc).build());
            this.newEntry = new TwoFactorAuthDatabaseEntry(this.data.userId, "WA",
                    WebAuthNAuthenticatorData.jsonMapper.writeValueAsString(result));
            return true;
        } catch (final IOException e) {
            e.printStackTrace();
            return false;
        } catch (final RegistrationFailedException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public VerifierData getData() {
        return this.newEntry;
    }

    public PublicKeyCredentialCreationOptions getRequest() {
        return this.request;
    }

}
