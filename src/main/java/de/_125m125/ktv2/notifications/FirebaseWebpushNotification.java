package de._125m125.ktv2.notifications;

import com.google.api.client.util.Key;
import com.google.firebase.messaging.WebpushNotification;

public class FirebaseWebpushNotification extends WebpushNotification {
    @Key("click_action")
    private final String clickAction;

    public FirebaseWebpushNotification(final String title, final String body, final String icon,
            final String click_action) {
        super(title, body, icon);
        this.clickAction = click_action;
    }

}
