package de._125m125.ktv2.notifications;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableMap;

import de._125m125.ktv2.beans.Token;
import de._125m125.ktv2.servlets.api.helper.Permission;

public class NotificationEvent {

    private final boolean                                selfCreated;
    private final long                                   uid;
    private final String                                 base32Uid;
    private final String                                 type;
    private final Map<String, String>                    details;
    private transient final Permission                   permission;
    private Object[]                                     contents;
    private transient final Supplier<? extends Object[]> contentProducer;

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final Permission permission) {
        this(selfCreated, uid, type, source, permission, (Object[]) null);
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final Permission permission, final Collection<?> contents) {
        this(selfCreated, uid, type, source, permission,
                contents.toArray(new Object[] { contents }));
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final Permission permission, final Object[] contents) {
        this(selfCreated, uid, type, source, Token.getBase32(BigInteger.valueOf(uid)), permission,
                contents);
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final Permission permission,
            final Supplier<? extends Object[]> contents) {
        this(selfCreated, uid, type, source, Token.getBase32(BigInteger.valueOf(uid)), permission,
                contents);
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final String key, final Permission permission) {
        this(selfCreated, uid, type, source, key, permission, (Object[]) null);
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final String key, final Permission permission,
            final Object[] contents) {
        this.selfCreated = selfCreated;
        this.uid = uid;
        this.contents = contents;
        this.contentProducer = null;
        this.base32Uid = Token.getBase32(BigInteger.valueOf(uid));
        this.type = type;
        this.details = ImmutableMap.of("source", source, "key", key, "channel",
                permission == Permission.NO_PERMISSIONS ? source : permission.getComName());
        this.permission = permission;
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final String key, final Permission permission,
            final List<Object> contents) {
        this(selfCreated, uid, type, source, key, permission,
                contents.toArray(new Object[contents.size()]));
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final String source, final String key, final Permission permission,
            final Supplier<? extends Object[]> contentProducer) {
        this.selfCreated = selfCreated;
        this.uid = uid;
        this.contents = null;
        this.contentProducer = contentProducer;
        this.base32Uid = Token.getBase32(BigInteger.valueOf(this.uid));
        this.type = type;
        this.details = ImmutableMap.of("source", source, "key", key, "channel",
                permission == Permission.NO_PERMISSIONS ? source : permission.getComName());
        this.permission = permission;
    }

    public NotificationEvent(final boolean selfCreated, final long uid, final String type,
            final Map<String, String> details, final Permission permission) {
        this(selfCreated, uid, type, details.get("source"), details.get("key"), permission);
    }

    public boolean isSelfCreated() {
        return this.selfCreated;
    }

    public long getUid() {
        return this.uid;
    }

    public String getBase32Uid() {
        return this.base32Uid;
    }

    public String getType() {
        return this.type;
    }

    public Map<String, String> getDetails() {
        return this.details;
    }

    public Permission getPermission() {
        return this.permission;
    }

    public Object[] getContents() {
        return this.contents != null || this.contentProducer == null ? this.contents
                : (this.contents = this.contentProducer.get());
    }

    public NotificationEvent withoutContents() {
        return new NotificationEvent(this.selfCreated, this.uid, this.type, this.details,
                this.permission);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.base32Uid == null) ? 0 : this.base32Uid.hashCode());
        result = prime * result + ((this.details == null) ? 0 : this.details.hashCode());
        result = prime * result + ((this.permission == null) ? 0 : this.permission.hashCode());
        result = prime * result + (this.selfCreated ? 1231 : 1237);
        result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
        result = prime * result + (int) (this.uid ^ (this.uid >>> 32));
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotificationEvent other = (NotificationEvent) obj;
        if (this.base32Uid == null) {
            if (other.base32Uid != null) {
                return false;
            }
        } else if (!this.base32Uid.equals(other.base32Uid)) {
            return false;
        }
        if (this.details == null) {
            if (other.details != null) {
                return false;
            }
        } else if (!this.details.equals(other.details)) {
            return false;
        }
        if (this.permission != other.permission) {
            return false;
        }
        if (this.selfCreated != other.selfCreated) {
            return false;
        }
        if (this.type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!this.type.equals(other.type)) {
            return false;
        }
        if (this.uid != other.uid) {
            return false;
        }
        return true;
    }

}
