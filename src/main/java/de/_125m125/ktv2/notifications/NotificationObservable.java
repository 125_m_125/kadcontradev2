package de._125m125.ktv2.notifications;

import java.util.Observable;

public class NotificationObservable extends Observable {

    public synchronized void notifyObservers(final NotificationEvent e) {
        if (e.getContents() != null && e.getContents().length == 0) {
            return;
        }
        setChanged();
        super.notifyObservers(e);
    }
}
