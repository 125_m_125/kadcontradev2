package de._125m125.ktv2.notifications;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Observable;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.WebpushConfig;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.ThreadManager;
import de._125m125.ktv2.UserHelper;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Token;

public class FirebaseNotificationManager {
    private static final FirebaseNotificationManager instance;

    private final boolean firebaseActive;

    static {
        boolean success = false;
        try {
            final FileInputStream serviceAccount =
                    new FileInputStream("/home/kadcontrade/serviceAccountKey.json");
            final FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://kadcontradre.firebaseio.com").build();
            FirebaseApp.initializeApp(options);
            success = true;
        } catch (final FileNotFoundException e) {
            e.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        instance = new FirebaseNotificationManager(success);
    }

    public FirebaseNotificationManager(final boolean success) {
        this.firebaseActive = success;
    }

    public static final FirebaseNotificationManager instance() {
        return FirebaseNotificationManager.instance;
    }

    public void setObservable(final NotificationObservable obs) {
        obs.addObserver(this::update);
    }

    public void update(final Observable o, final Object event) {
        if (!o.equals(Main.instance().getNotificationObservable())
                || !(event instanceof NotificationEvent)) {
            return;
        }
        final NotificationEvent e = (NotificationEvent) event;
        if (e.isSelfCreated() || e.getUid() <= 0) {
            return;
        }
        final String username = UserHelper.get(e.getUid());
        String title;
        String message;
        String url;
        String area;
        switch (e.getPermission()) {
        case READ_ITEMLIST_PERMISSION:
            title = Variables.getAppName() + " - Items";
            message = "Der Itemstand von " + username + " hat sich verändert";
            url = Variables.getCoreUrl();
            area = "itemlist";
            break;
        case READ_MESSAGES_PERMISSION:
            title = Variables.getAppName() + " - Nachrichten";
            message = username + " hat eine neue Nachricht  erhalten";
            url = Variables.getCoreUrl();
            area = "messages";
            break;
        case READ_ORDERS_PERMISSION:
            title = Variables.getAppName() + " - Orders";
            message = "Für eine Order von " + username + " wurde ein Gegenangebot gefunden";
            url = Variables.getCoreUrl() + "/markt";
            area = "orders";
            break;
        case READ_PAYOUTS_PERMISSION:
            title = Variables.getAppName() + " - Auszahlungen";
            message = "Der Auszahlungsstatus einer Auszahlung von " + username
                    + " hat sich verändert";
            url = Variables.getCoreUrl() + "/auszahlung";
            area = "payouts";
            break;
        case READ_WORKER_PAYOUTS_PERMISSION:
            title = Variables.getAppName() + " - Worker";
            message = "Es wurde eine neue Auszahlung beantragt";
            url = Variables.getCoreUrl();
            area = "workers";
            break;
        default:
            return;
        }
        // TODO check Topic
        final Message sendMessage = Message.builder().setTopic(getTopic(e.getUid(), area))
                .setNotification(new Notification(title, message))
                .setAndroidConfig(AndroidConfig.builder().setTtl(3600 * 1000)
                        .setNotification(AndroidNotification.builder().setIcon("icon")
                                .setColor("#ffffff").build())
                        .build())
                .setApnsConfig(
                        ApnsConfig.builder().setAps(Aps.builder().setBadge(42).build()).build())
                .setWebpushConfig(
                        WebpushConfig.builder()
                                .setNotification(new FirebaseWebpushNotification(title, message,
                                        Variables.getCoreUrl() + "/favicon.ico", url))
                                .build())
                .build();
        ThreadManager.instance().submit(() -> {
            try {
                FirebaseMessaging.getInstance().send(sendMessage);
            } catch (final FirebaseMessagingException e1) {
                e1.printStackTrace();
            }
        });
    }

    public boolean isConnected() {
        return this.firebaseActive;
    }

    public String getTopic(long uid, final String area) {
        if (area.equals("workers")) {
            uid = 0;
        }
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (final NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        final byte[] hash = digest.digest((Token.getBase32(BigInteger.valueOf(uid)) + ":" + area
                + "_" + Variables.getFirebaseSecret()).getBytes(StandardCharsets.UTF_8));
        return Base64.getUrlEncoder().encodeToString(hash).replaceAll("=", "~");
    }

    public void subscribe(final long uid, final String token, final String area) {
        final String topic = getTopic(uid, area);
        try {
            FirebaseMessaging.getInstance().subscribeToTopic(Arrays.asList(token), topic);
        } catch (final FirebaseMessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void unsubscribe(final long uid, final String token, final String area) {
        final String topic = getTopic(uid, area);
        try {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Arrays.asList(token), topic);
        } catch (final FirebaseMessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
