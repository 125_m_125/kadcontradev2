package de._125m125.ktv2.market;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import com.google.common.util.concurrent.Futures;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.GdprSettings;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.LotteryItemConfig;
import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;
import de._125m125.ktv2.market.Trade.buySell;
import de._125m125.ktv2.servlets.apiv2.HistoricTradeExecutionRestAPI;
import de._125m125.ktv2.servlets.apiv2.ItemRestAPI;
import de._125m125.ktv2.servlets.apiv2.MessageRestAPI;
import de._125m125.ktv2.servlets.apiv2.OrderBookRestAPI;
import de._125m125.ktv2.servlets.apiv2.TradeRestAPI;

public class TradeManager {
    public static final double TAX_PP_TOTAL_DEFAULT      = 0;
    public static final double TAX_PP_DIFFERENCE_DEFAULT = 0.08;

    private final ExecutorService executor;
    private final double          taxPpTotal;
    private final double          taxPpDifference;

    public TradeManager() {
        this(TradeManager.TAX_PP_TOTAL_DEFAULT, TradeManager.TAX_PP_DIFFERENCE_DEFAULT);
    }

    public TradeManager(final ExecutorService executor) {
        this(executor, TradeManager.TAX_PP_TOTAL_DEFAULT, TradeManager.TAX_PP_DIFFERENCE_DEFAULT);
    }

    public TradeManager(final double taxPpTotal, final double taxPpDifference) {
        this(Executors.newSingleThreadExecutor(), taxPpTotal, taxPpDifference);
    }

    public TradeManager(final ExecutorService executor, final double taxPpTotal,
            final double taxPpDifference) {
        this.executor = executor;
        this.taxPpTotal = taxPpTotal;
        this.taxPpDifference = taxPpDifference;
    }

    public void stop() {
        this.executor.shutdown();
        try {
            this.executor.awaitTermination(10, TimeUnit.SECONDS);
        } catch (final InterruptedException e) {
        }
    }

    private boolean enoughSpace(final Trade t) {
        final String owner = String.valueOf(t.getOwner());
        final String type = t.getType().getMysqlRepresentation();
        final ArrayList<HashMap<String, Object>> k = Main.instance().getConnector().executeQuery(
                "SELECT COUNT(*) FROM trade WHERE traderid=? AND type=? AND material=? AND buysell=?",
                owner, type, t.getMaterial(), t.isBuying() ? "1" : "0");
        if (k != null && k.size() == 1 && k.get(0) != null) {
            return (long) k.get(0).get("COUNT(*)") == 0;
        }
        return false;
    }

    private TradeResult checkPreconditions(final UtilTrade utilTrade) {
        final Trade t = utilTrade.getTrade();
        if (!enoughSpace(t)) {
            return new TradeResult(false, 422, "nofreeslots");
        }
        if (t.getAmount() > 3456) {
            return new TradeResult(false, 422, "morethanonedoublechest");
        }
        if (t.getAmount() < 1) {
            return new TradeResult(false, 422, "atLeastOneItem");
        }
        if (t.getMaterial().startsWith("-")) {
            if (!GdprSettings.loadSettings(utilTrade.getOwner()).isPublishLotteryData()) {
                return new TradeResult(false, 403, "lotteryPermissionRequired");
            }
            final Optional<LotteryItemConfig> config = Variables
                    .getLotteryItemConfig(t.getMaterial()).filter(c -> c.tradeOn(LocalDate.now()));
            if (!config.isPresent()) {
                return new TradeResult(false, 409, "itemcurrentlynotavailable");
            }
            if (t.getAmount() > config.get().getUnserTradeAmount()) {
                return new TradeResult(false, 422, "tradeLimit5");
            }
        }
        if (!t.checkResources()) {
            if (t.isBuying()) {
                return new TradeResult(false, 422, "notenoughmoney");
            } else {
                return new TradeResult(false, 422, "notenoughmaterials");
            }
        }
        return null;
    }

    private synchronized TradeResult executeCreate(final UtilTrade utilTrade,
            final boolean checkPreconditions) {
        Trade t;
        t = utilTrade.getTrade();
        if (checkPreconditions) {
            final TradeResult preconditionResult = checkPreconditions(utilTrade);
            if (preconditionResult != null) {
                return preconditionResult;
            }
        }
        t.setNextId();
        final ArrayList<HashMap<String, Object>> matches = Main.instance().getConnector()
                .executeQuery("SELECT * FROM trade WHERE type="
                        + (t.getType() == ITEM_OR_STOCK.ITEM) + " AND material='" + t.getMaterial()
                        + "' AND buysell=" + !t.isBuying() + " AND sold<amount AND price"
                        + (t.isBuying() ? "<=" : ">=") + t.getPrice() + " ORDER BY time");
        final List<Trade> changedTrades = new ArrayList<>();
        final Map<Long, List<Message>> newMessages = new HashMap<>();
        changedTrades.add(t);
        final long[] totalRemainingTaxes = { 0 };
        final Successindicator indicator = Main.instance().getConnector().executeAtomic(con -> {
            try (final PreparedStatement tradesDoneQuery = con.prepareStatement(
                    "INSERT INTO tradesdone(type,material,amount,price) VALUES(?,?,?,?)");
                    final PreparedStatement tradesDoneHistoryQuery = con.prepareStatement(
                            "INSERT INTO tradesDoneHistory(id1,id2,amount,price,taxPerPerson) VALUES(?,?,?,?,?)")) {
                tradesDoneQuery.setBoolean(1, t.getType() == ITEM_OR_STOCK.ITEM);
                tradesDoneQuery.setString(2, t.getMaterial());
                if (matches != null) {
                    for (int i = 0; i < matches.size() && t.getSold() < t.getAmount(); i++) {
                        final Trade other = new Trade(matches.get(i), t.getType());
                        final long amount = Math.min(t.getAmount() - t.getSold(),
                                other.getAmount() - other.getSold());
                        final long price = (other.getPrice() + t.getPrice()) / 2;
                        final long taxPP = (long) Math
                                .ceil(((price - (t.isBuying() ? other.getPrice() : t.getPrice()))
                                        * this.taxPpDifference + price * this.taxPpTotal) * amount);

                        newMessages.computeIfAbsent(t.getOwner(), l -> new ArrayList<>())
                                .add(t.trade(con, amount, price, taxPP));
                        newMessages.computeIfAbsent(other.getOwner(), l -> new ArrayList<>())
                                .add(other.trade(con, amount, price, taxPP));
                        other.updateDatabaseAfterTrade(con);
                        tradesDoneQuery.setLong(3, amount);
                        tradesDoneQuery.setLong(4, price);
                        tradesDoneQuery.executeUpdate();
                        if (t.isBuying()) {
                            tradesDoneHistoryQuery.setLong(1, t.getID());
                            tradesDoneHistoryQuery.setLong(2, other.getID());
                        } else {
                            tradesDoneHistoryQuery.setLong(1, other.getID());
                            tradesDoneHistoryQuery.setLong(2, t.getID());
                        }
                        tradesDoneHistoryQuery.setLong(3, amount);
                        tradesDoneHistoryQuery.setLong(4, price);
                        tradesDoneHistoryQuery.setLong(5, taxPP);
                        tradesDoneHistoryQuery.executeUpdate();
                        if (taxPP > 0) {
                            totalRemainingTaxes[0] += refBonuses(con, taxPP, t.getOwner());
                            totalRemainingTaxes[0] += refBonuses(con, taxPP, other.getOwner());
                        }

                        Log.TRADE.executeLog(con, Level.INFO,
                                "Trade " + t.getID() + " from " + t.getOwner()
                                        + " was paired with trade " + other.getID() + " from "
                                        + other.getOwner() + ". Amount: " + amount
                                        + " combined price: " + price,
                                t.getOwner(), other.getOwner());
                        changedTrades.add(other);
                    }
                }
            }
            t.enterIntoDatabase(con);
            distributeTaxes(con, totalRemainingTaxes[0]);
            return () -> true;
        });
        if (indicator.wasSuccessful()) {
            TradeRestAPI.invalidate(t.getOwner(), changedTrades);
            for (final Trade changedTrade : changedTrades) {
                ItemRestAPI.invalidate(String.valueOf(changedTrade.getOwner()), () -> Item
                        .getItems(changedTrade.getOwner(), "-1", changedTrade.getMaterial()), true);
            }
            ItemRestAPI.invalidate(String.valueOf(t.getOwner()),
                    () -> Item.getItems(t.getOwner(), "-1", t.getMaterial()), true);
            HistoricTradeExecutionRestAPI.invalidate(t.getMaterial(), System.currentTimeMillis());
            for (final Entry<Long, List<Message>> i : newMessages.entrySet()) {
                MessageRestAPI.invalidate(Long.toString(i.getKey()), i.getValue(),
                        i.getKey() == t.getOwner());
            }
            OrderBookRestAPI.invalidate(t.getMaterial());
            if (t.getSold() != t.getAmount()) {
                return new TradeResult(true, 201, "creationsuccess", t);
            } else {
                return new TradeResult(true, 200, "creationsuccessCompleted", t);
            }
        } else {
            return new TradeResult(false, 500, "unknownError", t);
        }
    }

    private synchronized TradeResult executeCancel(final UtilTrade utilTrade) {
        Trade t;
        t = Trade.fullyLoad(utilTrade.getId(), utilTrade.getOwner(), utilTrade.getType());
        if (t != null) {
            t.cancel(utilTrade.getForcer());
            TradeRestAPI.invalidate(t);
            ItemRestAPI.invalidate(String.valueOf(t.getOwner()),
                    () -> Item.getItems(t.getOwner(), t.getMaterial(), "-1"), true);
            OrderBookRestAPI.invalidate(t.getMaterial());
            return new TradeResult(true, 200, "tradecancelsuccess", t);
        } else {
            return new TradeResult(false, 404, "tradenotexisting");
        }
    }

    private long refBonuses(final Connection con, final long taxes, final long owner)
            throws SQLException {
        final long quarttax = taxes / 4;
        long remainingTaxes = taxes;
        try (final PreparedStatement refSelect = con.prepareStatement(
                "SELECT id,? FROM referrer where ref=? UNION ALL (SELECT ref,(SELECT FLOOR(?/COUNT(*)) FROM referrer WHERE id=?) FROM referrer WHERE id=?)");
                PreparedStatement taxInsert = con.prepareStatement(
                        "INSERT INTO reftaxes(id,amount) VALUES(?,?) ON DUPLICATE KEY UPDATE amount=amount+VALUES(amount)")) {
            refSelect.setLong(1, quarttax);
            refSelect.setLong(2, owner);
            refSelect.setLong(3, quarttax);
            refSelect.setLong(4, owner);
            refSelect.setLong(5, owner);
            try (ResultSet refs = refSelect.executeQuery()) {
                while (refs.next()) {
                    final long value = refs.getLong(2);
                    if (value > 0) {
                        taxInsert.setLong(1, refs.getLong(1));
                        taxInsert.setLong(2, refs.getLong(2));
                        taxInsert.addBatch();
                        remainingTaxes -= value;
                    }
                }
            }
            taxInsert.executeBatch();
        }
        return remainingTaxes;
    }

    private void distributeTaxes(final Connection con, final long remainingTaxes)
            throws SQLException {
        try (final PreparedStatement shareHolderQuery = con.prepareStatement(
                "SELECT id,amount,(SELECT SUM(amount) FROM materials WHERE type=FALSE AND matid=-1) AS sum FROM materials WHERE type=FALSE AND matid=-1 ORDER BY RAND()");
                PreparedStatement taxInsert = con.prepareStatement(
                        "INSERT INTO reftaxes(id,amount) VALUES(?,?) ON DUPLICATE KEY UPDATE amount=amount+VALUES(amount)");
                ResultSet shareHolders = shareHolderQuery.executeQuery()) {
            if (shareHolders.next()) {
                final long sum = shareHolders.getLong("sum");
                final double remainingTaxesDouble = (double) remainingTaxes;
                final double valuePerShare = remainingTaxesDouble / sum;
                long currentWeights = 0;
                long lastRounded = 0;
                do {
                    final long amount = shareHolders.getLong("amount");
                    currentWeights += amount;
                    final long newRounded = Math.round(valuePerShare * currentWeights);
                    final long change = newRounded - lastRounded;
                    lastRounded = newRounded;
                    if (change > 0) {
                        taxInsert.setLong(1, shareHolders.getLong("id"));
                        taxInsert.setLong(2, change);
                        taxInsert.addBatch();
                    }
                } while (shareHolders.next());
                taxInsert.executeBatch();
            }
        }
    }

    public Future<TradeResult> cancel(final long id, final long owner, final ITEM_OR_STOCK type) {
        return this.executor.submit(() -> executeCancel(new UtilTrade(id, owner, type)));
    }

    public Future<TradeResult> cancel(final long id, final long owner, final ITEM_OR_STOCK type,
            final long forcer) {
        return this.executor.submit(() -> executeCancel(new UtilTrade(id, owner, type, forcer)));
    }

    public Future<TradeResult> create(final int amount, final buySell buysell, final String object,
            final long owner, final long pricePerItem, final ITEM_OR_STOCK type) {
        return create(amount, buysell, object, owner, pricePerItem, type, true);
    }

    public Future<TradeResult> create(final int amount, final buySell buysell, final String object,
            final long owner, final long pricePerItem, final ITEM_OR_STOCK type,
            final boolean checkPrecondition) {
        return this.executor.submit(() -> executeCreate(
                new UtilTrade(new Trade(amount, buysell, object, owner, pricePerItem, type)),
                checkPrecondition));
    }

    public Future<TradeResult> create(final String amount, final buySell buysell,
            final String object, final long owner, final String pricePerItem,
            final ITEM_OR_STOCK type) {
        try {
            final int iAmount = Integer.parseInt(amount);
            if (iAmount <= 0 || iAmount > 3456) {
                return Futures
                        .immediateCheckedFuture(new TradeResult(false, 422, "amountOutOfBounds"));
            }
            final long iPricePerItem = IDSConverter.stringToLong(pricePerItem, 4);
            if (iPricePerItem <= 0) {
                return Futures
                        .immediateCheckedFuture(new TradeResult(false, 422, "priceOutOfBounds"));
            }
            if (!Variables.resourceExists(object)) {
                return Futures
                        .immediateCheckedFuture(new TradeResult(false, 422, "unknownMaterial"));
            }
            return create(iAmount, buysell, object, owner, iPricePerItem, type, true);
        } catch (final NumberFormatException e) {
            return Futures.immediateCheckedFuture(new TradeResult(false, 422, e.getMessage()));
        }
    }

    public Future<TradeResult> cancel(final String tradeId, final long owner,
            final ITEM_OR_STOCK type) {
        try {
            final long iTradeid = Long.parseLong(tradeId);
            return cancel(iTradeid, owner, type);
        } catch (final NumberFormatException e) {
            return Futures.immediateCheckedFuture(new TradeResult(false, 422, e.getMessage()));
        }
    }

}

class UtilTrade {
    private final ITEM_OR_STOCK type;
    private final long          id;
    private final long          owner;
    private final Trade         t;
    private final long          forcer;

    public UtilTrade(final long id, final long owner, final ITEM_OR_STOCK type) {
        this.id = id;
        this.owner = owner;
        this.type = type;
        this.t = null;
        this.forcer = 0;
    }

    public UtilTrade(final long id, final long owner, final ITEM_OR_STOCK type, final long forcer) {
        this.id = id;
        this.owner = owner;
        this.type = type;
        this.t = null;
        this.forcer = forcer;
    }

    public ITEM_OR_STOCK getType() {
        return this.type;
    }

    public UtilTrade(final Trade t) {
        this.id = t.getID();
        this.owner = t.getOwner();
        this.type = t.getType();
        this.t = t;
        this.forcer = 0;
    }

    public long getId() {
        return this.id;
    }

    public long getOwner() {
        return this.owner;
    }

    public Trade getTrade() {
        return this.t;
    }

    public long getForcer() {
        return this.forcer;
    }
}