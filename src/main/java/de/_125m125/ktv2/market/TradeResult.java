package de._125m125.ktv2.market;

import de._125m125.ktv2.beans.TradeSlot;
import de._125m125.ktv2.beans.UpdatedTradeSlot;
import de._125m125.ktv2.servlets.api.helper.HelperResult;

public class TradeResult extends HelperResult<UpdatedTradeSlot> {
    public TradeResult(final boolean success, final int status, final String message,
            final Trade data) {
        super(success, status, message, data == null ? null : data.toTradeSlot());
    }

    public TradeResult(final boolean success, final int status, final String message) {
        super(success, status, message);
    }

    public TradeResult(final boolean success, final int status, final Trade data) {
        super(success, status, data == null ? null : data.toTradeSlot());
    }

    public TradeResult(final boolean success, final int status, final String message,
            final UpdatedTradeSlot data) {
        super(success, status, message, data);
    }

    public boolean isSuccess() {
        return super.wasSuccessful();
    }

    public String getResultString() {
        return super.getMessage();
    }

    public TradeSlot getTradeResult() {
        return super.getData();
    }
}
