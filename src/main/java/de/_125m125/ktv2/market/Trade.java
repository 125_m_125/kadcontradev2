package de._125m125.ktv2.market;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;

import javax.json.Json;
import javax.json.JsonObject;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.beans.Item;
import de._125m125.ktv2.beans.Message;
import de._125m125.ktv2.beans.UpdatedTradeSlot;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.logging.Log;

public class Trade {
    private static final Random RANDOM = new Random();

    public static enum buySell {
        BUY,
        NULL,
        SELL
    }

    public static enum ITEM_OR_STOCK {
        ITEM("1"),
        STOCK("0");

        private final String mysqlRepresentation;

        ITEM_OR_STOCK(final String mysqlRepresentation) {
            this.mysqlRepresentation = mysqlRepresentation;
        }

        public String getMysqlRepresentation() {
            return this.mysqlRepresentation;
        }
    }

    public static Trade fullyLoad(final long id, final long owner, final ITEM_OR_STOCK type) {
        final StringBuilder sb = new StringBuilder("SELECT * FROM trade WHERE type=")
                .append(type == ITEM_OR_STOCK.ITEM).append(" AND id=").append(id)
                .append(" AND traderid=").append(owner);
        final ArrayList<HashMap<String, Object>> k =
                Main.instance().getConnector().executeQuery(sb.toString());
        if (k != null && k.size() > 0 && k.get(0) != null) {
            return new Trade(k.get(0), type);
        } else {
            return null;
        }
    }

    private final long    amount;
    private final buySell buysell;
    private boolean       cancelled;
    private long          id;
    private final String  object;
    private final long    owner;
    private final long    pricePerItem;
    private long          sold;
    private long          itemUpdate  = 0l;
    private long          moneyUpdate = 0l;

    private final ITEM_OR_STOCK type;

    protected Trade(final HashMap<String, Object> hm, final ITEM_OR_STOCK type) {
        this.amount = (long) hm.get("amount");
        this.buysell = (boolean) hm.get("buysell") ? buySell.BUY : buySell.SELL;
        this.cancelled = false;
        this.id = (long) hm.get("id");
        this.object = (String) hm.get("material");
        this.owner = (long) hm.get("traderid");
        this.pricePerItem = (long) hm.get("price");
        this.sold = (long) hm.get("sold");
        this.type = type;
    }

    public Trade(final long id, final int amount, final buySell buysell, final String object,
            final long owner, final int pricePerItem, final int sold, final ITEM_OR_STOCK type) {
        this.amount = amount;
        this.buysell = buysell;
        this.cancelled = false;
        this.id = id;
        this.object = object;
        this.owner = owner;
        this.pricePerItem = pricePerItem;
        this.sold = sold;
        this.type = type;
    }

    public Trade(final int amount, final buySell buysell, final String object, final long owner,
            final long pricePerItem2, final ITEM_OR_STOCK type) {
        this.amount = amount;
        this.buysell = buysell;
        this.cancelled = false;
        this.object = object;
        this.owner = owner;
        this.pricePerItem = pricePerItem2;
        this.sold = 0;
        this.type = type;
    }

    public void cancel() {
        cancel(0);
    }

    public void cancel(final long forcer) {
        if (this.cancelled) {
            return;
        }
        switch (this.buysell) {
        case BUY:
            this.moneyUpdate = this.moneyUpdate + this.pricePerItem * (this.amount - this.sold);
            break;
        default:
            this.itemUpdate = this.itemUpdate + this.amount - this.sold;
            break;
        }
        this.sold = this.amount;
        this.cancelled = true;
        Main.instance().getConnector().executeAtomic(c -> {
            updateDatabaseAfterTrade(c);
            if (forcer <= 0) {
                Log.TRADE.executeLog(c, Level.INFO, this.owner + " cancelled trade " + this.id,
                        this.owner);
            } else {
                Log.TRADE.executeLog(c, Level.INFO,
                        forcer + " forced cancel of trade " + this.id + " of " + this.owner, forcer,
                        this.owner);
                MySQLHelper.sendMessage(c, this.owner, "forceCancelMessage", this.amount,
                        this.object, IDSConverter.longToDoubleString(this.pricePerItem));
            }
            return () -> true;
        });
    }

    public void enterIntoDatabase(final Connection con) throws SQLException {
        try (PreparedStatement trade = con.prepareStatement(
                "INSERT INTO trade(id,buysell,traderid,type,material,amount,price,sold) VALUES(?,?,?,?,?,?,?,?)");
                PreparedStatement history = con.prepareStatement(
                        "INSERT INTO tradeHistory(id,buysell,traderid,type,material,amount,price) VALUES(?,?,?,?,?,?,?)")) {
            trade.setLong(1, this.id);
            history.setLong(1, this.id);
            trade.setBoolean(2, this.buysell == buySell.BUY);
            history.setBoolean(2, this.buysell == buySell.BUY);
            trade.setLong(3, this.owner);
            history.setLong(3, this.owner);
            trade.setBoolean(4, this.type == ITEM_OR_STOCK.ITEM);
            history.setBoolean(4, this.type == ITEM_OR_STOCK.ITEM);
            trade.setString(5, this.object);
            history.setString(5, this.object);
            trade.setLong(6, this.amount);
            history.setLong(6, this.amount);
            trade.setLong(7, this.pricePerItem);
            history.setLong(7, this.pricePerItem);
            trade.setLong(8, this.sold);
            // trade.setLong(9, this.tradingResult[0]);
            // trade.setLong(10, this.tradingResult[1]);
            if (this.sold != this.amount) {
                trade.executeUpdate();
            }
            history.executeUpdate();
            MySQLHelper.addResource(con, this.owner, this.type == ITEM_OR_STOCK.ITEM,
                    this.buysell == buySell.BUY ? "-1" : this.object,
                    -(this.buysell == buySell.BUY ? this.amount * this.pricePerItem : this.amount),
                    "tradeCreated " + this.id, false);
            MySQLHelper.addResource(con, this.owner, this.type == ITEM_OR_STOCK.ITEM, this.object,
                    this.itemUpdate, "tradupdate " + this.id, false);
            MySQLHelper.addResource(con, this.owner, true, "-1", this.moneyUpdate,
                    "tradupdate " + this.id, false);
            Log.TRADE.executeLog(con, Level.INFO,
                    this.owner + " created new trade sucessfully: tradeid: " + this.id + " "
                            + (this.buysell == buySell.BUY ? "buy" : "sell") + " " + this.amount
                            + " * " + this.object + ", each for " + this.pricePerItem,
                    this.owner);

        }
    }

    public long getAmount() {
        return this.amount;
    }

    public long getOwner() {
        return this.owner;
    }

    public void setNextId() {
        this.id = new BigInteger(63, Trade.RANDOM).longValue();

    }

    public Message trade(final Connection c, final long amount2, final long price, final long taxes)
            throws SQLException {
        this.sold += amount2;
        switch (this.buysell) {
        case BUY:
            this.moneyUpdate =
                    this.moneyUpdate + Math.abs(this.pricePerItem - price) * amount2 - taxes;
            this.itemUpdate = this.itemUpdate + amount2;
            break;
        case SELL:
            this.moneyUpdate = this.moneyUpdate + amount2 * price - taxes;
            break;
        default:
            throw new IllegalStateException("trade is not buying or selling.");
        }
        final String item;
        switch (this.type) {
        case ITEM:
            item = Variables.ID_TO_DN.get(this.object);
            break;
        case STOCK:
            item = " Aktie " + this.object;
            break;
        default:
            throw new IllegalStateException("trade is of invalid type");
        }
        return MySQLHelper.sendMessage(c, this.owner, "tradeProgressed", amount2, item,
                IDSConverter.longToDoubleString(
                        price + (this.buysell == buySell.BUY ? -taxes / amount2 : taxes / amount2)),
                this.buysell == buySell.BUY ? "gekauft" : "verkauft");

    }

    public void updateDatabaseAfterTrade(final Connection c) throws SQLException {
        if (this.sold == this.amount) {
            try (PreparedStatement ps = c.prepareStatement("DELETE FROM trade WHERE id=?")) {
                ps.setLong(1, this.id);
                ps.executeUpdate();
            }
            if (this.cancelled) {
                try (PreparedStatement ps = c.prepareStatement(
                        "UPDATE tradeHistory SET cancelled=CURRENT_TIMESTAMP() WHERE id=?")) {
                    ps.setLong(1, this.id);
                    ps.executeUpdate();
                }
            }
        } else {
            try (PreparedStatement ps = c.prepareStatement("UPDATE trade SET sold=? WHERE id=?")) {
                ps.setLong(1, this.sold);
                ps.setLong(2, this.id);
                ps.executeUpdate();
            }
        }
        MySQLHelper.addResource(c, this.owner, this.type == ITEM_OR_STOCK.ITEM, this.object,
                this.itemUpdate, "tradupdate " + this.id, false);
        MySQLHelper.addResource(c, this.owner, true, "-1", this.moneyUpdate,
                "tradupdate " + this.id, false);
    }

    public boolean checkResources() {
        ArrayList<HashMap<String, Object>> query;
        switch (this.buysell) {
        case BUY:
            query = Main.instance().getConnector().executeQuery("SELECT * FROM materials WHERE id="
                    + this.owner + " AND type=true AND matid='-1'");
            return query != null && query.size() == 1 && query.get(0) != null
                    && (long) query.get(0).get("amount") >= this.amount * this.pricePerItem;
        case SELL:
            query = Main.instance().getConnector()
                    .executeQuery(
                            "SELECT * FROM materials WHERE id=" + this.owner + " AND type="
                                    + (this.type == ITEM_OR_STOCK.ITEM) + " AND matid=?",
                            this.object);
            return query != null && query.size() == 1 && query.get(0) != null
                    && (long) query.get(0).get("amount") >= this.amount;
        default:
            return false;
        }
    }

    public long getSold() {
        return this.sold;
    }

    public ITEM_OR_STOCK getType() {
        return this.type;
    }

    public String getMaterial() {
        return this.object;
    }

    public boolean isBuying() {
        return this.buysell == buySell.BUY;
    }

    public long getPrice() {
        return this.pricePerItem;
    }

    public long getID() {
        return this.id;
    }

    public long getItemUpdate() {
        return this.itemUpdate;
    }

    public long getMoneyUpdate() {
        return this.moneyUpdate;
    }

    public JsonObject toJsonObject() {
        return Json.createObjectBuilder().add("id", this.id).add("buy", this.buysell == buySell.BUY)
                .add("materialId", this.object)
                .add("materialName", Variables.ID_TO_DN.get(this.object)).add("amount", this.amount)
                .add("price", IDSConverter.longToDouble(this.pricePerItem)).add("sold", this.sold)
                .add("moneyUpdate", this.moneyUpdate).add("itemUpdate", this.itemUpdate)
                .add("cancelled", this.cancelled).build();
    }

    public UpdatedTradeSlot toTradeSlot() {
        final UpdatedTradeSlot ts = new UpdatedTradeSlot();
        ts.setActive(true);
        ts.setBuy(this.buysell == buySell.BUY);
        ts.setCancelled(this.cancelled);
        ts.setCurrent(this.sold);
        ts.setItem(new Item(this.object));
        ts.setMax(this.amount);
        ts.setPrice(IDSConverter.longToDoubleString(this.pricePerItem));
        ts.setTradeid(this.id);
        ts.setUpdatedItems(this.itemUpdate);
        ts.setUpdatedMoney(this.moneyUpdate);
        return ts;
    }

}
