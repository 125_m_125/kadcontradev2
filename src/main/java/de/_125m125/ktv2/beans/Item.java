package de._125m125.ktv2.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.calculators.IDSConverter;

public class Item extends Tsvable {

    public static Item[] getItems(final long userId, final String... materials) {
        Arrays.sort(materials);
        try (Connection c = Main.instance().getConnector().getConnection();
                final PreparedStatement ps = c.prepareStatement(
                        "SELECT * FROM materials WHERE id=? and type=true AND matid IN(?,?,?,?,?) ORDER by matid")) {
            final Item[] result = new Item[materials.length];
            ps.setLong(1, userId);
            for (int i = 0; i <= materials.length; i += 5) {
                for (int j = i; j < i + 5; j++) {
                    if (j < materials.length) {
                        ps.setString(j - i + 2, materials[j]);
                    } else {
                        ps.setString(j - i + 2, "0");
                    }
                }
                try (ResultSet rs = ps.executeQuery()) {
                    int k = i;
                    while (rs.next() && k < materials.length) {
                        final Item item = fromCurrent(rs);
                        while (!item.getId().equals(materials[k])) {
                            result[k] = new Item(materials[k]);
                            k++;
                        }
                        result[k++] = item;
                    }
                    while (k < materials.length) {
                        result[k] = new Item(materials[k]);
                        k++;
                    }
                }
            }
            return result;
        } catch (final SQLException e) {
            e.printStackTrace();
            return new Item[] {};
        }
    }

    public static List<Item> from(final ResultSet rs) throws SQLException {
        final List<Item> items = new ArrayList<>();
        while (rs.next()) {
            items.add(fromCurrent(rs));
        }
        return items;
    }

    public static Item fromCurrent(final ResultSet rs) throws SQLException {
        final Item i = new Item();
        i.setId(rs.getString("matid"));
        i.setAmount(rs.getLong("amount"));
        return i;
    }

    public static final String HEADER = "id\tname\tamount";
    public static final Item   NONE   = new Item("0");

    private String             amount;
    private String             id;

    public Item() {
        super(Item.HEADER);
    }

    public Item(final String string) {
        this(string, 0);
    }

    public Item(final String string, final long l) {
        super(Item.HEADER);
        setId(string);
        setAmount(l);
    }

    public Item(final String id, final String amount) {
        super(Item.HEADER);
        setId(id);
        Objects.requireNonNull(amount);
        if (!"-1".equals(id)) {
            if (!amount.matches("-?[0-9]+(\\.0*)?")) {
                throw new NumberFormatException("Invalid amount for non-money item: " + amount);
            }
            this.amount = amount.replaceAll("\\.0*", "");
        } else if (!amount.matches("-?([0-9]*?\\.)?[0-9]+")) {
            throw new NumberFormatException("Invalid amount: " + amount);
        } else {
            this.amount = amount;
        }
    }

    public String getAmount() {
        return this.amount;
    }

    public long getLongAmount() {
        if ("-1".equals(this.id)) {
            return IDSConverter.stringToLong(this.amount);
        } else {
            return Long.parseLong(this.amount);
        }
    }

    public String getName() {
        return Variables.ID_TO_DN.get(this.id);
    }

    public String getImageName() {
        if (this.id.contains(":")) {
            final String[] parts = this.id.split(":");
            return parts[0] + "-" + parts[1] + ".png";
        }
        return this.id + "-0.png";
    }

    public String getId() {
        return this.id;
    }

    public void setAmount(final long l) {
        if ("-1".equals(this.id)) {
            this.amount = IDSConverter.longToDoubleString(l);
        } else {
            this.amount = String.valueOf(l);
        }
    }

    public void setId(final String name) {
        this.id = Objects.requireNonNull(name);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.amount == null) ? 0 : this.amount.hashCode());
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        if (this.amount == null) {
            if (other.amount != null) {
                return false;
            }
        } else if (!this.amount.equals(other.amount)) {
            return false;
        }
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.id);
        stringBuilder.append("\t");
        stringBuilder.append(getName());
        stringBuilder.append("\t");
        stringBuilder.append(this.amount);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

    @Override
    public long getLastModified() {
        return -1;
    }

    @Override
    public String toString() {
        return "Item [amount=" + this.amount + ", name=" + this.id + "]";
    }

}
