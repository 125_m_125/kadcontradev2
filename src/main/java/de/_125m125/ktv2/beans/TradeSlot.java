package de._125m125.ktv2.beans;

import de._125m125.ktv2.cache.Tsvable;

public class TradeSlot extends Tsvable {

    public static final String HEADER = "id\tbuy\tmaterialId\tmaterialName\tamount\tprice\tsold";

    private boolean active       = false, buy = true;
    private long    sold         = 0, amount = 1, id = -1;
    private String  price        = "1";
    private String  materialId   = null;
    private String  materialName = null;

    public TradeSlot() {
        this(TradeSlot.HEADER);
    }

    public TradeSlot(final String header) {
        super(header);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.active ? 1231 : 1237);
        result = prime * result + (int) (this.amount ^ (this.amount >>> 32));
        result = prime * result + (this.buy ? 1231 : 1237);
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.materialId == null) ? 0 : this.materialId.hashCode());
        result = prime * result + ((this.materialName == null) ? 0 : this.materialName.hashCode());
        result = prime * result + ((this.price == null) ? 0 : this.price.hashCode());
        result = prime * result + (int) (this.sold ^ (this.sold >>> 32));
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TradeSlot other = (TradeSlot) obj;
        if (this.active != other.active) {
            return false;
        }
        if (this.amount != other.amount) {
            return false;
        }
        if (this.buy != other.buy) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (this.materialId == null) {
            if (other.materialId != null) {
                return false;
            }
        } else if (!this.materialId.equals(other.materialId)) {
            return false;
        }
        if (this.materialName == null) {
            if (other.materialName != null) {
                return false;
            }
        } else if (!this.materialName.equals(other.materialName)) {
            return false;
        }
        if (this.price == null) {
            if (other.price != null) {
                return false;
            }
        } else if (!this.price.equals(other.price)) {
            return false;
        }
        if (this.sold != other.sold) {
            return false;
        }
        return true;
    }

    public long getCurrent() {
        return this.sold;
    }

    public String getItem() {
        return this.materialName;
    }

    public String getMaterialId() {
        return this.materialId;
    }

    public long getMax() {
        return this.amount;
    }

    public String getPrice() {
        return this.price;
    }

    public long getTradeid() {
        return this.id;
    }

    public boolean isActive() {
        return this.active;
    }

    public boolean isBuy() {
        return this.buy;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public void setBuy(final boolean buy) {
        this.buy = buy;
    }

    public void setCurrent(final long l) {
        this.sold = l;
    }

    public void setItem(final Item item) {
        this.materialId = item.getId();
        this.materialName = item.getName();
    }

    public void setMax(final long l) {
        this.amount = l;
    }

    public void setPrice(final String string) {
        this.price = string;
    }

    public void setTradeid(final long l) {
        this.id = l;
    }

    @Override
    public String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.id);
        stringBuilder.append("\t");
        stringBuilder.append(this.buy);
        stringBuilder.append("\t");
        stringBuilder.append(this.materialId);
        stringBuilder.append("\t");
        stringBuilder.append(this.materialName);
        stringBuilder.append("\t");
        stringBuilder.append(this.amount);
        stringBuilder.append("\t");
        stringBuilder.append(this.price);
        stringBuilder.append("\t");
        stringBuilder.append(this.sold);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

    @Override
    public long getLastModified() {
        return -1;
    }
}
