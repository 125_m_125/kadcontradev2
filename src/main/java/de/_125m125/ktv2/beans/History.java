package de._125m125.ktv2.beans;

public class History extends TsvableList<HistoryEntry> {

    public History() {
        super(HistoryEntry.HEADER);
    }

    @Override
    protected HistoryEntry[] createArray(final int size) {
        return new HistoryEntry[size];
    }

    @Override
    public synchronized HistoryEntry[] getEntries() {
        return super.getEntries();
    }

}
