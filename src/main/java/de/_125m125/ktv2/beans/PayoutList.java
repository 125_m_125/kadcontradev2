package de._125m125.ktv2.beans;

public class PayoutList extends TsvableList<Payout> {

    public PayoutList() {
        super(Payout.HEADER);
    }

    @Override
    protected Payout[] createArray(final int size) {
        return new Payout[size];
    }

    @Override
    public synchronized Payout[] getEntries() {
        return super.getEntries();
    }
}
