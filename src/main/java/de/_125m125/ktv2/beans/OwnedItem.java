package de._125m125.ktv2.beans;

import de._125m125.ktv2.cache.Tsvable;

public class OwnedItem extends Tsvable {
    public static final String TSV_HEADER = "owner\tid\tamount";

    private final long   owner;
    private final String id;
    private final String amount;

    public OwnedItem(long owner, String itemId, String amount) {
        super(OwnedItem.TSV_HEADER);
        this.owner = owner;
        this.id = itemId;
        this.amount = amount;
    }

    public OwnedItem(Item item, long owner) {
        super(OwnedItem.TSV_HEADER);
        this.owner = owner;
        this.id = item.getId();
        this.amount = item.getAmount();
    }

    @Override
    public long getLastModified() {
        return 0;
    }

    @Override
    protected String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.owner);
        stringBuilder.append("\t");
        stringBuilder.append(this.id);
        stringBuilder.append("\t");
        stringBuilder.append(this.amount);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

}
