package de._125m125.ktv2.beans;

import java.sql.Timestamp;

import de._125m125.ktv2.cache.Tsvable;

/**
 * A message received by a user.
 */
public class Message extends Tsvable {

    /** The header for the tsv string. */
    public static final String HEADER = "timestamp\tmessage";

    /** The time of reception. */
    private final Timestamp    timestamp;

    /** The content. */
    private final String       message;

    /**
     * Instantiates a new message.
     *
     * @param timestamp
     *            the timestamp
     * @param content
     *            the content
     */
    public Message(final Timestamp timestamp, final String content) {
        super(Message.HEADER);
        this.timestamp = timestamp;
        this.message = content;
    }

    /* (non-Javadoc)
     * @see de._125m125.ktv2.cache.Tsvable#buildTsvString()
     */
    @Override
    public String buildTsvString() {
        return this.timestamp.getTime() + "\t" + this.message + "\r\n";
    }

    /* (non-Javadoc)
     * @see de._125m125.ktv2.cache.Tsvable#getLastModified()
     */
    @Override
    public long getLastModified() {
        return this.timestamp.getTime();
    }

    /**
     * Gets the time of receive.
     *
     * @return the time of receive
     */
    public Timestamp getTimestamp() {
        return this.timestamp;
    }

    /**
     * Gets the content.
     *
     * @return the content
     */
    public String getContent() {
        return this.message;
    }

}
