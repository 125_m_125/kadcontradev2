package de._125m125.ktv2.beans;

import java.io.IOException;
import java.time.LocalDate;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class LotteryItemConfig {

    public static class LocalDateAdapter extends TypeAdapter<LocalDate> {
        @Override
        public void write(final JsonWriter jsonWriter, final LocalDate localDate)
                throws IOException {
            if (localDate == null) {
                jsonWriter.nullValue();
            } else {
                jsonWriter.value(localDate.toString());
            }
        }

        @Override
        public LocalDate read(final JsonReader jsonReader) throws IOException {
            if (jsonReader.peek() == JsonToken.NULL) {
                return null;
            } else {
                return LocalDate.parse(jsonReader.nextString());
            }
        }
    }

    private final long      seller;
    private final String    item;

    private final LocalDate dailyInsertStart;
    private final LocalDate dailyInsertEnd;
    private final int       dailyInsertAmount;

    private final LocalDate start;
    private final LocalDate close;
    private final int       userTradeAmount;

    public LotteryItemConfig(final long seller, final String item, final LocalDate dailyInsertStart,
            final LocalDate dailyInsertEnd, final int dailyInsertAmount, final LocalDate start,
            final LocalDate close, final int userTradeAmount) {
        super();
        this.seller = seller;
        this.item = item;
        this.dailyInsertStart = dailyInsertStart;
        this.dailyInsertEnd = dailyInsertEnd;
        this.dailyInsertAmount = dailyInsertAmount;
        this.start = start;
        this.close = close;
        this.userTradeAmount = userTradeAmount;
    }

    public LocalDate getDailyInsertStart() {
        return this.dailyInsertStart;
    }

    public LocalDate getDailyInsertEnd() {
        return this.dailyInsertEnd;
    }

    public boolean dailyInsertOn(final LocalDate date) {
        return (this.dailyInsertStart == null || date.compareTo(this.dailyInsertStart) >= 0)
                && (this.dailyInsertEnd == null || date.compareTo(this.dailyInsertEnd) < 0);
    }

    public int getDailyInsertAmount() {
        return this.dailyInsertAmount;
    }

    public LocalDate getStart() {
        return this.start;
    }

    public LocalDate getClose() {
        return this.close;
    }

    public boolean tradeOn(final LocalDate date) {
        return (this.start == null || date.compareTo(this.start) >= 0)
                && (this.close == null || date.compareTo(this.close) < 0);
    }

    public int getUnserTradeAmount() {
        return this.userTradeAmount;
    }

    public String getItem() {
        return this.item;
    }

    public long getSeller() {
        return this.seller;
    }

}
