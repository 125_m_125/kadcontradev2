package de._125m125.ktv2.beans;

import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.calculators.IDSConverter;

public class OwnedItemMovement extends Tsvable {

    public static final String TSV_HEADER = "timestamp\towner\tmatid\tamount\treason";

    private final long   timestamp;
    private final long   owner;
    private final String matid;
    private final String amount;
    private final String reason;

    public OwnedItemMovement(long timestamp, long owner, String matid, long amount, String reason) {
        super(OwnedItemMovement.TSV_HEADER);
        this.timestamp = timestamp;
        this.owner = owner;
        this.matid = matid;
        this.amount = "-1".equals(matid) ? IDSConverter.longToDoubleString(amount)
                : String.valueOf(amount);
        this.reason = reason;
    }

    @Override
    public long getLastModified() {
        return this.timestamp;
    }

    @Override
    protected String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.timestamp);
        stringBuilder.append("\t");
        stringBuilder.append(this.owner);
        stringBuilder.append("\t");
        stringBuilder.append(this.matid);
        stringBuilder.append("\t");
        stringBuilder.append(this.amount);
        stringBuilder.append("\t");
        stringBuilder.append(this.reason);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }
}
