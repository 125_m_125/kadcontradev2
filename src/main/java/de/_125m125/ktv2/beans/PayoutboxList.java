package de._125m125.ktv2.beans;

public class PayoutboxList extends TsvableList<Payoutbox> {

    public PayoutboxList() {
        super(Payoutbox.HEADER);
    }

    @Override
    protected Payoutbox[] createArray(final int size) {
        return new Payoutbox[size];
    }

    @Override
    public synchronized Payoutbox[] getEntries() {
        return super.getEntries();
    }

}
