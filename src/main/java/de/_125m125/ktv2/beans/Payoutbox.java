package de._125m125.ktv2.beans;

import de._125m125.ktv2.cache.Tsvable;

public class Payoutbox extends Tsvable {

    public static final String HEADER = "boxid\tserver\tx\ty\tz\townerid\townername\tverified";
    private final long         boxid;
    private final int          server;
    private final int          x;
    private final int          y;
    private final int          z;
    private final long         ownerid;
    private final String       ownername;
    private final boolean      verified;

    public Payoutbox(final long boxid, final int server, final int x, final int y, final int z, final long ownerid,
            final String ownername, final boolean verified) {
        super(Payoutbox.HEADER);
        this.boxid = boxid;
        this.server = server;
        this.x = x;
        this.y = y;
        this.z = z;
        this.ownerid = ownerid;
        this.ownername = ownername;
        this.verified = verified;
    }

    @Override
    public long getLastModified() {
        return -1;
    }

    @Override
    protected String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.boxid);
        stringBuilder.append("\t");
        stringBuilder.append(this.server);
        stringBuilder.append("\t");
        stringBuilder.append(this.x);
        stringBuilder.append("\t");
        stringBuilder.append(this.y);
        stringBuilder.append("\t");
        stringBuilder.append(this.z);
        stringBuilder.append("\t");
        stringBuilder.append(this.ownerid);
        stringBuilder.append("\t");
        stringBuilder.append(this.ownername);
        stringBuilder.append("\t");
        stringBuilder.append(this.verified);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

    public long getBoxid() {
        return this.boxid;
    }

    public int getServer() {
        return this.server;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getZ() {
        return this.z;
    }

    public long getOwnerid() {
        return this.ownerid;
    }

    public String getOwnername() {
        return this.ownername;
    }

    public boolean isVerified() {
        return this.verified;
    }
}
