package de._125m125.ktv2.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.market.Trade.buySell;

public class HistoricTrade {

    public static HistoricTrade get(final long id) {
        final ArrayList<HashMap<String, Object>> entries = Main.instance().getConnector()
                .executeQuery("SELECT id,time,traderid,buysell,material,amount,price FROM tradeHistory WHERE id=" + id);
        if (entries != null && entries.size() == 1) {
            return HistoricTrade.of(entries.get(0));
        }
        return null;
    }

    public static HistoricTrade[] get(final long owner, final Timestamp time, final String material) {
        if ("-1".equals(material)) {
            final ArrayList<HashMap<String, Object>> entries = Main.instance().getConnector().executeQuery(
                    "SELECT id,time,traderid,buysell,material,amount,price FROM tradeHistory WHERE traderid=" + owner
                            + " AND type=1 AND buysell=1 AND time>? ",
                    time.toString());
            if (entries != null && entries.size() > 0) {
                final HistoricTrade[] result = new HistoricTrade[entries.size()];
                for (int i = 0; i < entries.size(); i++) {
                    result[i] = HistoricTrade.of(entries.get(i));
                }
                return result;
            }
        } else {
            final ArrayList<HashMap<String, Object>> entries = Main.instance().getConnector()
                    .executeQuery(
                            "SELECT id,time,traderid,buysell,material,amount,price FROM tradeHistory WHERE traderid="
                                    + owner + " AND type=1 AND buysell=0 AND material=? AND time>? ",
                            material, time.toString());
            if (entries != null && entries.size() > 0) {
                final HistoricTrade[] result = new HistoricTrade[entries.size()];
                for (int i = 0; i < entries.size(); i++) {
                    result[i] = HistoricTrade.of(entries.get(i));
                }
                return result;
            }
        }
        return new HistoricTrade[0];
    }

    private static HistoricTrade of(final HashMap<String, Object> hashMap) {
        return new HistoricTrade((long) hashMap.get("id"), (long) hashMap.get("traderid"),
                (Timestamp) hashMap.get("time"), (boolean) hashMap.get("buysell") ? buySell.BUY : buySell.SELL,
                (String) hashMap.get("material"), (long) hashMap.get("amount"), (long) hashMap.get("price"));
    }

    private final long                   id;
    private final long                   owner;
    private final Timestamp              creationTime;
    private final buySell                buyOrSell;
    private final String                 material;
    private final long                   amount;
    private final long                   price;
    private List<HistoricTradeExecution> executions;

    public HistoricTrade(final long id, final long owner, final Timestamp creationTime, final buySell buyOrSell,
            final String material, final long amount, final long price) {
        super();
        this.id = id;
        this.owner = owner;
        this.creationTime = creationTime;
        this.buyOrSell = buyOrSell;
        this.material = material;
        this.amount = amount;
        this.price = price;
    }

    public long getId() {
        return this.id;
    }

    public long getOwner() {
        return this.owner;
    }

    public Timestamp getCreationTime() {
        return this.creationTime;
    }

    public buySell getBuyOrSell() {
        return this.buyOrSell;
    }

    public String getMaterial() {
        return this.material;
    }

    public long getAmount() {
        return this.amount;
    }

    public long getPrice() {
        return this.price;
    }

    public List<HistoricTradeExecution> getExecutions() {
        if (this.executions == null) {
            synchronized (this) {
                if (this.executions == null) {
                    this.executions = HistoricTradeExecution.getFor(this);
                }
            }
        }
        return this.executions;
    }

    public JsonValue toJson() {
        final JsonObjectBuilder job = Json.createObjectBuilder();
        job.add("id", this.id);
        job.add("owner", this.owner);
        job.add("creationTime", this.creationTime.getTime());
        job.add("buyOrSell", this.buyOrSell == buySell.BUY ? true : false);
        job.add("material", this.material);
        job.add("amount", this.amount);
        job.add("price", this.price);
        return job.build();
    }
}
