package de._125m125.ktv2.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorReport {
    private String  message;
    private String  source;
    private Integer lineno;
    private Integer colno;
    private String  error;

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(final String source) {
        this.source = source;
    }

    public Integer getLineno() {
        return this.lineno;
    }

    public void setLineno(final Integer lineno) {
        this.lineno = lineno;
    }

    public Integer getColno() {
        return this.colno;
    }

    public void setColno(final Integer colno) {
        this.colno = colno;
    }

    public String getError() {
        return this.error;
    }

    public void setError(final String error) {
        this.error = error;
    }
}
