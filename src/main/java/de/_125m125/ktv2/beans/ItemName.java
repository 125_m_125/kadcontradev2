package de._125m125.ktv2.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.cache.Tsvable;

public class ItemName extends Tsvable {

    private static final class ItemnameTsvableList extends TsvableList<ItemName> {
        private ItemnameTsvableList() {
            super(ItemName.ITEMNAME_HEADER);
        }

        @Override
        protected ItemName[] createArray(final int size) {
            return new ItemName[size];
        }
    }

    private static final String          ITEMNAME_HEADER = "id\tname";
    private static TsvableList<ItemName> current;

    public static void create(final String[][] idsToNames) {
        final TsvableList<ItemName> items = new ItemnameTsvableList();
        for (final String[] idToName : idsToNames) {
            items.addEntry(new ItemName(idToName[0], idToName[1]));
        }
        ItemName.current = items;
    }

    public static TsvableList<ItemName> getCurrentItemnames() {
        if (ItemName.current == null) {
            synchronized (ItemName.class) {
                if (ItemName.current == null) {
                    create(Variables.allItems);
                }
            }
        }
        return ItemName.current;
    }

    public static TsvableList<ItemName> getCurrentStocknames(final String language) {
        return getObjectNames(1, language, false);
    }

    public static TsvableList<ItemName> getObjectNames(final long server, final String language,
            final boolean includeGeneralItems) {
        String query;
        if (language == null) {
            query = "SELECT id,name FROM objectnames WHERE (server=?";
            if (includeGeneralItems) {
                query += " OR server=0";
            }
            query += ") AND language=\"def\"";
        } else {
            query = "SELECT COALESCE(b.id,a.id) id, COALESCE(b.name,a.name) name from objectnames a LEFT JOIN objectnames b USING(server,id) WHERE (a.server=?";
            if (includeGeneralItems) {
                query += " OR a.server=0";
            }
            query += ") AND b.language=? AND a.language=\"def\"";
        }
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement(query)) {
            ps.setLong(1, server);
            if (language != null) {
                ps.setString(2, language);
            }
            try (ResultSet rs = ps.executeQuery()) {
                final TsvableList<ItemName> list = new ItemnameTsvableList();
                while (rs.next()) {
                    list.addEntry(new ItemName(rs.getString("id"), rs.getString("name")));
                }
                return list;
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private final String id;
    private final String name;

    public ItemName(final String id, final String name) {
        super(ItemName.ITEMNAME_HEADER);
        this.id = id;
        this.name = name;
    }

    @Override
    public long getLastModified() {
        return -1;
    }

    @Override
    protected String buildTsvString() {
        return this.id + "\t" + this.name + "\r\n";
    }

}
