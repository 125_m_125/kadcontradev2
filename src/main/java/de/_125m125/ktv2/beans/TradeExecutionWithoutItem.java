package de._125m125.ktv2.beans;

import java.sql.Timestamp;

import de._125m125.ktv2.cache.Tsvable;

public class TradeExecutionWithoutItem extends Tsvable {
    public static final String HEADER = "timestamp\tamount\tprice";

    private final Timestamp    timestamp;
    private final long         amount;
    private final String       price;

    public TradeExecutionWithoutItem(final Timestamp timestamp, final long amount,
            final String price) {
        this(TradeExecutionWithoutItem.HEADER, timestamp, amount, price);
    }

    protected TradeExecutionWithoutItem(final String header, final Timestamp timestamp,
            final long amount, final String price) {
        super(header);
        this.timestamp = timestamp;
        this.amount = amount;
        this.price = price;
    }

    protected TradeExecutionWithoutItem(final TradeExecutionWithoutItem tradeExecution) {
        this(TradeExecutionWithoutItem.HEADER, tradeExecution.timestamp, tradeExecution.amount,
                tradeExecution.price);
    }

    @Override
    public long getLastModified() {
        return this.timestamp.getTime();
    }

    @Override
    protected String buildTsvString() {
        return this.timestamp.getTime() + "\t" + this.amount + "\t" + this.price;
    }

}
