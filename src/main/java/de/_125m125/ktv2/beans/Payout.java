package de._125m125.ktv2.beans;

import java.sql.Timestamp;

import de._125m125.ktv2.Variables;
import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.calculators.IDSConverter;
import de._125m125.ktv2.market.Trade.ITEM_OR_STOCK;

public class Payout extends Tsvable {
    public static enum PAYOUT_STATE {
        OPEN(true, false, false, true, false),
        FORCED(false, false, false, true, false),
        UNKNOWN(false, false, false, true, true),
        IN_PROGRESS(false, false, false, false, false),
        UNKNOWN_IN_PROGRESS(false, false, false, false, true),
        SUCCESS(false, true, true, false, false),
        SPLIT(false, true, true, false, true),
        CANCELLED(false, true, false, false, false),
        FAILED(true, false, true, false, false),
        FAILED_TAKEN(false, false, true, false, false),;

        private final boolean userInteraction;
        private final boolean success;
        private final boolean adminInteractionCompleted;
        private final boolean requiresAdminInteraction;
        private final boolean unknown;

        private PAYOUT_STATE(final boolean userInteraction, final boolean success,
                final boolean adminInteractionCompleted, final boolean requiresAdminInteraction,
                final boolean unknown) {
            this.userInteraction = userInteraction;
            this.success = success;
            this.adminInteractionCompleted = adminInteractionCompleted;
            this.requiresAdminInteraction = requiresAdminInteraction;
            this.unknown = unknown;
        }

        public static PAYOUT_STATE of(final boolean userInteraction, final boolean success,
                final boolean adminInteractionCompleted, final boolean requiresAdminInteraction,
                final boolean unknown) {
            for (final PAYOUT_STATE ps : PAYOUT_STATE.values()) {
                if (userInteraction == ps.userInteraction && success == ps.success
                        && adminInteractionCompleted == ps.adminInteractionCompleted
                        && requiresAdminInteraction == ps.requiresAdminInteraction && unknown == ps.unknown) {
                    return ps;
                }
            }
            return null;
        }

        public boolean isUserInteraction() {
            return this.userInteraction;
        }

        public boolean isSuccess() {
            return this.success;
        }

        public boolean isAdminInteractionCompleted() {
            return this.adminInteractionCompleted;
        }

        public boolean isRequiresAdminInteraction() {
            return this.requiresAdminInteraction;
        }

        public boolean isUnknown() {
            return this.unknown;
        }

    }

    public static final String  HEADER = "id\tmaterial\tmaterialName\tamount\tstate\tpayoutType\tdate\tmessage";

    private final long          id;
    private final long          uid;
    private final ITEM_OR_STOCK itemOrStock;
    private final String        material;
    private final long          amount;
    private final PAYOUT_STATE  state;
    private final Timestamp     date;
    private final String        message;

    private final String        itemName;
    private final String        payoutType;

    public Payout(final long id, final String material, final long amount, final PAYOUT_STATE state,
            final String payoutType, final Timestamp date, final String message) {
        this(id, 0, ITEM_OR_STOCK.ITEM, material, amount, state, payoutType, date, message);
    }

    public Payout(final long id, final long uid, final String material, final long amount, final PAYOUT_STATE state,
            final String payoutType, final Timestamp date, final String message) {
        this(id, uid, ITEM_OR_STOCK.ITEM, material, amount, state, payoutType, date, message);
    }

    public Payout(final long id, final long uid, final ITEM_OR_STOCK itemOrStock, final String material,
            final long amount, final PAYOUT_STATE state, final String payoutType, final Timestamp date,
            final String message) {
        super(Payout.HEADER);
        this.id = id;
        this.uid = uid;
        this.itemOrStock = itemOrStock;
        this.material = material;
        this.amount = amount;
        this.state = state;
        this.payoutType = payoutType;
        this.date = date;
        this.message = message;
        this.itemName = Variables.ID_TO_DN.get(material);
    }

    public long getId() {
        return this.id;
    }

    public long getUid() {
        return this.uid;
    }

    public String getMaterial() {
        return this.material;
    }

    public long getAmount() {
        return this.amount;
    }

    public String getStringAmount() {
        if ("-1".equals(this.material)) {
            return IDSConverter.longToDoubleString(this.amount);
        } else {
            return String.valueOf(this.amount);
        }
    }

    public PAYOUT_STATE getState() {
        return this.state;
    }

    public Timestamp getDate() {
        return this.date;
    }

    public String getMessage() {
        return this.message;
    }

    public String getItemName() {
        return this.itemName;
    }

    @Override
    public String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.id);
        stringBuilder.append("\t");
        stringBuilder.append(this.material);
        stringBuilder.append("\t");
        stringBuilder.append(getItemName());
        stringBuilder.append("\t");
        stringBuilder.append((this.material.equals("-1") ? IDSConverter.longToDoubleString(this.amount) : this.amount));
        stringBuilder.append("\t");
        stringBuilder.append(Variables.translate(this.state.toString()));
        stringBuilder.append("\t");
        stringBuilder.append(this.payoutType);
        stringBuilder.append("\t");
        stringBuilder.append(this.date.getTime());
        stringBuilder.append("\t");
        stringBuilder.append(this.message);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

    @Override
    public long getLastModified() {
        return this.date.getTime();
    }

    public String getPayoutType() {
        return this.payoutType;
    }

    public ITEM_OR_STOCK getItemOrStock() {
        return this.itemOrStock;
    }

    public boolean isItem() {
        return this.itemOrStock == ITEM_OR_STOCK.ITEM;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (this.amount ^ (this.amount >>> 32));
        result = prime * result + ((this.date == null) ? 0 : this.date.hashCode());
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.itemOrStock == null) ? 0 : this.itemOrStock.hashCode());
        result = prime * result + ((this.material == null) ? 0 : this.material.hashCode());
        result = prime * result + ((this.message == null) ? 0 : this.message.hashCode());
        result = prime * result + ((this.payoutType == null) ? 0 : this.payoutType.hashCode());
        result = prime * result + ((this.state == null) ? 0 : this.state.hashCode());
        result = prime * result + (int) (this.uid ^ (this.uid >>> 32));
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Payout)) {
            return false;
        }
        final Payout other = (Payout) obj;
        if (this.amount != other.amount) {
            return false;
        }
        if (this.date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!this.date.equals(other.date)) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (this.itemOrStock != other.itemOrStock) {
            return false;
        }
        if (this.material == null) {
            if (other.material != null) {
                return false;
            }
        } else if (!this.material.equals(other.material)) {
            return false;
        }
        if (this.message == null) {
            if (other.message != null) {
                return false;
            }
        } else if (!this.message.equals(other.message)) {
            return false;
        }
        if (this.payoutType == null) {
            if (other.payoutType != null) {
                return false;
            }
        } else if (!this.payoutType.equals(other.payoutType)) {
            return false;
        }
        if (this.state != other.state) {
            return false;
        }
        if (this.uid != other.uid) {
            return false;
        }
        return true;
    }

}
