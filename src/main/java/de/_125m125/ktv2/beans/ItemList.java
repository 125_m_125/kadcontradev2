package de._125m125.ktv2.beans;

public class ItemList extends TsvableList<Item> {

    public ItemList() {
        super(Item.HEADER);
    }

    @Override
    protected Item[] createArray(final int size) {
        return new Item[size];
    }

    @Override
    public synchronized Item[] getEntries() {
        return super.getEntries();
    }

}
