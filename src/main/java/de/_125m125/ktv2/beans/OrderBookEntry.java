package de._125m125.ktv2.beans;

import de._125m125.ktv2.cache.Tsvable;

public class OrderBookEntry extends Tsvable {
    public static final String HEADER = "type\tprice\tamount";

    private String             type;
    private long               amount;
    private String             price;

    public OrderBookEntry() {
        super(OrderBookEntry.HEADER);
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public long getAmount() {
        return this.amount;
    }

    public void setAmount(final long amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    @Override
    public String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.type);
        stringBuilder.append("\t");
        stringBuilder.append(this.price);
        stringBuilder.append("\t");
        stringBuilder.append(this.amount);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

    @Override
    public long getLastModified() {
        return -1;
    }

}
