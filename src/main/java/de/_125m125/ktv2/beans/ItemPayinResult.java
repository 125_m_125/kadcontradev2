package de._125m125.ktv2.beans;

public class ItemPayinResult {
    private ItemList succeeded;
    private ItemList failed;

    public ItemPayinResult() {
    }

    public void addSucceeded(final String id, final long amount) {
        if (this.succeeded == null) {
            this.succeeded = new ItemList();
        }
        this.succeeded.addEntry(new Item(id, amount));
    }

    public void addFailed(final String id) {
        if (this.failed == null) {
            this.failed = new ItemList();
        }
        this.failed.addEntry(new Item(id));
    }

    public ItemList getSucceeded() {
        return this.succeeded;
    }

    public ItemList getFailed() {
        return this.failed;
    }

    public boolean hasFailures() {
        return this.failed != null && !this.failed.isEmpty();
    }

    public boolean hasSuccesses() {
        return this.succeeded != null && !this.succeeded.isEmpty();
    }
}
