package de._125m125.ktv2.beans;

import java.io.IOException;
import java.io.StringWriter;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

public class GeneratedCertificate {
    private ClientCertificate details;
    private String            certificate;
    private List<String>      permissions;
    private int               validity;

    public ClientCertificate getDetails() {
        return this.details;
    }

    public void setDetails(final ClientCertificate details) {
        this.details = details;
    }

    public String getCertificate() {
        return this.certificate;
    }

    public void setCertificate(final X509Certificate x509) throws IOException {
        try (StringWriter sw = new StringWriter()) {
            try (JcaPEMWriter pemWriter = new JcaPEMWriter(sw)) {
                pemWriter.writeObject(x509);
            }
            setCertificate(sw.toString());
        }
    }

    public void setCertificate(final String certificate) {
        this.certificate = certificate;
    }

    public List<String> getPermissions() {
        return this.permissions;
    }

    public void setPermissions(final List<String> permissions) {
        this.permissions = permissions;
    }

    public void addPermission(final String permission) {
        if (this.permissions == null) {
            this.permissions = new ArrayList<>();
        }
        this.permissions.add(permission);
    }

    public int getValidity() {
        return this.validity;
    }

    public void setValidity(final int validity) {
        this.validity = validity;
    }

}
