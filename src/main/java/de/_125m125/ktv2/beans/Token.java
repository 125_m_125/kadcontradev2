package de._125m125.ktv2.beans;

import java.math.BigInteger;
import java.util.Map;

import de._125m125.ktv2.servlets.api.helper.Permission;

public class Token {
    private static final int RADIX = 32;

    public static BigInteger getBigInt(final String sourceString) {
        if (sourceString.startsWith("D")) {
            return new BigInteger(sourceString.substring(1), 10);
        }
        return new BigInteger(sourceString, Token.RADIX);
    }

    public static String getBase32(final BigInteger source) {
        return source.toString(32);
    }

    private final String               tid;
    private final String               token;
    private final Map<String, Boolean> permissions;

    public Token(final long tid, final BigInteger token, final int permissions) {
        this.tid = Long.toString(tid, Token.RADIX);
        this.token = token.toString(Token.RADIX);
        this.permissions = Permission.generateComPermissionMap(permissions);
    }

    public String getTid() {
        return this.tid;
    }

    public String getToken() {
        return this.token;
    }

    public Map<String, Boolean> getPermissions() {
        return this.permissions;
    }

}
