package de._125m125.ktv2.beans;

public class OrderBook extends TsvableList<OrderBookEntry> {

    public OrderBook() {
        super(OrderBookEntry.HEADER);
    }

    @Override
    protected OrderBookEntry[] createArray(final int size) {
        return new OrderBookEntry[size];
    }

    @Override
    public synchronized OrderBookEntry[] getEntries() {
        return super.getEntries();
    }

}
