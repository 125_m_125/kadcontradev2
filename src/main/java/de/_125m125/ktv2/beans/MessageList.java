package de._125m125.ktv2.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageList extends TsvableList<Message> {

    public static MessageList of(final ResultSet rs) throws SQLException {
        final MessageList ml = new MessageList();
        while (rs.next()) {
            final Message m = new Message(rs.getTimestamp("time"), rs.getString("message"));
            ml.addEntry(m);
        }
        return ml;
    }

    public MessageList() {
        super(Message.HEADER);
    }

    @Override
    protected Message[] createArray(final int size) {
        return new Message[size];
    }

    @Override
    public synchronized Message[] getEntries() {
        return super.getEntries();
    }

}
