package de._125m125.ktv2.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import de._125m125.ktv2.Main;

public class HistoricTradeExecution {

    public static List<HistoricTradeExecution> getFor(final HistoricTrade sourceTrade) {
        final ArrayList<HashMap<String, Object>> entries = Main.instance().getConnector()
                .executeQuery("SELECT time,id2,amount FROM tradesDoneHistory WHERE id1=" + sourceTrade.getId());
        if (entries == null || entries.size() == 0) {
            return new ArrayList<>();
        }
        final ArrayList<HistoricTradeExecution> result = new ArrayList<>(entries.size());
        for (final HashMap<String, Object> entry : entries) {
            result.add(HistoricTradeExecution.of(sourceTrade, entry));
        }
        return result;

    }

    private static HistoricTradeExecution of(final HistoricTrade sourceTrade, final HashMap<String, Object> entry) {
        return new HistoricTradeExecution((Timestamp) entry.get("time"), (long) entry.get("id2"),
                (long) entry.get("amount"), sourceTrade);
    }

    private final Timestamp executionTime;
    private final long      id1;
    private final long      id2;
    private final long      amount;
    private HistoricTrade   trade1;
    private HistoricTrade   trade2;

    public HistoricTradeExecution(final Timestamp timestamp, final long id1, final long id2, final long amount) {
        this(timestamp, id1, id2, amount, null);
    }

    public HistoricTradeExecution(final Timestamp timestamp, final long id2, final long amount,
            final HistoricTrade source) {
        this(timestamp, source.getId(), id2, amount, source);
    }

    public HistoricTradeExecution(final Timestamp timestamp, final long id1, final long id2, final long amount,
            final HistoricTrade source) {
        this.executionTime = timestamp;
        this.id1 = id1;
        this.id2 = id2;
        this.amount = amount;
        this.trade1 = source;
    }

    public Timestamp getExecutionTime() {
        return this.executionTime;
    }

    public long getId1() {
        return this.id1;
    }

    public long getId2() {
        return this.id2;
    }

    public long getAmount() {
        return this.amount;
    }

    public HistoricTrade getTrade1() {
        if (this.trade1 == null) {
            synchronized (this) {
                if (this.trade1 == null) {
                    this.trade1 = HistoricTrade.get(this.id1);
                }
            }
        }
        return this.trade1;
    }

    public HistoricTrade getTrade2() {
        if (this.trade2 == null) {
            synchronized (this) {
                if (this.trade2 == null) {
                    this.trade2 = HistoricTrade.get(this.id2);
                }
            }
        }
        return this.trade2;
    }

    public JsonValue toJson() {
        final JsonObjectBuilder job = Json.createObjectBuilder();
        job.add("executionTime", this.executionTime.getTime());
        job.add("id1", this.id1);
        job.add("id2", this.id2);
        job.add("amount", this.amount);
        return job.build();
    }

}
