package de._125m125.ktv2.beans;

import de._125m125.ktv2.cache.Tsvable;

public class User extends Tsvable {
    public static final String TSV_HEADER = "id\tname";

    private final long   id;
    private final String name;

    public User(long id, String name) {
        super(User.TSV_HEADER);
        this.id = id;
        this.name = name;
    }

    @Override
    public long getLastModified() {
        return 0;
    }

    @Override
    protected String buildTsvString() {
        return this.id + "\t" + this.name + "\r\n";
    }

}
