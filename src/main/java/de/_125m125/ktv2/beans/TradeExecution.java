package de._125m125.ktv2.beans;

import java.sql.Timestamp;

public class TradeExecution extends TradeExecutionWithoutItem {
    public static final String HEADER = "material\t" + TradeExecutionWithoutItem.HEADER;

    private final String       material;

    public TradeExecution(final Timestamp timestamp, final String material, final long amount,
            final String price) {
        super(TradeExecution.HEADER, timestamp, amount, price);
        this.material = material;
    }

    @Override
    protected String buildTsvString() {
        return this.material + "\t" + super.buildTsvString();
    }

    public TradeExecutionWithoutItem withoutItem() {
        return new TradeExecutionWithoutItem(this);
    }
}
