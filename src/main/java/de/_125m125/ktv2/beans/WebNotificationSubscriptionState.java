package de._125m125.ktv2.beans;

public class WebNotificationSubscriptionState {

    private final boolean itemlist;
    private final boolean messages;
    private final boolean orders;
    private final boolean payouts;

    public WebNotificationSubscriptionState(final boolean itemlist, final boolean messages, final boolean orders,
            final boolean payouts) {
        this.itemlist = itemlist;
        this.messages = messages;
        this.orders = orders;
        this.payouts = payouts;
    }

    public boolean isItemlist() {
        return this.itemlist;
    }

    public boolean isMessages() {
        return this.messages;
    }

    public boolean isOrders() {
        return this.orders;
    }

    public boolean isPayouts() {
        return this.payouts;
    }

}
