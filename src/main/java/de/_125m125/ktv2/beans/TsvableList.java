package de._125m125.ktv2.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalLong;

import de._125m125.ktv2.cache.Tsvable;

/**
 * A list of tsvable entries that can be converted to tsv itself.
 *
 * @param <T>
 *            the generic type
 */
public abstract class TsvableList<T extends Tsvable> extends Tsvable {

    /** The entries of the list. */
    private final List<T>  entries = new ArrayList<>();

    /** The latest time an entry of the list was modified (or -1 if unknown). */
    private transient long lastModified;

    /**
     * Instantiates a new tsvable list.
     *
     * @param header
     *            the header for the tsv result
     */
    public TsvableList(final String header) {
        super(header);
    }

    /**
     * Adds a new entry.
     *
     * @param e
     *            the entry
     */
    public synchronized void addEntry(final T e) {
        this.entries.add(e);
        this.lastModified = Math.max(this.lastModified, e.getLastModified());
        invalidate();
    }

    /**
     * Adds a new entry at a given index.
     *
     * @param index
     *            the index
     * @param e
     *            the entry
     */
    public synchronized void addEntry(final int index, final T e) {
        this.entries.add(index, e);
        this.lastModified = Math.max(this.lastModified, e.getLastModified());
        invalidate();
    }

    /**
     * Checks if the list is empty.
     *
     * @return true, if the list is empty
     */
    public synchronized boolean isEmpty() {
        return this.entries.isEmpty();
    }

    /* (non-Javadoc)
     * @see de._125m125.ktv2.cache.Tsvable#buildTsvString()
     */
    @Override
    protected synchronized String buildTsvString() {
        final StringBuilder sb = new StringBuilder();
        this.entries.forEach(he -> {
            final String tsvString = he.getTsvString(false);
            sb.append(tsvString);
            if (!tsvString.endsWith("\r\n")) {
                sb.append("\r\n");
            }
        });
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see de._125m125.ktv2.cache.Tsvable#getLastModified()
     */
    @Override
    public synchronized long getLastModified() {
        if (this.lastModified == 0) {
            final OptionalLong max =
                    this.entries.stream().mapToLong(Tsvable::getLastModified).max();
            if (max.isPresent()) {
                this.lastModified = max.getAsLong();
            } else {
                this.lastModified = -1;
            }
        }
        return this.lastModified;
    }

    /**
     * Gets the entries.
     *
     * @return the entries
     */
    public synchronized T[] getEntries() {
        return this.entries.toArray(createArray(this.entries.size()));
    }

    /**
     * Creates an array for the elements of a given size.
     *
     * @param size
     *            the size of the array to create
     * @return the created array
     */
    protected abstract T[] createArray(int size);
}
