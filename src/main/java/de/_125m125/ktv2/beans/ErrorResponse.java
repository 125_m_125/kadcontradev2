package de._125m125.ktv2.beans;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.WebApplicationException;

import de._125m125.ktv2.Variables;

public class ErrorResponse {
    private static final Pattern p = Pattern.compile("HTTP [0-9]+ (.*)");

    private final int            code;
    private final String         message;
    private final String         humanReadableMessage;

    public ErrorResponse(final WebApplicationException exception) {
        this.code = exception.getResponse().getStatus();
        final Matcher m = ErrorResponse.p.matcher(exception.getMessage());
        if (m.find()) {
            this.message = m.group(1);
        } else {
            this.message = exception.getMessage();
        }
        this.humanReadableMessage = Variables.translate(this.message);
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public String getHumanReadableMessage() {
        return this.humanReadableMessage;
    }

}
