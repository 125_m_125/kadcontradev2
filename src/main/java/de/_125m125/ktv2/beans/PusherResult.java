package de._125m125.ktv2.beans;

public class PusherResult {
    private final String authdata;
    private final String channelname;

    public PusherResult(final String authdata, final String channelname) {
        this.authdata = authdata;
        this.channelname = channelname;
    }

    public String getAuthdata() {
        return this.authdata;
    }

    public String getChannelname() {
        return this.channelname;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.authdata == null) ? 0 : this.authdata.hashCode());
        result = prime * result + ((this.channelname == null) ? 0 : this.channelname.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PusherResult other = (PusherResult) obj;
        if (this.authdata == null) {
            if (other.authdata != null) {
                return false;
            }
        } else if (!this.authdata.equals(other.authdata)) {
            return false;
        }
        if (this.channelname == null) {
            if (other.channelname != null) {
                return false;
            }
        } else if (!this.channelname.equals(other.channelname)) {
            return false;
        }
        return true;
    }

}
