package de._125m125.ktv2.beans;

public class TradeSlotList extends TsvableList<TradeSlot> {

    public TradeSlotList() {
        super(TradeSlot.HEADER);
    }

    @Override
    public TradeSlot[] getEntries() {
        final TradeSlot[] values = super.getEntries();

        for (int i = 0; i < values.length; i++) {
            if (values[i] == null) {
                values[i] = new TradeSlot();
            }
        }

        return values;
    }

    @Override
    protected TradeSlot[] createArray(final int size) {
        return new TradeSlot[size + 1];
    }

}
