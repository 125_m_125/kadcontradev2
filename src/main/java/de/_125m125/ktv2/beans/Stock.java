package de._125m125.ktv2.beans;

import de._125m125.ktv2.cache.Tsvable;

public class Stock extends Tsvable {
    public static final String HEADER = "id\tname\tamount\ttotal";

    private final String id;
    private final long   amount;
    private long         totalCount;

    public Stock(final String id, final long amount) {
        super(Stock.HEADER);
        this.id = id;
        this.amount = amount;
    }

    public Stock(final String id, final long amount, final long totalCount) {
        super(Stock.HEADER);
        this.id = id;
        this.amount = amount;
        this.totalCount = totalCount;
    }

    @Override
    public String buildTsvString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.id);
        stringBuilder.append("\t");
        stringBuilder.append(this.amount);
        stringBuilder.append("\r\n");
        stringBuilder.append(this.totalCount);
        stringBuilder.append("\r\n");
        return stringBuilder.toString();
    }

    @Override
    public long getLastModified() {
        return -1;
    }

}
