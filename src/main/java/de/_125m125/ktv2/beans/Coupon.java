package de._125m125.ktv2.beans;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import de._125m125.ktv2.Main;
import de._125m125.ktv2.Variables;
import de._125m125.ktv2.datastorage.Connector.Successindicator;
import de._125m125.ktv2.datastorage.MySQLHelper;
import de._125m125.ktv2.logging.Log;
import de._125m125.ktv2.servlets.apiv2.ItemRestAPI;
import de._125m125.ktv2.servlets.apiv2.MessageRestAPI;

public class Coupon {
    private static final Random rnd             = new SecureRandom();
    private static final String insert          = "INSERT INTO giftcodes(precode, code, material, amount) VALUES(?,?,?,?)";
    private static final String get             = "SELECT precode, code, material, amount, redeemer FROM giftcodes WHERE precode=? AND code=?";
    private static final String getIfUnredeemed = "SELECT precode, code, material, amount, redeemer FROM giftcodes WHERE precode=? AND code=? AND ISNULL(redeemer)";
    private static final String redeem          = "UPDATE giftcodes SET redeemer=? WHERE precode=? AND code=? AND ISNULL(redeemer)";

    public static List<BigInteger> create(final String preCode, final String material,
            final long materialAmount, final int couponCount) {
        if (couponCount < 1) {
            throw new IllegalArgumentException("couponCount must be greater than zero");
        }
        if (!Variables.resourceExists(material)) {
            throw new IllegalArgumentException("resource not existing");
        }
        final ArrayList<BigInteger> result = new ArrayList<>(couponCount);

        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement preparedStatement = c.prepareStatement(Coupon.insert)) {
            preparedStatement.setString(1, preCode);
            preparedStatement.setString(3, material);
            preparedStatement.setLong(4, materialAmount);
            for (int i = 0; i < couponCount; i++) {
                try {
                    final BigInteger code = nextCode();
                    preparedStatement.setString(2, code.toString());
                    preparedStatement.executeUpdate();
                    result.add(code);
                } catch (final SQLException e) {
                    if (e.getErrorCode() == 1062) {
                        i--;
                    } else {
                        throw e;
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Coupon redeemIfPossible(final long user, final String codeString) {
        final Coupon c = getIfExists(codeString, true);
        if (c == null) {
            return null;
        }
        final Message[] sendMessage = new Message[1];
        final Successindicator exec = Main.instance().getConnector().executeAtomic(con -> {
            try (PreparedStatement ps = con.prepareStatement(Coupon.redeem)) {
                ps.setLong(1, user);
                ps.setString(2, c.getPreCode());
                ps.setString(3, c.getCode().toString());
                final int executeUpdate = ps.executeUpdate();
                if (executeUpdate == 1) {
                    final List<String[]> addResource = MySQLHelper.addResource(user, true,
                            c.getMaterial(), c.getAmount(), "couponRedemption " + codeString);
                    for (final String[] s : addResource) {
                        try (PreparedStatement ps2 = con.prepareStatement(s[0])) {
                            for (int i = 1; i < s.length; i++) {
                                ps2.setString(i, s[i]);
                            }
                            ps2.executeUpdate();
                        }
                    }
                    Log.COUPON.executeLog(con, Level.INFO,
                            "User " + user + " redeemed coupon " + c.getCodeAsString(), user);
                    sendMessage[0] = MySQLHelper.sendMessage(con, user, "couponRedeemedMessage",
                            c.getAmount(), Variables.getDisplayNameForMatId(c.getMaterial()));
                    return () -> true;
                } else {
                    Log.COUPON.log(Level.INFO,
                            "User " + user + " failed to redeem Coupon " + codeString, user);
                    return () -> false;
                }
            }
        });
        if (exec.wasSuccessful()) {
            c.setRedeemer(user);
            ItemRestAPI.invalidate(String.valueOf(user), () -> Item.getItems(user, c.getMaterial()),
                    true);
            MessageRestAPI.invalidate(String.valueOf(user), sendMessage, true);
            return c;
        } else {
            Log.COUPON.log(Level.INFO, "User " + user + " failed to redeem Coupon " + codeString,
                    user);
            return null;
        }
    }

    public static Coupon getIfExists(final String codeString) {
        return getIfExists(codeString, false);
    }

    public static Coupon getIfExists(String codeString, final boolean unredeemed) {
        if (codeString == null || codeString.length() < 17 || !codeString.startsWith("kt")) {
            return null;
        }
        codeString = codeString.replace("-", "");
        if (codeString.length() != 17) {
            return null;
        }
        final String preCode = codeString.substring(2, 4);
        final BigInteger code;
        try {
            code = stringToCode(codeString);
        } catch (final IllegalArgumentException e) {
            return null;
        }
        return getIfExists(preCode, code);
    }

    public static Coupon getIfExists(final String preCode, final BigInteger code) {
        return getIfExists(preCode, code, false);
    }

    public static Coupon getIfExists(final String preCode, final BigInteger code,
            final boolean unredeemed) {
        try (final Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement query = c
                        .prepareStatement(unredeemed ? Coupon.getIfUnredeemed : Coupon.get)) {
            query.setString(1, preCode);
            query.setString(2, code.toString());
            try (ResultSet result = query.executeQuery()) {
                if (result.next()) {
                    final String material = result.getString("material");
                    final long amount = result.getLong("amount");
                    final long redeemer = result.getLong("redeemer");
                    return new Coupon(preCode, code, material, amount, redeemer);
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static BigInteger nextCode() {
        return new BigInteger(64, Coupon.rnd);
    }

    private static final BigInteger MAX_VALUE = new BigInteger("18446744073709551615");

    public static String codeToString(final String preCode, final BigInteger code) {
        if (preCode == null || preCode.length() != 2) {
            throw new IllegalArgumentException("invalid preCode: " + preCode);
        }
        if (code == null || Coupon.MAX_VALUE.compareTo(code) < 0
                || BigInteger.ZERO.compareTo(code) > 0) {
            throw new IllegalArgumentException("code out of range: " + code);
        }
        String base32Code = code.toString(32);
        while (base32Code.length() < 13) {
            base32Code = "0" + base32Code;
        }
        final StringBuilder sb = new StringBuilder("kt");
        sb.append(preCode);
        for (int i = 0; i < 3; i++) {
            sb.append("-");
            sb.append(base32Code.substring(i * 4, (i + 1) * 4));
        }
        sb.append("-");
        sb.append(base32Code.substring(12));
        return sb.toString();
    }

    public static BigInteger stringToCode(String codeString) {
        if (codeString == null) {
            throw new IllegalArgumentException("codeString is null");
        }
        codeString = codeString.replace("-", "");
        if (codeString.length() != 17) {
            throw new IllegalArgumentException("invalid codeString");
        }
        final BigInteger code = new BigInteger(codeString.substring(4), 32);
        if (Coupon.MAX_VALUE.compareTo(code) < 0) {
            throw new IllegalArgumentException("code is too big");
        }
        return code;
    }

    private final String     preCode;
    private final BigInteger code;
    private final String     material;
    private final long       amount;
    private Long             redeemer;

    public Coupon(final String preCode, final BigInteger code, final String material,
            final long amount, final long redeemer) {
        this.preCode = preCode;
        this.code = code;
        this.material = material;
        this.amount = amount;
        this.redeemer = redeemer;

    }

    public String getPreCode() {
        return this.preCode;
    }

    public BigInteger getCode() {
        return this.code;
    }

    public String getMaterial() {
        return this.material;
    }

    public long getAmount() {
        return this.amount;
    }

    public boolean isRedeemed() {
        return this.redeemer != null;
    }

    public Long getRedeemer() {
        return this.redeemer;
    }

    public String getCodeAsString() {
        return codeToString(this.preCode, this.code);
    }

    private void setRedeemer(final long redeemer) {
        this.redeemer = redeemer;
    }
}
