package de._125m125.ktv2.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de._125m125.ktv2.Main;

public class GdprSettings {
    public static GdprSettings loadSettings(final long userid) {
        final GdprSettings gdprSettings = new GdprSettings();
        try (Connection c = Main.instance().getConnector().getConnection();
                PreparedStatement ps = c.prepareStatement("SELECT * FROM gdpr WHERE user=?")) {
            ps.setLong(1, userid);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    switch (rs.getString("type")) {
                    case "PL":
                        gdprSettings.setPublishLotteryData(true);
                        break;
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return gdprSettings;
    }

    private boolean publishLotteryData = false;

    public boolean isPublishLotteryData() {
        return this.publishLotteryData;
    }

    public void setPublishLotteryData(final boolean publishLotteryData) {
        this.publishLotteryData = publishLotteryData;
    }
}
