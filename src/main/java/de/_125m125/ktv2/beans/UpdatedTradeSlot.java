package de._125m125.ktv2.beans;

public class UpdatedTradeSlot extends TradeSlot {
    public static final String HEADER =
            TradeSlot.HEADER + "\tupdatedMoney\tupdatedItems\tcancelled";

    private long updatedMoney;
    private long updatedItems;

    private boolean cancelled = false;

    public UpdatedTradeSlot() {
        super(UpdatedTradeSlot.HEADER);
    }

    public long getUpdatedMoney() {
        return this.updatedMoney;
    }

    public void setUpdatedMoney(final long updatedMoney) {
        this.updatedMoney = updatedMoney;
    }

    public long getUpdatedItems() {
        return this.updatedItems;
    }

    public void setUpdatedItems(final long updatedItems) {
        this.updatedItems = updatedItems;
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setCancelled(final boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (this.cancelled ? 1231 : 1237);
        result = prime * result + (int) (this.updatedItems ^ (this.updatedItems >>> 32));
        result = prime * result + (int) (this.updatedMoney ^ (this.updatedMoney >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UpdatedTradeSlot other = (UpdatedTradeSlot) obj;
        if (this.cancelled != other.cancelled) {
            return false;
        }
        if (this.updatedItems != other.updatedItems) {
            return false;
        }
        if (this.updatedMoney != other.updatedMoney) {
            return false;
        }
        return true;
    }

    @Override
    public String buildTsvString() {
        final StringBuilder sb = new StringBuilder(super.buildTsvString());
        sb.append("\t");
        sb.append(this.updatedMoney);
        sb.append("\t");
        sb.append(this.updatedItems);
        sb.append("\t");
        sb.append(this.cancelled);
        return sb.toString();
    }

}
