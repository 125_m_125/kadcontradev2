package de._125m125.ktv2.beans;

import java.math.BigInteger;

import de._125m125.ktv2.cache.Tsvable;

public class ClientCertificate extends Tsvable {
    public static final String HEADER = "serialNr\ttype";

    public static enum Type {
        REVOKED(0),
        TWO_FACTOR_AUTH(1),
        API(2),;

        private final int mysqlId;

        private Type(final int mysqlId) {
            this.mysqlId = mysqlId;

        }

        public int getMysqlId() {
            return this.mysqlId;
        }
    }

    private final BigInteger serialNo;
    private final Type       type;

    public ClientCertificate(final BigInteger serialNo, final Type type) {
        super(ClientCertificate.HEADER);
        this.serialNo = serialNo;
        this.type = type;
    }

    public ClientCertificate(final BigInteger serialNo, final int type) {
        super(ClientCertificate.HEADER);
        this.serialNo = serialNo;
        // this.type = Type.values()[type];
        switch (type) {
        case 0:
            this.type = Type.REVOKED;
            break;
        case 1:
            this.type = Type.TWO_FACTOR_AUTH;
            break;
        case 2:
            this.type = Type.API;
            break;
        default:
            throw new IllegalArgumentException("Unknown type id");
        }
    }

    public BigInteger getSerialNo() {
        return this.serialNo;
    }

    public Type getType() {
        return this.type;
    }

    @Override
    public long getLastModified() {
        return 0;
    }

    @Override
    protected String buildTsvString() {
        return new StringBuilder().append(this.serialNo).append("\t").append(this.type).append("\r\n").toString();
    }

}
