package de._125m125.ktv2.beans;

import java.sql.Date;

import de._125m125.ktv2.cache.Tsvable;
import de._125m125.ktv2.calculators.IDSConverter;

public class HistoryEntry extends Tsvable {
    public static final String HEADER = "date\topen\thigh\tlow\tclose\tunitVolume\tdollarVolume";
    private final String       close;
    private final String       open;
    private final String       high;
    private final String       low;
    private final long         unitVolume;
    private final String       dollarVolume;
    private final Date         date;

    public HistoryEntry(final Long open, final Long close, final Long low, final Long high, final long amount,
            final long total, final Date date) {
        super(HistoryEntry.HEADER);
        this.dollarVolume = IDSConverter.longToDoubleString(total);
        this.close = IDSConverter.longToDoubleString(close);
        this.open = open != null ? IDSConverter.longToDoubleString(open) : "";
        this.high = high != null ? IDSConverter.longToDoubleString(high) : "";
        this.low = low != null ? IDSConverter.longToDoubleString(low) : "";
        this.unitVolume = amount;
        this.date = date;
    }

    public String getClose() {
        return this.close;
    }

    public String getOpen() {
        return this.open;
    }

    public String getHigh() {
        return this.high;
    }

    public String getLow() {
        return this.low;
    }

    public long getAmount() {
        return this.unitVolume;
    }

    public Date getDate() {
        return this.date;
    }

    public String getTotal() {
        return this.dollarVolume;
    }

    @Override
    public long getLastModified() {
        return this.date.getTime();
    }

    @Override
    protected String buildTsvString() {
        return new StringBuilder().append(this.date).append("\t").append(this.open).append("\t").append(this.high)
                .append("\t").append(this.low).append("\t").append(this.close).append("\t").append(this.unitVolume)
                .append("\t").append(this.dollarVolume).append("\r\n").toString();
    }

}
