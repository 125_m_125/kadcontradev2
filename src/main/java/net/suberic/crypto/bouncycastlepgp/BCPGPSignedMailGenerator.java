package net.suberic.crypto.bouncycastlepgp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Enumeration;
import java.util.Iterator;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentSignerBuilder;

public class BCPGPSignedMailGenerator {
    private final PGPSignatureGenerator fact;
    private final PGPPrivateKey         privKey;
    private final int                   hashAlgo;

    /**
     * base constructor
     */
    public BCPGPSignedMailGenerator(final PGPPrivateKey privKey, final PGPPublicKey pubKey, final int hashAlgorithm,
            final String provider) throws NoSuchAlgorithmException, NoSuchProviderException, PGPException {
        this.hashAlgo = hashAlgorithm;
        this.privKey = privKey;

        this.fact = new PGPSignatureGenerator(
                new JcaPGPContentSignerBuilder(pubKey.getAlgorithm(), this.hashAlgo).setProvider(provider));
        this.fact.init(PGPSignature.CANONICAL_TEXT_DOCUMENT, privKey);

        final PGPSignatureSubpacketGenerator spGen = new PGPSignatureSubpacketGenerator();
        final Iterator it = pubKey.getUserIDs();
        if (it.hasNext()) {
            spGen.setSignerUserID(false, (String) it.next());
            this.fact.setHashedSubpackets(spGen.generate());
        }
    }

    /**
     * at this point we expect our body part to be well defined.
     * 
     * @throws XMaiLException
     */
    public MimeMultipart generateMIME(MimeBodyPart body, final String sigProvider) throws Exception {
        final StringBuffer sb = new StringBuffer("signed; protocol=\"application/pgp-signature\"");
        addHashHeader(sb);

        final MimeMultipart mp = new MimeMultipart(sb.toString());

        body = makeContentBodyPart(body);

        mp.addBodyPart(body);

        final InternetHeaders headers = new InternetHeaders();
        headers.addHeader("Content-Type", "application/pgp-signature; name=\"signature.asc\"");
        headers.addHeader("Content-Disposition", "attachment; filename=\"signature.asc\"");
        headers.addHeader("Content-Description", "OpenPGP digital signature");

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            body.writeTo(baos);
        } catch (final IOException e) {
            ;
        }

        final PGPSignature sig = make(baos.toByteArray());
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        // Hashtable headers = new Hashtable();
        // headers.put("Comment", "Created by Lijun Liao");

        final ArmoredOutputStream aOut = new ArmoredOutputStream(out);// ,
                                                                      // headers);
        final BCPGOutputStream bOut = new BCPGOutputStream(aOut);
        sig.encode(bOut);
        aOut.close();

        final byte[] encoded = out.toByteArray();

        final MimeBodyPart sigPart = new MimeBodyPart(headers, encoded);

        mp.addBodyPart(sigPart);

        return mp;
    }

    private void addHashHeader(final StringBuffer header) {
        String strHashAlgo;

        switch (this.hashAlgo) {
        case HashAlgorithmTags.MD5:
            strHashAlgo = "pgp-md5";
            break;
        case HashAlgorithmTags.SHA1:
            strHashAlgo = "pgp-sha1";
            break;
        case HashAlgorithmTags.RIPEMD160:
            strHashAlgo = "pgp-ripemd160";
            break;
        default:
            strHashAlgo = "unknown";
            break;
        }
        header.append("; micalg=" + strHashAlgo);
    }

    /**
     * at this point we expect our body part to be well defined - generate with
     * data in the signature
     * 
     * @throws Exception
     */
    public String generateTextBody(final String content, final String sigProvider) throws Exception {
        final byte[] encodedContent = content.getBytes();

        final PGPSignature sig = make(encodedContent);

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ArmoredOutputStream aOut = new ArmoredOutputStream(out);

        aOut.beginClearText(this.hashAlgo);
        aOut.write(encodedContent);
        aOut.write('\n');
        aOut.endClearText();

        final BCPGOutputStream bOut = new BCPGOutputStream(aOut);
        sig.encode(bOut);
        aOut.close();

        return new String(out.toByteArray());
    }

    /**
     * This method must be called if not the first message will be generated
     * 
     * @throws PGPException
     */
    public void reset() throws PGPException {
        this.fact.init(PGPSignature.CANONICAL_TEXT_DOCUMENT, this.privKey);
    }

    public PGPSignature make(final InputStream content) throws PGPException, SignatureException, IOException {
        reset();

        int ch;
        while ((ch = content.read()) >= 0) {
            this.fact.update((byte) ch);
        }

        return this.fact.generate();
    }

    public PGPSignature make(final byte[] contentBytes) throws PGPException, SignatureException, IOException {
        reset();

        this.fact.update(contentBytes);

        return this.fact.generate();
    }

    /**
     * Make sure we have a valid content body part - setting the headers with
     * defaults if neccessary.
     */
    protected MimeBodyPart makeContentBodyPart(final MimeBodyPart content) throws PGPException {
        //
        // add the headers to the body part - if they are missing, in
        // the event they have already been set the content settings override
        // any defaults that might be set.
        //
        try {
            final MimeMessage msg = new MimeMessage((Session) null);

            Enumeration e = content.getAllHeaders();

            msg.setDataHandler(content.getDataHandler());

            while (e.hasMoreElements()) {
                final Header hdr = (Header) e.nextElement();

                msg.setHeader(hdr.getName(), hdr.getValue());
            }

            msg.saveChanges();

            //
            // we do this to make sure at least the default headers are
            // set in the body part.
            //
            e = msg.getAllHeaders();

            while (e.hasMoreElements()) {
                final Header hdr = (Header) e.nextElement();

                if (hdr.getName().toLowerCase().startsWith("content-")) {
                    content.setHeader(hdr.getName(), hdr.getValue());
                }
            }
        } catch (final MessagingException e) {
            throw new PGPException("exception saving message state.", e);
        }

        return content;
    }
}
