package net.suberic.crypto.bouncycastlepgp;

import net.suberic.crypto.PGPEncryptionUtils;

/**
 * PGP Encryption Utils that use the Cryptix PGP Provider.
 */
public class BCPGPEncryptionUtils extends PGPEncryptionUtils {
  /**
   * Creates a new instance of this class and sets the PGPProviderImpl
   * to a CryptixPGPProviderImpl.
   */
  public BCPGPEncryptionUtils() {
    javax.activation.MailcapCommandMap _mailcap =
      (javax.activation.MailcapCommandMap)javax.activation.CommandMap.getDefaultCommandMap();
    
    _mailcap.addMailcap("application/pgp-signature;;x-java-content-handler=com.sun.mail.handlers.text_plain");
    javax.activation.CommandMap.setDefaultCommandMap(_mailcap);
    
    setPGPProviderImpl(new BCPGPProviderImpl());
  }
}
