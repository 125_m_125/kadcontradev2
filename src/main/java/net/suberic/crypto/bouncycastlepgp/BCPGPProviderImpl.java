package net.suberic.crypto.bouncycastlepgp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.bc.BcPublicKeyKeyEncryptionMethodGenerator;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;

import net.suberic.crypto.EncryptionKeyManager;
import net.suberic.crypto.PGPProviderImpl;

/**
 * Something which decrypts PGP streams.
 */
public class BCPGPProviderImpl implements PGPProviderImpl {

    public BCPGPProviderImpl() {
        // register the handler
        final MailcapCommandMap _mailcap = (MailcapCommandMap) CommandMap.getDefaultCommandMap();

        _mailcap.addMailcap(
                "application/pgp-keys;;x-java-content-handler=de.rub.crypto.openpgp.handler.ApplicationPGPKeysHandler");

        CommandMap.setDefaultCommandMap(_mailcap);

        // register the BouncyCastle provider.
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
    }

    /**
     * Decrypts a section of text using a Key.
     */
    @Override
    public byte[] decrypt(java.io.InputStream encryptedStream, final java.security.Key key)
            throws java.security.NoSuchAlgorithmException, java.io.IOException, java.security.GeneralSecurityException {
        final BCPGPEncryptionKey pgpKey = (BCPGPEncryptionKey) key;
        final PGPPrivateKey priKey = pgpKey.getPrivateKey();
        if (priKey == null) {
            throw new InvalidKeyException("Found no private key");
        }

        final PGPPublicKey pubKey = pgpKey.getPublicKey();

        try {
            encryptedStream = PGPUtil.getDecoderStream(encryptedStream);

            final PGPObjectFactory pgpF = new PGPObjectFactory(encryptedStream, new JcaKeyFingerprintCalculator());
            final PGPEncryptedDataList encList = (PGPEncryptedDataList) pgpF.nextObject();

            PGPPublicKeyEncryptedData encP = null;
            // Find the encrypted session packet
            for (int i = 0; i < encList.size(); i++) {
                final PGPPublicKeyEncryptedData tmp = (PGPPublicKeyEncryptedData) encList.get(i);
                if (tmp.getKeyID() == pubKey.getKeyID()) {
                    encP = tmp;
                    break;
                }
            }

            if (encP == null) {
                throw new GeneralSecurityException("Found no packet encrypted by the specified key");
            }

            final InputStream clear = encP.getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder()
                    .setProvider(PGPUtil.getDefaultProvider()).build(priKey));

            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int ch;
            while ((ch = clear.read()) >= 0) {
                baos.write(ch);
            }

            final byte[] clearBytes = baos.toByteArray();

            try {
                PGPObjectFactory pgpFact = new PGPObjectFactory(clearBytes, new JcaKeyFingerprintCalculator());
                final PGPCompressedData cData = (PGPCompressedData) pgpFact.nextObject();

                pgpFact = new PGPObjectFactory(cData.getDataStream(), new JcaKeyFingerprintCalculator());
                final Object obj = pgpFact.nextObject();
                if (obj instanceof PGPLiteralData) {
                    final PGPLiteralData ld = (PGPLiteralData) obj;
                    final InputStream is = ld.getInputStream();
                    final ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                    while ((ch = is.read()) >= 0) {
                        baos2.write(ch);
                    }

                    return baos2.toByteArray();
                }
            } catch (final Exception e) {
                // The text is not compressed
                return clearBytes;
            }

            return null;
        } catch (final PGPException e) {
            e.printStackTrace();
            throw new GeneralSecurityException("Incorrectly formed message:  " + e.toString());
        }
    }

    /**
     * Encrypts a section of text using keys.
     */
    @Override
    public byte[] encrypt(final java.io.InputStream rawStream, final java.security.Key[] keys)
            throws java.security.NoSuchAlgorithmException, java.io.IOException, java.security.GeneralSecurityException {
        try {
            final ByteArrayOutputStream cbOut = new ByteArrayOutputStream();
            final ArmoredOutputStream aOut = new ArmoredOutputStream(cbOut);

            final PGPEncryptedDataGenerator cPk = new PGPEncryptedDataGenerator(
                    new JcePGPDataEncryptorBuilder(SymmetricKeyAlgorithmTags.AES_256).setWithIntegrityPacket(true)
                            .setSecureRandom(new SecureRandom()).setProvider(PGPUtil.getDefaultProvider()));

            for (int i = 0; i < keys.length; i++) {
                final BCPGPEncryptionKey pgpKey = (BCPGPEncryptionKey) keys[i];
                // BouncyCastlePGPEncryptionKey pgpKey =
                // (BouncyCastlePGPEncryptionKey) keys[keys.length - 1-i];
                final PGPPublicKey puK = pgpKey.getPublicKey();
                cPk.addMethod(new BcPublicKeyKeyEncryptionMethodGenerator(puK));
            }

            final ByteArrayOutputStream tmp = new ByteArrayOutputStream();
            int ch;
            while ((ch = rawStream.read()) >= 0) {
                tmp.write(ch);
            }

            final byte[] bytes = tmp.toByteArray();

            final ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            final PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(CompressionAlgorithmTags.ZIP);
            final PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
            final OutputStream os = lData.open(comData.open(bOut), PGPLiteralData.BINARY, PGPLiteralData.CONSOLE,
                    (long) bytes.length, new Date());
            os.write(bytes);
            lData.close();
            comData.close();

            final byte[] text = bOut.toByteArray();
            final OutputStream cOut = cPk.open(aOut, text.length);
            cOut.write(text);
            cOut.close();

            aOut.close();
            return cbOut.toByteArray();
        } catch (final PGPException e) {
            throw new GeneralSecurityException("Incorrectly formed message:  " + e.getMessage());
        }
    }

    /**
     * Signs a section of text.
     */
    @Override
    public byte[] sign(final InputStream rawStream, final java.security.Key key)
            throws java.security.NoSuchAlgorithmException, java.io.IOException, GeneralSecurityException {
        try {
            final BCPGPEncryptionKey _key = (BCPGPEncryptionKey) key;
            final PGPPrivateKey signKey = _key.getPrivateKey();
            if (signKey == null) {
                throw new InvalidKeyException("Found no private key");
            }

            final PGPPublicKey pubKey = _key.getPublicKey();
            // create the generator for creating an smime/signed message

            final int hashAlgo = HashAlgorithmTags.SHA1;
            final BCPGPSignedMailGenerator gen = new BCPGPSignedMailGenerator(signKey, pubKey, hashAlgo,
                    PGPUtil.getDefaultProvider());

            final PGPSignature sig = gen.make(rawStream);

            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final ArmoredOutputStream aOut = new ArmoredOutputStream(out);

            final BCPGOutputStream bOut = new BCPGOutputStream(aOut);
            sig.encode(bOut);
            aOut.close();

            return out.toByteArray();

        } catch (final PGPException pe) {
            throw new GeneralSecurityException("PGPException: " + pe.getMessage());
        }
    }

    /**
     * Checks a signature against a section of text.
     */
    @Override
    public boolean checkSignature(final MimeMultipart mPart, final java.security.Key key)
            throws java.security.NoSuchAlgorithmException, java.io.IOException, GeneralSecurityException {

        try {
            final BCPGPSigned s = new BCPGPSigned(mPart);
            final BCPGPEncryptionKey verifyingKey = (BCPGPEncryptionKey) key;

            final PGPSignature signer = s.getSigners()[0];

            final Object content = s.getContent();
            if (content instanceof byte[]) {
                return verify(signer, (byte[]) content, verifyingKey.getPublicKey());
            } else {
                final MimeBodyPart bp = (MimeBodyPart) content;
                final ByteArrayOutputStream tmpOut = new ByteArrayOutputStream();
                bp.writeTo(tmpOut);
                return verify(signer, tmpOut.toByteArray(), verifyingKey.getPublicKey());
            }
        } catch (final InvalidKeyException ie) {
            throw ie;
        } catch (final Exception e) {
            throw new GeneralSecurityException("Exception while verifying PGP signature: " + e.getMessage());
        }

    }

    /**
     * Returns a KeyStore provider.
     */
    @Override
    public EncryptionKeyManager createKeyManager() {
        return new BCPGPKeyManager();
    }

    /**
     * Returns a KeyStore provider.
     */
    @Override
    public EncryptionKeyManager createKeyManager(final java.io.InputStream inputStream, final char[] password)
            throws IOException, java.security.KeyStoreException, java.security.NoSuchAlgorithmException,
            java.security.cert.CertificateException, java.security.NoSuchProviderException {

        final EncryptionKeyManager keyMgr = new BCPGPKeyManager();
        keyMgr.loadPublicKeystore(inputStream, password);
        keyMgr.loadPrivateKeystore(inputStream, password);
        return keyMgr;
    }

    /**
     * verify the signature (assuming the cert is contained in the message)
     */
    private boolean verify(final PGPSignature signer, final byte[] content, final PGPPublicKey verifyingKey)
            throws Exception {
        if (signer.getKeyID() != verifyingKey.getKeyID()) {
            throw new InvalidKeyException("The key (" + verifyingKey.getKeyID() + ") cannot "
                    + "verify the signature created by key (" + signer.getKeyID() + ")");
        }

        //
        // verify that the sig is correct and that it was generated
        // when the certificate was current
        //
        final Date signingTime = signer.getCreationTime();

        boolean verified = true;

        // Verifying whether the public key is valid while signing
        if (verifyingKey.getValidSeconds() != 0) {
            final long diff = signingTime.getTime() - verifyingKey.getCreationTime().getTime();
            if (diff / 1000 > verifyingKey.getValidSeconds()) {
                verified = false;
            }
        }
        signer.init(new JcaPGPContentVerifierBuilderProvider().setProvider(PGPUtil.getDefaultProvider()), verifyingKey);

        signer.update(content);

        if (verified) {
            verified = signer.verify();
        }

        return verified;
    }

    public boolean checkSignature(final String pgpSignedMessage, final Key key)
            throws NoSuchAlgorithmException, IOException, GeneralSecurityException {
        final ByteArrayInputStream in = new ByteArrayInputStream(pgpSignedMessage.getBytes());
        final ArmoredInputStream aIn = new ArmoredInputStream(in);
        //
        // read the input, making sure we ingore the last newline.
        //
        int ch;
        boolean newLine = false;
        final ByteArrayOutputStream bOut = new ByteArrayOutputStream();

        // while ((ch = aIn.read()) >= 0 && aIn.isClearText())
        while ((ch = aIn.read()) >= 0 && aIn.isClearText()) {
            if (newLine) {
                bOut.write((byte) '\n');
                newLine = false;
            }
            if (ch == '\n') {
                newLine = true;
                continue;
            }

            bOut.write((byte) ch);
        }

        byte[] bytes = bOut.toByteArray();
        if (bytes[bytes.length - 1] == '\r') {
            final byte[] tmpBytes = new byte[bytes.length - 1];
            System.arraycopy(bytes, 0, tmpBytes, 0, tmpBytes.length);
            bytes = tmpBytes;
        }

        final PGPObjectFactory pgpFact = new PGPObjectFactory(aIn, new JcaKeyFingerprintCalculator());
        final PGPSignatureList p3 = (PGPSignatureList) pgpFact.nextObject();
        if (p3.size() < 1) {
            throw new GeneralSecurityException("No signature found in the stream");
        }
        final PGPSignature signer = p3.get(0);

        final PGPPublicKey verifyingKey = ((BCPGPEncryptionKey) key).getPublicKey();
        try {
            return verify(signer, bytes, verifyingKey);
        } catch (final InvalidKeyException ie) {
            throw ie;
        } catch (final Exception e) {
            throw new GeneralSecurityException("Exception while verifying PGP signature: " + e.getMessage());
        }
    }

    /**
     * @see net.suberic.crypto.PGPProviderImpl#encrypt(java.io.InputStream,
     *      java.security.Key)
     * @deprecated uses {@link #encrypt(InputStream, Key[])} instead.
     */
    @Deprecated
    @Override
    public byte[] encrypt(final InputStream rawStream, final Key key)
            throws NoSuchAlgorithmException, IOException, GeneralSecurityException {
        return encrypt(rawStream, new java.security.Key[] { key });
    }

    @Override
    public Key[] extractKeys(final InputStream rawStream) throws GeneralSecurityException, IOException {
        throw new UnsupportedOperationException("not supported yet");
    }

    @Override
    public byte[] packageKeys(final Key[] keys) throws GeneralSecurityException, IOException {
        throw new UnsupportedOperationException("not supported yet");
    }
}
