package net.suberic.crypto.bouncycastlepgp;

import java.util.List;

import javax.mail.internet.InternetAddress;

import org.bouncycastle.bcpg.PublicKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;

import net.suberic.crypto.EncryptionKey;
import net.suberic.crypto.EncryptionManager;
import net.suberic.crypto.EncryptionUtils;

public class BCPGPEncryptionKey implements EncryptionKey {
    PGPSecretKey  secKey;
    PGPPrivateKey priKey;
    PGPPublicKey  pubKey;
    // Only not null is pubKey or secKey does not contain any userIds
    List          userIds;

    public BCPGPEncryptionKey(final PGPPublicKey pubKey) {
        this.pubKey = pubKey;
    }

    public BCPGPEncryptionKey(final PGPSecretKey secKey) {
        this.secKey = secKey;
    }

    @Override
    public void setPassphrase(final char[] passphrase) {
        if (this.secKey != null) {
            try {
                this.priKey = this.secKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
                        .setProvider(PGPUtil.getDefaultProvider()).build(passphrase));
                this.pubKey = this.secKey.getPublicKey();
            } catch (final PGPException e) {
                ;
            }
        }
    }

    public void setUserIds(final List userIds) {
        this.userIds = userIds;
    }

    public PGPPublicKey getPublicKey() {
        return this.pubKey != null ? this.pubKey : this.secKey.getPublicKey();
    }

    public PGPSecretKey getSecretKey() {
        return this.secKey;
    }

    /**
     * Uses first {@link #setPassphrase(char[])} to set the passphrase
     * 
     * @return
     */
    public PGPPrivateKey getPrivateKey() {
        return this.priKey;
    }

    @Override
    public String getAlgorithm() {
        return null;
    }

    @Override
    public String getFormat() {
        return null;
    }

    @Override
    public byte[] getEncoded() {
        return null;
    }

    /**
     * Returns the email address(es) associated with this key.
     */
    @Override
    public String[] getAssociatedAddresses() {
        final java.util.ArrayList addressList = new java.util.ArrayList();

        java.util.Iterator it;
        if (this.userIds != null) {
            it = this.userIds.iterator();
        } else {
            it = (this.secKey != null) ? this.secKey.getUserIDs() : this.pubKey.getUserIDs();
        }

        if (it.hasNext()) {
            try {
                final String str = (String) it.next();
                if (str != null) {
                    final int idx = str.indexOf('@');
                    if (idx > 0 && idx < str.length() - 2) {
                        final InternetAddress address = new InternetAddress(str);
                        addressList.add(address.getAddress());
                    }
                }
            } catch (final Exception e) {
                // do nothing; go on to next one.
            }
        }

        return (String[]) addressList.toArray(new String[0]);
    }

    /**
     * Returns the EncryptionUtils that created this Key.
     */
    @Override
    public EncryptionUtils getEncryptionUtils() throws java.security.NoSuchProviderException {
        return EncryptionManager.getEncryptionUtils(EncryptionManager.PGP);
    }

    /**
     * Returns the type (S/MIME, PGP) of this Key.
     */
    @Override
    public String getType() {
        return EncryptionManager.PGP;
    }

    /**
     * Returns the display information for this Key.
     */
    @Override
    public String getDisplayAlias() {
        java.util.Iterator it = null;
        if (this.pubKey != null) {
            it = this.pubKey.getUserIDs();
        } else {
            it = this.secKey.getUserIDs();
        }

        if (it.hasNext()) {
            return (String) it.next();
        }

        return null;
    }

    @Override
    public boolean isForEncryption() {
        if (this.pubKey != null) {
            return this.pubKey.isEncryptionKey();
        } else {
            return this.secKey.getPublicKey().isEncryptionKey();
        }
    }

    @Override
    public boolean isForSignature() {
        if (this.pubKey != null) {
            return isSigningKey(this.pubKey);
        } else {
            return this.secKey.isSigningKey();
        }
    }

    public static boolean isSigningKey(final PGPPublicKey key) {
        final int algorithm = key.getAlgorithm();

        return ((algorithm == PublicKeyAlgorithmTags.RSA_GENERAL) || (algorithm == PublicKeyAlgorithmTags.RSA_SIGN)
                || (algorithm == PublicKeyAlgorithmTags.DSA) || (algorithm == PublicKeyAlgorithmTags.ECDSA)
                || (algorithm == PublicKeyAlgorithmTags.ELGAMAL_GENERAL));
    }

}
