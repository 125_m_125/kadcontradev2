package net.suberic.crypto.bouncycastlepgp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;

import net.suberic.crypto.EncryptionKeyManager;

/**
 * 
 * @author Lijun Liao (Lijun.Liao@nds.rub.de)
 *
 */
public class BCPGPKeyManager implements EncryptionKeyManager {
    protected HashMap secretKeys = new HashMap();
    protected HashMap publicKeys = new HashMap();

    @Override
    public boolean containsPrivateKeyAlias(final String alias) throws KeyStoreException {
        return this.secretKeys.containsKey(alias);
    }

    @Override
    public boolean containsPublicKeyAlias(final String alias) throws KeyStoreException {
        return this.publicKeys.containsKey(alias);
    }

    @Override
    public void deletePrivateKeyEntry(final String alias, final char[] password) throws KeyStoreException {
        this.secretKeys.remove(alias);
    }

    @Override
    public void deletePublicKeyEntry(final String alias) throws KeyStoreException {
        this.publicKeys.remove(alias);
    }

    @Override
    public Key getPrivateKey(final String alias, final char[] password)
            throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
        final BCPGPEncryptionKey key = (BCPGPEncryptionKey) this.secretKeys.get(alias);
        if (password != null) {
            key.setPassphrase(password);
        }

        if (key.getPrivateKey() == null) {
            throw new UnrecoverableKeyException("cannot retrieve the private key");
        }

        return key;
    }

    @Override
    public Key getPublicKey(final String alias)
            throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
        return (BCPGPEncryptionKey) this.publicKeys.get(alias);
    }

    @Override
    public void loadPrivateKeystore(final InputStream stream, final char[] password) throws IOException,
            NoSuchAlgorithmException, KeyStoreException, NoSuchProviderException, CertificateException {
        if (stream == null) {
            throw new IOException("input stream cannot be null");
        }

        // try {
        PGPSecretKeyRingCollection sc;
        try {
            sc = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(stream), new JcaKeyFingerprintCalculator());
        } catch (final PGPException e) {
            throw new IOException("PGPException: " + e.getMessage());
        }

        final Iterator it = sc.getKeyRings();
        while (it.hasNext()) {
            final PGPSecretKeyRing keyRing = (PGPSecretKeyRing) it.next();
            final Iterator it2 = keyRing.getSecretKeys();

            // If using DSAElGamal PGP Key, the encryption key is not bound with
            // user IDs,
            // we must specify it explicitly. Genarally the first public key is
            // for signature
            // and is associated with user IDs
            List userIds = null;

            while (it2.hasNext()) {
                final PGPSecretKey key = (PGPSecretKey) it2.next();
                if (userIds == null) {
                    final Iterator id_uids = key.getUserIDs();
                    while (id_uids.hasNext()) {
                        userIds = new LinkedList();
                        userIds.add(id_uids.next());
                    }
                }

                final BCPGPEncryptionKey _key = new BCPGPEncryptionKey(key);

                if (!key.getUserIDs().hasNext()) {
                    _key.setUserIds(userIds);
                }

                String alias = "0X" + Long.toHexString(key.getKeyID()).toUpperCase();
                if (userIds != null && userIds.size() > 0) {
                    alias += " " + ((String) userIds.get(0));
                }
                this.secretKeys.put(alias, _key);
            }
        }
    }

    @Override
    public void loadPublicKeystore(final InputStream stream, final char[] password) throws IOException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, NoSuchProviderException {
        if (stream == null) {
            throw new IllegalArgumentException("input stream cannot be null");
        }

        PGPPublicKeyRingCollection pc;

        try {
            pc = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(stream), new JcaKeyFingerprintCalculator());
        } catch (final PGPException e) {
            throw new IOException("PGPException: " + e.getMessage());
        }

        final Iterator it = pc.getKeyRings();
        while (it.hasNext()) {
            final PGPPublicKeyRing keyRing = (PGPPublicKeyRing) it.next();
            final Iterator it2 = keyRing.getPublicKeys();

            // If using DSAElGamal PGP Key, the encryption key is not bound with
            // user IDs,
            // we must specify it explicitly. Genarally the first public key is
            // for signature
            // and is associated with user IDs
            List userIds = null;
            while (it2.hasNext()) {
                final PGPPublicKey key = (PGPPublicKey) it2.next();
                if (userIds == null) {
                    final Iterator id_uids = key.getUserIDs();
                    while (id_uids.hasNext()) {
                        userIds = new LinkedList();
                        userIds.add(id_uids.next());
                    }
                }

                final BCPGPEncryptionKey _key = new BCPGPEncryptionKey(key);
                if (!key.getUserIDs().hasNext()) {
                    _key.setUserIds(userIds);
                }

                String alias = "0X" + Long.toHexString(key.getKeyID()).toUpperCase();
                if (userIds != null && userIds.size() > 0) {
                    alias += " " + ((String) userIds.get(0));
                }
                this.publicKeys.put(alias, _key);
            }
        }
    }

    @Override
    public Set privateKeyAliases(final boolean forSignature) throws KeyStoreException {
        final Set aliases = this.secretKeys.keySet();
        final Iterator it = aliases.iterator();

        final Set retValue = new HashSet();
        while (it.hasNext()) {
            final Object obj = it.next();
            final BCPGPEncryptionKey encKey = (BCPGPEncryptionKey) this.secretKeys.get(obj);
            final PGPSecretKey key = (PGPSecretKey) encKey.getSecretKey();
            if (forSignature) {
                if (key.isSigningKey()) {
                    retValue.add(obj);
                }
            } else {
                if (key.getPublicKey().isEncryptionKey()) {
                    retValue.add(obj);
                }
            }

        }

        return retValue;
    }

    @Override
    public Set publicKeyAliases(final boolean forSignature) throws KeyStoreException {
        final Set aliases = this.publicKeys.keySet();
        final Iterator it = aliases.iterator();

        final Set retValue = new HashSet();
        while (it.hasNext()) {
            final Object obj = it.next();
            final BCPGPEncryptionKey encKey = (BCPGPEncryptionKey) this.publicKeys.get(obj);
            final PGPPublicKey key = encKey.getPublicKey();
            if (forSignature) {
                if (BCPGPEncryptionKey.isSigningKey(key)) {
                    retValue.add(obj);
                }
            } else {
                if (key.isEncryptionKey()) {
                    retValue.add(obj);
                }
            }

        }

        return retValue;
    }

    @Override
    public void setPrivateKeyEntry(final String alias, final Key key, final char[] password) throws KeyStoreException {
        throw new UnsupportedOperationException("setPrivateKeyEntry");
    }

    @Override
    public void setPublicKeyEntry(final String alias, final Key key) throws KeyStoreException {
        throw new UnsupportedOperationException("setPublicKeyEntry");
    }

    @Override
    public int size() throws KeyStoreException {
        throw new UnsupportedOperationException("size");
    }

    @Override
    public void storePrivateKeystore(final OutputStream stream, final char[] password)
            throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        throw new UnsupportedOperationException("storePrivateKeystore");
    }

    @Override
    public void storePublicKeystore(final OutputStream stream, final char[] password)
            throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
        throw new UnsupportedOperationException("storePublicKeystore");
    }

    /**
     * @see net.suberic.crypto.EncryptionKeyManager#privateKeyAliases()
     * @deprecated use {@link #privateKeyAliases(boolean)} instead.
     */
    @Deprecated
    @Override
    public Set privateKeyAliases() throws KeyStoreException {
        final Set aliases = privateKeyAliases(true);
        final Set aliases2 = privateKeyAliases(false);

        final Iterator it = aliases2.iterator();
        while (it.hasNext()) {
            final Object alias = it.next();
            if (!aliases.contains(alias)) {
                aliases.add(alias);
            }
        }
        return aliases;
    }

    /**
     * @see net.suberic.crypto.EncryptionKeyManager#publicKeyAliases()
     * @deprecated use {@link #publicKeyAliases(boolean)} instead
     */
    @Deprecated
    @Override
    public Set publicKeyAliases() throws KeyStoreException {
        final Set aliases = publicKeyAliases(true);
        final Set aliases2 = publicKeyAliases(false);

        final Iterator it = aliases2.iterator();
        while (it.hasNext()) {
            final Object alias = it.next();
            if (!aliases.contains(alias)) {
                aliases.add(alias);
            }
        }
        return aliases;
    }

}
