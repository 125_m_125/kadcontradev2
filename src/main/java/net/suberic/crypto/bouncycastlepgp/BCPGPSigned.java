package net.suberic.crypto.bouncycastlepgp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;

import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.mail.smime.SMIMEException;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;

public class BCPGPSigned {
    Object         message;

    // Either of type byte[] or MimeBodyPart
    Object         content;
    PGPSignature[] signatures;

    private static InputStream getInputStream(final Part bodyPart) throws MessagingException {
        try {
            if (bodyPart.isMimeType("multipart/signed")) {
                throw new MessagingException(
                        "attempt to create signed data object from multipart content - use MimeMultipart constructor.");
            }

            return bodyPart.getInputStream();
        } catch (final IOException e) {
            throw new MessagingException("can't extract input stream: " + e);
        }
    }

    /**
     * base constructor using a defaultContentTransferEncoding of 7bit
     *
     * @exception MessagingException
     *                on an error extracting the signature or otherwise
     *                processing the message.
     * @exception CMSException
     *                if some other problem occurs.
     */
    public BCPGPSigned(final MimeMultipart message) throws MessagingException, PGPException, IOException {
        // super(new CMSProcessableBodyPartInbound(message.getBodyPart(0)),
        // getInputStream(message.getBodyPart(1)));
        this.content = message.getBodyPart(0);
        this.message = message;

        final BodyPart sigPart = message.getBodyPart(1);

        final ArmoredInputStream aIn = new ArmoredInputStream(getInputStream(sigPart));
        final PGPObjectFactory pgpFact = new PGPObjectFactory(aIn, new JcaKeyFingerprintCalculator());
        final PGPSignatureList p3 = (PGPSignatureList) pgpFact.nextObject();
        this.signatures = new PGPSignature[p3.size()];
        for (int i = 0; i < this.signatures.length; i++) {
            this.signatures[i] = p3.get(i);
        }
    }

    /**
     * base constructor for a signed message with encapsulated content.
     *
     * @exception MessagingException
     *                on an error extracting the signature or otherwise
     *                processing the message.
     * @exception SMIMEException
     *                if the body part encapsulated in the message cannot be
     *                extracted.
     * @exception CMSException
     *                if some other problem occurs.
     */
    public BCPGPSigned(final Part message) throws MessagingException, PGPException, IOException {
        this.message = message;

        // InputStream is = PGPUtil.getDecoderStream();
        final ArmoredInputStream aIn = new ArmoredInputStream(getInputStream(message));

        //
        // read the input, making sure we ingore the last newline.
        //
        int ch;
        boolean newLine = false;
        final ByteArrayOutputStream bOut = new ByteArrayOutputStream();

        // while ((ch = aIn.read()) >= 0 && aIn.isClearText())
        while ((ch = aIn.read()) >= 0 && aIn.isClearText()) {
            if (newLine) {
                bOut.write((byte) '\n');
                newLine = false;
            }
            if (ch == '\n') {
                newLine = true;
                continue;
            }

            bOut.write((byte) ch);
        }

        this.content = bOut.toByteArray();

        final PGPObjectFactory pgpFact = new PGPObjectFactory(aIn, new JcaKeyFingerprintCalculator());
        final PGPSignatureList p3 = (PGPSignatureList) pgpFact.nextObject();
        this.signatures = new PGPSignature[p3.size()];
        for (int i = 0; i < this.signatures.length; i++) {
            this.signatures[i] = p3.get(i);
        }
    }

    public PGPSignature[] getSigners() {
        return this.signatures;
    }

    /**
     * return the content that was signed.
     */
    public Object getContent() {
        return this.content;
    }

    /**
     * Return the content that was signed as a mime message.
     *
     * @param session
     * @return a MimeMessage holding the content.
     * @throws MessagingException
     */
    public MimeMessage getContentAsMimeMessage(final Session session) throws MessagingException, IOException {
        byte[] contentBytes = null;

        if (this.content instanceof byte[]) {
            contentBytes = (byte[]) this.content;
        } else if (this.content instanceof MimePart) {
            final MimePart part = (MimePart) this.content;
            ByteArrayOutputStream out;

            if (part.getSize() > 0) {
                out = new ByteArrayOutputStream(part.getSize());
            } else {
                out = new ByteArrayOutputStream();
            }

            part.writeTo(out);
            contentBytes = out.toByteArray();
        } else {
            String type = "<null>";
            if (this.content != null) {
                type = this.content.getClass().getName();
            }

            throw new MessagingException("Could not transfrom content of type " + type + " into MimeMessage.");
        }

        if (contentBytes != null) {
            final ByteArrayInputStream in = new ByteArrayInputStream(contentBytes);

            return new MimeMessage(session, in);
        }

        return null;
    }

    /**
     * return the content that was signed - depending on whether this was
     * unencapsulated or not it will return a MimeMultipart or a MimeBodyPart
     */
    public Object getContentWithSignature() {
        return this.message;
    }
}
