<%@ page language="java" isErrorPage="true"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Verboten!</title>
<jsp:include page="/c/chead.jsp" />
</head>
<body>
	<jsp:include page="/c/top2.jsp" />
	<div id="rcontent" class="col-md-12">
		<div class="alert alert-danger text-center">
			Der derzeit authentifizierte Benutzer besitzt nicht die notwendigen
			Rechte diese Aktion durchzuf&uuml;hren (403)<br><c:out value="${requestScope['javax.servlet.error.message']}"/></div>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>