<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - 2FA</title>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bowser/1.9.3/bowser.min.js"
	defer></script>
<script type="text/javascript"
	src="static/v/${jsVersions['u2f-api.min.js']}/lib/u2f-api.min.js" defer></script>
<script type="text/javascript"
	src="static/v/${jsVersions['webauthn.js']}/lib/webauthn.js" defer></script>
<jsp:include page="/c/chead.jsp" />
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	<c:if test="${auths.size() eq 1}">
		<div class=" col-md-12 alert alert-warning alert-dismissable">
			Mit diesem Account ist nur eine 2FA-Methode aktiv. Wir empfehlen
			dringend mindestens zwei voneinander unabhängige Methoden zu
			aktivieren.</div>
	</c:if>
	<div id="rcontent" class="col-md-12">
		<div class="row">
			<form id='2FAForm' method="POST" class="col-md-12">
				<fieldset>
					<legend>Zwei-Faktor-Authentifizierung</legend>
					<input type="hidden" name="csrfPreventionSalt"
						value="${csrfPreventionSalt}" />
					<c:set var="previous" scope="request" value="${false}" />
					<c:if test="${not empty auths['GA']}">
						<div class="form-group row">
							<label for="gaAuth"><c:if test="${previous}">Oder gib</c:if>
								<c:if test="${!previous}">Gib</c:if> hier dein TOPT(Google
								Authenticator, Authy, ...)-Token ein.</label> <input type="number"
								step="1" min="1" max="999999" class="form-control" id="gaAuth"
								name="gaAuth" placeholder="Token" />
						</div>
						<c:set var="previous" scope="request" value="${true}" />
					</c:if>
					<c:if test="${not empty auths['MA']}">
						<div class="form-group row">
							<label for="maAuth"> <c:if test="${previous}">Oder gib</c:if>
								<c:if test="${!previous}">Gib</c:if> hier den Code ein, der dir
								gerade per Mail zugesendet wurde.
							</label> <input type="number" step="1" min="1" max="999999"
								class="form-control" id="maAuth" name="maAuth"
								placeholder="Code" />
						</div>
						<c:set var="previous" scope="request" value="${true}" />
					</c:if>
					<c:if test="${not empty auths['SC']}">
						<div class="form-group row">
							<label for="scAuth"><c:if test="${previous}">Oder gib</c:if>
								<c:if test="${!previous}">Gib</c:if> hier eines deiner
								Einmalpasswörter ein.</label> <input type="number" step="1" min="1"
								max="99999999" class="form-control" id="scAuth" name="scAuth"
								placeholder="Passwort" />
						</div>
						<c:set var="previous" scope="request" value="${true}" />
					</c:if>
					<c:if test="${not empty auths['UF']}">
						<div class="form-group row">
							<label for="ufAuth"><c:if test="${previous}">Oder aktiviere</c:if>
								<c:if test="${!previous}">Aktiviere</c:if> dein U2F-Gerät.</label> <input
								type="hidden" name="ufAuth" id="ufAuth" />
							<div id="no_U2F" class="alert alert-danger collapse">
								U2F wird durch diesen Browser vermutlich nicht unterstützt.
								Siehe <a href="https://caniuse.com/#feat=u2f">https://caniuse.com/#feat=u2f</a>
								für mehr Informationen.
							</div>
							<script type="text/javascript">
								$(window).load(function() {
									var request = JSON.parse('${auths["UF"]}');
									u2f.sign(request.appId, request.challenge, request.signRequests, function(data) {
										if (data.errorCode) {
											switch (data.errorCode) {
											case 4:
												alert("This device is not registered for this account.");
												break;
											default:
												alert("U2F failed with error code: " + data.errorCode);
											}
											return;
										} else {
											$('#ufAuth').val(JSON.stringify(data));
											$('#2FAForm').append($("<input>").attr("type", "hidden").attr("name", "2FaAuth").val("submit"));
											$('#2FAForm').submit();
										}
									});
								})
							</script>
						</div>
						<c:set var="previous" scope="request" value="${true}" />
					</c:if>
					<c:if test="${not empty auths['WA']}">
						<div class="form-group row">
							<label for="waAuth"><c:if test="${previous}">Oder starte </c:if>
								<c:if test="${!previous}">Starte</c:if> die Authentifizierung mit WebAuthn.</label> <input
								type="hidden" name="waAuth" id="waAuth" />
							<div id="no_WAN" class="alert alert-danger collapse">
								WebAuthn wird durch diesen Browser nicht unterstützt.
								Siehe <a href="https://caniuse.com/#feat=webauthn">https://caniuse.com/#feat=webauthn</a>
								für mehr Informationen.
							</div>
							<script type="text/javascript">
								$(window).load(function() {
									if (!detectWebAuthnSupport()) {
										return;
									}
									var makeAssertionOptions = JSON.parse('${auths["WA"]}').publicKeyCredentialRequestOptions;
									makeAssertionOptions.challenge = bufferDecode(makeAssertionOptions.challenge);
				                    makeAssertionOptions.allowCredentials.forEach(function (listItem) {
				                        listItem.id = bufferDecode(listItem.id)
				                    });
				                    navigator.credentials.get({
				                            publicKey: makeAssertionOptions
				                        })
				                        .then(function (credential) {
				                            let authData = new Uint8Array(credential.response.authenticatorData);
				                            let clientDataJSON = new Uint8Array(credential.response.clientDataJSON);
				                            let rawId = new Uint8Array(credential.rawId);
				                            let sig = new Uint8Array(credential.response.signature);
				                            let userHandle = new Uint8Array(credential.response.userHandle);
				                            
				                            $('#waAuth').val(JSON.stringify({
			                                    id: credential.id,
			                                    // rawId: bufferEncode(rawId),
			                                    type: credential.type,
			                                    response: {
			                                        authenticatorData: bufferEncode(authData),
			                                        clientDataJSON: bufferEncode(clientDataJSON),
			                                        signature: bufferEncode(sig),
			                                        userHandle: bufferEncode(userHandle) || null, // u2f returns no userhandle. ensure missing handles are null
			                                    },
			                                    clientExtensionResults: {},
			                                }));
											$('#2FAForm').append($("<input>").attr("type", "hidden").attr("name", "2FaAuth").val("submit"));
											$('#2FAForm').submit();
				                            
				                        }).catch(function (err) {
				                            console.log(err.name);
				                            alert("WebAuthn authentifizierung fehlgeschlagen: "+err.message);
				                        });
								})
							</script>
						</div>
						<c:set var="previous" scope="request" value="${true}" />
					</c:if>
					<c:if test="${not empty auths['CT']}">
						<div class="form-group row">
							<div class="alert alert-danger">
								Die zertifikatsbasierte Authentifizierung ist fehlgeschlagen: <c:out value="${auths['CT']}" />.
							</div>
						</div>
						<c:set var="previous" scope="request" value="${true}" />
					</c:if>
					<div class="form-group row">
						<label> <input type="checkbox" name="cookie"
							value="cookie"> Browser speichern
						</label> <label> <input type="checkbox" name="ip" value="ip">
							IP speichern
						</label>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<input type="submit" class="btn btn-block btn-primary"
								name="2FaAuth" value="Prüfen" />
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>

	<jsp:include page="/c/foot.jsp" />
</body>
</html>