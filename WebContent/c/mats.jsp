<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<div class="col-xs-12 col-sm-4 col-md-3 sidebar-offcanvas">
		<button data-toggle="collapse" data-target="#outercollapse"
			class="visible-xs-block btn btn-primary btn-block">Deine
			Items</button>
		<div id="outercollapse" class="collapse">
			<div id="mats" class="mats"
				<c:if test="${matIntro}">data-intro='Hier werden all deine derzeit eingezahlten Items angezeigt.'</c:if>>
				<h4 class="hidden-xs">Deine Items</h4>
				<div class="row">
					<div class="col-sm-12 col-xs-6 checkbox"
						<c:if test="${matIntro}">data-intro='Mit dieser Checkbox kannst du dir nur die Items anzeigen lassen, die du tatsächlich besitzt.'</c:if>>
						<label><input type="checkbox" id="owned">im Besitz</label>
					</div>
					<div class="col-sm-12 col-xs-6"
						<c:if test="${matIntro}">data-intro='Hier kannst du nach dem Namen oder der ID eines Items suchen.'</c:if>>
						<input type="text" id="matfilter" placeholder="Suchen"
							class="form-control" />
					</div>
				</div>
				<ul class="list-group col-md-12" id="itemContainer">
					<!-- will be filled by script -->
				</ul>
			</div>
		</div>
	</div>
</body>
</html>