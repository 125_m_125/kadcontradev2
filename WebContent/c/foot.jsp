<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</div>
</div>
<footer class="footer">
	<ul id="footer">
		<li><a href="p/impressum">Impressum</a> |</li>
		<li><a href="p/datenschutz">Datenschutz</a> |</li>
		<li><a href="p/agb">AGB</a> |</li>
		<c:choose>
		  <c:when test="${lightTheme}">
			<li><a href="#" onclick="theme(1, event)">Dunkles Layout</a> |</li>
			<li><a href="#" onclick="theme(2, event)">Schwarzes Layout</a> |</li>
		  </c:when>
		  <c:when test="${blackTheme}">
			<li><a href="#" onclick="theme(0, event)">Helles Layout</a> |</li>
			<li><a href="#" onclick="theme(1, event)">Dunkles Layout</a> |</li>
		  </c:when>
		  <c:otherwise>
			<li><a href="#" onclick="theme(0, event)">Helles Layout</a> |</li>
			<li><a href="#" onclick="theme(2, event)">Schwarzes Layout</a> |</li>
		  </c:otherwise>
		</c:choose>
		<li><a href="#" onclick="disableCookies(event)">Cookies ablehnen</a> |</li>
		<li>&copy;2018 LEGEND-BANK</li>
	</ul>
	<div style="text-align: center">
		NOT AN OFFICIAL MINECRAFT PRODUCT. NOT APPROVED BY OR ASSOCIATED WITH
		MOJANG. <br> KEIN OFFIZIELLES KADCON PRODUKT. BEI PROBLEMEN BITTE
		AN DAS <a href="https://forum.kadcon.de/ConversationAdd/?userID=5445" target="_blank">${applicationScope.appName}-TEAM</a> UND
		NICHT AN DAS KADCON-TEAM WENDEN
		<p class="pull-right">Build-Nr. ${version}</p>
	</div>
</footer>