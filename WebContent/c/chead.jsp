<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<base href="${pageContext.request.contextPath}/">


<link rel="apple-touch-icon" sizes="144x144"
	href="/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png"
	sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-16x16.png"
	sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">

<script type="text/javascript">
	var versionWithHash = "v/${hash}/";
	var version = "${version}";
	window.addEventListener("load", function() {
		window.cookieconsent.initialise({
			"palette" : {
				"popup" : {
					"background" : "#000"
				},
				"button" : {
					"background" : "#f1d600"
				}
			},
			"cookie": {
				"name": "cookieconsent_dismissed",
			},
			"theme" : "classic",
			"position" : "bottom-right",
			"content" : {
				"message" : "Diese Webseite verwendet für einige Funktionen Cookies oder den LocalStorage. Solange dies nicht akzeptiert wird, stehen einige Funktionen dieser Website möglicherweise nicht zur Verfügung. Die Einverständnis kann durch einen Click auf \"Cookies ablehnen\" jederzeit widerrufen werden.",
				"dismiss" : "Cookies akzeptieren",
				"link" : "Weitere Informationen",
				"href" : "p/datenschutz"
			},
			"onStatusChange" : function(status) {
				if (this.hasConsented()) {
					$(".cookieWaiting").prop("disabled", false);
					$(".hideWithCookies").hide();
					$(".cookieWaiting").prop("title", null);
				}
			},
		});
	});
</script>

<!-- chead css -->
<!-- select2 -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.7.0/introjs.min.css" />
<link rel="stylesheet" type="text/css"
	href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />

<!-- mycss -->
<link rel="stylesheet" type="text/css"
	href="static/v/${hash}/css/kt.min.css" />
<c:choose>
	<c:when test="${lightTheme}">
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css"
			href="static/v/${jsVersions['light/main.min.css']}/css2/light/main.min.css">
		<link rel="stylesheet" type="text/css"
			href="static/v/${jsVersions['light/graphs.min.css']}/css2/light/graphs.min.css">
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" type="text/css"
			href="static/v/${jsVersions['dark/bootstrap.min.css']}/css2/dark/bootstrap.min.css">
		<link rel="stylesheet" type="text/css"
			href="static/v/${jsVersions['dark/main.min.css']}/css2/dark/main.min.css">
		<link rel="stylesheet" type="text/css"
			href="static/v/${jsVersions['dark/graphs.min.css']}/css2/dark/graphs.min.css">
		<c:if test="${blackTheme}">
			<style>
body, .panel, .list-group-item, table {
	background-color: black;
}
</style>
		</c:if>
	</c:otherwise>
</c:choose>

<!-- chead js -->
${additionalScripts}
<!-- Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"
	async="async"></script>
<!-- select2 -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"
	defer></script>
<!-- jquery -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- bootstrap -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"
	defer></script>
<!-- d3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js"
	defer></script>
<!-- matchHeight -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
<!-- es6-primise -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"
	defer></script>
<!-- myjs -->
<script src="static/v/${jsVersions['jquery.initialize.min.js']}/lib/jquery.initialize.min.js" defer></script>
<script src="static/v/${hash}/js/kt.min.js" defer></script>
<!-- intro -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.7.0/intro.min.js"
	defer></script>
	