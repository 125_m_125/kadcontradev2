<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/c/cdata.jsp" />
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a href="" class="navbar-brand">${applicationScope.appName}</a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href=""> Home</a></li>
				<li><a href="markt">Markt</a></li>
				<li><a href="stats">Statistiken</a></li>
				<li><a href="auszahlung">Auszahlung</a></li>
				<c:if test="${sessionScope.admin}">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Administration<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="admin/payouts">Auszahlungen</a></li>
							<li><a href="admin/pobox">Auszahlungsboxen</a></li>
							<li><a href="admin/users">Spielereinsicht</a></li>
							<li><a href="admin/coupon">Couponerstellug</a></li>
							<li><a href="admin/log">Logeinsicht</a></li>
							<li><a href="admin/maintainance">Wartungsmodus</a></li>
							<li><a href="admin/mysqlexporter">Exporter</a></li>
						</ul></li>
				</c:if>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"
					onclick="introJs().start().onbeforechange(onNextIntorjsStep);return false"><span
						class="glyphicon glyphicon-question-sign"></span></a></li>
				<c:if test="${id==null || id==-1}">
					<li>
						<form method="post" class="navbar-form" action="read">
							<input type="hidden" name="csrfPreventionSalt"
								value="${csrfPreventionSalt}" />
							<button type="submit" name="account" class="btn btn-primary"
								value="Kontoauszug auslesen" data-intro='Mit diesem Button kann das Auslesen des Kontoauszuges aktiviert werden. Dadurch werden alle Einzahlungen, Registrierungen und Passwortänderungen übernommen. Unter Umständen kann es bis zu zwei Minuten dauern, bis eine auf Kadcon ausgeführte Aktion im Kontoauszug auftaucht. Sollte eine Änderung mit dem ersten Auslesen nicht übernommen worden sein, sollte erst ein weiterer Ausleseversuch unternommen werden. Wenn sie dann immer noch nicht übernommen wurde, sollte das Team von ${applicationScope.appName} möglichst zeitnah informiert werden.'>
								<i class="glyphicon glyphicon-cloud-download gbtn" ></i>Kontoauszug
								auslesen
							</button>
						</form>
					</li>
				</c:if>
				<c:if test="${id!=null && id!=-1}">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span> 
							<strong>${sessionScope.name}</strong> <span
							class="glyphicon glyphicon-chevron-down"></span>
					</a>
						<ul class="dropdown-menu">
							<li>
								<div class="navbar-login">
									<div class="row">
										<div class="col-lg-12">
											<form method="post" action="read">
												<input type="hidden" name="csrfPreventionSalt"
													value="${csrfPreventionSalt}" />
												<button type="submit" name="account"
													class="btn btn-primary btn-block"
													value="Kontoauszug auslesen">
													<i class="glyphicon glyphicon-cloud-download gbtn"></i>Kontoauszug
													auslesen
												</button>
											</form>
										</div>
									</div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="navbar-login">
									<div class="row">
										<div class="col-lg-12">
											<a href="einstellungen"
												class="btn btn-primary btn-block bottomFlat"><i
												class="glyphicon glyphicon-cog gbtn"></i>Einstellungen</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="navbar-login">
									<div class="row">
										<div class="col-lg-12">
											<form method="post" action="logout">
												<input type="hidden" name="csrfPreventionSalt"
													value="${csrfPreventionSalt}" />
												<button type="submit"
													class="btn btn-danger btn-block topFlat" name="logout"
													value="Logout">
													<i class="glyphicon glyphicon-log-out gbtn"></i> Logout
												</button>
											</form>
										</div>
									</div>
								</div>
							</li>
						</ul></li>
				</c:if>
			</ul>
		</div>
	</div>
</div>
<div id="content" class="container">
	<noscript>
		<div class="alert alert-danger">Javascript ist derzeit
			deaktiviert. Um alle Funktionen von ${applicationScope.appName} nutzen zu können,
			muss Javascript aktiviert sein.</div>
	</noscript>
	<c:if test="${not empty sessionScope.error}">
		<div class="alert alert-danger">${sessionScope.error}</div>
		<c:remove var="error" scope="session" />
	</c:if>
	<c:if test="${not empty requestScope.error}">
		<div class="alert alert-danger"><c:out value="${requestScope.error}" /></div>
	</c:if>
	<%-- <c:if test="${not empty param.error}">
		<div class="alert alert-danger"><c:out value="${param.error}" /></div>
	</c:if> --%>

	<c:if test="${not empty sessionScope.info}">
		<div class="alert alert-info">${sessionScope.info}</div>
		<c:remove var="info" scope="session" />
	</c:if>
	<c:if test="${not empty requestScope.info}">
		<div class="alert alert-info"><c:out value="${requestScope.info}" /></div>
	</c:if>
	<%-- <c:if test="${not empty param.info}">
		<div class="alert alert-info"><c:out value="${param.info}" /></div>
	</c:if> --%>
	<div class="row row-offcanvas row-offcanvas-left">