/**
 * 
 */
$(document).ready(function() {
	var lastRequestedDate;
	var tbl;
	var currentRes;
    if (window.location.href.indexOf("stats") < 0) {
        return;
    }

    function setItem(res) {
        var parseDate = d3.timeParse("%Y-%m-%d");
        localStorage.setItem("lastStatsItem", res);
        $("#line").empty();
        $("#table").empty();
        $("#orderBody").empty();
        $("#executedOrdersTable").empty();
        $("#graphs").find(".alert").remove();
        lastRequestedDate = new Date();
        lastRequestedDate.setHours(0,0,0,0);
        lastRequestedDate.setDate(lastRequestedDate.getDate() + 1);
        currentRes = res;
        if (res === "-1") {
            $("#orderbook").hide();
            $("#statsTitle").text("Statistiken für den Ressourcenindex");
        } else {
            $("#orderbook").show();
            if (window.itemlist)
                window.itemlist.then(function(itemlist) {
                    $("#statsTitle").text("Statistiken für " + itemlist.find(function(entry) {
                        return entry.id === res
                    }).name);
                });
        }
        d3.tsv("api/v2.0/history/" + res, function(error, data) {
            if (!data.length) {
                $("#graphs").append("<div class='alert alert-warning'>F&uumlr dieses Item stehen noch keine Statistiken zur Verf&uuml;gung</div>");
                return;
            }

            // line
            var linedata = [
                ['x'],
                ['value'],
                ['volume'],
                ['low'],
                ['high']
            ];
            data.forEach(function(entry) {
                linedata[0].push(parseDate(entry.date));
                linedata[1].push(entry.close);
                linedata[2].push(entry["unitVolume"]);
                linedata[3].push(entry.low || null);
                linedata[4].push(entry.high || null);
            });
            var chart = c3.generate({
                bindto: '#line',
                width: '100%',
                data: {
                    x: 'x',
                    columns: linedata,
                    names: {
                        value: 'Wert',
                        volume: '# gehandelter Items',
                        low: "Niedrigster Handelswert",
                        high: "Höchster Handelswert",
                    },
                    axes: {
                        value: 'y2',
                        volume: 'y',
                        low: 'y2',
                        high: 'y2',
                    },
                    types: {
                        value: 'area',
                        volume: 'bar',
                        low: 'area',
                        high: 'area',
                    }
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {
                            format: '%d.%m'
                        }
                    },
                    y: {
                        label: {
                            text: '# gehandelter Items',
                            position: 'inner-middle',
                        },
                        padding: {
                            top: 360
                        }
                    },
                    y2: {
                        show: true,
                        label: {
                            text: 'Wert',
                            position: 'inner-middle',
                        }
                    }
                }
            });
            chart.hide(['low', 'high']);
            // table
            if (res === "-1") {
                var keys = ["date", "open", "close", "unitVolume", "dollarVolume"];
                var names = ["Datum", "Startwert", "Endwert", "Volumen(#)", "Volumen(K)"];
            } else {
                var keys = ["date", "open", "close", "high", "low", "unitVolume", "dollarVolume"];
                var names = ["Datum", "Startwert", "Endwert", "Höchster Handelswert", "Niedrigster Handelswert", "Volumen(#)", "Volumen(K)"];
            }
            var table = $('<table></table>').addClass('table table-responsive table-bordered');
            var th = names.reduce(function(old, current) {
                return old + "<th>" + current + "</th>";
            }, "");
            table.append($('<thead>').append($('<tr></tr>').html(th)));

            data.forEach(function(entry) {
                var rowContent = "";
                keys.forEach(function(key) {
                    if (entry[key]) {
                        rowContent += "<td>" + entry[key] + "</td>";
                    } else {
                        rowContent += "<td>&zwnj;</td>";
                    }
                });
                var row = $('<tr></tr>').html(rowContent);
                table.append(row);
            });
            $('#table').append(table);

            var value = data[0].close;
            var change = data[0].open ? ((data[0].close - data[0].open) / data[0].open * 100).toFixed(4) : "n/a";
            var change30 = (data[29] && data[29].open) ? ((data[0].close - data[29].open) / data[29].open * 100).toFixed(4) : "n/a";
            var tradeCount = data[0]["unitVolume"];
            $("#value").val(value);
            $("#d1").val((change > 0 ? "+" : "") + change + "%");
            $("#d30").val((change30 > 0 ? "+" : "") + change30 + "%");
            $("#amount").val(tradeCount);
        });

        if (res !== "-1") {
            d3.tsv("api/v2.0/orderbook/" + res, function(error, data) {
                data.forEach(function(entry) {
                    var rowContent = "<td>" + entry.type + "</td>";
                    rowContent += "<td>" + entry.price + "</td>";
                    rowContent += "<td>" + entry.amount + "</td>";
                    var invType = entry.type === "buy" ? "sell" : "buy";
                    if (entry.price.startsWith("<") || entry.price.startsWith(">")) {
                        rowContent += "<td></td>"
                    } else {
                        rowContent += "<td><a href=\"markt?bs=" + invType + "&item=" + res + "&price=" + parseFloat(entry.price).toFixed(2) + "&count=" + entry.amount + "\">" +
                            (invType === "buy" ? "kaufen" : "verkaufen") + "</a></td>";
                    }
                    var row = $("<tr class=\"" + entry.type + "\"></tr>").append(rowContent);
                    $("#orderBody").append(row);
                });
            });
        }
        tbl = $('<table>').addClass('table table-responsive table-bordered');
    	var head = $('<tr></tr>').append($("<th>").text("Zeit"));
    	if (res=="-1")
    		head.append($("<th>").text("Material"))
		head.append($("<th>").text("Menge")).append($("<th>").text("Stückpreis"));
    	tbl.append($('<thead>').append(head));
        $("#executedOrdersTable").append(tbl);
        $("#executedOrdersTable").append('<a href="#" id="requestMore">Nächsten Tag laden...</a>');
        $("#requestMore").on("click", requestMoreItems)
        requestMoreItems();
        
        var lastSelected = sessionStorage.getItem("statsGraphSelection");
        if (lastSelected) {
            $("#" + lastSelected).click();
        } else {
            $("#lineSelector").click();
        }

        $("#selection a").on("click", function() {
            sessionStorage.setItem("statsGraphSelection", $(this).attr("id"));
        });
    }
    
    function requestMoreItems() {
    	var queryParams = "?";
        if (currentRes!="-1") queryParams += "material="+currentRes+"&";
        queryParams += "end="+lastRequestedDate.getTime()+"&";
        lastRequestedDate.setDate(lastRequestedDate.getDate() - 1);
        queryParams += "start="+lastRequestedDate.getTime()+"&";
        d3.tsv("api/v2.0/historicTrades/executed" +queryParams, function(error, data) {
            data.forEach(function(entry) {
            	console.log(entry,entry.timestamp,entry.material);
                var row = $("<tr>")
                row.append($("<td>").text(new Date(parseInt(entry.timestamp)).toLocaleString()));
            	if (currentRes==="-1")
            		row.append($("<td>").text(entry.material))
                row.append($("<td>").text(entry.amount));
                row.append($("<td>").text(entry.price));
                tbl.append(row);
            });
        });
        return false;
    }
    
    window.setStatsItem = setItem;
    var res = getUrlParameter("res");
    if (!res) {
        res = localStorage.getItem("lastStatsItem");
        if (!res) {
            res = "-1";
        }
    }
    setItem(res);
});
