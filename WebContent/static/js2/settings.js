;
(function () {
	var messaging = false;
	var token = false;
	var active = false;

	function getStatus(t) {
		token = t;
		d3.json("api/v2.0/users/D" + userid + "/webpush/" + t, function (err, data) {
			if (err) {
				if (err.status === 404 || err.currentTarget.status === 404) {
					webPushInactive();
				} else {
					$("#webPushInfo").text("Fehler beim Abfragen des Status.");
				}
				return;
			}
			if (data.itemlist || data.messages || data.orders || data.payouts) {
				webPushActive();
			} else {
				webPushInactive();
			}
		});
	}

	function webPushInactive() {
		active = false;
		$("#webPushInfo").text("Web Push Benachrichtigungen sind nicht aktiv.");
		$("#webPushButton").attr("value", "Web Push Benachrichtigungen aktivieren").attr("onClick", "activateWP()");
	}

	function webPushActive() {
		active = true;
		$("#webPushInfo").text("Web Push Benachrichtigungen sind aktiviert.");
		$("#webPushButton").attr("value", "Web Push Benachrichtigungen deaktivieren").attr("onClick", "deactivateWP()");
	}

	window.activateWP = function () {
		if (!active) {
			d3.request("api/v2.0/users/D" + userid + "/webpush/" + token + "?csrfPreventionSalt=" + csrfPreventionSalt).header("Content-Type", "application/x-www-form-urlencoded").post("itemlist=true&orders=true&payouts=true&workers=true", function (err, response) {
				var extra;
				if (err) {
					try {
						extra = "error=Fehler beim aktivieren von Web Push Benachrichtigungen: " + JSON.parse(err.currentTarget.response).humanReadableMessage;
					} catch (e) {
						extra = "error=Unbekannter Fehler beim aktivieren von Web Push Benachrichtigungen";
					}
				} else {
					extra = "info=Web Push Benachrichtungen erfolgreich aktiviert";
				}
				var url = window.location.href;
				if (url.indexOf('?') > -1) {
					url += '&'
				} else {
					url += '?'
				}
				window.location.href = url + extra;
			});
		}
	}

	window.deactivateWP = function () {
		if (active) {
			d3.request("api/v2.0/users/D" + userid + "/webpush/" + token + "?csrfPreventionSalt=" + csrfPreventionSalt).send("delete", "", function (err, response) {
				var extra;
				if (err) {
					try {
						extra = "error=Fehler beim deaktivieren von Web Push Benachrichtigungen: " + JSON.parse(err.currentTarget.response).humanReadableMessage;
					} catch (e) {
						extra = "error=Unbekannter Fehler beim deaktivieren von Web Push Benachrichtigungen";
					}
				} else {
					extra = "info=Web Push Benachrichtungen erfolgreich deaktiviert";
				}
				var url = window.location.href;
				if (url.indexOf('?') > -1) {
					url += '&'
				} else {
					url += '?'
				}
				window.location.href = url + extra;
			});
		}
	}

	function initFirebase() {
		// Initialize Firebase
		var config = {
			apiKey: "AIzaSyBjsBDaOIil3mCgKa6IgiRC_YHBFyUIT6w",
			authDomain: "kadcontradre.firebaseapp.com",
			databaseURL: "https://kadcontradre.firebaseio.com",
			projectId: "kadcontradre",
			storageBucket: "kadcontradre.appspot.com",
			messagingSenderId: "1006958123666"
		};
		firebase.initializeApp(config);

		messaging = firebase.messaging();
		messaging.usePublicVapidKey("BAtSFfCG83CrakhVhurQdQ839kszyLbcnKE3SKbl1SjAwBE3hJ1wwhX-bAlvL_1zsQCo_sYWHjNbRH1NPTMkB5A");

		messaging.requestPermission().then(function () {
			console.log('Notification permission granted.');
			// Get Instance ID token. Initially this makes a network call, once
			// retrieved
			// subsequent calls to getToken will return from cache.
			messaging.getToken().then(function (currentToken) {
				if (currentToken) {
					getStatus(currentToken);
				} else {
					console.log('No Instance ID token available. Request permission to generate one.');
				}
			}).catch(function (err) {
				console.log('An error occurred while retrieving token. ', err);
				showToken('Error retrieving Instance ID token. ', err);
			});
			// Callback fired if Instance ID token is updated.
			messaging.onTokenRefresh(function () {
				messaging.getToken().then(function (refreshedToken) {
					console.log('Token refreshed.');
					getStatus(refreshedToken);
				}).catch(function (err) {
					console.log('Unable to retrieve refreshed token ', err);
					showToken('Unable to retrieve refreshed token ', err);
				});
			});
		}).catch(function (err) {
			console.log("hi");
			$("#webPushInfo").text("Web Push Benachrichtigungen sind in diesem Browser nicht erlaubt. Erlaube Notifications/Benachrichtigungen um Web Push Benachrichtigungen erhalten zu können.");
			$("#webPushButton").prop("disabled", true);;
			console.log('Unable to get permission to notify.', err);
		});
	}

	window.toggleWP = toggleWP = function () {
		if (!messaging) {
			initFirebase();
		}
	}

	$(document).ready(function () {
		if (!('serviceWorker' in navigator && 'PushManager' in window)) {
			$("#webPushError").show();
		}
	});
}());

function requestTOTP() {
	$.post(window.location.href, $("#TOTP1").serialize(), function (data, textStatus, request) {
		$(".csrfPreventionSalt").val(request.getResponseHeader('csrfPreventionSalt'));
		if (data.qrUrl) {
			$("#TOTP1").hide();
			$("#TOTP2").show();
			$("#TOTPqr").empty();
			$("#TOTPqr").qrcode(data.qrUrl);
			$("#TOTPqr").append($("<p>").text("Secret: " + data.secret));
		}
	});
}
function confirmTOTP() {
	$.post(window.location.href, $("#TOTP2").serialize(), function (data, textStatus, request) {
		$(".csrfPreventionSalt").val(request.getResponseHeader('csrfPreventionSalt'));
		if (data.success) {
			location.reload();
		} else {
			console.log(data);
			$("#TOTPError").text(data.message);
			$("#TOTPError").show();
			setTimeout($("#TOTPError").hide, 2000);
		}
	});
}

function requestMail() {
	$.post(window.location.href, $("#Mail1").serialize(), function (data, textStatus, request) {
		$(".csrfPreventionSalt").val(request.getResponseHeader('csrfPreventionSalt'));
		if (data.mail) {
			$("#Mail1").hide();
			$("#Mail2").show();
		}
	});
}
function confirmMail() {
	$.post(window.location.href, $("#Mail2").serialize(), function (data, textStatus, request) {
		$(".csrfPreventionSalt").val(request.getResponseHeader('csrfPreventionSalt'));
		if (data.success) {
			location.reload();
		} else {
			console.log(data);
			$("#MailError").text(data.message);
			$("#MailError").collapse("show");
			setTimeout(function () {
				$("#MailError").collapse("hide")
			}, 2000);
		}
	});
}

function requestOTP() {
	$.post(window.location.href, $("#OTP").serialize(), function (data, textStatus, request) {
		$("#sendOTPrequest").prop("disabled", "disabled");
		$(".csrfPreventionSalt").val(request.getResponseHeader('csrfPreventionSalt'));
		if (data) {
			data = JSON.parse(data);
			console.log(data, typeof data);
			var text = "";
			for (var i = 0; i < data.length; i++) {
				text += data[i] + "\r\n";
			}
			;
			$("#OTPfield").text(text);
		} else {
			$("#OTPfield").text("Anfrage fehlgeschlagen. Bitte versuche es später noch einmal.");
			$("#sendOTPrequest").prop("disabled", false);
		}
	});
}
function requestU2F() {
	$.post(window.location.href, $("#U2F").serialize(), function (data, textStatus, request) {
		$(".csrfPreventionSalt").val(request.getResponseHeader('csrfPreventionSalt'));
		$("#U2Finfo").show();
		if (!data.success) {
			$("#U2Finfo").text("Anfrage fehlgeschlagen.");
			return;
		}
		$("#U2Finfo").text("Aktiviere nun das U2F-Gerät");
		u2f.register(data.registrationData.appId, data.registrationData.registerRequests, data.registrationData.registeredKeys, function (data) {
			console.log(data);
			if (data.errorCode) {
				switch (data.errorCode) {
					case 4:
						alert$("#U2Finfo").text("Dieses Gerät ist bereits registriert.");
						break;
					default:
						$("#U2Finfo").text("U2F failed with error: " + data.errorCode);
				}
			} else {
				$("#U2Fresponse").val(JSON.stringify(data));
				$.post(window.location.href, $("#U2F2").serialize(), function (data, textStatus, request) {
					location.reload();
				});
			}
		});
	});
}

function requestWAN() {
	if (!detectWebAuthnSupport()) {
		return;
	}
	$.post(window.location.href, $("#WAN").serialize(), function (data, textStatus, request) {
		$(".csrfPreventionSalt").val(request.getResponseHeader('csrfPreventionSalt'));
		$("#WANinfo").show();
		if (!data.success) {
			$("#WANinfo").text("Anfrage fehlgeschlagen.");
			return;
		}
		$("#WANinfo").text("Aktiviere nun das WAN-Gerät");

		var makeCredentialOptions = data.registrationData;
		
//		makeCredentialOptions.authenticatorSelection = {userVerification: "preferred"};
		makeCredentialOptions.challenge = bufferDecode(makeCredentialOptions.challenge);
        makeCredentialOptions.user.id = bufferDecode(makeCredentialOptions.user.id);
        if (makeCredentialOptions.excludeCredentials) {
            for (var i = 0; i < makeCredentialOptions.excludeCredentials.length; i++) {
                makeCredentialOptions.excludeCredentials[i].id = bufferDecode(makeCredentialOptions.excludeCredentials[i].id);
            }
        }
        navigator.credentials.create({
            publicKey: makeCredentialOptions
        }).then(function (newCredential) {
            console.log("PublicKeyCredential Created");
            console.log(newCredential);
            state.createResponse = newCredential;
            
            let attestationObject = new Uint8Array(newCredential.response.attestationObject);
            let clientDataJSON = new Uint8Array(newCredential.response.clientDataJSON);
            let rawId = new Uint8Array(newCredential.rawId);
            
            $("#WANresponse").val(JSON.stringify({
                id: newCredential.id,
//                rawId: bufferEncode(rawId),
                type: newCredential.type,
                response: {
                    attestationObject: bufferEncode(attestationObject),
                    clientDataJSON: bufferEncode(clientDataJSON),
                },
                clientExtensionResults: {},
            }));
            
            $.post(window.location.href, $("#WAN2").serialize(), function(data,
            		textStatus, request) {
        			location.reload();
    			});
            
        }).catch(function (err) {
			$("#WANinfo").text("Fehler: "+err);
        });
	});
}
// u2f.register(data.registrationData.appId,
// data.registrationData.registerRequests, data.registrationData.registeredKeys,
// function(data) {
// console.log(data);
// if (data.errorCode) {
// switch (data.errorCode) {
// case 4:
// alert$("#WAFinfo").text("Dieses Gerät ist bereits registriert.");
// break;
// default:
// $("#U2Finfo").text("U2F failed with error: " + data.errorCode);
// }
// } else {
// $("#U2Fresponse").val(JSON.stringify(data));
// $.post(window.location.href, $("#U2F2").serialize(), function(data,
// textStatus, request) {
// location.reload();
// });
// }
// });

function requestCertificate(prefix) {
	d3.json("api/v2.0/users/D" + userid + "/certificates/" + prefix).header("Content-Type", "application/x-www-form-urlencoded").post($("#" + prefix + "ClientCert").serialize(), function (err, response) {
		if (err) {
			var extra;
			try {
				extra = "error=Fehler beim Generieren des Zertifikates: " + JSON.parse(err.currentTarget.response).humanReadableMessage;
			} catch (e) {
				extra = "error=Unbekannter Fehler beim Generieren des Zertifikates.";
			}
			var url = window.location.href;
			if (url.indexOf('?') > -1) {
				url += '&'
			} else {
				url += '?'
			}
			window.location.href = url + extra;
			return;
		} else {
			$("#" + prefix + "ClientCertResultCert").text(response.certificate);
			$("#" + prefix + "ClientCertResultInfo").text("Berechtigungen des Zertifikates: " + response.permissions.map(function (n) { return window.translations.permissions[n]; }).join(", "));
			$("#" + prefix + "ClientCertResult").show();
			$("#send" + prefix + "ClientCertrequest").prop("disabled", true);
		}
	});
}

$(document).ready(function () {
	$("#publickey").collapse({
		toggle: false
	});
	$(".mailEncryptionRadio").on("change", function () {
		if (this.value) {
			if (this.value === "NONE") {
				$("#publickey").collapse("hide");
			} else {
				$("#publickey").collapse("show");
			}
		}
	});
	d3.tsv("api/v2.0/users/D" + userid + "/certificates/", function (error, data) {
		if (error) {
			$("#certificatesTableBody").append($("<tr>").append($('<td>').text("Fehler beim laden der Zertifikate")));
			return;
		}
		if (data.length == 0) {
			$("#certificatesTableBody").append($("<tr>").append($('<td>').text("Derzeit existieren keine gültigen Zertifikate für diesen User")));
			return;
		}
		data.forEach(function (entry) {
			var tr = $("<tr>");
			tr.append($('<td>').text(entry.serialNr));
			tr.append($('<td>').text(entry.type));

			var form = $("<form method=\"POST\">");
			var btn = $("<input type=\"button\" class=\"btn btn-primary btn-block\" value=\"Zertifikat widerrufen\">");
			btn.on("click", function () {
				btn.prop("disabled", true);
				d3.request("api/v2.0/users/D" + userid + "/certificates/" + entry.serialNr + "?csrfPreventionSalt=" + csrfPreventionSalt).send("delete", "", function (err, response) {
					var extra;
					if (err) {
						try {
							extra = "error=Fehler beim Wirderrufen des Zertifikates: " + JSON.parse(err.currentTarget.response).humanReadableMessage;
						} catch (e) {
							extra = "error=Unbekannter Fehler beim Widerrufen des Zertifikates";
						}
					} else {
						extra = "info=Zertifikat erfolgreich widerrufen.";
					}
					var url = window.location.href;
					if (url.indexOf('?') > -1) {
						url += '&'
					} else {
						url += '?'
					}
					window.location.href = url + extra;
				});
			});
			form.append(btn);
			tr.append($("<td>").append(form));
			$("#certificatesTableBody").append(tr);
		});
	});
});