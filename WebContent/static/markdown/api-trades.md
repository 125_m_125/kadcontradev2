# Orders
## Order eines Benutzers abrufen
|-------------------|---------------|
|URL                |/api/trades    |
|Methode            |GET            |
|Authentifizierung  |true           |
|Rückgabeformat     |csv            |

### Parameter
|-------------------|--------------------------------|
|uid                |Die id des angefragten Benutzers|

### Spalten der Rückgabe
|-------------------|-----------------------------------------------------------|
|id					|Die ID der Order											|
|buy				|true bei einer Kauforder, false bei einer Verkaufsorder	|
|materialId			|Die ID des Items											|
|materialName		|Der Name des Items											|
|amount				|Die Anzahl der Items										|
|price				|Der Preis pro Item											|
|sold				|Die Anzahl bisher gehandelter Items						|
|toTakeM			|Die Menge an Geld, die entnommen werden kann				|
|toTakeI			|Die Anzahl der Items, die entnommen werden können			|
|cancelled			|`true`, wenn die Order abgebrochen wurde, ansonsten false	|

### Beispielanfrage
/api/trades?uid=1

### Beispielantwort
    id,buy,materialId,materialName,amount,price,sold,toTakeM,toTakeI,cancelled
    236,true,4,Cobblestone(4),64,0.05,0,0.000000,0,false
    237,false,296,Wheat(296),16,0.25,7,1.749825,0,false

## Eine neue Order erstellen
|-------------------|---------------------------|
|URL                |/api/trades?create=create	|
|Methode            |POST           			|
|Authentifizierung  |true           			|
|Rückgabeformat     |json           			|

### Parameter
|-------------------|---------------------------------------------------|
|bs					|`buy` für eine Kauforder, `sell` für eine Verkaufsorder	|
|item				|Die ID des zu handelnden Items								|
|count				|Die Anzahl der zu handelnden Items							|
|price				|Der Preis pro Item											|

### Inhalt der Rückgabe
|-------------------|---------------------------------------------------|
|success			|`true`, wenn die Ordererstellung erfolgreich war								|
|message			|`creationsuccess` bei Erfolg. Enthält weitere Informationen bei Fehlschlägen	|
|result				|Die erstellte Order, sofern die Erstellung erfolgreich war						|

### Beispielanfrage
/api/trades?create=create&bs=buy&item=4&count=64&price=0.05

### Beispielantwort
    {
      "success":true,
      "message":"creationsuccess",
      "result":{
        "id":239,
        "buy":true,
        "materialid":"4",
        "materialName":"Cobblestone(4)",
        "amount":64,
        "price":0.05,
        "sold":0,
        "toTakeM":0,
        "toTakeI":0,
        "cancelled":false
      }
    }

## Eine Order abbrechen
|-------------------|---------------------------|
|URL                |/api/trades?cancel=cancel	|
|Methode            |POST           			|
|Authentifizierung  |true           			|
|Rückgabeformat     |json           			|

### Parameter
|-------------------|---------------------------------------------------|
|tradeid			|Die ID der abzubrechenden Order					|

### Inhalt der Rückgabe
|-------------------|---------------------------------------------------|
|success			|`true`, wenn der Orderabbruch erfolgreich war										|
|message			|`tradecancelsuccess` bei Erfolg. Enthält weitere Informationen bei Fehlschlägen	|
|result				|Die abgebrochene Order, sofern der Abbruch erfolgreich war							|

### Beispielanfrage
/api/trades?cancel=cancel&tradeid=239

### Beispielantwort
    {  
      "success":true,
      "message":"tradecancelsuccess",
      "result":{  
        "id":239,
        "buy":true,
        "materialid":"4",
        "materialName":"Cobblestone(4)",
        "amount":64,
        "price":0.05,
        "sold":64,
        "toTakeM":3.200000,
        "toTakeI":0,
        "cancelled":true
      }
    }
## Items/Geld aus einer Order entnehmen
|-------------------|---------------------------|
|URL                |/api/trades?takeout=takeout|
|Methode            |POST           			|
|Authentifizierung  |true           			|
|Rückgabeformat     |json           			|

### Parameter
|-------------------|---------------------------------------------------|
|tradeid			|Die ID der Order, aus dem die Waren entnommen werden sollen|

### Inhalt der Rückgabe
|-------------------|---------------------------------------------------|
|success			|`true`, wenn die Entnahme erfolgreich war																			|
|message			|`creationsuccess` bei Erfolg. Enthält weitere Informationen bei Fehlschlägen										|
|result				|Die bearbeitete Order, sofern die Anfrage erfolgreich war und die Order noch nicht vollständig abgeschlossen ist	|

### Beispielanfrage
/api/trades?takeout=takeout&tradeid=239

### Beispielantwort
    {
      "success":true,
      "message":"takeoutsuccess"
    }
    