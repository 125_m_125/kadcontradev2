# Nachrichten
## Nachrichten eines Benutzers abrufen
|-------------------|---------------|
|URL                |/api/messages  |
|Methode            |GET            |
|Authentifizierung  |true           |
|Rückgabeformat     |csv            |

### Parameter
|-------------------|---------------|
|uid                |Die id des angefragten Benutzers|

### Spalten der Rückgabe
|-------------------|-------------------------------------------------------|
|timestamp          |Der Zeitpunkt, an dem diese Nachricht erstellt wurde   |
|message            |Der Inhalt der Nachricht                               |

### Beispielanfrage
/api/messages?uid=1

### Beispielantwort
    timestamp,message
    2016-09-14 15:49:04.0,"Du hast 1 mal Cobblestone(4) f&uuml;r jeweils 0.050000 gekauft"
    2016-09-14 15:45:48.0,"Du hast 1 mal Cobblestone(4) f&uuml;r jeweils 0.050000 gekauft"
    2016-09-06 16:13:37.0,"Dein Passwort wurde erfolgreich geaendert"
    2016-08-12 00:00:00.0,"Dir wurde 0.000037 * Kadis(-1) gutgeschrieben: Refferalprogramm"
    2016-08-11 22:23:17.0,"Du hast 10 mal Stone(1) f&uuml;r jeweils 0.150026 verkauft"
    2016-08-07 20:23:19.0,"Dir wurde 12.340000 * Kadis(-1) gutgeschrieben: Einzahlung"
    2016-08-05 00:00:00.0,"Dir wurde 0.001151 * Kadis(-1) gutgeschrieben: Refferalprogramm"
    2016-08-04 18:37:01.0,"Du hast 10 mal Stone(1) f&uuml;r jeweils 0.155003 gekauft"
    2016-07-31 21:58:59.0,"Dein Account wurde erfolgreich erstellt"