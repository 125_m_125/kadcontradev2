window.translations = {
	"permissions" : {
		"rItems" : "Itemliste auslesen",
		"rMessages" : "Nachrichten auslesen",
		"rOrders" : "Orders auslesen",
		"rPayouts" : "Auszahlungen auslesen",
		"wOrders" : "Orders erstellen/bearbeiten",
		"wPayouts" : "Auszahlungen erstellen/bearbeiten",
		"2fa" : "Zweiter Faktor",
	}
};