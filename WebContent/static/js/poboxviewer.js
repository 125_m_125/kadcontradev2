$(document).ready(function() {
	if ($('#poboxTable').length) {
		var table = $('#poboxTable').DataTable({
			"scrollX" : false,
			"scrollCollapse" : true
		});
		d3.tsv("api/v2.0/admin/poboxes?uid=D" + userid, function(error, data) {
			data.forEach(function(entry) {
				var rowData = [ entry.boxid, entry.server, entry.x + "|" + entry.y + "|" + entry.z, entry.ownername, entry.verified ];

				var form = $("<form method=\"POST\" action=\"admin/pobox\">");
				form.append("<input type=\"hidden\" name=\"csrfPreventionSalt\" value=\"" + csrfPreventionSalt + "\">");
				form.append("<input type=\"hidden\" name=\"boxid\" value=\"" + entry.boxid + "\">");
				if (!entry.ownername || entry.ownername === "null") {
					form.append("<input type=\"submit\" class=\"btn btn-primary btn-block\" name=\"remove\" value=\"Löschen\">");
				} else {
					form.append("<input type=\"submit\" class=\"btn btn-primary btn-block\" name=\"dispossess\" value=\"Enteignen\">");
					if (entry.verified === "false")
						form.append("<input type=\"submit\" class=\"btn btn-primary btn-block\" name=\"verify\" value=\"Bestätigen\">");
				}
				rowData.push($('<div>').append(form).html());
				table.row.add(rowData);
			});
			table.draw();
		});
	}
});