$(document).ready(function() {
	var moreinfo = " Siehe <a href=\"https://caniuse.com/#feat=u2f\" target=\"_blank\">https://caniuse.com/#feat=u2f</a> für mehr Informationen.";
	var notSupported = "Dieser Browser unterstützt kein U2F.";
	var notSupportedConfig = "Dieser Browser unterstützt mit den aktuellen Einstellungen kein U2F.";
	if (typeof u2f === "undefined") {
		if (typeof bowser !== "undefined" && bowser.firefox && bowser.version >= 47) {
			$("#no_U2F").html(notSupportedConfig + moreinfo);
		} else {
			$("#no_U2F").html(notSupported + moreinfo);
		}
		$("#no_U2F").show();
	}
});