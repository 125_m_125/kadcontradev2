function recalculate() {
	var c = parseFloat($("#count").val());
	$("#totalOption").val(c);
	var p = parseFloat($("#price").val().replace(",", "."));
	var a = parseFloat($("#priceAmount").val());
	var unfixed = p / a;
	var perItem = unfixed.toFixed(4);
	$("#total").val((perItem * c).toFixed(4));
	$("#ppi").val(perItem);
	if (unfixed !== parseFloat(perItem)) {
		$("#priceWarning").show();
	} else {
		$("#priceWarning").hide();
	}
}

$(document).ready(function() {
	if ($("#tradeaccordion").length) {
		d3.tsv("api/v2.0/users/D" + userid + "/orders", function(data) {
			awaitItemList(data);
		});
	}

	function awaitItemList(data) {
		if (window.itemlist) {
			window.itemlist.then(function(itemlist) {
				createTrades(data, itemlist);
			});
		} else {
			setTimeout(function() {
				awaitItemList(data);
			}, 50);
		}
	}

	function createTrades(data, itemlist) {
		var processed = [];
		data.forEach(function(entry, index) {
			var entryProcessed = {};
			for ( var k in entry)
				entryProcessed[k] = entry[k];
			entry.buy = entryProcessed.buy = (entry.buy == "true");
			entryProcessed.src = entry;
			entryProcessed.active = true;
			entryProcessed.index = index + 1;
			entryProcessed.buySellText = entry.buy ? "Kauft" : "Verkauft";
			entryProcessed.buySellCommand = entry.buy ? "buy" : "sell";
			entryProcessed.submitValue = "Abbrechen";
			entryProcessed.submitName = "cancel";
			entryProcessed.maxed = entry.amount === entry.sold;
			entryProcessed.percent = entry.sold / entry.amount * 100;
			entryProcessed.imageName = getImageName(entry.materialId);
			entryProcessed.color = entry.cancelled === "true" ? "progress-bar-danger" : (entryProcessed.maxed ? "progress-bar-success" : "progress-bar-warning")
			entryProcessed.csrfPreventionSalt = csrfPreventionSalt;

			processed.push(entryProcessed);
		});

		var last = {};
		last.active = false;
		last.index = data.length + 1;
		last.item = getUrlParameter("item");
		last.amount = getUrlParameter("count");
		last.price = getUrlParameter("price");
		last.buy = getUrlParameter("bs") === "buy";
		last.submitValue = last.buy ? "Kaufen" : "Verkaufen";
		last.submitName = "create";
		itemlist = itemlist.filter(function(current) {
			return current.id !== "-1";
		});
		itemlist.forEach(function(entry) {
			if (entry.id == last.item) {
				entry.selected = true;
			}
		});
		last.itemlist = itemlist;
		last.csrfPreventionSalt = csrfPreventionSalt;

		processed.push(last);

		data.processed = processed;
		$("#tradeaccordion").html(Handlebars.templates.market(data));

		$(".itemList").select2();
		$("#collapse" + last.index).collapse('show');
		$(".buyRadio").on("change", function(event) {
			if ($(this).is(':checked')) {
				console.log($("#submit" + $(this).attr('id').substring(3)));
				$("#submit" + $(this).attr('id').substring(3)).prop("value", "Kaufen");
			}
		})
		$(".sellRadio").on("change", function(event) {
			if ($(this).is(':checked')) {
				$("#submit" + $(this).attr('id').substring(4)).prop("value", "Verkaufen");
			}
		})

		$("#count").on('input', recalculate);
		$("#price").on('input', recalculate);
		$("#priceAmount").on('change', recalculate);
		recalculate()
	}
});
