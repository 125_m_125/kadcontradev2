;
$("document").ready(function() {
	function checkValidity(input) {
		var id = input.id;
		
		//we don't want to check the login password
		if (id==="l_pass")
			return;

		var value = input.value;
		if (id.endsWith("_2")) {
			var other = document.getElementById(id.substring(0,id.length - 2));
			console.log(other.value, value)
			if (other && other.value !== value) {
				input.setCustomValidity("Dieses Passwort entspricht nicht dem oben angegebenen Passwort");
				input.reportValidity();
				return;
			}
			input.setCustomValidity("");
			return;
		}

		var other = document.getElementById(id.substring(0,id.length - 4) + "name");
		if (other && value.indexOf(other.value) !== -1) {
			input.setCustomValidity("Der Benutzername darf nicht im Passwort enthalten sein");
			input.reportValidity();
			return;
		}
		// length
		if (value.length < 7) {
			input.setCustomValidity("Das Passwort muss mindestens 7 Zeichen lang sein");
			input.reportValidity();
			return;
		}
		// charGroups
		if ([ /[0-9]/, /[a-z]/, /[A-Z]/, /[^0-9a-zA-Z]/, ].filter(function(regex) {
			return regex.test(value);
		}).length < 3) {
			input.setCustomValidity("Das Passwort muss jeweils ein Zeichen aus mindestens drei der folgenden Gruppen enthalten: Großbuchstaben, Kleinbuchstaben, Zahlen oder Sonderzeichen");
			input.reportValidity();
			return;
		}
		input.setCustomValidity("");
		return;
	}

	$("input:password").on("input", function() {
		checkValidity(this);
	});
});