;
(function() {
    var itemImageVersions;

    function getImageName(name) {
        if (name === "-1") {
            return "itemimages/coinstack.png";
        }
        if (name.indexOf(":") > 0) {
            parts = name.split(":");
            return "itemimages/" + parts[0] + "-" + parts[1] + ".png";
        }
        return "itemimages/" + name + "-0.png";
    }

    function getImagePath(filename, callback, callbackArg, count) {
        if (!callback || count > 1000) {
            return;
        }
        if (!itemImageVersions) {
            itemImageVersions = new Promise(function(res, rej) {
                $.getJSON("static/json/imageVersions.json", function(data) {
                    res(data);
                });
            });
        }
        itemImageVersions.then(function(itemImageVersions) {
            if (itemImageVersions[filename]) {
                callback("static/v/" + itemImageVersions[filename] + "/img/" +
                    filename, callbackArg);
            } else {
                callback("static/img/" + filename, callbackArg);
            }
        })
    }

    function promiseImagePath(filename) {
        if (!itemImageVersions) {
            itemImageVersions = new Promise(function(res, rej) {
                $.getJSON("static/json/imageVersions.json", function(data) {
                    res(data);
                });
            });
        }
        return itemImageVersions.then(function(versions) {
            if (itemImageVersions[filename]) {
                return "static/v/" + itemImageVersions[filename] + "/img/" +
                    filename;
            } else {
                return "static/img/" + filename;
            }
        })
    }

    $.initialize(".ktimage", function() {
        var e = $(this);
        var image = e.attr("image");
        if (image) {
            e.attr("image", null);
            promiseImagePath(image).then(function(path) {
                e.append($("<img>").attr("data-image-src", path));
                continueImageSearch();
            });
        }
    });

    function preloadImage(img) {
        const src = img.dataset['imageSrc']
        if (src && src.length > 0) img.src = src

        const srcset = img.dataset['imageSrcset']
        if (srcset && srcset.length > 0) img.srcset = srcset

        const style = img.dataset['style'] || ""
        img.style = style
    }

    function hookupPreloads() {
        console.log("searching for imgs");
        count--;
        if (count == 0)
            clearInterval(i);
        const images = document.querySelectorAll('img[data-image-src],img[data-image-srcset]');
        // If we don't have support for intersection observer, load the
        // images immediately
        if (!('IntersectionObserver' in window)) {
            Array.from(images).forEach(image => preloadImage(image));
        } else {
            const config = {
                // If the image gets within 50px in the Y axis, start the
                // download.
                rootMargin: '50px 0px'
            };

            // The observer for the images on the page
            let observer = new IntersectionObserver(onIntersection, config);

            function onIntersection(entries) {
                // Loop through the entries
                entries.forEach(entry => {
                    // Are we in viewport?
                    if (entry.intersectionRatio > 0) {
                        // Stop watching and load the image
                        observer.unobserve(entry.target);
                        preloadImage(entry.target);
                    }
                });
            }

            images.forEach(image => {
                if (!image.waitingForPreload) {
                    console.log("found new image: ");
                    observer.observe(image);
                    image.waitingForPreload = true;
                }
            });
        }
    }

    document.addEventListener("DOMContentLoaded", function() {
        // clearInterval(i)
        hookupPreloads()
    })

    var count = 0;
    var i;
    continueImageSearch();

    function continueImageSearch() {
        var newInterval = count == 0;
        count = 5;
        if (newInterval)
        	i = setInterval(hookupPreloads, 1000)
    }
    window.continueImageSearch = continueImageSearch;

    window.getImageName = getImageName;
    window.getImagePath = getImagePath;
}());