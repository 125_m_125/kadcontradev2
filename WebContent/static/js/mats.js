;
(function() {
    window.itemlist = new Promise(function(res, rej) {
        $(document).ready(function() {
            if ($("#mats").length) {
                function filter() {
                    sessionStorage.setItem("itemFilter_name", $("#matfilter").val());
                    sessionStorage.setItem("itemFilter_owned", $("#owned").is(':checked'));
                    $('.item').each(function(i) {
                        if ($("#matfilter").val() == "itemname" || $(this).attr('id').toLowerCase().indexOf($("#matfilter").val().toLowerCase()) >= 0) {
                            if ($("#owned").is(':checked')) {
                                if ($(this).attr('id').indexOf(".0.") >= 0) {
                                    $(this).show();
                                } else {
                                    $(this).hide();
                                }
                            } else {
                                $(this).show();
                            }
                        } else {
                            $(this).hide();
                        }
                    });
                }
                $("#matfilter").keyup(function() {
                    filter()
                });
                $("#owned").change(function() {
                    filter()
                });

                d3.tsv("api/v2.0/users/D" + userid + "/items", function(error, data) {
                    if (error) {
                        rej(error);
                        return;
                    }
                    var cdl = new CountDownLatch(data.length, function() {
                        $("#matfilter").val(sessionStorage.getItem("itemFilter_name"));
                        $('#owned').prop('checked', sessionStorage.getItem("itemFilter_owned") !== "false");
                        filter();
                        var scroll = sessionStorage.getItem("itemScroll");
                        if (scroll) {
                        	$("#mats").scrollTop(scroll);
                        }
                        $("#mats").on("scroll", function() {
                    		sessionStorage.setItem("itemScroll", $("#mats").scrollTop());
                    	});
                    });
                    data.forEach(function(entry) {
//                        getImagePath(getImageName(entry.id), function(filename, entry) {
                            var htmlEntry = $("<li>");
                            htmlEntry.addClass("list-group-item item itemlistentry");
                            htmlEntry.attr("id", entry.name);
                            if (entry.amount > 0) {
                                htmlEntry.attr("id", htmlEntry.attr("id") + ".0.");
                            }
                            htmlEntry.on("click", function() {
                                var activeTradeBoxes = $(".active_trade_box");
                                if (activeTradeBoxes.length > 0) {
                                    activeTradeBoxes.val(entry.id).trigger('change');
                                } else if ($("#stats").length > 0) {
                                    history.pushState(null, "", "stats?res=" + entry.id);
                                    window.setStatsItem(entry.id);
                                } else {
                                    location.href = "stats?res=" + entry.id;
                                }
                            });
                            var innerHtml = "<div class='row'><div class='ktimage col-xs-2' image="+getImageName(entry.id)+"></div><div class=col-xs-10>";
                            innerHtml += entry.name + "<br>";
                            innerHtml += entry.amount + "</div></div></li>";
                            htmlEntry.html(innerHtml);
                            $("#itemContainer").append(htmlEntry);
                            cdl.countDown();
//                        }, entry);
                    });
                    res(data);
                });
            }
        });
    });
}());
