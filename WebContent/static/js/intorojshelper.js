function onNextIntorjsStep(targetElement) {
	console.log("expanding ", targetElement)
	var jqueryElement = $(targetElement);
	if (jqueryElement.hasClass("panel")) {
		jqueryElement.find(".panel-collapse").collapse('show');
	}
}