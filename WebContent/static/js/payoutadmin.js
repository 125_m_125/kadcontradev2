$(document).ready(function() {
	var form = $("#adminPayoutEdit");
	if (form.length) {
		$("#adminPayoutEdit").on("submit", function(event) {
			try {
				var result = {};
				$('#payoutAdminData > tbody  > tr').each(function(i, row) {
					row = $(row);
					var tid = row.find(".entryid").text();
					var amount = row.find(".modAmount").val();
					if (tid && amount)
						result[tid] = Number(amount);
				});
				var data = $("<input>").attr("name", "materials").val(JSON.stringify(result));
				$(form).append(data);
				return true;
			} catch (e) {
				event.preventDefault();
			}
		});
	}
});
