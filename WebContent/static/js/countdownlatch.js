function CountDownLatch(count, callback) {
	this.countDown = function() {
		if (--count<=0){
			callback();
		}
	}
}