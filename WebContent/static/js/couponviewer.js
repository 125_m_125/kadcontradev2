$(document).ready(function() {
	if ($('#couponTable').length) {
		$('#couponTable').DataTable({
			"processing" : true,
			"serverSide" : true,
			"ajax" : {
				"url" : "admin/coupon/ajax"
			},
			"columns" : [ {
				"data" : "code"
			}, {
				"data" : "material"
			}, {
				"data" : "amount"
			}, {
				"data" : "redeemer"
			} ],
			"ordering" : false,
			"scrollX" : false,
			"scrollCollapse" : true
		});
		$("#material").select2();
	}
});