$(document).ready(function() {
    if ($("#messageBody").length) {
        d3.tsv("api/v2.0/users/D" + (typeof auserid!=="undefined"? auserid : userid) + "/messages", function(error, data) {
            data.forEach(function(entry) {
                var rowContent = "<td>" + new Date(parseInt(entry.timestamp)).toLocaleString() + " " + entry.message + "</td>";
                var row = $("<tr></tr>").append(rowContent);
                $("#messageBody").append(row);
            });
        });
    }
});