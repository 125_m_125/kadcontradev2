$(document).ready(function() {
	if ($("#aItemBody").length) {
		d3.tsv("api/v2.0/users/D" + auserid + "/items", function(error, data) {
			data.forEach(function(entry) {
				var tr = $("<tr>").attr("id", "tr" + entry.id);
				tr.append($('<td>').text(entry.name));
				tr.append($('<td>').text(entry.amount));

				$("#aItemBody").append(tr);
			});
		});
	}
	if ($("#aPayoutBody").length) {
		d3.tsv("api/v2.0/users/D" + auserid + "/payouts", function(error, data) {
			data.forEach(function(entry) {
				var tr = $("<tr>");
				tr.append($('<td>').text(entry.materialName));
				tr.append($('<td>').text(entry.amount));
				tr.append($('<td>').text(entry.state));
				tr.append($('<td>').text(entry.message));

				if (entry.state === "Offen" || entry.state === "Fehlgeschlagen") {
					var form = $("<form method=\"POST\" action=\"admin/users\">");
					form.append("<input type=\"hidden\" name=\"csrfPreventionSalt\" value=\"" + csrfPreventionSalt + "\">");
					form.append("<input type=\"hidden\" name=\"id\" value=\"" + entry.id + "\">");
					form.append("<input type=\"hidden\" name=\"uid\" value=\"" + auserid + "\">");
					form.append("<input type=\"submit\" class=\"btn btn-primary btn-block\" name=\"cancelPayout\" value=\"Zwangsabbrechen\">");
					tr.append(form);
				} else {
					tr.append("<input class=\"btn btn-primary btn-block\" name=\"cancelPayout\" value=\"Zwangsabbrechen\" disabled>")
				}

				$("#aPayoutBody").append(tr);
			});
		});
	}
	if ($("#aOrderBody").length) {
		d3.tsv("api/v2.0/users/D" + auserid + "/orders", function(error, data) {
			data.forEach(function(entry) {
				var tr = $("<tr>");
				tr.append($('<td>').text(entry.buy));
				tr.append($('<td>').text(entry.materialId));
				tr.append($('<td>').text(entry.amount));
				tr.append($('<td>').text(entry.price));
				tr.append($('<td>').text(entry.sold));

				if (entry.sold !== entry.amount) {
					var form = $("<form method=\"POST\" action=\"admin/users\">");
					form.append("<input type=\"hidden\" name=\"csrfPreventionSalt\" value=\"" + csrfPreventionSalt + "\">");
					form.append("<input type=\"hidden\" name=\"id\" value=\"" + entry.id + "\">");
					form.append("<input type=\"hidden\" name=\"uid\" value=\"" + auserid + "\">");
					form.append("<input type=\"submit\" class=\"btn btn-primary btn-block\" name=\"cancelOrder\" value=\"Zwangsabbrechen\">");
					tr.append($('<td>').html(form));
				} else {
					tr.append($('<td>').html("<input class=\"btn btn-primary btn-block\" name=\"cancelOrder\" value=\"Zwangsabbrechen\" disabled>"));
				}

				$("#aOrderBody").append(tr);
			});
		});
	}
	$("#playerselector #uid").on("input", function() {
		$("#playerselector #uname").val("");
	});
	$("#playerselector #uname").on("input", function() {
		$("#playerselector #uid").val("");
	});
});