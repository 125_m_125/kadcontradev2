$(document).ready(function() {
	if ($('#logTable').length) {
		$('#logTable').DataTable({
			"processing" : true,
			"serverSide" : true,
			"ajax" : {
				"url" : "admin/log/ajax",
				"data" : function(d) {
					d.uid = $("#uid").val();
					d.level = $("#level").val();
					d.types = $("#types").val();
				}
			},
			"columns" : [ {
				"data" : "id"
			}, {
				"data" : "time"
			}, {
				"data" : "type"
			}, {
				"data" : "level"
			}, {
				"data" : "uid"
			}, {
				"data" : "message"
			} ],
			"ordering" : false,
			"searching" : false,
			"scrollX" : false,
			"scrollCollapse" : true
		});
		$("#types").select2()
	}
});