$(document).ready(function() {
	if ($("#payoutBody").length) {
		$("#item").select2();

		d3.tsv("api/v2.0/users/D" + userid + "/payouts", function(error, data) {
			data.forEach(function(entry) {
				var tr = $("<tr>");
				tr.append($('<td>').text(entry.materialName));
				tr.append($('<td>').text(entry.payoutType));
				tr.append($('<td>').text(entry.amount));
				var state = $('<td>').text(entry.state);
				if (entry.state.indexOf("Fehlgeschlagen")>=0) {
					state.css("color", "red");
				} else if (entry.state.indexOf("Unbekannt")>=0) {
					state.css("color", "orange");
				} else if (entry.state.indexOf("Erfolgreich")>=0) {
					state.css("color", "green");
				} else if (entry.state.indexOf("Bearbeitung")>=0) {
					state.css("color", "dodgerblue");
				} else if (entry.state.indexOf("Aufgeteilt")>=0) {
					state.css("color", "gray");
				}
				tr.append(state);
				tr.append($('<td>').text(entry.message));

				if (entry.state === "Offen") {
					var form = $("<form method=\"POST\" action=\"auszahlung\">");
					form.append("<input type=\"hidden\" name=\"csrfPreventionSalt\" value=\"" + csrfPreventionSalt + "\">");
					form.append("<input type=\"hidden\" name=\"id\" value=\"" + entry.id + "\">");
					form.append("<input type=\"hidden\" name=\"amount\" value=\"" + entry.amount + "\">");
					form.append("<input type=\"hidden\" name=\"item\" value=\"" + entry.material + "\">");
					form.append("<input type=\"submit\" class=\"btn btn-primary btn-block\" name=\"cancel\" value=\"Abbrechen\">");
					tr.append($('<td>').append(form));
				} else if (entry.state === "Fehlgeschlagen") {
					var form = $("<form method=\"POST\" action=\"auszahlung\">");
					form.append("<input type=\"hidden\" name=\"csrfPreventionSalt\" value=\"" + csrfPreventionSalt + "\">");
					form.append("<input type=\"hidden\" name=\"id\" value=\"" + entry.id + "\">");
					form.append("<input type=\"hidden\" name=\"amount\" value=\"" + entry.amount + "\">");
					form.append("<input type=\"hidden\" name=\"item\" value=\"" + entry.material + "\">");
					form.append("<input type=\"submit\" class=\"btn btn-primary btn-block\" name=\"takeout\" value=\"Entnehmen\">");
					tr.append($('<td>').append(form));
				} else {
					tr.append($('<td>'));
				}

				$("#payoutBody").append(tr);
			});
		});
	}
});