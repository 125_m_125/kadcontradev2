window.onerror = function(message, source, lineno, colno, error) {
	if ((typeof userid !== "undefined") && userid>0) {
		$.ajax({
			url: "api/v2.0/users/D" + userid + "/errors?csrfPreventionSalt="+csrfPreventionSalt,
			type: "POST",
			dataType: "xml/html/script/json",
			contentType: "application/json",
			data: JSON.stringify({message,source,lineno,colno,error:error.stack})
		});
	}
};
