<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Auszahlung</title>
<jsp:include page="/c/chead.jsp" />
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	<jsp:include page="/c/mats.jsp" />

	<div id="rcontent" class="col-xs-12 col-sm-8 col-md-9"
		data-intro='Hier können die aktuellen Auszahlungen eingesehen und bearbeitet werden. Außerdem können neue Auszahlungen in Auftrag gegeben werden.'>
		<div class="row"
			data-intro='hier können neue Auszahlungen erstellt werden.'>
			<form method="POST" action="auszahlung"
				class="col-md-6 form-horizontal">
				<fieldset>
					<legend>Neue Auszahlung für Kadis</legend>
					<input type="hidden" name="csrfPreventionSalt"
						value="${csrfPreventionSalt}" /> <input type="hidden" name="item"
						value="-1">
					<div class="form-group row"
						data-intro='Auszahlungen von Kadis können entweder ingame direkt an den Spieler oder über VKBA getätigt werden. Eine Auszahlung über VKBA erfordert ein Konto bei VKBA, das auf den Namen des Spielers läuft. Ingame-Überweisungen werden nur direkt an den Spieler selbst und nicht an dritte oder Konten getätigt.'>
						<label for="type" class="col-md-2 control-label">Typ</label>
						<div class="col-md-10">
							<select name="type" class="form-control">
								<option value="kadcon"
									<c:if test="${param.type eq 'kadcon'}">selected</c:if>
									value="kadcon">Überweisung</option>
								<%--<option value="vkba"
									<c:if test="${param.type eq 'vkba'}">selected</c:if>
									value="vkba">vkba</option>  --%>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="amount" class="col-md-2 control-label">Anzahl</label>
						<div class="col-md-10">
							<input type="number" min="0.01" step="0.01" name="amount"
								class="form-control"
								value='<c:out value="${param.amount}"></c:out>'>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<input type="submit" class="btn btn-block btn-primary"
								name="payout" value="Auszahlen" />
						</div>
					</div>
				</fieldset>
			</form>
			<form method="POST" action="auszahlung"
				class="col-md-6 form-horizontal">
				<fieldset>
					<legend>Neue Auszahlung für Items</legend>
					<input type="hidden" name="csrfPreventionSalt"
						value="${csrfPreventionSalt}" />
					<div class="form-group row"
						data-intro='Um sich Items auszahlen lassen zu können, müssen erst einige Angaben unter Einstellungen gemacht werden. Zur Auswahl stehen anschließend entweder eine Lieferung an die gewünschte Position, oder eine Lieferung über die Auszahlungsboxen auf den verschiedenen Servern.'>
						<label class="col-md-2 control-label">Typ</label>
						<div class="col-md-10">
							<select name="type" class="form-control">
								<option
									<c:if test="${param.type eq 'lieferung'}">selected</c:if>
									<c:if test="${not payoutTypes[0]}">disabled</c:if>
									value="lieferung">Lieferung</option>
								<option <c:if test="${param.type eq 'bs1'}">selected</c:if>
									<c:if test="${not payoutTypes[1]}">disabled</c:if> value="bs1">Box
									S1</option>
								<option <c:if test="${param.type eq 'bs2'}">selected</c:if>
									<c:if test="${not payoutTypes[2]}">disabled</c:if> value="bs2">Box
									S2</option>
								<option <c:if test="${param.type eq 'bs3'}">selected</c:if>
									<c:if test="${not payoutTypes[3]}">disabled</c:if> value="bs3">Box
									S3</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2 control-label">Item</label>
						<div class="col-md-10">

							<select id="item" name="item" class="form-control"
								style="height: 34px;">
								<c:forEach var="i" items="${allItems}">
									<c:if test="${not fn:startsWith(i[0], '-')}">
										<option value="${i[0]}"
											<c:if test="${param.item eq i[0]}">selected</c:if>><c:out
												value="${i[1]}" /></option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="amount" class="col-md-2 control-label">Anzahl</label>
						<div class="col-md-10">
							<input id="amount" type="number" min="1" step="1" name="amount"
								class="form-control"
								value="<c:out value="${param.amount}"></c:out>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<input type="submit" class="btn btn-block btn-primary"
								name="payout" value="Auszahlen" />
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="row flip-scroll"
			data-intro='hier werden die letzten Auszahlungen angezeigt. Über die Buttons in der Aktions-Spalte können Auszahlungen abgebrochen oder die Items aus fehlgeschlagenen Auszahlungen wieder entnommen werden.'>
			<table class="table">
				<thead>
					<tr>
						<th>Item</th>
						<th>Typ</th>
						<th>Anzahl</th>
						<th
							data-intro='<b>Offen:</b> Die Auszahlung wurde bisher noch nicht bearbeitet. Ein Mitarbeiter von ${applicationScope.appName} wird sich innerhalb der nächsten 24 Stunden um diese kümmern.<br><b>Unbekannt:</b> Bei der Verarbeitung der Anfrage ist ein unbekannter Fehler aufgetreten. Ein Mitarbeiter von ${applicationScope.appName} wird sich so bald wie möglich um diese kümmern.<br><b>In Bearbeitung:</b> Ein Mitarbeiter von ${applicationScope.appName} bearbeitet gerade diese Auszahlung.<br><b>Erfolgreich: </b>Die Bearbeitung der Auszahlung wurdre erfolgreich abgeschlossen. Die Items/Kadis befinden sich an der Gewünschten Position.<br><b>Aufgeteilt: </b>Diese Auszahlung wurde partiell bearbeitet und deshalb in zwei Auszahlungen aufgeteilt.<br><b>Abgebrochen: </b>Diese Auszahlung wurde durch den Spieler abgebrochen.<br><b>Fehlgeschlagen: </b>Diese Auszahlung konnte nicht erfüllt werden. Die Items können wieder in das Inventar entnommen werden.<br><b>Fehlgeschlagen(entnommen): </b></b>Diese Auszahlung konnte nicht erfüllt werden. Die Items befinden sich wieder im inventar des Spielers.'>Status</th>
						<th>Bemerkung</th>
						<th>Aktion</th>
					</tr>
				</thead>
				<tbody id="payoutBody">
					<!-- will be filled by script -->
				</tbody>
			</table>
		</div>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>