<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Captcha required</title>
</head>
<body>
	<c:out value="${captchaReason}"></c:out>
	<br> Um die Sperre aufzuheben, kannst du entweder einige Minuten
	warten, oder ein Captcha lösen.
	<br>
	<c:if test="${allowsCaptcha}">
		<script type="text/javascript">
			var script = document.createElement("script");
			script.setAttribute("type", "text/javascript");
			script.setAttribute("src", "https://www.google.com/recaptcha/api.js");
			document.getElementsByTagName("head")[0].appendChild(script);
		</script>
		<form method="POST">
			<input type="hidden" name="csrfPreventionSalt"
				value="${csrfPreventionSalt}" />
			<div class="g-recaptcha"
				data-sitekey='<c:out value="${reCaptchaSiteKey}"></c:out>'></div>
			<input type="submit" name="submit" value="Abschicken" />
		</form>
	</c:if>
	<c:if test="${not allowsCaptcha}">
		<form method="POST">
			<input type="hidden" name="csrfPreventionSalt"
				value="${csrfPreventionSalt}" /> <input type="checkbox"
				name="accept" id="accept" /> <label for="accept">ich bin
				damit einverstanden, dass mir ein reCaptcha von Google angezeigt
				wird, wodurch Google mich tracken kann.</label> <input type="submit"
				name="submitAccept" value="Abschicken" />
		</form>
	</c:if>
</body>
</html>