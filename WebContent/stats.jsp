<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Statistiken</title>
<jsp:include page="/c/chead.jsp" />

<!-- graph libraries -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.5.4/c3.min.js" defer></script>
<script type="text/javascript"  src="static/v/${jsVersions['stats.min.js']}/js2/stats.min.js" defer></script>
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.5.4/c3.min.css">
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	<jsp:include page="/c/mats.jsp" />

	<div id="rcontent" class="col-xs-12 col-sm-8 col-md-9" data-intro='Hier können Statistiken zu den verschiedenen Handelbaren Items eingesehen werden. Das aktuell angezeigte Item kann durch einen Klick auf ein Item aus der Itemliste geändert werden. Die Statistiken für Kadis stellen dabei einen Index dar, der den aktuellen Gesamttrend aller Items zusammen anzeigt.'>
		<h3 id="statsTitle">Statistiken f&uuml;r unknown</h3>
		<br>
		<div id="stats" data-intro='In diesem Abschnitt werden einige generalle Statistiken zu dem aktuellen Item angezeigt. Der Wert errechnet sich dabei aus den Preisen der am letzten Tag ausgeführten Trades, der offenen Orders und dem Preis des Vorherigen Tages.'>
			<label for="value" class="col-md-3 form-control-label">Wert</label>
			<div class="col-md-3">
				<input id="value" class="form-control" type="text" disabled />
			</div>
			<label for="d1" class="col-md-3 form-control-label">Wert&auml;nderung
				1 Tag</label>
			<div class="col-md-3">
				<input id="d1" class="form-control" type="text" disabled />
			</div>
			<label for="amount" class="col-md-3 form-control-label">Anzahl
				gehandelter Items</label>
			<div class="col-md-3">
				<input id="amount" class="form-control" type="text" disabled />
			</div>
			<label for="d30" class="col-md-3 form-control-label">Wert&auml;nderung
				30 Tage</label>
			<div class="col-md-3">
				<input id="d30" class="form-control" type="text" disabled />
			</div>
		</div>
		<table id="orderbook" class="table table-responsive table-bordered" data-intro='Im Orderbuch werden aktuell offene Orders angezeigt. Durch einen Klick auf kaufen oder verkaufen, wir der Markt geöffnet und das Formular für eine neue Order mit den entsprechenden Daten ausgefüllt.'>
			<caption>Orderbuch</caption>
			<thead>
				<tr>
					<td>Typ</td>
					<td>Preis</td>
					<td>Anzahl</td>
					<td>Kaufen/Verkaufen</td>
				</tr>
			</thead>
			<tbody id="orderBody">
				<!-- will be filled by script -->
			</tbody>
		</table>
		<div id="graphs" data-intro='Hier wird der Wertverlauf des Items in den letzten dreißig Tagen dargestellt. Im Graphen können durch einen Klick auf die Einträge in der Legende verschiedene Daten versteckt oder angezeigt werden.'>
			<div id="selection" data-intro='Hier kann die Art der Visualisierung geändert werden.'>
				<ul class="nav nav-pills nav-justified">
					<li><a id="lineSelector" data-toggle="tab" href="#line">Zeitstrahl</a></li>
					<li><a id="tableSelector" data-toggle="tab" href="#table">Tabelle</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div id="line" class="tab-pane fade"></div>
				<div id="table" class="flip-scroll tab-pane fade"></div>
			</div>
		</div>
		<h4>Vergangene durchgeführte Trades</h4>
		<div id="executedOrdersTable" class="col-md-12" data-intro='Hier werden die zuletzt ausgeführten Trades angezeigt.'></div>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>