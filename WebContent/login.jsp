<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Home</title>
<jsp:include page="/c/chead.jsp" />
</head>
<body>
	<c:if test="${!sessionExists}">
		<jsp:include page="/c/top2.jsp" />
	</c:if>
	<c:if test="${sessionExists}">
		<jsp:include page="/c/top.jsp" />
	</c:if>
	<c:if test="${not cookies}"><div class="alert alert-danger hideWithCookies">Derzeit werden keine Cookies akzeptiert. Einige Funktionen wie zum Beispiel der Login stehen nicht zur Verfügung.</div></c:if>
	<!-- Login -->
	<div id="rcontent" class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<form method="POST" action="." class="col-md-12">
					<fieldset>
						<legend>Login</legend>
						<input type="hidden" name="csrfPreventionSalt"
							value="${csrfPreventionSalt}" />
						<div class="form-group row">
							<!-- <!-- <label for="l_name" class="col-md-2 form-control-label">Benutzername</label> -->
							<div class="col-md-12">
								<input type="text" class="form-control" id="l_name"
									name="l_name" placeholder="Benutzername"
									value="<c:out value="${param.l_name}" />" />
							</div>
						</div>
						<div class="form-group row">
							<!-- <label for="l_pass" class="col-md-2 form-control-label">Passwort</label> -->
							<div class="col-md-12">
								<input type="password" class="form-control" id="l_pass"
									name="l_pass" placeholder="Passwort" />
							</div>
						</div>
						<div class="form-group row">
							<!-- <label for="l_pass" class="col-md-2 form-control-label">Passwort</label> -->
							<div class="col-md-12">
								<div class="checkbox">
									<label> <input type="checkbox" name="remember" id="remember" <c:if test="remember">checked="checked"</c:if> />
										Eingeloggt bleiben
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<input type="submit"
									class="btn btn-block btn-primary cookieWaiting" name="login"
									value="Login"
									<c:if test="${not cookies}">disabled=disabled title="Es müssen erst Cookies akzeptiert werden!"</c:if> />
							</div>
						</div>
					</fieldset>
				</form>

				<!-- Password -->
				<form method="POST" action="." class="col-md-12"
					data-intro='Hier kann das Passwort eines existierenden Spielers geändert werden.'>
					<fieldset>
						<legend>Passwort &auml;ndern</legend>
						<input type="hidden" name="csrfPreventionSalt"
							value="${csrfPreventionSalt}" />
						<div class="form-group row">
							<!-- <label for="c_name" class="col-md-2 form-control-label">Benutzername</label> -->
							<div class="col-md-12">
								<input type="text" class="form-control" id="c_name"
									name="c_name" placeholder="Benutzername"
									value="<c:out value="${param.c_name}" />" />
							</div>
						</div>
						<div class="form-group row">
							<!-- <label for="c_pass" class="col-md-2 form-control-label">Passwort</label> -->
							<div class="col-md-12">
								<input type="password" class="form-control" id="c_pass"
									name="c_pass" placeholder="Neues Passwort" />
							</div>
						</div>
						<div class="form-group row">
							<!-- <label for="c_pass_2" class="col-md-2 form-control-label">Passwort
				wiederholen</label> -->
							<div class="col-md-12">
								<input type="password" class="form-control" id="c_pass_2"
									name="c_pass_2" placeholder="Neues Passwort wiederholen" />
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<input type="submit"
									class="btn btn-block btn-primary cookieWaiting" name="changepw"
									value="Passwort &auml;ndern"
									data-intro='Nach dem Abschicken des Formulares wird ein Schild genannt, dass ingame aktiviert werden muss. Dadurch verifizieren wir, dass der passwortändernde Spieler tatsächlich der ist, als den er sich ausgibt. Nach der Aktivierung des Schildes muss zwei Minuten gewartet werden und anschließend der Button zum Auslesen des Kontoauszuges oben rechts in der Ecke geklickt werden. Danach kann das neue Passwort genutzt werden.'
									<c:if test="${not cookies}">disabled=disabled title="Es müssen erst Cookies akzeptiert werden!"</c:if> />
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<!-- Register -->
			<div class="col-md-6"
				data-intro='Hier kann ein neuer Benutzer registriert werden.'>
				<form method="POST" class="col-md-12">
					<fieldset>
						<legend>Registrieren</legend>
						<input type="hidden" name="csrfPreventionSalt"
							value="${csrfPreventionSalt}" />
						<div class="form-group row"
							data-intro='Der Benutzername muss mit dem Ingamenamen auf Kadcon übereinstimmen.'>
							<!-- <label for="r_name" class="col-md-2 form-control-label">Benutzername</label> -->
							<div class="col-md-12">
								<input type="text" class="form-control" id="r_name"
									name="r_name" placeholder="Benutzername"
									value="<c:out value="${param.r_name}" />" />
							</div>
						</div>
						<div class="form-group row">
							<!-- <label for="r_pass" class="col-md-2 form-control-label">Passwort</label> -->
							<div class="col-md-12">
								<input type="password" class="form-control" id="r_pass"
									name="r_pass" placeholder="Passwort" />
							</div>
						</div>
						<div class="form-group row">
							<!-- <label for="r_pass_2" class="col-md-2 form-control-label">Passwort
				wiederholen</label> -->
							<div class="col-md-12">
								<input type="password" class="form-control" id="r_pass_2"
									name="r_pass_2" placeholder="Passwort wiederholen" />
							</div>
						</div>
						<!-- <br>
			Server:   -->
						<input type="hidden" name="r_server"
							value="<c:out value="${param.r_server}"/>" />
						<!-- <br>
			x-Koordinate:   -->
						<input type="hidden" name="r_x"
							value="<c:out value="${param.r_x}"/>" />
						<!-- <br>
			y-Koordinate:   -->
						<input type="hidden" name="r_y"
							value="<c:out value="${param.r_y}"/>" />
						<!-- <br>
			z-Koordinate:  -->
						<input type="hidden" name="r_z"
							value="<c:out value="${param.r_z}"/>" />
						<c:if test="${empty r}">
							<div class="form-group row"
								data-intro='Hier kann die ID des Spielers angegeben werden, der dich auf ${applicationScope.appName} aufmerksam gemacht hat. Bei bestimmten Referral-Links wird dieses Feld automatisch ausgefüllt. Sowohl der werbende Spieler als auch der geworbene Spieler erhalten einen kleinen Teil der Steuern, die vom jeweils anderen Spieler gezahlt wurden. Für keinen der Spieler entstehen dabei höhere Preise oder Steuern.'>
								<!-- <label for="ref" class="col-md-2 form-control-label">Werber</label> -->
								<div class="col-md-12">
									<input type="text" class="form-control" id="ref" name="ref"
										placeholder="(Optional) Angeworben von"
										value="<c:out value="${param.ref}" />" />
								</div>
							</div>
						</c:if>
						<div class="form-group row">
							<!-- <label for="agb" class="col-md-2 form-control-label"></label> -->
							<div class="col-md-12">
								<div class="checkbox">
									<label> <input type="checkbox" name="agb" id="agb" required/>
										<a href="p/agb" target="_blank">Ich stimme den AGB und
											Datenschutzbestimmungen zu</a>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<input type="submit"
									class="btn btn-block btn-primary cookieWaiting" name="register"
									value="Registrieren"
									data-intro='Nach dem Abschicken des Formulares wird ein Schild genannt, dass ingame aktiviert werden muss. Dadurch verifizieren wir, dass der sich registrierende Spieler tatsächlich der ist, als den er sich ausgibt. Nach der Aktivierung des Schildes muss zwei Minuten gewartet werden und anschließend der Button zum Auslesen des Kontoauszuges oben rechts in der Ecke geklickt werden. Danach kann sich der Spieler einloggen.'
									<c:if test="${not cookies}">disabled=disabled title="Es müssen erst Cookies akzeptiert werden!"</c:if> />
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

		<div id="news" class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<h2>News:</h2>
					<p>Derzeit gibt es keine news</p>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>