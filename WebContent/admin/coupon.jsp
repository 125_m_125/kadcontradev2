<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Logeinsicht</title>
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css">
<jsp:include page="/c/chead.jsp" />
<!-- datatables -->
<script type="text/javascript" charset="utf8"
	src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
	<jsp:include page="/c/top.jsp" />

	<div id="rcontent" class="col-md-12">
		<form method="POST" action="/admin/coupon" class="col-md-12">
			<fieldset>
				<legend>Neue Coupons erstellen</legend>
				<input type="hidden" name="csrfPreventionSalt"
					value="${csrfPreventionSalt}" />
				<div class="form-group-row">
					<label for="preCode" class="col-md-3 control-label">preCode</label>
					<div class="col-md-3">
						<input type="text" class="form-control" id="preCode"
							name="preCode" />
					</div>
					<label for="couponCount" class="col-md-3 control-label">Couponzahl</label>
					<div class="col-md-3">
						<input type="number" min="1" step="1" class="form-control"
							id="couponCount" name="couponCount" />
					</div>
				</div>
				<div class="form-group-row">
					<label for="material" class="col-md-3 control-label">Material</label>
					<div class="col-md-9">
						<select id="material" name="material" class="form-control"
							style="height: 34px;">
							<c:forEach var="i" items="${allItems}">
								<option value="${i[0]}"
									<c:if test="${param.item eq i[0]}">selected</c:if>><c:out
										value="${i[1]}" /></option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group-row">
					<label for="materialAmount" class="col-md-3 control-label">Materialmenge</label>
					<div class="col-md-9">
						<input type="number" min="1" step="1" class="form-control"
							id="materialAmount" name="materialAmount" />
					</div>
				</div>
				<div class="col-md-12 form-group-row">
					<input type="submit" class="btn btn-block btn-primary"
						name="create" value="Erstellen" />
				</div>
			</fieldset>
		</form>
		<table id="couponTable">
			<thead>
				<tr>
					<th>Code</th>
					<th>Material</th>
					<th>Anzahl</th>
					<th>Einlöser</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Code</th>
					<th>Material</th>
					<th>Anzahl</th>
					<th>Einlöser</th>
				</tr>
			</tfoot>
		</table>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>