<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Logeinsicht</title>
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css">
<jsp:include page="/c/chead.jsp" />
<!-- datatables -->
<script type="text/javascript" charset="utf8"
	src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
	<jsp:include page="/c/top.jsp" />

	<div id="rcontent" class="col-md-12">
		<form class="form-horizontal">
			<div class="form-group-row">
				<label for="uid" class="col-md-2 control-label">Userid</label>
				<div class="col-md-4">
					<input type="number" min="1" step="1" class="form-control" id="uid"
						name="uid" />
				</div>
				<label for="level" class="col-md-2 control-label">Level</label>
				<div class="col-md-4">
					<input type="number" min="1" class="form-control" id="level"
						name="level" />
				</div>
			</div>
			<div class="form-group-row">
				<label for="types" class="col-md-2 control-label">Typen</label>
				<div class="col-md-10">
					<select id="types" name="types" multiple="multiple"
						class="form-control" style="width: 100%">
						<option value="br" selected="selected">Bankleser</option>
						<option value="mt" selected="selected">Fehlende
							Übersetzung</option>
						<option value="pa" selected="selected">Auszahlungen</option>
						<option value="sr">Seitenanfragen</option>
						<option value="sy" selected="selected">System</option>
						<option value="tr" selected="selected">Handel</option>
						<option value="at" selected="selected">Angriffe</option>
						<option value="ad" selected="selected">Administratoren</option>
						<option value="ws">Websocket</option>
						<option value="to" selected="selected">Token</option>
						<option value="co" selected="selected">Coupons</option>
					</select>
				</div>
			</div>
		</form>
		<table id="logTable">
			<thead>
				<tr>
					<th>id</th>
					<th>Zeit</th>
					<th>Typ</th>
					<th>Level</th>
					<th>Benutzerid</th>
					<th>Nachricht</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>id</th>
					<th>Zeit</th>
					<th>Typ</th>
					<th>Level</th>
					<th>Benutzerid</th>
					<th>Nachricht</th>
				</tr>
			</tfoot>
		</table>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>