<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - PO-Box Administration</title>
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css">
<jsp:include page="/c/chead.jsp" />
<!-- datatables -->
<script type="text/javascript" charset="utf8"
	src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
	<jsp:include page="/c/top.jsp" />

	<div id="rcontent" class="col-md-12">
		<form method="POST" action="admin/pobox" class="col-md-12 form-horizontal" role="form">
			<fieldset>
				<legend>Neue PO-Box erstellen</legend>
				<input type="hidden" name="csrfPreventionSalt"
					value="${csrfPreventionSalt}" />
				<div class="form-group row">
					<label for="server" class="col-md-1 control-label">Server</label>
					<div class="col-md-2">
						<input type="number" step="1" min="1" max="3" class="form-control"
							id="server" name="server"
							value="<c:out value="${param.server}" />" />
					</div>
					<label for="x" class="col-md-1 control-label">X</label>
					<div class="col-md-2">
						<input type="number" step="1" class="form-control" id="x" name="x"
							value="<c:out value="${param.x}" />" />
					</div>
					<label for="y" class="col-md-1 control-label">Y</label>
					<div class="col-md-2">
						<input type="number" step="1" class="form-control" id="y" name="y"
							value="<c:out value="${param.y}" />" />
					</div>
					<label for="z" class="col-md-1 control-label">Z</label>
					<div class="col-md-2">
						<input type="number" step="1" class="form-control" id="z" name="z"
							value="<c:out value="${param.z}" />" />
					</div>
				</div>
				<div class="col-md-12 form-group row">
					<input type="submit" class="btn btn-block btn-primary"
						name="create" value="Erstellen" />
				</div>
			</fieldset>
		</form>
		<table id="poboxTable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Server</th>
					<th>X|Y|Z</th>
					<th>Besitzer</th>
					<th>Status</th>
					<th>Aktion</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>Server</th>
					<th>X|Y|Z</th>
					<th>Besitzer</th>
					<th>Status</th>
					<th>Aktion</th>
				</tr>
			</tfoot>
		</table>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>