<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Adminstration - Auszahlungen</title>
<jsp:include page="/c/chead.jsp" />
<link
	href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
</head>
<body>
	<jsp:include page="/c/top.jsp" />

	<div id="rcontent" class="col-md-12">
		<c:if test="${empty payout}">
			<p>
				Aktuelle Anzahl an offenen Auszahlungen:
				<c:out value="${totalPayoutCount}" />
			</p>
			<form method="post" action="admin/payouts">
				<input type="hidden" name="csrfPreventionSalt"
					value="${csrfPreventionSalt}" />
				<button type="submit" name="request"
					class="btn btn-primary btn-block" value="Anfordern">Neue
					Auszahlungen erhalten</button>
			</form>
		</c:if>
		<c:if test="${not empty payout}">
			<table class="table table-bordered table-hover">
				<caption>Auszahlungseinstellungen</caption>
				<thead>
					<tr>
						<th>Benutzerid</th>
						<th>Benutzername</th>
						<th>Server</th>
						<th>X</th>
						<th>Y</th>
						<th>Z</th>
						<th>Warp</th>
						<th>Karte</th>
						<th>Finde</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><c:out value="${servantId}" /></td>
						<td><c:out value="${servantName}" /></td>
						<td><c:out value="${settings.server}" /></td>
						<td><c:out value="${settings.x}" /></td>
						<td><c:out value="${settings.y}" /></td>
						<td><c:out value="${settings.z}" /></td>
						<td><c:catch var="exception"><c:out value="${settings.warp}" /></c:catch></td>
						<td><a target="_blank"
							href="http://map${settings.server}.kadcon.de/?x=${settings.x}&y=${settings.y}&z=${settings.z}&zoom=7">http://map${settings.server}.kadcon.de/?x=${settings.x}&y=${settings.y}&z=${settings.z}&zoom=7</a></td>
						<td>/finde ${settings.x},${settings.y},${settings.z}</td>
					</tr>
				</tbody>
			</table>

			<table id="payoutAdminData" class="table table-bordered table-hover">
				<caption>Auszahlungen</caption>
				<thead>
					<tr>
						<th>Besitzer</th>
						<th>TradeID</th>
						<th>Item</th>
						<th>Anzahl</th>
						<th>Status</th>
						<th>Typ</th>
						<th>Bemerkung</th>
						<th>Bearbeitungsmenge</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="entry" items="${payout}">
						<tr>
							<td><c:out value="${entry.uid}" /></td>
							<td class="entryid"><c:out value="${entry.id}" /></td>
							<td><c:out value="${entry.itemName}" /></td>
							<td>${entry.stringAmount} (<fmt:parseNumber integerOnly="true" type="number" value="${entry.amount/64}" /> Stacks + ${entry.amount%64})</td>
							<td><c:out value="${entry.stringAmount}" /></td>
							<td><c:out value="${entry.state}" /></td>
							<td><c:out value="${entry.payoutType}" /></td>
							<td><c:out value="${entry.message}" /></td>
							<c:choose>
							<c:when test="${entry.material == '-1'}">
							<td><input class="col-md-12 modAmount" type="number" value=<c:out value="${entry.stringAmount}"/> min="0" max=<c:out value="${entry.stringAmount}"/> step="0.01"></td>
							</c:when>
							<c:otherwise>
							<td><input class="col-md-12 modAmount" type="number" value=<c:out value="${entry.amount}"/> min="0" max=<c:out value="${entry.amount}"/> step="1"></td>
							</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<form id="adminPayoutEdit" method="post" action="admin/payouts">
				<fieldset>
					<legend>Auszahlungsstatus bearbeiten</legend>
					<div class="col-md-2">
						<input type="hidden" name="csrfPreventionSalt"
							value="${csrfPreventionSalt}" /> <select name="state"
							class="form-control">
							<option value="SUCCESS">Erfolgreich</option>
							<option value="FAILURE">Fehlgeschlagen</option>
							<!-- <option value="3">Offen (Bearbeitung abbrechen)</option> -->
						</select>
					</div>
					<div class="col-md-8">
						<input type="text" name="message" placeholder="Nachricht"
							maxlength="50" class="form-control" />
					</div>
					<div class="col-md-2">
						<button type="submit" name="partialUpdate"
							class="btn btn-primary btn-block" value="Status &auml;ndern">Status
							&auml;ndern</button>
					</div>
				</fieldset>
			</form>
		</c:if>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>