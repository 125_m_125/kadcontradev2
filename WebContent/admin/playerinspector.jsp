<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Wartungsmodus</title>
<jsp:include page="/c/chead.jsp" />
<link
	href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
	<script type="text/javascript">auserid = ${requestScope.id}</script>
</head>
<body>
	<jsp:include page="/c/top.jsp" />

	<div id="rcontent" class="col-md-12">
		<form class="col-md-12 form-horizontal" method="GET"
			action="admin/users" id="playerselector">
			<label for="uname" class="col-xs-1 control-label">Name</label>
			<div class="col-md-3">
				<input type="text" class="form-control" id="uname" name="uname"
					value="<c:out value="${name}" />" />
			</div>
			<label for="uid" class="col-md-1 control-label">ID</label>
			<div class="col-md-3">
				<input type="number" step="1" min="1" class="form-control" id="uid"
					name="uid" value="<c:out value="${requestScope.id}" />" />
			</div>
			<input type="submit" class="btn col-md-4 btn-primary"
				value="Untersuchen" />
		</form>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingMessages">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse"
							href="#collapseMessages" aria-expanded="false"
							aria-controls="collapseMessages">Nachrichten</a>
					</h4>
				</div>
				<div id="collapseMessages" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="headingMessages">
					<div class="panel-body">
						<table class="table table-hover">
							<tbody id="messageBody">
								<!-- will be filled by script -->
							</tbody>
						</table>
						<form class="form-horizontal" method="POST" action="admin/users">
							<fieldset class="col-md-6">
								<legend>Nachricht senden</legend>
								<input type="hidden" name="csrfPreventionSalt"
									value="${csrfPreventionSalt}" /> <input type="hidden"
									name="uid" value="${requestScope.id}" />
								<div class="form-group row">
									<label for="message" class="control-label col-xs-3">Nachricht</label>
									<div class="col-xs-6">
										<input type="text" name="message" class="form-control" />
									</div>
									<div class="col-xs-3">
										<input type="submit" class="btn btn-block btn-primary"
											name="sendMessage" value="Ausf&uumlhren" />
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingItems">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse"
							href="#collapseItems" aria-expanded="false"
							aria-controls="collapseItems">Items</a>
					</h4>
				</div>
				<div id="collapseItems" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="headingItems">
					<div class="panel-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Item</th>
									<th>Anzahl</th>
								</tr>
							</thead>
							<tbody id="aItemBody">
								<!-- will be filled by script -->
							</tbody>
						</table>
						<form class="form-horizontal" method="POST" action="admin/users">
							<fieldset class="col-md-6">
								<legend>Zwangsauszahlung</legend>
								<input type="hidden" name="csrfPreventionSalt"
									value="${csrfPreventionSalt}" /> <input type="hidden"
									name="uid" value="${requestScope.id}" />
								<div class="form-group row">
									<label for="amount" class="control-label col-xs-3">ItemID</label>
									<div class="col-xs-9">
										<input type="text" name="matid" class="form-control" />
									</div>
								</div>
								<div class="form-group row">
									<label for="amount" class="control-label col-xs-3">Anzahl</label>
									<div class="col-xs-9">
										<input type="number" min="1" step="1" name="amount"
											class="form-control" />
									</div>
								</div>
								<div class="form-group row">
									<label for="message" class="control-label col-xs-3">Nachricht</label>
									<div class="col-xs-9">
										<input type="text" name="message" class="form-control" />
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-12">
										<input type="submit" class="btn btn-block btn-primary"
											name="payout" value="Auszahlen" />
									</div>
								</div>
							</fieldset>
						</form>
						<form class="form-horizontal" method="POST" action="admin/users">
							<fieldset class="col-md-6">
								<legend>Gutschrift/Lastschrift</legend>
								<input type="hidden" name="csrfPreventionSalt"
									value="${csrfPreventionSalt}" /> <input type="hidden"
									name="uid" value="${requestScope.id}" />
								<div class="form-group row">
									<label for="amount" class="control-label col-xs-3">ItemID</label>
									<div class="col-xs-9">
										<input type="text" name="matid" class="form-control" />
									</div>
								</div>
								<div class="form-group row">
									<label for="amount" class="control-label col-xs-3">Anzahl</label>
									<div class="col-xs-9">
										<input type="number" step="1" name="amount"
											class="form-control" />
									</div>
								</div>
								<div class="form-group row">
									<label for="message" class="control-label col-xs-3">Nachricht</label>
									<div class="col-xs-9">
										<input type="text" name="message" class="form-control" />
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-12">
										<input type="submit" class="btn btn-block btn-primary"
											name="addItems" value="Ausf&uumlhren" />
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOrders">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse"
							href="#collapseOrders" aria-expanded="false"
							aria-controls="collapseOrders">Orders</a>
					</h4>
				</div>
				<div id="collapseOrders" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="headingOrders">
					<div class="panel-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Kauf?</th>
									<th>Item</th>
									<th>Anzahl</th>
									<th>Preis</th>
									<th>Gehandelt</th>
									<th>Zwangsabbrechen</th>
								</tr>
							</thead>
							<tbody id="aOrderBody">
								<!-- will be filled by script -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingPayouts">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse"
							href="#collapsePayouts" aria-expanded="false"
							aria-controls="collapsePayouts">Auszahlungen</a>
					</h4>
				</div>
				<div id="collapsePayouts" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="headingPayouts">
					<div class="panel-body">
						<table class="table table-bordered table-hover">
							<caption>Auszahlungseinstellungen</caption>
							<thead>
								<tr>
									<th>Server</th>
									<th>X</th>
									<th>Y</th>
									<th>Z</th>
									<th>Warp</th>
									<th>Karte</th>
									<th>Finde</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><c:out value="${payoutSettings.server}" /></td>
									<td><c:out value="${payoutSettings.x}" /></td>
									<td><c:out value="${payoutSettings.y}" /></td>
									<td><c:out value="${payoutSettings.z}" /></td>
									<td><c:out value="${payoutSettings.warp}" /></td>
									<td><a target="_blank"
										href="http://map${payoutSettings.server}.kadcon.de/?x=${payoutSettings.x}&y=${payoutSettings.y}&z=${payoutSettings.z}&zoom=7">http://map${payoutSettings.server}.kadcon.de/?x=${payoutSettings.x}&y=${payoutSettings.y}&z=${payoutSettings.z}&zoom=7</a></td>
									<td>/finde ${payoutSettings.x},${payoutSettings.y},${payoutSettings.z}</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered table-hover">
							<caption>Auszahlungen</caption>
							<thead>
								<tr>
									<th>Item</th>
									<th>Anzahl</th>
									<th>Status</th>
									<th>Bemerkung</th>
									<th>Abbrechen</th>
								</tr>
							</thead>
							<tbody id="aPayoutBody">
								<!-- will be filled by script -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingLogs">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse"
							href="#collapseLogs" aria-expanded="false"
							aria-controls="collapseLogs">Logs</a>
					</h4>
				</div>
				<div id="collapseLogs" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="headingLogs">
					<div class="panel-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Zeit</th>
									<th>Typ</th>
									<th>Level</th>
									<th>Nachricht</th>
								</tr>
							</thead>
							<tbody id="logBody">
								<c:forEach var="logEntry" items="${logs}">
									<tr>
										<td><c:out value="${logEntry.time}"></c:out></td>
										<td><c:out value="${logEntry.type}"></c:out></td>
										<td><c:out value="${logEntry.level}"></c:out></td>
										<td><c:out value="${logEntry.message}"></c:out></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>