<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - MysqlExporter</title>
<jsp:include page="/c/chead.jsp" />
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	
	<div id="rcontent" class="col-md-12">
	<form method="POST" action="admin/mysqlexporter" class="col-md-12">
		<fieldset>
			<legend>Daten exportieren</legend>
			<input type="hidden" name="csrfPreventionSalt"
				value="${csrfPreventionSalt}" />
			<div class="form-group row">
				<label for="uid" class="col-md-2 control-label">Userid</label>
				<div class="col-md-2">
					<input type="number" min="1" step="1" class="form-control" id="user" name="user">
				</div>
				<label for="material" class="col-md-2 control-label">Material</label>
				<div class="col-md-2">
					<input type="number" min="-10" class="form-control" id="material" name="material">
				</div>
				<div class="col-md-2">
					<label><input type="checkbox" name="anonymous">Anonymisieren</label>
				</div>
				<div class="col-md-2">
					<label><input type="checkbox" name="everything">Alle</label>
				</div>
				<div class="col-md-2">
					<label><input type="checkbox" name="usertables">Usertabellen</label>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-3">
					<label><input type="checkbox" name="checksums">Prüfsummen</label>
				</div>
				<div class="col-md-3">
					<c:out value="${filenames[\"checksums\"]}"></c:out>
				</div>
				<div class="col-md-3">
					<label><input type="checkbox" name="zip">Zippen</label>
				</div>
				<div class="col-md-3">
					<c:out value="${filenames[\"zip\"]}"></c:out>
				</div>
			</div>
			<c:forEach items="${tables}" var="table">
				<div class="form-group row">
					<div class="col-md-8">
						<label><input type="checkbox" name="tables" value="${table}">${table}</label>
					</div>
					<div class="col-md-4"><c:out value="${filenames[table]}"></c:out></div>
				</div>
			</c:forEach>
			<div class="form-group row">
				<div class="col-md-12">
					<input type="submit" class="btn btn-block btn-primary"
						name="generate" value="Generieren">
				</div>
			</div>
		</fieldset>
	</form>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>