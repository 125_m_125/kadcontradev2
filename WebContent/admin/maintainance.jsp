<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Wartungsmodus</title>
<jsp:include page="/c/chead.jsp" />
<link
	href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	
	<div id="rcontent" class="col-md-12">
	<form method="POST" action="admin/maintainance" class="col-md-12">
		<fieldset>
			<legend>Wartungsmodus bearbeiten</legend>
			<input type="hidden" name="csrfPreventionSalt"
				value="${csrfPreventionSalt}" />
			<div class="form-group row">
				<div class="col-md-12">
					<div class="col-sm-6 col-md-1">
						<label><input type="checkbox" name="active"
							<c:if test="${active}">checked</c:if>>aktiv</label>
					</div>
					<div class="col-sm-6 col-md-2">
						<label><input type="checkbox" name="all"
							<c:if test="${all}">checked</c:if>>Alles sperren</label>
					</div>
					<div class="col-sm-4 col-md-3">
						<label><input type="checkbox" name="lockAdmins"
							<c:if test="${lockAdmins}">checked</c:if>>Administratoren
							sperren</label>
					</div>
					<div class="col-sm-4 col-md-3">
						<label><input type="checkbox" name="login"
							<c:if test="${login}">checked</c:if>>Loginsperre zulassen</label>
					</div>
					<div class="col-sm-4 col-md-3">
						<label><input type="checkbox" name="resources"
							<c:if test="${resources}">checked</c:if>>Resourcensperre
							zulassen</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-12">
					<input type="text" class="form-control" name="regex"
						placeholder="Regex" value="${regex}">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-12">
					<input type="submit" class="btn btn-block btn-primary"
						name="maintSubmit" value="Änderungen sichern">
				</div>
			</div>
		</fieldset>
	</form>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>