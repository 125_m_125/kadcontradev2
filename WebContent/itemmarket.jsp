<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Markt</title>
<jsp:include page="/c/chead.jsp" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.runtime.min.js"></script>
<script type="text/javascript" src="static/v/${jsVersions['handlebar.min.js']}/js2/handlebar.min.js"></script>
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	<jsp:include page="/c/mats.jsp" />

	<div id="rcontent" class="col-xs-12 col-sm-8 col-md-9" data-intro='Auf dieser Seite können die Orders verwaltet werden. Eine Order stellt dabei eine Kauf- oder Verkaufsanfrage dar, die mit den Ordern der anderen Spieler abgewickelt werden. Sobald ein passendes Gegenangebot für eine Order gefunden wird, werden die beiden Orders miteinander verrechnet. Bei einem Orderpaar muss dabei der Preis der Kauforder größer oder gleich dem Preis der Verkaufsorder sein. Der tatsächliche Preis entspricht dem Durchschnittspreis der beiden Orders.'>
		<div class="panel-group" id="tradeaccordion" role="tablist"
			aria-multiselectable="true">
			<!-- filled by script -->
		</div>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>