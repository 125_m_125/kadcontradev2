<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - &Uuml;bersicht</title>
<jsp:include page="/c/chead.jsp" />
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	<jsp:include page="/c/mats.jsp" />
	
	<div id="rcontent" class="col-xs-12 col-sm-8 col-md-9">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>letzte Neuigkeiten</th>
				</tr>
			</thead>
			<tbody id="messageBody">
				<!-- will be filled by script -->
			</tbody>
		</table>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>