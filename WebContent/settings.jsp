<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="de">
<head>
<title>${applicationScope.appName} - Einstellungen</title>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bowser/1.9.3/bowser.min.js"
	defer></script>
<script type="text/javascript"
	src="static/v/${jsVersions['u2f-api.min.js']}/lib/u2f-api.min.js" defer></script>
<script type="text/javascript"
	src="static/v/${jsVersions['webauthn.js']}/lib/webauthn.js" defer></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js"
	defer></script>
<script
	src="https://www.gstatic.com/firebasejs/5.0.4/firebase-messaging.js"
	defer></script>
<script
	src="static/v/${jsVersions['settings.min.js']}/js2/settings.min.js"
	defer></script>
<jsp:include page="/c/chead.jsp" />
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"
	defer></script>
<link rel="manifest" href="/manifest.json">
</head>
<body>
	<jsp:include page="/c/top.jsp" />
	<jsp:include page="/c/mats.jsp" />

	<div id="rcontent" class="col-xs-12 col-sm-8 col-md-9">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingPS">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapsePS" aria-expanded="false"
						aria-controls="collapseMessages">Einstellungen f&uuml;r
						Auszahlungen</a>
				</h4>
			</div>
			<div id="collapsePS" class="panel-collapse collapse" role="tabpanel"
				aria-labelledby="headingPS">
				<div class="panel-body">
					<form class="panel panel-default form-horizontal" method="POST"
						action="einstellungen">
						<fieldset>
							<legend>Lieferungseinstellungen</legend>
							<input type="hidden" name="csrfPreventionSalt"
								class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
							<div class="form-group row">
								<label for="server" class="col-md-1 control-label">Server</label>
								<div class="col-md-2">
									<input type="number" step="1" min="1" max="3"
										class="form-control" id="server" name="server"
										value="<c:out value="${payoutSettings.server}" />" />
								</div>
								<label for="x" class="col-md-1 control-label">X</label>
								<div class="col-md-2">
									<input type="number" step="1" class="form-control" id="x"
										name="x" value="<c:out value="${payoutSettings.x}" />" />
								</div>
								<label for="y" class="col-md-1 control-label">Y</label>
								<div class="col-md-2">
									<input type="number" step="1" class="form-control" id="y"
										name="y" value="<c:out value="${payoutSettings.y}" />" />
								</div>
								<label for="z" class="col-md-1 control-label">Z</label>
								<div class="col-md-2">
									<input type="number" step="1" class="form-control" id="z"
										name="z" value="<c:out value="${payoutSettings.z}" />" />
								</div>
							</div>
							<div class="form-group row">
								<label for="warp" class="col-md-1 control-label">Warp</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="warp" name="warp"
										value="<c:out value="${payoutSettings.warp}" />" />
								</div>
								<div class="col-md-5">
									<input type="submit" class="btn btn-block btn-primary"
										name="savePayoutSettings" value="Speichern" />
								</div>
							</div>
						</fieldset>
					</form>
					<div class="col-md-12">
						<c:forEach var="pobox" items="${payoutSettings.payoutboxes}"
							varStatus="loop">
							<div class="panel panel-default col-md-4 heightGroup0">
								<form class="form-horizontal" method="POST"
									action="einstellungen">
									<fieldset>
										<legend>Auszahlungsbox S${loop.index+1}</legend>
										<c:if test="${empty pobox}">

											<input type="hidden" name="csrfPreventionSalt"
												class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
											<input type="hidden" name="server" value="${loop.index+1}" />
											<div class="col-md-12">
												<input type="submit" class="btn btn-block btn-primary"
													name="requestPOBox" value="Kostenlos beantragen">
											</div>
										</c:if>
										<c:if test="${not empty pobox}">
											<c:if test="${not pobox.verified}">
												<div class="form-group row alert alert-danger">Nicht
													verifiziert</div>
											</c:if>
											<div class="form-group row">
												<label for="x" class="col-xs-3 control-label">X</label>
												<div class="col-xs-9">
													<input type="number" step="1" class="form-control" id="x"
														name="x" value="<c:out value="${pobox.x}" />"
														readonly="readonly" />
												</div>
											</div>
											<div class="form-group row">
												<label for="y" class="col-xs-3 control-label">Y</label>
												<div class="col-xs-9">
													<input type="number" step="1" class="form-control" id="y"
														name="y" value="<c:out value="${pobox.y}" />"
														readonly="readonly" />
												</div>
											</div>
											<div class="form-group row">
												<label for="z" class="col-xs-3 control-label">Z</label>
												<div class="col-xs-9">
													<input type="number" step="1" class="form-control" id="z"
														name="z" value="<c:out value="${pobox.z}" />"
														readonly="readonly" />
												</div>
											</div>
										</c:if>
									</fieldset>
								</form>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingCoupons">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapseCoupons" aria-expanded="false"
						aria-controls="collapseMessages">Coupon einlösen</a>
				</h4>
			</div>
			<div id="collapseCoupons" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="headingCoupons">
				<div class="panel-body">
					<form class="form-horizontal" method="POST" action="einstellungen">
						<fieldset>
							<input type="hidden" name="csrfPreventionSalt"
								class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
							<div class="form-group row">
								<label for="code" class="col-md-1 control-label">Code</label>
								<div class="col-md-5">
									<input type="text" class="form-control" id="code" name="code" />
								</div>
								<div class="col-md-6 form-group-row">
									<input type="submit" class="btn btn-block btn-primary"
										name="redeemCoupon" value="Einlösen" />
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingRL">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapseRL" aria-expanded="false"
						aria-controls="collapseRL">Referral Links</a>
				</h4>
			</div>
			<div id="collapseRL" class="panel-collapse collapse" role="tabpanel"
				aria-labelledby="headingRL">
				<div class="panel-body">
					<c:set var="req" value="${pageContext.request}" />
					<c:set var="base"
						value="${fn:replace(fn:replace(req.requestURL, fn:substring(req.requestURI, 0, fn:length(req.requestURI)), req.contextPath),'http:','https:')}" />
					<form class="form-horizontal">
						<fieldset>
							<p class="help-block">
								Missachtung der <a href="p/agb#referralprogramm" target="_blank">Regeln
									zum Referralprogramm</a> kann zu einem Ausschluss aus dem
								Referralprogramm bzw. einer Sperrung des Accounts führen.
							</p>
							<div class="form-group row">
								<label class="col-md-3 control-label">Schwacher
									Nutzername:</label>
								<div class="col-md-9">
									<input type="text" class="form-control"
										value="<c:url value="${base}/login?ref=${name}"/>"
										readonly="readonly" onfocus="this.select();"
										onmouseup="return false;" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-3 control-label">Schwache Nutzerid:</label>
								<div class="col-md-9">
									<input type="text" class="form-control"
										value="<c:url value="${base}/login?ref=${sid}"/>"
										readonly="readonly" onfocus="this.select();"
										onmouseup="return false;" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-3 control-label">Starker
									Nutzername:</label>
								<div class="col-md-9">
									<input type="text" class="form-control"
										value="<c:url value="${base}/login?r=${name}"/> "
										readonly="readonly" onfocus="this.select();"
										onmouseup="return false;" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-3 control-label">Starke Nutzerid:</label>
								<div class="col-md-9">
									<input type="text" class="form-control"
										value="<c:url value="${base}/login?r=${sid}"/>"
										readonly="readonly" onfocus="this.select();"
										onmouseup="return false;" />
								</div>
							</div>
							<p>${applicationScope.appName} stellt testweise auch ein Werbebild zur
								Verfügung. In diesem werden aktuelle Preise und
								Preisentwicklungen angezeigt.</p>
							<img src="static/advertisements/ad.png" alt="Werbebild">
							<div class="form-group row">
								<label class="col-md-3 control-label">URL:</label>
								<div class="col-md-9">
									<input type="text" class="form-control"
										value="<c:url value="${fn:replace(base,'kt.','nocertkt.')}/static/advertisements/ad.png"/>"
										readonly="readonly" onfocus="this.select();"
										onmouseup="return false;" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-3 control-label">BB-Code (Schwache
									Nutzerid):</label>
								<div class="col-md-9">
									<input type="text" class="form-control"
										value="<c:url value="[url=${base}/login?ref=${sid}][img]${fn:replace(base,'kt.','nocertkt.')}/static/advertisements/ad.png[/img][/url]"/>"
										readonly="readonly" onfocus="this.select();"
										onmouseup="return false;" />
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingAPIT">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapseAPIT" aria-expanded="false"
						aria-controls="collapseAPIT">API Authentifizierung</a>
				</h4>
			</div>
			<div id="collapseAPIT" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="headingAPIT">
				<div class="panel-body">
					<div class=" col-md-12 alert alert-danger alert-dismissable">
						Gib dein Passwort niemals an andere Anwendungen oder Personen
						weiter! Gib nur Anwendungen, denen du vertraust, ein Token oder
						Zertifikat! Weise allen API Zugangsdaten nur so wenige Rechte wie
						wirklich nötig zu! Wenn eine authentifizierte Anwendung
						unerwünschte Aktionen durchführt, haftet ${applicationScope.appName} nicht für
						entstandene Schäden. Wenn eine Anwendung nicht mehr benutzt wird,
						sollte das Token gelöscht oder das Zertifikat entwertet werden!</div>
					<div class="panel panel-default col-md-12">
						Mithilfe von Tokens können sich Anwendungen gegenüber ${applicationScope.appName}
						authentifizieren. Für jedes Token kann dabei eine Menge an
						Berechtigungen vergeben werden, die mit diesem Token durchgeführt
						werden können. Aktionen, für die das Token keine Berechtigungen
						hat, können nicht durchgeführt werden. Für jede Anwendung sollte
						ein eigenes Token erstellt werden. Sobald die Anwendung nicht mehr
						benutzt wird, kann das Token entwertet werden (alle
						Berechtigungshäkchen entfernen und speichern).
						<p class="font-weight-bold">
							Benutzerid:
							<c:out value="${sid}" />
						</p>
						<div class="table-responsive">
							<table class="table table-bordered table-hover"
								id="tokenSettings">
								<thead>
									<tr>
										<th>Token ID</th>
										<th>Token</th>
										<th>IL</th>
										<th>NL</th>
										<th>AL</th>
										<th>OL</th>
										<th>BL</th>
										<th>HL</th>
										<th>AB</th>
										<th>OB</th>
										<c:if test="${admin}">
											<th>AIL</th>
											<th>AAL</th>
											<th>ABL</th>
											<th>AHL</th>
											<th>AOL</th>
											<th>ANL</th>
											<th>AIB</th>
											<th>ANB</th>
											<th>ARB</th>
											<th>AOB</th>
											<th>AAB</th>
											<th>WAL</th>
											<th>WAB</th>
										</c:if>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<c:forEach var="token" items="${tokens}">
									<tr id=tr${token.tid}>
										<td><input type="hidden" name="tid" value="${token.tid}" />${token.tid}</td>
										<td><input type="hidden" name="csrfPreventionSalt"
											class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
											<button class="btn btn-block btn-primary"
												onclick="$(this).replaceWith('${token.token}')">Anzeigen</button></td>
										<td><input type="checkbox" name="rItems"
											<c:if test="${token.permissions['rItems']}">checked="checked"</c:if> /></td>
										<td><input type="checkbox" name="rMessages"
											<c:if test="${token.permissions['rMessages']}">checked="checked"</c:if> /></td>
										<td><input type="checkbox" name="rPayouts"
											<c:if test="${token.permissions['rPayouts']}">checked="checked"</c:if> /></td>
										<td><input type="checkbox" name="rOrders"
											<c:if test="${token.permissions['rOrders']}">checked="checked"</c:if> /></td>
										<td><input type="checkbox" name="rItemmovements"
											<c:if test="${token.permissions['rItemmovements']}">checked="checked"</c:if> /></td>
										<td><input type="checkbox" name="rTradeexecution"
											<c:if test="${token.permissions['rTradeexecution']}">checked="checked"</c:if> /></td>
											
										<td><input type="checkbox" name="wPayouts"
											<c:if test="${token.permissions['wPayouts']}">checked="checked"</c:if> /></td>
										<td><input type="checkbox" name="wOrders"
											<c:if test="${token.permissions['wOrders']}">checked="checked"</c:if> /></td>
										<c:if test="${admin}">
											<td><input type="checkbox" name="arItems"
												<c:if test="${token.permissions['arItems']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="arPayouts"
												<c:if test="${token.permissions['arPayouts']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="arItemmovements"
												<c:if test="${token.permissions['arItemmovements']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="arTradeexecutions"
												<c:if test="${token.permissions['arTradeexecutions']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="arOrders"
												<c:if test="${token.permissions['arOrders']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="arMessages"
												<c:if test="${token.permissions['arMessages']}">checked="checked"</c:if> /></td>
												
											<td><input type="checkbox" name="awItems"
												<c:if test="${token.permissions['awItems']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="awMessages"
												<c:if test="${token.permissions['awMessages']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="awOrders"
												<c:if test="${token.permissions['awOrders']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="awRegistrations"
												<c:if test="${token.permissions['awRegistrations']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="awPayouts"
												<c:if test="${token.permissions['awPayouts']}">checked="checked"</c:if> /></td>
												
											<td><input type="checkbox" name="wwPayouts"
												<c:if test="${token.permissions['wwPayouts']}">checked="checked"</c:if> /></td>
											<td><input type="checkbox" name="wrPayouts"
												<c:if test="${token.permissions['wrPayouts']}">checked="checked"</c:if> /></td>
										</c:if>
										<td><button type="button"
												class="btn btn-block btn-primary" data-toggle="modal"
												onclick="$('#qrDiv').empty();$('#qrDiv').qrcode('uid=${sid}&tid=${token.tid}&tkn=${token.token}');$('#qrDiv').append('uid=${sid}&tid=${token.tid}&tkn=${token.token}');$('#qrModal').modal()">QR
												Code</button></td>
										<td><input type='hidden' name='editToken'
											value='editToken' />
											<button class="btn btn-block btn-primary"
												onclick="submitRowAsForm('settings','tr${token.tid}')">Speichern</button></td>
									</tr>
								</c:forEach>
								<tr id="trnew">
									<td><input type="hidden" name="csrfPreventionSalt"
										class="csrfPreventionSalt" value="${csrfPreventionSalt}" /></td>
									<td></td>
									<td><input type="checkbox" name="rItems" /></td>
									<td><input type="checkbox" name="rMessages" /></td>
									<td><input type="checkbox" name="rPayouts" /></td>
									<td><input type="checkbox" name="rOrders" /></td>
									<td><input type="checkbox" name="rItemmovements" /></td>
									<td><input type="checkbox" name="rTradeexecution" /></td>
									<td><input type="checkbox" name="wPayouts" /></td>
									<td><input type="checkbox" name="wOrders" /></td>
									<c:if test="${admin}">
										<td><input type="checkbox" name="arItems" /></td>
										<td><input type="checkbox" name="arPayouts" /></td>
										<td><input type="checkbox" name="arItemmovements"/></td>
										<td><input type="checkbox" name="arTradeexecutions"/></td>
										<td><input type="checkbox" name="arOrders" /></td>
										<td><input type="checkbox" name="arMessages" /></td>
										
										<td><input type="checkbox" name="awItems" /></td>
										<td><input type="checkbox" name="awRegister" /></td>
										<td><input type="checkbox" name="awPayouts" /></td>
										<td><input type="checkbox" name="awMessages" /></td>
										<td><input type="checkbox" name="awOrders" /></td>
												
										<td><input type="checkbox" name="wwPayouts" /></td>
										<td><input type="checkbox" name="wrPayouts" /></td>
									</c:if>
									<td></td>
									<td><input type='hidden' name='newToken' value='newToken' />
										<button class="btn btn-block btn-primary"
											onclick="submitRowAsForm('settings','trnew')">Erstellen</button></td>
								</tr>
							</table>
						</div>
						IL = Itemstand auslesen, NL = Nachrichten Auslesen, AL =
						Auszahlungen lesen, OL = Orders lesen, AB = Auszahlungen
						bearbeiten (erstellen, abbrechen, ...), OB = Orders bearbeiten
						(erstellen, abbrechen, ...).
					</div>
					<div class="panel panel-default col-md-12">
						Mithilfe eines Klientzertifikates kann sich eine Anwendung
						gegenüber ${applicationScope.appName} authentifizieren. Das Zertifikat kann
						mithilfe einer Zertifizierungsanfrage vom Server signiert werden.
						Der <a href="/download/certificateHelper/" target="_blank">${applicationScope.appName}
							Zertifikatsmanager</a> kann bei der Erstellung der Zertifikate
						helfen. zertifikate können unter dem Punkt Zertifikatsverwaltung
						widerrufen werden. <br> Benutzerid:
						<c:out value="${sid}" />
						<form id="apiClientCert" class="form-horizontal" method="post">
							<input type="hidden" class="csrfPreventionSalt"
								name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
								type="hidden" name="requestOTPs" value="requestOTPs" />
							<div class="form-group row">
								<label for="clientCertValidTime" class="col-md-3 control-label">Gültigkeitsdauer</label>
								<div class="col-md-9">
									<input type="number" step="30" min="30" max="720"
										class="form-control" id="clientCertValidTime"
										name="clientCertValidTime" value="360" />
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-12">
									<textarea id="apiCertCsr" name="csr" rows="5"
										class="form-control" placeholder="Hier den CSR einfügen"></textarea>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-12">
									<input id="sendapiClientCertrequest"
										class="btn btn-block btn-primary" value="Zertifikat anfordern"
										onClick="requestCertificate('api')" />
								</div>
							</div>
						</form>
						<div id="apiClientCertResult" class="col-md-12 collapse">
							<p>Das Zertifikat wurde erfolgreich generiert.</p>
							<p id="apiClientCertResultInfo"></p>
							<textarea id="apiClientCertResultCert" class="col-md-12"
								rows="10">
									</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingBPN">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapseBPN" aria-expanded="false"
						aria-controls="collapseBPT">Browser Push Benachrichtigungen</a>
				</h4>
			</div>
			<div id="collapseBPN" class="panel-collapse collapse" role="tabpanel"
				aria-labelledby="headingBPN">
				<div class="panel-body" id="webPushPanel">
					<div id="webPushError"
						class="col-md-12 alert alert-danger alert-dismissable collapse">
						Web Push Benachrichtigungen werden in diesem Browser vermutlich
						nicht unterstützt. <a href="https://caniuse.com/#feat=push-api"
							target="_blank">Mehr Infos:
							https://caniuse.com/#feat=push-api</a>
					</div>
					<form class="form-horizontal">
						<input type="hidden" name="csrfPreventionSalt"
							class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
						<div id="webPushInfo"></div>
						<div class="col-md-12 form-group row">
							<input id="webPushButton" type="button"
								class="btn btn-block btn-primary"
								value="Browser Push Status prüfen" onClick="toggleWP()" />
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading2fa">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapse2fa" aria-expanded="false"
						aria-controls="collapse2fa">Zwei-Faktor-Authentifizierung</a>
				</h4>
			</div>
			<div id="collapse2fa" class="panel-collapse collapse" role="tabpanel"
				aria-labelledby="heading2fa">
				<div class=" col-md-12 alert alert-danger alert-dismissable">
					Warnung: Wenn die Zwei-Faktor-Authentifizierung aktiviert ist, aber
					die entsprechenden Daten verloren oder unzugänglich werden, kann
					der Account nicht mehr benutzt werden. Wir empfehlen daher dringend
					mindestens zwei voneinander unabhängige Methoden zu aktivieren./></div>
				<c:if test="${mfas.size eq 1}">
					<div class=" col-md-12 alert alert-warning alert-dismissable">
						Mit diesem Account ist nur eine 2FA-Methode aktiv. Wir empfehlen
						dringend mindestens zwei voneinander unabhängige Methoden zu
						aktivieren.</div>
				</c:if>
				<div class="panel-group" id="accordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse1">Deaktivieren</a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse">
							<div class="panel-body">
								<form class="form-horizontal" method="post">
									<fieldset>
										<input type="hidden" name="csrfPreventionSalt"
											class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
										<input type="hidden" name="type" value="NONE" />
										<div class="form-group row">
											<div class="col-md-12">
												<input type="submit" class="btn btn-block btn-primary"
													name="request2fa"
													value="Zwei-Faktor-Authentifizierung vollständig deaktivieren" />
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse4">Backup Codes<c:if test="${mfas.otp}">
										<span class="glyphicon glyphicon-ok pull-right"></span>
									</c:if></a>
							</h4>
						</div>
						<div id="collapse4" class="panel-collapse collapse">
							<div class="panel-body">
								Backup Codes/Einmalpasswörter sind achtstellige Codes, die
								jeweils genau einmal verwendet werden können. Danach ist der
								jeweils verwendete Code nicht mehr gültig. Die Codes sollten am
								besten offline auf Papier ausgedruckt oder abgeschrieben und an
								einem sicherem Ort aufbewahrt werden.
								<form id="OTP" class="form-horizontal" method="post">
									<input type="hidden" class="csrfPreventionSalt"
										name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
										type="hidden" name="requestOTPs" value="requestOTPs" />
									<div class="form-group row">
										<div class="col-md-12">
											<input id="sendOTPrequest" class="btn btn-block btn-primary"
												value="Neue Einmalpasswörter anfordern"
												onClick="requestOTP()" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<textarea id="OTPfield" class="col-md-12" rows="10"
												disabled="disabled">Bestehende Einmalpasswörter können nicht eingesehen werden.</textarea>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse7">WebAuthn<c:if test="${mfas.webauthn}">
										<span class="glyphicon glyphicon-ok pull-right"></span>
									</c:if></a>
							</h4>
						</div>
						<div id="collapse7" class="panel-collapse collapse">
							<div class="panel-body">
								WebAuthn ermöglicht es dem Browser sich mithilfe von einem
								Schlüsselpaar gegenüber dem Server zu identifizieren. Dabei
								können sowohl Hardwaretoken (siehe Sicherheitsschlüssel (U2F))
								als auch Browserinterne Speicherverfahren verwendet werden.
								browserinterne Speicherverfahren können dabei zum Beispiel auch
								durch Fingerabdrücke oder ähnliche Methoden gesichert werden.
								<div id="no_WAN" class="alert alert-danger collapse">
									WebAuthn wird durch diesen Browser nicht unterstützt. Siehe <a
										href="https://caniuse.com/#feat=webauthn">https://caniuse.com/#feat=webauthn</a>
									für mehr Informationen.
								</div>
								<form id="WAN" class="form-horizontal" method="post">
									<fieldset>
										<input type="hidden" name="csrfPreventionSalt"
											class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
										<input type="hidden" name="type" value="WAN" /><input
											type="hidden" name="requestwan" value="submit" />
										<div class="form-group row">
											<div class="col-md-12">
												<input class="btn btn-block btn-primary" name="request2fa"
													value="WebAuthN aktivieren" onclick="requestWAN()" />
											</div>
										</div>
									</fieldset>
								</form>
								<p id="WANinfo"></p>
								<form id="WAN2" class="form-horizontal .d-none" method="post">
									<input type="hidden" name="csrfPreventionSalt"
										class="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
										type="hidden" id="WANresponse" name="2faToken" /> <input
										type="hidden" name="verify2faData" value="submit" />
								</form>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse5">Sicherheitsschlüssel (U2F)<c:if
										test="${mfas.u2f}">
										<span class="glyphicon glyphicon-ok pull-right"></span>
									</c:if></a>
							</h4>
						</div>
						<div id="collapse5" class="panel-collapse collapse">
							<div class="panel-body">
								Sicherheitsschlüssel sind Hardwaregeräte, die mit dem Computer
								verbunden und anschließend für die Zwei-Faktor-Authentifizierung
								benutzt werden können. Anstelle einen Code aus einer App, E-Mail
								oder einem Zettel abzutippen, muss bei diesen Geräten nur ein
								Button gedrückt werden und die Authentifizierung wird
								automatisch durchgeführt. ${applicationScope.appName} unterstützt nur Geräte,
								die den FIDO U2F Standard verwenden.
								<div id="no_U2F" class="alert alert-danger collapse">
									U2F wird durch diesen Browser vermutlich nicht unterstützt.
									Siehe <a href="https://caniuse.com/#feat=u2f">https://caniuse.com/#feat=u2f</a>
									für mehr Informationen.
								</div>
								<form id="U2F" class="form-horizontal" method="post">
									<fieldset>
										<input type="hidden" name="csrfPreventionSalt"
											class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
										<input type="hidden" name="type" value="U2F" /><input
											type="hidden" name="request2fa" value="submit" />
										<div class="form-group row">
											<div class="col-md-12">
												<input class="btn btn-block btn-primary" name="request2fa"
													value="U2F aktivieren" onclick="requestU2F()" />
											</div>
										</div>
									</fieldset>
								</form>
								<p id="U2Finfo"></p>
								<form id="U2F2" class="form-horizontal .d-none" method="post">
									<input type="hidden" name="csrfPreventionSalt"
										class="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
										type="hidden" id="U2Fresponse" name="2faToken" /> <input
										type="hidden" name="verify2faData" value="submit" />
								</form>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse2">Zeitbasiertes Einmalpasswort (TOTP) <c:if
										test="${mfas.totp}">
										<span class="glyphicon glyphicon-ok pull-right"></span>
									</c:if></a>
							</h4>
						</div>
						<div id="collapse2" class="panel-collapse collapse">
							<div class="panel-body">
								Das zeitbasierte Einmalpasswort kann mithilfe von Apps wie
								Google Authenticator (<a
									href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Play
									Store</a>, <a
									href="https://itunes.apple.com/de/app/google-authenticator/id388497605">App
									Store</a>), Authy(<a
									href="https://play.google.com/store/apps/details?id=com.authy.authy">Play
									Store</a>, <a
									href="https://itunes.apple.com/de/app/google-authenticator/id494168017">App
									Store</a>) oder ähnlichen verwendet werden. Die Apps generieren
								alle 30 Sekunden einen Code, welcher nach dem Login in das
								entsprechende Feld eingegeben werden kann.
								<form id="TOTP1" class="form-horizontal" method="post">
									<fieldset>
										<input type="hidden" class="csrfPreventionSalt"
											name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
											type="hidden" name="type" value="TOTP" /> <input
											type="hidden" name="request2fa" value="submit" />
										<div class="col-md-6"></div>
										<div class="col-md-6">
											<input class="btn btn-block btn-primary" value="Anfordern"
												onClick="requestTOTP()" />
										</div>
									</fieldset>
								</form>
								<form id="TOTP2" class="form-horizontal collapse" method="post">
									<fieldset>
										<input type="hidden" class="csrfPreventionSalt"
											name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
											type="hidden" name="type" value="TOTP" /> <input
											type="hidden" name="verify2faData" value="submit" />
										<div class="form-group row">
											<div id="TOTPqr" class="col-md-6"></div>
										</div>
										<div id="TOTPError"
											class=" col-md-12 alert alert-danger alert-dismissable collapse">

										</div>
										<div class="form-group row">
											<div class="col-md-6">
												<input type="number" step="1" min="1" max="999999"
													class="form-control" name="2faToken" placeholder="Token" />
											</div>
											<div class="col-md-6">
												<input class="btn btn-block btn-primary" value="Bestätigen"
													onClick="confirmTOTP()" />
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse3">E-Mail<c:if test="${mfas.mail}">
										<span class="glyphicon glyphicon-ok pull-right"></span>
									</c:if></a>
							</h4>
						</div>
						<div id="collapse3" class="panel-collapse collapse">
							<div class="panel-body">
								Bei dieser Methode, wird nach dem Login ein einmalig gültiger
								Code per E-Mail versendet, welcher anschließend in das
								entsprechende Feld eingegeben werden muss. Um die Sicherheit
								weiter zu erhöhen, kann ${applicationScope.appName} diese E-mails auch
								verschlüsselt versenden.
								<form id="Mail1" class="form-horizontal" method="post">
									<fieldset>
										<input type="hidden" class="csrfPreventionSalt"
											name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
											type="hidden" name="type" value="MAIL" /> <input
											type="hidden" name="request2fa" value="submit" />
										<div class="form-group row">
											<div class="col-md-12">
												<label class="radio-inline col-md-4">Verschlüsselung:</label>
												<div
													class="radio radio-info radio-inline mailEncryptionRadio">
													<input id="mailNONE" class="mailEncryptionRadio"
														type="radio" name="encryption" value="NONE" checked /> <label
														for="mailNONE">Keine</label>
												</div>
												<div
													class="radio radio-info radio-inline mailEncryptionRadio">
													<input id="mailSMIME" class="mailEncryptionRadio"
														type="radio" name="encryption" value="SMIME" /> <label
														for="mailSMIME">S/MIME</label>
												</div>
												<div class="radio radio-info radio-inline">
													<input id="mailPGP" class="mailEncryptionRadio"
														type="radio" name="encryption" value="PGP" /> <label
														for="mailPGP">PGP (BETA)</label>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-md-12">
												<textarea id="publickey" name="publickey" rows="5"
													class="form-control collapse"
													placeholder="Hier den Publickey einfügen"></textarea>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-md-6">
												<input type="email" class="form-control" name="mail"
													placeholder="E-Mail Adresse" />
											</div>
											<div class="col-md-6">
												<input class="btn btn-block btn-primary" value="Anfordern"
													onClick="requestMail()" />
											</div>
										</div>
									</fieldset>
								</form>
								<form id="Mail2" class="form-horizontal collapse" method="post">
									<fieldset>
										<input type="hidden" class="csrfPreventionSalt"
											name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
											type="hidden" name="type" value="MAIL" /> <input
											type="hidden" name="verify2faData" value="submit" />
										<div class="form-group row">
											<div class="col-md-6">
												<input type="number" step="1" min="1" max="999999"
													class="form-control" name="2faToken" placeholder="Token" />
											</div>
											<div class="col-md-6">
												<input class="btn btn-block btn-primary" value="Bestätigen"
													onClick="confirmMail()" />
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse6">Client-Authentifizierung<c:if
										test="${mfas.cert}">
										<span class="glyphicon glyphicon-ok pull-right"></span>
									</c:if></a>
							</h4>
						</div>
						<div id="collapse6" class="panel-collapse collapse">
							<div class="panel-body">
								Bei der Client-Authentifizierung nutzt der Browser ein
								Zertifikat um sich gegenüber dem Server zu authentifizeren. Das
								Zertifikat kann mithilfe einer Zertifizierungsanfrage vom Server
								ausgestellt werden und muss anschließend im Browser installiert
								werden. Der <a href="/download/certificateHelper/"
									target="_blank">${applicationScope.appName} Zertifikatsmanager</a> kann bei der
								Erstellung der Zertifikate helfen.<br> Benutzerid:
								<c:out value="${sid}" />
								<form id="2faClientCert" class="form-horizontal" method="post">
									<input type="hidden" class="csrfPreventionSalt"
										name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input
										type="hidden" name="requestOTPs" value="requestOTPs" />
									<div class="form-group row">
										<label for="clientCertValidTime"
											class="col-md-3 control-label">Gültigkeitsdauer</label>
										<div class="col-md-9">
											<input type="number" step="30" min="30" max="720"
												class="form-control" id="clientCertValidTime"
												name="clientCertValidTime" value="360" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<textarea id="clientCertCsr" name="csr" rows="5"
												class="form-control" placeholder="Hier den CSR einfügen"></textarea>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="send2faClientCertrequest"
												class="btn btn-block btn-primary"
												value="Zertifikat anfordern"
												onClick="requestCertificate('2fa')" />
										</div>
									</div>
								</form>
								<div id="2faClientCertResult" class="col-md-12 collapse">
									<p>Das Zertifikat wurde erfolgreich generiert.</p>
									<p id="2faClientCertResultInfo"></p>
									<textarea id="2faClientCertResultCert" class="col-md-12"
										rows="10">
									</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingGdpr">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapseGdpr" aria-expanded="false"
						aria-controls="collapseMessages">Datenschutzeinstellungen</a>
				</h4>
			</div>
			<div id="collapseGdpr" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="headingCoupons">
				<div class="panel-body">

					<h3>Notwendige Einverständnis</h3>
					<p>Das Speichern von allgemeinen Daten umfasst zwingend
						notwendige Daten wie den Nutzernamen, die eingezahlten Items,
						Handelsanfragen, Auszahlungsanfragen und ähnliches. Diese Daten
						sind für die Funktionsweise von ${applicationScope.appName} notwendig. Ein Entzug
						der Erlaubnis zum Speichern dieser Daten erzwingt eine Löschung
						des Kontos.</p>
					<p>Neben dem Speichern dieser Daten ist in dieser Erlaubnis
						auch enthalten, dass wir diese Daten dem Kadon-Team offenlegen,
						sollte dieses die Daten zur Bekämpfung von Betrugsfällen, Bugusing
						oder ähnlichem anfordern. Sollten wir auffällige Transaktionen auf
						dem Konto eines Nutzers festellen, die einen Betrug oder Bugusing
						auf den Servern von Kadcon als Ursache haben könnten, behalten wir
						uns vor bei dem Team unseren Verdacht dem Team von Kadcon
						vorzutragen.</p>
					<p>Außerdem fließen Aktionen des Spielers in veröffentlichte
						Statistiken ein. In diesen Statistiken werden weder eine ID noch
						der Name der Spieler genannt.</p>
					<p>
						<b>Berechtigung aktuell erteilt: JA</b>
					</p>
					<h3>Veröffentlichung von Gewinnspieldaten</h3>
					<p>Um Gewinnspiele für alle Spieler nachvollziehbar zu machen,
						veröffentlichen wir nach Abschluss des Gewinnspiels alle
						Transaktionen und Itemstände, die das Gewinnspielitem beinhalten.
						Diese Daten beinhaltet die Anzahl der Items dieses Types, die such
						zum Vorlosungszeitpunk im Inventar des Spielers befinden, alle
						eröffneten Orders(Name des Erstellers, Erstellungszeitpunkt, Kauf
						oder Verkauf, Anzahl der Items und Preis pro Item) und alle
						durchgeführten Trades(Zeitpunk der Durchführung, Name des
						Verkäufers, Name des Käufers, Anzahl der Gehandelten Items,
						tatsächlicher Preis pro Item) für dieses Item. IDs von
						gewinnspielitems sind immer kleiner als -1. Daten zu Items mit
						höherer ID werden nicht veröffentlicht.</p>
					<p>Sollte diese Berechtigung später wieder entzogen werden,
						werden alle Aktionen dieses Spielers unter dem Namen "Anonym"
						veröffentlicht.</p>
					<p>Ohne diese Berechtigung kann nicht mit Gewinnspielitems
						gehandelt werden und es dürfen keine Gewinnspielitems besessen
						werden.</p>
					<form class="form-horizontal" method="POST">
						<fieldset>
							<input type="hidden" name="csrfPreventionSalt"
								class="csrfPreventionSalt" value="${csrfPreventionSalt}" />
							<c:choose>
								<c:when test="${gdpr.publishLotteryData}">
									<b>Berechtigung aktuell erteilt: JA</b>
									<input type="hidden" name="revoke" value="PL">
									<input type="submit" name="modifyGdpr"
										value="Berechtigung entziehen">
								</c:when>
								<c:otherwise>
									<b>Berechtigung aktuell erteilt: NEIN</b>
									<input type="hidden" name="grant" value="PL">
									<input type="submit" name="modifyGdpr"
										value="Berechtigung erteilen">
								</c:otherwise>
							</c:choose>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingCertificates">
				<h4 class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse"
						href="#collapseCertificates" aria-expanded="false"
						aria-controls="collapseCertificates">Zertifikatsverwaltung</a>
				</h4>
			</div>
			<div id="collapseCertificates" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="headingCertificates">
				<div class="panel-body">
					<div class="row flip-scroll">
						<table class="table">
							<thead>
								<tr>
									<th>Seriennummer</th>
									<th>Typ</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="certificatesTableBody">
								<!-- will be filled by script -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="qrModal" role="dialog">
		<div class="modal-dialog" id=qrModalDialog>
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="qrDiv"></div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/c/foot.jsp" />
</body>
</html>